﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceGateways;
using System.Text;


namespace InterpriseSuiteEcommerce
{
    public partial class get_quote : SkinBase
    {
		IStringResourceService _stringResourceService = null;
        string ItemCode = string.Empty;

		protected override void OnInit(EventArgs e)
        {
            PageNoCache();
            RequireSecurePage();
			RequireCustomerRecord();
			  
			InitializeDomainServices();
            base.OnInit(e);
        }

		protected override void OnLoad(EventArgs e)
        {
            InitPageContent();
            base.OnLoad(e);
        }

        protected void page_load (object sender, System.EventArgs e)
        {
            SetCustomerProfileValues();
        }
		
		private void InitializeDomainServices()
        {
            
            _stringResourceService = ServiceFactory.GetInstance<IStringResourceService>();
        
        }
		
	    private void InitPageContent()
        {
			renderProductInfo();
            SetPrintedColoursValue();
			
			char[] delimiterChars = { ',' };
			
			var i = AppLogic.AppConfig("custom.states").Trim();
			string[] str = i.Split( delimiterChars );
			
			var listItem = new ListItem();
			listItem.Text  = "- Please Select --";
			listItem.Value = "";
			
			drpState.Items.Add(listItem);
			
			char[] d = { ':' };
			foreach( string y in str ){
				
				string[] s = y.Split(d);
				
				listItem = new ListItem();
				listItem.Value = s[0];
				listItem.Text  = s[0];
				
				drpState.Items.Add(listItem);
                if (ThisCustomer.PrimaryShippingAddress.State.ToLower() == listItem.Value.ToLower())
                {
                    drpState.SelectedIndex = drpState.Items.Count - 1;
                }
			}
			
            //drpCountry.Text = DB.GetSqlS("SELECT TOP 1 CountryCode AS S FROM ECommerceAddressCountryView");
            SetCostCenter();
		}

        private void renderProductInfo()
        {

            string id = Request.QueryString["id"];
            string color = Request.QueryString["colour"];
            litColor.Text = color;

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                string str = String.Format("SELECT ItemCode, ItemDescription, ItemName FROM InventoryItemView WHERE Counter = {0}", id.ToDbQuote());
                using (var reader = DB.GetRSFormat(con, str))
                {
                    if (reader.Read())
                    {
                        litProductName.Text = DB.RSField(reader, "ItemDescription");
                        ItemCode = DB.RSField(reader, "ItemCode");

                        var itemWebOption = ServiceFactory.GetInstance<IProductService>().GetWebOption(ItemCode);
                        litMinQty.Text = itemWebOption.MinOrderQuantity == null || itemWebOption.MinOrderQuantity == 0 ? "1" : itemWebOption.MinOrderQuantity.ToString("0.#");
                        txtQuantity.Text = litMinQty.Text;

                        litProductCode.Text = DB.RSField(reader, "ItemName");
                        displayProductSize(ItemCode);

                        var img = ProductImage.Locate("product", ItemCode, "icon");
                        litImage.Text = string.Format("<img src=\"{0}\" title=\"{1}\" alt=\"{2}\" />", img.src, img.Title, img.Alt);
                    }
                }
            };

            var showFabric = DB.GetSqlNBool(string.Format("SELECT ShowFabricColour_C AS N FROM InventoryItemWebOption WHERE ItemCode = {0} AND WebsiteCode = {1}", DB.SQuote(ItemCode), DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode)));
            pnlFabric.Visible = showFabric;
            if (ThisCustomer.IsRegistered)
            {
                litIfYouHaveAlreadyAnAccount.Visible = false;
                litSignIn.Visible = false;
            }
        }
		
		private void displayProductSize( string itemCode ){
            string ProductSize_C = string.Empty;
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                string str = String.Format("SELECT ProductSize_C FROM InventoryItem WHERE ItemCode = {0}", itemCode.ToDbQuote());
                using (var reader = DB.GetRSFormat(con, str))
                {
                    if (reader.Read())
                    {
                        ProductSize_C = reader.ToRSField("ProductSize_C");
                    }
                }
            }
		
            //litProductDimension.Text =  ProductSize_C;
            litProductDimension.Text = AppLogic.FormatProductSize(ProductSize_C, false);
		}
		
        private void SetCostCenter()
        {
            ThisCustomer.ThisCustomerSession["CostCenter"] = AppLogic.SetCostCenter(ItemCode, false);
        }

        private void SetCustomerProfileValues()
        {
            if (ThisCustomer.IsRegistered)
            {
                txtFirstName.Text = ThisCustomer.FirstName;
                txtLastName.Text = ThisCustomer.LastName;
                txtEmail.Text = ThisCustomer.EMail;
                //txtMobile.Text = ThisCustomer.Mobile;
                txtPhone.Text = ThisCustomer.Phone;
                txtCompany.Text = ThisCustomer.CompanyName;
                //txtAddress.Text = ThisCustomer.PrimaryShippingAddress.Address1;
                //txtCity.Text = ThisCustomer.PrimaryShippingAddress.City;
                txtState.Text = ThisCustomer.PrimaryShippingAddress.State;
                drpState.Text = ThisCustomer.PrimaryShippingAddress.State;
                txtCountry.Text = ThisCustomer.PrimaryShippingAddress.Country;
                //drpCountry.Text = ThisCustomer.PrimaryShippingAddress.Country;
                txtPostal.Text = ThisCustomer.PrimaryShippingAddress.PostalCode;
            }
        }

        private void SetPrintedColoursValue()
        {
            var config = string.Empty;
            string itemCode = string.Empty;
            string desc = string.Empty;
            var output = new StringBuilder();
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRS("SELECT ConfigValue FROM EcommerceAppConfig WHERE Name = 'custom.smartbag.printed.colours'"))
                {
                    if (reader.Read())
                    {
                        config = reader.ToRSField("ConfigValue");
                    }
                }

                if (config != "")
                {
                    using (var reader = DB.GetRSFormat(con, "SELECT A.Counter, A.ItemCode, B.ItemDescription FROM InventoryItem A INNER JOIN InventoryItemDescription B ON A.ItemCode = B.ItemCode " +
                        "WHERE A.ItemName IN ({0}) AND LanguageCode = {1}", config, DB.SQuote(ThisCustomer.LanguageCode)))
                    {
                        while (reader.Read())
                        {
                            itemCode = reader.ToRSField("ItemCode");
                            desc = reader.ToRSField("ItemDescription");

                            var listItem = new ListItem();
                            listItem.Text = desc;
                            listItem.Value = itemCode;

                            drpPrintedColours.Items.Add(listItem);
                        }
                    }
                }
            }
        }
	}
}