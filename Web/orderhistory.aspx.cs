﻿using InterpriseSuiteEcommerceCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerce
{
    public partial class orderhistory : SkinBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RequiresLogin(CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables("QUERY_STRING"));
            var contact = "contactGUID".ToQueryString();
            if (contact == "ALL" && (ThisCustomer.ThisCustomerSession["JobRole"] == "Store Admin" || ThisCustomer.ThisCustomerSession["JobRole"] == "Admin"))
            {
                ThisCustomer.ThisCustomerSession["drpContactList"] = "ALL";
            }
            ltContactList.Text = AppLogic.GetContactList("orderhistory", contact);
            this.SETitle = "Welcome to the smartportal";
        }

        protected override void RegisterScriptsAndServices(ScriptManager manager)
        {

        }
    }
}