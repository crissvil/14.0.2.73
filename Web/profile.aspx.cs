﻿using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;
using System;
using System.Text;

namespace InterpriseSuiteEcommerce
{
    public partial class profile : SkinBase
    {
        IStringResourceService _stringResourceService = null;
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeDomainServices();
            InitializeProfileControls();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            RequiresLogin(CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables("QUERY_STRING"));
            if (ThisCustomer.ThisCustomerSession["JobRole"] == "Admin")
            {
                ltContactList.Text = AppLogic.GetContactList();
            }
            this.SETitle = "Welcome to the smartportal";
        }

        private void InitializeDomainServices()
        {
            _stringResourceService = ServiceFactory.GetInstance<IStringResourceService>();
        }

        private void InitializeProfileControls()
        {
            ProfileControl.AccountType = Interprise.Framework.Base.Shared.Const.CUSTOMER;
            ProfileControl.IsShowEditPasswordArea = true;
            ProfileControl.IsHideAccountNameArea = true;
            ProfileControl.LabelFirstNameText = _stringResourceService.GetString("account.aspx.55");
            ProfileControl.LabelLastNameText = _stringResourceService.GetString("account.aspx.56");
            ProfileControl.LabelContactNumberText = _stringResourceService.GetString("account.aspx.58");
            ProfileControl.LabeEditPasswordText = _stringResourceService.GetString("account.aspx.34");
            ProfileControl.LabelCurrentPasswordText = _stringResourceService.GetString("account.aspx.36");
            ProfileControl.LabelPasswordText = _stringResourceService.GetString("account.aspx.59");
            ProfileControl.LabelConfirmPasswordText = _stringResourceService.GetString("account.aspx.60");
            ProfileControl.DefaultSalutationText = _stringResourceService.GetString("account.aspx.61");
            ProfileControl.LabelEmailText = _stringResourceService.GetString("account.aspx.88");
            ProfileControl.LabelMobileNumberText = _stringResourceService.GetString("account.aspx.89");
            ProfileControl.FirstName = ThisCustomer.FirstName;
            ProfileControl.LastName = ThisCustomer.LastName;
            ProfileControl.Email = ThisCustomer.EMail;
            ProfileControl.ContactNumber = ThisCustomer.Phone;
            ProfileControl.Mobile = ThisCustomer.Mobile;
            ProfileControl.SelectedSalutation = ThisCustomer.Salutation;
            chkIsOkToEmail.Checked = ThisCustomer.OKToEMail;
            chkIsOver13Checked.Checked = true;
            ProfileControl.BindData();

            switch (ThisCustomer.CostCenter)
            {
                case "Smartbag-PREMIUM APG":
                case "Smartbag-PREMIUM CUE":
                case "Smartbag-PREMIUM DION LEE":
                    litBillingAddress.Text = ThisCustomer.PrimaryBillingAddress.DisplayString(true, true, true, false, "<div class='height-5'></div>");
                    break;
                default:
                    litBillingAddress.Text = ThisCustomer.PrimaryShippingAddress.DisplayString(true, true, true, false, "<div class='height-5'></div>");
                    break;
            }
            litShippingAddress.Text = ThisCustomer.PrimaryShippingAddress.DisplayString(true, true, true, false, "<div class='height-5'></div>");
            lnkChangeBilling.Text = "Change";
            lnkChangeBilling.NavigateUrl = "javascript:self.location='portalselectaddress.aspx?Checkout=false&AddressType=billing&returnURL=profile.aspx'";
            if (!AppLogic.AppConfigBool("AllowShipToDifferentThanBillTo"))
            {
                pnlShipping.Visible = false;
            }
            else
            {
                lnkChangeShipping.Text = "Change";
                lnkChangeShipping.NavigateUrl = "javascript:self.location='portalselectaddress.aspx?Checkout=false&AddressType=shipping&returnURL=profile.aspx'";
                lnkAddShippingAddress.NavigateUrl = "portalselectaddress.aspx?add=true&addressType=Shipping&Checkout=false&returnURL=profile.aspx";
            }
        }
                
    }
}