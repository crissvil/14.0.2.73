using System;
using InterpriseSuiteEcommerceCommon;
using System.Collections.Generic;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerce
{
    public partial class expressprint : SkinBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.CacheControl = "private";
            Response.Expires = 0;
            Response.AddHeader("pragma", "no-cache");
            if (AppLogic.AppConfigBool("GoNonSecureAgain"))
            {
                SkinBase.GoNonSecureAgain();
            }
            SectionTitle = "Express Print";

            string dcode = "dcode".ToQueryString();
            string ccode = "ccode".ToQueryString();
            string recid = "recid".ToQueryString();
            string qty = "qty".ToQueryString();
            //string designURL = string.Format("https://s3-ap-southeast-2.amazonaws.com/smartbag-test/index.html?id={0}", dcode);
            string designURL = string.Format("designit.aspx?dcode={0}&ccode={1}&recid={2}&qty={3}", dcode, ccode, recid, qty);
            List<XmlPackageParam> runtimeParams = new List<XmlPackageParam>();
            runtimeParams.Add(new XmlPackageParam("DesignURL", designURL));
            designURL = string.Format("uploaddesign.aspx?dcode={0}&ccode={1}&recid={2}", dcode, ccode, recid);
            runtimeParams.Add(new XmlPackageParam("UploadYourDesign", designURL));
            designURL = string.Format("wedesign.aspx?dcode={0}&ccode={1}&recid={2}", dcode, ccode, recid);
            runtimeParams.Add(new XmlPackageParam("WeDesign", designURL));
            litContent.Text = AppLogic.RunXmlPackage("page.expressprint.xml.config", null, ThisCustomer, SkinID, string.Empty, runtimeParams, false, false);
        }
        
    }
}