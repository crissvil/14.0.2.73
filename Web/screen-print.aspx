<%@ Page Language="C#" AutoEventWireup="true" CodeFile="screen-print.aspx.cs" Inherits="InterpriseSuiteEcommerce.screen_print" %>

<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>About Us</title>
</head>
<body>
    <div class="clear-both height-5">&nbsp;</div>
    <form id="frmScreenPrint" runat="server" action="">
        <asp:Panel ID="pnlPageContentWrapper" runat="server">
            <h1>Screen Print</h1>
            <div class="content-box">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <h3 style="text-align:center;font-size:18px;">MULTIPLE COLOUR / SOLID SCREEN PRINT</h3>
                    <center>
                        <img class="i-fancy-image" data-src="i-fancy-image-hidden-1" src="images/screen-print/sample-1.png" style="width: 90%" />
                        <div class="i-hide" id="i-fancy-image-hidden-1">
                            <img src="images/screen-print/poplet-1.jpg" style="width: 100%" />
                        </div>
                        <br />
                        <a href="javascript:void(1);" class="i-fancy-image" data-src="i-fancy-image-hidden-1">Click image to enlarge</a>
                    </center>
                    <br />
                    <p style="font-size: 16px">Screen print has unlimited use of solid colours and price is based on the number of print colours.</p>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <h3 style="text-align:center;font-size:18px;">SHADE EFFECT USING SCREEN PRINT</h3>
                    <center>
                        <img class="i-fancy-image" data-src="i-fancy-image-hidden-2" src="images/screen-print/sample-2.png" style="width: 90%" />
                        <div class="i-hide" id="i-fancy-image-hidden-2">
                            <img src="images/screen-print/poplet-2.jpg" style="width: 100%" />
                        </div>
                        <br />
                        <a href="javascript:void(1);" class="i-fancy-image" data-src="i-fancy-image-hidden-2">Click image to enlarge</a>
                    </center>
                    <br />
                    <p style="font-size: 16px">Shade effect achieved by using different sized screen perforations. Tonal effects and graduations of colour must use heat transfer.</p>
                </div>

                <p class="i-text-clear-5"></p>
            </div>
        </asp:Panel>
    </form>
</body>
</html>
