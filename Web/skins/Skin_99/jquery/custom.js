$(document).ready(function () {
	
  $("#drpIndustryGroup").change(function(){
	$("#txtIndustryGroup").val( $(this).val() );
  });
  
   $("#drpState").change(function(){
	   console.log( $(this).val() );
		$("#txtState").val( $(this).val() );
  });
  
   $("#drpCountry").change(function(){
	$("#txtCountry").val( $(this).val() );
  });
  
	$(".ui-dialog-titlebar").removeClass("i-bg-white");	

	$("#i-large-photo-selector").dialog({ 
			resizable: false,
			width: "auto",
			modal: true, 
			autoOpen:false,
			draggable : false,
			open: function(event, ui) {  

				$(".ui-dialog-titlebar-close").parent("div").html("<span id='ui-id-1' class='ui-dialog-title'>Image</span> <i id='i-close-notification-1' style='float: right;color:#000;cursor:pointer;'>X</i>"); 
				$(".ui-dialog-titlebar").addClass("i-bg-white");


				$("#i-close-notification-1").unbind("click").click( function(){
					
					$("#i-large-photo-selector").dialog("close");
					$(".ui-dialog-titlebar").removeClass("i-bg-white");	

				});

			}
	});

	
	var $alert = $( "#i-notification-alert" );

	$alert.dialog({ 
			resizable: false,
			width:400, 
			modal: true, 
			autoOpen:false,
			open: function(event, ui) {  

				$(".ui-dialog-titlebar-close").parent("div").html("<span id='ui-id-2' class='ui-dialog-title'>GET QUOTE ERROR</span> <i id='i-close-notification-2' style='float: right;'>X</i>"); 
				$(".ui-dialog-titlebar").removeClass("i-bg-white");	

				$("#i-close-notification-2").unbind("click").click( function(){
					
					$alert.dialog("close");
					

				});

			}
	});
	
	$(".f-selector").click(function(){
		var o = $(this);
		var c = $.trim(o.attr("data-color"));
		$("#i-fabric-selected").html(c);
		$("#i-fabric-selected-product").html("Selected: <b>" + c + "</b> (click to change)");
		$("#i-fabric-selected-product-value").html(c);
		$("#itemColour").val( c );
		
	});

	$("#i-get-quote").click(function(){

		var min = 1;
		var qty = $("#i-quote-qty").val();
		qty = parseInt( qty );
		
		if ($(".matrix-selector").val() == "BagSize") {
		    var message = '<p>Error: Please select a Bag Size first.</p>';
		    $.fancybox(message, {
		        'autoDimensions': false,
		        'width': 250,
		        'height': 'auto',
		        'padding': 20,
		        'transitionIn': 'elastic',
		        'transitionOut': 'elastic'
		    });
		    return false;
		}
		else if (qty % 1 === 0) {
		    $("#ItemQuantity").val(qty);
		    //$("#i-get-quote-selector").trigger("click");
		}
		else {
		    var message = '<p>Error: The quantity you have selected is below the Minimum Order Requirement</p>';
		    $.fancybox(message, {
		        'autoDimensions': false,
		        'width': 250,
		        'height': 'auto',
		        'padding': 20,
		        'transitionIn': 'elastic',
		        'transitionOut': 'elastic'
		    });
		    return false;
		}
		$(this).attr("href", function(i, href) {
		    return href + '&colour=' + $("#i-fabric-selected-product-value").text();
		});
	});
	
	$(".i-fancy-image").click( function(){
		
		var src = $(this).attr("data-src");
		src = $.trim( src );
		
		var message = "<p style='text-align:center;'>" + $("#" + src).html() + "</p>";
		$.fancybox(message, {
			'autoDimensions' : false,
			'width' : 450,
			'height' : "auto",
			'padding' : 20,
			'transitionIn' : 'elastic',
			'transitionOut' : 'elastic'
		});
		
	});

    $("#catwebformbutton").click(function () {
        $(this).css('display', 'none');
        $('#place-order-button-container').fadeIn('slow');
    });

    $("#BillingAddressControl_txtPostal").keypress(function () {
            //e.preventDefault();
    });

});


$(window).ready(function(){

  $(".i-category-images").unbind("click").click(function(){

    var $o = $(this);
    var src = $o.find("div").find("img").attr("src");
    var id = $o.find("div").find("img").attr("id").split('_');
    $("#saleInfo").html (document.getElementById("pnlStockHint_" + id[1]).innerHTML);
    if( typeof src == "undefined" || src == "" ){
      return false;
    }
    src = src.split("/");
    src = src.length > 1 ? src[src.length - 1] : src[0];
    src = $.trim(src);

    if( src == "" || src == "nopictureicon.gif" ){
      return false;
    }


    $("#i-large-photo-selector-src").attr("src", "images/product/large/" + src);
    $("#i-large-photo-selector").dialog( "open" );

  });

});

//Global Popup
function ShowModalPopup(message) {
    var height = $(window).height();
    var width = $(window).width();
    var popup = $("#modalpopup-content");
    var posTop = (height / 2 - popup.height() / 2) - 70;
    var posLeft = (width / 2 - popup.width() / 2);

    $("#modalpopup-content").css({
        "margin-top": posTop,
        "margin-left": posLeft
    });

    $("#modalpopup-text").html(message);
    $("#modalpopup").css({ "display": "block" });
}

function CloseModalPopup() {
    $("#modalpopup").css({ "display": "none" });
}

function ShowMessageBoxPopup(message) {
    var height = $(window).height();
    var width = $(window).width();
    var popup = $("#messagebox-content");
    var posTop = (height / 2 - popup.height() / 2) - 70;
    var posLeft = (width / 2 - popup.width() / 2);

    $("#messagebox-content").css({
        "margin-top": posTop,
        "margin-left": posLeft
    });

    $("#messagebox-text").html(message);
    $("#messagebox").css({ "display": "block" });
    $("#btnClose").focus();
}

function CloseMessageBoxPopup() {
    $("#messagebox").css({ "display": "none" });
}