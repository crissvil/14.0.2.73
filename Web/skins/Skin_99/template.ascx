﻿<!DOCTYPE html>
<%@ Control Language="c#" AutoEventWireup="false" Inherits="InterpriseSuiteEcommerce.TemplateBase" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<!--<![endif]-->

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    <title>(!METATITLE!)</title>
    <meta name="description" content="(!METADESCRIPTION!)" />
    <meta name="keywords" content="(!METAKEYWORDS!)" />
    <link rel="stylesheet" href="skins/Skin_(!SKINID!)/bootstrap/css/bootstrap.min.css" media="screen" />
    <link rel="stylesheet" href="skins/Skin_(!SKINID!)/ui-lightness/ui.custom.min.css" type="text/css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab" />
    <link rel="stylesheet" href="skins/Skin_(!SKINID!)/style.css" type="text/css" />
    <link rel="stylesheet" href="skins/Skin_(!SKINID!)/custom.css" type="text/css" />

    <script src="https://use.fontawesome.com/55068458dc.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/jquery-2.1.1.min.js"></script><%--nothing--%>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/jquery-migrate-1.2.1.min.js"></script><%--nothing--%>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/jquery-ui-1.8.16.custom.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/core.min.js"></script><%--nothing--%>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/menu.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/attribute.selectors.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/jquery.format.1.05.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/address.dialog.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/bubble.message.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/jquery.loader.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/custom.js"></script>
    
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/jquery.bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/camera/camera.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/fancybox/jquery.fancybox.pack.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/script.min.js"></script>
    <%--<script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/livesearch.min.js"></script>--%>
    <%--<script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/tmstickup.min.js"></script>--%>

    
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/jquery.ui.totop.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/jquery.unveil.min.js"></script>
    <%--<script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/superfish.min.js"></script>--%>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/elavatezoom/jquery.elevatezoom.min.js"></script>

    <script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/greensock/jquery.gsap.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/greensock/jquery.scrollmagic.min.js"></script>
    <%--<script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/greensock/TimelineMax.min.js"></script>--%>
    <%--<script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/greensock/TweenMax.min.js"></script>--%>
    <%--<script src="skins/Skin_(!SKINID!)/theme/js/photo-swipe/klass.min.js" type="text/javascript"></script>--%>
    <%--<script src="skins/Skin_(!SKINID!)/theme/js/photo-swipe/code.photoswipe-3.0.5.min.js" type="text/javascript"></script>--%>
    
    <!-- Global Loader -->
    <script type="text/javascript">
        $(document).ready(function () {
            $("body").globalLoader({ autoHide: false, image: 'images/ajax-loader.gif', opacity: 0.3, text: 'loading...' });
            //sample implementation to display the loader
            //$("body").data("globalLoader").show();
            AdGenerator('');
        });

        function AdGenerator(status) {
            $.ajax({
                type: "POST",
                url: "ActionService.asmx/AdGenerator",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: '{"ShowAd":' + JSON.stringify(status) + '}',
                success: function (result) {
                    if (result.d != "false") {
                        $("#top-ad").css('display', 'block');
                    }
                    else {
                        $("#top-ad").css('display', 'none');
                    }
                },
                fail: function (result) {

                }
            });
        }
    </script>

    <!-- Google Analytics -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script src="https://www.googletagmanager.com/gtag/js?id=UA-1376701-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'UA-1376701-1');
    </script>

    <!-- Global site tag (gtag.js) - Google Ads: 1069190230 -->
    <script src="https://www.googletagmanager.com/gtag/js?id=AW-1069190230"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());

        gtag('config', 'AW-1069190230');
    </script>

    <!-- Hotjar Tracking Code for https://www.smartbag.com.au/ -->
    <script>
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () { (h.hj.q = h.hj.q || []).push(arguments) };
            h._hjSettings = { hjid: 1053377, hjsv: 6 };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');
    </script>
    
    <%--For google adwords--%>
    <script src="https://www.googletagmanager.com/gtag/js?id=AW-1069190230"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());
        gtag('config', 'AW-1069190230');
    </script>

    (!JAVASCRIPT_INCLUDES!)
</head>

<body class="common-home">
    <asp:Panel ID="pnlForm" runat="server" Visible="false" />
    <!-- PAGE INVOCATION: '(!INVOCATION!)' -->
    <!-- PAGE REFERRER: '(!REFERRER!)' -->
    <!-- STORE LOCALE: '(!STORELOCALE!)' -->
    <!-- CUSTOMER LOCALE: '(!CUSTOMERLOCALE!)' -->
    <!-- STORE VERSION: '(!STORE_VERSION!)' -->
    <!-- CACHE MENUS: '(!AppConfig name="CacheMenus"!)' -->
    <!-- HEADER -->

    <!-- Global Loader -->
    <div id="modalpopup">
        <div id="modalpopup-content">
            <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
            <div id="modalpopup-text">Loading...</div>
        </div>
    </div>

    <!-- Global MessageBox -->
    <div id="messagebox">
        <div id="messagebox-content">
            <div id="messagebox-text">Loading...</div>
            <div id="messagebox-footer">
                <div style="text-align: right;">
                    <button type="button" name="btnClose" id="btnClose" class="messagebox-ok-button" onclick="CloseMessageBoxPopup();">OK</button></div>
            </div>
        </div>
    </div>

    <div id="top-ad" class="top-ad">
        <img src="skins/Skin_(!SKINID!)/images/top-ad.jpg" alt="smartbag ad" usemap="#ad-image-map" />
        <map name="ad-image-map">
            <%--<area target="_self" alt="Close" title="Close" href="#" onclick="AdGenerator('false'); return false;" coords="1160,6,1177,23" shape="rect" />--%>
            <area target="_self" alt="On Sale Now" title="On Sale Now" href="specialoffers.aspx" coords="931,67,1150,108" shape="rect" />
        </map>
    </div>
    <div id="topmessage">
        <div class="marquee">
            SPECIAL OFFER - Bundle and save! - Purchase white unprinted bags and we'll bundle with stickers printed with your logo/messages.**
        </div>
    </div>

    <div class="swipe">
        <div class="swipe-menu">
            <ul>
                <li>
                    <a href="account.aspx" title="My Account">
                        <i class="fa fa-user"></i>
                        <span>My Account
                        </span>
                    </a>
                </li>
                <li>
                    <a href="createaccount.aspx">
                        <i class="fa fa-user"></i>
                        Register
                    </a>
                </li>
                <li>
                    <a href="signin.aspx">
                        <i class="fa fa-lock"></i>
                        Login
                    </a>
                </li>

                <li>
                    <a href="shoppingcart.aspx" title="Shopping Cart">
                        <i
                            class="fa fa-shopping-bag"></i>
                        <span>Shopping Cart
                        </span>
                    </a>
                </li>
            </ul>
            <ul class="foot">
                <li>
                    <a href="about-us.aspx">About Us
                    </a>
                </li>
                <li>
                    <a href="how-to-order.aspx">How To Order
                    </a>
                </li>
                <li>
                    <a href="fabric-colours.aspx">Fabric Colours
                    </a>
                </li>
                <li>
                    <a href="faq.aspx">Fabric FAQ
                    </a>
                </li>
                <li>
                    <a href="privacy-policy.aspx">Privacy Policy
                    </a>
                </li>
                <li>
                    <a href="terms.aspx">Terms &amp; Conditions
                    </a>
                </li>
                <li>
                    <a href="print.aspx">Print
                    </a>
                </li>
                <li>
                    <a href="our-clients.aspx">Our Clients
                    </a>
                </li>
            </ul>
            <ul class="foot foot-1">
                <li>
                    <a href="contact-us.aspx">Contact Us
                    </a>
                </li>
                <li>
                    <a href="sitemap.aspx">Site Map
                    </a>
                </li>
                <li>
                    <a href="signout.aspx">Log Out
                    </a>
                </li>
            </ul>
            <%--<ul class="foot foot-2">
                <li>
                    <a href="#">Brands
                    </a>
                </li>
                <li>
                    <a href="#">Gift Vouchers
                    </a>
                </li>
                <li>
                    <a href="#">Specials
                    </a>
                </li>
            </ul>
            <ul class="foot foot-3">
                <li>
                    <a href="#">Order History
                    </a>
                </li>
                <li>
                    <a href="#">Newsletter
                    </a>
                </li>
            </ul>--%>
        </div>
    </div>

    <div id="page">
        <div class="shadow"></div>
        <div class="toprow-1">
            <ul>
                <li class="first">
                    <a class="swipe-control" href="#"><i class="fa fa-align-justify"></i></a>
                </li>
                <li>
                    <div id="cart-total-1" class="cart-total">(!NUM_CART_ITEMS-1!)</div>
                    <a href="shoppingcart.aspx"><img src="skins/Skin_(!SKINID!)/images/cart-icon.png" alt = "shopping cart" /></a>
                </li>
                <li>
                    <div class="special-offer">
                        <%--<img src="skins/Skin_(!SKINID!)/images/special-offer-icon.png" alt="On Sale Now" />
                        <a href="specialoffers.aspx" title="On Sale Now"><span class="uppercase">On Sale Now</span></a>--%>
                        <a href="specialoffers.aspx" title="On Sale Now"><img src="skins/Skin_(!SKINID!)/images/on-sale-now.png" alt="On Sale Now" /></a>
                    </div>
                </li>
                <li class="search-menu-container">
                    (!XmlPackage Name="rev.searchmobile"!)
                </li>
            </ul>
        </div>

        <div class="nav toprow-2">
            <ul>
                <li class="first">
                    <a class="swipe-control" href="#"><i class="fa fa-align-justify"></i></a>
                </li>
                <li>
                    <div id="cart-total-2" class="cart-total">(!NUM_CART_ITEMS-2!)</div>
                    <%--<button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="shopping-cart-button">
                        <img src="skins/Skin_(!SKINID!)/images/cart-icon.png" alt = "shopping cart" />
                    </button>--%>
                    <a href="shoppingcart.aspx"><img src="skins/Skin_(!SKINID!)/images/cart-icon.png" alt = "shopping cart" /></a>
                </li>
                <li>
                    <div class="special-offer">
                        <%--<img src="skins/Skin_(!SKINID!)/images/special-offer-icon.png" alt="On Sale Now" />
                        <a href="specialoffers.aspx" title="On Sale Now"><span class="uppercase">On Sale Now</span></a>--%>
                        <a href="specialoffers.aspx" title="On Sale Now"><img src="skins/Skin_(!SKINID!)/images/on-sale-now.png" alt="On Sale Now" /></a>
                    </div>
                </li>
                <li class="search-menu-container">
                    (!XmlPackage Name="rev.searchmobile"!)
                </li>
            </ul>
        </div>

        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3 col-xs-6 logo">
                        <a href="default.aspx">
                            <img src="skins/Skin_(!SKINID!)/images/logo.png" title="Smartbag" alt="Smartbag online store" class="img-responsive" /></a>
                    </div>
                    <div class="col-sm-6 col-xs-6 search-menu-container-normal">
                        (!XmlPackage Name="rev.search"!)
                    </div>
                    <div class="col-sm-3 col-xs-6 phone">
                        <a href="tel:1300874559">
                            <img src="skins/Skin_(!SKINID!)/images/call-us.jpg" alt="call us" /></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <nav id="top" class="top_panel">
                            <div id="top-links" class="nav">
                                <ul>
                                    <li class="first">
                                        <a href="default.aspx"><span class="hidden-sm"><i class="fa fa-arrow-circle-right"></i>Home</span></a>
                                    </li>
                                    <li class="dropdown">
                                        <a href="#" title="My Account" class="dropdown-toggle" data-toggle="dropdown">
                                            <span class="hidden-sm">My Account</span> <span class="caret hidden-sm"></span>
                                        </a>
                                        <ul id="i-my-account-menu" data-isregistered="(!IS_REGISTERED!)" class="dropdown-menu dropdown-menu-left">
                                            <li class="i-show-if-guest i-hide">
                                                <a href="createaccount.aspx">Register</a>
                                            </li>
                                            <li class="i-show-if-guest i-hide">
                                                <a href="signin.aspx">Login</a>
                                            </li>
                                            <li class="i-show-if-registered i-hide">
                                                <a href="account.aspx">Profile</a>
                                            </li>
                                            <li class="i-show-if-registered i-hide">
                                                <a href="signout.aspx">Log Out</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <%--<li>
                                        <a href="categories.aspx" title="Shop by Category"><span class="hidden-sm">Shop by Category</span></a>
                                    </li>--%>
                                    <li>
                                        <a href="shoppingcart.aspx">
                                            <div id="cart-total" class="cart-total">(!NUM_CART_ITEMS!)</div>
                                        <%--<button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="shopping-cart-button">
                                            <img src="skins/Skin_(!SKINID!)/images/cart-icon.png" alt = "shopping cart" />
                                        </button>--%>
                                        <img src="skins/Skin_(!SKINID!)/images/cart-icon.png" alt = "shopping cart" /></a>
                                    </li>
                                    <li>
                                        <div class="special-offer">
                                            <%--<img src="skins/Skin_(!SKINID!)/images/special-offer-icon.png" alt="On Sale Now" />
                                            <a href="specialoffers.aspx" title="On Sale Now"><span class="hidden-sm">On Sale Now</span></a>--%>
                                            <a href="specialoffers.aspx" title="On Sale Now"><img src="skins/Skin_(!SKINID!)/images/on-sale-now.png" alt="On Sale Now" /></a>
                                        </div>
                                    </li>
                                    <%--<li class="search-menu-container">
                                        (!XmlPackage Name="rev.search"!)
                                    </li>--%>
                                </ul>
                                <%--<div class="box-cart" id="i-cart-items-count">
                                    <div id="cart" class="cart">
                                        <button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="dropdown-toggle">
                                            <img src="skins/Skin_(!SKINID!)/images/cart-icon.png" alt = "shopping cart" />
                                            <span id="cart-total" class="cart-total"><span class="shopping-cart-num">(!NUM_CART_ITEMS!)</span>&nbsp;item(s)</span>
                                        </button>
                                         <div id="mini-cart"> (!MINICART!) </div>
                                         <ul class="dropdown-menu pull-right">
		                                    <li>
                                                (!MINICART!)
                                                <div id="mini-cart"> </div>
	                                        </li>
	                                    </ul>
                                    </div>
                                </div>--%>
                            </div>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div id="menu_container" runat="server"></div>
            </div>
        </div>

        <div class="header_modules"></div>

        <div id="page-container">
            <div class="container">
                <div class="row" id="content">
                    <aside id="column-left" class="col-sm-3 ">
                        <div class="box category">
                            (!XmlPackage Name="rev.categories"!)        

                        </div>
                    </aside>
                    <!-- CONTENT -->
                    <div class="col-sm-9 full-screen">
                        <div class="main-content box">
                            <!-- MAIN CONTENT -->

                            <%--Breadcrumb not used by Smartbag--%>
                            <%--(!SECTION_TITLE!)--%>

                            <asp:PlaceHolder ID="PageContent" runat="server"></asp:PlaceHolder>
                            <div class="clear-both"></div>

                            <!-- END OF MAIN CONTENT -->
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="content_bottom">
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="footer-line">
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <div class="footer_box">
                        <h5>Information</h5>
                        <ul class="list-unstyled">
                            <li><a href="about-us.aspx">About Us</a></li>
                            <li><a href="how-to-order.aspx">How To Order</a></li>
                            <li><a href="fabric-colours.aspx">Fabric Colours</a></li>
                            <li><a href="faq.aspx">Fabric FAQ</a></li>
                            <li><a href="privacy-policy.aspx">Privacy Policy</a></li>
                            <li><a href="terms.aspx">Terms &amp; Conditions</a></li>
                            <li><a href="print.aspx">Print</a></li>
                            <li><a href="our-clients.aspx">Our Clients</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <div class="footer_box">
                        <h5>Customer Service</h5>
                        <ul class="list-unstyled">
                            <li><a href="contact-us.aspx">Contact Us</a></li>
                            <li><a href="sitemap.aspx">Site Map</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 footer_box_spacer">
                    <div class="footer_box">
                        <h5>My Account</h5>
                        <ul class="list-unstyled">
                            <li><a href="account.aspx">Update My Profile</a></li>
                            <li><a href="account.aspx">Track Current Orders</a></li>
                            <li><a href="account.aspx">Previous Orders</a></li>
                            <li class="i-show-if-guest i-hide"><a href="signin.aspx">Login</a></li>
                            <li class="i-show-if-registered i-hide"><a href="signout.aspx">Log out</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <div class="footer_box">
                        <h5>Connect With Us</h5>
                        <ul class="list-unstyled social">
                            <li>
                                <a href="https://www.facebook.com/smartbagaus/" target="_blank">
                                    <span class="fa fa-facebook"></span>
                                    Facebook                            
                                </a>
                            </li>
                            <li>
                                <a href="//www.twitter.com/">
                                    <span class="fa fa-twitter"></span>
                                    Twitter
                                </a>
                            </li>
                            <li>
                                <a href="//plus.google.com/">
                                    <span class="fa fa-google-plus"></span>
                                    Google +
                                </a>
                            </li>
                            <li>
                                <a href="//www.youtube.com/">
                                    <span class="fa fa-youtube"></span>
                                    YouTube
                                </a>
                            </li>
                            <li>
                                <a href="blog.aspx?showarticle=2">
                                    <span class="fa fa-pencil-square-o"></span>
                                    Blog
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clear">
                </div>
            </div>
        </div>
    </footer>

    <div class="copyright">
        <div class="container">
            <div class="col-md-8 col-sm-9 col-xs-12">
                <span class="footer-logo">
                    <img src="skins/Skin_(!SKINID!)/images/logo-footer.png" title="Smartbag" alt="Smartbag online store" /></span>&nbsp;&nbsp;
                <span class="footer-copyright-text">&copy; Copyright 2019 | eCommerce Made by SMART WEB MARKETING</span>
            </div>
            <div class="col-md-4 col-sm-3 col-xs-12 secure-payment">
                <span style="vertical-align: middle;">Secure Payment Methods</span><img src="skins/Skin_(!SKINID!)/images/payment-methods.jpg" title="Payment Methods" alt="Smartbag online store" />

            </div>
        </div>
    </div>

    <div id="i-large-photo-selector" title="">
        <div id="saleInfo"></div>
        <div style="width: 100%; padding: 5px; text-align: center; margin-bottom: 39px;">
            <img id="i-large-photo-selector-src" src="" style="max-width: 100%;" />
        </div>
    </div>

    <div class="modal fade" id="fabricColoursModal">
        <div class="modal-dialog">
            <div class="modal-content fabric-colours-container">
                <div style="border: 2px solid #808080;">
                    <div class="modal-header" style="padding: 7px 14px 20px 4px;">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">x </span></button>
                    </div>
                    <div class="modal-body">
                        <h1>Click on a colour to select a colour template:</h1>

                        <ul class="c-fabric-colours">
                            <li id="c-lime" data-dismiss="modal" data-color="Lime" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/lime.png" alt="lime" />
                                </a>
                                <span>Lime</span>
                            </li>
                            <li id="c-pine" data-dismiss="modal" data-color="Pine" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/pine.png" alt="pine" />
                                </a>
                                <span>Pine</span>
                            </li>
                            <li id="c-bottle" data-dismiss="modal" data-color="Bottle" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/bottle.png" alt="bottle" />
                                </a>
                                <span>Bottle</span>
                            </li>
                            <li id="c-dalton" data-dismiss="modal" data-color="Dalton" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/dalton.png" alt="dalton" />
                                </a>
                                <span>Dalton</span>
                            </li>
                            <li id="c-royal" data-dismiss="modal" data-color="Royal" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/royal.png" alt="royal" />
                                </a>
                                <span>Royal</span>
                            </li>
                            <li id="c-electric" data-dismiss="modal" data-color="Electric" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/electric.png" alt="electric" />
                                </a>
                                <span>Electric</span>
                            </li>
                            <li id="c-navy" data-dismiss="modal" data-color="Navy" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/navy.png" alt="navy" />
                                </a>
                                <span>Navy</span>
                            </li>
                            <li id="c-slate" data-dismiss="modal" data-color="Slate" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/slate.png" alt="slate" />
                                </a>
                                <span>Slate</span>
                            </li>
                            <li id="c-white" data-dismiss="modal" data-color="White" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/white.png" alt="white" />
                                </a>
                                <span>White</span>
                            </li>
                            <li id="c-natural" data-dismiss="modal" data-color="Natural" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/natural.png" alt="natural" />
                                </a>
                                <span>Natural</span>
                            </li>
                            <li id="c-pink" data-dismiss="modal" data-color="Pink" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/pink.png" alt="pink" />
                                </a>
                                <span>Pink</span>
                            </li>
                            <li id="c-lavender" data-dismiss="modal" data-color="Lavender" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/lavender.png" alt="lavender" />
                                </a>
                                <span>Lavender</span>
                            </li>
                            <li id="c-sunflower" data-dismiss="modal" data-color="Sunflower" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/sunflower.png" alt="sunflower" />
                                </a>
                                <span>Sunflower</span>
                            </li>
                            <li id="c-jaffa" data-dismiss="modal" data-color="Jaffa" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/jaffa.png" alt="jaffa" />
                                </a>
                                <span>Jaffa</span>
                            </li>
                            <li id="c-red" data-dismiss="modal" data-color="Red" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/red.png" alt="red" />
                                </a>
                                <span>Red</span>
                            </li>
                            <li id="c-burdundy" data-dismiss="modal" data-color="Burgundy" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/burgundy.png" alt="burgundy" />
                                </a>
                                <span>Burgundy</span>
                            </li>
                            <li id="c-chocolate" data-dismiss="modal" data-color="Chocolate" class="f-selector">
                                <a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/chocolate.png" alt="chocolate" /></a>
                                <span>Chocolate</span>
                            </li>
                            <li id="c-black" data-dismiss="modal" data-color="Black" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/black.png" alt="black" />
                                </a>
                                <span>Black</span>
                            </li>

                            <li id="c-sand" data-dismiss="modal" data-color="Sand" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/sand.png" alt="sand" />
                                </a>
                                <span>Sand</span>
                            </li>
                            <li id="c-coolgrey" data-dismiss="modal" data-color="Cool Grey" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/cool-grey.png" alt="cool grey" />
                                </a>
                                <span>Cool Grey</span>
                            </li>
                            <li id="c-hotpink" data-dismiss="modal" data-color="Hot Pink" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/hot-pink.png" alt="hot pink" />
                                </a>
                                <span>Hot Pink</span>
                            </li>
                            <li id="c-grape" data-dismiss="modal" data-color="Grape" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/grape.png" alt="grape" />
                                </a>
                                <span>Grape</span>
                            </li>
                            <li id="c-teal" data-dismiss="modal" data-color="Teal" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/teal.png" alt="teal" />
                                </a>
                                <span>Teal</span>
                            </li>
                            <li id='c-babypink' data-dismiss="modal" data-color="Baby Pink" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/baby-pink.png" alt="baby pink" />
                                </a>
                                <span>Baby Pink</span>
                            </li>

                            <li id="c-purple" data-dismiss="modal" data-color="Purple" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/purple.png" alt="purple" />
                                </a>
                                <span>Purple</span>
                            </li>
                            <li id="c-cargo" data-dismiss="modal" data-color="Cargo" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/cargo.png" alt="cargo" />
                                </a>
                                <span>Cargo</span>
                            </li>
                            <li id="c-torquoise" data-dismiss="modal" data-color="Torquoise" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/torquoise.png" alt="torquoise" />
                                </a>
                                <span>Torquoise</span>
                            </li>
                            <li id="c-tennisball" data-dismiss="modal" data-color="Tennis Ball" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/tennis-ball.png" alt="tennis ball" />
                                </a>
                                <span>Tennis Ball</span>
                            </li>
                            <li id="c-aqua" data-dismiss="modal" data-color="Aqua" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/aqua.png" alt="aqua" />
                                </a>
                                <span>Aqua</span>
                            </li>
                            <li id="c-shiraz" data-dismiss="modal" data-color="Shiraz" class="f-selector">
                                <a href="#">
                                    <img src="skins/Skin_(!SKINID!)/images/fabric-colours/shiraz.png" alt="shiraz" />
                                </a>
                                <span>Shiraz</span>
                            </li>
                        </ul>
                        <div style="clear: both;"></div>
                        <h3>Pantone colour match for quantities 10,000+.
                            <br />
                            Print in any colour.</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BUBBLE MESSAGE -->
    <div id="ise-message-tips"><span id="ise-message" class="custom-font-style"></span><span id="message-pointer"></span></div>
    <!-- ADDRESS VERIFICATION -->
    (!ADDRESS_VERIFICATION_DIALOG_LISTING!)

    <script type="text/javascript">
        $(window).load(function () {
            var is_registered = trim($("#i-my-account-menu").attr("data-isRegistered"));
            if (is_registered != '') {
                is_registered = is_registered.toLowerCase();
            }

            if (is_registered == "true") {

                $(".i-show-if-guest").remove();
                $(".i-show-if-registered").removeClass("i-hide");

            } else {

                $(".i-show-if-registered").remove();
                $(".i-show-if-guest").removeClass("i-hide");
            }

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setupControls();
            SetActiveTopMenu();
        });
        function setupControls() {
            //buttons
            $('.site-button').addClass('btn btn-info');
            $('#btnUpateWishList1').removeClass('btn-info');
            $('#btnUpdateWishList1').addClass('btn-success');
            $('#btnUpateWishList2').removeClass('btn-info');
            $('#btnUpateWishList2').addClass('btn-success');
            $('input.addto').addClass('btn btn-info');
            //store language
            var showLanguage = false;
            var languageVisibility = '(!COUNTRYDIVVISIBILITY!)';
            var languageContainer = $('#storeLanguage');
            if (languageVisibility == 'visible') { showLanguage = true; }
            if (showLanguage) { languageContainer.show(); }
            //store vat
            var showVat = false;
            var vatVisibility = '(!VATDIVVISIBILITY!)';
            var vatContainer = $('#storeVAT');
            if (vatVisibility == 'visible') { showVat = true; }
            if (showVat) { vatContainer.show(); }

            $("#i-cart-items-count").click(function () {
                location.href = "shoppingcart.aspx"
            });

            $("#sign-out").click(function () {
                location.href = "signout.aspx";
            });

            var custom_bag_header = $("#i-custom-bags-header");
            if (custom_bag_header.hasClass("no-arrow")) {
                custom_bag_header.parent("li").find("i").remove();
            }
        }

        function SetActiveTopMenu() {
            var pagePathName = window.location.pathname;
            var activemenu = pagePathName.substring(pagePathName.lastIndexOf("/") + 1);
            $("#top-links li a").each(function () {
                if (($(this).attr("href") == activemenu || $(this).attr("href") == '' || (activemenu == '' && $(this).attr("href") == 'default.aspx')) && ($(this).attr("href") != 'specialoffers.aspx'))
                    $(this).addClass("active");

                if (activemenu == 'createaccount.aspx' || activemenu == 'signin.aspx' || activemenu == 'account.aspx')
                    $('.dropdown-toggle').addClass("active");
            })
        }

        $("#btnSearch").click(function(e) {
            e.preventDefault();
            var minSearchKey = '3';
            var currentSearchKey = $("#txtSearch").val().length;
            var invalidLengthMsg = 'Please enter at least 3 characters in the Search For field.';

            if(currentSearchKey >= minSearchKey){
                var form = $(this).closest("form")
                form.submit();
            }
            else {
                alert(invalidLengthMsg);
            }
        });

    </script>
    <!-- BUY SAFE SEAL -->
    <%--(!BUY_SAFE_SEAL!)--%>
    <%--<script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/jquery.marquee.min.js"></script>--%>
	<script type="text/javascript">
        //jQuery('.marquee').marquee({
        //    speed: 15000,
        //    gap: 100,
        //    delayBeforeStart: 0,
        //    direction: 'left',

        //    pauseOnHover: true
        //});
        jQuery('div#topmessage').css('display', 'none');
    </script>
    
    <%--CHAT--%>
    <!-- Start of LiveChat (www.livechatinc.com) code -->
    <script type="text/javascript">
        window.__lc = window.__lc || {};
        window.__lc.license = 9586470;
        (function () {
            var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
            lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
        })();
    </script>
    <noscript>
    <a href="https://www.livechatinc.com/chat-with/9586470/">Chat with us</a>,
    powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener" target="_blank">LiveChat</a>
    </noscript>
    <!-- End of LiveChat code -->

	<link href="skins/Skin_(!SKINID!)/theme/stylesheet/livesearch.min.css" rel="stylesheet" />
    <link href="skins/Skin_(!SKINID!)/theme/stylesheet/owl-carousel.min.css" rel="stylesheet" />
    <link href="skins/Skin_(!SKINID!)/theme/stylesheet/photoswipe.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="skins/Skin_(!SKINID!)/theme/js/jquery.bxslider/jquery.bxslider.min.css" type="text/css" />
    <link rel="stylesheet" href="skins/Skin_(!SKINID!)/camera/camera.min.css" type="text/css" />
    <link rel="stylesheet" href="skins/Skin_(!SKINID!)/theme/js/fancybox/jquery.fancybox.min.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="skins/Skin_(!SKINID!)/theme/stylesheet/material-design.min.css" type="text/css" />
</body>
</html>