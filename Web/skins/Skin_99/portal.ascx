﻿<!DOCTYPE html>
<%@ Control Language="c#" AutoEventWireup="false" Inherits="InterpriseSuiteEcommerce.TemplateBase" TargetSchema="http://schemas.microsoft.com/intellisense/ie5" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    <title>(!METATITLE!)</title>
    <meta name="description" content="(!METADESCRIPTION!)" />
    <meta name="keywords" content="(!METAKEYWORDS!)" />
    <link rel="stylesheet" href="skins/Skin_(!SKINID!)/bootstrap/css/bootstrap.min.css" media="screen" />
    <link rel="stylesheet" href="skins/Skin_(!SKINID!)/ui-lightness/ui.custom.css" type="text/css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" />
    <%--<script src="https://use.fontawesome.com/55068458dc.js"></script>--%>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">

    <link rel="stylesheet" href="skins/skin_(!skinid!)/style.css" type="text/css" />
    <link rel="stylesheet" href="skins/skin_(!skinid!)/custom.css" type="text/css" />
    <link rel="stylesheet" href="skins/skin_(!skinid!)/portal.css" type="text/css" />

    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/jquery-migrate-1.2.1.js"></script>
    <script type="text/javascript" src="jscripts/core.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/minified/attribute.selectors.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/address.dialog.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/bubble.message.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/minified/jquery.loader.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/portal.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/theme/js/elavatezoom/jquery.elevatezoom.min.js"></script>

    <%--<link rel="stylesheet" href="skins/Skin_(!SKINID!)/style.min.css" type="text/css" />
    <link rel="stylesheet" href="skins/Skin_(!SKINID!)/custom.min.css" type="text/css" />
    <link rel="stylesheet" href="skins/Skin_(!SKINID!)/portal.min.css" type="text/css" />

    <script type="text/javascript" src="skins/Skin_(!SKINID!)/minified/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/minified/jquery-migrate-1.2.1.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/minified/core.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/minified/attribute.selectors.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/minified/address.dialog.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/minified/bubble.message.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/minified/jquery.loader.js"></script>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/minified/portal.js"></script>--%>

    <!-- Google Analytics -->
    <script type="text/javascript">
        var _gaq = _gaq || [];
        $(window).load(function () {
            var pageTracking = ise.Configuration.getConfigValue("GoogleAnalytics.PageTracking");
            var ecTracking = ise.Configuration.getConfigValue("GoogleAnalytics.ConversionTracking");
            var gaAccount = ise.Configuration.getConfigValue("GoogleAnalytics.TrackingCode");
            var orderNumber = getQueryString()["ordernumber"]; // get ordernumber query string
            var imAtConfirmOrderPage = false;

            if (typeof orderNumber != "undefined" && ecTracking == 'true') { // verify if ordernumber query string is defined and ecTracking app config is  true
                imAtConfirmOrderPage = true;
            }

            if ((pageTracking == 'true' || imAtConfirmOrderPage) && gaAccount != "") { // verify if pageTracking app config is true OR current page is orderconfirmation.aspx
                //  uncomment to test your Google Analytics on localhost:
                // _gaq.push(['_setDomainName', 'none']); 
                _gaq.push(['_setAccount', gaAccount]); // set google analytics account (see app config GoogleAnalytics.TrackingCode
                _gaq.push(['_trackPageview']); // request page tracking
            }
        });
    </script>
    (!JAVASCRIPT_INCLUDES!)

</head>
<body id="portal">
    <!-- Global Loader -->
    <asp:Panel ID="pnlForm" runat="server" Visible="false" />
    <div id="modalpopup">
        <div id="modalpopup-content">
            <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
            <div id="modalpopup-text">Loading...</div>
        </div>
        
    </div>
    
    <!-- Global MessageBox -->
    <div id="messagebox">
        <div id="messagebox-content">
            <div id="messagebox-text">Loading...</div>
            <div id="messagebox-footer">
                <div style="text-align:right;"><button type="button" name="btnClose" id="btnClose" class="messagebox-ok-button" onclick="CloseMessageBoxPopup();">OK</button></div>
            </div>
        </div>
    </div>

    <!-- Global ConfirmationBox -->
    <div id="confirmationbox">
        <div id="confirmationbox-content">
            <div id="confirmationbox-text">Loading...</div>
            <div id="confirmationbox-footer">
                <%--Can be used as storage of temp value--%>
                <input name="misc" id="misc" value="" type="hidden" />
                <span style="text-align:right;"><button type="button" name="btnNo" id="btnNo" class="confirmationbox-no-button" onclick="CloseMessageBoxPopup();">No</button></span>
                <span style="text-align:right;"><button type="button" name="btnYes" id="btnYes" class="confirmationbox-yes-button" onclick="CloseMessageBoxPopup();">Yes</button></span>
            </div>
        </div>
    </div>

    <div class="container">
        <%--<div>
            <span id="spanTimeLeft"></span> seconds left
        </div>--%>
        <%--Logo--%>
        <div class="company-logo">
            (!COMPANY_LOGO!)
        </div>
        <div class="top-header i-hide" data-isregistered="(!IS_REGISTERED!)">
            <span class="page-breadcrumb">Store: <b>(!PORTAL_SHIPTONAME!)</b></span>
            <span class="box-cart" id="i-cart-items-count">
                <div id="cart" class="cart">
                    <button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="dropdown-toggle">
                        <i class="fa fa-shopping-cart"></i>
                        <span id="cart-total3" class="cart-total3 hidden-xs"><span class="shopping-cart-num">(!NUM_CART_ITEMS!)</span>&nbsp;item(s)</span>
                        <%--<span class="fa fa-angle-down" id="shopping-cart"></span>--%>
                    </button>
                    <%--<div id="mini-cart">(!MINICART!) </div>--%>
                    <%--<ul class="dropdown-menu pull-right">
                    <li>(!MINICART!)
                                                <div id="mini-cart"></div>
                    </li>
                </ul>--%>
                </div>
            </span>
        </div>
        <div class="clear-both"></div>
        <div class="row" id="content">
            <div id="column-left" class="col-sm-3 ">
                <%--Left Menu--%>
                <div class="portal-menu">
                    (!XmlPackage Name="rev.portalmenu"!)
                </div>
            </div>
            <div class="col-sm-9">
                <%--Content Area--%>
                <div class="portal-content">
                    <!-- MAIN CONTENT -->
                    <asp:PlaceHolder ID="PageContent" runat="server"></asp:PlaceHolder>
                    <div class="clear-both"></div>
                    <!-- END OF MAIN CONTENT -->
                </div>
            </div>
        </div>
    </div>
    
    <!-- Bubble Message Container -->
    <div id="ise-message-tips">
    <div id="divMessageTips"><span id="ise-message" class="custom-font-style"></span></div>
        <span id="message-pointer"></span>
    </div>

    <%--Javascript initialization--%>
    <!-- GOOGLE ANALYTICS -->
    <script type="text/javascript">
        $(window).load(function () {
            var pageTracking = $.trim(ise.Configuration.getConfigValue("GoogleAnalytics.PageTracking"));
            var ecTracking = $.trim(ise.Configuration.getConfigValue("GoogleAnalytics.ConversionTracking"));
            var gaAccount = $.trim(ise.Configuration.getConfigValue("GoogleAnalytics.TrackingCode"));
            var orderNumber = getQueryString()["ordernumber"]; // get ordernumber query string
            var imAtConfirmOrderPage = false;

            if (typeof orderNumber != "undefined" && ecTracking == 'true') imAtConfirmOrderPage = true;

            if ((pageTracking == 'true' || imAtConfirmOrderPage) && gaAccount != "GoogleAnalytics.TrackingCode") {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            }

            var is_admin = trim($("#menu").attr("data-isadmin"));
            var cost_center = trim($("#menu").attr("data-costcenter"));

            if (is_admin == "false") {

                $(".portal-admin-menu").addClass("i-hide");
                $(".top-header").addClass("i-hide");
            }
            else {
                $(".portal-admin-menu").removeClass("i-hide");
                $(".top-header").removeClass("i-hide");
            }

            var is_registered = trim($(".top-header").attr("data-isregistered"));
            is_registered = is_registered.toLowerCase();
            if (is_registered == "false") {
                $(".portal-menu").addClass("i-hide");
                $(".top-header").addClass("i-hide");
            }
            else {
                $(".top-header").removeClass("i-hide");
                $(".i-link-sub-menus").removeClass("i-hide");
            }

            if (cost_center == "smartbag-premium cue" || cost_center == "smartbag-premium apg" || cost_center == "smartbag-premium dion lee")
            {
                $("#usage-by-product").addClass("i-hide");
                $("#usage-forecast").addClass("i-hide");
                $("#statement-of-account").addClass("i-hide");
            }
            else
            {
                $("#usage-by-product").addClass("i-hide");
                $("#usage-forecast").addClass("i-hide");
                //$("#statement-of-account").removeClass("i-hide");
            }

            $("#i-cart-items-count").click(function () {
                location.href = "portalcheckoutreview.aspx"
            });
        });

        //Logic for auto logout
        $(function () {
            $("body").on('click keypress', function () {
                ResetThisSession();
            });
        });

        var timeInSecondsAfterSessionOut = 3600; // change this to change session time out (in seconds).
        var secondTick = 0;

        function ResetThisSession() {
            secondTick = 0;
        }

        function StartThisSessionTimer() {
            secondTick++;
            var timeLeft = ((timeInSecondsAfterSessionOut - secondTick) / 60).toFixed(0); // in minutes
            timeLeft = timeInSecondsAfterSessionOut - secondTick; // override, we have 30 secs only 

            //Create span on page to view timer. 
            //$("#spanTimeLeft").html(timeLeft);

            if (secondTick > timeInSecondsAfterSessionOut) {
                clearTimeout(tick);
                window.location = "signout.aspx";
                return;
            }
            tick = setTimeout("StartThisSessionTimer()", 1000);
        }

        StartThisSessionTimer();
        //End Logic for auto logout
    </script>
</body>
</html>
