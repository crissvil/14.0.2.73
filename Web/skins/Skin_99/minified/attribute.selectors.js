$("document").ready(function () {
    var c = getQueryString().el, d = getQueryString().aid, a = getQueryString().gid, b = getQueryString().guid, g = getQueryString().dfp; updateAttributeUrls(d, a, c, b); if (null != d) {
        var e = 0, c = $(".selected-attributes").size(); $(".selected-attributes").each(function () { var a = $(this).attr("id"), a = a.split("::"), a = a[1]; $(this).addClass("selected-" + e); $(this).attr("onclick", "removeThisSelection(" + a + ", " + e + ")"); e++ }); e == c && 1 < c ? $("#remove-all").attr("href", g + ".aspx") : $("#remove-all").css("display",
        "none")
    } else $("#selections").css("display", "none"); 0 == $(".attributes a").size() && $("#attributes-listing").css("display", "none")
});
function updateAttributeUrls(c, d, a, b) {
    var g = "", e = "", f = "", m = "", h = "", l = ""; null != c ? (g = "-" + c, e = "-" + d, f = "_" + b, h = getQueryString().dfp) : (c = location.pathname, c = c.split("/"), activeBaseAspx = c = c[c.length - 1], h = c.split("."), h = h[0], l = h.split("-"), l = l[0]); var r = "", k = "", n = "", q = ""; $(".attributes a").each(function () {
        var d = $(this).attr("href"), c = $(this).attr("id"), b = c.split("#"); "parent" != b[0] && "selected" != b[0] ? (n = c.split("::"), r = n[1], k = n[2], q = d.split("-"), c = $(".attributes a").index(this), null != a && (m = "-" + a), b = d, "p" ==
        l && (h = getDefaultPageIfProduct(d)), b += "&aid=" + q[1] + g, b += "&gid=" + r + e, b += "&guid=" + k + f, b += "&el=" + c + m, b += "&dfp=" + h, $(this).attr("href", b)) : c = $(".attributes a").index(this); $(this).addClass("attribute-a-" + c)
    })
} function getDefaultPageIfProduct(c) { var d = "", d = c.split("?"), d = d[1], d = d.split("&"); c = d[1]; c = c.split("="); c = c[1]; d = d[2]; d = d.split("="); d = d[1]; return d = ("Category" == c ? "c" : "d") + "-" + d }
function removeThisSelection(c, d) {
    var a = getQueryString().aid, b = getQueryString().gid, g = getQueryString().guid, e = getQueryString().el, f = "", m = "", h = "", l = ""; if (null != e) {
        var a = a.split("-"), b = b.split("-"), g = g.split("_"), r = e.split("-"), k = [], e = [], n = [], q = []; if (1 < a.length) {
            var s = []; $(".selected-attributes").each(function () { var a = $(this).attr("id"), a = a.split("::"), a = a[1]; void 0 != a && s.push(a) }); var p = []; for (indx = 0; indx < s.length; ++indx) { var t = a.indexOf(s[indx]); s.length > t && p.push(t) } for (indx = 0; indx < p.length; ++indx) k.push(a[parseInt(p[indx])]),
            e.push(b[parseInt(p[indx])]), n.push(g[parseInt(p[indx])]), q.push(r[parseInt(p[indx])])
        } for (a = 0; a < k.length; a++) d != a && (f += k[a] + "-", m += q[a] + "-", h += e[a] + "-", l += n[a] + "_")
    } a = ""; 0 == d && 1 < k.length && (a = $(".selected-1").attr("id"), a = a.split("::"), a = a[1] + "-" + a[2]); 0 < d && d <= k.length && (a = $(".selected-0").attr("id"), a = a.split("::"), a = a[1] + "-" + a[2]); b = location.pathname; b = b.split("/"); g = getQueryString().dfp; e = "?EntityID=" + getQueryString().EntityID + "&EntityName=" + getQueryString().EntityName; e += "&aid=" + f.substr(0, f.length -
    1); e += "&gid=" + h.substr(0, h.length - 1); e += "&guid=" + l.substr(0, l.length - 1); e += "&el=" + m.substr(0, m.length - 1); e += "&dfp=" + g; f = b[1] + "/a-" + a + ".aspx" + e; "" != a && 2 == b.length && (f = "/a-" + a + ".aspx" + e); "" == a && 0 == k.length && 1 < b.length && (f = b[1] + "/" + g + ".aspx"); 0 == d && 1 == k.length && 1 < b.length && (f = b[1] + "/" + g + ".aspx"); "" == a && 2 == b.length && (f = g + ".aspx"); window.location = null != b[1] && 2 < b.length ? "/" + f : f
}
function getQueryString() { for (var c = [], d, a = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&"), b = 0; b < a.length; b++) d = a[b].split("="), c.push(d[0]), c[d[0]] = d[1]; return c };