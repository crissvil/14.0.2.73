jQuery(document).ready(function () {
    /**************swipe menu***************/
    jQuery('#page').click(function () {
        if (jQuery(this).parents('body').hasClass('ind')) {
            jQuery(this).parents('body').removeClass('ind');
            return false
        }
    })
    jQuery('.swipe-control').click(function () {
        if (jQuery(this).parents('body').hasClass('ind')) {
            jQuery(this).parents('body').removeClass('ind');
            return false
        }
        else {
            jQuery(this).parents('body').addClass('ind');
            return false
        }
    })
///****************BACK TO TOP*********************/
    $().UItoTop({easingType: 'easeOutQuart'});

    /************************************************************************************************shadow height*****************************************************************************************************/
    var sect = 1;
    $(document).ready(function () {
        $('.swipe').height($(window).height() - 50);

        $(window).resize(function () {
            $('.swipe').height($(window).height() - 50);
        });

        var sects = $('.swipe').size();

    });
    /**************lazy load***************/
    jQuery("img.lazy").unveil(1, function () {
        jQuery(this).load(function () {
            jQuery(this).animate({'opacity': 1}, 700);
        });
    });


    /**********************************************************add icon aside li *****************************************************************************/
    $(document).ready(function () {
        $('column').find('.box-category .menu  li li a').prepend('<i class="fa fa-angle-right "></i>');
        $('#content').find('ul.list-unstyled li a').prepend('<i class="fa fa-angle-right "></i>');
        $('.site-map-page #content ').find(' ul li a').prepend('<i class="fa fa-angle-right "></i>');
        $('.manufacturer-content ').find(' div>a').prepend('<i class="fa fa-angle-right "></i>');
        //$('footer .col-sm-3 ').find(' li>a').prepend('<i class="fa fa-chevron-right"></i>');
        $('#tm_menu div > ul > li > ul  ').find(' li>a').prepend('<i class="fa fa-chevron-right"></i>');
        $('.box.info .box-content ul li  ').find('a').prepend('<i class="fa fa-chevron-right"></i>');
    });
    /******************* category name height**************************/
    (function ($) {
        $.fn.equalHeights = function (minHeight, maxHeight) {
            tallest = (minHeight) ? minHeight : 0;
            this.each(function () {
                if ($(this).height() > tallest) {
                    tallest = $(this).height()
                }
            });
            if ((maxHeight) && tallest > maxHeight)tallest = maxHeight;
            return this.each(function () {
                $(this).height(tallest)
            })
        }
    })(jQuery)
    $(window).load(function () {
        if ($("#content .product-grid .name").length) {
            $("#content .product-grid .name").equalHeights()
        }
    });
    /**************related name height ******************/
    (function ($) {
        $.fn.equalHeights = function (minHeight, maxHeight) {
            tallest = (minHeight) ? minHeight : 0;
            this.each(function () {
                if ($(this).height() > tallest) {
                    tallest = $(this).height()
                }
            });
            if ((maxHeight) && tallest > maxHeight)tallest = maxHeight;
            return this.each(function () {
                $(this).height(tallest)
            })
        }
    })(jQuery)
    $(window).load(function () {
        if ($(".maxheight-r").length) {
            $(".maxheight-r").equalHeights()
        }

        if ($(".maxheight1").length) {
            $(".maxheight1").equalHeights()
        }

        if ($(".maxheight2").length) {
            $(".maxheight2").equalHeights()
        }

        if ($(".equal-height-767").length) {
            if ($(window).width() < 768) {
                $(".equal-height-767").equalHeights();
            }
        }

        if ($(".equal-height-767-2").length) {
            if ($(window).width() < 768) {
                $(".equal-height-767-2").equalHeights();
            }
        }
    });
    /***********CATEGORY DROP DOWN****************/
    jQuery("#menu-icon").on("click", function () {
        jQuery("#menu-gadget .menu").slideToggle();
        jQuery(this).toggleClass("active");
    });

    jQuery('#menu-gadget .menu').find('li>ul').before('<i class="fa fa-angle-down"></i>');
    jQuery('#menu-gadget .menu li i').on("click", function () {
        if (jQuery(this).hasClass('fa-angle-up')) {
            jQuery(this).removeClass('fa-angle-up').parent('li').find('> ul').slideToggle();
        }
        else {
            jQuery(this).addClass('fa-angle-up').parent('li').find('> ul').slideToggle();
        }
    });


    jQuery(".accordion-menu").on("click", function () {
        $(this).parent().children(".box-content").slideToggle();
        jQuery(this).toggleClass("active");
    });

    //jQuery('.box-category .menu').find('li>ul').before('<i class="fa fa-angle-up"></i>');
    jQuery('.box-category .menu li i').on("click", function () {
        if (jQuery(this).hasClass('fa-angle-down')) {
            jQuery(this).removeClass('fa-angle-down');
            jQuery(this).addClass('fa-angle-up').parent('li').find('> ul').slideToggle();
        }
        else {
            jQuery(this).removeClass('fa-angle-up');
            jQuery(this).addClass('fa-angle-down').parent('li').find('> ul').slideToggle();
        }
    });
    /***********column dropdown box*******************/
    if ($('body').width() < 768) {
        leftColumn = $('body').find('#column-left');
        leftColumn.remove().insertAfter('#content');
        jQuery("img.lazy").unveil(1, function () {
            jQuery(this).load(function () {
                jQuery(this).animate({'opacity': 1}, 700);
            });
        });
        jQuery('.col-sm-3 .box-heading h3').append('<i class="fa fa-minus-circle"></i>');
        jQuery('.col-sm-3 .box-heading').on("click", function () {
            if (jQuery(this).find('i').hasClass('fa-plus-circle')) {
                jQuery(this).find('i').removeClass('fa-plus-circle').parents('.col-sm-3 .box').find('.box-content').slideToggle();
                jQuery(this).find('i').addClass('fa-minus-circle');
            }
            else {
                jQuery(this).find('i').removeClass('fa-minus-circle');
                jQuery(this).find('i').addClass('fa-plus-circle').parents('.col-sm-3 .box').find('.box-content').slideToggle();
            }
        })
    }
    ;


    /*********product tabs**********/
    if ($('body').width() < 768) {
        jQuery('.tab-heading').append('<i class="fa fa-plus-circle"></i>');
        jQuery('.tab-heading').on("click", function () {
            if (jQuery(this).find('i').hasClass('fa-minus-circle')) {
                jQuery(this).find('i').removeClass('fa-minus-circle').parents('.tabs').find('.tab-content').slideToggle();
            }
            else {
                jQuery(this).find('i').addClass('fa-minus-circle').parents('.tabs').find('.tab-content').slideToggle();
            }
        })
    }
    ;

});


var isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent);

/***********************************/
if (!isMobile) {
    /***********************Green Sock*******************************/

    $(document).ready(function () {
        var stickMenu = false;
        var docWidth = $('body').find('.container').width();


        if (!isMobile) {
            // init controller
            controller = new ScrollMagic();
        }

    })


    function listBlocksAnimate(block, element, row, offset, difEffect) {
        if (!isMobile) {
            if (jQuery(block).length) {
                var i = 0;
                var j = row;
                var k = 1;
                var effect = -1;

                $(element).each(function () {
                    i++;

                    if (i > j) {
                        j += row;
                        k = i;
                        effect = effect * (-1);
                    }

                    if (effect == -1 && difEffect == true) {
                        ef = TweenMax.from(element + ':nth-child(' + i + ')', 0.5, {
                            left: -1 * 200 - i * 300 + "px",
                            alpha: 0,
                            ease: Power1.easeOut
                        })

                    } else {
                        ef = TweenMax.from(element + ':nth-child(' + i + ')', 0.5, {
                            right: -1 * 200 - i * 300 + "px",
                            alpha: 0,
                            ease: Power1.easeOut
                        })
                    }

                    var scene_new = new ScrollScene({
                        triggerElement: element + ':nth-child(' + k + ')',
                        offset: offset,
                    }).setTween(ef)
                        .addTo(controller)
                        .reverse(false);
                });
            }
        }
    }

    function listTabsAnimate(element) {
        if (!isMobile) {
            if (jQuery(element).length) {
                TweenMax.staggerFromTo(element, 0.3, {alpha: 0, rotationY: -90, ease: Power1.easeOut}, {
                    alpha: 1,
                    rotationY: 0,
                    ease: Power1.easeOut
                }, 0.1);
            }
        }
    }

}


