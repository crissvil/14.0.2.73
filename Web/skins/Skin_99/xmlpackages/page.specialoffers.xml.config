<?xml version="1.0" standalone="yes"?>
<package version="2.1" displayname="Special Offers" includeentityhelper="true" debug="false">

  <!-- ###################################################################################################### -->
  <!-- Licensed by Interprise Solutions.                                    -->
  <!-- http://www.InterpriseSolutions.com                                                                     -->
  <!-- For details on this license please visit  the product homepage at the URL above.                       -->
  <!-- THE ABOVE NOTICE MUST REMAIN INTACT.                                                                   -->
  <!-- ###################################################################################################### -->

  <query name="Products" rowElementName="Product">
    <sql>
      <![CDATA[ 
           exec EcommerceGetSpecialOffers @SearchTerm, @WebSiteCode, @LanguageCode, NULL, 'ANY', 'ANY', '0', '0', '0', '0',@SearchDescriptions, @CurrentDate, @ProductFilterID, @ContactCode, @CBMode, @PageNum, @CurrencyCode, @MinPrice, @MaxPrice
          ]]>
    </sql>
    <queryparam paramname="@SearchTerm" paramtype="request" requestparamname="SearchTerm" sqlDataType="varchar" defvalue="" validationpattern="" />
    <queryparam paramname="@WebSiteCode" paramtype="runtime" requestparamname="WebSiteCode" sqlDataType="varchar" defvalue="" validationpattern="" />
    <queryparam paramname="@LanguageCode" paramtype="runtime" requestparamname="LanguageCode" sqlDataType="varchar" defvalue="" validationpattern="" />
    <queryparam paramname="@SearchDescriptions" paramtype="runtime" requestparamname="SearchDescriptions" sqlDataType="varchar" defvalue="0" validationpattern="" />
    <queryparam paramname="@CurrentDate" paramtype="runtime" requestparamname="Date" sqlDataType="datetime" defvalue="null" validationpattern="" />
    <queryparam paramname="@ProductFilterID" paramtype="runtime" requestparamname="ProductFilterID" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@ContactCode" paramtype="runtime" requestparamname="ContactCode" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@CBMode" paramtype="runtime" requestparamname="CBNMode" sqlDataType="bit" defvalue="0" validationpattern="" />
    <queryparam paramname="@PageNum" paramtype="request" requestparamname="ppagenum" sqlDataType="int" defvalue="1" validationpattern="" />
    <queryparam paramname="@CurrencyCode" paramtype="runtime" requestparamname="CurrencySetting" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@MinPrice" paramtype="request" requestparamname="MinPrice" sqlDataType="decimal" defvalue="null" validationpattern="" />
    <queryparam paramname="@MaxPrice" paramtype="request" requestparamname="MaxPrice" sqlDataType="decimal" defvalue="null" validationpattern="" />
  </query>

  <XmlHelperPackage name="helper.entity.xml.config, helper.product.xml.config" />
  <PackageTransform>
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ise="urn:ise" xmlns:custom="urn:custom" exclude-result-prefixes="ise">
      <xsl:output method="html" omit-xml-declaration="yes" />
      <xsl:param name="LocaleSetting" select="/root/Runtime/LocaleSetting" />
      <xsl:param name="WebConfigLocaleSetting" select="/root/Runtime/WebConfigLocaleSetting" />

      <xsl:param name="pSearchTerm">
        <xsl:choose>
          <xsl:when test="/root/QueryString/searchterm">
            <xsl:value-of select="/root/QueryString/searchterm" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="/root/QueryString/searchterm" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <xsl:param name="IsUserRegistered">
        <xsl:choose>
          <xsl:when test="ise:ToLower(/root/Runtime/CustomerIsRegistered) = 'true'">true</xsl:when>
          <xsl:otherwise>false</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <xsl:param name="ShowItemPrice">
        <xsl:choose>
          <xsl:when test="(ise:AppConfigBool('WholesaleOnlySite') = 'true' and ise:IsCustomerRetail() = 'false') or (ise:AppConfigBool('WholesaleOnlySite') = 'false' and ise:IsCustomerRetail() = 'true') or (ise:AppConfigBool('ShowItemPriceWhenLogin') = 'true' and $IsUserRegistered = 'true')">true</xsl:when>
          <xsl:otherwise>false</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <xsl:param name="CurrentPage">
        <xsl:choose>
          <xsl:when test="/root/QueryString/entitytype">
            <xsl:choose>
              <xsl:when test="/root/QueryString/entitytype = 'p'">
                <xsl:value-of select="/root/QueryString/ppagenum" />
              </xsl:when>
              <xsl:when test="/root/QueryString/entitytype = 'c'">
                <xsl:value-of select="/root/QueryString/cpagenum" />
              </xsl:when>
              <xsl:when test="/root/QueryString/entitytype = 'd'">
                <xsl:value-of select="/root/QueryString/dpagenum" />
              </xsl:when>
              <xsl:when test="/root/QueryString/entitytype = 'm'">
                <xsl:value-of select="/root/QueryString/mpagenum" />
              </xsl:when>
              <xsl:when test="/root/QueryString/entitytype = 'a'">
                <xsl:value-of select="/root/QueryString/apagenum" />
              </xsl:when>
              <xsl:otherwise>1</xsl:otherwise>
            </xsl:choose>
          </xsl:when>
          <xsl:otherwise>1</xsl:otherwise>
        </xsl:choose>
      </xsl:param>

      <xsl:param name="BaseURL">
        search.aspx?SearchTerm=<xsl:value-of select="$pSearchTerm"  disable-output-escaping="yes" />
      </xsl:param>
      <xsl:param name="SubcatGridCols">4</xsl:param>
      <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
      <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
      <xsl:template match="/">
        <xsl:value-of select="ise:Topic('SearchPageHeader')" disable-output-escaping="yes" />

        <xsl:choose>
          <xsl:when test="ise:AppConfigBool('Search_ShowProductsInResults')='true' and count(/root/Products/Product)>0">
            <div class="grid">
              <xsl:value-of select="ise:LoadBatchProductImage('product', 'icon', 'center',  boolean('true'), /root/Products/Product | following-sibling::*[position() &lt; $SubcatGridCols])" />
              <xsl:value-of select="ise:LoadBatchItemWebOptionSettings(/root/Products/Product | following-sibling::*[position() &lt; $SubcatGridCols], 'true')"/>
              <xsl:value-of select="custom:LoadBatchItemUnitMeasures(/root/Products/Product | following-sibling::*[position() &lt; $SubcatGridCols])"/>
              <div class="sub-entity-header">
                <H1></H1>
                <h2>
                  All our paper bags, laminated bags and eco bags are at wholesale prices. These are even lower than wholesale.
                </h2>
              </div>
              <ul class="productList productSmall">
                <xsl:apply-templates select="/root/Products/Product" />
              </ul>
            </div>
          </xsl:when>
        </xsl:choose>

        <xsl:if test="$pSearchTerm != '' and count(/root/Products/Product)=0">
          <p align="left">
            <xsl:value-of select="ise:StringResource('search.aspx.8', $LocaleSetting)" disable-output-escaping="yes" />
            <b />
          </p>
        </xsl:if>
        <xsl:value-of select="ise:Topic('SearchPageFooter')" disable-output-escaping="yes" />
      </xsl:template>

      <xsl:template match="Product">
        <xsl:variable name="delta">
          <xsl:choose>
            <xsl:when test="(count(/root/Products/Product) mod number($SubcatGridCols)) = 0">0</xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="number($SubcatGridCols)-(count(/root/Products/Product) mod number($SubcatGridCols))" />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:variable name="rows" select="ceiling(count(/root/Products/Product) div number($SubcatGridCols))" />


        <xsl:if test="$SubcatGridCols = 1">
          <xsl:call-template name="ProductCell"></xsl:call-template>
        </xsl:if>

        <xsl:if test="position() mod $SubcatGridCols = 1 and $SubcatGridCols &gt; 1">

          <xsl:for-each select=". | following-sibling::*[position() &lt; $SubcatGridCols]">
            <xsl:call-template name="ProductCell"></xsl:call-template>
          </xsl:for-each>

          <xsl:if test="ceiling(position() div  number($SubcatGridCols)) = $rows and $delta &gt; 0">
            <xsl:call-template name="FillerCells">
              <xsl:with-param name="cellCount" select="$delta" />
            </xsl:call-template>
          </xsl:if>
        </xsl:if>
      </xsl:template>
      <xsl:template name="ProductCell">

        <xsl:param name="pName">
          <xsl:value-of select="ItemName" />
        </xsl:param>
        <xsl:param name="pSalesPromptName">
          <xsl:value-of select="SalesPromptName" />
        </xsl:param>
        <xsl:param name="CellWidth" select="100 div $SubcatGridCols" />
        <xsl:variable name="pDisplayName">
          <xsl:choose>
            <xsl:when test="string-length(ItemDescription)>0">
              <xsl:value-of select="ItemDescription" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="ItemName" />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <xsl:variable name="item_href" select="ise:ProductandEntityLink(Counter, SEName, ItemCode, ise:Encode($pDisplayName), 0)" />

        <li class="productItem" style="height: auto;">

          <div class="product-listing-layout-cont">
            <div class="instock-products">
              <div class="product-small-2">
                <div>
                  <xsl:choose>
                    <xsl:when test="IsSale_C='true'">
                      <span class="entity-item-flag-sale i-status-flag">
                        <xsl:attribute name="id">
                          <xsl:value-of select="concat('pnlStockHintBanner_',Counter)"/>
                        </xsl:attribute>
                        <div>
                          <xsl:attribute name="id">
                            <xsl:value-of select="concat('pnlStockHint_',Counter)"/>
                          </xsl:attribute>
                          <xsl:attribute name="name">
                            <xsl:value-of select="concat('pnlStockHint_',Counter)"/>
                          </xsl:attribute>
                          <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/sale.png')}">
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('imgStockHint_',Counter)"/>
                            </xsl:attribute>
                          </img>
                          <xsl:if test="ShowPricePerPiece_C = 'True'">
                            <div id="{concat('lblPricePerPiece_',Counter)}" name="{concat('lblPricePerPiece_',Counter)}" class="price-per-piece">
                              <xsl:value-of select="PricePerPieceLabel_C" disable-output-escaping="yes"/>
                            </div>
                          </xsl:if>
                        </div>
                      </span>
                    </xsl:when>
                    <xsl:otherwise>
                      <span class="i-status-flag entity-item-flag-sale">
                        <xsl:attribute name="id">
                          <xsl:value-of select="concat('pnlStockHintBanner_',Counter)"/>
                        </xsl:attribute>
                        <div>
                          <xsl:attribute name="id">
                            <xsl:value-of select="concat('pnlStockHint_',Counter)"/>
                          </xsl:attribute>
                          <xsl:attribute name="name">
                            <xsl:value-of select="concat('pnlStockHint_',Counter)"/>
                          </xsl:attribute>
                          <span>
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('lblStockHint_',Counter)"/>
                            </xsl:attribute>
                          </span>
                          <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/sale.png')}">
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('imgStockHint_',Counter)"/>
                            </xsl:attribute>
                          </img>
                          <xsl:if test="ShowPricePerPiece_C = 'True'">
                            <div id="{concat('lblPricePerPiece_',Counter)}" name="{concat('lblPricePerPiece_',Counter)}" class="price-per-piece">
                              <xsl:value-of select="PricePerPieceLabel_C" disable-output-escaping="yes"/>
                            </div>
                          </xsl:if>
                        </div>
                      </span>
                    </xsl:otherwise>
                  </xsl:choose>

                  <div style="padding: 5px;">
                    <div class="entity-item-price-per">
                      <xsl:attribute name="id">
                        <xsl:value-of select="concat('pnlItemPricePer_',Counter)"/>
                      </xsl:attribute>
                    </div>
                  </div>

                  <div class="entity-item-img">
                    <a href="javascript:void(1);" style="vertical-align:bottom;" class="i-category-images">
                      <xsl:value-of select="ise:DisplayImage('product', Counter, 'icon', SEAltText, 'AltText', 'center', ItemCode, boolean('true'))" disable-output-escaping="yes" />
                    </a>
                    <xsl:if test="IsFeatured = 'True'">
                      <div class="hot-seller-category">
                        <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/hot-seller.png')}">
                          <xsl:attribute name="id">
                            <xsl:value-of select="concat('imgHot_',Counter)"/>
                          </xsl:attribute>
                        </img>
                      </div>
                    </xsl:if>
                  </div>

                </div>

                <div class="product-options">

                  <div class="item-title">
                    <xsl:value-of select="ise:Encode($pDisplayName)" disable-output-escaping="yes" />
                  </div>

                  <xsl:choose>
                    <xsl:when test="translate(ItemType, $uppercase, $smallcase)='kit'">
                      <xsl:if test= "translate(HidePriceUntilCart, $uppercase, $smallcase) = 'false'">
                        <xsl:if test="$ShowItemPrice = 'true'">
                          <xsl:value-of select="ise:RegisterProduct(Counter, ItemCode, ItemType, false(), false())" disable-output-escaping="yes" />
                          <div>
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('pnlItemPriceLevelContainer_',Counter)"/>
                            </xsl:attribute>
                            <xsl:value-of select="ise:SetXmlPackageHelperTemplate(1)" disable-output-escaping="yes"/>
                            <xsl:value-of select="custom:GetPricingLevelExpressPrint(ItemCode)" disable-output-escaping="yes" />
                          </div>

                          <script type="text/javascript" language="Javascript">
                            $add_windowLoad(
                            function() {
                            ise.StringResource.registerString('showproduct.aspx.46', 'Please select an option to check availability');
                            ise.StringResource.registerString('showproduct.aspx.47', 'Item(s) Available');
                            var product = ise.Products.ProductController.getProduct(<xsl:value-of select="Counter" disable-output-escaping="yes"/>);
                            var ctrlSmartStockHint = new ise.Products.SmartStockHintControl(<xsl:value-of select="Counter" disable-output-escaping="yes"/>, 'imgStockHint_<xsl:value-of select="Counter" disable-output-escaping="yes"/>');
                            ctrlSmartStockHint.setProduct(product);

                            var id = '<xsl:value-of select="Counter" disable-output-escaping="yes"/>';
                            var labelPrice = $('#' + 'pnlItemPricePer_' + id);
                            if ( labelPrice === undefined) return;
                            <![CDATA[
                                var hasItem = false;
                                for(var ctr=0; ctr < product.groups.length; ctr++) {
                                    if (hasItem){break;}
                                    var group = product.groups[ctr];
                                    var items = group.getSelectedItems();
                                    for(var ictr=0; ictr < items.length; ictr++) {
                                        var item = items[ictr];
                                        if(item) {
                                            hasItem = item.hasAvailableStock();
                                        }
                                        if (hasItem){break;}
                                    }
                                }
                              ]]>

                            if (hasItem) {
                              ctrlSmartStockHint.showInStock();
                              $(labelPrice).css({background: '#fff'});
                              $(labelPrice).css({'margin-top': '20px'});
                            }
                            else {
                              $(labelPrice).text('SORRY, OUT OF STOCK');
                              $(labelPrice).removeClass('entity-item-price-per');
                              $(labelPrice).addClass('entity-item-price-out-of-stock');
                              ctrlSmartStockHint.showOutOfStock();
                            }
                            });
                          </script>

                          <div class="entity-item-dimensions">
                            <xsl:value-of select="custom:displayProductSize(ItemCode, 'true')" disable-output-escaping="yes" />
                          </div>

                          <div class="productItemCode">
                            <strong>Product Code:</strong>
                            <br/>
                            <span>
                              <xsl:value-of select="ise:Encode(ItemName)" disable-output-escaping="yes" />
                            </span>
                          </div>
                          
                          <div>
                            <xsl:value-of select="ise:SetXmlPackageHelperTemplate(1)" disable-output-escaping="yes"/>
                            <xsl:value-of select="ise:DisplayAddToCartForm(Counter, ItemCode, ItemType, 'h')" disable-output-escaping="yes" />
                          </div>

                        </xsl:if>
                      </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>

                      <xsl:choose>
                        <xsl:when test="translate(ItemType, $uppercase, $smallcase)='matrix group'">
                          <xsl:value-of select="ise:RegisterProduct(Counter, ItemCode, ItemType)" disable-output-escaping="yes" />
                          <div>
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('pnlItemPriceLevelContainer_',Counter)"/>
                            </xsl:attribute>
                            <xsl:value-of select="ise:DisplayPrice(Counter, ItemCode)" disable-output-escaping="yes" />
                          </div>
                          <xsl:value-of select="ise:DisplayStockHint(Counter, ItemCode, ItemType)" disable-output-escaping="yes" />
                          <xsl:value-of select="ise:SetXmlPackageHelperTemplate(1)" disable-output-escaping="yes"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="ise:RegisterProduct(Counter, ItemCode, ItemType, false(), false())" disable-output-escaping="yes" />
                          <xsl:value-of select="ise:SetXmlPackageHelperTemplate(1)" disable-output-escaping="yes"/>
                          <div>
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('pnlItemPriceLevelContainer_',Counter)"/>
                            </xsl:attribute>

                            <xsl:value-of select="custom:GetPricingLevel(ItemCode)" disable-output-escaping="yes" />
                          </div>
                        </xsl:otherwise>
                      </xsl:choose>

                      <script type="text/javascript" language="Javascript">
                        $add_windowLoad(
                        function() {
                        ise.StringResource.registerString('showproduct.aspx.46', 'Please select an option to check availability');
                        ise.StringResource.registerString('showproduct.aspx.47', 'Item(s) Available');
                        var product = ise.Products.ProductController.getProduct(<xsl:value-of select="Counter" disable-output-escaping="yes"/>);
                        var ctrlSmartStockHint = new ise.Products.SmartStockHintControl(<xsl:value-of select="Counter" disable-output-escaping="yes"/>, 'imgStockHint_<xsl:value-of select="Counter" disable-output-escaping="yes"/>');
                        ctrlSmartStockHint.setProduct(product);

                        var id = '<xsl:value-of select="Counter" disable-output-escaping="yes"/>';
                        var labelPrice = $('#' + 'pnlItemPricePer_' + id);
                        if ( labelPrice === undefined) return;
                        if (product.getItemType() == 'Non-Stock' ||product.getItemType() == 'Service' || product.getIsDropShip() == true){
                        $(labelPrice).css({background: '#fff'});
                        $(labelPrice).css({'margin-top': '20px'});
                        }
                        else {
                        if ( product.hasAvailableStock()) {
                        $(labelPrice).css({background: '#fff'});
                        $(labelPrice).css({'margin-top': '20px'});
                        }
                        else {
                        $(labelPrice).text('SORRY, OUT OF STOCK');
                        $(labelPrice).removeClass('entity-item-price-per');
                        $(labelPrice).addClass('entity-item-price-out-of-stock');
                        }
                        }
                        $(".i-status-flag").removeClass("i-hide");
                        }
                        );
                      </script>

                      <div class="entity-item-dimensions">
                        <xsl:value-of select="custom:displayProductSize(ItemCode, 'true')" disable-output-escaping="yes" />
                      </div>

                      <div class="productItemCode">
                        <strong>Product Code:</strong>
                        <br/>
                        <span>
                          <xsl:value-of select="ise:Encode(ItemName)" disable-output-escaping="yes" />
                        </span>
                      </div>

                      <div>

                        <xsl:choose>
                          <xsl:when test="translate(ItemType, $uppercase, $smallcase)='matrix group'">

                            <xsl:value-of select="ise:ProductMatrixControl(Counter, ItemCode, ItemType)" disable-output-escaping="yes" />

                            <xsl:value-of select="ise:SetXmlPackageHelperTemplate(1)" disable-output-escaping="yes"/>

                            <xsl:value-of select="ise:DisplayAddToCartForm(Counter, ItemCode, ItemType, 'h')" disable-output-escaping="yes" />
                            <script>
                              $(document).ready(function(){

                              <!--  $(".matrix-selector").each(function(){
                              $p = $(this).parent("div").children("div");
                              $p.addClass("i-hide");
                            }); 
                  
                            $(".unit-measure-container").addClass("i-hide");-->

                              });
                            </script>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="ise:SetXmlPackageHelperTemplate(1)" disable-output-escaping="yes"/>

                            <xsl:value-of select="ise:DisplayAddToCartForm(Counter, ItemCode, ItemType, 'h')" disable-output-escaping="yes" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </div>

                    </xsl:otherwise>
                  </xsl:choose>

                </div>

              </div>
            </div>
          </div>

        </li>
      </xsl:template>
      <xsl:template name="FillerCells">
        <xsl:param name="cellCount" />
        <xsl:param name="CellWidth" select="100 div $SubcatGridCols" />
        <li class="productItem" style="height: auto;"/>
        <xsl:if test="$cellCount > 1">
          <xsl:call-template name="FillerCells">
            <xsl:with-param name="cellCount" select="$cellCount - 1" />
          </xsl:call-template>
        </xsl:if>
      </xsl:template>
    </xsl:stylesheet>
  </PackageTransform>
</package>



