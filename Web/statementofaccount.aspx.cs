﻿using InterpriseSuiteEcommerceCommon;
using System;
using System.Data;
using System.IO;
using System.Text;

namespace InterpriseSuiteEcommerce
{
    public partial class statementofaccount : SkinBase
    {
        string path = string.Empty;
        protected override void OnLoad(EventArgs e)
        {
            StringBuilder content = new StringBuilder();
            string type = string.Empty;
            var costCenter = ThisCustomer.CostCenter;
            if (costCenter == "Harvey Norman Cost Centre")
            {
                path = "images/StatementAccount/hn/";
            }
            else if (costCenter == "Smartbag-PREMIUM CUE")
            {
                path = "images/StatementAccount/cue/";
            }
            else if (costCenter == "Smartbag-PREMIUM APG")
            {
                path = "images/StatementAccount/apg/";
            }
            else if (costCenter == "Smartbag-PREMIUM DION LEE")
            {
                path = "images/StatementAccount/dl/";
            }
            else if (costCenter == "AMEX")
            {
                path = "images/StatementAccount/amex/";
            }
            else if (costCenter == "Smart Colour Print")
            {
                path = "images/StatementAccount/print/";
            }
            else
            {
                ltContent.Text = "<div style=\"font-size: 15px;\">No Data Found</div>";
                return;
            }

            //Get list of files in folder
            string[] fileEntries = Directory.GetFiles(CommonLogic.SafeMapPath(path));
            if (fileEntries.Length <= 0)
            {
                ltContent.Text = "<div style=\"font-size: 15px;\">No Data Found</div>";
                return;
            }
            string icon = string.Empty;
            var dv = CreateTable(fileEntries);
            dv.Sort = "DateCreated DESC, Filename ASC";
            content.AppendLine("<div class=\"responsive-grid-header\">");
            content.AppendLine("<div class=\"responsive-col\"></div>");
            content.AppendFormat("<div class=\"responsive-col-2\">{0}</div>", AppLogic.GetString("portal.aspx.35"));
            content.AppendLine("");
            content.AppendFormat("<div class=\"responsive-col-1\">{0}</div>", AppLogic.GetString("portal.aspx.37"));
            content.AppendLine("");
            content.AppendFormat("<div class=\"responsive-col-1\">{0}</div>", AppLogic.GetString("portal.aspx.36"));
            content.AppendLine("");
            content.Append("</div>");
            foreach(DataRowView row in dv)
            {
                type = row["Type"].ToString().Replace(".", "");
                if (type == "xls" || type == "xlsx")
                {
                    icon = "excel excel";
                }
                else if (type == "pdf")
                {
                    icon = "pdf pdf";
                }
                else
                {
                    icon = "alt others";
                }
                content.AppendFormat("<div class=\"responsive-grid-content\"><a href=\"{0}\" target=\"_blank\">", row["URL"]);
                content.AppendLine("");
                content.AppendFormat("<div class=\"responsive-col\"><i class=\"fas fa-file-{0}\"></i></div>", icon);
                content.AppendLine("");
                content.AppendFormat("<div class=\"responsive-col-2\">{0}</div>", row["Filename"]);
                content.AppendLine("");
                content.AppendFormat("<div class=\"responsive-col-1\">{0}</div>", type);
                content.AppendLine("");
                content.AppendFormat("<div class=\"responsive-col-1\">{0}</div>", Convert.ToDateTime(row["DateCreated"]).Date.ToShortDateString());
                content.Append("</a></div>");
            }
            
            ltContent.Text = content.ToString();
        }

        private DataView CreateTable (string[] data)
        {
            string extension, filename, fileurl = string.Empty;
            DateTime filecreated;
            DataTable dt = new DataTable("Statement");
            // Add two columns
            dt.Columns.Add("Filename", typeof(string));
            dt.Columns.Add("DateCreated", typeof(DateTime));
            dt.Columns.Add("Type", typeof(string));
            dt.Columns.Add("URL", typeof(string));
            foreach (string fileinfo in data)
            {
                //Get file extension name
                fileurl = path + Path.GetFileName(fileinfo);
                extension = Path.GetExtension(fileinfo);
                filename = Path.GetFileNameWithoutExtension(fileinfo);
                filecreated = File.GetCreationTime(fileinfo).Date;
                dt.Rows.Add(filename, filecreated, extension, fileurl);
            }
            return dt.DefaultView;
        }
    }
}