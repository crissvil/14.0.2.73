﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="producthistory.aspx.cs" Inherits="producthistory" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report</title>
    <link href="mvwres:1-DevExpress.Web.ASPxEditors.Css.Default.css" rel="stylesheet" type="text/css" />
    <link href="mvwres:1-DevExpress.Web.ASPxEditors.Css.Default.css" rel="stylesheet" type="text/css" />
    <link href="mvwres:1-DevExpress.Web.ASPxEditors.Css.Default.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.min.css" />
    <script src="//code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="//use.fontawesome.com/55068458dc.js"></script>
    <script>
        $(function () {
            $("#txtInvoiceDateFrom").datepicker();
            $("#txtInvoiceDateTo").datepicker();
        });
    </script>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" />
    <style>
        body {
            font-family: Roboto;
            font-weight: 400;
            font-size: 14px;
        }

        #modalpopup, #messagebox, #confirmationbox {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 999; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        #modalpopup-content {
            background-color: #f2f2f2;
            border-radius: 10px;
            box-shadow: 0 1px 5px #000;
            padding: 30px 30px;
            text-align: center;
            vertical-align: middle;
            min-height: 125px;
            min-width: 200px;
            font-size: 15px;
            display: inline-table;
        }

            #modalpopup-content i {
                font-size: 40px;
            }

        #modalpopup-text {
            padding-top: 5px;
        }

        #messagebox-content, #confirmationbox-content {
            min-height: 125px;
            width: 300px;
            border: 1px solid #666;
            background: #fff;
        }

        #messagebox-text, #confirmationbox-text {
            padding: 30px 20px;
            font-size: 14px;
        }

        #messagebox-footer {
            border-top: 1px solid #cdcbc9;
            padding: 15px 30px;
            background: #eff0f1;
        }

        .filter-date {
            background-color: #f0f0f0;
            border: 1px solid #a8a8a8;
            box-sizing: border-box;
            padding: 3px 3px 0 10px;
            height: 32px;
            border-top: none;
        }

        .hide {
            display: none;
        }

        .dxmtb {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="frmUserList" runat="server">
        <div style="text-align: center;">
            <div>
                <dxwc:ReportToolbar ID="ReportToolbar1" runat="server" ReportViewer="<%# ViewerReport %>"
                    ShowDefaultButtons="False">
                    <Items>
                        <dxwc:ReportToolbarButton ImageUrl="~/images/icon.png" Name="Home" ToolTip="Back to Homepage" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton ItemKind="Search" ToolTip="Display the search window" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton ItemKind="PrintReport" ToolTip="Print the report" />
                        <dxwc:ReportToolbarButton ItemKind="PrintPage" ToolTip="Print the current page" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton Enabled="False" ItemKind="FirstPage" ToolTip="First Page" />
                        <dxwc:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" ToolTip="Previous Page" />
                        <dxwc:ReportToolbarLabel Text="Page" />
                        <dxwc:ReportToolbarComboBox ItemKind="PageNumber" Width="50px">
                        </dxwc:ReportToolbarComboBox>
                        <dxwc:ReportToolbarLabel Text="of" />
                        <dxwc:ReportToolbarTextBox ItemKind="PageCount" Width="35px" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton ItemKind="NextPage" ToolTip="Next Page" />
                        <dxwc:ReportToolbarButton ItemKind="LastPage" ToolTip="Last Page" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton ItemKind="SaveToDisk" ToolTip="Export a report and save it to the disk" />
                        <dxwc:ReportToolbarButton ItemKind="SaveToWindow" ToolTip="Export a report and show it in a new window" />
                        <dxwc:ReportToolbarComboBox ItemKind="SaveFormat" Width="70px">
                            <Elements>
                                <dxwc:ListElement Text="Pdf" Value="pdf" />
                                <%--<dxwc:ListElement Text="Xls" Value="xls" />--%>
                                <dxwc:ListElement Text="Xls" Value="csv" />
                                <dxwc:ListElement Text="Rtf" Value="rtf" />
                                <dxwc:ListElement Text="Mht" Value="mht" />
                                <dxwc:ListElement Text="Text" Value="txt" />
                                <dxwc:ListElement Text="Image" Value="png" />
                            </Elements>
                        </dxwc:ReportToolbarComboBox>
                    </Items>
                    <ClientSideEvents ItemClick="function(s, e) {
	                        if(e.item.name =='Home' ) 
                                window.location.href = 'portal.aspx';
                                                                }" />
                    <Styles>
                        <LabelStyle>
                            <Margins MarginLeft="3px" MarginRight="3px" />
                        </LabelStyle>
                        <ComboBoxStyle>
                            <Margins MarginRight="5px" />
                        </ComboBoxStyle>
                    </Styles>
                </dxwc:ReportToolbar>
            </div>
            <div class="filter-date">
                <div style="float: left; position: relative;" id="divItemName">
                    <span style="padding-right: 8px; font-size: 13px;">Item Name</span><input type="text" id="txtItemName" name="txtItemName" style="height: 15px; margin-top: 2px;" />
                </div>
                <div style="float: left; position: relative; padding-left: 3px;" id="divStoreName">
                    <span style="padding: 0 8px; font-size: 13px;">Store Name</span><input type="text" id="txtStoreName" name="txtStoreName" style="height: 15px; margin-top: 2px;" />
                </div>
                <div style="float: left; position: relative; padding-left: 3px;" id="divMovex">
                    <span style="padding: 0 8px; font-size: 13px;"><asp:Literal ID="ltMovex" runat="server" Text="Movex"></asp:Literal></span>
                    <input type="text" id="txtMovex" name="txtMovex" style="height: 15px; margin-top: 2px;" />
                </div>
                <div style="float: left; position: relative; padding-left: 3px; padding-top: 4px;" id="divInvoiceDateLabel">
                    <span style="padding: 0 0 0 8px; font-size: 13px;">Invoice Date:</span>
                </div>
                <div style="float: left; position: relative; padding-left: 3px;" id="divInvoiceDateFrom">
                    <i class="fa fa-calendar" aria-hidden="true" style="right: 0; position: absolute; pointer-events: none; padding: 5px;"></i>
                    <span style="padding: 0 8px 0 3px; font-size: 13px;">From</span><input type="text" id="txtInvoiceDateFrom" name="txtInvoiceDateFrom" style="height: 15px; margin-top: 2px;" />
                </div>
                <div style="float: left; position: relative; padding-left: 3px;" id="divInvoiceDateTo">
                    <i class="fa fa-calendar" aria-hidden="true" style="right: 0; position: absolute; pointer-events: none; padding: 5px;"></i>
                    <span style="padding: 0 8px; font-size: 13px;">To</span><input type="text" id="txtInvoiceDateTo" name="txtInvoiceDateTo" style="height: 15px; margin-top: 2px;" />
                </div>
                <div style="float: left; padding: 0 0 0 7px;">
                    <button type="submit" name="btnGo" id="btnGo" class="placeorder-primary-button site-button" onclick="return setValue();" style="height: 24px;">Go</button>
                </div>
            </div>
            <dxwc:ReportViewer ID="ViewerReport" runat="server" OnCacheReportDocument="ViewerReport_CacheReportDocument" OnRestoreReportDocumentFromCache="ViewerReport_RestoreReportDocumentFromCache">
            </dxwc:ReportViewer>
        </div>
        <div style="padding-left: 10px; font-size: 18px;">
            <div id="modalpopup">
                <div id="modalpopup-content">
                    <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                    <div id="modalpopup-text">Loading...</div>
                </div>
            </div>

            <!-- Global MessageBox -->
            <div id="messagebox">
                <div id="messagebox-content">
                    <div id="messagebox-text">Loading...</div>
                    <div id="messagebox-footer">
                        <div style="text-align: right;">
                            <button type="button" name="btnClose" id="btnClose" class="messagebox-ok-button" onclick="CloseMessageBoxPopup();">OK</button>
                        </div>
                    </div>
                </div>
            </div>
            <asp:Literal ID="ltReport" runat="server" Visible="false" />
        </div>
    </form>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtItemName").val(localStorage.getItem("txtItemName"));
            $("#txtStoreName").val(localStorage.getItem("txtStoreName"));
            $("#txtMovex").val(localStorage.getItem("txtMovex"));
            $("#txtInvoiceDateFrom").val(localStorage.getItem("txtInvoiceDateFrom"));
            $("#txtInvoiceDateTo").val(localStorage.getItem("txtInvoiceDateTo"));
            localStorage.removeItem("txtItemName");
            localStorage.removeItem("txtStoreName");
            localStorage.removeItem("txtMovex");
            localStorage.removeItem("txtInvoiceDateFrom");
            localStorage.removeItem("txtInvoiceDateTo");
        });

        function ShowModalPopup(message) {
            var height = $(window).height();
            var width = $(window).width();
            var popup = $("#modalpopup-content");
            var posTop = (height / 2 - popup.height() / 2) - 70;
            var posLeft = (width / 2 - popup.width() / 2) - 150;

            $("#modalpopup-content").css({
                "margin-top": posTop,
                "margin-left": posLeft
            });

            $("#modalpopup-text").html(message);
            $("#modalpopup").css({ "display": "block" });
        }

        function ShowMessageBoxPopup(message) {
            var height = $(window).height();
            var width = $(window).width();
            var popup = $("#messagebox-content");
            var posTop = (height / 2 - popup.height() / 2) - 70;
            var posLeft = (width / 2 - popup.width() / 2);

            $("#messagebox-content").css({
                "margin-top": posTop,
                "margin-left": posLeft
            });

            $("#messagebox-text").html(message);
            $("#messagebox").css({ "display": "block" });
            $("#btnClose").focus();
        }

        function CloseMessageBoxPopup() {
            $("#messagebox").css({ "display": "none" });
        }

        function setValue() {
            localStorage.setItem("txtItemName", $("#txtItemName").val());
            localStorage.setItem("txtStoreName", $("#txtStoreName").val());
            localStorage.setItem("txtMovex", $("#txtMovex").val());
            localStorage.setItem("txtInvoiceDateFrom", $("#txtInvoiceDateFrom").val());
            localStorage.setItem("txtInvoiceDateTo", $("#txtInvoiceDateTo").val());
            if (($("#txtInvoiceDateTo").val() != "" && $("#txtInvoiceDateFrom").val() == "") || ($("#txtInvoiceDateTo").val() == "" && $("#txtInvoiceDateFrom").val() != "")) {
                ShowMessageBoxPopup("Please enter Invoice Date range");
                return false;
            }
            ShowModalPopup("Processing...");
            return true;
        }
    </script>
</body>
</html>
