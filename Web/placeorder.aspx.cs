﻿using System;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Web.UI;

namespace InterpriseSuiteEcommerce
{
    public partial class placeorder : SkinBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RequiresLogin(CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables("QUERY_STRING"));
            if (AppLogic.AppConfigBool("GoNonSecureAgain"))
            {
                SkinBase.GoNonSecureAgain();
            }

            this.SETitle = "Welcome to the smartportal";
            
            // set the Customer context, and set the SkinBase context, so meta tags to be set if they are not blank in the XmlPackage results
            Package1.SetContext = this;
        }
        
        protected override void RegisterScriptsAndServices(System.Web.UI.ScriptManager manager)
        {
            manager.CompositeScript.Scripts.Add(new ScriptReference(string.Format("skins/skin_{0}/minified/product_ajax.js", ThisCustomer.SkinID)));
            manager.Scripts.Add(new ScriptReference(string.Format("skins/skin_{0}/minified/imagezoom.min.js", ThisCustomer.SkinID)));
            manager.Services.Add(new ServiceReference("~/actionservice.asmx"));
        }
    }
}