<%@ Page Language="c#" Inherits="InterpriseSuiteEcommerce.editaddress" CodeFile="editaddress.aspx.cs" %>

<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="cc1" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="uc" TagName="AddressControl" Src="~/UserControls/AddressControl.ascx" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>
<%@ Import Namespace="InterpriseSuiteEcommerceCommon" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="frmEditAddress" runat="server">
        <asp:Literal ID="litswitchformat" runat="server" Mode="PassThrough"></asp:Literal>
        <asp:Panel ID="pnlCheckoutImage" runat="server" HorizontalAlign="Center" Visible="false">
            <asp:ImageMap ID="CheckoutImage" HotSpotMode="Navigate" runat="server">
                <asp:RectangleHotSpot AlternateText="Back To Step 1: Shopping Cart" Top="0" Left="0" Right="87" Bottom="54" HotSpotMode="Navigate" NavigateUrl="~/shoppingcart.aspx?resetlinkback=1" />
            </asp:ImageMap>
            <br />
            <br />
        </asp:Panel>
        <cc1:InputValidatorSummary ID="errorSummary" CssClass="error float-left normal-font-style" runat="server" Register="false" Style="width: 660px;"></cc1:InputValidatorSummary>
        <div class="clear-both"></div>
        <div class="sections-place-holder no-padding">
            <h1>
                <asp:Literal ID="litEditAddress" runat="server">(!editaddress.aspx.14!)</asp:Literal></h1>
            <div class="editaddress-content-section">
                <div style="background: #f2f2f2; border-radius: 5px; padding: 14px 14px 16px; margin-bottom: 50px;">
                    <asp:Panel ID="pnlAddress" runat="server" Visible="true">
                        <div class="form-controls-place-holder">
                            <span class="form-controls-span form-control-companyname">
                                <label id="lblContactName" class="form-field-label">
                                    <asp:Literal ID="litContactName" runat="server">(!editaddress.aspx.11!)</asp:Literal>
                                </label>
                                <br />
                                <asp:TextBox ID="txtContactName" MaxLength="100" class="light-style-input" runat="server"></asp:TextBox>
                            </span>
                        </div>
                        <div class="form-controls-place-holder">
                            <span class="form-controls-span" style="display: none;">
                                <label id="lblContactNumber" class="form-field-label">
                                    <asp:Literal ID="litContactNumber" runat="server">(!editaddress.aspx.12!)</asp:Literal>
                                </label>
                                <br />
                                <asp:TextBox ID="txtContactNumber" runat="server" MaxLength="50" class="light-style-input"></asp:TextBox>
                            </span>
                        </div>
                        <div class="clear-both height-12 profile-section-clears"></div>
                        <uc:AddressControl ID="AddressControl" runat="server" />
                        <div style="padding-top: 20px;">
                            <div id="return-address-button">
                                <div id="return-address-button-place-holder">
                                    <div id="update-address-loader" style="padding-bottom: 10px; padding-right: 5px;"></div>
                                    <div id="Panel1" class="form-editaddress-return-button">
                                        <asp:Button ID="btnReturn" runat="server" CssClass="shoppingcart-secondary-button site-button" />
                                        <asp:Button ID="btnCheckOut" runat="server" Visible="false" CssClass="shoppingcart-secondary-button site-button" />
                                    </div>
                                    <div id="pnlUpdasteAddress" class="form-editaddress-save-button">
                                        <div id="update-address-button">
                                            <div id="update-address-button-place-holder">
                                                <input type="button" id="save-address" class="shoppingcart-primary-button site-button"
                                                    data-contentkey="editaddress.aspx.3"
                                                    data-contentvalue="<%=AppLogic.GetString("editaddress.aspx.3", true)%>"
                                                    data-contenttype="string resource"
                                                    value="<%=AppLogic.GetString("editaddress.aspx.3", true)%>" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="clear-both height-12"></div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>

        <!-- do not remove or modify / start here -->
        <div class="display-none">
            <input type="text" id="hidSkinID" runat="server" />
            <input type="text" id="hidLocale" runat="server" />
            <input type="text" id="txtPhone" runat="server" />
            <input type="text" id="load-at-page" value="edit-address" />
            <asp:Button ID="btnUpdateAddress" runat="server" OnClick="btnUpdateAddress_Click" />
            <asp:TextBox ID="txtCityStates" runat="server"></asp:TextBox>
        </div>
        <div style="display: none; margin: auto" title="Address Verification" id="ise-address-verification"></div>
        <!-- do not remove or modify / ends here -->
        <script type="text/javascript" src="jscripts/jquery/address.control.js"></script>
        <script type="text/javascript" src="jscripts/jquery/address.verification.js"></script>
        <script type="text/javascript" src="jscripts/jquery/customer.js"></script>
        <script type="text/javascript">
    <!-- reference path : /component/address-verificatio/real-time-address-verification-plugin -->
    $(window).load(function () {
        var basePlugin = new jqueryBasePlugin();
        basePlugin.downloadPlugin('components/address-verification/setup.js', function () {

            var loader = new realtimeAddressVerificationPluginLoader();
            loader.start(function (config) {

                var $plugin = $.fn.RealTimeAddressVerification;

                config.submitButtonID = "btnUpdateAddress";
                config.addressMatchDialogContainerID = "ise-address-verification";
                config.errorContainerId = "errorSummary";
                config.progressContainterId = "update-address-loader";
                config.buttonContainerId = "update-address-button-place-holder";
                config.isWithShippingAddress = false;
                config.isAllowShipping = false;
                config.billingInputID = {
                    POSTAL_CODE: "AddressControl_txtPostal",
                    CITY: "AddressControl_txtCity",
                    STATE: "AddressControl_txtState",
                    COUNTRY: "AddressControl_drpCountry",
                    STREET_ADDRESS: "AddressControl_txtStreet",
                    CITY_STATE_SELECTOR: "city-states"
                };

                config.billingLabelID = {
                    POSTAL_CODE: "AddressControl_lblStreet",
                    CITY: "AddressControl_lblCity",
                    STATE: "AddressControl_lblState",
                    STREET_ADDRESS: "AddressControl_lblPostal"
                };

                config.shippingInputID = {
                    POSTAL_CODE: "",
                    CITY: "",
                    STATE: "",
                    COUNTRY: "",
                    STREET_ADDRESS: "",
                    RESIDENCE_TYPE: "AddressControl_drpType",
                    CITY_STATE_SELECTOR: ""
                };

                var realTimeAddressVerificationPluginStringKeys = new Object();

                realTimeAddressVerificationPluginStringKeys.unableToVerifyAddress = "editaddress.aspx.18";
                realTimeAddressVerificationPluginStringKeys.confirmCorrectAddress = "editaddress.aspx.19";
                realTimeAddressVerificationPluginStringKeys.useBillingAddressProvided = "editaddress.aspx.20";
                realTimeAddressVerificationPluginStringKeys.selectMatchingBillingAddress = "editaddress.aspx.21";
                realTimeAddressVerificationPluginStringKeys.gatewayErrorText = "editaddress.aspx.22";
                realTimeAddressVerificationPluginStringKeys.progressText = "editaddress.aspx.23";

                config.stringResourceKeys = realTimeAddressVerificationPluginStringKeys;

                $plugin.setup(config);
            });
        });
    });
        </script>
    </form>
</body>
</html>
