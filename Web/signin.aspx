<%@ Page Language="c#" Inherits="InterpriseSuiteEcommerce.signin" CodeFile="signin.aspx.cs" %>

<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>
<html>
<head>
</head>
<body>
    <form runat="Server" method="POST" id="SigninForm" name="SigninForm">
        <div>
            <div align="center" style="text-align: left; color: #000;">
                <asp:Panel ID="FormPanel" runat="server" Width="100%">
                    <asp:Panel ID="CheckoutPanel" runat="server">
                        <div id="CheckoutSequence" align="center">
                            <asp:ImageMap ID="CheckoutMap" runat="server" ImageUrl="(!skins/skin_(!SKINID!)/images/step_2.gif!)">
                                <asp:RectangleHotSpot Bottom="54" HotSpotMode="Navigate"
                                    NavigateUrl="shoppingcart.aspx?resetlinkback=1&amp;checkout=true" Right="87" />
                            </asp:ImageMap>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="ErrorPanel" runat="server" Visible="False" HorizontalAlign="Left">
                        <asp:Label CssClass="errorLg" ID="ErrorMsgLabel" runat="server"></asp:Label>
                        <asp:LinkButton ID="lnkContactUs" runat="server" PostBackUrl="contact-us.aspx" Visible="false" />
                    </asp:Panel>
                    <ise:Topic runat="server" ID="HeaderMsg" TopicName="SigninPageHeader" />

                    <div class="clr height-12"></div>
                    <h1 class="signinheadertext">Sign In</h1>
                    <div class="clr height-10"></div>
                    <div class="signin-left-section">
                        <div style="background: #f2f2f2; border-radius: 5px; padding: 20px 18px;">
                            <span class="signinheaderlabel">Returning Customer</span>
                            <div style="font-size: 15px; padding: 15px 0px 8px; line-height: 21px; color: #7c7a78;">
                                If you have ordered before or have been provided with login details; please sign in below:
                                <p style="padding-top:13px;" />
                                Enter your e-mail address & password:
                            </div>
                            <%--<div style="font-size: 15px; padding-bottom: 5px;">My email address:</div>--%>
                            <asp:TextBox ID="EMail" runat="server" ValidationGroup="Group1" Columns="30" MaxLength="100" CausesValidation="True" placeholder="Email Address"
                                AutoCompleteType="Email" CssClass="signintextbox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="Group1"
                                ErrorMessage="(!signin.aspx.3!)" ControlToValidate="EMail"></asp:RequiredFieldValidator>
                            <%--<div style="font-size: 15px; padding-bottom: 5px;">Password:</div>--%>
                            <asp:TextBox ID="Password" runat="server" ValidationGroup="Group1" Columns="30" MaxLength="50" placeholder="Password"
                                CausesValidation="True" TextMode="Password" AutoCompleteType="Disabled" autocomplete="off" CssClass="signintextbox"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Group1"
                                ErrorMessage="(!signin.aspx.4!)" ControlToValidate="Password"></asp:RequiredFieldValidator>
                            <div style="padding-bottom: 10px; color: #7c7a78;">
                                <label class="checkbox-container">
                                <span class="checkbox-captions custom-font-style" id="i-copy-billing-text">
                                    <span style="vertical-align: bottom; font-size: 13px;">Remember Password?</span>
                                </span>
                                <asp:CheckBox ID="PersistLogin" runat="server"></asp:CheckBox>
                                <span class="checkmark"></span>
                            </label>
                            </div>
                            <div style="text-align: right;">
                                <asp:Button ID="LoginButton" runat="server" CssClass="signin-button content" ValidationGroup="Group1" />
                            </div>
                            <asp:Panel runat="Server" ID="pnlSecurityCode" Visible="false">
                                <div style="padding: 0 15px 0 5px; text-align: right;">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="SecurityCode"
                                        ErrorMessage="(!signin.aspx.17!)" ValidationGroup="Group1" Enabled="False" Style="font-size: 15px;"></asp:RequiredFieldValidator>
                                </div>
                                <div style="padding: 10px 0; text-align: right;">
                                    <asp:Label ID="Label1" runat="server" Text="(!signin.aspx.18!)" Style="font-size: 15px;"></asp:Label>
                                    <asp:TextBox ID="SecurityCode" runat="server" ValidationGroup="Group1"
                                        CausesValidation="True" Width="73px" EnableViewState="False" CssClass="signintextbox"></asp:TextBox>
                                </div>
                                <div style="text-align: right;">
                                    <asp:Image ID="SecurityImage" runat="server"></asp:Image>
                                </div>
                            </asp:Panel>
                        </div>

                        <div style="background: #f2f2f2; border-radius: 5px; padding: 26px 16px 16px; margin-top: 30px;">
                            <div style="font-size: 18px; font-weight: 500; color: #6c6a6b;">Forgotten your password?</div>
                            <div style="font-size: 15px; color: #7c7a78; padding: 9px 0 15px; line-height: 21px;">
                                To receive your password via e-mail,<br />
                                please enter your email address below:
                            </div>
                            <%--<div style="font-size: 15px; padding-bottom: 5px;">Email address:</div>--%>
                            <div>
                                <asp:TextBox ID="ForgotEMail" runat="server" ValidationGroup="Group2" CausesValidation="True" AutoCompleteType="Email" Columns="30" CssClass="signintextbox" placeholder="Email Address"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ForgotEMail" ErrorMessage="(!signin.aspx.3!)" ValidationGroup="Group2"></asp:RequiredFieldValidator>
                            </div>
                            <div style="text-align: right;">
                                <asp:Button ID="RequestPassword" runat="server" CssClass="signin-button signin-button-secondary content" ValidationGroup="Group2"></asp:Button>
                            </div>
                        </div>
                    </div>

                    <div class="signin-right-section checkoutanon-not-registered">
                        <div style="background: #f2f2f2; border-radius: 5px; padding: 21px 20px 16px; height: 388px;">
                            <span class="signinheaderlabel">Not Registered Yet?</span>
                            <div style="font-size: 15px; color: #7c7a78; padding: 15px 0 18px; line-height: 19px;">
                                Create an account and take advantage of faster checkouts and other great benefits.
                            </div>
                            <div class="signinperks">
                                <ul>
                                    <li>
                                        <i class="fa fa-check"></i><span>Save your re-order and purchase history</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i><span>Faster checkout process</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i><span>VIP exclusive offers</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i><span>Check the status of your order</span>
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i><span>Store multiple shipping addresses</span>
                                    </li>
                                </ul>
                                <div style="text-align: center;padding-top:39px;">
                                    <asp:HyperLink ID="SignUpLink" runat="server" CssClass="signin-button">CREATE ACCOUNT</asp:HyperLink>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--Log In Form Section Ends Here -->
                    <div class="clr height-12"></div>
                    <div class="clr height-12"></div>
                </asp:Panel>
            </div>
            <asp:Panel ID="ExecutePanel" runat="server" Width="90%">
                <div align="center">
                    <img src="images/spacer.gif" alt="" width="100%" height="40" />
                    <b>
                        <asp:Literal ID="SignInExecuteLabel" runat="server"></asp:Literal></b>
                </div>
            </asp:Panel>
            <asp:CheckBox ID="DoingCheckout" runat="server" Visible="False" />
            <asp:Label ID="ReturnURL" runat="server" Text="default.aspx" Visible="False" />
        </div>
    </form>
</body>
</html>
