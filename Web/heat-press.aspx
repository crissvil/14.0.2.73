<%@ Page Language="C#" AutoEventWireup="true" CodeFile="heat-press.aspx.cs" Inherits="InterpriseSuiteEcommerce.heat_press" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>About Us</title>
</head>
<body>
	<div class="clear-both height-5">&nbsp;</div>
	<form id="frmHeatPress" runat="server" action=>
	<asp:Panel ID="pnlPageContentWrapper" runat="server">
		<h1>Heat Press</h1>
		<ul id="c-print">
			<li>
				<h3>HEAT TRANSFER (BACKGROUND SCREEN PRINT)</h3>
				<center>
				<img class="i-fancy-image" data-src="i-fancy-image-hidden-1" src="images/heat-press/sample-1.png" style="width:90%"/>
				<div class="i-hide" id="i-fancy-image-hidden-1">
					<img  src="images/heat-press/poplet-1.jpg" style="width:100%"/>
				</div>
				<br/>
				<a href="javascript:void(1);" class="i-fancy-image" data-src="i-fancy-image-hidden-1" >Click image to enlarge</a>
				</center>
				<br/>
				<p style="font-size:16px">Can achieve a photo quality result for smaller areas and may be combined with screen print.</p>
				
			</li>
			<li>
				<h3>HEAT TRANSFER (LARGE COVERAGE)</h3>
				<center>
				<img class="i-fancy-image" data-src="i-fancy-image-hidden-2"  src="images/heat-press/sample-2.png" style="width:90%"/>
				<div class="i-hide" id="i-fancy-image-hidden-2" >
					<img src="images/heat-press/poplet-2.jpg" style="width:100%"/>
				</div>
				<br/>
				<a href="javascript:void(1);" class="i-fancy-image" data-src="i-fancy-image-hidden-2" >Click image to enlarge</a>
				</center>
				<br/>
				<p style="font-size:16px">Maximum size heat transfer for photo quality print not covering the whole bag. Entire bag coverage must use lamination.</p>
				
			</li>
		</ul>
		<p class="i-text-clear-5"></p>   
	</asp:Panel>
	</form>
</body>
</html>