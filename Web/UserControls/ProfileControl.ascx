﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ProfileControl.ascx.cs" Inherits="UserControls_ProfileControl" %>
<div id="profile-section-wrapper">
    <% if (!IsHideAccountNameArea && !IsExcludeAccountName)
       { %>
    <div id="account-name-wrapper" class="form-controls-place-holder">
        <span class="form-controls-span form-control-companyname">
            <label id="lblAccountName" class="form-field-label">
                <asp:Literal ID="litCompanyName" runat="server"></asp:Literal>
            </label>
            <br />
            <label id="lblInternalAccountName" class="internal-form-field-label">
                <asp:Literal ID="litInternalCompanyName" runat="server"></asp:Literal>
            </label>
            <asp:TextBox ID="txtAccountName" runat="server" class="light-style-input" MaxLength="100"></asp:TextBox>
        </span>
    </div>
    <% }  %>

    <% if (!IsUseFullnameTextbox)
       { %>
    <div class="form-controls-place-holder ">
        <div class="clear-both height-12"></div>
        <% if (IsShowSalutation)
           { %>
        <span class="form-controls-span form-control-salutation">
            <label class="form-field-label">
                Salutation
            </label>
            <br />
            <span class="form-controls-span" id="salutation-place-holder" style="width: 100%">
                <asp:DropDownList ID="drpLstSalutation" class="light-style-input" runat="server"></asp:DropDownList>
            </span>
        </span>
        <% } %>

        <span class="form-controls-span form-controls-span-spaceleft form-control-fname">
            <label id="lblFirstName" class="form-field-label">
                <asp:Literal ID="litFirstName" runat="server"></asp:Literal>
            </label>
            <br />
            <asp:TextBox ID="txtFirstName" CssClass="light-style-input" runat="server" MaxLength="50"></asp:TextBox>
        </span>

        <span class="form-controls-span form-controls-span-spaceleft form-control-lname">
            <label id="lblLastName" class="form-field-label">
                <asp:Literal ID="litLastName" runat="server"></asp:Literal>
            </label>
            <br />
            <asp:TextBox ID="txtLastName" CssClass="light-style-input" runat="server" MaxLength="50"></asp:TextBox>
        </span>
    </div>
    <div class="clear-both height-12"></div>

    <div class="form-controls-place-holder">
        <span class="form-controls-span form-control-contactnumber">
            <label id="lblContactNumber" class="form-field-label">
                <asp:Literal ID="litContactNumber" runat="server"></asp:Literal>
            </label>
            <br />
            <asp:TextBox ID="txtContactNumber" CssClass="light-style-input" MaxLength="50" runat="server"></asp:TextBox>
        </span>

        <span class="form-controls-span form-controls-span-spaceleft form-control-mobile" data-accounttype="<%=AccountType%>">
            <label id="lblMobile" class="form-field-label">
                <asp:Literal ID="litMobile" runat="server"></asp:Literal>
            </label>
            <br />
            <asp:TextBox ID="txtMobile" CssClass="light-style-input" MaxLength="50" runat="server"></asp:TextBox>
        </span>
    </div>

    <% if (IsAnonymousCheckout)
       { %>
    <div class="form-controls-place-holder">
        <label id="lblAnonymousEmail" class="form-field-label">
            <asp:Literal ID="litAnonymousEmail" runat="server" Visible="false"></asp:Literal>
        </label>
        <asp:TextBox ID="txtAnonymousEmail" CssClass="light-style-input" MaxLength="50" runat="server" Visible="false"></asp:TextBox>
    </div>
    <% } %>
    <% }
       else
       { %>

    <div class="form-controls-place-holder">
        <div class="clear-both height-12"></div>
        <span class="form-controls-span form-control-shippingname">
            <label id="lblShippingContactName" class="form-field-label">
                <asp:Literal ID="litShippingContactName" runat="server"></asp:Literal>
            </label>
            <br />
            <asp:TextBox ID="txtShippingContactName" CssClass="light-style-input" runat="server" MaxLength="100"></asp:TextBox>
        </span>

        <span class="form-controls-span form-controls-span-spaceleft form-control-shippingtel">
            <label id="lblShippingContactNumber" class="form-field-label">
                <asp:Literal ID="litShippingContactNumber" runat="server"></asp:Literal>
            </label>
            <br />
            <asp:TextBox ID="txtShippingContactNumber" MaxLength="50" CssClass="light-style-input" runat="server"></asp:TextBox>
        </span>
    </div>
    <div class="form-controls-place-holder">
        <div class="form-controls-span form-control-shippingemail" id="spanShippingSectionEmail" data-accounttype="<%=AccountType%>">
            <div class="clear-both height-12"></div>
            <label id="lblShippingEmail" class="form-field-label">
                <asp:Literal ID="litShippingEmail" runat="server"></asp:Literal>
            </label>
            <br />
            <asp:TextBox ID="txtShippingEmail" CssClass="light-style-input" runat="server" MaxLength="50"></asp:TextBox>
        </div>
    </div>
    <div class="clear-both height-5"></div>
    <% } %>

    <% if (!IsAnonymousCheckout)
       { %>
    <div class="clear-both height-12"></div>
    <div class="form-controls-place-holder">
        <%--<span class="form-controls-span" id="spanEmailAddress">
               <asp:Literal ID="litEmailCaption" runat="server"></asp:Literal>
         </span>--%>
        <span class="form-controls-span form-control-email" data-accounttype="<%=AccountType%>">
            <label id="lblEmail" class="form-field-label">
                <asp:Literal ID="litEmail" runat="server"></asp:Literal>
            </label>
            <br />
            <asp:TextBox ID="txtEmail" CssClass="light-style-input" runat="server" MaxLength="50"></asp:TextBox>
        </span>
    </div>
    <div class="clear-both height-5 accounts-clear"></div>
    <% }  %>

    <% if (IsShowEditPasswordArea && !IsExcludeAccountName)
       {%>

    <div class="clear-both height-12 accounts-clear"></div>

    <div class="form-controls-place-holder">
        <label class="checkbox-container">
            <span class="checkbox-captions custom-font-style">
                <asp:Literal ID="litEditPassword" runat="server"></asp:Literal>
            </span>
            <input type="checkbox" id="edit-password" />
            <span class="checkmark"></span>
        </label>
    </div>
    <div class="clear-both height-12 accounts-clear"></div>
    <div id="Div1" class="form-controls-place-holder">
        <span class="form-controls-span display-none form-control-oldpassword" id="old-password-label-place-holder">
            <asp:Literal ID="litOldPassword" runat="server"></asp:Literal>
        </span>
        <span class="form-controls-span form-control-oldpassword">
            <label id="old-password-input-label" class="form-field-label">
                <asp:Literal ID="litCurrentPassword" runat="server"></asp:Literal>
            </label>
            <br />
            <input id="old-password-input" maxlength="50" class="light-style-input " autocomplete="off" type="password" char="." />
        </span>
    </div>
    <div class="clear-both height-12 accounts-clear"></div>


    <% } %>

    <% if (!IsExcludePassword)
       { %>

    <div id="passwords-wrapper" class="form-controls-place-holder">
        <%--<% if (!IsHideAccountNameArea) { %>
             <span class="form-controls-span" id="password-caption">
               <asp:Literal ID="litAccountSecurity" runat="server"></asp:Literal>
            </span>
           <% } else {  %>
             <span class="form-controls-span" id="new-password-caption">
                <asp:Literal ID="litNewPassword" runat="server"></asp:Literal>
            </span>
           <% } %>--%>


        <span class="form-controls-span form-control-password">
            <label id="lblPassword" class="form-field-label">
                <asp:Literal ID="litPassword" runat="server"></asp:Literal>
            </label>
            <br />
            <asp:TextBox ID="txtPassword" MaxLength="50" class="light-style-input" TextMode="Password" runat="server" AutoCompleteType="Disabled" autocomplete="off"></asp:TextBox>
        </span>

        <span class="form-controls-span form-controls-span-spaceleft form-control-confirmpassword">
            <label id="lblConfirmPassword" class="form-field-label">
                <asp:Literal ID="litConfirmPassword" runat="server"></asp:Literal>
            </label>
            <br />
            <asp:TextBox ID="txtConfirmPassword" MaxLength="50" class="light-style-input" TextMode="Password" runat="server" AutoCompleteType="Disabled" autocomplete="off"></asp:TextBox>
        </span>
    </div>
    <div class="clear-both height-12"></div>
    <% } %>
</div>
<script>
    $(document).ready(function () {

        var salutation = $("#ProfileControl_drpLstSalutation").val();
        if (typeof (salutation) == "undefined") {
            $("#ProfileControl_txtFirstName").addClass("new-first-name-width");
            $("#ProfileControl_txtLastName").addClass("new-last-name-width");
        } else {
            $("#ProfileControl_txtFirstName").removeClass("new-first-name-width");
            $("#ProfileControl_txtLastName").removeClass("new-last-name-width");
        }

        if (trimString($("#ProfileControl_txtAccountName").val()) != "") {
            $("#lblInternalAccountName").addClass("display-none");
        }

        $("#ProfileControl_txtAccountName").focus(function () {
            $("#lblInternalAccountName").addClass("display-none");
        });

        $("#ProfileControl_txtAccountName").blur(function () {
            if ($(this).val() == "") {
                $("#lblInternalAccountName").removeClass("display-none");
            }
        });
    });
</script>
