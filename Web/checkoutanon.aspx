<%@ Page Language="c#" Inherits="InterpriseSuiteEcommerce.checkoutanon" CodeFile="checkoutanon.aspx.cs" %>

<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>
<%@ Import Namespace="InterpriseSuiteEcommerceCommon" %>
<html>
<head>
</head>
<body>
    <form runat="Server" id="CheckoutAnon">
        <div>
            <div align="center" style="text-align: left; color: #000; padding-bottom: 20px;">
                <asp:Panel ID="ErrorPanel" runat="server" Visible="False" HorizontalAlign="center">
                    <asp:Label ID="ErrorMsgLabel" CssClass="errorLg" runat="server"></asp:Label>
                    <asp:LinkButton ID="lnkContactUs" runat="server" PostBackUrl="contactus.aspx" Visible="false" />
                    <br />
                    <br />
                </asp:Panel>
                <asp:Panel ID="FormPanel" runat="server" Width="100%">
                    <h1 class="checkoutanon-headertext" style="width: 98%">Checkout</h1>
                    <div class="checkoutanon-left-section checkoutanon-not-registered">
                        <div style="background: #f2f2f2; border-radius: 5px;">
                            <div class="checkoutanon-header-container-secondary">
                                <span class="checkoutanon-header-label-secondary">Continue as guest</span>
                            </div>
                            <div style="padding: 17px 14px;">
                                <div style="font-size: 15px; font-weight: 400; color: #7c7a78;">Proceed directly to Checkout.</div>
                                <div style="padding-top: 12px;">
                                    <asp:Button ID="Skipregistration" CssClass="site-button checkoutanon-tertiary-button content" Text="(!checkoutanon.aspx.14!)" runat="server" CausesValidation="false" />
                                </div>
                            </div>
                        </div>
                        <asp:Panel runat="Server" ID="PasswordOptionalPanel" Visible="false" Style="background: #f2f2f2; border-radius: 5px; margin-top: 23px;">
                            <div class="checkoutanon-header-container">
                                <span class="checkoutanon-header-label">Welcome! Create an Account</span>
                            </div>
                            <div STYLE="padding: 25px 14px 19px; ">
                                <asp:Literal ID="litPromo" runat="server" />
                                <div style="font-size: 15px; font-weight: 400; color: #7c7a78; padding: 20px 0;">
                                    Creating an account allows you to save your orders, access your purchase history, track your orders, store multiple delivery addresses and much more!
                                </div>
                                <div>
                                    <asp:Button ID="RegisterAndCheckoutButton" CssClass="site-button checkoutanon-secondary-button content" Text="(!checkoutanon.aspx.13!)" runat="server" CausesValidation="false" />
                                </div>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="checkoutanon-right-section checkoutanon-not-registered">
                        <div style="background: #f2f2f2; border-radius: 5px;">
                            <div class="checkoutanon-header-container">
                                <span class="checkoutanon-header-label">I'm a returning customer</span>
                            </div>
                            <div style="padding: 0 20px 20px;">
                                <div style="font-size: 15px; font-weight: 400; color: #7c7a78; padding: 15px 0px 24px; line-height: 21px;">If you're already registered, please sign in using your email address and your password:</div>
                                <div style="font-size: 15px; padding-bottom: 5px; display: none;">My email address:</div>
                                <asp:TextBox ID="EMail" runat="server" ValidationGroup="Group1" MaxLength="100" CausesValidation="True"
                                    AutoCompleteType="Email" CssClass="signintextbox" placeholder="Email Address"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="EMail"
                                    runat="server" ValidationGroup="Group1" ErrorMessage="(!signin.aspx.3!)" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <div style="font-size: 15px; padding-bottom: 5px; display: none;">Password:</div>
                                <asp:TextBox ID="Password" runat="server" ValidationGroup="Group1" MaxLength="50"
                                    CausesValidation="True" TextMode="Password" AutoCompleteType="Disabled" autocomplete="off" CssClass="signintextbox" placeholder="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="Password"
                                    runat="server" ErrorMessage="(!signin.aspx.4!)" ValidationGroup="Group1" Font-Bold="True"
                                    SetFocusOnError="True"></asp:RequiredFieldValidator><br />
                                <div>
                                    <label class="checkbox-container">
                                        <span class="checkbox-captions custom-font-style" id="i-copy-billing-text">
                                            <span style="vertical-align: bottom; font-size: 15px;">Remember Password?</span>
                                        </span>
                                        <asp:CheckBox ID="PersistLogin" runat="server"></asp:CheckBox>
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div>
                                    <asp:Button ID="btnSignInAndCheckout" CssClass="site-button checkoutanon-primary-button content" Text="(!checkoutanon.aspx.12!)" runat="server" ValidationGroup="Group1" CausesValidation="true" />
                                </div>
                                <asp:Panel runat="Server" ID="pnlSecurityCode" Visible="false">
                                    <div style="padding: 20px 15px 0 5px; text-align: right;">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="SecurityCode"
                                            ErrorMessage="(!signin.aspx.17!)" ValidationGroup="Group1" Enabled="False"></asp:RequiredFieldValidator>
                                    </div>
                                    <div style="padding: 10px 0; text-align: right;">
                                        <asp:Label ID="Label10" runat="server" Text="(!signin.aspx.18!)" Visible="False" Style="font-size: 15px;"></asp:Label>
                                        <asp:TextBox ID="SecurityCode" runat="server" ValidationGroup="Group1"
                                            CausesValidation="True" Width="73px" EnableViewState="False"></asp:TextBox>
                                    </div>
                                    <div style="text-align: right;">
                                        <asp:Image ID="SecurityImage" runat="server"></asp:Image>
                                    </div>
                                </asp:Panel>
                            </div>
                        </div>
                        <div style="background: #f2f2f2; border-radius: 5px; padding: 26px 16px 17px; margin-top: 25px;">
                            <div style="font-size: 18px; font-weight: 700; color: #7c7a78; padding-bottom: 10px;">Forgotten your password?</div>
                            <%--<div style="font-size: 15px; font-weight: 400; color: #7c7a78; padding: 5px 0 30px; line-height: 19px;">
                                To receive your password via e-mail,<br />
                                please enter your email address below:
                            </div>--%>
                            <div style="font-size: 15px; padding-bottom: 5px; display: none;">Email address:</div>
                            <div>
                                <asp:TextBox ID="ForgotEMail" runat="server" ValidationGroup="Group2" CausesValidation="True" AutoCompleteType="Email" Columns="30" CssClass="signintextbox" placeholder="Enter Email Address"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ForgotEMail" ErrorMessage="(!signin.aspx.3!)" ValidationGroup="Group2"></asp:RequiredFieldValidator>
                            </div>
                            <div>
                                <asp:Button ID="RequestPassword" runat="server" CssClass="site-button checkoutanon-secondary-button content" ValidationGroup="Group2"></asp:Button>
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <asp:Panel ID="ExecutePanel" runat="server" Width="90%" HorizontalAlign="center" Visible="false">
                    <img src="images/spacer.gif" alt="" width="100%" height="40" />
                    <asp:Label ID="SignInExecuteLabel" runat="server" Font-Bold="true"></asp:Label>
                </asp:Panel>
            </div>
        </div>
    </form>
</body>
</html>
