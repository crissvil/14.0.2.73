<%@ Page Language="c#" Inherits="InterpriseSuiteEcommerce.checkoutreview" CodeFile="checkoutreview.aspx.cs" %>

<%@ Register TagPrefix="ise" Namespace="InterpriseSuiteEcommerceControls" Assembly="InterpriseSuiteEcommerceControls" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>
<%@ Register TagPrefix="ise" TagName="XmlPackage" Src="XmlPackageControl.ascx" %>
<%@ Import Namespace="InterpriseSuiteEcommerceCommon" %>
<%@ Register TagPrefix="uc" TagName="ScriptControl" Src="~/UserControls/ScriptControl.ascx" %>
<html>
<head>
</head> 
<body>
    <uc:ScriptControl ID="ctrlScript" runat="server"/>
    <asp:Panel runat="server">
        <div>
            <ise:Topic runat="server" ID="CheckoutReviewPageHeader" TopicName="CheckoutReviewPageHeader" />
            <asp:Literal ID="XmlPackage_CheckoutReviewPageHeader" runat="server" Mode="PassThrough"></asp:Literal>
            <form runat="server" id="CheckoutReview" name="CheckoutReview">
            <div style="clear: both; height: 14px;">
            </div>
            <div style="font-size: 15px; color: #414040; font-weight: 400; line-height: 18px;">
                <%--<asp:Literal ID="checkoutreviewaspx6" Mode="PassThrough" runat="server" Text="(!checkoutreview.aspx.6!)"></asp:Literal>--%>
                You are now ready to complete your order. Please review your order below, <br />and then click the 'Place Order' button only once below to process your order...
            </div>
            <div style="clear: both; height: 17px;">
            </div>
            <div id='place-order-button-container'>
                <div>
                    <div class="place-order-message">
                        <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
                        <span>
                            <asp:Literal ID="checkoutreviewaspx14" Mode="PassThrough" runat="server" Text="(!checkoutreview.aspx.14!)"></asp:Literal>
                        </span>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlButtonPlaceHolder" runat="server"  style="text-align: right;">
            <%if (Customer.Current.IsInEditingMode() && Security.IsAdminCurrentlyLoggedIn())
              { %>
                 <input type="button"  id="create-customer-account" 
                        class="site-button content" 
                        data-contentKey="checkoutreview.aspx.7" 
                        data-contentType="string resource"
                        data-contentValue="<%= AppLogic.GetString("checkoutreview.aspx.7", true)%>"
                        value="<%= AppLogic.GetString("checkoutreview.aspx.7", true)%>"/> 
            <% }
              else
              { %>
                <asp:Button ID="btnContinueCheckout1" 
                            Text="(!checkoutreview.aspx.7!)" 
                            CssClass="site-button checkoutreview-place-order-button" 
                            runat="server" />
            <% } %>
            </asp:Panel>

        <!-- Counpon Section { -->
    <div class="clear-both height-20"></div>
    <asp:Panel ID="panelCoupon" class="no-margin no-padding" runat="server">
        <div class="sections-place-holder no-padding">
            <div class="shoppingcart-section-header">
                <span><asp:Literal ID="Literal1" runat="server">(!checkoutshipping.aspx.14!)</asp:Literal></span>
            </div>
            <div id="divCouponEntered"><asp:Literal ID="Literal2" runat="server">(!order.cs.12!)</asp:Literal><asp:Literal runat="server" ID="litCouponEntered"></asp:Literal></div>
            </div>
    </asp:Panel>
    <!-- Counpon Section } -->

    <div class="sections-place-holder no-padding">
        <!-- Order Summary Section { -->
        <div class="sections-place-holder">
            <div class="shoppingcart-section-header">
                <span><asp:Literal ID="litItemsToBeShipped" runat="server">(!checkoutpayment.aspx.39!)</asp:Literal></span>
                <span class="one-page-link-right normal-font-style  float-right"><a href="shoppingcart.aspx" class="custom-font-style"><i class="fa fa-pencil"></i><asp:Literal ID="litEditCart" runat="server">(!checkoutpayment.aspx.40!)</asp:Literal></a></span>
            </div>
              
            <asp:Literal ID="OrderSummary" runat="server"></asp:Literal>
        </div>
        <!-- Order Summary Section } -->
    </div>
    <div style="clear:both; padding-top:72px;"></div>

    <div class="checkoutreview-billing-section">
        <div class="checkoutreview-billing-header">
            <span>
                <asp:Label ID="checkoutreviewaspx8" Text="(!checkoutreview.aspx.8!)" runat="server"></asp:Label>
            </span>
            <span class="one-page-link-right float-right">
                <a href="selectaddress.aspx?AddressType=Billing&editaddress=true&checkout=true"><asp:Literal ID="EditBillingAddress" runat="server"></asp:Literal></a>
            </span>
        </div>
        <div class="checkoutreview-label">
            <asp:Literal ID="litBillingAddress" runat="server" Mode="PassThrough"></asp:Literal>
        </div>
    </div>

    <div class="checkoutreview-shipping-section">
        <div class="checkoutreview-billing-header">
            <span>
                <asp:Label ID="ordercs57" Text="(!order.cs.19!)" runat="server"></asp:Label>
            </span>
            <span class="one-page-link-right float-right">
                <a href="selectaddress.aspx?AddressType=Shipping&editaddress=true&checkout=true"><asp:Literal ID="EditShippingAddress" runat="server"></asp:Literal></a>
            </span>
        </div>
        <div class="checkoutreview-label">
            <asp:Panel runat="server" Visible="false" ID="pnlShippingPickMessage">
                (<asp:Label ID="lblShippingPickMessage" runat="server" Text="(!checkoutreview.aspx.16!)" class="review-shipping-address-pickup-message" />)
                <div class="height-12"></div>
            </asp:Panel>
            <asp:Literal ID="litShippingAddress" runat="server" Mode="PassThrough"></asp:Literal>
        </div>
    </div>

    <div class="clear-both height-25"></div>
    <div class="height-25"></div>
    <div id='place-order-button-container-bottom'>
        <div>
            <div class="place-order-message">
                <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
                <span>
                    <asp:Literal ID="Literal3" Mode="PassThrough" runat="server" Text="(!checkoutreview.aspx.14!)"></asp:Literal>
                </span>
            </div>
        </div>
    </div>
    <div class="float-right">
        <asp:Button ID="btnContinueCheckout2" Text="(!checkoutreview.aspx.7!)" CssClass="site-button checkoutreview-place-order-button" runat="server" />
    </div>
    <ise:Topic runat="server" ID="CheckoutReviewPageFooter" TopicName="CheckoutReviewPageFooter" />
    <asp:Literal ID="XmlPackage_CheckoutReviewPageFooter" runat="server" Mode="PassThrough"></asp:Literal>
    <script type="text/javascript">
    $(document).ready(function () {
        var classIndex = 0;
        $(".aTaxRateValue").click(function () {
            var $this = $(this);

            var $divTaxBreakdown = $this.parent("span").parent("div").children(".divTaxBreakdownWrapper");
            var $hideDivBorder = $this.parent("span").parent("div").parent("div").children(".hide-on-tax-breakdown-display");

            var title = $this.attr("title");

            var mode = $this.attr("data-mode");
            mode = (typeof (mode) == "undefined") ? "show" : $.trim(mode);

            if (mode == "show") {

                $hideDivBorder.css("border-bottom", "1px solid #fff");
                $divTaxBreakdown.show("slide", { direction: "up" }, function () {
                    $this.attr("data-mode", "hide");
                });

            } else {

                $divTaxBreakdown.hide("slide", { direction: "up" }, function () {
                    $this.attr("data-mode", "show");
                    $hideDivBorder.css("border-bottom", "1px solid #ccc");
                });
            }

            $this.attr("title", $this.attr("data-title"));
            $this.attr("data-title", title);
        });
    });
    </script>
            </form>
        </div>
    </asp:Panel>
</body>
</html>
