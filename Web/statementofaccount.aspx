﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="statementofaccount.aspx.cs" Inherits="InterpriseSuiteEcommerce.statementofaccount" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Statement of Account</title>
</head>
<body>
    <form id="frmStatementofAccount" runat="server">
        <div class="shoppingcart-section-header">
            <span>Statement of Account</span>
        </div>
        <div style="padding-top: 15px;">
            <asp:Literal ID="ltContent" runat="server" />
        </div>
    </form>
</body>
</html>
