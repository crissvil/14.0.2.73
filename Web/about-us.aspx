﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="about-us.aspx.cs" Inherits="InterpriseSuiteEcommerce.about_us" %>

<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>About Us</title>
    <style>
        #frmAbouttUs p {
            line-height: 24px;
        }
    </style>
</head>
<body>
    <div class="clear-both height-5">&nbsp;</div>
    <form id="frmAbouttUs" runat="server">
        <h1>About Us</h1>
        <div class="content-box">
                <p style="font-size: 22px; color: #f48c00; line-height: 30px;">About Smartbag Australia</p>
                <div class="clear-both height-22"></div>
                <div class="height-10"></div>
                <p style="line-height: 24px;">Since 2003, Smartbag Australia has been a leading online provider of wholesale custom printed bags to clients in Australia. Operating from our warehouse in Sydney, 
                    Australia, we are proud to be a trusted online supplier of a wide range of custom packaging materials, including:
                </p>
                <div class="clear-both height-13">&nbsp;</div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-26-brown-kraft-paper-bags.aspx" target="_blank">Brown paper bags</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-29-white-kraft-paper-bags.aspx" target="_blank">White paper bags</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-247-brown-paper-wine-bags-with-die-cut-handles.aspx" target="_blank">Wine bags with die cut handles</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-187-reusable-eco-bags.aspx" target="_blank">Reusable eco bags</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-248-premium-paper-bags.aspx" target="_blank">Premium paper bags</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-11-stickers-and-stamps.aspx" target="_blank">Sticker and stamps</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-33-colourful-paper-bags.aspx" target="_blank">Colourful paper bags</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-191-laminated-paper-bags.aspx" target="_blank">Laminated paper bags</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-6-garment-covers.aspx" target="_blank">Garment covers</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-210-tissue-paper.aspx" target="_blank">Tissue paper</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-55-postal-satchels.aspx" target="_blank">Postal satchels</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-218-plastic-bags.aspx" target="_blank">Plastic bags</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="custom-bags-non-woven" target="_blank">Non-Woven/Reusable eco bags</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-83-kraft-paper-bags.aspx" target="_blank">Kraft paper bags</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-89-calico-bags.aspx" target="_blank">Calico bags</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="c-107-laminated.aspx" target="_blank">Laminated paper bags</a>
                    </p>
                    <div class="clear-both height-13"></div>
                    <p class="i-text-indent">
                        <i class="fa fa-caret-right"></i><a href="t-premium-bags.aspx" target="_blank">Boutique bags</a>
                    </p>
                <div class="clear-both height-13">&nbsp;</div>
                <p>Providing Reusable Bags to Large and Small Businesses in Australia</p>
                <div class="clear-both height-13">&nbsp;</div>
                <p style="line-height: 24px;">
                    Smartbag has provided custom printed bags to some of the biggest brands in the world, including Toyota, Mazda, Harvey Norman, Bose, Billabong, 
                    Air New Zealand, Caltex, and many more. We also cater to the custom bag needs of small businesses in the country, offering high-quality packaging materials at highly competitive prices.
                </p>
                <div class="clear-both height-13">&nbsp;</div>
                <p>Use Express Print for Short-run, Full-Colour Printed Bags</p>
                <div class="clear-both height-13">&nbsp;</div>
                <p style="line-height: 24px;">
                    Smartbag also offers custom a bag printing solution for retail, fashion, food and beverage, and corporate giveaways. Use our<a href="c-110-available-now-express-printed.aspx" style="color:#f48c00;text-decoration:underline;">Express
                    Print Service</a> to personalise your paper bags in one only design portal, adding your logo, graphics, and branding. 
                    You can also commission our in-house team to design your bags for you. Either way, all you have to do is wait for your bags to arrive at your doorstep.
                </p>
                <div class="clear-both height-13">&nbsp;</div>
                <p>
                    For inquiries or quotes of our reusable and custom printed bags, contact <a href="contact-us.aspx" style="color:#f48c00;text-decoration:underline;">Smartbag Australia</a> at 1300 874 559 or +61 2 9605 6871.
                </p>
            </div>
    </form>
</body>
</html>
