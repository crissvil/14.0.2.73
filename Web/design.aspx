﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="design.aspx.cs" Inherits="InterpriseSuiteEcommerce.design" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<script type="text/javascript" src="https://www.smartcolourprint.com.au/sgeprintapieditor/content/scripts/composer.js"></script>--%>
    <script type="text/javascript" src="http://ds01631.hosting24.com.au/sgeprintapieditor/content/scripts/composer.js"></script>
</head>
<body>
    <form id="frmDesign" runat="server">
        <div style="padding-bottom: 30px; text-align: right;">
            <input name="btnSave" value="Save Changes" id="btnSave" class="site-button design-save-button hide" type="submit" />
        </div>
        <div id="wysiwyg"></div>
        <div style="padding-top: 30px; text-align: right;">
            <input name="btnSave2" value="Save Changes" id="btnSave2" class="site-button design-save-button hide" type="submit" />
        </div>
        <div class="height-20"></div>

        <script>
            $(document).ready(function () {
                ShowModalPopup("Processing...");
                SetPrintAPIEditor();
                CloseModalPopup();
            });

            function SetPrintAPIEditor() {
                var api = "";
                var token = "";
                var param = new Object();
                param.ItemDocumentCode = GetQueryString("dcode");
                param.CustomizationCode = GetQueryString("ccode");
                param.RecID = GetQueryString("recid");
                $.ajax({
                    type: "POST",
                    url: "ActionService.asmx/GetPrintAPISetting",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(param),
                    dataType: "json",
                    async: false,
                    success: function (result) {
                        if (result.d != null) {
                            var lst = $.parseJSON(result.d);
                            for (var i = 0; i < lst.length; i++) {
                                if (lst[i].Key == "custom.print.api.username") {
                                    api = lst[i].Value;
                                }
                                else if (lst[i].Key == "custom.print.api.editorcode") {
                                    token = lst[i].Value;
                                }
                            }
                        }
                    },
                    fail: function (result) {
                        alert("An error was encountered.");
                    }
                });

                var editor = new Composer.SimpleEditor({
                    apiKey: api,
                    editToken: token,
                });

                editor.setWYSIWYG('wysiwyg');
                $('#btnSave').removeClass('hide');
                $('#btnSave2').removeClass('hide');

                $('#btnSave').click(function () {
                    editor.close();
                });

                $('#btnSave2').click(function () {
                    editor.close();
                });
            }

            function GetQueryString(key) {
                var vars = [], hash;
                var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
                for (var i = 0; i < hashes.length; i++) {
                    hash = hashes[i].split('=');
                    vars.push(hash[0]);
                    vars[hash[0]] = hash[1];
                }
                if (vars[key])
                {
                    return vars[key];
                }
                return "";
            }
        </script>
    </form>

</body>
</html>
