﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="custom-bags-non-woven.aspx.cs" Inherits="InterpriseSuiteEcommerce.custom_bags_non_woven"%>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title></title>
</head>
<body>
<ise:InputValidatorSummary ID="errorSummary" CssClass="error float-left normal-font-style" runat="server" Register="False" />
<div class="clear-both height-5">&nbsp;</div>
<form id="frmCustomBags" runat="server">
<asp:Panel ID="pnlPageContentWrapper" runat="server">
<h4>Scroll Down For More Products</h4>
<h1 id="ï-custom-woven-page-title">Custom Bags Non Woven</h1>
<div class="no-padding">
    <ul runat="server" id="listWrapper"></ul>
</div>
 <script type="text/javascript">
     $(document).ready(function () {
         $("#custom-bags-non-woven-submenus").fadeIn();
         //$("#i-custom-bags-header").css("color", "#fc8204").removeClass("no-arrow");
         $("#i-custom-bags-header").removeClass("no-arrow").addClass("menu-selected");
     });
 </script>
</asp:Panel>
</form>
</body>
</html>