﻿using System;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Data.SqlClient;
using InterpriseSuiteEcommerceCommon;
using System.Data;
using System.Text;
using System.IO;

namespace InterpriseSuiteEcommerce
{
    public partial class uploaddesign : SkinBase
    {
        string fname, lname, email = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            litItemInfo.Text = GetItemDetails();
        }

        public void btnSubmit_Click(object sender, EventArgs e)
        {
            SaveDesign();
            AppLogic.SaveCustomizationCode(2);
            Response.Redirect("shoppingcart.aspx");
        }
        public void btnDesignYourOwn_Click(object sender, EventArgs e)
        {
            string dcode = "dcode".ToQueryString();
            string ccode = "ccode".ToQueryString();
            string recid = "recid".ToQueryString();
            string qty = "qty".ToQueryString();
            string designURL = string.Format("designit.aspx?dcode={0}&ccode={1}&recid={2}&qty={3}", dcode, ccode, recid, qty);
            Response.Redirect(designURL);
        }

        public void btnDesignForYou_Click(object sender, EventArgs e)
        {
            string dcode = "dcode".ToQueryString();
            string ccode = "ccode".ToQueryString();
            string recid = "recid".ToQueryString();
            string qty = "qty".ToQueryString();
            string designURL = string.Format("wedesign.aspx?dcode={0}&ccode={1}&recid={2}&qty={3}", dcode, ccode, recid, qty);
            Response.Redirect(designURL);
            
        }

        private void InitializeContent()
        {
            SectionTitle = "Upload your own design";
            if (ThisCustomer.IsRegistered)
            {
                fname = ThisCustomer.FirstName;
                lname = ThisCustomer.LastName;
                email = ThisCustomer.EMail;
            }
        }

        private string GetItemDetails()
        {
            string dcode = "dcode".ToQueryString();
            string itemDesc = string.Empty, dimension = string.Empty, printArea = string.Empty;
            decimal qty = 0;
            var content = new StringBuilder();
            bool exist = false;
            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "SELECT C.ItemDescription, A.Quantity, B.ProductSize_C, PrintedArea_C FROM EcommerceShoppingCart A INNER JOIN InventoryItem B ON A.ItemCode = B.ItemCode INNER JOIN InventoryItemDescription C ON A.ItemCode = C.ItemCode WHERE A.VDPDocumentCode_DEV004817 = {0} AND CustomerCode = {1} AND LanguageCode = {2}", DB.SQuote(dcode), DB.SQuote(ThisCustomer.CustomerCode), DB.SQuote(ThisCustomer.LanguageCode)))
                {
                    if (reader.Read())
                    {
                        itemDesc = reader.ToRSField("ItemDescription");
                        qty = reader.ToRSFieldDecimal("Quantity");
                        dimension = reader.ToRSField("ProductSize_C");
                        printArea = reader.ToRSField("PrintedArea_C");
                        if (printArea == "")
                        {
                            printArea = "N/A";
                        }
                        exist = true;
                    }
                }
            }
            if (exist)
            {
                content.Append("<ul>");
                content.AppendLine("<li>" + itemDesc + "</li>");
                content.AppendLine("<li>" + dimension + "</li>");
                content.AppendLine("<li>Quantity: " + Localization.ParseLocaleDecimal(qty, ThisCustomer.LocaleSetting) + "</li>");
                content.AppendLine("<li>Print Area: " + printArea + "</li>");
                content.AppendLine("</ul>");
            }
            return content.ToString();
        }
        public string GetFirstName()
        {
            return fname;
        }
        public string GetLasttName()
        {
            return lname;
        }
        public string GetEmail()
        {
            return email;
        }

        private void SaveDesign()
        {
            string comments = Request.Form["Comments"];
            string recid = "recid".ToQueryString();
            string new_fileName = Guid.NewGuid().ToString();
            string fileName = Path.GetFileNameWithoutExtension(fileUploadImage.FileName);
            string fileExtension = Path.GetExtension(fileUploadImage.FileName);
            string str_image = new_fileName + fileExtension;
            string pathToSave = Server.MapPath("~/images/upload/") + str_image;
            //Save design to server folder
            fileUploadImage.SaveAs(pathToSave);
            //Save design to database
            byte[] data = new byte[fileUploadImage.PostedFile.ContentLength];
            fileUploadImage.PostedFile.InputStream.Read(data, 0, fileUploadImage.PostedFile.ContentLength);
            //DB.ExecuteSQL("UPDATE EcommerceShoppingCart SET DesignComments_C = {0}, DesignFilename_C = '{1}', DesignFile_C = {2} WHERE ShoppingCartRecGUID = {3}", DB.SQuote(comments), str_image, data.Length, DB.SQuote(recid));
            using (SqlConnection con = DB.NewSqlConnection())
            {
                string query = "UPDATE EcommerceShoppingCart SET DesignComments_C = @DesignComments_C, DesignFilename_C = @DesignFilename_C, DesignFile_C = @DesignFile_C WHERE ShoppingCartRecGUID = @ShoppingCartRecGUID";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@DesignComments_C", comments);
                    cmd.Parameters.AddWithValue("@DesignFilename_C", str_image);
                    cmd.Parameters.AddWithValue("@DesignFile_C", data);
                    cmd.Parameters.AddWithValue("@ShoppingCartRecGUID", recid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            } 

            //LoadImage(recid);
        }

        private void LoadImage(string recid)
        {
            byte[] image = new byte[] { };
            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "SELECT DesignFile_C FROM EcommerceShoppingCart WHERE ShoppingCartRecGUID = {0}", DB.SQuote(recid)))
                {
                    if (reader.Read())
                    {
                        image = reader.ToRSFieldImage("DesignFile_C");
                    }
                }
            }
            FileStream fs = new FileStream("C:/tmp/test.jpg", FileMode.OpenOrCreate, FileAccess.Write);
            fs.Write(image, 0, image.Length);
            fs.Close();
        }
    }
}