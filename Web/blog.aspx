﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="blog.aspx.cs" Inherits="InterpriseSuiteEcommerce.blog" %>

<%@ Register TagPrefix="ise" TagName="XmlPackage" Src="XmlPackageControl.ascx" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body class="Mozilla WinOS">
    <form id="frmBlog" runat="server">
        <ise:XmlPackage ID="XmlPackage1" PackageName="page.blog.xml.config" runat="server"
            EnforceDisclaimer="true" EnforcePassword="True" EnforceSubscription="true" AllowSEPropogation="True" />
    </form>
</body>
</html>
