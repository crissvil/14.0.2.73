﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using InterpriseSuiteEcommerceGateways;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerce
{
    public partial class contact_us : SkinBase
    {
        protected override void OnInit(EventArgs e)
        {
            btnSendMessage.Click += (sender, ex) => SendMessage();
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            InitPageContent();
        }


        protected void SendMessage()
        {
            try
            {
                if (!Interprise.Framework.Base.Shared.Common.IsValidEmail(txtEmail.Text))
                {
                    errorSummary.DisplayErrorMessage(AppLogic.GetString("contactus.aspx.12", true));
                    txtCaptcha.Text = String.Empty;
                    return;
                }

                if (!IsSecurityCodeGood(txtCaptcha.Text))
                {
                    errorSummary.DisplayErrorMessage(AppLogic.GetString("contactus.aspx.13", true));
                    return;
                }

                if (AppLogic.OnLiveServer() && (Request.UrlReferrer == null || Request.UrlReferrer.Authority != Request.Url.Authority))
                {
                    Response.Redirect("default.aspx", true);
                    return;
                }

                //   var content = new StringBuilder();

                string subject = AppLogic.AppConfig("custom.contact.us.title");
                string senderName = txtFirstName.Text.Trim() + " " + txtLastName.Text.Trim();
                string senderEmail = txtEmail.Text.Trim();

                string toName = CommonLogic.IIF(AppLogic.AppConfig("ContactUsNameTo").IsNullOrEmptyTrimmed(), AppLogic.AppConfig("GotOrderEMailToName"), AppLogic.AppConfig("ContactUsNameTo"));
                toName = toName.Trim();

                string toEmail = CommonLogic.IIF(AppLogic.AppConfig("ContactUsEmailTo").IsNullOrEmptyTrimmed(), AppLogic.AppConfig("GotOrderEMailTo"), AppLogic.AppConfig("ContactUsEmailTo"));
                toEmail = toEmail.Trim();

                string message = txtMessageDetails.Text.Trim();

                string html_msg = "<table style='width:98%;padding:0;background:#f5f5f5;'>";
                html_msg += "<tbody>";
                html_msg += "<tr>";
                html_msg += "<td style='background:#2f2f2f;color:#fff;padding:5px'><i>Message</i></td>";
                html_msg += "</tr>";
                html_msg += "<tr>";
                html_msg += "<td style='padding:5px;height:20px;'>";
                html_msg += "<p>" + txtMessageDetails.Text.Trim() + "</p>";
                html_msg += "</td>";
                html_msg += "</tr>";
                html_msg += "<tr>";
                html_msg += "<td style='background:#2f2f2f;color:#fff;padding:5px'><i>Contact Information</i></td>";
                html_msg += "</tr>";
                html_msg += "<tr>";
                html_msg += "<td>";
                html_msg += "<table style='width:100%'>";
                html_msg += "<tbody>";
                html_msg += "<tr>";
                html_msg += "<td style='border-bottom:1px solid #fff;width:150px;background:#ccc;padding:5px'>First Name</td>";
                html_msg += "<td>" + txtFirstName.Text.Trim() + "</td>";
                html_msg += "</tr>";
                html_msg += "<tr>";
                html_msg += "<td style='border-bottom:1px solid #fff;width:150px;background:#ccc;padding:5px'>Last Name</td>";
                html_msg += "<td>" + txtLastName.Text.Trim() + "</td>";
                html_msg += "</tr>";
                html_msg += "<tr>";
                html_msg += "<td style='border-bottom:1px solid #fff;width:150px;background:#ccc;padding:5px'>Email Address</td>";
                html_msg += "<td>" + txtEmail.Text.Trim() + "</td>";
                html_msg += "</tr>";
                html_msg += "<tr>";
                html_msg += "<td style='border-bottom:1px solid #fff;width:150px;background:#ccc;padding:5px'>Mobile Number</td>";
                html_msg += "<td>" + txtMobile.Text.Trim() + "</td>";
                html_msg += "</tr>";
                html_msg += "<tr>";
                html_msg += "<td style='border-bottom:1px solid #fff;width:150px;background:#ccc;padding:5px'>Phone Number</td>";
                html_msg += "<td>" + txtPhone.Text.Trim() + "</td>";
                html_msg += "</tr>";
                html_msg += "</tbody>";
                html_msg += "</table>";
                html_msg += "</td>";
                html_msg += "</tr>";

                html_msg += "<tr>";
                html_msg += "<td>";
                html_msg += "<table style='width:100%'>";
                html_msg += "<tbody>";
                html_msg += "<tr>";
                html_msg += "<td style='border-bottom:1px solid #fff;width:150px;background:#ccc;padding:5px'>Company</td>";
                html_msg += "<td>" + txtCompany.Text.Trim() + "</td>";
                html_msg += "</tr>";
                html_msg += "<tr>";
                html_msg += "<td style='border-bottom:1px solid #fff;width:150px;background:#ccc;padding:5px'>Industry Group</td>";
                html_msg += "<td>" + txtIndustryGroup.Text.Trim() + "</td>";
                html_msg += "</tr>";
                html_msg += "</tbody>";
                html_msg += "</table>";
                html_msg += "</td>";
                html_msg += "</tr>";


                html_msg += "<tr>";
                html_msg += "<td>";
                html_msg += "<table style='width:100%'>";
                html_msg += "<tbody>";
                html_msg += "<tr>";
                html_msg += "<td style='border-bottom:1px solid #fff;width:150px;background:#ccc;padding:5px'>Address</td>";
                html_msg += "<td>" + txtAddress.Text.Trim() + "</td>";
                html_msg += "</tr>";
                html_msg += "<tr>";
                html_msg += "<td style='border-bottom:1px solid #fff;width:150px;background:#ccc;padding:5px'>City / Suburb</td>";
                html_msg += "<td>" + txtCity.Text.Trim() + "</td>";
                html_msg += "</tr>";
                html_msg += "<tr>";
                html_msg += "<td style='border-bottom:1px solid #fff;width:150px;background:#ccc;padding:5px'>State</td>";
                html_msg += "<td>" + txtState.Text.Trim() + "</td>";
                html_msg += "</tr>";
                html_msg += "<tr>";
                html_msg += "<td style='border-bottom:1px solid #fff;width:150px;background:#ccc;padding:5px'>Postal</td>";
                html_msg += "<td>" + txtPostal.Text.Trim() + "</td>";
                html_msg += "</tr>";
                html_msg += "<tr>";
                html_msg += "<td style='border-bottom:1px solid #fff;width:150px;background:#ccc;padding:5px'>Country</td>";
                html_msg += "<td>" + txtCountry.Text.Trim() + "</td>";
                html_msg += "</tr>";
                html_msg += "</tbody>";
                html_msg += "</table>";
                html_msg += "</td>";
                html_msg += "</tr>";


                html_msg += "</tbody>";
                html_msg += "</table>";


                //  content.Append(message);
                //  content.Append("<br/><br/>");

                //  content.AppendFormat("Contact Name: {0}", senderName);
                //  content.Append("<br/>");
                //   content.AppendFormat("Phone: {0}", txtPhone.Text.Trim());
                //   content.Append("<br/>");
                //   content.AppendFormat("Email Address: {0}", senderEmail);

                AppLogic.SendMailRequest(subject, html_msg, true, senderEmail, senderEmail, toEmail, toName, string.Empty, true);
                Response.Redirect("t-ContactUsFormThankYouPage.aspx");

            }
            catch (Exception ex)
            {
                errorSummary.DisplayErrorMessage(ex.Message);
            }
        }


        protected bool IsSecurityCodeGood(string code)
        {

            if (!AppLogic.AppConfigBool("SecurityCodeRequiredOnContactUs")) return true;

            if (Session["SecurityCode"] != null)
            {

                string sCode = Session["SecurityCode"].ToString();
                string fCode = code;

                if (AppLogic.AppConfigBool("Captcha.CaseSensitive"))
                {
                    if (fCode.Equals(sCode)) return true;
                }
                else
                {
                    if (fCode.Equals(sCode, StringComparison.InvariantCultureIgnoreCase)) return true;
                }

                return false;
            }

            return true;

        }

        protected void InitPageContent()
        {
            // SectionTitle = AppLogic.GetString("contactus.aspx.1", true);
            btnSendMessage.Text = AppLogic.GetString("contactus.aspx.11", true);

            if (!AppLogic.AppConfigBool("SecurityCodeRequiredOnContactUs")) pnlSecurityCode.Visible = false;

            if (ThisCustomer.IsInEditingMode())
            {
                AppLogic.EnableButtonCaptionEditing(btnSendMessage, "contactus.aspx.11");
            }

            char[] delimiterChars = { ',' };

            var i = AppLogic.AppConfig("custom.industrial.group").Trim();
            string[] str = i.Split(delimiterChars);

            var listItem = new ListItem();
            listItem.Text = "- Please Select --";
            listItem.Value = "";

            drpIndustryGroup.Items.Add(listItem);

            foreach (string y in str)
            {

                listItem = new ListItem();
                listItem.Value = y;
                drpIndustryGroup.Items.Add(listItem);
            }

            i = AppLogic.AppConfig("custom.states").Trim();
            str = i.Split(delimiterChars);

            listItem = new ListItem();
            listItem.Text = "- Please Select --";
            listItem.Value = "";

            drpState.Items.Add(listItem);

            char[] d = { ':' };
            foreach (string y in str)
            {

                string[] s = y.Split(d);

                listItem = new ListItem();
                listItem.Value = s[0];
                listItem.Text = s[0];

                drpState.Items.Add(listItem);
            }
            AppLogic.GetAUCountry(ref drpCountry);
        }
    }
}