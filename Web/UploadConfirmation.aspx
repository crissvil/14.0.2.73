﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UploadConfirmation.aspx.cs" Inherits="UploadConfirmation" %>
<%@ Import Namespace="InterpriseSuiteEcommerceCommon" %>
<%@ Import Namespace="InterpriseSuiteEcommerceCommon.Extensions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Smart Bag</title>
    
</head>
<body>
    <form id="frmUploadConfirmation" runat="server" action="">
    <asp:Panel runat="server" ID="pnlContent">
        <DIV class=uploadconfirmation-container>
        <DIV class=uploadconfirmation-content><IMG src="skins/Skin_(!SKINID!)/images/quote-checkbox.jpg"> 
        <DIV class=uploadconfirmation-label>Thank you for your upload</DIV></DIV>
        <DIV class=uploadconfirmation-contactus>Someone from the Smartbag team will contact you<BR>shortly regarding your enquiry.</DIV>
        </DIV>
    </asp:Panel>
  </form>
</body>
</html>
