﻿using InterpriseSuiteEcommerceCommon;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerce
{
    public partial class portalapprovals : SkinBase
    {
        Customer ThisCustomer = Customer.Current;
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.CacheControl = "private";
            Response.Expires = 0;
            Response.AddHeader("pragma", "no-cache");
            RequiresLogin(CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables("QUERY_STRING"));
            RequireSecurePage();
            if (Page.IsPostBack)
            {
                ProcessApprovals();
            }
            InitializePageContent();
        }

        private void InitializePageContent()
        {
            var month = "month".ToQueryString();
            List<XmlPackageParam> runtimeParams = new List<XmlPackageParam>();
            runtimeParams.Add(new XmlPackageParam("WarehouseCode", ThisCustomer.WarehouseCode));
            runtimeParams.Add(new XmlPackageParam("CurrentMonth", month));
            runtimeParams.Add(new XmlPackageParam("CurrentDate", String.Format("{0:dd/MM/yyyy}", DateTime.Now.Date)));
            runtimeParams.Add(new XmlPackageParam("TwoWeeks", String.Format("{0:dd/MM/yyyy}", DateTime.Now.Date.AddDays(14))));
            runtimeParams.Add(new XmlPackageParam("CostCenter", ThisCustomer.CostCenter));
            ltPageContent.Text = AppLogic.RunXmlPackage("page.portalapprovals.xml.config", base.GetParser, ThisCustomer, SkinID, String.Empty, runtimeParams, true, true);
        }

        private void ProcessApprovals()
        {
            string counter = string.Empty, POCode = string.Empty, PODate = string.Empty, ETA = string.Empty;
            string status = Request.Form["drpApprovalDate"];
            StringBuilder approved = new StringBuilder(), declined = new StringBuilder();
            foreach (string key in Request.Form.AllKeys)
            {
                try
                {
                    if (key.StartsWith("chkApproved_"))
                    {
                        string[] field = key.Split('_');
                        counter = field[1];
                        POCode = Request.Form["PurchaseOrderCode_" + counter];
                        PODate = Request.Form["DateApproved1_" + counter];
                        ETA = Request.Form["ETA1_" + counter];
                        UpdatePurchaseOrderApproved(POCode, PODate, ETA);
                        SendEmail(POCode, "Approved");
                    }
                    else if (key.StartsWith("chkDeclined_"))
                    {
                        string[] field = key.Split('_');
                        counter = field[1];
                        POCode = Request.Form["PurchaseOrderCode_" + counter];
                        PODate = Request.Form["DateDeclined1_" + counter];
                        if (PODate.Length > 0)
                        {
                            UpdatePurchaseOrderDeclined(POCode);
                            SendEmail(POCode, "Declined");
                        }
                    }
                    else if (key.StartsWith("chkDeclined1_"))
                    {
                        if (Request.Form[key] != "1" || status != "1")
                        {
                            continue;
                        }
                        string[] field = key.Split('_');
                        counter = field[1];
                        POCode = Request.Form["PurchaseOrderCode_" + counter];
                        UpdatePurchaseOrderDeclinedUncheck(POCode);
                        SendEmail(POCode, "UnDeclined");
                    }
                }
                catch
                {
                    // do nothing, add the items that we can
                }
            }
        }

        private bool UpdatePurchaseOrderApproved(string POCode, string PODate, string ETA)
        {
            try
            {
                if(ETA.Length > 0)
                {
                    ETA = Convert.ToDateTime(ETA).ToString("MM/dd/yyyy");
                }
                DB.ExecuteSQL(string.Format("UPDATE SupplierPurchaseOrder SET IsApproved_DEV004817 = 1, DateApproved_DEV004817 = {0}, ETA_C = {1} WHERE PurchaseOrderCode = {2}", DB.SQuote(Convert.ToDateTime(PODate).ToString("MM/dd/yyyy")), DB.SQuote(ETA), DB.SQuote(POCode)));
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private bool UpdatePurchaseOrderDeclined(string POCode)
        {
            try
            {
                DB.ExecuteSQL(string.Format("UPDATE SupplierPurchaseOrder SET IsDeclined_DEV004817 = 1, DateDeclined_DEV004817 = {0} WHERE PurchaseOrderCode = {1}", DB.SQuote(Convert.ToDateTime(DateTime.Today).ToString("MM/dd/yyyy")), DB.SQuote(POCode)));
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private bool UpdatePurchaseOrderDeclinedUncheck(string POCode)
        {
            try
            {
                DB.ExecuteSQL(string.Format("UPDATE SupplierPurchaseOrder SET IsDeclined_DEV004817 = 0, DateDeclined_DEV004817 = '' WHERE PurchaseOrderCode = {0}", DB.SQuote(POCode)));
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private bool SendEmail(string POCode, string Type)
        {
            string approvedRecipient = string.Empty, declinedRecipient = string.Empty, undeclinedRecipient = string.Empty, recipient = string.Empty, subject = string.Empty;
            switch (ThisCustomer.CostCenter)
            {
                case "Smartbag-PREMIUM CUE":
                    approvedRecipient = AppLogic.AppConfig("custom.cue.approval.approved.email.notification");
                    declinedRecipient = AppLogic.AppConfig("custom.cue.approval.declined.email.notification");
                    undeclinedRecipient = AppLogic.AppConfig("custom.cue.approval.undeclined.email.notification");
                    break;
                case "Smartbag-PREMIUM APG":
                    approvedRecipient = AppLogic.AppConfig("custom.apg.approval.approved.email.notification");
                    declinedRecipient = AppLogic.AppConfig("custom.apg.approval.declined.email.notification");
                    undeclinedRecipient = AppLogic.AppConfig("custom.apg.approval.undeclined.email.notification");
                    break;
                case "Smartbag-PREMIUM DION LEE":
                    approvedRecipient = AppLogic.AppConfig("custom.dl.approval.approved.email.notification");
                    declinedRecipient = AppLogic.AppConfig("custom.dl.approval.declined.email.notification");
                    undeclinedRecipient  = AppLogic.AppConfig("custom.dl.approval.undeclined.email.notification");
                    break;
                default:
                    approvedRecipient = AppLogic.AppConfig("custom.hn.approval.approved.email.notification");
                    declinedRecipient = AppLogic.AppConfig("custom.hn.approval.declined.email.notification");
                    undeclinedRecipient = AppLogic.AppConfig("custom.hn.approval.undeclined.email.notification");
                    break;
            }
            if (Type == "Approved")
            {
                recipient = approvedRecipient;
            }
            else if (Type == "Declined")
            {
                recipient = declinedRecipient;
            }
            else
            {
                recipient = undeclinedRecipient;
            }
            subject = Type + " Purchase Order from " + ThisCustomer.CostCenter + "[" + POCode + "]";

            List<XmlPackageParam> runtimeParams = new List<XmlPackageParam>();
            runtimeParams.Add(new XmlPackageParam("PurchaseOrderCode", POCode));
            runtimeParams.Add(new XmlPackageParam("LanguageCode", ThisCustomer.LanguageCode));
            runtimeParams.Add(new XmlPackageParam("Type", Type));
            runtimeParams.Add(new XmlPackageParam("CostCenter", ThisCustomer.CostCenter));
            runtimeParams.Add(new XmlPackageParam("CurrentDate", String.Format("{0:dd/MM/yyyy}", DateTime.Now.Date)));
            var msg = AppLogic.RunXmlPackage("notification.reorderapproval.xml.config", null, Customer.Current, Customer.Current.SkinID, String.Empty, runtimeParams, true, true);
            if (msg.Length > 0)
            {
                AppLogic.SendMail(subject, msg, true, null, null, recipient, recipient, "", AppLogic.AppConfig("MailMe_Server"));
                return true;
            }
            return false;
        }
    }
}