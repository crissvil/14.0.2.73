﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="userlist.aspx.cs" Inherits="userlist" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report</title>
    <link href="mvwres:1-DevExpress.Web.ASPxEditors.Css.Default.css" rel="stylesheet" type="text/css" />
    <link href="mvwres:1-DevExpress.Web.ASPxEditors.Css.Default.css" rel="stylesheet" type="text/css" />
    <link href="mvwres:1-DevExpress.Web.ASPxEditors.Css.Default.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.min.css" />
    <script src="//code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/55068458dc.js"></script>
    
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" />
    <style>
        body {
            font-family: Roboto;
            font-weight: 400;
            font-size: 14px;
        }

        #modalpopup, #messagebox, #confirmationbox {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 999; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        #modalpopup-content {
            background-color: #f2f2f2;
            border-radius: 10px;
            box-shadow: 0 1px 5px #000;
            padding: 30px 30px;
            text-align: center;
            vertical-align: middle;
            min-height: 125px;
            min-width: 200px;
            font-size: 15px;
            display: inline-table;
        }

            #modalpopup-content i {
                font-size: 40px;
            }

        #modalpopup-text {
            padding-top: 5px;
        }

        .filter-date {
            background-color: #f0f0f0;
            border: 1px solid #a8a8a8;
            box-sizing: border-box;
            float: left;
            padding: 3px 3px 0 10px;
            height: 32px;
            border-left: none;
        }

        .hide {
            display: none;
        }
    </style>
</head>
<body>
    <form id="frmUserList" runat="server">
        <div style="text-align: center;">
            <div style="float: left;">
                <dxwc:ReportToolbar ID="ReportToolbar1" runat="server" ReportViewer="<%# ViewerReport %>"
                    ShowDefaultButtons="False">
                    <Items>
                        <dxwc:ReportToolbarButton ImageUrl="~/images/icon.png" Name="Home" ToolTip="Back to Homepage" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton ItemKind="Search" ToolTip="Display the search window" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton ItemKind="PrintReport" ToolTip="Print the report" />
                        <dxwc:ReportToolbarButton ItemKind="PrintPage" ToolTip="Print the current page" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton Enabled="False" ItemKind="FirstPage" ToolTip="First Page" />
                        <dxwc:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" ToolTip="Previous Page" />
                        <dxwc:ReportToolbarLabel Text="Page" />
                        <dxwc:ReportToolbarComboBox ItemKind="PageNumber">
                        </dxwc:ReportToolbarComboBox>
                        <dxwc:ReportToolbarLabel Text="of" />
                        <dxwc:ReportToolbarTextBox ItemKind="PageCount" Width="35px" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton ItemKind="NextPage" ToolTip="Next Page" />
                        <dxwc:ReportToolbarButton ItemKind="LastPage" ToolTip="Last Page" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton ItemKind="SaveToDisk" ToolTip="Export a report and save it to the disk" />
                        <dxwc:ReportToolbarButton ItemKind="SaveToWindow" ToolTip="Export a report and show it in a new window" />
                        <dxwc:ReportToolbarComboBox ItemKind="SaveFormat">
                            <Elements>
                                <dxwc:ListElement Text="Xls" Value="csv" />
                                <dxwc:ListElement Text="Pdf" Value="pdf" />
                                <%--<dxwc:ListElement Text="Xls" Value="xls" />--%>
                                <dxwc:ListElement Text="Rtf" Value="rtf" />
                                <dxwc:ListElement Text="Mht" Value="mht" />
                                <dxwc:ListElement Text="Text" Value="txt" />
                                <dxwc:ListElement Text="Image" Value="png" />
                            </Elements>
                        </dxwc:ReportToolbarComboBox>
                    </Items>
                    <ClientSideEvents ItemClick="function(s, e) {
	                        if(e.item.name =='Home' ) 
                                window.location.href = 'portal.aspx';
                                                                }" />
                    <Styles>
                        <LabelStyle>
                            <Margins MarginLeft="3px" MarginRight="3px" />
                        </LabelStyle>
                    </Styles>
                </dxwc:ReportToolbar>
            </div>
            <dxwc:ReportViewer ID="ViewerReport" runat="server" OnCacheReportDocument="ViewerReport_CacheReportDocument">
            </dxwc:ReportViewer>
        </div>
        <div style="padding-left: 10px; font-size: 18px;">
            <div id="modalpopup">
                <div id="modalpopup-content">
                    <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                    <div id="modalpopup-text">Loading...</div>
                </div>
            </div>
            <asp:Literal ID="ltReport" runat="server" Visible="false" />
        </div>
    </form>

    <script type="text/javascript">
        function ShowModalPopup(message) {
            var height = $(window).height();
            var width = $(window).width();
            var popup = $("#modalpopup-content");
            var posTop = (height / 2 - popup.height() / 2) - 70;
            var posLeft = (width / 2 - popup.width() / 2) - 150;

            $("#modalpopup-content").css({
                "margin-top": posTop,
                "margin-left": posLeft
            });

            $("#modalpopup-text").html(message);
            $("#modalpopup").css({ "display": "block" });
        }
        
    </script>
</body>
</html>
