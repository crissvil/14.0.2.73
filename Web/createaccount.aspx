﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="createaccount.aspx.cs" Inherits="InterpriseSuiteEcommerce.createaccount" %>

<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="ShippingAddressControl" Src="~/UserControls/AddressControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="BillingAddressControl" Src="~/UserControls/AddressControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProfileControl" Src="~/UserControls/ProfileControl.ascx" %>
<%@ Import Namespace="InterpriseSuiteEcommerceCommon" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <ise:InputValidatorSummary ID="errorSummary" CssClass="error float-left normal-font-style" runat="server" Register="False" />
    <form id="createAccount" runat="server">
        <asp:Panel ID="pnlPageContentWrapper" runat="server">
            <div>
                <div align="center" style="text-align: left;">
                    <asp:Panel ID="FormPanel" runat="server" Width="100%">
                        <div class="clr height-12"></div>
                        <h1 class="createaccount-headertext">Create a Smartbag Account</h1>
                        <div class="clr"></div>
                        <div class="createaccount-top-section">
                            <div style="background: #86d6a3; color: #449060; border-radius: 5px; padding: 30px 20px 20px;">
                                <span class="signinheaderlabel">Why create an account?</span>
                                <div style="border-top: 1px solid #449060; margin-top: 15px; padding: 10px 0;"></div>
                                <div class="signinperks">
                                    <ul>
                                        <li>
                                            <i class="fa fa-check"></i><span>Save your re-order and purchase history</span>
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i><span>Faster checkout process</span>
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i><span>VIP exclusive offers</span>
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i><span>Check the status of your order</span>
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i><span>Store multiple shipping addresses</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="createaccount-left-section">
                            <div style="background: #f2f2f2; border-radius: 5px; padding: 27px 15px 16px; color: #6c6a6b; font-size: 15px">
                                <span class="signinheaderlabel">Profile information</span>
                                <div style="padding-top: 15px; line-height: 21px; color: #7c7a78; font-weight: 300;">Your account provides you with access to a variety of benefits, such as Order Status and Purchase History:</div>
                                <div style="padding: 10px 0 30px; color: #7c7a78; font-weight: 300;">If you already have an account, click <a href="signin.aspx" class="i-orange-link">HERE</a> to login.</div>
                                <div style="text-align: right; color: #ff0000; font-size: 12px; font-weight: 400; padding: 8px 0 19px;">
                                    *Required field
                                </div>
                                <div>
                                    <uc:ProfileControl ID="ProfileControl" runat="server" />
                                </div>
                                <div>
                                    <uc:BillingAddressControl ID="BillingAddressControl" IdPrefix="billing-" runat="server" />
                                </div>
                                <div style="padding-top: 35px;">
                                    <span id="lit-shipping-info" class="">
                                        <asp:Literal ID="litShippingInfo" runat="server">(!createaccount.aspx.110!)</asp:Literal></span>
                                    <span id="copy-billing-info-place-holder">
                                        <label class="checkbox-container">
                                            <span class="checkbox-captions custom-font-style" id="i-copy-billing-text">
                                                <asp:Literal ID="litSameAsBillingInfo" runat="server">(!createaccount.aspx.111!)</asp:Literal></span>
                                            <asp:CheckBox ID="copyBillingInfo" runat="server" />
                                            <span class="checkmark"></span>
                                        </label>
                                    </span>
                                    <div class="shipping-section-clears"></div>
                                    <div id="shipping-info-place-holder">
                                        <uc:ShippingAddressControl ID="ShippingAddressControl" IdPrefix="shipping-" runat="server" />
                                    </div>
                                </div>
                                <div style="clear: both;"></div>
                                <div class="form-controls-place-holder i-hide">
                                    <span class="form-controls-span label-outside" id="age-13-place-holder">
                                        <asp:CheckBox ID="chkOver13" runat="server" />
                                        <span class="checkbox-captions custom-font-style">
                                            <asp:Literal ID="litImOver13" runat="server">(!createaccount.aspx.143!)</asp:Literal></span>
                                    </span>
                                </div>
                                <!-- Captcha Section Starts Here -->
                                <div class="form-controls-place-holder captcha-section" style="padding: 30px 0 10px; text-align: right;">
                                    <asp:Label ID="Label1" runat="server" Text="(!signin.aspx.18!)" Visible="true" Style="font-size: 15px;"></asp:Label>
                                    <asp:TextBox ID="txtCaptcha" runat="server" class="light-style-input"></asp:TextBox>
                                </div>
                                <div id="create-account-captcha-wrapper" class="form-controls-place-holder  captcha-section" style="text-align: right;">
                                    <div id="captcha-image">
                                        <img alt="captcha" src="Captcha.ashx?id=1" id="captcha" />
                                    </div>
                                    <div id="captcha-refresh">
                                        <a href="javascript:void(1);" id="captcha-refresh-button" alt="Refresh Captcha" title="Click to change the security code"></a>
                                    </div>
                                </div>
                                <!-- Captcha Section Ends Here -->
                                <div class="clr height-5"></div>
                                <div class="height-10"></div>
                                <div class="height-13"></div>
                                <div>
                                    <div id="account-form-button-place-holder">
                                        <div id="save-account-button">
                                            <div id="save-account-loader"></div>
                                            <div id="save-account-button-place-holder" style="text-align: right;">
                                                <input type="button" id="create-customer-account"
                                                    class="site-button createaccount-primary-button content"
                                                    data-contentkey="<%= CommonLogic.IIF(CommonLogic.QueryStringBool("checkout"),  "createaccount.aspx.25",  "createaccount.aspx.24")%>"
                                                    data-contenttype="string resource"
                                                    data-contentvalue="<%= CommonLogic.IIF(CommonLogic.QueryStringBool("checkout"),  AppLogic.GetString("createaccount.aspx.25", true),  
                                            AppLogic.GetString("createaccount.aspx.24", true))%>"
                                                    value="<%= CommonLogic.IIF(CommonLogic.QueryStringBool("checkout"),  AppLogic.GetString("createaccount.aspx.25", true),  
                                            AppLogic.GetString("createaccount.aspx.24", true))%>" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="display-none">
                                        <asp:Button ID="btnCreateAccount" runat="server" Text="" OnClick="btnCreateAccount_Click" />
                                        <asp:TextBox ID="billingTxtCityStates" runat="server"></asp:TextBox>
                                        <asp:TextBox ID="shippingTxtCityStates" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="checkoutanon-not-registered createaccount-bottom-section">
                            <div style="background: #86d6a3; color: #449060; border-radius: 5px; padding: 30px 20px 20px;">
                                <span class="signinheaderlabel">Why create an account?</span>
                                <div style="border-top: 1px solid #449060; margin-top: 15px; padding: 10px 0;"></div>
                                <div class="signinperks">
                                    <ul>
                                        <li>
                                            <i class="fa fa-check"></i><span>Save your re-order and purchase</span>
                                            <span style="padding-left: 22px;">history</span>
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i><span>Faster checkout process</span>
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i><span>VIP exclusive offers</span>
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i><span>Check the status of your order</span>
                                        </li>
                                        <li>
                                            <i class="fa fa-check"></i><span>Store multiple shipping addresses</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <!--Log In Form Section Ends Here -->
                        <div class="clr height-12"></div>
                        <div class="clr height-12"></div>
                    </asp:Panel>

                    <asp:Panel ID="ExecutePanel" runat="server" Width="90%">
                        <div align="center">
                            <img src="images/spacer.gif" alt="" width="100%" height="40" />
                            <b>
                                <asp:Literal ID="SignInExecuteLabel" runat="server"></asp:Literal></b>
                        </div>
                    </asp:Panel>
                    <asp:CheckBox ID="DoingCheckout" runat="server" Visible="False" />
                    <asp:Label ID="ReturnURL" runat="server" Text="default.aspx" Visible="False" />
                    <div class="clear-both height-12 accounts-clear"></div>
                </div>
            </div>












        </asp:Panel>

        <%-- 
        do not touch the following html script, the following elements are used in overriding postal listing dialog assignment of values to city and zip
        see jscripts/jquery.cbe.address.verification.js updateAddressInputValues function
        
        --%>

        <div id="submit-case-caption" style="display: none;">
            <asp:Literal ID="LtrCreateAccount_Caption" runat="server"></asp:Literal>
        </div>
        <div style="display: none; margin: auto" title="Address Verification" id="ise-address-verification-for-create-account"></div>

        <input type="hidden" id="load-at-page" value="create-account" />
        <%--<script type="text/javascript" src="jscripts/minified/address.control.js"></script>--%>
        <script type="text/javascript" src="jscripts/jquery/address.control.js"></script>
        <%--<script type="text/javascript" src="jscripts/minified/address.verification.js"></script>--%>
        <script type="text/javascript" src="jscripts/jquery/address.verification.js"></script>
        <%--<script type="text/javascript" src="jscripts/minified/customer.js"></script>--%>
        <%--<script type="text/javascript" src="skins/Skin_(!SKINID!)/jscripts/jquery/customer.js"></script>--%>
        <script type="text/javascript" src="jscripts/jquery/customer.js"></script>
        <script type="text/javascript">
    <!-- reference path : /component/address-verificatio/real-time-address-verification-plugin -->
    $(window).load(function () {
        var basePlugin = new jqueryBasePlugin();
        basePlugin.downloadPlugin('components/address-verification/setup.js', function () {

            var loader = new realtimeAddressVerificationPluginLoader();
            loader.start(function (config) {

                var $plugin = $.fn.RealTimeAddressVerification;

                config.submitButtonID = "btnCreateAccount";
                config.isAllowShipping = $plugin.toBoolean(ise.Configuration.getConfigValue("AllowShipToDifferentThanBillTo"));
                config.addressMatchDialogContainerID = "ise-address-verification-for-create-account";
                config.errorContainerId = "errorSummary";
                config.progressContainterId = "save-account-loader";
                config.buttonContainerId = "save-account-button-place-holder";
                config.isWithShippingAddress = $plugin.toBoolean(ise.Configuration.getConfigValue("AllowShipToDifferentThanBillTo"));
                config.billingInputID = {
                    POSTAL_CODE: "BillingAddressControl_txtPostal",
                    CITY: "BillingAddressControl_txtCity",
                    STATE: "BillingAddressControl_txtState",
                    COUNTRY: "BillingAddressControl_drpCountry",
                    STREET_ADDRESS: "BillingAddressControl_txtStreet",
                    CITY_STATE_SELECTOR: "billing-city-states"
                };

                config.billingLabelID = {
                    POSTAL_CODE: "BillingAddressControl_lblStreet",
                    CITY: "BillingAddressControl_lblCity",
                    STATE: "BillingAddressControl_lblState",
                    STREET_ADDRESS: "BillingAddressControl_lblPostal"
                };

                config.shippingInputID = {
                    POSTAL_CODE: "ShippingAddressControl_txtPostal",
                    CITY: "ShippingAddressControl_txtCity",
                    STATE: "ShippingAddressControl_txtState",
                    COUNTRY: "ShippingAddressControl_drpCountry",
                    STREET_ADDRESS: "ShippingAddressControl_txtStreet",
                    RESIDENCE_TYPE: "ShippingAddressControl_drpType",
                    CITY_STATE_SELECTOR: "shipping-city-states"
                };

                config.shippingLabelID = {
                    POSTAL_CODE: "ShippingAddressControl_lblStreet",
                    CITY: "ShippingAddressControl_lblCity",
                    STATE: "ShippingAddressControl_lblState",
                    STREET_ADDRESS: "ShippingAddressControl_lblPostal"
                };



                var realTimeAddressVerificationPluginStringKeys = new Object();

                realTimeAddressVerificationPluginStringKeys.unableToVerifyAddress = "createaccount.aspx.114";
                realTimeAddressVerificationPluginStringKeys.confirmCorrectAddress = "createaccount.aspx.115";
                realTimeAddressVerificationPluginStringKeys.useBillingAddressProvided = "createaccount.aspx.116";
                realTimeAddressVerificationPluginStringKeys.useShippingAddressProvided = "createaccount.aspx.117";
                realTimeAddressVerificationPluginStringKeys.selectMatchingBillingAddress = "createaccount.aspx.118";
                realTimeAddressVerificationPluginStringKeys.selectMatchingShippingAddress = "createaccount.aspx.119";
                realTimeAddressVerificationPluginStringKeys.gatewayErrorText = "createaccount.aspx.152";
                realTimeAddressVerificationPluginStringKeys.progressText = "createaccount.aspx.153";

                config.stringResourceKeys = realTimeAddressVerificationPluginStringKeys;

                $plugin.setup(config);
            });
        });
    });
        </script>
        <%-- do not touch <-- --%>
    </form>
</body>
</html>
