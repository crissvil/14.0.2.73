<%@ Page Language="C#" AutoEventWireup="true" CodeFile="heat-transfer.aspx.cs" Inherits="InterpriseSuiteEcommerce.heat_transfer" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Heat Transfer</title>
</head>
<body>
	<div class="clear-both height-5">&nbsp;</div>
	<form id="frmPrint" runat="server" action=>
	<asp:Panel ID="pnlPageContentWrapper" runat="server">
		<h1>Heat Transfer</h1>
        <div class="content-box">
            <div class="col-lg-6 col-md-12 col-sm-12">
                <h3 style="text-align:center;font-size:18px;">HEAT TRANSFER (BACKGROUND SCREEN PRINT)</h3>
				<center>
				<img class="i-fancy-image" data-src="i-fancy-image-hidden-1" src="images/heat-transfer/sample-1.png" style="width:90%"/>
				<div class="i-hide" id="i-fancy-image-hidden-1">
					<img  src="images/heat-transfer/poplet-1.jpg" style="width:100%;height:518px;"/>
				</div>
				<br/>
				<a href="javascript:void(1);" class="i-fancy-image" data-src="i-fancy-image-hidden-1" >Click image to enlarge</a>
				</center>
				<br/>
				<p style="font-size:16px">Can achieve a photo quality result for smaller areas and may be combined with screen print.</p>
            </div>
            <div class="col-lg-6 col-md-12 col-sm-12">
                <h3 style="text-align:center;font-size:18px;">HEAT TRANSFER (LARGE COVERAGE)</h3>
				<center>
				<img class="i-fancy-image" data-src="i-fancy-image-hidden-2"  src="images/heat-transfer/sample-2.png" style="width:90%"/>
				<div class="i-hide" id="i-fancy-image-hidden-2" >
					<img src="images/heat-transfer/poplet-2.jpg" style="width:100%;height:518px;"/>
				</div>
				<br/>
				<a href="javascript:void(1);" class="i-fancy-image" data-src="i-fancy-image-hidden-2" >Click image to enlarge</a>
				</center>
				<br/>
				<p style="font-size:16px">Maximum size heat transfer for photo quality print not covering the whole bag. Entire bag coverage must use lamination.</p>
            </div>
		<p class="i-text-clear-5"></p>   
        </div>
	</asp:Panel>
	</form>
</body>
</html>