// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Web.UI;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using InterpriseSuiteEcommerceGateways;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerce
{
    public partial class checkoutanon : SkinBase
    {
        #region Declaration

        private string PaymentMethod = String.Empty;
        private InterpriseShoppingCart cart;
        private string _checkoutType = String.Empty;
        private IAuthenticationService _authenticationService = null;
        private IStringResourceService _stringResourceService = null;
        private INavigationService _navigationService = null;
        private IAppConfigService _appConfigService = null;

        #endregion

        #region Methods

        private void RegisterDomainServices()
        {
            _authenticationService = ServiceFactory.GetInstance<IAuthenticationService>();
            _stringResourceService = ServiceFactory.GetInstance<IStringResourceService>();
            _navigationService = ServiceFactory.GetInstance<INavigationService>();
            _appConfigService = ServiceFactory.GetInstance<IAppConfigService>(); 
        }

        private void InitializePageContent()
        {
            btnSignInAndCheckout.Text = _stringResourceService.GetString("checkoutanon.aspx.12", true);
            RegisterAndCheckoutButton.Text = _stringResourceService.GetString("checkoutanon.aspx.13", true);
            Skipregistration.Text = _stringResourceService.GetString("checkoutanon.aspx.14", true);
            litPromo.Text = _stringResourceService.GetString("custom.text.27", true);

            if (ThisCustomer.IsInEditingMode())
            {
                AppLogic.EnableButtonCaptionEditing(btnSignInAndCheckout, "checkoutanon.aspx.12");
                AppLogic.EnableButtonCaptionEditing(RegisterAndCheckoutButton, "checkoutanon.aspx.13");
                AppLogic.EnableButtonCaptionEditing(Skipregistration, "checkoutanon.aspx.14");
            }
        }

        private void BindControls()
        { 
            btnSignInAndCheckout.Click += btnSignInAndCheckout_Click;
            RegisterAndCheckoutButton.Click += RegisterAndCheckoutButton_Click;
            Skipregistration.Click += Skipregistration_Click;
            RequestPassword.Click += RequestPassword_Click;
        }

        #endregion

        #region Events

        protected override void OnInit(EventArgs e)
        {
            BindControls();
            RegisterDomainServices();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.PageNoCache();
            this.RequireSecurePage();
            this.ThisCustomer.RequireCustomerRecord();

            _checkoutType = "checkoutType".ToQueryString();
            SectionTitle = _stringResourceService.GetString("checkoutanon.aspx.1", true);
            RequestPassword.Text = _stringResourceService.GetString("signin.aspx.15", true).ToUpper();

            cart = new InterpriseShoppingCart(base.EntityHelpers, SkinID, ThisCustomer, CartTypeEnum.ShoppingCart, String.Empty, false, true);
            if (cart.IsEmpty())
            {
                _navigationService.NavigateToShoppingCartRestLinkBack();
            }

            if (cart.InventoryTrimmed)
            {
                _navigationService.NavigateToShoppingCartRestLinkBackWithErroMessage(_stringResourceService.GetString("shoppingcart.aspx.1", true));
            }

            if (!cart.MeetsMinimumOrderAmount(AppLogic.AppConfigUSDecimal("CartMinOrderAmount")))
            {
                _navigationService.NavigateToShoppingCartRestLinkBack();
            }

            if (!cart.MeetsMinimumOrderWeight(AppLogic.AppConfigUSDecimal("MinOrderWeight")))
            {
                _navigationService.NavigateToShoppingCartRestLinkBack();
            }

            if (!cart.MeetsMinimumOrderQuantity(AppLogic.AppConfigUSInt("MinCartItemsBeforeCheckout")))
            {
                _navigationService.NavigateToShoppingCartRestLinkBack();
            }

            if (AppLogic.AppConfigBool("PasswordIsOptionalDuringCheckout"))
            {
                PasswordOptionalPanel.Visible = true;
            }

            ErrorMsgLabel.Text = String.Empty;
            if (!IsPostBack)
            {
                InitializePageContent();
                var rememberMeCustomer = _authenticationService.GetRememberMeInfo();
                if (rememberMeCustomer != null)
                {
                    EMail.Text = rememberMeCustomer.Email;
                    this.Password.Attributes.Add("value", rememberMeCustomer.DecryptedPassword);
                    this.PersistLogin.Checked = true;
                }
                else
                {
                    EMail.Text = String.Empty;
                    Password.Text = String.Empty;
                }
            }

            if (AppLogic.AppConfigBool("SecurityCodeRequiredOnStoreLogin"))
            {
                // Create a random code and store it in the Session object.
                pnlSecurityCode.Visible = true;
                if (!IsPostBack)
                    SecurityImage.ImageUrl = "Captcha.ashx?id=1";
                else
                    SecurityImage.ImageUrl = "Captcha.ashx?id=2";
            }
        }

        protected void btnSignInAndCheckout_Click(object sender, EventArgs e)
        {
            Page.Validate();

            //if (!Page.IsValid) return;

            string EMailField = EMail.Text.ToLower();
            string PasswordField = Password.Text;

            if (AppLogic.AppConfigBool("SecurityCodeRequiredOnStoreLogin"))
            {
                string sCode = Session["SecurityCode"].ToString();
                string fCode = SecurityCode.Text;

                bool codeMatch = false;

                if (AppLogic.AppConfigBool("Captcha.CaseSensitive"))
                {
                    if (fCode.Equals(sCode))
                        codeMatch = true;
                }
                else
                {
                    if (fCode.Equals(sCode, StringComparison.InvariantCultureIgnoreCase))
                        codeMatch = true;
                }

                if (!codeMatch)
                {
                    ErrorMsgLabel.Text = _stringResourceService.GetString("signin.aspx.22", true)
                                                               .FormatWith(String.Empty, String.Empty);
                    ErrorPanel.Visible = true;                        
                    SecurityImage.ImageUrl = "Captcha.ashx?id=1";
                    return;
                }
            }

            var status = _authenticationService.Login(EMail.Text, PasswordField, true);
            if (!status.IsValid)
            {
                if (status.IsAccountExpired)
                {
                    ErrorMsgLabel.Text = _stringResourceService.GetString("signin.aspx.message.1", true);
                    lnkContactUs.Text = _stringResourceService.GetString("menu.Contact", true);
                    lnkContactUs.Visible = true;
                }
                else 
                {
                    ErrorMsgLabel.Text = _stringResourceService.GetString("signin.aspx.20", true);
                }
                
                ErrorPanel.Visible = true;
                return;
            }

            var customerWithValidLogin = _authenticationService.GetCurrentLoggedInCustomer();
            if (_checkoutType == "pp")
            {
                if (!cart.IsSalesOrderDetailBuilt)
                {
                    cart.BuildSalesOrderDetails();
                }
                customerWithValidLogin.ThisCustomerSession["paypalFrom"] = "checkoutanon";
                _navigationService.NavigateToUrl(PayPalExpress.CheckoutURL(cart));
            }
            else
            {
                FormPanel.Visible = false;
                ExecutePanel.Visible = true;
                SignInExecuteLabel.Text = _stringResourceService.GetString("signin.aspx.2", true);
                _navigationService.NavigateToShoppingCart() ;
            }
        }

        protected void RegisterAndCheckoutButton_Click(object sender, EventArgs e)
        {
            if (_checkoutType == "pp")
            {
                _navigationService.NavigateToUrl("createaccount.aspx?checkout=true&isAnonPayPal=true");
            }
            else
            {
                _navigationService.NavigateToUrl("createaccount.aspx?checkout=true");
            }
        }

        protected void Skipregistration_Click(object sender, EventArgs e)
        {
            if (_appConfigService.CheckoutUseOnePageCheckout && _appConfigService.PasswordIsOptionalDuringCheckout)
            {
                if (ThisCustomer.IsNotRegistered && !_appConfigService.AllowShipToDifferentThanBillTo)
                {
                    _navigationService.NavigateToUrl("createaccount.aspx?checkout=true&skipreg=true");
                }
                else
                {
                    _navigationService.NavigateToCheckout1();
                }
            }
            else
            {
                if (_checkoutType == "pp")
                {
                    _navigationService.NavigateToUrl("createaccount.aspx?checkout=true&skipreg=true&isAnonPayPal=true");
                }
                else
                {
                    _navigationService.NavigateToUrl("createaccount.aspx?checkout=true&skipreg=true");
                }
            }
        }

        protected void RequestPassword_Click(object sender, EventArgs e)
        {
            ErrorPanel.Visible = true; // that is where the status msg goes, in all cases in this routine

            //FireFox does not validate RequiredFieldValidator1.
            //This code will double check forgotemail has value.
            if (ForgotEMail.Text.Trim() == string.Empty)
            {
                ErrorMsgLabel.Text = AppLogic.GetString("signin.aspx.3", true);
                return;
            }

            //Decrypt connectionstring using salt & vector scheme implemented by Interprise.
            ErrorMsgLabel.Text = String.Empty;
            string PWD = String.Empty;
            bool passwordValid = true;
            string customerCode = String.Empty;
            string contactCode = String.Empty;
            bool exists = false;

            string sql = string.Format("SELECT EntityCode, cc.ContactCode, Password,PasswordSalt,PasswordIV FROM CRMContact cc WITH (NOLOCK) INNER JOIN EcommerceCustomerActiveSites ecas ON cc.ContactCode = ecas.ContactCode WHERE IsAllowWebAccess=1 AND UserName= {0} AND ecas.WebSiteCode = {1} AND ecas.IsEnabled = 1", DB.SQuote(ForgotEMail.Text.ToLower()), DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode));
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var rs = DB.GetRSFormat(con, sql))
                {
                    exists = rs.Read();
                    if (exists)
                    {
                        string pwdCypher = DB.RSField(rs, "Password");
                        string salt = DB.RSField(rs, "PasswordSalt");
                        string iv = DB.RSField(rs, "PasswordIV");
                        customerCode = DB.RSField(rs, "EntityCode");
                        contactCode = DB.RSField(rs, "ContactCode");

                        try
                        {
                            var tmpCrypto = new Interprise.Licensing.Base.Services.CryptoServiceProvider();
                            PWD = tmpCrypto.Decrypt(Convert.FromBase64String(pwdCypher),
                                                    Convert.FromBase64String(salt),
                                                    Convert.FromBase64String(iv));
                        }
                        catch
                        {
                            passwordValid = false;
                        }
                    }
                    else
                    {
                        ErrorMsgLabel.Text = AppLogic.GetString("lostpassword.aspx.4", true);
                        return;
                    }
                }
            }

            if (exists && !passwordValid)
            {
                byte[] salt = InterpriseHelper.GenerateSalt();
                byte[] iv = InterpriseHelper.GenerateVector();

                string newPassword = Guid.NewGuid().ToString("N").Substring(0, 8);
                string newPasswordCypher = InterpriseHelper.Encryption(newPassword, salt, iv);

                string saltBase64 = Convert.ToBase64String(salt);
                string ivBase64 = Convert.ToBase64String(iv);

                DB.ExecuteSQL("UPDATE CRMContact SET Password = {0}, PasswordSalt = {1}, PasswordIV = {2} WHERE EntityCode = {3} AND ContactCode = {4}", DB.SQuote(newPasswordCypher), DB.SQuote(saltBase64), DB.SQuote(ivBase64), DB.SQuote(customerCode), DB.SQuote(contactCode));

                PWD = newPassword;
            }

            if (PWD.Length != 0)
            {
                string FromEMail = AppLogic.AppConfig("MailMe_FromAddress");
                string EMail = ForgotEMail.Text;
                bool SendWasOk = false;
                try
                {
                    string WhoisRequestingThePassword = "\r\n" + ThisCustomer.LastIPAddress + "\r\n" + DateTime.Now.ToString();
                    string MsgBody = string.Empty;

                    MsgBody = InterpriseHelper.GetPasswordEmailTemplate(EMail);
                    if (MsgBody.Length > 0)
                    {
                        AppLogic.SendMail(AppLogic.AppConfig("StoreName") + " " + AppLogic.GetString("lostpassword.aspx.5", true), MsgBody, true, FromEMail, FromEMail, EMail, EMail, "", AppLogic.AppConfig("MailMe_Server"));
                        SendWasOk = true;
                    }
                    else
                    {
                        ErrorMsgLabel.Text = AppLogic.GetString("lostpassword.aspx.4", true);
                    }
                }
                catch { }
                if (SendWasOk)
                {
                    ErrorMsgLabel.Text = AppLogic.GetString("lostpassword.aspx.2", true);
                }
                else
                {
                    ErrorMsgLabel.Text = AppLogic.GetString("lostpassword.aspx.3", true);
                }
            }
            else
            {
                ErrorMsgLabel.Text = AppLogic.GetString("lostpassword.aspx.4", true);
            }
        }

        #endregion
    }
}
