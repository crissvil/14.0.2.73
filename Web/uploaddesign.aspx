﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="uploaddesign.aspx.cs" Inherits="InterpriseSuiteEcommerce.uploaddesign" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload your own design</title>
    <script type="text/javascript" src="jscripts/jquery/jquery.validate.js"></script>
</head>
<body>
    <asp:Panel runat="server" ID="pnlContent">
        <form id="frmUploadDesign" runat="server">
            <img src="skins/Skin_(!SKINID!)/images/Express-Print-Design-Bag-for-Me-Banner-A.png" />
            <h1>Upload your own design</h1>
            <p>
                Have your own design? Just upload a pdf or eps to our specifications and let us do the rest. A soft copy proof will be sent to you for your approval.
            </p>
            <div class="height-5"></div>
            <p>
                Before you upload does your artwork match these specifications?
            </p>
            <div class="height-5"></div>
            <p>
                <asp:Literal ID="litItemInfo" runat="server" />
            </p>
            <div class="height-5"></div>
            <p>
                An artwork recreation fee may be charged if the artwork supplied is of poor quality or not one of our approved file types.
            </p>
            <div class="height-5"></div>
            <p>
                If you have any trouble uploading the artwork contact us on <b>1300 874 559</b>
            </p>
            <div class="height-5"></div>
            <p>
                Use the following form to upload your artwork.
            </p>
            <div>
                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-comments">
                        <label for="CAT_Custom_201134">Comments</label>
                        <br />
                        <textarea onkeydown="if(this.value.length>=4000)this.value=this.value.substring(0,3999);" class="cat_listbox light-style-input" rows="4" cols="23" id="Comments" name="Comments" style="width: 100%;"></textarea>
                    </span>
                </div>
                <div class="clear-both height-17"></div>
                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-comments">
                        <label for="CAT_Custom_201134">Attach File<font style="color: red">*</font> (25mb Limit)</label>
                        <br />
                        <asp:FileUpload ID="fileUploadImage" runat="server" accept=".pdf, .eps, .jpg, .jpeg, .tif" />
                    </span>
                </div>
                <div class="clear-both height-25"></div>
                <div class="clear-both height-25"></div>
                <div class="submit-button-place-holder">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="shoppingcart-primary-button site-button" OnClick="btnSubmit_Click" />
                </div>
                <div class="clear-both height-25"></div>
                <div class="clear-both height-25"></div>
                <div class="submit-button-place-holder">
                    <asp:Button ID="btnDesignYourOwn" runat="server" Text="DESIGN YOUR OWN" CssClass="shoppingcart-secondary-button site-button" OnClick="btnDesignYourOwn_Click" />
                </div>
                <div class="clear-both height-12"></div>
                <div class="submit-button-place-holder">
                    <asp:Button ID="btnDesignForYou" runat="server" Text="LET US DESIGN FOR YOU" CssClass="shoppingcart-secondary-button site-button" OnClick="btnDesignForYou_Click" />
                </div>
                <div class="height-20"></div>
            </div>
        </form>
        <script type="text/javascript">
            var uploadParam = { Filename: "", Path: "", Comments: "" };
            var supportedExtensions = ["pdf", "eps", "jpg", "jpeg", "tif"];
            var imageFile = null;

            function sendFile(file) {
                var file = $('#fileUploadImage')[0].files[0];
                var filename = file.name;
                var fileExtension = filename.substr((filename.lastIndexOf('.') + 1));

                if ($.inArray(fileExtension, supportedExtensions) < 0) {
                    $("#btnSubmit").css('display', 'block');
                    ShowMessageBoxPopup("File type not supported. Please choose .pdf, .jpg, .eps & .tif");
                    return false;
                }
                return true;
            }

            $("#fileUploadImage").on('change', function () {

                var file;
                if ((file = this.files[0])) {
                    imageFile = file;
                }
            })

            $("#btnSubmit").click(function () {
                $(this).css('display', 'none');
                $('#place-order-button-container').fadeIn('slow');
            });

            $(':submit').click(function () {
                var name = this.id;
                if (name == 'btnSubmit') {
                    if (imageFile != null || imageFile != undefined) {
                        return sendFile(imageFile);
                    }
                    else {
                        $("#btnSubmit").css('display', 'block');
                        ShowMessageBoxPopup("Please select a logo to upload first.");
                        return false;
                    }
                }
            });
        </script>
    </asp:Panel>
</body>
</html>
