﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="orderhistory.aspx.cs" Inherits="InterpriseSuiteEcommerce.orderhistory" %>

<%@ Import Namespace="InterpriseSuiteEcommerceCommon" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="skins/Skin_(!SKINID!)/jquery/jquery.tmpl.min.js"></script>
    <script type="text/javascript">
        var orderHistoryPluginStringKeys = new Object();
        orderHistoryPluginStringKeys.displayText = "account.aspx.43";
        orderHistoryPluginStringKeys.ofText = "account.aspx.29";
        orderHistoryPluginStringKeys.onText = "account.aspx.19";
        orderHistoryPluginStringKeys.orderDate = "account.aspx.12";
        orderHistoryPluginStringKeys.orderNotes = "account.aspx.16";
        orderHistoryPluginStringKeys.orderNumber = "account.aspx.11";
        orderHistoryPluginStringKeys.orderTotal = "account.aspx.15";
        orderHistoryPluginStringKeys.paymentMethod = "account.aspx.17";
        orderHistoryPluginStringKeys.paymentStatus = "account.aspx.13";
        orderHistoryPluginStringKeys.reorder = "account.aspx.22";
        orderHistoryPluginStringKeys.reorderPrompt = "account.aspx.26";
        orderHistoryPluginStringKeys.resetText = "account.aspx.44";
        orderHistoryPluginStringKeys.shippedText = "account.aspx.18";
        orderHistoryPluginStringKeys.shippingStatus = "account.aspx.14";
        orderHistoryPluginStringKeys.trackingNumber = "account.aspx.32";
        orderHistoryPluginStringKeys.viewing = "account.aspx.28";
    </script>
    <script type="text/javascript" src="components/order-history/portal.setup.js"></script>
</head>
<body>
    <form id="frmOrderHistory" runat="server">
        <div class="sections-place-holder no-padding">
            <div class="shoppingcart-section-header">
                <span>
                    <asp:Literal ID="litOrderHistory" runat="server">(!account.aspx.68!)</asp:Literal>
                </span>
            </div>

            <div class="section-content-wrapper">
                <asp:Literal ID="ltContactList" runat="server"/>
                <div id="AccountOrderHistory" style="border: none !important">
                    <div id="accountOrderHistoryLink" runat="server">Loading. Please wait...</div>
                    <div id="pnlOrderHistory"></div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
