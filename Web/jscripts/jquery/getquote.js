$(document).ready(function(){
    $(this).GetQuote.Initialize();
});

(function ($) {
    
    var thisGetQuotePlugIn;
    var securityCodeCounter = 1;

    var config = {};

    var global = {
        selected: '',
        selector: ''
    };

    var currentSelectedControl = null;

    var editorConstants = {
        EMPTY_VALUE: '',
        DOT_VALUE: '.'
    }
	
	var REQUIRED_QTY = 1;
	var ITEM_NAME 	 = "";
	var ITEM_CODE 	 = "";
	
    var defaults = {

        sendMessageButtonId              : 'send-message',
        sendMessageButtonPlaceHolderId   : "contact-form-button-place-holder",
        sendMessageProgressPlaceHolderId : "sending-message-progress-place-holder",
        sendMessageErrorPlaceHolderId    : "sending-message-error-place-holder",
        securityCodeRefreshButtonId      : "i-get-another-captcha",
        messages:
        {
            MESSAGE_SENDING_PROGRESS : 'Sending...',
        },
        confactFormControls:
        {
            CONTACT_NAME_ID    : 'txtContactName',
            EMAIL_ADDRESS_ID   : 'txtEmail',
            PHONE_ID           : 'txtContactNumber',
            SUBJECT_ID         : 'txtSubject',
            MESSAGE_DETAILS_ID : 'txtMessageDetails',
            CAPTCHA_ID         : 'txtCaptcha'
        },
        bubbleMessagePlaceHolderId              : 'ise-message-tips',
        bubbleMessageRequiresValidationClass    : '.requires-validation',
        requiredInputClass                      : 'required-input',
        objectOnFocusClass                      : 'current-object-on-focus'

    };

    var init = $.prototype.init;
    $.prototype.init = function (selector, context) {
        var r = init.apply(this, arguments);
        if (selector && selector.selector) {
            r.context = selector.context, r.selector = selector.selector;
        }
        if (typeof selector == 'string') {
            r.context = context || document, r.selector = selector;
            global.selector = r.selector;
        }
        global.selected = r;
        return r;
    }

    $.prototype.init.prototype = $.prototype;

    $.fn.GetQuote = {

        Initialize: function (options) {

            setConfig($.extend(defaults, options)); 

            thisGetQuotePlugIn = this;

            this.attachEventsListener();
            this.initializedBubbleMessage();
            this.setBubbleMessageRequiredStringResources();
			
			var min_order = $(selectorChecker("txtQuantity")).val();
	
			if( typeof min_order == undefined ){
				min_order = 1;
			}
			
			min_order = $.trim(min_order);
			
			if( min_order == "" ){
				min_order = 1;
			}else{
				min_order = parseInt( min_order );
			}
			
			if( min_order == 0 ){
				min_order = 1;
			}
			
			REQUIRED_QTY = min_order;
			ITEM_NAME = $(selectorChecker("i-item-selected")).html();
			ITEM_CODE = $(selectorChecker("i-item-code-selected")).html();
			
        },
        initializedBubbleMessage: function (){
            
			$(selectorChecker("txtFirstName")).ISEBubbleMessage({ "input-id": "txtFirstName", "label-id": "lblFirstName" });
			$(selectorChecker("txtLastName")).ISEBubbleMessage({ "input-id": "txtLastName", "label-id": "lblLastName" });
			$(selectorChecker("txtEmail")).ISEBubbleMessage({ "input-id": "txtEmail", "label-id": "lblEmail", "input-mode" : "email" });
			$(selectorChecker("txtMobile")).ISEBubbleMessage({ "input-id": "txtMobile", "label-id": "lblMobile", "optional" : true });
			$(selectorChecker("txtPhone")).ISEBubbleMessage({ "input-id": "txtPhone", "label-id": "lblPhone", "optional" : true  });
			$(selectorChecker("txtCompany")).ISEBubbleMessage({ "input-id": "txtCompany", "label-id": "lblCompany" });
			$(selectorChecker("txtAddress")).ISEBubbleMessage({ "input-id": "txtAddress", "label-id": "lblAddress", "optional" : true  });
			$(selectorChecker("txtCity")).ISEBubbleMessage({ "input-id": "txtCity", "label-id": "lblCity", "optional" : true  });
			$(selectorChecker("txtPostal")).ISEBubbleMessage({ "input-id": "txtPostal", "label-id": "lblPostal" });
			$(selectorChecker("txtMessageDetails")).ISEBubbleMessage({ "input-id": "txtMessageDetails", "optional": "lblMessageDetails" });
			$(selectorChecker("txtCaptcha")).ISEBubbleMessage({ "input-id": "txtCaptcha", "label-id": "lblCaptcha" });
			
        },
        attachEventsListener: function(){

            var config = getConfig();

            $(selectorChecker(config.securityCodeRefreshButtonId)).unbind('click');
            $(selectorChecker(config.securityCodeRefreshButtonId)).click(function () {
                  securityCodeCounter++;
                  $("#captcha").attr("src", "Captcha.ashx?id=" + securityCodeCounter);
            });
			
			var r = [47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 8];
			$(selectorChecker("txtQuantity")).unbind("keypress").keypress( function(e){
				 var $o = $(this);
				 var v = $o.val();

				 if( $.inArray( e.which, r ) < 1){
					 return false;
				 } 
				 
				$o.removeClass( "invalid-quantity");
				$("#ise-message-tips").fadeOut("slow");
				
			});
			
			$(selectorChecker("txtQuantity")).unbind("blur").blur( function(){
				
				var $o = $(this); 
				 
				var qty = $o.val();
				qty = $.trim( qty );

				var leftPosition  = $o.offset().left;
				var leftDeduction = 17;
				var topPosition   = $o.offset().top;
				var topDeduction  = 47;
				
				if( qty == "" ){
					
					$o.removeClass(config.objectOnFocusClass);
					$o.addClass("invalid-quantity");

					$("#ise-message-tips").css("top",  topPosition - topDeduction );
					$("#ise-message-tips").css("left", leftPosition - leftDeduction);

					$("#ise-message").html( "Please complete this required information.");
					$("#ise-message-tips").fadeIn("slow");
					
					return false;
				}
				
				qty = parseInt( qty );
				if( isNaN( qty ) || qty < 1 || qty < REQUIRED_QTY ){
					
					$o.removeClass(config.objectOnFocusClass);
					$o.addClass("invalid-quantity");

					$("#ise-message-tips").css("top",  topPosition - topDeduction );
					$("#ise-message-tips").css("left", leftPosition - leftDeduction);

					$("#ise-message").html( "The quantity you have selected is below the Minimum Order Requirement : " + REQUIRED_QTY );
					$("#ise-message-tips").fadeIn("slow");
				
					return false;
				
				}
				
			});
			
			var $msg_tips = $("#ise-message-tips");
			
			$(selectorChecker("drpCountry")).change(function(){
				var v = $.trim( $(this).val() );
				if( v != "" ){
					$("#i-country-selector").removeClass("required-input");
					$msg_tips.fadeOut("slow");
				}
			});

			$(selectorChecker("drpState")).change(function(){
				var v = $.trim( $(this).val() );
				if( v != "" ){
					$("#i-state-selector").removeClass("required-input");
					$msg_tips.fadeOut("slow");
				}
			});
			
        },
        setBubbleMessageRequiredStringResources: function(){

              var setBubbleMessageGlobalVariable = function(){}
              var keys = new Array();

              keys.push("customersupport.aspx.15");
              keys.push("customersupport.aspx.16");
              keys.push("customersupport.aspx.18");
              keys.push("createaccount.aspx.94");

              loadStringResource(keys, setBubbleMessageGlobalVariable);
        },
        validate: function(){
           
            var config  = getConfig();
            var formHasEmptyFields = false;
			
		
			var show_tips = function( top, left, message ){
				
				var $msg = $("#ise-message-tips");
					
				$msg.css("top",  top - 47 );
				$msg.css("left", left - 17 );

				$("#ise-message").html( message );
				$msg.fadeIn("slow");
					
			};
			
            var counter = 0;

            $(selectorChecker(config.bubbleMessagePlaceHolderId)).fadeOut("slow");
	
			var $input 	= $(selectorChecker("txtQuantity"));
			var qty 	= $input.val();
			qty = $.trim( qty );
			
			if( qty == "" ){
				
				$input.removeClass(config.objectOnFocusClass);
				$input.addClass("invalid-quantity");

				show_tips( $input.offset().top, $input.offset().left, "Please complete this required information." );
				
				return false;
			}
			
			qty = parseInt( qty );
			if( isNaN( qty ) || qty < 1 || qty < REQUIRED_QTY ){
				
				$input.removeClass(config.objectOnFocusClass);
				$input.addClass("invalid-quantity");

				show_tips( $input.offset().top, $input.offset().left,   "The quantity you have selected is below the Minimum Order Requirement : " + REQUIRED_QTY );
				
				return false;
			
			}
			
            $(selectorChecker(config.bubbleMessageRequiresValidationClass)).each(function () {
            
                var $this = $(this);
                if($this.val() == ""){

                    $this.removeClass(config.objectOnFocusClass);
                    $this.addClass(config.requiredInputClass);

                    if(counter == 0){

                        $this.addClass(config.objectOnFocusClass);
                        $this.focus();

                    }

                    formHasEmptyFields = true;
                    counter++;
                }

            });
			
			var state = $(selectorChecker("drpState")).val();
			state = $.trim( state );
			
			var $state_span = $("#i-state-selector");
			
			if( state == "" ){
				
			    //$state_span.addClass("required-input");
			    $("#drpState").addClass("required-input");
				
				if( !formHasEmptyFields ){
					show_tips( $state_span.offset().top, $state_span.offset().left,   "Please complete this required information." );
					formHasEmptyFields = true;
				}
				
			}else{
			    //$state_span.removeClass("required-input");
			    $("#drpState").removeClass("required-input");
			}
			
			//var country  = $(selectorChecker("drpCountry")).val();
			//country = $.trim( country );
			
			//var $country_span 	 = $("#i-country-selector");
			
			//if( country == "" ){
				
			//    $("#drpCountry").addClass("required-input");
				
			//	if( !formHasEmptyFields ){
			//		show_tips( $country_span.offset().top, $country_span.offset().left,   "Please complete this required information." );
			//		formHasEmptyFields = true;
			//	}
				
			//}else{
			//    $("#drpCountry").removeClass("required-input");
			//}
			
		
			if ( formHasEmptyFields ) {
                return false;
            }
			
			
			var $emailInputBox = $(selectorChecker( "txtEmail") );
			  
            if ($emailInputBox.hasClass("invalid-email") ) {
                $emailInputBox.focus();
                return false;
            }
		
			postQuote();
            return false;
        }
    }  

    function setConfig(value) {
        config = value;
    }

    function getConfig() {
        return config;
    }

    function selectorChecker(selector) {
        if (selector == editorConstants.EMPTY_VALUE) return selector;

        if (selector.indexOf(editorConstants.DOT_VALUE) == -1) {
            selector = "#" + selector;
        }
        return selector;
    }

    function loadStringResource(keys, callBack) {

        ise.StringResource.loadResources(keys, callBack);
    }
	
	function postQuote(){
		
		var fn 	= $(selectorChecker("txtFirstName")).val();
		fn 	= $.trim( fn );
		
		var ln 	= $(selectorChecker("txtLastName")).val();
		ln	= $.trim( ln );
		
		var email = $(selectorChecker("txtEmail")).val();
		email = $.trim( email );
		
		//var mobile 	= $(selectorChecker("txtMobile")).val();
	    //mobile = $.trim( mobile );
		var mobile = '';
		
		var phone 	= $(selectorChecker("txtPhone")).val();
		phone = $.trim( phone );
		
		var company = $(selectorChecker("txtCompany")).val();
		company = $.trim( company );
		
		//var address = $(selectorChecker("txtAddress")).val();
	    //address = $.trim( address );
		var address = '';
		
		//var city 	= $(selectorChecker("txtCity")).val();
	    //city = $.trim( city );
		var city = '';
		
		var postal 	= $(selectorChecker("txtPostal")).val();
		postal = $.trim( postal );
		
		//var country  = $(selectorChecker("drpCountry")).val();
		//country 	 = $.trim( country );
		var country = 'Australia';
		
		var state 	 = $(selectorChecker("drpState")).val();
		state 		 = $.trim( state );
		
		var msg  	 = $(selectorChecker("txtMessageDetails")).val();
		msg = $.trim( msg );
		
		var captcha  = $(selectorChecker("txtCaptcha")).val();
		captcha = $.trim( captcha );
		
		var item_code 	= $.trim( ITEM_CODE );
		var item_name 	= $.trim( ITEM_NAME );
		var item_color 	= $(selectorChecker("i-fabric-selected")).html();
		item_color 		= $.trim( item_color );
		var printed_color = $(selectorChecker("drpPrintedColours")).val();
		var qty 		= $(selectorChecker("txtQuantity")).val();
		
		var data = { 
			captcha 	: captcha,
			fn 			: fn,
			ln 			: ln,
			email 		: email,
			mobile 		: mobile,
			phone 		: phone,
			company 	: company,
			address 	: address,
			city 		: city,
			postal 		: postal,
			state 		: state,
			country 	: country,
			code 		: item_code,
			name 		: item_name,
			color  		: item_color,
			qty 		: qty,
			msg         : msg,
            printed     : printed_color
		}
		
		var $progress = $(".i-process-message");
		var $buttons  = $(".i-btn-container");
		
		var successCallback = function (result) {
			var msg = $.trim( result.d );
			if( msg == "invalid-quantity"){
				return false;
			}

			if (msg == "email-duplicates") {
			    $progress.fadeOut("slow", function(){
			        $buttons.fadeIn("slow");
			    $("#txtEmail").addClass("email-duplicates");
			    $("#txtEmail").focus();
			    });
			    return false;
			}

			if (msg == "invalid-email") {
			    $progress.fadeOut("slow", function(){
			        $buttons.fadeIn("slow");
			    $("#txtEmail").addClass("invalid-email");
			    $("#txtEmail").focus();
			    });
			    return false;
			}
			
			$c = $(selectorChecker("txtCaptcha"));
		
			if( msg == "invalid-captcha" ){
				$progress.fadeOut("slow", function(){
					$buttons.fadeIn("slow");
					$c.addClass("invalid-captcha");
					$c.focus();
				});
				return false;
			}
			
			if( msg == "null-captcha" ){
				$progress.fadeOut("slow", function(){
					$buttons.fadeIn("slow");
					$c.addClass("invalid-captcha");
					$c.focus();
				});
				return false;
			}
			
			if( msg == "invalid-item-code" ){
				$progress.fadeOut("slow", function(){
					$buttons.fadeIn("slow");
				});
				return false;
			}
			
			location.href = result.d;
			
		}

		var failedCallback = function (result) {
		    $("#error-message").removeClass("i-hide")
		    $progress.fadeOut("slow", function () {
		        $buttons.fadeIn("slow");
		    });
		}

		$buttons.fadeOut("slow", function(){
			
			$progress.fadeIn("slow", function(){
				 AjaxCallCommon("ActionService.asmx/PostQuote", data, successCallback, failedCallback);
			});
			
		});
		
	}


  })(jQuery);

  function formInfoIsGood(){  
       return $(this).GetQuote.validate();
  }