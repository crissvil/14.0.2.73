﻿$(document).ready(function(){
     $(this).ContactUs.Initialize();
});

(function ($) {
    
    var thisContactUsPlugIn;
    var securityCodeCounter = 1;

    var config = {};

    var global = {
        selected: '',
        selector: ''
    };

    var currentSelectedControl = null;

    var editorConstants = {
        EMPTY_VALUE: '',
        DOT_VALUE: '.'
    }

    var defaults = {

        sendMessageButtonId              : 'send-message',
        sendMessageButtonPlaceHolderId   : "contact-form-button-place-holder",
        sendMessageProgressPlaceHolderId : "sending-message-progress-place-holder",
        sendMessageErrorPlaceHolderId    : "sending-message-error-place-holder",
        securityCodeRefreshButtonId      : "i-get-another-captcha",
        messages:
        {
            MESSAGE_SENDING_PROGRESS : 'Sending...',
        },
        confactFormControls:
        {
            CONTACT_NAME_ID    : 'txtContactName',
            EMAIL_ADDRESS_ID   : 'txtEmail',
            PHONE_ID           : 'txtContactNumber',
            SUBJECT_ID         : 'txtSubject',
            MESSAGE_DETAILS_ID : 'txtMessageDetails',
            CAPTCHA_ID         : 'txtCaptcha'
        },
        bubbleMessagePlaceHolderId              : 'ise-message-tips',
        bubbleMessageRequiresValidationClass    : '.requires-validation',
        requiredInputClass                      : 'required-input',
        objectOnFocusClass                      : 'current-object-on-focus'

    };

    var init = $.prototype.init;
    $.prototype.init = function (selector, context) {
        var r = init.apply(this, arguments);
        if (selector && selector.selector) {
            r.context = selector.context, r.selector = selector.selector;
        }
        if (typeof selector == 'string') {
            r.context = context || document, r.selector = selector;
            global.selector = r.selector;
        }
        global.selected = r;
        return r;
    }

    $.prototype.init.prototype = $.prototype;

    $.fn.ContactUs = {

        Initialize: function (options) {

            setConfig($.extend(defaults, options)); 

            thisContactUsPlugIn = this;

            this.attachEventsListener();
            this.initializedBubbleMessage();
            this.setBubbleMessageRequiredStringResources();

        },
        initializedBubbleMessage: function (){
            
			$(selectorChecker("txtFirstName")).ISEBubbleMessage({ "input-id": "txtFirstName", "label-id": "lblFirstName" });
			$(selectorChecker("txtLastName")).ISEBubbleMessage({ "input-id": "txtLastName", "label-id": "lblLastName" });
			$(selectorChecker("txtEmail")).ISEBubbleMessage({ "input-id": "txtEmail", "label-id": "lblEmail" });
			$(selectorChecker("txtMobile")).ISEBubbleMessage({ "input-id": "txtMobile", "label-id": "lblMobile", "optional" : true });
			$(selectorChecker("txtPhone")).ISEBubbleMessage({ "input-id": "txtPhone", "label-id": "lblPhone", "optional" : true  });
			$(selectorChecker("txtCompany")).ISEBubbleMessage({ "input-id": "txtCompany", "label-id": "lblCompany" });
			$(selectorChecker("txtAddress")).ISEBubbleMessage({ "input-id": "txtAddress", "label-id": "lblAddress", "optional" : true  });
			$(selectorChecker("txtCity")).ISEBubbleMessage({ "input-id": "txtCity", "label-id": "lblCity", "optional" : true  });
			$(selectorChecker("txtPostal")).ISEBubbleMessage({ "input-id": "txtPostal", "label-id": "lblPostal" });
			$(selectorChecker("txtMessageDetails")).ISEBubbleMessage({ "input-id": "txtMessageDetails", "label-id": "lblMessageDetails" });
			$(selectorChecker("txtCaptcha")).ISEBubbleMessage({ "input-id": "txtCaptcha", "label-id": "lblCaptcha" });
			
        },
        attachEventsListener: function(){

            var config = getConfig();

            $(selectorChecker(config.securityCodeRefreshButtonId)).unbind('click');
            $(selectorChecker(config.securityCodeRefreshButtonId)).click(function () {
                  securityCodeCounter++;
                  $("#captcha").attr("src", "Captcha.ashx?id=" + securityCodeCounter);
            });

        },
        setBubbleMessageRequiredStringResources: function(){

              var setBubbleMessageGlobalVariable = function(){}
              var keys = new Array();

              keys.push("customersupport.aspx.15");
              keys.push("customersupport.aspx.16");
              keys.push("customersupport.aspx.18");

              loadStringResource(keys, setBubbleMessageGlobalVariable);

        },
        validate: function(){
           
            var config  = getConfig();
            var formHasEmptyFields = false;
         
            var counter = 0;

            $(selectorChecker(config.bubbleMessagePlaceHolderId)).fadeOut("slow");

            $(selectorChecker(config.bubbleMessageRequiresValidationClass)).each(function () {
            
                var $this = $(this);
                if($this.val() == ""){

                    $this.removeClass(config.objectOnFocusClass);
                    $this.addClass(config.requiredInputClass);

                    if(counter == 0){

                        $this.addClass(config.objectOnFocusClass);
                        $this.focus();

                    }

                    formHasEmptyFields = true;
                    counter++;
                }

            });

            var $emailInputBox = $(selectorChecker( "txtEmail") );

            if (formHasEmptyFields) {
                return false;
            }

            if ($emailInputBox.hasClass("invalid-email") ) {
                $emailInputBox.focus();
                return false;
            }

            return true;
        }
    }  

    function setConfig(value) {
        config = value;
    }

    function getConfig() {
        return config;
    }

    function selectorChecker(selector) {
        if (selector == editorConstants.EMPTY_VALUE) return selector;

        if (selector.indexOf(editorConstants.DOT_VALUE) == -1) {
            selector = "#" + selector;
        }
        return selector;
    }

    function loadStringResource(keys, callBack) {

        ise.StringResource.loadResources(keys, callBack);
    }

  })(jQuery);

  function formInfoIsGood(){  
       return $(this).ContactUs.validate();
  }