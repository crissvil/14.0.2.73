﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceGateways;
using System.Data;
using System.Data.SqlClient;
using InterpriseSuiteEcommerceControls.Validators;

namespace InterpriseSuiteEcommerce
{
    public partial class custom_bags_non_woven : SkinBase
    {
        #region Variable Declaration


        ICustomerService _customerService = null;
        IStringResourceService _stringResourceService = null;
        INavigationService _navigationService = null;
        IAppConfigService _appConfigService = null;
        IShoppingCartService _shoppingCartService = null;

        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
			RequireCustomerRecord();
            InitializeDomainServices();
            InitPageContent();
        }

        private void InitializeDomainServices()
        {
            _customerService = ServiceFactory.GetInstance<ICustomerService>();
            _stringResourceService = ServiceFactory.GetInstance<IStringResourceService>();
            _navigationService = ServiceFactory.GetInstance<INavigationService>();
            _appConfigService = ServiceFactory.GetInstance<IAppConfigService>();
            _shoppingCartService = ServiceFactory.GetInstance<IShoppingCartService>();
        }



        protected void InitPageContent()
        {

            string qry = string.Format("SELECT A.Counter, A.CategoryCode, WebDescription, Summary FROM SystemCategory A INNER JOIN SystemCategoryWebOptionDescription B ON A.CategoryCode = B.CategoryCode " +
            "WHERE ParentCategory IN ('Non Woven/Eco','Non-Woven/Fabric Eco Bags') AND WebSiteCode = {0} AND LanguageCode = {1} ORDER BY SortOrder ASC", DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode), DB.SQuote(ThisCustomer.LanguageCode));
            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader rs = DB.GetRSFormat(con, qry ))
                {
                    int counter = 0;
                    string str = string.Empty;
                    string resource = string.Empty;
                    string category = string.Empty;
                    string description = string.Empty;
                    string webDescription = string.Empty;

                    int i = 0;
                    while (rs.Read())
                    {
                        counter = rs.ToRSFieldInt("Counter");
                        category = rs.ToRSField("CategoryCode");
                        webDescription = "<div class=\"i-custom-woven-sub-header\">" + rs.ToRSField("Summary") + "</div>";
                        resource = "custom_woven_" + category.ToLower().Replace(" ", "");
                        description = _stringResourceService.GetString(resource);
                        if (description == resource)
                        {
                            description = "";
                        }
                        else
                        {
                            description = "<span class=\"i-custom-woven-sub-header\">" + description + "</span>";
                        }

                        qry = "  SELECT a.Counter, a.ItemCode, a.ItemName, a.ItemDescription, a.ItemType, a.ItemCode, b.FileName FROM InventoryItemView AS a ";
                        qry += " INNER JOIN InventoryItemWebOption c ON a.ItemCode = c.ItemCode INNER JOIN InventoryCategory d ON d.ItemCode = a.ItemCode ";
                        qry += " LEFT JOIN InventoryImageWebOptionDescription AS b ON a.ItemCode = b.ItemCode AND b.LanguageCode = a.LanguageCode ";
                        qry += " WHERE a.CategoryCode = '" + category + "' AND a.LanguageCode='English - Australia' AND  a.ItemType = 'Non-Stock' AND d.IsPrimary = 1";
                        qry += " AND (ISNULL(c.WebsiteGroup_C, '') = 'Smartbag' OR ISNULL(c.WebsiteGroup_C, '') = '')";
                        qry += " AND (ISNULL(c.NonStockMatrixType_C, '') != 'Child' OR ISNULL(c.NonStockMatrixType_C, '') = '')";
                        qry += " AND a.ItemStatus = 'active' AND c.Published = 1 AND c.WebSiteCode = '" + InterpriseHelper.ConfigInstance.WebSiteCode + "'";
                        qry += " ORDER BY d.SortOrder ASC ";

                        string div = "";
                        string seName = string.Empty;

                        using (SqlConnection con2 = DB.NewSqlConnection())
                        {
                            con2.Open();
                            using (IDataReader rs2 = DB.GetRSFormat(con2, qry))
                            {
                                while (rs2.Read())
                                {
                                    string itemCounter = rs2.ToRSFieldInt("Counter").ToString(), itemCode = rs2.ToRSField("ItemCode"), itemType = rs2.ToRSField("ItemType");
                                    string defaultNonStockMatrix = AppLogic.GetNonStockMatrixDefault(itemCounter, itemCode, itemType);
                                    seName = SE.MungeName(rs2.ToRSField("ItemDescription"));
                                    string items = "<div class='product-small-1'>";
                                        items += "<div>";
                                        items += "<div class='product-small-1-container'>";
                                        items += "<a  href='p-" + itemCounter + "-" + seName + ".aspx' style='bottom: 0; left: -20px; margin: 0px;'>";
			                                        items += "<div id='pnlEntityImage_653' align='center'>";

                                                    string img = rs2.ToRSField("FileName").Trim();
                                                    if (img == "")
                                                    {
                                                        img = "skins/skin_99/images/nopicture.gif";
                                                    }
                                                    else
                                                    {
                                                        img = "images/product/icon/" + img;
                                                    }

                                                    items += "<img id='imgEntity_653' class='content' src='" + img + "' alt='' border='0' title='" + rs2.ToRSField("ItemDescription")  + "'>";
			                                        items += "</div>";
			                                     items += "</a>";
		                                    items += "</div>";
                                        items += "</div>";
                                        items += "<div class='item-title'>";
                                        items += "<a href='p-" + itemCounter + "-" + seName + ".aspx'>" + rs2.ToRSField("ItemDescription") + "</a>";
	                                    items += "</div>";
                                        //items += "<br/>";
                                        items += "</div>";
                                        items += "<div class='product-small-1-buttons i-no-padding'>";
                                        //items += "<a href='get-quote.aspx?id=" + defaultNonStockMatrix + "' id='quote-btn' style='padding-right:10px;'>";
                                        //items += "<span class='badge entity-getquote-button'>GET QUOTE</span>";
                                        //    items += "</a>";
                                        items += "<a href='p-" + itemCounter + "-" + seName + ".aspx'>";
                                        //items += "<span class='badge entity-moreinfo-button'>GET QUOTE / MORE INFO</span></a>";
                                        items += "<span class='badge entity-getquote-button'>GET QUOTE / MORE INFO</span></a>";
                                    items += "</div>";
                                    div += "<li>" + items + "</li>";
                                }

                            }
                        }

                        //if (i > 0)
                        //{
                        //    str += "<li class='ï-custom-woven-list height-10'></li>";
                        //}
                        var title = category.ToLower().Replace(" ", "-");
                        var url = SE.MakeEntityLink("Category", counter.ToString(), title);
                        str += "<li class='ï-custom-woven-list'><p class='i-custom-woven-header'><a href="+ url + ">" + category + "</a></p>" + description + webDescription + "</li>";
                        str += "<li class='ï-custom-woven-list'><ul class='ï-custom-woven-items'>" + div + "</ul></li>";

                        i++;
                    }

                    listWrapper.InnerHtml = str;

                }
            }
		}
        

    }
}