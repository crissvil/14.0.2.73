<%@ Page Language="C#" AutoEventWireup="true" CodeFile="empty-cart.aspx.cs" Inherits="InterpriseSuiteEcommerce.empty_cart" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <ise:Topic ID="TopicEmptyCart" runat="server" TopicName="EmptyCartText" />
    </div>
    </form>
</body>
</html>
