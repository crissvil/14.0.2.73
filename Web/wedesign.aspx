﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="wedesign.aspx.cs" Inherits="InterpriseSuiteEcommerce.wedesign" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Let us design for you</title>
    <script type="text/javascript" src="jscripts/jquery/jquery.validate.js"></script>
</head>
<body>
    <asp:Panel runat="server" ID="pnlContent">
        <form id="frmWeDesign" runat="server">
            <img src="skins/Skin_(!SKINID!)/images/Express-Print-Design-Bag-for-Me-Banner-A.png" />
            <h1>Let us design for you</h1>
            <p>
                Dont have a design? Have no time to design your own? For a fee of $95 we will give you a professional design in a few days. Simply fill out the brief form, sit back and let us do the rest. 
            </p>
            <div class="height-5"></div>
            <p>
                If you have any trouble uploading the artwork contact us on 1300 874 559
            </p>
            <div class="height-5"></div>
            <p>
                Use the following form to upload your artwork.
            </p>
            <div class="height-5"></div>
            <h2>Design Brief</h2>
            <div class="height-5"></div>
            <p>
                <b>Question 1:</b> What is the purpose of your business/company and what products/services do you provide?
            </p>
            <div class="height-5"></div>
            <p>
                <b>Question 2:</b> Design Look & Feel. Inspiration - The more clues you can provide about your design tastes, the more likely we will be able to produce what you want! As professional designers we will not just copy the reference ideas you send - but will use it as the start of the design process. Can you provide any examples of design looks/feels you like? Other examples of logos, stickers, business cards, or flyers? 
            </p>
            <div class="height-5"></div>
            <p>
                <b>Question 3:</b> What colour/imagery/font style/atmosphere, mood or feel? 
            </p>
            <div class="height-5"></div>
            <p>
                <b>Question 4:</b> What is the primary message you want to communicate? Do you want to add content such as a Website address, Contact details, your Slogan?
            </p>
            <div class="height-5"></div>
            <p>
                Please tick any of the following keywords which best suit the message you want to communicate:
            </p>
            <div class="height-5"></div>
            <div class="checkbox-section">
                <input type="checkbox" name="professional" value="professional"/> professional<br/>
                <input type="checkbox" name="contemporary" value="contemporary"/> contemporary<br/>
                <input type="checkbox" name="traditional" value="traditional"/> traditional<br/>
                <input type="checkbox" name="friendly" value="friendly"/> friendly<br/>
                <input type="checkbox" name="serious" value="serious"/> serious<br/>
            </div>
            <div class="checkbox-section">
                <input type="checkbox" name="casual" value="casual"/> casual<br/>
                <input type="checkbox" name="cutting_edge" value="cutting_edge"/> cutting edge<br/>
                <input type="checkbox" name="affordable" value="affordable"/> affordable<br/>
                <input type="checkbox" name="cheap" value="cheap"/> cheap<br/>
                <input type="checkbox" name="expensive" value="expensive"/> expensive<br/>
            </div>
            <div class="checkbox-section">
                <input type="checkbox" name="luxury" value="luxury"/> luxury<br/>
                <input type="checkbox" name="exclusive" value="exclusive"/> exclusive<br/>
                <input type="checkbox" name="personal" value="personal"/> personal<br/>
                <input type="checkbox" name="mass_market" value="mass_market"/> mass market<br/>
                <input type="checkbox" name="small_business" value="small_business"/> small business<br/>
            </div>
            <div class="checkbox-section">
                <input type="checkbox" name="big_business" value="big_business"/> big business<br/>
                <input type="checkbox" name="corporate" value="corporate"/> corporate<br/>
                <input type="checkbox" name="mature" value="mature"/> mature<br/>
                <input type="checkbox" name="distinguished" value="distinguished"/> distinguished<br/>
                <input type="checkbox" name="approachable" value="approachable"/> approachable<br/>
            </div>
            <div class="height-10 clear"></div>
            <div class="height-5"></div>
            <p>
                Please tick any of the following keywords which best suit the message you want to communicate:
            </p>
            <div class="height-5"></div>
            <div class="checkbox-section">
                <input type="checkbox" name="serene" value="professional"/> serene<br/>
                <input type="checkbox" name="spiritual" value="spiritual"/> spiritual<br/>
                <input type="checkbox" name="powerful" value="powerful"/> powerful<br/>
                <input type="checkbox" name="professional" value="professional"/> professional<br/>
                <input type="checkbox" name="hip" value="hip"/> hip<br/>
            </div>
            <div class="checkbox-section">
                <input type="checkbox" name="classic" value="classic"/> classic<br/>
                <input type="checkbox" name="elegant" value="elegant"/> elegant<br/>
                <input type="checkbox" name="funky" value="funky"/> funky<br/>
                <input type="checkbox" name="ethnic" value="ethnic"/> ethnic<br/>
                <input type="checkbox" name="naturalorganic" value="naturalorganic"/> natural/organic<br/>
            </div>
            <div class="checkbox-section">
                <input type="checkbox" name="hi_tech" value="hi_tech"/> hi tech<br/>
                <input type="checkbox" name="fun" value="fun"/> fun<br/>
                <input type="checkbox" name="flashy" value="flashy"/> flashy<br/>
                <input type="checkbox" name="muted" value="muted"/> muted<br/>
                <input type="checkbox" name="soft" value="soft"/> soft<br/>
            </div>
            <div class="checkbox-section">
                <input type="checkbox" name="playful" value="playful"/> playful<br/>
                <input type="checkbox" name="mellow" value="mellow"/> mellow<br/>
                <input type="checkbox" name="comforting" value="comforting"/> comforting<br/>
                <input type="checkbox" name="romantic" value="romantic"/> romantic<br/>
                <input type="checkbox" name="sensual" value="sensual"/> sensual<br/>
            </div>
            <div class="checkbox-section">
                <input type="checkbox" name="authoritative" value="authoritative"/> authoritative<br/>
                <input type="checkbox" name="energetic" value="energetic"/> energetic<br/>
                <input type="checkbox" name="historical" value="historical"/> historical<br/>
            </div>
            <div class="height-10 clear"></div>
            <div class="height-5"></div>
            <p>
                <b>Question 5:</b> Anything to avoid?
            </p>
            <div class="height-5"></div>
            <p>
                Is there anything you don’t like, or wouldn’t want for your Artwork?
            </p>
            <div class="height-5"></div>
            <p>
                 <input type="checkbox" id="agree" name="agree" value="agree"/>&nbsp;I agree to the $95 ex GST art cost<font style="color: red">*</font>
            </p>
            <div class="height-20"></div>
            <p class="title">
                 What is the best way to contact you? <br />
                Please provide your details below.
            </p>
            <div class="height-5"></div>
            <p class="title">
                 Preferred contact method?
            </p>
            <div>
                <div class="required-field-label">*Required field</div>
                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-comments">
                        <label for="CAT_Custom_201134">Comments</label>
                        <br />
                        <textarea onkeydown="if(this.value.length>=4000)this.value=this.value.substring(0,3999);" class="cat_listbox light-style-input" rows="4" cols="23" id="Comments" name="Comments" style="width: 100%;"></textarea>
                    </span>
                </div>
                <div class="clear-both height-17"></div>
                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-comments">
                        <label for="CAT_Custom_201134">Attach File<font style="color: red"> *</font> (25mb Limit)</label>
                        <br />
                        <asp:FileUpload ID="fileUploadImage" runat="server" accept=".pdf, .eps, .jpg, .jpeg, .tif" />
                    </span>
                </div>
                <div class="clear-both height-25"></div>
                <div class="clear-both height-25"></div>
                <div class="submit-button-place-holder">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="shoppingcart-primary-button site-button" OnClick="btnSubmit_Click" />
                </div>
                <div class="clear-both height-25"></div>
                <div class="clear-both height-25"></div>
                <div class="submit-button-place-holder">
                    <asp:Button ID="btnDesignYourOwn" runat="server" Text="DESIGN YOUR OWN" CssClass="shoppingcart-secondary-button site-button" OnClick="btnDesignYourOwn_Click" />
                </div>
                <div class="clear-both height-12"></div>
                <div class="submit-button-place-holder">
                    <asp:Button ID="btnUploadYourDesign" runat="server" Text="UPLOAD YOUR DESIGN" CssClass="shoppingcart-secondary-button site-button" OnClick="btnUploadYourDesign_Click" />
                </div>
                <div class="height-20"></div>
            </div>
        </form>
        <script type="text/javascript">
            var uploadParam = { Filename: "", Path: "", Comments: "" };
            var supportedExtensions = ["pdf", "eps", "jpg", "jpeg", "tif"];
            var imageFile = null;

            function sendFile(file) {
                var formData = new FormData();
                var file = $('#fileUploadImage')[0].files[0];

                var filename = file.name;
                var fileExtension = filename.substr((filename.lastIndexOf('.') + 1));

                if ($.inArray(fileExtension, supportedExtensions) < 0) {
                    $("#btnSubmit").css('display', 'block');
                    ShowMessageBoxPopup("File type not supported. Please choose .pdf, .jpg, .eps & .tif");
                    return false;
                }
                return true;
            }

            $("#fileUploadImage").on('change', function () {

                var file;
                if ((file = this.files[0])) {
                    imageFile = file;
                }
            })

            $("#btnSubmit").click(function () {
                $(this).css('display', 'none');
                $('#place-order-button-container').fadeIn('slow');
            });

            $(':submit').click(function () {
                var name = this.id;
                if (name == 'btnSubmit') {
                    var isChecked = $('#agree').prop('checked');
                    if (isChecked == false) {
                        $("#btnSubmit").css('display', 'block');
                        ShowMessageBoxPopup("You must agree to the $95.00 design fee.");
                        return false;
                    }
                    if (imageFile != null || imageFile != undefined) {
                        return sendFile(imageFile);
                    }
                    else {
                        $("#btnSubmit").css('display', 'block');
                        ShowMessageBoxPopup("Please select a design to upload first.");
                        return false;
                    }
                }
            });
        </script>
    </asp:Panel>
</body>
</html>
