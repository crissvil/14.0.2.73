﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="get-quote.aspx.cs" Inherits="InterpriseSuiteEcommerce.get_quote" %>

<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="BillingAddressControl" Src="~/UserControls/AddressControl.ascx" %>
<%@ Register TagPrefix="uc" TagName="ProfileControl" Src="~/UserControls/ProfileControl.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Get Qoute</title>
    <style>
        body.modal-open {
            margin-right: 16px !important;
        }
    </style>
</head>
<body>
    <div class="clear-both height-5">&nbsp;</div>
    <form id="frmGetQuote" runat="server" action="">
        <asp:Panel ID="pnlPageContentWrapper" runat="server">

            <h1>Custom Quote Form</h1>
            <div>
                <!-- Form Left Section Starts Here -->
                <div id="divFormLeft" class="get-quote-left-container">
                    <h3>Your Contact Information</h3>
                    <p style="font-size: 15px;color: #414040;">
                        <asp:Literal ID="litIfYouHaveAlreadyAnAccount" runat="server">(!checkout1.aspx.88!)</asp:Literal>&nbsp;<a href="signin.aspx" class="i-orange-link"><asp:Literal ID="litSignIn" runat="server">(!checkout1.aspx.89!)</asp:Literal></a>
                    </p>

                    <%--Contact Name and ContactNumber Input Box Section Starts Here--%>
                    <div style="text-align: right; color: #ff0000; font-size: 12px; font-weight: 400; padding: 7px 0 0;">
                        *Required field
                    </div>
                    <div class="form-controls-place-holder">
                        <span class="form-controls-span form-control-fname">
                            <label id="lblFirstName" class="form-field-label">
                                First Name<font style="color: red">*</font>
                            </label>
                            <br />
                            <asp:TextBox ID="txtFirstName" MaxLength="100" class="light-style-input" runat="server"></asp:TextBox>
                        </span>
                        <span class="form-controls-span form-controls-span-spaceleft form-control-lname">
                            <label id="lblLastName" class="form-field-label">
                                Last Name<font style="color: red">*</font>
                            </label>
                            <br />
                            <asp:TextBox ID="txtLastName" MaxLength="50" class="light-style-input" runat="server"></asp:TextBox>
                        </span>
                    </div>

                    <div class="clear-both height-12"></div>

                    <div class="form-controls-place-holder">
                        <span class="form-controls-span form-control-email">
                            <label id="lblEmail" class="form-field-label" maxlength="50">
                                <asp:Literal ID="litEmail" runat="server">(!contactus.aspx.5!)</asp:Literal>
                            </label>
                            <br />
                            <asp:TextBox ID="txtEmail" MaxLength="50" runat="server" class="light-style-input"></asp:TextBox>
                        </span>
                    </div>
                    <div class="clear-both height-12"></div>

                    <div class="form-controls-place-holder">
                        <%--<span class="form-controls-span form-control-mobile">
                            <label id="lblMobile" class="form-field-label">
                                Mobile Number ( Optional )
                            </label>
                            <br />
                            <asp:TextBox ID="txtMobile" MaxLength="20" runat="server" class="light-style-input" runat="server"></asp:TextBox>
                        </span>--%>
                        <span class="form-controls-span form-control-tel">
                            <label id="lblPhone" class="form-field-label">
                                Phone Number ( Optional ) 
                            </label>
                            <br />
                            <asp:TextBox ID="txtPhone" MaxLength="20" runat="server" class="light-style-input" runat="server"></asp:TextBox>
                        </span>
                    </div>

                    <div class="clear-both height-12"></div>
                    <div class="form-controls-place-holder">
                        <span class="form-controls-span form-control-company">
                            <label id="lblCompany" class="form-field-label">
                                Company<font style="color: red">*</font>
                            </label>
                            <br />
                            <asp:TextBox ID="txtCompany" MaxLength="20" runat="server" class="light-style-input" runat="server"></asp:TextBox>
                        </span>
                    </div>
                    <%--<div class="clear-both height-12"></div>
                    <div class="form-controls-place-holder">
                        <span class="form-controls-span form-control-address">
                            <label id="lblAddress" class="form-field-label" maxlength="50">
                                Address ( Optional )
                            </label>
                            <br />
                            <asp:TextBox ID="txtAddress" MaxLength="50" TextMode="multiline" runat="server" class="light-style-input"></asp:TextBox>
                        </span>
                    </div>--%>
                    <div class="clear-both height-12"></div>
                    <div class="clear-both height-5"></div>
                    <div class="form-controls-place-holder">
                        <%--<span class="form-controls-span form-control-city">
                            <label id="lblCity" class="form-field-label" maxlength="50">
                                City / Suburb ( Optional )
                            </label>
                            <br />
                            <asp:TextBox ID="txtCity" MaxLength="50" runat="server" class="light-style-input"></asp:TextBox>
                        </span>--%>
                        <span class="form-controls-span form-control-state" id="i-state-selector">
                            <label id="lblState" class="form-field-label">
                                State<font style="color: red">*</font>
                            </label>
                            <br />
                            <asp:DropDownList ID="drpState" runat="server" CssClass=""></asp:DropDownList>
                        </span>
                        <span class="form-controls-span form-controls-span-spaceleft form-control-postal">
                            <label id="lblPostal" class="form-field-label" maxlength="50">
                                Postcode<font style="color: red">*</font>
                            </label>
                            <br />
                            <asp:TextBox ID="txtPostal" MaxLength="5" runat="server" class="light-style-input"></asp:TextBox>
                            <br />
                            <asp:RegularExpressionValidator ID="regextxtPostal" ControlToValidate="txtPostal" Display="Dynamic" runat="server"
                                ValidationExpression="^\d+$" ErrorMessage="Invalid Postal Code" CssClass="invalid-postal-message"></asp:RegularExpressionValidator>
                        </span>
                    </div>
                    <asp:TextBox ID="txtCountry" MaxLength="50" runat="server" class="light-style-input i-hide"></asp:TextBox>
                    <asp:TextBox ID="txtState" MaxLength="50" runat="server" class="light-style-input i-hide"></asp:TextBox>
                    <asp:TextBox ID="txtIndustryGroup" MaxLength="50" runat="server" class="light-style-input i-hide"></asp:TextBox>
                    <div class="clear-both height-12"></div>
                    <%--<div class="form-controls-place-holder">
                        <span class="form-controls-span form-control-country" id="i-country-selector">
                            <label id="lblCountry" class="form-field-label">
                                Country<font style="color: red">*</font>
                            </label>
                            <br />--%>
                            <%--<asp:DropDownList ID="drpCountry" runat="server"></asp:DropDownList>--%>
                            <%--<asp:TextBox ID="drpCountry" MaxLength="80" runat="server" class="light-style-input" ReadOnly="true"></asp:TextBox>
                        </span>
                    </div>
                    <div class="clear-both height-12"></div>--%>
                    <%--<div class="clear-both height-5"></div>--%>
                    <%--<span class="form-section" style="font-size:15px;">Comments</span>
					<div class="clear-both height-5"></div>--%>
                    <div class="form-controls-place-holder">
                        <span class="form-controls-span form-control-comment">
                            <label id="lblMessageDetails" class="form-field-label">
                                <asp:Literal ID="litMessageDetails" runat="server">Comments</asp:Literal>
                            </label>
                            <br />
                            <asp:TextBox ID="txtMessageDetails" Rows="24" TextMode="MultiLine" runat="server" class="light-style-input" Columns="55"></asp:TextBox>
                        </span>
                    </div>

                    <%-- Security Code / Captcha Section Starts Here --%>
                    <asp:Panel runat="server" ID="pnlSecurityCode" Style="text-align: right;">
                        <div class="clear-both height-20"></div>
                        <div class="clear-both height-25"></div>
                        <span class="form-controls-span">
                            <label class="form-field-label capitalize-text">
                                <asp:Literal ID="LitEnterSecurityCodeBelow" runat="server">(!customersupport.aspx.12!)</asp:Literal>
                            </label>
                        </span>
                        <div class="clear-both"></div>
                        <span class="form-controls-span">
                            <label id="lblCaptcha" class="form-field-label">
                                <asp:Literal ID="litCaptcha" runat="server">(!customersupport.aspx.13!)</asp:Literal>&nbsp;&nbsp;
                            </label>
                            <asp:TextBox ID="txtCaptcha" runat="server" class="light-style-input"></asp:TextBox>
                        </span>
                        <div class="clear-both height-5"></div>
                        <div style="text-align: right;">
                            <img alt="captcha" src="Captcha.ashx?id=1" id="captcha" />
                        </div>
                        <div class="clear-both height-5"></div>
                        <a href="javascript:void(1);" style="color: red; position: relative; font-weight: 400; font-size: 10px;" id="i-get-another-captcha">Get Another Captcha</a>
                        <%--<table>
							<tr>
								<td>
							<span style="padding-right: 86px;font-size:14px;" class="form-controls-span custom-font-style capitalize-text" id="support-captcha-label">
                                <asp:Literal ID="LitEnterSecurityCodeBelow" runat="server">(!customersupport.aspx.12!)</asp:Literal>
                            </span>
                         
								</td>
								<td>
								   <span class="form-controls-span">
                                <label id="lblCaptcha" class="form-field-label">
                                    <asp:Literal ID="litCaptcha" runat="server" >(!customersupport.aspx.13!)</asp:Literal>
                                </label>
                                <asp:TextBox ID="txtCaptcha" runat="server" class="light-style-input" style="text-align: center;font-size: 21px !important;height: 38px !important;color: #000;font-weight: bold;"></asp:TextBox>
                            </span>
								
								</td>
							</tr>
							<tr>
								<td></td>
								<td style="text-align:right">
								<img alt="captcha" src="Captcha.ashx?id=1" id="captcha"/>
								<div class="clear-both height-5"></div>
								<a href="javascript:void(1);" style="color:red;position:relative;font-weight:bold;font-size:11px;" id="i-get-another-captcha">Get Another Captcha</a>
								</td>
							</tr>
							
						</table>--%>
                    </asp:Panel>
                    <%-- Security Code / Captcha Section Ends Here --%>
                    <div class="clear-both height-5"></div>
                    <div class="clear-both height-17"></div>

                    <%-- Form Lower Section : Button Container Starts Here --%>
                    <div id="contact-form-button-place-holder">
                        <div id="send-message-button">
                            <div id="send-message-loader" class="i-hide i-process-message" style="text-align: right">
                                <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
                                <span>We are processing your order, please be patient this may take a moment...</span>
                            </div>
                            <div class="height-13"></div>
                            <div id="save-case-button-place-holder" class="i-btn-container" style="text-align: right">
                                <asp:Button ID="btnSendMessage" runat="server" OnClientClick="if (!formInfoIsGood()) {return false;};" Text="Submit Quote" UseSubmitBehavior="False" CssClass="site-button get-quote-primary-button" />
                            </div>
                            <div id="error-message" class="i-hide" style="text-align: right; color: red;">
                                <span style="font-size: 14px;">An error occured. Please contact the website admin.</span>
                            </div>
                        </div>
                    </div>
                    <%-- Form Lower Section : Button Container Ends Here --%>
                </div>
                <%--Form Left Section Ends Here--%>
                <%-- Form Right Section Starts Heree--%>
                <div id="divFormRight" class="get-quote-right-container">
                    <div class="get-quote-product-info-container">
                        <h3>Product Information</h3>
                        <div class="get-quote-image">
                            <asp:Literal ID="litImage" runat="server"></asp:Literal>
                        </div>
                        <div>
                            <table style="position: relative;" class="get-quote-product-info">
                                <tr>
                                    <td id="i-item-selected" colspan="2">
                                        <asp:Literal ID="litProductName" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 5px 5px 7px;">Product Code:</td>
                                    <td id="i-item-code-selected" style="padding: 5px 5px 7px;">
                                        <asp:Literal ID="litProductCode" runat="server"></asp:Literal></td>
                                </tr>

                                <tr>
                                    <td style="padding: 5px; height: 35px;">Dimension (HxWxD):</td>
                                    <td>
                                        <asp:Literal ID="litProductDimension" runat="server"></asp:Literal></td>
                                </tr>
                                <tr style="display: none">
                                    <td style="padding: 5px; height: 40px;">Minimum Order:</td>
                                    <td id='i-min-order'>
                                        <asp:Literal ID="litMinQty" runat="server"></asp:Literal></td>
                                </tr>
                                <asp:Panel ID="pnlFabric" runat="server">
                                    <tr class="fabric-colours">
                                        <td style="padding: 5px; width: 138px; height: 40px;">Fabric Colour:</td>
                                        <td>
                                            <i class="light-style-input" id="i-fabric-selected">
                                                <asp:Literal ID="litColor" runat="server"></asp:Literal>
                                            </i>
                                            <i class="i-color-swatch" data-toggle="modal" data-target="#fabricColoursModal" title="Click this icon to selec fabric colours"></i>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <asp:Panel ID="pnlPrintedColours" runat="server">
                                    <tr class="fabric-colours">
                                        <td style="padding: 5px; width: 138px; height: 40px;">Printed Colours:</td>
                                        <td>
                                            <asp:DropDownList ID="drpPrintedColours" runat="server" CssClass=""></asp:DropDownList>
                                        </td>
                                    </tr>
                                </asp:Panel>
                                <tr>
                                    <td style="padding: 5px; width: 138px; height: 40px;">Quantity:</td>
                                    <td>
                                        <asp:TextBox ID="txtQuantity" MaxLength="10" class="light-style-input requires-validation" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clear-both height-20"></div>
                    <div class="height-5"></div>
                    <div class="get-quote-contact-info-container">
                        <h3>Our contact information</h3>
                        <div style="font-size: 16px; color: #6c6a6b; padding-top: 11px;">
                            <p>Smartbag Pty Ltd</p>
                            <p>Unit 20, 19 Aero Road</p>
                            <p>INGLEBURN, NSW, 2565</p>
                            <p>Australia</p>
                            <p class="height-20"></p>
                            <p>PH : 1300 874 559</p>
                            <p>FAX : +61 2 9605 1711</p>
                            <p>EMAIL : <a href="mailto:sales@smartbag.com.au"><u>sales@smartbag.com.au</u></a></p>

                        </div>
                    </div>
                </div>
                <%-- Form Right Section Ends Here--%>
            </div>

            <div class="clear-both height-5">&nbsp;</div>
        </asp:Panel>
        <script type="text/javascript" src="jscripts/jquery/getquote.js"></script>
    </form>
</body>
</html>
