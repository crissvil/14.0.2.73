﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="how-to-order.aspx.cs" Inherits="InterpriseSuiteEcommerce.how_to_order" %>

<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div class="clear-both height-5">&nbsp;</div>
    <form id="frmHowToOrder" runat="server">
        <h1>How To Order</h1>
        <div class="content-box">
                <p style="font-size: 22px; color: #f48c00; line-height: 30px;">Reply to your email quote or contact your salesperson with what you would like to order.</p>
                <div class="clear-both height-22">&nbsp;</div>
                <div class="height-10"></div>
                <p style="line-height: 24px;">Attach your artwork to an email in an eps, pdf or jpg file. We can also use gif, word and other files for simpler artwork.</p>
                <div class="clear-both height-13">&nbsp;</div>
                <p style="line-height: 24px;">
                    We will send a pdf proof and 40% deposit invoice by email within a couple of days of receiving your order.
Approve your proof and pay your deposit. We will manufacture a pre-production sample within 2 weeks of payment and approval.
                </p>
                <div class="clear-both height-13">&nbsp;</div>
                <p>For tight deadlines you may also approve by jpg photo or pdf proof.</p>
                <div class="clear-both height-13">&nbsp;</div>
                <p>Approve your sample and the full order will arrive within 6-8 weeks.</p>
                <div class="clear-both height-13">&nbsp;</div>
                <p>We make it easy for you to get the best bag that fits your budget and style.</p>
                <div class="clear-both height-13">&nbsp;</div>
                <p style="line-height: 24px;">We have air freight and express air freight options for quicker delivery. You may choose to have a portion of your bags air freighted and the rest sent by sea.</p>
                <div class="clear-both height-13">&nbsp;</div>
                <p style="line-height: 24px;">For our in stock items, delivery is within 3-4 days of payment. Allow maximum 7 working days for sticker manufacture.</p>
                <div class="clear-both height-22"></div>
                <div class="height-10"></div>
                <p style="font-size: 22px; color: #f48c00; line-height: 30px;">Most importantly, if you have any questions, please call <b>1300 874 559</b>.</p>
            </div>
    </form>
</body>
</html>
