﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="faq.aspx.cs" Inherits="InterpriseSuiteEcommerce.faq" %>

<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FAQ</title>
    <style>
        #frmFAQ p {
            line-height: 24px;
        }
    </style>
</head>
<body>
    <div class="clear-both height-5">&nbsp;</div>
    <form id="frmFAQ" runat="server" action="">
        <h1>Fabric FAQs</h1>
        <div class="content-box">
                <p class="text-section-title">What is polypropylene and NWPP?</p>

                <p>Polypropylene is the material that the "supermarket green bags" are made from. It's most common use is in the production of medical gowns, caps, masks and gloves.</p>
                <div class="clear-both height-13"></div>

                <p>It is actually called non-woven polypropylene (NWPP) as the cross thatched pattern on the material is stamped on to make it look woven.</p>
                <div class="clear-both height-13"></div>

                <p>PP is a thermoplastic polymer - a type of plastic that can be melted down and recycled into such things as garden stakes, garden furniture and flower pots.</p>


                <p class="text-section-title">But isn't polypropylene just another plastic?</p>

                <p>There is substantial proof that it's not just another plastic. Polypropylene is strong, well priced, can be made to any colour, and print options are vibrant and unlimited.</p>
                <div class="clear-both height-13"></div>

                <p>It is also recyclable and leaves a smaller ecological footprint than many other plastics.</p>
                <div class="clear-both height-13"></div>

                <p>The Greenpeace pyramid of plastics,(see below), is a ranking of plastics into four groups according to their hazardous characteristics.</p>
                <div class="clear-both height-13"></div>

                <p>PVC, the most problematic and polluting plastic, is at the top of the pyramid at number one, while biobased plastics, the least polluting are number four at the pyramid's base. Polypropylene (PP) is just above biobased plastics at number three along with PET (the material used to make recyclable plastic bottles).</p>
                <div class="clear-both height-13"></div>

                <p>Furthermore, Zero Waste South Australia produced a report stating that Polypropylene has lower greenhouse gas impact and energy use in manufacture than either paper or calico (including production of raw materials).</p>
                <div class="clear-both height-13"></div>

                <p>Greenpeace pyramid of plastics</p>
                <div class="clear-both height-13"></div>

                <p class="i-text-indent"><i class="fa fa-caret-right"></i>&nbsp;&nbsp; Polyvinyl chloride (PVC) and other halogenated plastics</p>
                <div class="clear-both height-13"></div>

                <p class="i-text-indent"><i class="fa fa-caret-right"></i>&nbsp;&nbsp; Polyurethane (PU), Polystyrene (PS), Acrylonitrile- butadiene-styrene (ABS), Polycarbonate (PC)</p>
                <div class="clear-both height-13"></div>

                <p class="i-text-indent"><i class="fa fa-caret-right"></i>&nbsp;&nbsp; Polyethylene-terephthalate (PET), Polyolefins (PE, PP, NWPP etc.)</p>
                <div class="clear-both height-13"></div>

                <p class="i-text-indent"><i class="fa fa-caret-right"></i>&nbsp;&nbsp; Biobased plastics</p>
                <div class="clear-both height-13"></div>

                <p>Analysis of Levies and Environmental Impacts Table 4.3 - Assessment of Alternatives, in the report</p>

                <p class="text-section-title">What makes NWPP so good to use as a fabric?</p>
                <p>NWPP makes a tough water repelling bag that can be dyed and printed to any Pantone Matching System (PMS) colour. Even the most complex designs can be screen printed or heat transferred onto it. All buckles, zips, loops and studs are made from polypropylene (PP) and are recyclable.</p>
                <div class="clear-both height-13"></div>

                <p>You may find that some of your bags and backpacks at home contain a large component of PVC. PVC is cheap and tough but also highly toxic. From it's manufacture to it's disposal, PVC emits toxic, cancer causing compounds.</p>
                <div class="clear-both height-13"></div>

                <p>NWPP's strength to weight ratio and low cost makes it a suitable alternative to polyester or nylon.</p>
                <div class="clear-both height-13"></div>

                <p>It is the perfect material to make a smart and fashionable bag designed for every day use.</p>

                <p class="text-section-title">Where can I recycle my NWPP bags?</p>

                <p>Most larger supermarkets including IGA, Coles and Woolworths provide wheelie style recycle bins. These are red and displayed prominently near the entrance of all stores.</p>
                <div class="clear-both height-13"></div>

                <p>Recycling facilities can also be found at local Stockland shopping centres. Officeworks stores also have red Planet Ark bins suitable for these bags.</p>

                <p class="text-section-title">When will there be more recycling locations?</p>


                <p>As market demand for recycled and recyclable material grows, there will be more recycling points available. Increased production of polypropylene products will lead to more recycling facilities.</p>

                <p class="text-section-title">Why are these NWPP bags called 'green' or 'eco' bags?</p>

                <p>They were originally introduced by supermarkets under pressure to reduce their consumption of single use plastic bags that mostly ended up as land fill or litter.</p>
                <div class="clear-both height-13"></div>

                <p>People are encouraged to 'say no to plastic bags' as there is now a better alternative. NWPP bags are recycled in the same way as PET bottles and paper.</p>
                <div class="clear-both height-13"></div>
            </div>
    </form>
</body>
</html>
