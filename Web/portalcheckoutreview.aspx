﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="portalcheckoutreview.aspx.cs" Inherits="InterpriseSuiteEcommerce.portalcheckoutreview" %>

<%@ Register TagPrefix="ise" Namespace="InterpriseSuiteEcommerceControls" Assembly="InterpriseSuiteEcommerceControls" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>
<%@ Register TagPrefix="ise" TagName="XmlPackage" Src="XmlPackageControl.ascx" %>
<%@ Import Namespace="InterpriseSuiteEcommerceCommon" %>
<%@ Register TagPrefix="uc" TagName="ScriptControl" Src="~/UserControls/ScriptControl.ascx" %>
<html>
<head>
</head>
<body>
    <uc:ScriptControl ID="ctrlScript" runat="server" />
    <asp:Panel ID="pnlContent" runat="server">
        <div>
            <form id="frmPortalCheckoutReview" runat="server">
                <div style="clear: both; height: 17px;">
                </div>
                <div style="font-size: 18px; color: #000; line-height: 22px;">
                    <asp:Literal ID="checkoutreviewaspx6" Mode="PassThrough" runat="server"></asp:Literal>
                </div>
                <div style="clear: both; height: 30px;">
                </div>
                <div id='place-order-button-container'>
                    <div>
                        <div class="place-order-message">
                            <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
                            <span>
                                <asp:Literal ID="checkoutreviewaspx14" Mode="PassThrough" runat="server" Text="(!checkoutreview.aspx.14!)"></asp:Literal>
                            </span>
                        </div>
                    </div>
                </div>
                <asp:Panel ID="pnlButtonPlaceHolder" runat="server" Style="text-align: right;">
                    <span>
                        <asp:Button ID="btnContinueShoppingTop" Text="(!shoppingcart.cs.12!)" CssClass="portalcheckoutreview-secondary-button site-button content" runat="server" /></span>
                    <span>
                        <asp:Button ID="btnChargeAccount1" Text="(!custom.text.21!)" CssClass="site-button checkoutreview-place-order-button" runat="server" /></span>
                </asp:Panel>
                <div class="clear-both height-20"></div>
                <div class="height-20"></div>
                <div class="cancellation-text">
                    <asp:Literal ID="Literal2" Mode="PassThrough" runat="server" Text="(!portal.aspx.34!)"></asp:Literal>
                </div>
                <asp:Panel ID="pnlErrorMsg" runat="Server" Visible="false">
                    <p>
                        <asp:Label ID="ErrorMsgLabel" CssClass="errorLg" runat="Server"></asp:Label>
                    </p>
                </asp:Panel>
                <div class="sections-place-holder no-padding">
                    <!-- Order Summary Section { -->
                    <div class="sections-place-holder">
                        <div class="portalcheckoutreview-section-header">
                            <span>
                                <asp:Literal ID="litItemsToBeShipped" runat="server">(!checkoutpayment.aspx.39!)</asp:Literal></span>
                        </div>
                        <asp:Literal ID="OrderSummary" runat="server"></asp:Literal>
                    </div>
                    <!-- Order Summary Section } -->
                </div>
                <div class="update-cart-layout">
                    <asp:Button ID="btnUpdateCart" CssClass="portalcheckoutreview-secondary-button site-button content" Text="(!shoppingcart.cs.33!)" runat="server" />
                </div>
                <div style="clear: both; padding-top: 100px;"></div>

                <div class="checkoutreview-billing-section">
                    <div class="checkoutreview-billing-header">
                        <span>
                            <asp:Label ID="checkoutreviewaspx8" Text="(!checkoutreview.aspx.8!)" runat="server"></asp:Label>
                        </span>
                        <span class="one-page-link-right float-right">
                            <a href="selectaddress.aspx?AddressType=Billing&editaddress=true&checkout=true">
                                <asp:Literal ID="EditBillingAddress" runat="server"></asp:Literal></a>
                        </span>
                    </div>
                    <div class="checkoutreview-label">
                        <asp:Literal ID="litBillingAddress" runat="server" Mode="PassThrough"></asp:Literal>
                    </div>
                </div>

                <div class="checkoutreview-shipping-section">
                    <div class="checkoutreview-billing-header">
                        <span>
                            <asp:Label ID="ordercs57" Text="(!order.cs.19!)" runat="server"></asp:Label>
                        </span>
                        <span class="one-page-link-right float-right">
                            <a href="selectaddress.aspx?AddressType=Shipping&editaddress=true&checkout=true">
                                <asp:Literal ID="EditShippingAddress" runat="server"></asp:Literal></a>
                        </span>
                    </div>
                    <div class="checkoutreview-label">
                        <asp:Panel runat="server" Visible="false" ID="pnlShippingPickMessage">
                            (<asp:Label ID="lblShippingPickMessage" runat="server" Text="(!checkoutreview.aspx.16!)" class="review-shipping-address-pickup-message" />)
                <div class="height-12"></div>
                        </asp:Panel>
                        <asp:Literal ID="litShippingAddress" runat="server" Mode="PassThrough"></asp:Literal>
                    </div>
                </div>

                <div class="clear-both height-20"></div>
                <div class="clear-both height-20"></div>
                <div class="sections-place-holder no-padding">
                    <div class="sections-place-holder">
                        <div class="portalcheckoutreview-section-header">
                            <asp:Label ID="litComments" runat="server" Text="(!portal.aspx.46!)" class="" />
                        </div>
                        <div style="padding: 10px 0;">
                            <asp:TextBox ID="OrderNotes" Columns="90" Rows="4" TextMode="MultiLine" Width="100%" runat="server"></asp:TextBox>
                        </div>
                        <div style="color: #ff0000;">
                            <asp:Literal runat="server" Visible="false" ID="ltOrderName" Text="(!portal.aspx.45!)" />
                        </div>
                    </div>
                </div>

                <div class="height-20"></div>
                <div class="height-20"></div>
                <div class="checkoutreview-terms-header" style="display:none;">
                    <label class="checkbox-container">
                        <asp:Literal runat="server" ID="litTerms" Text="custom.text.19" />
                        <input type="checkbox" id="chkTerms" name="chkTerms" />
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="checkoutreview-terms-content" style="display:none;">
                    <asp:Literal runat="server" ID="Literal1" Text="custom.text.20" />
                </div>

                <div class="clear-both height-20"></div>
                <div class="height-20"></div>
                <div id='place-order-button-container-bottom'>
                    <div>
                        <div class="place-order-message">
                            <i class="fa fa-spinner fa-pulse fa-2x fa-fw"></i>
                            <span>
                                <asp:Literal ID="Literal3" Mode="PassThrough" runat="server" Text="(!checkoutreview.aspx.14!)"></asp:Literal>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="float-right">
                    <asp:Button ID="btnChargeAccount2" Text="(!custom.text.21!)" CssClass="site-button checkoutreview-place-order-button" runat="server" />
                </div>
                <div class="clear-both height-20"></div>
                <script type="text/javascript">
                    $(document).ready(function () {
                        var keys = new Array();
                        keys.push("checkoutpayment.aspx.5");
                        var callback = function () { }
                        ise.StringResource.loadResources(keys, callback);

                        $("#btnChargeAccount1").click(function (e) {
                            var status = CheckTerms()
                            if (status != true) {
                                e.preventDefault();
                                $('html, body').animate({
                                    scrollTop: $(".checkoutreview-terms-header").offset().top
                                }, 1000);
                                return false;
                            }
                        });

                        $("#btnChargeAccount2").click(function (e) {
                            var status = CheckTerms()
                            if (status != true) {
                                e.preventDefault();
                                return false;
                            }
                        });

                        $("#chkTerms").click(function () {
                            $("#ise-message-tips").fadeOut("slow");
                        });

                        $("#ise-message-tips").click(function () {
                            $("#ise-message-tips").fadeOut("slow");
                        });
                    });

                    function CheckTerms() {
                        var isTermsAndConditionsChecked = $("#chkTerms").is(':checked');
                        //disable checking as required by new specs.
                        isTermsAndConditionsChecked = true;
                        if (isTermsAndConditionsChecked) {
                            $(this).css('display', 'none');
                            $("#btnChargeAccount1").css('display', 'none');
                            $("#btnChargeAccount2").css('display', 'none');
                            $("#btnContinueShoppingTop").css('display', 'none');
                            $("#btnUpdateCart").css('display', 'none');
                            $(".shoppingcart-continue-link").css('display', 'none');
                            $(".shoppingcart-update-link").css('display', 'none');
                            $('#place-order-button-container').fadeIn('slow');
                            $('#place-order-button-container-bottom').fadeIn('slow');
                            return true;
                        }
                        else {
                            $("#chkTerms").addClass("required-input");
                            var thisLeft = $("#chkTerms").offset().left;
                            var thisTop = $("#chkTerms").offset().top;
                            $("#ise-message-tips").css("top", thisTop - 53);
                            $("#ise-message-tips").css("left", thisLeft - 174);
                            var keys = new Array();
                            keys.push("checkoutpayment.aspx.5");
                            var callback = function () { }
                            ise.StringResource.loadResources(keys, callback);
                            $("#ise-message").html(ise.StringResource.getString("checkoutpayment.aspx.5"));
                            $("#ise-message-tips").fadeIn("slow");
                            return false;
                        }
                    }
                </script>
            </form>
        </div>
    </asp:Panel>
</body>
</html>
