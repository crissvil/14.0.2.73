<%@ Page Language="C#" AutoEventWireup="true" CodeFile="print.aspx.cs" Inherits="InterpriseSuiteEcommerce.print" %>

<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Print</title>
</head>
<body>
    <div class="clear-both height-5">&nbsp;</div>
    <form id="frmPrint" runat="server" action="">
        <h1>Print information and artwork file specifications</h1>
        <div class="content-box">
            <ul id="c-print">
                <li class="printing">
                    <h3>Printing</h3>
                    <i class="text-section-title">Three types of printing available on our bags : </i>
                    <ul class="c-print-links">
                        <li>
                            <div class="height-20"></div>
                            <div class="height-10"></div>
                            <div class="height-8"></div>
                            <div style="float:left;">
                                <a href="screen-print.aspx">
                                    <img src="skins/Skin_(!SKINID!)/images/screen-print.png" alt="Screen Print" /></a>
                            </div>
                            <div style="float:left; width: 65%;">
                                <a href="screen-print.aspx">Screen print</a><br />
                                <p>Unlimited <a href="color-swatches.aspx" style="font-size: 15px;">PMS (Pantone) colours</a>, block colours only.</p>
                            </div>
                        </li>
                        <li>
                            <div class="height-20"></div>
                            <div class="height-20"></div>
                            <div class="height-5"></div>
                            <div style="float:left;">
                                <a href="heat-transfer.aspx"><img src="skins/Skin_(!SKINID!)/images/heat-transfer.png" alt="Heat Transfer" /></a>
                            </div>
                            <div style="float:left; width: 65%;">
                                <a href="heat-transfer.aspx">Heat Transfer</a><br />
                                <p>Full colour process for smaller areas of print.</p>
                            </div>
                        </li>
                        <li>
                            <div class="height-20"></div>
                            <div class="height-20"></div>
                            <div class="height-5"></div>
                            <div style="float:left;">
                                <a href="laminated.aspx"><img src="skins/Skin_(!SKINID!)/images/laminated.png" alt="Laminated" /></a>
                            </div>
                            <div style="float:left; width: 65%;">
                                <a href="laminated.aspx">Laminated</a>
                                <p>
                                    Full coverage and full colour.
                                </p>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="print-artwork">
                    <h3>Your Artwork Logo</h3>
                    <ul class="c-print-links">
                        <li>
                            <p>For simple artwork/ logo we can use most files (word doc for example).</p>
                        </li>
                        <li>
                            <p style="padding-top: 20px; padding-bottom: 20px;">High resolution required.</p>
                        </li>
                        <li>
                            <img src="skins/Skin_(!SKINID!)/images/file-type-format.png" alt="File Type" style="padding: 10px 0;" /></li>
                        <li style="padding-bottom: 15px;padding-top: 10px;">
                            <p />
                            <p>
                                PDF or EPS is preferable, especially
                                for more complex work.
                            </p>
                        </li>
                        <li style="padding-bottom: 15px;">
                            <p>
                                If in doubt, send the best file you have and
                                Smartbag will advise.
                            </p>
                        </li>
                        <li>
                            <p>
                                Email <a href="mailto:sales@smartbag.com.au" style="font-size: 18px; font-weight: 400;"><u>sales@smartbag.com.au</u></a> or
                                    <br />
                                call <b>1300 874 559</b>
                            </p>
                        </li>
                    </ul>
                </li>
            </ul>
            <p class="i-text-clear-5"></p>
        </div>
    </form>
</body>
</html>
