﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerce
{
    public partial class designit : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string process = "process".ToQueryString();
            if (process == "save" || process == "edit")
            {
                AppLogic.SaveCustomizationCode(1);
                Response.Redirect("shoppingcart.aspx");
            }
        }
    }
}