﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="terms.aspx.cs" Inherits="InterpriseSuiteEcommerce.terms" %>

<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <div class="clear-both height-5">&nbsp;</div>
    <form id="frmTerms" runat="server" action="">
        <h1>Terms and Conditions</h1>
        <div class="content-box">
                <p class="text-section-title">SHIPPING & RETURNS</p>
                <div class="i-text-indent">
                    <p>
                        These terms and conditions of sale apply to all orders placed through 
	                    the web site of http://smartbag.com.au. In these terms and conditions the words "we", "us" and "our" 
	                    refer to Smartbag Pty Ltd (ABN 51 076 136 760) and the words "you" and "your" refer to the person placing 
	                    the relevant order.
                    </p>

                    <p class="text-section-title">1. Returns Process</p>
                    <p class="text-section-title">1.1</p>

                    <p>We do not refund or exchange for incorrect bag size. We do not refund or exchange for change of mind.</p>
                    <p>All measurements are provided on our website, and it is your responsibility to order the correct size.</p>
                    <p>Refunds and exchanges will only be done if the products are faulty or incorrect product delivered.</p>
                    
                    <p class="text-section-title">2. Shipping</p>
                    <p class="text-section-title">2.1 Shipping Times</p>

                    <p>Shipping and dispatch times on our website are indicative only. We will make every effort to dispatch your order in a timely manner, delivery times are indicative only and we are not liable for shipments which do not arrive at the expected time. To avoid delays in transit, we suggest you provide an attended business address such as a work address where someone you trust will be available to receive the order during normal business hours.</p>

                    <p class="text-section-title">2.2 Loss and Damage</p>

                    <p>(a) Goods in Transit - All our risk for loss of goods or damage in transit ends at</p>
                    the time we provide the goods to the courier, or they otherwise leave our possession. 
	                <p class="i-text-clear-5"></p>

                    <p>
                        (b) Transit Goods Insurance - Insurance is available for your goods, and must be specified 
	                    when you are ordering. Insurance is charged on a per consignment basis to cover the value of goods.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <p class="text-section-title">2.3 Additional Fees</p>
                    <p>
                        (a) Where we incur extra delivery charges as a result of your action or inaction, these may be passed on to you at our discretion.
                    </p>

                    <p class="text-section-title">2.4 Transfer of Ownership</p>
                    <p>(a) Ownership of the goods will pass to you once they have been paid to us in full.</p>

                    <p class="text-section-title">3. Ordering</p>
                    <i class="text-section-title">3.1</i>

                    <p>All orders placed with us are subject to our acceptance. We have no obligations in relation to any order placed until we have accepted it.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">3.2</i>
                    <p>Unless we expressly agree with you otherwise, we are not required to despatch any order until we have received payment in full for all items on the order.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">3.3</i>
                    <p>We offer a <span style="border-bottom: 3px double blue; font-size: 18px;">30 day</span> credit account for Pre-Approved Trade Partners Only which have provided us with a completed credit application signed, witnessed and approved by our Credit Controller to which we have agreed 
	                    in writing to such credit. We reserve the right to refuse or revoke credit to any organisation or individual.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <p class="text-section-title">4. Stock Availability</p>
                    <i class="text-section-title">4.1</i>
                    <p>
                        Despite stock information displayed on our website being linked live to our inventory system,
	                    quantities on our website are a GUIDE only. We do not guarantee the accuracy of stocking information although we aim to keep this information as accurate as possible.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">4.2</i>
                    <p>We will not reserve items on your order until we have received payment in full for those items and have accepted your order.</p>
                    <p class="i-text-clear-5"></p>

                    <p class="text-section-title">5. DOA, Faulty Goods and mis-picked goods</p>

                    <i class="text-section-title">5.1</i>
                    <p>You will need to notify us WITHIN 14 DAYS from invoice date if a product you have received is DOA ("Dead on Arrival" - not working), is faulty, has been damaged in transit,has been mis-picked (IE is not the product you have been invoiced for) or if you feel the product differs from the picture or description advertised on our website.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">5.2</i>
                    <p>Please ensure products and packaging remain in new condition. We do send out replacement products in advance of receiving the returned goods. Once goods arrive,they will be tested by our staff. If that testing shows that the goods are in normal working order, they will be held at our warehouse for collection and we may charge you handling and shipping fees.</p>
                    <p class="i-text-clear-5"></p>

                    <p class="text-section-title">6. Liability and General</p>
                    <i class="text-section-title">6.1</i>
                    <p>
                        To the greatest extent permitted by law, we exclude all terms, conditions and warranties which would otherwise be implied into this agreement. Where we are not permitted to exclude a term, condition or warranty, it forms part of this agreement and, to the greatest extent permitted by law, our liability for a breach of that term, condition or warranty, is limited, at our election, to one or more of the following:
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i>In the case of goods :</i>
                    <p class="i-text-clear-5"></p>
                    <p class="i-text-indent"><span class="i-span-inline">(i)</span> the replacement of the goods or the supply of equivalent goods; </p>
                    <p class="i-text-indent"><span class="i-span-inline">(ii)</span> the repair of the goods; </p>
                    <p class="i-text-indent"><span class="i-span-inline">(iii)</span> the payment of the cost of replacing the goods or of acquiring equivalent goods;  </p>
                    <p class="i-text-indent"><span class="i-span-inline">(iv)</span> the payment of the cost of having the goods repaired; or </p>
                    <p class="i-text-indent"><span class="i-span-inline">(v)</span> the the case of services: </p>
                    <p class="i-text-indent"><span class="i-span-inline">(vi)</span> the supplying of the services again; or  </p>
                    <p class="i-text-indent"><span class="i-span-inline">(vii)</span> the payment of the cost of having the services supplied again. </p>
                    <p class="i-text-clear-5"></p>
                    <i class="text-section-title">6.2</i>
                    <p>
                        We are not liable to you for any special, consequential or indirect loss or damage arising from or in relation to any good or service acquired from us. In particular, we have no liability to you for any loss of profits, or opportunity, any internal costs you incur, or any data you lose resulting from any goods or services acquired from us.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">6.3</i>
                    <p>
                        Subject to clause 6.1, our total aggregate liability to you for all loss or damage arising out of or in respect to this agreement, or any acquisition of goods or services from us is limited to the amount received by us from you in respect of the acquisition of that good or service.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">6.4</i>
                    <p>
                        This contract sets out the whole of the agreement between us and, except to the extent explicitly agreed by us in writing, any terms or conditions appearing on any other documentation, including on a purchase order, are of no effect.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">6.5</i>
                    <p>
                        This agreement is governed by the laws in force in the State of New South Wales. All actions in relation to this agreement, or any product or service purchased under this agreement must only be commenced or continued in a court located within NSW. No action in relation to this agreement or any product or service purchased under this agreement may be commenced more than 12 months after the cause of action arose.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">6.6</i>
                    <p>
                        Where we are prevented from performing an obligation under this agreement as a result of a cause outside our reasonable control, our obligation is suspended until a reasonable time after that cause ceases to prevent us from complying with that obligation.
                    </p>
                    <p class="i-text-clear-5"></p>
                </div>

                <div class="text-section-title">CONDITIONS OF USE</div>
                <div class="i-text-indent">
                    <p class="text-section-title">1. Definitions</p>
                    <i class="text-section-title">1.1</i>
                    <p>"Seller" shall mean Smartbag Pty Ltd its successors and assigns or any person acting on behalf of and with the authority of Smartbag Pty Ltd.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">1.2</i>
                    <p>
                        "Customer" shall mean the Customer (or any person acting on behalf of and with the authority of the Customer) as described on any quotation, 
                        work authorisation or other form as provided by the Seller to the Customer.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">1.3</i>
                    <p>"Guarantor" means that person (or persons), or entity, who agrees to be liable for the debts of the Customer on a principal debtor basis.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">1.4</i>
                    <p>
                        "Goods" shall mean Goods supplied by the Seller to the Customer (and where the context so permits shall include any supply of Services as hereinafter defined) and are as described on the invoices,
                        quotation, work authorisation or any other forms as provided by the Seller to the Customer.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">1.5</i>
                    <p>
                        "Services" shall mean all Services supplied by the Seller to the Customer and includes any advice or recommendations (and where the context so permits shall include any supply of Goods as defined above).
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">1.6</i>
                    <p>
                        "Price" shall mean the price payable for the Goods as agreed between the Seller and the Customer in accordance with clause 4 of this contract.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <p class="text-section-title">2. The Commonwealth Trade Practices Act 1974 ("TPA") and <span style="border-bottom: 3px double blue; font-size: 18px;">Fair Trading Acts</span> ("FTA")</p>

                    <i class="text-section-title">2.1</i>
                    <p>
                        Nothing in this agreement is intended to have the effect of contracting out of any applicable provisions 
                        of the TPA or the FTA in each of the States and Territories of Australia, except to the extent permitted by those Acts where applicable.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <p class="text-section-title">3. Acceptance</p>
                    <i class="text-section-title">3.1</i>
                    <p>
                        Any instructions received by the Seller from the Customer for the supply of Goods and/or the Customer's acceptance of Goods supplied by the Seller shall constitute acceptance of the terms and conditions contained herein.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">3.2</i>
                    <p>
                        Where more than one Customer has entered into this agreement, the Customers shall be jointly and severally liable for all payments of the Price.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">3.3</i>
                    <p>
                        Upon acceptance of these terms and conditions by the Customer the terms and conditions are binding and can only be amended with the written consent of the Seller.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">3.4</i>
                    The Customer shall give the Seller not less than fourteen (14) days prior written notice of any proposed change of ownership of the Customer or any change in the Customer's name and/or any other change in
                    the Customer's details (including but not limited to, changes in the Customer's address, facsimile number, or business practice). The Customer shall be liable for any loss incurred by the Seller as a result of the Customer's failure to comply with this clause.</div>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">3.5</i>
                    <p>
                        Goods are supplied by the Seller only on the terms and conditions of trade herein to the exclusion of anything to the contrary in the terms of the Customer's order notwithstanding that any such order is placed on terms that
                        purport to override these terms and conditions of trade.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <p class="text-section-title">4. Price <span style="border-bottom: 3px double blue; font-size: 18px;">And</span> Payment</p>
                    <i class="text-section-title">4.1</i>
                    <p>At the Seller's sole discretion the Price shall be either:</p>
                    <p class="i-text-clear-5"></p>

                    <p class="i-text-indent"><span class="i-span-inline">(a)</span> as indicated on invoices provided by the Seller to the Customer in respect of Goods supplied; or</p>
                    <p class="i-text-indent"><span class="i-span-inline">(b)</span> the Seller's current Price at the date of delivery of the Goods according to the Seller's current Stock Catalogue; or</p>
                    <p class="i-text-indent"><span class="i-span-inline">(c)</span> the Seller's quoted Price (subject to clause 4.2) which shall be binding upon the Seller provided that the Customer shall accept the Seller's quotation in writing within seven (7) days.</p>

                    <i class="text-section-title">4.2</i>
                    <p>At the Seller's sole discretion a non-refundable deposit may be required.:</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">4.3</i>
                    <p>At the Seller's sole discretion :</p>
                    <p class="i-text-clear-5"></p>
                    <p class="i-text-indent"><span class="i-span-inline">(a)</span> payment shall be due on delivery of the Goods; or</p>
                    <p class="i-text-indent"><span class="i-span-inline">(b)</span> payment for approved Customers shall be due thirty (30) days following the end of the month in which a statement is posted to the Customer's address or address for notices.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">4.4</i>
                    <p>Time for payment for the Goods shall be of the essence and will be stated on the invoice or any other forms. If no time is stated then payment shall be due seven (7) days following the date of the invoice.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">4.5</i>
                    <p>Payment will be made by cash, or by cheque, or by bank cheque, or by credit card (excluding Amex or Diners), or by direct credit, or by any other method as agreed to between the Customer and the Seller.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">4.6</i>
                    <p>GST and other taxes and duties that may be applicable shall be added to the Price except when they are expressly included in the Price.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">4.7</i>
                    <p>All prices are in Australian Dollars (AUD)</p>
                    <p class="i-text-clear-5"></p>

                    <div class="text-section-title">5. Delivery <span style="border-bottom: 3px double blue; font-size: 18px;">Of</span> Goods</div>

                    <div class="i-text-indent">
                    <i class="text-section-title">5.1</i>
                    <p>At the Seller's sole discretion delivery of the Goods shall take place when :</p>
                    <p class="i-text-clear-5"></p>
                    <p class="i-text-indent"><span class="i-span-inline">(a)</span> the Customer takes possession of the Goods at the Seller's address; or</p>
                    <p class="i-text-indent">
                        <span class="i-span-inline">(b)</span> the Customer takes possession of the Goods at the Customer's nominated address (in the event that the Goods are delivered by the Seller or the Seller's nominated carrier).
                    </p>

                    <i class="text-section-title">5.2</i>
                    <p>At the Seller's sole discretion the costs of delivery are in addition to the Price.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">5.3</i>
                    <p>
                        The Customer shall make all arrangements necessary to take delivery of the Goods whenever they are tendered for delivery. In the event that the Customer is unable to take delivery of the Goods as arranged then the Seller shall be entitled to charge a
	                    reasonable fee for redelivery.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">5.4</i>
                    <p>Delivery of the Goods to a third party nominated by the Customer is deemed to be delivery to the Customer for the purposes of this agreement.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">5.5</i>
                    <p>
                        The Customer shall take delivery of the Goods tendered notwithstanding that the quantity so delivered shall be either greater or lesser than the quantity purchased provided that :
                    </p>
                    <p class="i-text-clear-5"></p>

                    <p class="i-text-indent"><span class="i-span-inline">(a)</span>  such discrepancy in quantity shall not exceed ten percent (10%) on custom printed orders; and </p>
                    <p class="i-text-indent"><span class="i-span-inline">(b)</span>  the Price shall be adjusted pro rata to the discrepancy. </p>

                    <i class="text-section-title">5.6</i>

                    <p>The failure of the Seller to deliver shall not entitle either party to treat this contract as repudiated.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">5.7</i>
                    <p>The Seller shall not be liable for any loss or damage whatsoever due to failure by the Seller to deliver the Goods (or any of them) promptly or at all, where due to circumstances beyond the control of the Seller.</p>
                    <p class="i-text-clear-5"></p>
                </div>

                <div class="text-section-title">6. Risk</div>
                <div class="i-text-indent">
                    <i class="text-section-title">6.1</i>
                    <p>If the Seller retains ownership of the Goods nonetheless, all risk for the Goods passes to the Customer on delivery.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">6.2</i>
                    <p>
                        Where the Customer expressly requests the Seller to leave Goods outside the Seller's premises for collection or to deliver the Goods to an unattended location then such Goods shall be left at the Customer's
	                    sole risk and it shall be the Customer's responsibility to ensure the Goods are insured adequately or at all.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">6.3</i>
                    <p>
                        If any of the Goods are damaged or destroyed following delivery but prior to ownership passing to the Customer, the Seller is entitled to receive all insurance proceeds payable for the Goods.
                        The production of these terms and conditions by the Seller is sufficient evidence of the Seller's rights to receive the insurance proceeds without the need for any person dealing with the Seller to make further enquiries.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <div class="text-section-title">7. Title</div>
                    <i class="text-section-title">7.1</i>
                    <p>The Seller and the Customer agree that ownership of the Goods shall not pass until:</p>
                    <p class="i-text-indent"><span class="i-span-inline">(a)</span> the Customer has paid the Seller all amounts owing for the particular Goods; and</p>
                    <p class="i-text-indent">
                        <span class="i-span-inline">(b)</span> the Customer has met all other obligations due by the Customer to the Seller in respect of all contracts between the Seller and the Customer.
                    </p>

                    <i class="text-section-title">7.2</i>
                    <p>
                        Receipt by the Seller of any form of payment other than cash shall not be deemed to be payment until that form of payment has been honoured, cleared or recognised and until then the Seller's ownership or rights in respect of the Goods shall continue.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">7.3</i>
                    <p>It is further agreed that:</p>
                    <p class="i-text-indent">
                        <span class="i-span-inline">(a)</span> where practicable the Goods shall be kept separate and identifiable until the Seller shall have received payment and all other obligations of the Customer are met; and
                    </p>
                    <p class="i-text-indent">
                        <span class="i-span-inline">(b)</span> until such time as ownership of the Goods shall pass from the Seller to the Customer the Seller may give notice in writing to the Customer to return the Goods or any of them to the Seller. 
	                    Upon such notice the rights of the Customer to obtain ownership or any other interest in the Goods shall cease; and
                    </p>
                    <p class="i-text-indent">
                        <span class="i-span-inline">(c)</span> the Seller shall have the right of stopping the Goods in transit whether or not delivery has been made; and
                    </p>
                    <p class="i-text-indent">
                        <span class="i-span-inline">(d)</span> if the Customer fails to return the Goods to the Seller then the Seller or the Seller's agent may enter upon and into land and premises owned, occupied or used by the Customer, 
	                    or any premises as the invitee of the Customer, where the Goods are situated and take possession of the Goods; and
                    </p>
                    <p class="i-text-indent">
                        <span class="i-span-inline">(e)</span> the Customer is only a bailee of the Goods and until such time as the Seller has received payment in full for the Goods then the Customer shall hold any proceeds from the sale or disposal of the Goods, 
	                    up to and including the amount the Customer owes to the Seller for the Goods, on trust for the Seller; and
                    </p>
                    <p class="i-text-indent">
                        <span class="i-span-inline">(f)</span> the Customer shall not deal with the money of the Seller in any way which may be adverse to the Seller; and
                    </p>
                    <p class="i-text-indent">
                        <span class="i-span-inline">(g)</span> the Customer shall not charge the Goods in any way nor grant nor otherwise give any interest in the Goods while they remain the property of the Seller; and
                    </p>
                    <p class="i-text-indent">
                        <span class="i-span-inline">(h)</span> the Seller can issue proceedings to recover the Price of the Goods sold notwithstanding that ownership of the Goods may not have passed to the Customer; and
                    </p>
                    <p class="i-text-indent">
                        <span class="i-span-inline">(i)</span> until such time that ownership in the Goods passes to the Customer, if the Goods are converted into other products, the parties agree that the Seller will be the owner of the end products.
                    </p>
                </div>

                <div class="text-section-title">8. Defects</div>
                <div class="i-text-indent">
                    <i class="text-section-title">8.1</i>
                    <p>
                        The Customer shall inspect the Goods on delivery and shall within seven (7) days of delivery (time being of the essence) notify the Seller of any alleged defect, shortage in quantity, damage or failure to comply with the description or quote.
                        The Customer shall afford the Seller an opportunity to inspect the Goods within a reasonable time following delivery if the Customer believes the Goods are defective in any way. If the Customer shall fail to comply with these provisions
                        the Goods shall be presumed to be free from any defect or damage. For defective Goods, which the Seller has agreed in writing that the Customer is entitled to reject, 
                        the Seller's liability is limited to either (at the Seller's discretion) replacing the Goods or repairing the Goods except where the Customer has acquired Goods as a consumer within the meaning of the Trade Practices Act 1974 (CWlth) or the Fair Trading Acts of
                        the relevant state or territories of Australia, and is therefore also entitled to, at the consumer's discretion either a refund of the purchase Price of the Goods, or repair of the Goods, or replacement of the Goods.
                    </p>
                </div>

                <div class="text-section-title">9. Returns</div>
                <div class="i-text-indent">
                    <i class="text-section-title">9.1</i>
                    <p>
                        Returns will only be accepted provided that: 
                        <p class="i-text-indent"><span class="i-span-inline">(a)</span> the Customer has complied with the provisions of clause 8.1; and</p>
                        <p class="i-text-indent"><span class="i-span-inline">(b)</span> the Seller has agreed in writing to accept the return of the Goods; and</p>
                        <p class="i-text-indent"><span class="i-span-inline">(c)</span> the Goods are returned at the Customer's cost within fourteen (14) days of the delivery date; and</p>
                        <p class="i-text-indent"><span class="i-span-inline">(d)</span> the Seller will not be liable for Goods which have not been stored or used in a proper manner; and</p>
                        <p class="i-text-indent">
                            <span class="i-span-inline">(e)</span> the Goods are returned in the condition in which they were delivered and with all packaging material, brochures and 
                            instruction material in as new condition as is reasonably possible in the circumstances.
                        </p>

                        <i class="text-section-title">9.2</i>
                        <p>Non-stocklist items or Goods made to the Customer's specifications are under no circumstances acceptable for credit or return.</p>
                        <p class="i-text-clear-5"></p>

                        <i class="text-section-title">9.3</i>
                        <p>
                            The Seller may (at their sole discretion) accept the return of Goods for credit but this may incur a handling fee of up toten percent (10%) of the value of the returned Goods plus any freight costs.
                        </p>
                        <p class="i-text-clear-5"></p>

                        <i class="text-section-title">9.3</i>
                        <p>
                            The Seller may (at their sole discretion) accept the return of Goods for credit but this may incur a handling fee of up to ten percent (10%) of the value of the returned Goods plus any freight costs.
                        </p>
                        <p class="i-text-clear-5"></p>

                        <i class="text-section-title">9.4</i>
                        <p>Returns will not be accepted on any Thermal Paper Roll orders.</p>
                        <p class="i-text-clear-5"></p>
                </div>


                <div class="text-section-title">10. Intellectual Property</div>
                <div class="i-text-indent">
                    <i class="text-section-title">10.1</i>
                    <p>
                        Where the Seller has designed, drawn or written Goods for the Customer, then the copyright in those designs and drawings and documents shall remain vested in the Seller, and shall only be used by the Customer at the Seller's discretion.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">10.2</i>
                    <p>
                        The Customer warrants that all designs or instructions to the Seller will not cause the Seller to infringe any patent, registered design or trademark in the execution of the Customer's order and the Customer agrees to indemnify the Seller against any 
                        action taken by a third party against the Seller in respect of any such infringement.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">10.3</i>
                    <p>
                        Where the Seller has designed or drawn Goods for the Customer then the Customer undertakes to acknowledge the Sellers design or drawings in the event that images of the Goods are utilised in advertising or marketing material by the Customer.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">10.4</i>
                    <p>
                        The Customer hereby authorises the Seller to utilise images of the Goods designed or drawn by the Seller in advertising, marketing, or competition material by the Seller.
                    </p>
                    <p class="i-text-clear-5"></p>

                </div>

                <div class="text-section-title">11. Default & Consequences of Default</div>
                <div class="i-text-indent">
                    <i class="text-section-title">11.1</i>
                    <p>
                        Interest on overdue invoices shall accrue daily from the date when payment becomes due, until the date of payment, at a rate of <span style="border-bottom: 3px double blue; font-size: 18px;">two and one half</span> percent (2.5%) per calendar month (and at the Seller's sole discretion such interest shall compound monthly at such a rate) 
                        after as well as before any judgment.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">11.2</i>
                    <p>In the event that the Customer's payment is dishonoured for any reason the Customer shall be liable for any dishonour fees incurred by the Seller.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">11.3</i>
                    <p>
                        If the Customer defaults in payment of any invoice when due, the Customer shall indemnify the Seller from and against all costs and disbursements incurred by the Seller in pursuing the debt including legal costs on a solicitor and own client basis and the Seller's collection agency costs.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">11.4</i>
                    <p>
                        Without prejudice to any other remedies the Seller may have, if at any time the Customer is in breach of any obligation (including those relating to payment) the Seller may suspend or terminate the supply of Goods to the Customer and any of its other obligations
                        under the terms and conditions. The Seller will not be liable to the Customer for any loss or damage the Customer suffers because the Seller has exercised its rights under this clause.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">11.5</i>
                    <p>
                        If any account remains overdue after thirty (30) days then an amount of the greater of twenty dollars ($20.00) or ten percent (10%) of the amount overdue (up to a maximum of two hundred dollars ($200.00)) shall be levied for administration 
                        fees which sum shall become immediately due and payable.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">11.6</i>
                    <p>
                        Without prejudice to the Seller's other remedies at law the Seller shall be entitled to cancel all or any part of any order of the Customer which remains unfulfilled and all amounts owing to the Seller shall, whether or not due for payment, become immediately 
                        payable in the event that :
                    </p>

                    <p class="i-text-indent">
                        <span class="i-span-inline">(a)</span> any money payable to the Seller becomes overdue, or in the Seller's opinion the Customer will be unable to meet its payments as they fall due; or 
                        <p class="i-text-indent">
                            <span class="i-span-inline">(b)</span> the Customer becomes insolvent, convenes a meeting with its creditors or proposes or enters into an arrangement with creditors, or makes an assignment for the benefit of its creditors; or 
                            <p class="i-text-indent">
                                <span class="i-span-inline">(c)</span> a receiver, manager, liquidator (provisional or otherwise) or similar person is appointed in respect of the Customer or any asset of the Customer.
                </div>

                <div class="text-section-title">12. Security <span style="border-bottom: 3px double blue; font-size: 18px;">And</span> Charge</div>
                <div class="i-text-indent">
                    <i class="text-section-title">12.1</i>
                    <p>Despite anything to the contrary contained herein or any other rights which the Seller may have howsoever :</p>
                    <p class="i-text-indent">
                        <span class="i-span-inline">(a)</span> where the Customer and/or the Guarantor (if any) is the owner of land, realty or any other asset capable of being charged, both the Customer and/or the Guarantor agree to mortgage and/or charge all of their joint and/or several interest in the said land, realty or any other asset to the Seller or the Seller's nominee to secure all amounts and other monetary obligations payable under these terms and conditions. The Customer and/or the Guarantor acknowledge and agree that the Seller (or the Seller's nominee) shall be entitled to lodge where appropriate a caveat, which caveat shall be withdrawn once all payments and other monetary obligations payable hereunder have been met. 
                        <p class="i-text-indent">
                            <span class="i-span-inline">(b)</span> should the Seller elect to proceed in any manner in accordance with this clause and/or its sub-clauses, the Customer and/or Guarantor shall indemnify the Seller from and against all the Seller's costs and disbursements including legal costs on a solicitor and own client basis. 
                            <p class="i-text-indent">
                                <span class="i-span-inline">(c)</span>
                    the Customer and/or the Guarantor (if any) agree to irrevocably nominate constitute and appoint the Seller or the Seller's nominee as the Customer's and/or Guarantor's true and lawful attorney to perform all necessary acts to give effect to the provisions of this clause 12.1.
                </div>

                <div class="text-section-title">13. Cancellation</div>
                <div class="i-text-indent">
                    <i class="text-section-title">13.1</i>
                    <p>
                        The Seller may cancel any contract to which these terms and conditions apply or cancel delivery of Goods at any time before the Goods are delivered by giving written notice to the Customer. On giving such notice the Seller shall repay to the Customer any sums paid in respect of the Price. The Seller shall not be liable for any loss or damage whatsoever arising from such cancellation.
                    </p>

                    <i class="text-section-title">13.2</i>
                    <p>
                        In the event that the Customer cancels delivery of Goods the Customer shall be liable for any loss incurred by the Seller (including, but not limited to, any loss of profits) up to the time of cancellation.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">13.3</i>
                    <p>
                        Cancellation of orders for Goods made to the Customer's specifications or non-stocklist items will definitely not be accepted, once production has commenced.
                    </p>
                    <p class="i-text-clear-5"></p>
                </div>

                <div class="text-section-title">14. Privacy Act 1988</div>
                <div class="i-text-indent">
                    <i class="text-section-title">14.1</i>
                    <p>
                        The Customer and/or the Guarantor/s agree for the Seller to obtain from a credit reporting agency a credit report containing personal credit information about the Customer and Guarantor/s in relation to credit provided by the Seller.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">14.2</i>
                    <p>
                        The Customer and/or the Guarantor/s agree that the Seller may exchange information about the Customer and the Guarantor/s with those credit providers either named as trade referees by the Customer or named in a consumer credit report issued 
	                    by a credit reporting agency for the following purposes:
                    </p>

                    <p class="i-text-indent">
                        <span class="i-span-inline">(a)</span> to assess an application by Customer; and/or 
	                <p class="i-text-indent">
                        <span class="i-span-inline">(b)</span> to notify other credit providers of a default by the Customer; and/or 
	                <p class="i-text-indent">
                        <span class="i-span-inline">(c)</span> to exchange information with other credit providers as to the status of this credit account, where the Customer is in default with other credit providers; and/or 
	                <p class="i-text-indent">
                        <span class="i-span-inline">(d)</span> to assess the credit worthiness of Customer and/or Guarantor/s.
	
	                <i class="text-section-title">14.3</i>
                        <p>
                            The Customer consents to the Seller being given a consumer credit report to collect overdue payment on commercial credit (Section 18K(1)(h) Privacy Act 1988).
                        </p>
                        <p class="i-text-clear-5"></p>

                        <i class="text-section-title">14.4</i>
                        <p>
                            The Customer agrees that personal credit information provided may be used and retained by the Seller for the following purposes and for other purposes as shall be agreed between the Customer and Seller or required by law from time to time:
                        </p>

                        <p class="i-text-indent">
                            <span class="i-span-inline">(a)</span> provision of Goods; and/or </p>
	                    <p class="i-text-indent">
                            <span class="i-span-inline">(b)</span> marketing of Goods by the Seller, its agents or distributors in relation to the Goods; and/or </p>
	                    <p class="i-text-indent">
                            <span class="i-span-inline">(c)</span> analysing, verifying and/or checking the Customer's credit, payment and/or status in relation to provision of Goods; and/or </p>
	                    <p class="i-text-indent">
                            <span class="i-span-inline">(d)</span> processing of any payment instructions, direct debit facilities and/or credit facilities requested by Customer; and/or </p>
	                    <p class="i-text-indent">
                            <span class="i-span-inline">(e)</span> enabling the daily operation of Customer's account and/or the collection of amounts outstanding in the Customer's account in relation to the Goods.</p>
	
	                    <i class="text-section-title">14.5</i>
                            <p>The Seller may give information about the Customer to a credit reporting agency for the following purposes: </p>
	                    <p class="i-text-indent">
                            <span class="i-span-inline">(a)</span> to obtain a consumer credit report about the Customer; and/or </p>
	                    <p class="i-text-indent">
                            <span class="i-span-inline">(b)</span>
                        allow the credit reporting agency to create or maintain a credit information file containing information about the Customer.</p>
                </div>

                <div class="text-section-title">15. General</div>
                <div class="i-text-indent">
                    <i class="text-section-title">15.1</i>
                    <p>
                        If any provision of these terms and conditions shall be invalid, void, illegal or unenforceable the validity, existence, legality and enforceability of the remaining provisions shall not be affected, prejudiced or impaired.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">15.2</i>
                    <p>
                        These terms and conditions and any contract to which they apply shall be governed by the laws of New South Wales and are subject to the jurisdiction of the courts of New South Wales.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">15.3</i>
                    <p>
                        The Seller shall be under no liability whatsoever to the Customer for any indirect and/or consequential loss and/or expense (including loss of profit) suffered by the Customer arising out of a breach by the Seller of these terms and conditions.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">15.4</i>
                    <p>
                        In the event of any breach of this contract by the Seller the remedies of the Customer shall be limited to damages which under no circumstances shall exceed the Price of the Goods.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">15.5</i>
                    <p>
                        The Customer shall not be entitled to set off against, or deduct from the Price, any sums owed or claimed to be owed to the Customer by the Seller nor to withhold payment of any invoice because part of that invoice is in dispute.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">15.6</i>
                    <p>The Seller may license or sub-contract all or any part of its rights and obligations without the Customer's consent.</p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">15.7</i>
                    <p>
                        The Customer agrees that the Seller may review these terms and conditions at any time. If, following any such review, there is to be any change to these terms and conditions, then that change will take effect from the date on which the Seller notifies
	                    the Customer of such change.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">15.8</i>
                    <p>
                         Neither party shall be liable for any default due to any act of God, war, terrorism, strike, lock-out, industrial action, fire, flood, storm or other event beyond the reasonable control of either party.
                    </p>
                    <p class="i-text-clear-5"></p>

                    <i class="text-section-title">15.9</i>
                    <p>
                        The failure by the Seller to enforce any provision of these terms and conditions shall not be treated as a waiver of that provision, nor shall it affect the Seller's right to subsequently enforce that provision.
                    </p>
                    <p class="i-text-clear-5"></p>
                </div>
        </div>
    </form>
</body>
</html>
