﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="contact-us.aspx.cs" Inherits="InterpriseSuiteEcommerce.contact_us" %>

<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <ise:InputValidatorSummary ID="errorSummary" CssClass="error float-left normal-font-style" runat="server" Register="False" />
    <div class="clear-both height-5">&nbsp;</div>
    <form id="frmContactUs" runat="server" action="">
        <asp:Panel ID="pnlPageContentWrapper" runat="server">
            <h1>Contact Us</h1>
            <p style="font-size: 14px; font-weight: 500;">Please supply your details and we will respond to your request within 1 business day.</p>
            <div class="clear-both height-20"></div>
            <div class="clear-both height-5"></div>
            <!-- Form Left Section Starts Here -->
            <div id="divFormLeft" class="contact-us-left-container">
                <h2 style="margin-right: 5px">Your Contact Information</h2>
                <div style="text-align: right; color: #ff0000; font-weight: 400; font-size: 12px; padding-bottom: 20px;">
                    *Required field
                </div>
                <%--Contact Name and ContactNumber Input Box Section Starts Here--%>
                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-fname">
                        <label id="lblFirstName" class="form-field-label">
                            Fist Name<font style="color: red">*</font>
                        </label>
                        <br />
                        <asp:TextBox ID="txtFirstName" MaxLength="100" class="light-style-input" runat="server"></asp:TextBox>
                    </span>
                    <span class="form-controls-span form-controls-span-spaceleft form-control-lname">
                        <label id="lblLastName" class="form-field-label">
                            Last Name<font style="color: red">*</font>
                        </label>
                        <br />
                        <asp:TextBox ID="txtLastName" MaxLength="50" class="light-style-input" runat="server"></asp:TextBox>
                    </span>
                </div>

                <div class="clear-both height-12"></div>

                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-email">
                        <label id="lblEmail" class="form-field-label">
                            <asp:Literal ID="litEmail" runat="server">(!contactus.aspx.5!)</asp:Literal>
                        </label>
                        <br />
                        <asp:TextBox ID="txtEmail" MaxLength="50" runat="server" class="light-style-input"></asp:TextBox>
                    </span>
                </div>
                <div class="clear-both height-12"></div>

                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-mobile">
                        <label id="lblMobile" class="form-field-label">
                            Mobile Number ( Optional )
                        </label>
                        <br />
                        <asp:TextBox ID="txtMobile" MaxLength="20" runat="server" class="light-style-input" runat="server"></asp:TextBox>
                    </span>
                    <span class="form-controls-span form-controls-span-spaceleft form-control-tel">
                        <label id="lblPhone" class="form-field-label">
                            Phone Number 
                        </label>
                        <br />
                        <asp:TextBox ID="txtPhone" MaxLength="20" runat="server" class="light-style-input" runat="server"></asp:TextBox>
                    </span>
                </div>
                <div class="clear-both height-12"></div>
                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-company">
                        <label id="lblCompany" class="form-field-label">
                            Company<font style="color: red">*</font>
                        </label>
                        <asp:TextBox ID="txtCompany" MaxLength="20" runat="server" class="light-style-input" runat="server"></asp:TextBox>
                    </span>
                </div>
                <div class="clear-both height-12"></div>
                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-industry">
                        <label id="lblIndustry" class="form-field-label">
                            Industry Group
                        </label>
                        <br />
                        <asp:DropDownList ID="drpIndustryGroup" runat="server"></asp:DropDownList>
                    </span>
                </div>
                <div class="clear-both height-12"></div>
                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-address">
                        <label id="lblAddress" class="form-field-label">
                            Address ( Optional )
                        </label>
                        <asp:TextBox ID="txtAddress" TextMode="MultiLine" MaxLength="50" runat="server" class="light-style-input"></asp:TextBox>
                    </span>
                </div>
                <div class="clear-both height-12"></div>
                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-city">
                        <label id="lblCity" class="form-field-label">
                            City / Suburb ( Optional )
                        </label>
                        <asp:TextBox ID="txtCity" MaxLength="50" runat="server" class="light-style-input"></asp:TextBox>
                    </span>
                </div>
                <div class="clear-both height-12"></div>

                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-state">
                        <label id="lblState" class="form-field-label">State</label><br />
                        <asp:DropDownList ID="drpState" runat="server"></asp:DropDownList>
                    </span>
                    <span class="form-controls-span form-controls-span-spaceleft  form-control-postal">
                        <label id="lblPostal" class="form-field-label">
                            Postal Code<font style="color: red">*</font>
                        </label>
                        <br />
                        <asp:TextBox ID="txtPostal" MaxLength="50" runat="server" class="light-style-input"></asp:TextBox>
                    </span>

                </div>
                <asp:TextBox ID="txtCountry" MaxLength="50" runat="server" class="light-style-input i-hide"></asp:TextBox>
                <asp:TextBox ID="txtState" MaxLength="50" runat="server" class="light-style-input i-hide"></asp:TextBox>
                <asp:TextBox ID="txtIndustryGroup" MaxLength="50" runat="server" class="light-style-input i-hide"></asp:TextBox>

                <div class="clear-both height-12"></div>

                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-country">
                        <label id="lblCountry" class="form-field-label">Country</label><br />
                        <asp:TextBox ID="drpCountry" runat="server" ReadOnly="true"></asp:TextBox>
                    </span>
                </div>
                <div class="clear-both height-12"></div>
                <div class="clear-both height-12"></div>
                <p>
                    <i>If your question is regarding any of our products, please indicate which
                                    products in more detail.</i>
                </p>
                <div class="form-controls-place-holder">
                    <span class="form-controls-span form-control-comments">
                        <label id="lblMessageDetails" class="form-field-label">
                            <asp:Literal ID="litMessageDetails" runat="server">Comments<font style="color:red">*</font></asp:Literal>
                        </label>
                        <asp:TextBox ID="txtMessageDetails" Rows="12" TextMode="MultiLine" runat="server" class="light-style-input" Columns="55"></asp:TextBox>
                    </span>
                </div>

                <%-- Security Code / Captcha Section Starts Here --%>
                <asp:Panel runat="server" ID="pnlSecurityCode" Style="text-align: right;">
                    <div class="clear-both height-5"></div>
                    <div class="clear-both height-25"></div>
                    <span class="form-controls-span custom-font-style capitalize-text">
                        <asp:Literal ID="LitEnterSecurityCodeBelow" runat="server">(!customersupport.aspx.12!)</asp:Literal>
                    </span>
                    <div class="clear-both height-12"></div>
                    <span class="form-controls-span">
                        <label id="lblCaptcha" class="form-field-label">
                            <asp:Literal ID="litCaptcha" runat="server">(!customersupport.aspx.13!)</asp:Literal>
                        </label>
                        <asp:TextBox ID="txtCaptcha" runat="server" class="light-style-input"></asp:TextBox>
                    </span>
                    <div class="clear-both height-12"></div>
                    <div id="captcha-image">
                        <img alt="captcha" src="Captcha.ashx?id=1" id="captcha" />
                    </div>
                    <div class="clear-both height-5"></div>
                    <a href="javascript:void(1);" style="color: red; position: relative; font-weight: 400; font-size: 10px;" id="i-get-another-captcha">Get Another Captcha</a>
                </asp:Panel>
                <%-- Security Code / Captcha Section Ends Here --%>
                <div class="clear-both height-20"></div>
                <div class="height-20"></div>

                <%-- Form Lower Section : Button Container Starts Here --%>
                <div id="contact-form-button-place-holder">
                    <div id="send-message-button">
                        <div id="send-message-loader"></div>
                        <div id="save-case-button-place-holder" style="text-align: right; padding-right: 5px;">
                            <asp:Button ID="btnSendMessage" runat="server" OnClientClick="if (!formInfoIsGood()) {return false;};" Text="" UseSubmitBehavior="False" CssClass="site-button shoppingcart-primary-button" />
                        </div>
                    </div>
                </div>
                <div class="clear-both height-20"></div>
                <%-- Form Lower Section : Button Container Ends Here --%>
            </div>
            <%--Form Left Section Ends Here--%>

            <%-- Form Right Section Starts Heree--%>
            <div id="divFormRight" class="contact-us-right-container">
                <div class="contact-us-product-office">
                    <h2>Head Office</h2>
                    <div class="section-values">
                        Smart Group Enterprises Pty Ltd
                                    <br />
                        Unit 20,19 Aero Road<br />
                        INGLEBURN, NSW, 2565<br />
                        Australia<br />
                        <p class="height-10"></p>
                        <div class="section-values" style="font-weight: 400;">
                            PH : 1300 874 559<br />
                            FAX : +61 2 9605 1711<br />
                            EMAIL : sales@smartbag.com.au<br />
                        </div>
                        <p class="i-text-clear-5"></p>
                        <img src="skins/Skin_(!SKINID!)/images/contact-supporting.png" />
                    </div>
                </div>
                <div class="clear-both height-20"></div>
                <div class="contact-us-product-help">
                    <h3>Need Help?<br />
                        Have a question?</h3>
                    <div class="height-5"></div>
                    <div class="section-values">
                        If you have an enquiry or question about something you cannot find on the site then please get in touch using the form below.
                                    <div class="height-20"></div>
                        For bag quotes and digital print quotes, please browse our products and when you find what you are looking for, click the quote link next to the product.
                    </div>
                </div>
            </div>
            <%-- Form Right Section Ends Here--%>
        </asp:Panel>
        <script type="text/javascript" src="jscripts/minified/contactus.js"></script>
    </form>
</body>
</html>
