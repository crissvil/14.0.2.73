﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="privacy-policy.aspx.cs" Inherits="InterpriseSuiteEcommerce.privacy_policy" %>

<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Privacy Policy</title>
</head>
<body>
    <div class="clear-both height-5">&nbsp;</div>
    <form id="frmPrivacyPolicy" runat="server">
            <h1>Privacy Policy</h1>
            <div class="content-box">
                <p>
                    Smartgroup Enterprises Pty Ltd. ACN  076 136 760/ABN  51 076 136 760  recognise the importance of protecting the privacy of information collected, 
        in particular information that is capable of identifying an individual (“personal information”). 
                </p>
                <p>
                    This Privacy Policy governs the manner in which your personal information will be dealt with which adheres to the Australian Privacy principles and we do so in the following ways:
                </p>

                <p class="text-section-title">Personal Information</p>

                <p>
                    Personal information is collected only when knowingly and voluntarily submitted. For example, we may need to collect such information to provide you with further services or to answer or forward any requests or enquiries. It is our intention that this policy will protect your personal information from being dealt with in any way that is inconsistent with applicable privacy laws in Australia.
                </p>

                <p class="text-section-title">User Of Information</p>
                <p>
                    Personal information that visitors submit to us is used only for the purpose for which it is submitted or for such other secondary purposes that are related to the primary purpose, unless we disclose other uses in this Privacy Policy or at the time of collection. Copies of correspondence sent that may contain personal information, are stored as archives for record-keeping and back-up purposes only.
                </p>

                <p class="text-section-title">Disclosure</p>
                <p>
                    Apart from where you have consented or disclosure is necessary to achieve the purpose for which it was submitted, personal information may be disclosed in special situations where we have reason to believe that doing so is necessary to identify, contact or bring legal action against anyone damaging, injuring, or interfering (intentionally or unintentionally) with our rights or property, users, or anyone else who could be harmed by such activities. Also, we may disclose personal information when we believe in good faith that the law requires disclosure.
                </p>

                <p class="text-section-title">Security</p>

                <p>
                    We strive to ensure the security, integrity and privacy of personal information and we review and update our security measures in light of current technologies.
                </p>
                <p>
                    We will endeavour to take all reasonable steps to protect the personal information you may transmit to us or from our online products and services. Once we do receive your transmission, we will also make our best efforts to ensure its security on our systems.
                </p>
                <p>
                    In addition, our employees and the contractors who provide services related to our information systems are obliged to respect the confidentiality of any personal information held by us. However, we will not be held responsible for events arising from unauthorised access to your personal information.
                </p>

                <p class="text-section-title">Access to Information</p>
                <p>
                    We will endeavour to take all reasonable steps to keep secure any information which we hold about you, and to keep this information accurate and up to date. If, at any time, you discover that information held about you is incorrect, you may contact us to have the information corrected.
                </p>
                <p>
                    In addition, our employees and the contractors who provide services related to our information systems are obliged to respect the confidentiality of any personal information held by us.
                </p>

                <p class="text-section-title">Purposes for which we collect, hold, use and disclose personal information</p>
                <i>We collect, hold, use and disclose your personal information for the following purposes :</i>

                <ul class="i-list-items">
                    <li>To provide products and services to you and/or your organisation and to send communications requested by you or your organisation. </li>
                    <li>We will disclose your information only to our associated companies only for the purposes of direct marketing and never to a third party neither in Australia nor overseas.</li>
                    <li>To answer enquiries and provide information or advice about existing and new products or services.</li>
                    <li>To process and respond to any complaint made by you.</li>
                    <li>To update our records and keep your contact details up to date.</li>
                    <li>To protect any person from death or serious bodily injury.</li>
                    <li>To protect our rights.</li>
                    <li>Your personal information will not be sold or rented for any reason.</li>
                    <li>Your personal information will not be shared or disclosed other than as described in this Privacy Policy.</li>
                </ul>
                <p class="text-section-title">Links To Other Sites</p>
                <p>
                    We provide links to Websites outside of our websites, as well as to third party Websites. These linked sites are not under our control, and we cannot accept responsibility for the conduct of companies linked to our website. Before disclosing your personal information on any other website, we advise you to examine the terms and conditions of using that Website and its Privacy Statement/Policy.
                </p>
                <p class="text-section-title">Problems or Questions</p>
                <p>
                    If we become aware of any ongoing concerns or problems, we will take these issues seriously and work to address these concerns. If you have any further queries relating to our Privacy Policy, or you have a problem or complaint, please contact us 02 9531 8000 or sales@sge.net.au
                </p>
                <p class="text-section-title">Further Privacy Information</p>
                <p>
                    For more information about privacy issues in Australia and protecting your privacy, visit the Australian Federal Privacy Commissioner’s website at 
        <a href="http://www.privacy.gov.au.">http://www.privacy.gov.au.</a>
                </p>
            </div>
    </form>
</body>
</html>
