﻿using InterpriseSuiteEcommerceCommon;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerce
{
    public partial class portalapprovalsemail : SkinBase
    {
        Customer ThisCustomer = Customer.Current;
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.CacheControl = "private";
            Response.Expires = 0;
            Response.AddHeader("pragma", "no-cache");
            //RequiresLogin(CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables("QUERY_STRING"));
            RequireSecurePage();
            var status = Request.QueryString["status"];
            InitializePageContent(status);
        }

        private void InitializePageContent(string status)
        {
            StringBuilder content = new StringBuilder();
            if (status == "done")
            {
                content.AppendLine("<script>");
                content.AppendLine("$(document).ready(function () {");
                content.AppendLine("ShowMessageBoxPopup('Emails sent');");
                content.AppendLine("});");
                content.AppendLine("</script>");
                ltPageContent.Text = content.ToString();
            }
            else if (status != null)
            {
                ltPageContent.Text = status;
            }
            else
            {
                content.AppendLine("<script>");
                content.AppendLine("$(document).ready(function () {");
                content.AppendLine("ShowModalPopup(\"Processing approvals email...\");");
                content.AppendLine("});");
                content.AppendLine("</script>");
                ltPageContent.Text = content.ToString();
                bool processed = false;
                processed = ProcessApprovals("Harvey Norman Cost Centre");
                processed = ProcessApprovals("Smartbag-PREMIUM APG");
                processed = ProcessApprovals("Smartbag-PREMIUM CUE");
                processed = ProcessApprovals("Smartbag-PREMIUM DION LEE");
                processed = ProcessApprovals("Smart Colour Print");
                if (processed == true)
                {
                    Response.Redirect("portalapprovalsemail.aspx?status=done");
                }
                else
                {
                    Response.Redirect("portalapprovalsemail.aspx?status=" + ltPageContent.Text);
                }
            }
        }

        private bool ProcessApprovals(string CostCenter)
        {
            bool exists = false;
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT 1 FROM InventoryItem A INNER JOIN InventoryItemDescription B ON A.ItemCode = B.ItemCode " +
	                    "INNER JOIN (SELECT A.PurchaseOrderCode, ItemCode, QuantityOrdered, IsApproved_DEV004817, IsDeclined_DEV004817 FROM SupplierPurchaseOrder A INNER JOIN SupplierPurchaseOrderDetail B ON A.PurchaseOrderCode = B.PurchaseOrderCode WHERE Type = 'Purchase Order' AND IsVoided != '1') AS C ON C.ItemCode = A.ItemCode " +
	                    "LEFT JOIN (SELECT B.ItemCode, SUM(B.QuantityOrdered) AS QuantityOrdered FROM CustomerSalesOrder A INNER JOIN CustomerSalesOrderDetail B ON A.SalesOrderCode = B.SalesOrderCode WHERE OrderStatus = 'Open' AND Type IN ('Sales Order', 'Back Order') GROUP BY B.ItemCode) AS D ON D.ItemCode = A.ItemCode " +
                        "WHERE B.LanguageCode = {0} AND A.CostCenter_DEV004817 = {1} AND A.Status = 'A' AND ISNULL(C.IsApproved_DEV004817, 0) = 0 AND ISNULL(IsDeclined_DEV004817, 0) = 0 AND C.PurchaseOrderCode IS NOT NULL AND C.PurchaseOrderCode NOT IN (SELECT PurchaseOrderCode FROM CompanyApprovalsEmail_C)", DB.SQuote(ThisCustomer.LanguageCode), DB.SQuote(CostCenter)))
                {
                    if (reader.Read())
                    {
                        exists = true;
                    }
                }
            }
            if (exists)
            {
                SendEmails(CostCenter);
                return UpdateReorderApprovals(CostCenter);
            }
            else
            {
                return true;
            }
        }

        private bool UpdateReorderApprovals(string CostCenter)
        {
            try
            {
                DB.ExecuteSQL(string.Format("InsertCompanyApprovalsEmail_C {0}, {1}, {2}, {3}", DB.SQuote(ThisCustomer.LanguageCode), "A", DB.SQuote(CostCenter), DB.SQuote(ThisCustomer.ContactCode)));
            }
            catch (Exception ex)
            {
                ltPageContent.Text = "An error was encountered: " + ex.Message;
                return false;
            }
            return true;
        }

        private bool SendEmails(string CostCenter)
        {
            List<XmlPackageParam> runtimeParams = new List<XmlPackageParam>();
            runtimeParams.Add(new XmlPackageParam("CostCenterApprovalEmail", CostCenter));
            var msg = AppLogic.RunXmlPackage("notification.reorderapprovalsemail.xml.config", base.GetParser, ThisCustomer, SkinID, String.Empty, runtimeParams, true, true);
            var subject = string.Empty;
            string receipient = string.Empty;
            switch (CostCenter)
            {
                case "Harvey Norman Cost Centre":
                    receipient = AppLogic.AppConfig("custom.hn.approval.email.notification");
                    subject = AppLogic.GetString("portal.aspx.43");
                    break;
                case "Smartbag-PREMIUM APG":
                    receipient = AppLogic.AppConfig("custom.apg.approval.email.notification");
                    subject = AppLogic.GetString("portal.aspx.53");
                    break;
                case "Smartbag-PREMIUM CUE":
                    receipient = AppLogic.AppConfig("custom.cue.approval.email.notification");
                    subject = AppLogic.GetString("portal.aspx.54");
                    break;
                case "Smartbag-PREMIUM DION LEE":
                    receipient = AppLogic.AppConfig("custom.dl.approval.email.notification");
                    subject = AppLogic.GetString("portal.aspx.55");
                    break;
                case "Smart Colour Print":
                    receipient = AppLogic.AppConfig("custom.dl.approval.email.notification");
                    subject = AppLogic.GetString("portal.aspx.56");
                    break;
            }
            
            if (msg.Length > 0)
            {
                AppLogic.SendMail(subject, msg, true, null, null, receipient, receipient, "", AppLogic.AppConfig("MailMe_Server"));
                return true;
            }
            return false;
        }
    }
}