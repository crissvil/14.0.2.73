<%@ Page Language="C#" AutoEventWireup="true" CodeFile="fabric-colours.aspx.cs" Inherits="InterpriseSuiteEcommerce.fabric_colours" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<title>Fabric Colours | (!METATITLE!)</title>
</head>
<body>
<div class="clear-both height-5">&nbsp;</div>
<form id="frmFabricColours" runat="server" action=>
<asp:Panel ID="pnlPageContentWrapper" runat="server">

    <h1>Fabric Colours</h1>
    <ul class="c-fabric-colours">
    	<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/lime.png" alt="lime" /></a>
    		<span>Lime</span>
    	</li>
    	<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/pine.png" alt="pine" /></a>
		    <span>Pine</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/bottle.png" alt="bottle" /></a>
		    <span>Bottle</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/dalton.png" alt="dalton" /></a>
		    <span>Dalton</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/royal.png" alt="royal" /></a>
		    <span>Royal</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/electric.png" alt="electric" /></a>
		    <span>Electric</span>
		</li>
    	<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/navy.png" alt="navy" /></a>
    		<span>Navy</span>
    	</li>
    	<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/slate.png" alt="slate" /></a>
			<span>Slate</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/white.png" alt="white" /></a>
			<span>White</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/natural.png" alt="natural" /></a>
			<span>Natural</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/pink.png" alt="pink" /></a>
			<span>Pink</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/lavender.png" alt="lavender" /></a>
			<span>Lavender</span>
		</li>
    	<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/sunflower.png" alt="sunflower" /></a>
    		<span>Sunflower</span>
    	</li>
    	<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/jaffa.png" alt="jaffa" /></a>
			<span>Jaffa</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/red.png" alt="red" /></a>
			<span>Red</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/burgundy.png" alt="burgundy" /></a>
			<span>Burgundy</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/chocolate.png" alt="chocolate" /></a>
			<span>Chocolate</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/black.png" alt="black" /></a>
			<span>Black</span>
		</li>
    	<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/sand.png" alt="sand" /></a>
    		<span>Sand</span>
    	</li>
    	<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/cool-grey.png" alt="coolgrey" /></a>
			<span>Cool Grey</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/hot-pink.png" alt="hotpink" /></a>
			<span>Hot Pink</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/grape.png" alt="grape" /></a>
			<span>Grape</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/teal.png" alt="teal" /></a>
			<span>Teal</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/baby-pink.png" alt="babypink" /></a>
			<span>Baby Pink</span>
		</li>
    	<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/purple.png" alt="purple" /></a>
    		<span>Purple</span>
    	</li>
    	<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/cargo.png" alt="cargo" /></a>
			<span>Cargo</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/torquoise.png" alt="torquoise" /></a>
			<span>Torquoise</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/tennis-ball.png" alt="tennisball" /></a>
			<span>Tennis Ball</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/aqua.png" alt="aqua" /></a>
			<span>Aqua</span>
		</li>
		<li>
    		<a href="#"><img src="skins/Skin_(!SKINID!)/images/fabric-colours/shiraz.png" alt="shiraz" /></a>
			<span>Shiraz</span>
		</li>
    </ul>   

</asp:Panel>

</form>
</body>
</html>