﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UploadLogo.aspx.cs" Inherits="InterpriseSuiteEcommerce.UploadLogo" %>

<%@ Import Namespace="InterpriseSuiteEcommerceCommon" %>
<%@ Import Namespace="InterpriseSuiteEcommerceCommon.Extensions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload Logo</title>
    <script type="text/javascript" src="jscripts/jquery/jquery.validate.js"></script>
    <style>
        .webform label {
            font-size: 15px;
        }

        input.cat_button {
            background: #f28b00;
            border: none;
            padding: 3px 30px;
            color: #fff;
            text-transform: uppercase;
        }
    </style>
</head>
<body>
    <asp:Panel runat="server" ID="pnlContent">
        <form id="formUpload" runat="server">
            <div class="row1">
                <div class="custom-upload-form">
                    <h1>Upload Logo</h1>
                    <p style="line-height: 20px;">Getting the artwork right is the most important part of any&nbsp;Express Print&nbsp;job. The quality of the bag will depend on the quality of the logo we are supplied. </p>
                    <div class="height-5"></div>
                    <p style="line-height: 20px;">
                        Using the form below please upload the artwork. We only accept:
                        <br />
                        PDF<br />
                        EPS
                    </p>
                    <div class="height-5"></div>
                    <p>An artwork recreation fee may be charged if the artwork supplied is of&nbsp;poor quality or not one of our approved file types.</p>
                    <p>If you have any trouble uploading the artwork contact us on <strong>02&nbsp;9531 8000</strong></p>
                    <p>Use the following form to upload your artwork.</p>

                    <p>
                        Fields marked with an asterisk (<span class="req">*</span>) are required.
                    </p>
                    <div style="height: 50px;"></div>
                    <div class="form-controls-place-holder">
                        <span class="form-controls-span form-control-orderid">
                            <label for="OrderId">Order ID</label>
                            <br />
                            <input type="text" readonly="true" maxlength="255" class="cat_textbox control-disabled light-style-input" id="OrderId" name="OrderId" />
                        </span>
                        <span class="form-controls-span form-controls-span-spaceleft form-control-email">
                            <label for="EmailAddress">Email Address</label>
                            <br />
                            <input type="text" readonly="true" maxlength="255" class="light-style-input cat_textbox control-disabled" id="EmailAddress" name="EmailAddress" />
                        </span>
                    </div>
                    <div class="clear-both height-25"></div>
                    <div class="form-controls-place-holder">
                        <span class="form-controls-span form-control-fname">
                            <label for="FirstName">First Name</label>
                            <br />
                            <input type="text" readonly="true" maxlength="255" class="cat_textbox control-disabled light-style-input" id="FirstName" name="FirstName"/>
                        </span>
                        <span class="form-controls-span form-controls-span-spaceleft form-control-lname">
                            <label for="LastName">Last Name</label>
                            <br />
                            <input type="text" readonly="true" maxlength="255" class="cat_textbox control-disabled light-style-input" id="LastName" name="LastName"/>
                        </span>
                    </div>
                    <div class="clear-both height-25"></div>
                    <div class="form-controls-place-holder">
                        <span class="form-controls-span form-control-comments">
                            <label for="CAT_Custom_201134">Comments</label>
                            <br />
                            <textarea onkeydown="if(this.value.length>=4000)this.value=this.value.substring(0,3999);" class="cat_listbox light-style-input" rows="4" cols="23" id="Comments" name="Comments" style="width:100%;"></textarea>
                        </span>
                    </div>
                    <div class="clear-both height-12"></div>
                    <div>
                        <input type="file" class="upload"  id="f_UploadImage" accept=".pdf, .eps, .jpg, .jpeg, .tif"/>
                    </div>
                    <div class="clear-both height-12"></div>
                    <div class="submit-button-place-holder">
                        <input type="submit" id="catwebformbutton" value="Submit" class="shoppingcart-primary-button site-button"/>
                    </div>



                    <%--<table cellspacing="0" cellpadding="2" border="0" class="webform">
                        <tbody>
                            <tr>
                                <td><label for="OrderId">Order Id</label><br/><input type="text" readonly="true" maxlength="255" class="cat_textbox control-disabled" id="OrderId" name="OrderId"/></td>
                                <td><label for="EmailAddress">Email Address</label><br/><input type="text" readonly="true" maxlength="255" class="cat_textbox control-disabled" id="EmailAddress" name="EmailAddress"/></td>
                            </tr>
                            <tr>
                                <td><label for="FirstName">First Name</label><br/><input type="text" readonly="true" maxlength="255" class="cat_textbox control-disabled" id="FirstName" name="FirstName"/></td>
                                <td><label for="LastName">Last Name</label><br/><input type="text" readonly="true" maxlength="255" class="cat_textbox control-disabled" id="LastName" name="LastName"/></td>
                            </tr>
                            <tr>
                                <td colspan="2"><label for="CAT_Custom_201134">Comments</label><br/><textarea onkeydown="if(this.value.length>=4000)this.value=this.value.substring(0,3999);" class="cat_listbox" rows="4" cols="23" id="Comments" name="Comments" style="width:463px;"></textarea></td>
                            </tr>
                           
                            <tr>
                                <td colspan="2"><input type="file" class="upload"  id="f_UploadImage" accept=".pdf, .eps, .jpg, .jpeg, .tif"/></td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="padding-top:50px;"><input type="submit" id="catwebformbutton" value="Submit" class="shoppingcart-primary-button site-button"/></div>
                                </td>
                            </tr>
                        </tbody>
                    </table>--%>
                </div>
            </div>
        </form>

        <%--<img id="myUploadedImg" alt="Photo" style="width:180px;" />--%>
        <script type="text/javascript">
            var so = '<%= GetSalesOrderCode() %>';

            $('#OrderId').val(so);
            $('#FirstName').val('<%= ThisCustomer.IsRegistered? ThisCustomer.FirstName: GetFirstName() %>');
            $('#LastName').val('<%= ThisCustomer.IsRegistered? ThisCustomer.LastName: GetLasttName() %>');
            $('#EmailAddress').val('<%=  ThisCustomer.IsRegistered? ThisCustomer.EMail: GetEmail() %>');
            var imageFile = null;

            if (so === undefined || so === null || so.length == 0) {
                $('#f_UploadImage').hide();
                $('#myUploadedImg').hide();
            }

            var uploadParam = { FileName: "", ImageSrc: "", SO: so };
            var supportedExtensions = ["pdf", "eps", "jpg", "jpeg", "tif"];
            function sendFile(file) {

                var formData = new FormData();
                var file = $('#f_UploadImage')[0].files[0];

                var filename = file.name;
                var fileExtension = filename.substr((filename.lastIndexOf('.') + 1));
                var src = "images/upload/" + filename;

                if ($.inArray(fileExtension, supportedExtensions) < 0) {
                    $("#catwebformbutton").css('display', 'block');
                    ShowMessageBoxPopup("File type not supported. Please choose .pdf, .jpg, .eps & .tif");
                    return;
                }

                uploadParam.FileName = filename;
                uploadParam.ImageSrc = src;
                uploadParam.Email = $('#EmailAddress').val();

                formData.append('file', file);
                $.ajax({
                    type: 'post',
                    url: 'logoUploader.ashx?fName=' + uploadParam.FileName +
                                        '&src=' + uploadParam.ImageSrc +
                                        '&email=' + uploadParam.Email +
                                        '&so=' + uploadParam.SO,
                    data: formData,
                    success: function (status) {
                        if (status != 'error') {
                            var my_path = "images/upload/" + status;
                            //$("#myUploadedImg").attr("src", my_path);
                            window.location.href = 'UploadConfirmation.aspx';
                        }
                    },
                    processData: false,
                    contentType: false,
                    error: function () {
                        alert("Whoops something went wrong!");
                    }
                });
            }

            var _URL = window.URL || window.webkitURL;
            $("#f_UploadImage").on('change', function () {

                var file, img;
                if ((file = this.files[0])) {
                    //img = new Image();
                    //img.onload = function () {
                    //    sendFile(file);
                    //};
                    //img.onerror = function () {
                    //    alert("Not a valid file:" + file.type);
                    //};
                    //img.src = _URL.createObjectURL(file);
                    imageFile = file;
                }
            })


            $("#formUpload").validate({
                rules: {
                    // simple rule, converted to {required:true}
                    OrderId: "required",
                    FirstName: "required",
                    LastName: "required",
                    EmailAddress: "required",
                    f_UploadImage: "required"

                },

                submitHandler: function (form) {
                    if (imageFile != null || imageFile != undefined) {
                        sendFile(imageFile);
                    }
                    else {
                        $("#catwebformbutton").css('display', 'block');
                        ShowMessageBoxPopup("Please select a logo to upload first.");
                        return false;
                    }
                }
            });


        </script>
    </asp:Panel>

</body>
</html>
