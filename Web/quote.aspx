﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="quote.aspx.cs" Inherits="quote" %>
<%@ Import Namespace="InterpriseSuiteEcommerceCommon" %>
<%@ Import Namespace="InterpriseSuiteEcommerceCommon.Extensions" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="jscripts/jquery/jquery.validate.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var itemCode = '<%= GetItemCode() %>';
            $('#itemCode').val('<%= GetItemName() %>');
            $('#itemName').val('<%= GetItemDesc() %>');
            $('#ItemQuantity').val('<%= GetQuantity() %>');
            $('#ItemAccessories').val('<%= GetAccessories() %>');
            $('#itemColour').val('<%= GetProductColor() %>');
            $("#captcha-refresh-button").click(function () {
                captchaCounter++;
                $("#captcha").attr("src", "Captcha.ashx?id=" + captchaCounter);
            });

            $.ajax({
                type: "POST",
                url: 'ActionService.asmx/GetCustomStates',
                //data: JSON.stringify({ forCountry: '<%=InterpriseHelper.ConfigInstance.CompanyInfo.CompanyCountry%>' }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (result) {
                    var detail = result.d;
                    $.each(detail, function (e) {
                        $('#WorkState')
                            .append($("<option></option>")
                            .attr("value", detail[e].code)
                            .text(detail[e].description));
                    });
                },
                error: function (result, textStatus, exception) { }
            });

            $("#formQuote").validate({
                rules: {
                    // simple rule, converted to {required:true}
                    FirstName: "required",
                    LastName: "required",
                    EmailAddress: "required",
                    WorkPhone: "required",
                    Company: "required",
                    WorkState: "required",
                    WorkZip: "required",
                    itemCode: "required",
                    ItemQuantity: "required",
                    textCaptcha: "required"

                },

                submitHandler: function (form) {

                    var isValid =
                        $.ajax({
                            url: "ActionService.asmx/ValidateCaptcha",
                            data: { "captcha": $('#textCaptcha').val() },
                            type: "POST",
                            async: false,
                            success: function (data) {
                                return data.d;
                            },
                            error: function(e) {
                                return false;
                            }
                         });

                        if (isValid) {
                            var quoteInfo = new Object();
                            quoteInfo.FirstName = $('#FirstName').val();
                            quoteInfo.LastName = $('#LastName').val();
                            quoteInfo.Email = $('#EmailAddress').val();
                            quoteInfo.Phone = $('#WorkPhone').val();
                            quoteInfo.Mobile = $('#CellPhone').val();
                            quoteInfo.Company = $('#Company').val();
                            quoteInfo.Address = $('#WorkAddress').val();
                            quoteInfo.City = $('#WorkCity').val();
                            quoteInfo.State = $('#WorkState option:selected').val();
                            quoteInfo.PostCode = $('#WorkZip').val();
                            quoteInfo.ItemCode = itemCode;//$('#itemCode').val();
                            quoteInfo.Quantity = $('#ItemQuantity').val();
                            quoteInfo.Color = $('#itemColour').val();
                            quoteInfo.Comments = $('#ItemComments').val();
                            quoteInfo.Accessories = $.parseJSON(JSON.stringify(<%= GetAccessoriesJSON() %>));

                            AjaxCallWithSecuritySimplified("CreateCustomQuote", { "quoteInfo": quoteInfo },
                            function (req) {
                                var resposne = req.d;
                                window.location.href = resposne;
                            },null, this.serviceToken);
                        }
                        else {
                            alert("invalid verification code!");
                        }
                   
                    
                       }
                   });

        });
    </script>
</head>
<body>
    <form id="formQuote" runat="server">
            <div class="custom-quote-form">
                <h1 style="color: #f28b00;">Custom Quote Form</h1>
                <p>
                Please supply your details and we will respond to your quote request within 1 business day.
                </p>
              
                    <p>
                    Fields marked with an asterisk (<span class="req">*</span>) are required.
                    </p>
                    <table cellspacing="0" cellpadding="2" border="0" class="webform" width="50%">
                        <tbody>
                            <tr>
                                <td><label for="FirstName">First Name <span class="req">*</span></label></td>
                                <td>
                                <input type="text" maxlength="255" class="cat_textbox" id="FirstName" name="FirstName"/>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="LastName">Last Name <span class="req">*</span></label></td>
                                <td>
                                <input type="text" maxlength="255" class="cat_textbox" id="LastName" name="LastName"/>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="EmailAddress">Email Address <span class="req">*</span></label></td>
                                <td>
                                <input type="text" maxlength="255" class="cat_textbox" id="EmailAddress" name="EmailAddress"/>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="WorkPhone">Phone Number <span class="req">*</span></label></td>
                                <td>
                                <input type="text" maxlength="255" class="cat_textbox" id="WorkPhone" name="WorkPhone"/>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="CellPhone">Mobile Number <span class="req"></span></label></td>
                                <td>
                                <input type="text" maxlength="255" class="cat_textbox" id="CellPhone" name="CellPhone"/>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="Company">Company <span class="req">*</span></label></td>
                                <td>
                                <input type="text" maxlength="255" class="cat_textbox" id="Company" name="Company"/>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="WorkAddress">Address <span class="req"></span></label></td>
                                <td>
                                <input type="text" maxlength="500" class="cat_textbox" id="WorkAddress" name="WorkAddress"/>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="WorkCity">City / Suburb <span class="req"></span></label></td>
                                <td>
                                <input type="text" maxlength="255" class="cat_textbox" id="WorkCity" name="WorkCity"/>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="WorkState">State <span class="req">*</span></label></td>
                                <td>
                                <div class="select-cont">
                                <select class="cat_dropdown" name="WorkState" id="WorkState" style="height: 30px;">
                                <option value=" ">Please Select</option>
                                </select>
                                </div>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="WorkZip">Postcode <span class="req">*</span></label></td>
                                <td>
                                <input type="text" maxlength="255" class="cat_textbox" id="WorkZip" name="WorkZip"/>
                                </td>
                            </tr>
                           
                            <tr>
                                <td><label for="CAT_Custom_201131">Product Code <span class="req">*</span></label></td>
                                <td>
                                <input type="text" class="cat_textbox" id="itemCode" name="itemCode" maxlength="4000" readonly="true"/>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="CAT_Custom_201132">Product Name <span class="req">*</span></label></td>
                                <td>
                                <input type="text" class="cat_textbox" id="itemName" name="itemName"  maxlength="4000" readonly="true"/>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="CAT_Custom_201133">Quantity <span class="req">*</span></label></td>
                                <td>
                                <input type="text" class="cat_textbox" id="ItemQuantity" name="ItemQuantity"  maxlength="4000"/>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="CAT_Custom_208876">Colour</label></td>
                                <td>
                                <input type="text" class="cat_textbox" id="itemColour" name="itemColour" maxlength="4000" readonly="true"/>
                                </td>
                            </tr>
                            <tr>
                                <td><label for="CAT_Custom_208877">Accessories</label></td>
                                <td>
                                <input  type="text" class="cat_textbox" id="ItemAccessories" name="ItemAccessories" maxlength="4000" readonly="true"/>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top"><label for="CAT_Custom_201134">Comments</label></td>
                                <td><textarea onkeydown="if(this.value.length>=4000)this.value=this.value.substring(0,3999);" class="cat_listbox" rows="4" cols="23" id="ItemComments" name="ItemComments"></textarea></td>
                            </tr>
                           
                            <tr>
                                 <td> </td>
                                <td> 
                                        <label>Enter Word Verification in box below <span class="req">*</span></label>

                                </td>
                                
                            </tr>

                             <tr>
                                <td> </td>
                                <td>
                                <div id="account-captcha-wrapper" class="float-left">
                                        <div id="captcha-image">
                                            <img alt="captcha" src="Captcha.ashx?id=1" id="captcha"/>
                                         </div>
                                         <div id="captcha-refresh">
                                            <a href="javascript:void(1);" id="captcha-refresh-button" alt="Refresh Captcha" title="Click to change the security code"></a>
                                         </div>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td> </td>
                                <td>
                                <!-- Captcha Section Starts Here -->
                                   <input id="textCaptcha" name="textCaptcha" class="cat_textbox" type="text" /> 
                                     
                               </td>
                            </tr>
                              <!-- Captcha Section Ends Here -->
                            <tr>
                                <td> </td>
                                <td>
                                <input type="submit" id="catwebformbutton" value="Submit" class="cat_button"/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                
            </div>
    </form>

</body>
</html>
