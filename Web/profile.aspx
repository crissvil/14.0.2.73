﻿<%@ Page Language="C#" CodeFile="profile.aspx.cs" Inherits="InterpriseSuiteEcommerce.profile" %>

<%@ Register TagPrefix="uc" TagName="ProfileControl" Src="~/UserControls/ProfileControl.ascx" %>
<%@ Import Namespace="InterpriseSuiteEcommerceCommon" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script type="text/javascript" src="jscripts/jquery/customer.js"></script>
</head>
<body>
    <asp:Label ID="ErrorMsgLabel" runat="server" Style="color: #FF0000;"></asp:Label>
    <form id="frmProfile" runat="server">
        <asp:Panel ID="pnlPageContentWrapper" runat="server">
            <div class="sections-place-holder no-padding">
                <div class="profile-section-header">
                    <span>
                        <asp:Literal ID="Literal2" runat="server">(!account.aspx.62!)</asp:Literal>
                    </span>
                    <span style="position:absolute;right:16px;"><asp:Literal ID="ltContactList" runat="server"/></span>
                </div>
                <div class="section-content-wrapper">
                    <div>
                        <div class="clear-both"></div>
                        <div id="profile-info-wrapper" style="padding: 12px;">
                            <div id="profile-account-info-place-holder">
                                <img src="skins/skin_(!SKINID!)/images/signin-person.jpg" alt="" />&nbsp;&nbsp;<span class="signinheaderlabel">Profile information</span>
                                <span style="position:absolute;right:16px;display:none;"><a href="createcontact.aspx" class="profile-secondary-button site-button">REGISTER NEW CONTACT</a></span>
                                <div class="height-12"></div>
                                <div style="text-align: right; color: #ff0000; font-style: italic; padding-bottom: 20px; width: 442px;">*Required field</div>
                                <uc:ProfileControl ID="ProfileControl" runat="server" />
                                <div class="clear-both height-12 profile-section-clears"></div>

                                <div class="clear-both height-5"></div>

                                <div id="account-section-wrapper">
                                    <span class="form-section custom-font-style">
                                        <asp:Literal ID="litAdditionalInfo" runat="server">(!account.aspx.64!)</asp:Literal>
                                    </span>
                                    <div class="clear-both height-12"></div>

                                    <div class="form-controls-place-holder">
                                        <span class="form-controls-span">
                                            <asp:CheckBox ID="chkIsOkToEmail" runat="server" />
                                            <span class="checkbox-captions custom-font-style  i-text-16">
                                                <asp:Literal ID="Literal3" runat="server">(!account.aspx.65!)</asp:Literal>
                                            </span>
                                        </span>
                                    </div>

                                    <div class="clear-both height-5  i-hide"></div>

                                    <div class="form-controls-place-holder i-hide">
                                        <span class="form-controls-span label-outside" id="age-13-place-holder">
                                            <asp:CheckBox ID="chkIsOver13Checked" runat="server" />
                                            <span class="checkbox-captions custom-font-style">
                                                <asp:Literal ID="litOver13" runat="server">(!account.aspx.66!)</asp:Literal>
                                            </span>
                                        </span>
                                    </div>
                                </div>

                                <div class="clear-both height-5"></div>

                                <!-- Captcha Section Starts Here -->
                                <div class="form-controls-place-holder captcha-section">
                                    <span class="form-controls-span custom-font-style" id="captcha-label" style="padding-right: 17px !important;">
                                        <asp:Literal ID="LtrEnterSecurityCodeBelow_Caption" runat="server">(!customersupport.aspx.12!)</asp:Literal>:
                                    </span>

                                    <span class="form-controls-span">
                                        <label id="lblCaptcha" class="form-field-label">
                                            <asp:Literal ID="litCaptcha" runat="server">(!customersupport.aspx.13!)</asp:Literal>
                                        </label>
                                        <input id="txtCaptcha" class="light-style-input" type="text" />
                                    </span>
                                </div>

                                <div class="clear-both height-5  captcha-section"></div>

                                <div class="form-controls-place-holder  captcha-section">

                                    <div id="account-captcha-wrapper" class="float-right">
                                        <div id="captcha-image">
                                            <img alt="captcha" src="Captcha.ashx?id=1" id="captcha" />
                                        </div>
                                        <div id="captcha-refresh">
                                            <a href="javascript:void(1);" id="captcha-refresh-button" alt="Refresh Captcha" title="Click to change the security code"></a>
                                        </div>
                                    </div>

                                </div>

                                <div class="clear-both height-5  captcha-section"></div>
                                <!-- Captcha Section Ends Here -->
                            </div>
                        </div>
                    </div>

                    <div class="clear-both"></div>

                    <div id="profile-info-button-place-holder">
                        <div id="save-profile-button">
                            <div id="save-profile-loader"></div>
                            <div id="save-profile-button-place-holder">
                                <input type="button" class="profile-secondary-button site-button" id="sign-out" value="Log Out" />

                                <input type="button" class="profile-primary-button site-button" id="update-profile"
                                    data-contenttype="string resource"
                                    data-contentkey="account.aspx.6"
                                    data-contentvalue="Update Account"
                                    value="Update Account" />
                            </div>
                        </div>
                    </div>
                    <div class="clear-both"></div>
                </div>
            </div>

            <%-- div section for address book starts here --%>
            <div class="clear-both height-12"></div>
            <div class="clear-both height-5"></div>
            <div class="sections-place-holder no-padding">
                <div class="profile-section-header">
                    <span>
                        <asp:Literal ID="Literal7" runat="server">(!account.aspx.67!)</asp:Literal></span>
                </div>

                <div class="section-content-wrapper">

                    <div id="Div2">
                        <div id="Div3" style="padding: 10px;">

                            <div class="section-block">
                                <span class="section-title">
                                    <asp:Literal ID="accountaspx30" runat="server" Text="(!account.aspx.8!)"></asp:Literal></span>
                                <div class="height-12"></div>
                                <span class="section-values">
                                    <asp:Literal ID="litBillingAddress" runat="server"></asp:Literal></span>
                                <div class="height-20"></div>
                                <div>
                                    <asp:HyperLink ID="lnkChangeBilling" runat="server" CssClass="profile-secondary-button site-button"></asp:HyperLink></div>
                            </div>
                            <div class="section-block">
                                <asp:Panel ID="pnlShipping" runat="server">
                                    <span class="section-title">
                                        <asp:Literal ID="accountaspx32" runat="server" Text="(!account.aspx.10!)"></asp:Literal></span>
                                    <div class="height-12"></div>
                                    <span class="section-values">
                                        <asp:Literal ID="litShippingAddress" runat="server"></asp:Literal></span>
                                    <div class="height-20"></div>
                                    <div>
                                        <asp:HyperLink ID="lnkChangeShipping" runat="Server" CssClass="profile-secondary-button site-button"></asp:HyperLink></div>
                                </asp:Panel>
                            </div>
                        </div>
                    </div>
                    <b>
                        <asp:HyperLink ID="lnkAddShippingAddress" runat="server"></asp:HyperLink></b>
                    <div class="clear-both height-12"></div>
                </div>
            </div>
        </asp:Panel>
    </form>
</body>
</html>
