﻿<%@ WebHandler Language="C#" Class="logoUploader" %>
using System;
using System.IO;
using System.Web;
using System.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.DataAccess;
public class logoUploader : IHttpHandler
{

    private class UploadParam
    {

        public string FileName { get; set; }
        public string ImageSrc { get; set; }
        public string SO { get; set; }
        public string Email { get; set; }
    }

    private HttpContext CurrentContext
    {
        get;
        set;
    }

    private UploadParam GetUploadParam()
    {
        UploadParam param = null;
        var request = CurrentContext.Request;

        if (request.QueryString.Count > 0)
        {
            param = new UploadParam()
            {
                FileName = request.QueryString["fName"],
                ImageSrc = request.QueryString["src"],
                SO = request.QueryString["so"],
                Email = request.QueryString["email"]
            };
        }

        return param;
    }

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        CurrentContext = context;
        try
        {
            var param = GetUploadParam();
            string filename = param.FileName;
            string so = param.SO;
            string email = param.Email;

            if (so.IsNullOrEmptyTrimmed()) { context.Response.Write("error"); return; } 
            
            string dirFullPath = HttpContext.Current.Server.MapPath("~/images/upload/");
            string[] files;
            int numFiles;
            files = System.IO.Directory.GetFiles(dirFullPath);
            numFiles = files.Length;
            numFiles = numFiles + 1;
            string str_image = "";

            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                string fileName = Path.GetFileNameWithoutExtension(file.FileName);
                string fileExtension = file.ContentType;

                if (!string.IsNullOrEmpty(fileName))
                {
                    fileExtension = Path.GetExtension(fileName);
                    str_image = so +"-"+ filename + fileExtension;
                    string pathToSave = HttpContext.Current.Server.MapPath("~/images/upload/") + str_image;
                    file.SaveAs(pathToSave);
                    
                    // Save to DB
                    SaveLogoToDB(so, file, filename, email);
                }
            }
            //  database record update logic here  ()

            context.Response.Write(str_image);
        }
        catch (Exception ac)
        {

        }
    }
    
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private void SaveLogoToDB(string orderid,HttpPostedFile file, string fileName, string email) {
        using (System.Data.SqlClient.SqlConnection con = new System.Data.SqlClient.SqlConnection(DB.GetDBConn()))
        {
            using (System.Data.SqlClient.SqlCommand updateOrderLogo = new System.Data.SqlClient.SqlCommand(
                "Update CustomerSalesOrder SET ExpressPrintLogoFileName_DEV004817 = @filename, ExpressPrintLogo_DEV004817 = @logo , ExpressLogoUploadedDate_DEV004817 = GETDATE() WHERE SalesOrderCode = @OrderId ", con))
            {
                updateOrderLogo.CommandType = System.Data.CommandType.Text;

                // OrderId
                SqlParameter paramOrderId = new SqlParameter("@OrderId", System.Data.SqlDbType.NVarChar);
                paramOrderId.Value = orderid;
                updateOrderLogo.Parameters.Add(paramOrderId);

                // email
                //SqlParameter paramEmail = new SqlParameter("@email", System.Data.SqlDbType.NVarChar);
                //paramEmail.Value = email;
                //updateOrderLogo.Parameters.Add(paramEmail);
                
                // filename
                SqlParameter paramFileName = new SqlParameter("@filename", System.Data.SqlDbType.NVarChar);
                paramFileName.Value = fileName;
                updateOrderLogo.Parameters.Add(paramFileName);

                byte[] data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);
                
                // ExpressPrintLogo_DEV004817
                SqlParameter paramImage = new SqlParameter("@logo", System.Data.SqlDbType.Image);
                paramImage.Value = data;
                updateOrderLogo.Parameters.Add(paramImage);

                con.Open();
                updateOrderLogo.ExecuteNonQuery();
                
            }
        }
    }

    
    
}
//*