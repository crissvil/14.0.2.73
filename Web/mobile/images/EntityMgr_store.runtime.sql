************************************  SQL Statement and parameters for query EntityMgr  ************************************

declare @EntityName VarChar
declare @PublishedOnly TinyInt
declare @locale VarChar
declare @WebSiteCode VarChar
declare @CurrentDate DateTime

set @EntityName = 'Manufacturer'
set @PublishedOnly = 1
set @locale = 'en-US'
set @WebSiteCode = 'WEB-000001'
set @CurrentDate = '9/16/2015 12:00:00 AM'


exec eCommerceEntityMgr @EntityName, @PublishedOnly, @locale, @WebSiteCode, @CurrentDate

************************************  SQL Statement and parameters for query ManufacturerDescriptions  ************************************




SELECT	sm.Counter AS ID, 
							sm.ManufacturerCode AS Code, 
							sl.ShortString AS Locale, 
							smd.Description AS MLField
					FROM SystemManufacturer sm with (NOLOCK)
					INNER JOIN SystemManufacturerDescription smd with (NOLOCK) ON sm.ManufacturerCode = smd.ManufacturerCode
					INNER JOIN SystemLanguage sl with (NOLOCK) ON smd.LanguageCode = sl.LanguageCode
					INNER JOIN SystemSellingLanguage ssl with (NOLOCK) ON ssl.Languagecode = sl.LanguageCode
					WHERE ssl.IsIncluded = 1


