﻿using InterpriseSuiteEcommerce;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

public partial class quote : SkinBase
{
    #region DomainServices

    IAuthenticationService _authenticationServie = null;
    INavigationService _navigationService = null;
    IOrderService _orderService = null;
    ICryptographyService _cryptoService = null;
    IProductService _productService = null;
    ECommerceProductInfoView _productInfoView = null;

    #endregion

    #region Initialize

    protected override void OnInit(EventArgs e)
    {
        InitializeDomainServices();
        PageNoCache();
        PerformPageAccessLogic();
        BindControls();
        InitializeContent();

        base.OnInit(e);
    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    #endregion

    #region Private Methods

    private void InitializeDomainServices()
    {
        _authenticationServie = ServiceFactory.GetInstance<IAuthenticationService>();
        _navigationService = ServiceFactory.GetInstance<INavigationService>();
        _orderService = ServiceFactory.GetInstance<IOrderService>();
        _cryptoService = ServiceFactory.GetInstance<ICryptographyService>();
        _productService = ServiceFactory.GetInstance<IProductService>();
    }
    private void PerformPageAccessLogic()
    {
        RequireSecurePage();

        // check if logged in customer
        string returnUrl = CommonLogic.GetThisPageName(false);
        string queryStrings = CommonLogic.ServerVariables("QUERY_STRING");
        if (!queryStrings.IsNullOrEmptyTrimmed()) { returnUrl = returnUrl + "?" + queryStrings; }
        //RequiresLogin(returnUrl);
    }
    private void InitializeContent()
    {
        // set string resources
       // SectionTitle = "Quote";
        GetProductInfo();
       
    }
    private void BindControls()
    {
        //btnCreateQuote.Click += (sender, e) => _navigationService.NavigateToCreateRMA();
    }

    #endregion

    #region Public Methods

    public void GetProductInfo() {
        string productID = CommonLogic.QueryStringCanBeDangerousContent("productId", true);
        string itemCode = String.Empty;
        itemCode = productID.IsNullOrEmptyTrimmed() ? itemCode : AppLogic.GetItemCodeByCounter(Convert.ToInt32(productID));
        _productInfoView = _productService.GetProductInfoViewForShowProduct(itemCode);

        string color = CommonLogic.QueryStringCanBeDangerousContent("color", true);
        bool isMatrixItem = _productInfoView == null ? false : _productInfoView.ItemType.EqualsIgnoreCase("Matrix Item");
        if (isMatrixItem && !color.IsNullOrEmptyTrimmed())
        {
            var inventoryMatrixItemModel = ServiceFactory.GetInstance<IProductService>().GetMatrixItemInfo(_productInfoView.ItemCode);
            if (inventoryMatrixItemModel != null)
            {
                var matrixAttributes = ServiceFactory.GetInstance<IProductService>().GetMatrixAttributes(inventoryMatrixItemModel.ItemCode);
                var matrixSizeAttribute = matrixAttributes.FirstOrDefault(x => x.AttributeCode.ToLowerInvariant().Contains("size"));
                
                var matrixItems = MatrixItemData.GetMatrixItems(inventoryMatrixItemModel.Counter, inventoryMatrixItemModel.ItemCode, true);
                if (matrixItems != null && matrixSizeAttribute !=null)
                {
                    foreach (var item in matrixItems) {
                        var colorAttribute = item.Attributes.FirstOrDefault(x => x.Code.ToUpperInvariant().Contains("COLOR".ToUpperInvariant()));
                        var sizeAttribte = item.Attributes.FirstOrDefault(x => x.Code.ToUpperInvariant().Contains("SIZE".ToUpperInvariant()));
                        if (colorAttribute != null && sizeAttribte != null) { 
                            if (colorAttribute.Value.EqualsIgnoreCase(color) && sizeAttribte.Value.EqualsIgnoreCase(matrixSizeAttribute.AttributeValueCode)){
                                _productInfoView = _productService.GetProductInfoViewForShowProduct(item.ItemCode);
                            }
                        }
                    }
                }
            }
        }

    }

    public string GetItemCode()
    {
        if (_productInfoView == null) { GetProductInfo(); }
        return _productInfoView != null ? _productInfoView.ItemCode : String.Empty;
    }

    public string GetQuantity()
    {
        string qty = CommonLogic.QueryStringCanBeDangerousContent("quantity", true);
        return qty;
    }

    public string GetItemName(){
        return _productInfoView != null ? _productInfoView.ItemName : String.Empty;
    }
    public string GetItemDesc()
    {
        return _productInfoView != null ? _productInfoView.ItemDescription : String.Empty;
    }

    public string GetAccessories() {
        string accessories = CommonLogic.QueryStringCanBeDangerousContent("accessories", true);
        if (accessories.IsNullOrEmptyTrimmed()) return String.Empty;
        
        var counters = accessories.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
        if (counters == null) return String.Empty; 

        List<string> aList = new List<string>();
        accessories.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ForEach(
            x => aList.Add(AppLogic.GetItemCodeByCounter(Convert.ToInt32(x))));
        if (aList.Count==0) return String.Empty; 
        
        string items = String.Join(",", aList);
        return items;
    }

    public string GetAccessoriesJSON()
    {
        string accessories = CommonLogic.QueryStringCanBeDangerousContent("accessories", true);
        if (accessories.IsNullOrEmptyTrimmed()) return String.Empty;

        var counters = accessories.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
        if (counters == null) return String.Empty;

        List<string> aList = new List<string>();
        accessories.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ForEach(
            x => aList.Add(AppLogic.GetItemCodeByCounter(Convert.ToInt32(x))));
        if (aList.Count == 0) return String.Empty;

        
        return _cryptoService.SerializeToJson<List<string>>(aList);
    }

    public string GetProductInfoJSON()
    {
        return _productInfoView != null ? _cryptoService.SerializeToJson<ECommerceProductInfoView>(_productInfoView) : String.Empty;
    }

    public string GetProductColor() {
        string color = CommonLogic.QueryStringCanBeDangerousContent("color", true);
        bool isMatrixItem = _productInfoView == null ? false : _productInfoView.ItemType.EqualsIgnoreCase("Matrix Item");
        if (isMatrixItem && color.IsNullOrEmptyTrimmed())
        {
            var inventoryMatrixItemModel = ServiceFactory.GetInstance<IProductService>().GetMatrixItemInfo(_productInfoView.ItemCode);
            if (inventoryMatrixItemModel != null) {
                var matrixItems = MatrixItemData.GetMatrixItems(inventoryMatrixItemModel.Counter, inventoryMatrixItemModel.ItemCode, true);
                if (matrixItems != null) {
                    var item = matrixItems.FirstOrDefault(x => x.Counter == _productInfoView.Counter);
                    if (item != null) {
                       var colorAttribute = item.Attributes.FirstOrDefault(x => x.Code.ToUpperInvariant().Contains("COLOR".ToUpperInvariant()));
                       if (colorAttribute !=null) {
                           color = colorAttribute.Value;
                       }
                    }
                }
            }
        }
        return color;
    }

    #endregion
}