﻿using InterpriseSuiteEcommerceCommon;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Xml.Linq;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerce
{
    public partial class reports : System.Web.UI.Page
    {
        Customer ThisCustomer = Customer.Current;
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.CacheControl = "private";
            Response.Expires = 0;
            Response.AddHeader("pragma", "no-cache");
            ServiceFactory.GetInstance<IRequestCachingService>()
                          .PageNoCache();

            Customer thisCustomer = Customer.Current;
            if (thisCustomer.IsNotRegistered)
            {
                Response.Redirect("login.aspx?returnurl=reports.aspx");
            }
            InitializePageContent();
        }

        private void InitializePageContent()
        {
            string dateFrom = Request.Form["txtDateFrom"], dateTo = Request.Form["txtDateTo"], asOf = Request.Form["txtAsOf"], type = Request.Form["drpDate"];
            if (dateFrom == null || dateFrom == "")
            {
                dateFrom = String.Format("07/01/2017");
            }
            else
            {
                dateFrom = DateTime.ParseExact(dateFrom, "d", System.Globalization.CultureInfo.GetCultureInfo("en-au")).Date.ToString();
            }
            if (dateTo == null || dateTo == "")
            {
                dateTo = String.Format("{0:MM/dd/yyyy}", DateTime.Now.Date);
            }
            else
            {
                dateTo = DateTime.ParseExact(dateTo, "d", System.Globalization.CultureInfo.GetCultureInfo("en-au")).Date.ToString();
            }
            if (string.IsNullOrEmpty(type))
            {
                type = "1";
            }
            List<XmlPackageParam> runtimeParams = new List<XmlPackageParam>();
            //runtimeParams.Add(new XmlPackageParam("DateFrom", String.Format("{0:MM/dd/yyyy}", CommonLogic.IIF(type == "0", asOf, dateFrom))));
            //runtimeParams.Add(new XmlPackageParam("DateTo", String.Format("{0:MM/dd/yyyy}", dateTo)));
            runtimeParams.Add(new XmlPackageParam("DateFrom", CommonLogic.IIF(type == "0", asOf, dateFrom)));
            runtimeParams.Add(new XmlPackageParam("DateTo", dateTo));
            runtimeParams.Add(new XmlPackageParam("Type", type));
            runtimeParams.Add(new XmlPackageParam("WarehouseCode", ThisCustomer.WarehouseCode));
            runtimeParams.Add(new XmlPackageParam("SkinID", ThisCustomer.SkinID.ToString()));
            runtimeParams.Add(new XmlPackageParam("CostCenter", ThisCustomer.CostCenter));
            
            var package = new XmlPackage2("page.sohreport.xml.config", runtimeParams);
            ltPageContent.Text = package.TransformString();
        }
    }
}