<%@ Page Language="c#" Inherits="InterpriseSuiteEcommerce._default" CodeFile="default.aspx.cs" %>

<%@ Register TagPrefix="ise" TagName="XmlPackage" Src="~/XmlPackageControl.ascx" %>
<html>
<head>
    <style>
        .carousel {
            border-bottom: 1px solid #000;
            height: 239px;
            width: 708px;
            position: relative;
        }

        .carousel-wholesale-only {
            height: 200px;
        }

            .carousel-wholesale-only .arrow {
                margin-top: -25px;
            }

        .carousel .wrapper3 {
            width: 648px;
            overflow: auto;
            min-height: 10em;
            margin-left: 30px;
            position: absolute;
            top: 0;
            height: 300px;
        }

            .carousel .wrapper3 ul {
                width: 9999px;
                margin: 0 0 0 3px;
                padding: 0;
                position: absolute;
                top: 0;
                list-style: none;
            }

        .carousel ul li {
            display: block;
            float: left;
            padding: 0;
            height: 85px;
            width: 162px;
        }

            .carousel ul li a img {
                display: block;
                width: 140px;
                height: 140px;
            }

        .carousel .arrow {
            display: block;
            height: 36px;
            width: 37px;
            text-indent: -999px;
            position: absolute;
            top: 50px;
            cursor: pointer;
        }

        .carousel .back, .carousel .back:hover, .carousel .forward:hover {
            display: block !important;
            height: 100px;
            width: 30px;
        }

        .forward-disabled {
            background: url(images/right_disabled.png) no-repeat !important;
            cursor: default;
        }

        .back-disabled {
            background: url(images/left_disabled.png) no-repeat !important;
            cursor: default;
        }

        .carousel .forward {
            right: 0;
            top: 55px;
            background: url(images/right.png) no-repeat;
            height: 100px;
            width: 30px;
        }

        .carousel .back {
            left: 0;
            top: 55px;
            background: url(images/left.png) no-repeat;
        }

        .carousel .forward:hover {
            background: url(images/right_hover.png) no-repeat;
        }

        .carousel .back:hover {
            background: url(images/left_hover.png) no-repeat;
        }
    </style>
    <script type="text/javascript">
        /************************* RELATED PRODUCTS************************************/
        $('.related-slider').owlCarousel({
            smartSpeed: 450,
            dots: false,
            nav: true,
            loop: true,
            margin: 30,
            navClass: ['owl-prev fa fa-angle-left', 'owl-next fa fa-angle-right'],
            responsive: {
                0: { items: 1 },
                768: { items: 2 },
                992: { items: 3 },
                1199: { items: 4 }
            }
        });

        $(window).load(function () {
            listBlocksAnimate('.box.featured', '.box.featured .product-layout', 4, -300, true);
            listBlocksAnimate('.box.latest', '.box.latest .product-layout', 4, -300, true);
            if (!isMobile) {
                if (jQuery(".fluid_container").length) {
                    var welcome = new TimelineMax();

                    welcome.from(".fluid_container h2", 0.5, { right: -300, autoAlpha: 0 })
                        .from(".fluid_container p", 0.5, { right: -300, autoAlpha: 0 })
                        .from(".fluid_container a", 0.5, { bottom: -200, autoAlpha: 0 });

                    var scene_welcome = new ScrollScene({
                        triggerElement: ".fluid_container",
                        offset: -100
                    }).setTween(welcome).addTo(controller).reverse(false);
                }

                if (jQuery(".banner-1").length) {
                    var welcome = new TimelineMax();

                    welcome.from(".banner-1:nth-child(1)", 0.5, { left: -300, autoAlpha: 0 })
                        .from(".banner-1:nth-child(2)", 0.5, { right: -300, autoAlpha: 0 })
                    var scene_welcome = new ScrollScene({
                        triggerElement: ".banners",
                        offset: -100
                    }).setTween(welcome).addTo(controller).reverse(false);
                }

                if (jQuery(".box_html.About").length) {
                    var welcome = new TimelineMax();

                    welcome.from(".box_html.About .col-sm-6:nth-child(1)", 0.5, { left: -300, autoAlpha: 0 })
                        .from(".box_html.About .col-sm-6:nth-child(2)", 0.5, { right: -300, autoAlpha: 0 })
                    var scene_welcome = new ScrollScene({
                        triggerElement: ".box_html.About",
                        offset: -100
                    }).setTween(welcome).addTo(controller).reverse(false);
                }
            }
        });
    </script>
</head>
<body>
    <ise:XmlPackage ID="Package1" PackageName="page.default.xml.config" runat="server" EnforceDisclaimer="true" EnforcePassword="true" EnforceSubscription="True" AllowSEPropogation="True" />
</body>
</html>
