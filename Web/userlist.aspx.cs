﻿using DevExpress.XtraReports.Web;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System;

public partial class userlist : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //RequiresLogin(CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables("QUERY_STRING"));
        ServiceFactory.GetInstance<IRequestCachingService>()
                      .PageNoCache();

        Customer thisCustomer = Customer.Current;
        if (thisCustomer.IsNotRegistered)
        {
            Response.Redirect("login.aspx?returnurl=userlist.aspx");
        }
        var reportcode = AppLogic.AppConfig("custom.hn.userlist.report");

        try
        {
            ViewerReport.Report = InterpriseHelper.LoadUserListReport(reportcode, thisCustomer.CustomerCode);
            ViewerReport.AutoSize = true;

        }
        catch (Exception ex)
        {
            ltReport.Text = "No data";
            ltReport.Visible = true;
        }
    }

    protected void ViewerReport_CacheReportDocument(object sender, CacheReportDocumentEventArgs e)
    {
        e.Key = Guid.NewGuid().ToString();
        Page.Session[e.Key] = e.SaveDocumentToMemoryStream();
    }
}