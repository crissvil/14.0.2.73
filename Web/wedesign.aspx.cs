﻿using System;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Data.SqlClient;
using InterpriseSuiteEcommerceCommon;
using System.Data;
using System.Text;
using System.IO;

namespace InterpriseSuiteEcommerce
{
    public partial class wedesign : SkinBase
    {
        string fname, lname, email = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
        
        }

        public void btnSubmit_Click(object sender, EventArgs e)
        {
            SaveDesign();
            AppLogic.SaveCustomizationCode(3);
            Response.Redirect("shoppingcart.aspx");
        }

        public void btnDesignYourOwn_Click(object sender, EventArgs e)
        {
            string dcode = "dcode".ToQueryString();
            string ccode = "ccode".ToQueryString();
            string recid = "recid".ToQueryString();
            string qty = "qty".ToQueryString();
            string designURL = string.Format("designit.aspx?dcode={0}&ccode={1}&recid={2}&qty={3}", dcode, ccode, recid, qty);
            Response.Redirect(designURL);
        }

        public void btnUploadYourDesign_Click(object sender, EventArgs e)
        {
            string dcode = "dcode".ToQueryString();
            string ccode = "ccode".ToQueryString();
            string recid = "recid".ToQueryString();
            string qty = "qty".ToQueryString();
            string designURL = string.Format("uploaddesign.aspx?dcode={0}&ccode={1}&recid={2}&qty={3}", dcode, ccode, recid, qty);
            Response.Redirect(designURL);
        }

        private void InitializeContent()
        {
            SectionTitle = "Let us design for you";
            if (ThisCustomer.IsRegistered)
            {
                fname = ThisCustomer.FirstName;
                lname = ThisCustomer.LastName;
                email = ThisCustomer.EMail;
            }
        }
        
        public string GetFirstName()
        {
            return fname;
        }
        public string GetLasttName()
        {
            return lname;
        }
        public string GetEmail()
        {
            return email;
        }

        private void SaveDesign()
        {
            var checkbox = new StringBuilder();
            foreach (string str in Request.Form)
            {
                if (str != "Comments" && str != "btnSubmit" && str != "__VIEWSTATE" && str != "__VIEWSTATEGENERATOR" && str != "__EVENTVALIDATION")
                {
                    checkbox.AppendFormat("{0}, ", str);
                }
            }
            if (checkbox.Length > 1)
            {
                checkbox.Remove(checkbox.Length - 2, 2);
            }
            string comments = Request.Form["Comments"];
            string recid = "recid".ToQueryString();
            string new_fileName = Guid.NewGuid().ToString();
            string fileName = Path.GetFileNameWithoutExtension(fileUploadImage.FileName);
            string fileExtension = Path.GetExtension(fileUploadImage.FileName);
            string str_image = new_fileName + fileExtension;
            string pathToSave = Server.MapPath("~/images/upload/") + str_image;
            //Save design to server folder
            fileUploadImage.SaveAs(pathToSave);
            //Save design to database
            byte[] data = new byte[fileUploadImage.PostedFile.ContentLength];
            fileUploadImage.PostedFile.InputStream.Read(data, 0, fileUploadImage.PostedFile.ContentLength);
            //DB.ExecuteSQL("UPDATE EcommerceShoppingCart SET DesignComments_C = {0}, DesignFilename_C = '{1}', DesignFile_C = '{2}', DesignCheckbox_C = {3} WHERE ShoppingCartRecGUID = {4}", DB.SQuote(comments), str_image, data, DB.SQuote(checkbox.ToString()), DB.SQuote(recid));
            using (SqlConnection con = DB.NewSqlConnection())
            {
                string query = "UPDATE EcommerceShoppingCart SET DesignComments_C = @DesignComments_C, DesignFilename_C = @DesignFilename_C, DesignFile_C = @DesignFile_C, DesignCheckbox_C = @DesignCheckbox_C WHERE ShoppingCartRecGUID = @ShoppingCartRecGUID";
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    cmd.Connection = con;
                    cmd.Parameters.AddWithValue("@DesignComments_C", comments);
                    cmd.Parameters.AddWithValue("@DesignFilename_C", str_image);
                    cmd.Parameters.AddWithValue("@DesignFile_C", data);
                    cmd.Parameters.AddWithValue("@DesignCheckbox_C", checkbox.ToString());
                    cmd.Parameters.AddWithValue("@ShoppingCartRecGUID", recid);
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            } 
        }
    }
}