﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="InterpriseSuiteEcommerce.companylogin" %>

<html>
<head>
</head>
<body>
    <form runat="Server" method="POST" id="frmLogin" name="frmLogin">
     <div>
         <div align="center" style="text-align: left; color: #000; padding-left: 40px;">
            <asp:Panel ID="FormPanel" runat="server" Width="100%">
                <asp:Panel ID="CheckoutPanel" runat="server">
                    <div id="CheckoutSequence" align="center">
                        <asp:ImageMap ID="CheckoutMap" runat="server" ImageUrl="(!skins/skin_(!SKINID!)/images/step_2.gif!)">
                            <asp:RectangleHotSpot Bottom="54" HotSpotMode="Navigate"
                                NavigateUrl="shoppingcart.aspx?resetlinkback=1&amp;checkout=true" Right="87" />
                        </asp:ImageMap>
                        </div>
                </asp:Panel>
                <asp:Panel ID="ErrorPanel" runat="server" Visible="False" HorizontalAlign="Left">
                            <asp:Label CssClass="errorLg" ID="ErrorMsgLabel" runat="server"></asp:Label>
                            <asp:LinkButton ID="lnkContactUs" runat="server" PostBackUrl="contactus.aspx" Visible="false" />
                </asp:Panel>
				
                <div class="clr height-12"></div>
                    <h1 class="companyheadertext">Sign In</h1>
                <div class="clr height-12"></div>
                <div class="col-lg-6 col-md-6 col-sm-6" style="padding-left:0">
                    <div style="font-weight:500; font-size: 16px;">If you have ordered before or have been provided with login details; please sign in below:</div>
                    <div style="font-size: 16px; padding: 20px 0 40px; font-weight: 700; color: #635c5d;">Enter your e-mail address & password:</div>
                    <div style="font-size: 15px;padding-bottom: 5px;">My email address:</div>
                    <asp:TextBox ID="EMail" runat="server" ValidationGroup="Group1" Columns="30" MaxLength="100" CausesValidation="True"
                                                        AutoCompleteType="Email" CssClass="signintextbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ValidationGroup="Group1"
                                                        ErrorMessage="(!signin.aspx.3!)" ControlToValidate="EMail"></asp:RequiredFieldValidator>
                    <div style="font-size: 15px;padding-bottom: 5px;">Password:</div>
                    <asp:TextBox ID="Password" runat="server" ValidationGroup="Group1" Columns="30" MaxLength="50"
                                                        CausesValidation="True" TextMode="Password" AutoCompleteType="Disabled" autocomplete="off" CssClass="signintextbox"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ValidationGroup="Group1"
                                                        ErrorMessage="(!signin.aspx.4!)" ControlToValidate="Password"></asp:RequiredFieldValidator>
                    <div style="padding-bottom: 10px;">
                        <asp:CheckBox ID="PersistLogin" runat="server"></asp:CheckBox>&nbsp;&nbsp;<span style="vertical-align:bottom;font-size: 15px;">Remember Password?</span>
                    </div>
                    <div style="text-align:right;">
                        <asp:Button ID="LoginButton" runat="server" CssClass="signin-button content" ValidationGroup="Group1" />
                    </div>
                    <div style="padding:20px 15px 0 5px; text-align:right;">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="SecurityCode"
                        ErrorMessage="(!signin.aspx.17!)" ValidationGroup="Group1" Enabled="False" Style="font-size: 15px;"></asp:RequiredFieldValidator>
                    </div>
                    <div style="padding:10px 0; text-align:right;">
                        <asp:Label ID="Label1" runat="server" Text="(!signin.aspx.18!)" Visible="False" style="font-size: 15px;"></asp:Label>
                        <asp:TextBox ID="SecurityCode" runat="server" Visible="False" ValidationGroup="Group1"
                                                        CausesValidation="True" Width="73px" EnableViewState="False" CssClass="signintextbox"></asp:TextBox>
                    </div>
                    <div style="text-align: right;">
                        <asp:Image ID="SecurityImage" runat="server" Visible="False"></asp:Image>
                    </div>
                    <div style="color: #3399ff; font-size: 17px; font-weight: 700; padding-top: 60px;">Forgotten your password?</div>
                    <div style="font-size: 16px;padding: 5px 0 30px; line-height: 19px;">To receive your password via e-mail,<br />please enter your email address below:</div>
                    <div style="font-size: 15px;padding-bottom: 5px;">Email address:</div>
                    <div>
                        <asp:TextBox ID="ForgotEMail" runat="server" ValidationGroup="Group2" CausesValidation="True" AutoCompleteType="Email" Columns="30" CssClass="signintextbox"></asp:TextBox>
                        <span class="errorLg"><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ForgotEMail" ErrorMessage="(!signin.aspx.3!)" ValidationGroup="Group2"></asp:RequiredFieldValidator></span>
                    </div>
                    <div style="text-align: right;">
                        <asp:Button ID="RequestPassword" runat="server" CssClass="signin-button signin-button-secondary content" ValidationGroup="Group2"></asp:Button>
                    </div>
                </div>
                
             <!--Log In Form Section Ends Here -->
			  <div class="clr height-12"></div>
             <div class="clr height-12"></div>
            </asp:Panel>
            </div>
            <asp:Panel ID="ExecutePanel" runat="server" Width="90%">
                <div align="center">
                    <img src="images/spacer.gif" alt="" width="100%" height="40" />
                    <b>
                        <asp:Literal ID="SignInExecuteLabel" runat="server"></asp:Literal></b></div>
            </asp:Panel>
            <asp:CheckBox ID="DoingCheckout" runat="server" Visible="False" />
            <asp:Label ID="ReturnURL" runat="server" Text="default.aspx" Visible="False" />
        </div>
    </form>
</body>
</html>
