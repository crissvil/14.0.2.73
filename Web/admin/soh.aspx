﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="soh.aspx.cs" Inherits="InterpriseSuiteEcommerce.soh" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SOH Report</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"/>
    <script src="https://use.fontawesome.com/55068458dc.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="js/table-format.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#txtDateFrom").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#txtDateTo").datepicker({ dateFormat: 'dd/mm/yy' });
        });

        $(function () {
            $('#fixed_hdr1').fxdHdrCol({
                fixedCols: 3,
                width: '100%',
                height: 550,
                colModal: [
                   { width: 110, align: 'left' },
                   { width: 110, align: 'left' },
                   { width: 400, align: 'left' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 200, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'left' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 150, align: 'right' },
                   { width: 200, align: 'left' },
                   { width: 200, align: 'left' },
                   { width: 200, align: 'left' }
                ]
            });
        });
    </script>

<style>
        body {
            color: #666;
            padding: 0;
            margin: 0;
            font-family: "Roboto", Lato, Helvetica, Arial, sans-serif;
            font-weight: 400;
            line-height: 20px;
            font-size: 14px;
        }

        a, a:hover {
            text-decoration: none;
        }

        #modalpopup, #messagebox, #confirmationbox {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 999; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        #modalpopup-content {
            background-color: #f2f2f2;
            border-radius: 10px;
            box-shadow: 0 1px 5px #000;
            padding: 30px 30px;
            text-align: center;
            vertical-align: middle;
            min-height: 125px;
            min-width: 200px;
            font-size: 15px;
            display: inline-table;
            z-index: 999; /* Sit on top */
        }

            #modalpopup-content i {
                font-size: 40px;
            }

        #modalpopup-text {
            padding-top: 5px;
        }

        .filter-date {
            background-color: #f0f0f0;
            border: 1px solid #a8a8a8 !important;
            box-sizing: border-box;
            float: left;
            padding: 4px 0 0 4px;
            height: 45px !important;
            width: 100%;
            margin-bottom: 20px !important;
            font-size: 15px;
        }

        .filter-date span {
            font-size: 15px;
        }

        .header-color {
            background: #7c7a78;
            color: #fff;
            width: 3260px;
            display: flex;
        }

        .fixed-header-color {
            background: #7c7a78;
            color: #fff;
            font-size: 15px;
            font-weight: 700;
        }

        .calendar {
            right: 0; 
            position: absolute; 
            pointer-events: none; 
            padding: 5px; 
            font-size: 24px;
        }

        .save-icon {
            right: 0; 
            padding: 5px; 
            font-size: 31px;
            padding: 2px 5px 0 10px;
            color: #66cc00;
        }

        .home {
            right: 0; 
            padding: 0 3px; 
            font-size: 24px;
            color: #fff;
        }

        .responsive-grid-content, .cart-item-divider{
            width: 3260px;
        }

        .responsive-col {
            padding: 5px;
            width: 150px;
        }

        .responsive-col-1 {
            width: 110px;
            padding: 5px;
        }

        .responsive-col-2 {
            width: 400px;
            padding: 5px;
        }

        .responsive-col-3 {
            width: 200px;
            padding: 5px;
        }

        .responsive-col-4 {
            width: 958px;
            padding: 5px;
        }

        .responsive-col-5 {
            width: 204px;
            padding: 5px;
            background: #7c7a78;
            font-size: 15px;
            font-weight: 700;
            color: #fff;
        }

        .responsive-col-6 {
            border-top: 1px solid #7c7a78;
            padding: 5px;
            width: 124px;
            font-size: 15px;
            font-weight: 700;
        }

        .responsive-col-7 {
            padding: 5px;
            width: 123px;
        }

        .responsive-col-subtotal {
            padding: 5px;
            width: 195px;
            float: left;
            font-weight: 700;
        }

        .report-go-button {
            background: #66cc00 none repeat scroll 0 0;
            font-size: 18px;
            width: 60px;
            margin-top: 0px;
            line-height: 35px;
            max-height: 35px;
        }

        .text-left-space {
            padding-left: 15px;
        }

        .text-align-right {
            text-align: right;
        }

        .site-button {
            border: medium none;
            border-radius: 3px;
            color: #fff;
            display: inline-block;
            font-size: 16px;
            font-weight: 500;
            height: 40px;
            line-height: 35px;
            padding: 0;
            text-align: center;
            text-transform: uppercase;
        }

        .site-button:hover {
            cursor: pointer;
        }

        .home-button {
            background: #0066cc none repeat scroll 0 0;
            color: #fff;
            height: 35px;
            line-height: 35px;
            margin: 0;
            width: 200px;
        }

        .report-date-option {
            width: 110px;
        }

        .report-date {
            width: 150px;
            height: 25px !important;
        }

        .textbox {
            border: 1px solid #a8a8a8;
            border-radius: 4px;
            font-size: 15px;
            height: 36px;
            padding: 4px;
        }

        button, input, label, select, textarea {
            color: #2f2f2f;
            font-size: 15px;
        }

        .uneditable-input, input[type="datetime"], 
        input[type="datetime-local"], input[type="date"], 
        input[type="month"], input[type="time"], input[type="week"], 
        input[type="number"], input[type="email"], input[type="url"], 
        input[type="search"], input[type="tel"], input[type="color"], 
        input[type="text"], input[type="password"], select[multiple], 
        select[size], select {
            border: 1px solid #a8a8a8;
            border-radius: 4px;
            box-shadow: none;
            font-size: 15px;
            height: 100%;
            padding: 4px 6px;
        }

        /*==========================================================*/
        /*START FREEZE COLUMN*/
        .ft_container table { border-width: 0px; margin: 0px; border-collapse: collapse; margin: 0; outline-style: none; font-size: 0.9em; background-color: #fff; }

        .ft_container table tr th {	font-weight: bold; }

        .ft_container table thead { -moz-user-select: none;-webkit-user-select: none;}
        .ft_container table tr th, 
        .ft_container table tr td { border-collapse: collapse; padding: 2px 4px; word-wrap: break-word; border: 1px solid #CCCCCC; border-top-width: 0px; border-left-width: 0px; border-right-width: 1px; border-bottom-width: 1px; overflow:hidden; word-wrap: break-word;}

        .ft_container table tr:first-child td,  
        .ft_container table tr:first-child th { border-top-width: 1px; border-color: #CCCCCC; }
        .ft_container table tr td:first-child,
        .ft_container table tr th:first-child { border-left-width: 1px; border-color: #CCCCCC; }

        .ft_container { overflow: hidden; padding: 0px; }

        .ft_rel_container { position: relative; overflow: hidden; border-width: 0px; width: 100%; height: 100%; }

        .ft_r, .ft_rc { background-image: none; }
        .ft_rc { position: absolute; z-index: 1005; }
        .ft_r, .ft_c { position: relative; }

        .ft_rwrapper, .ft_cwrapper { overflow: hidden; position: absolute; z-index: 999; border-width: 0px;  padding: 0px; margin: 0px; }

        .ft_scroller { overflow: auto; height: 100%; padding: 0px; margin: 0px;}

        .ft_container tbody tr.ui-widget-content, thead.ui-widget-header { background-image: none; }

        .ft_container table.sorttable thead tr th { cursor: pointer; }
        .ft_container table thead tr th.fx_sort_bg{ background-image: url(images/bg.gif); background-position: right center; background-repeat: no-repeat; }
        .ft_container table thead tr th.fx_sort_asc{ background-image: url(images/asc.gif); }
        .ft_container table thead tr th.fx_sort_desc{ background-image: url(images/desc.gif); }

        .ft_container table tr th {
            background: #7c7a78; 
            color: #fff;
            font-size: 15px;
            font-weight: 700;
        }

        .table-header-sticky {
          position: fixed;
          top: 0;
          width: 100%;
        }

        .table-header-sticky + .table-content {
          padding-top: 102px;
        }
        /*END FREEZE COLUMN*/
        /*==========================================================*/
    </style>
</head>
<body>
    <asp:Panel ID="pnlContent" runat="server">
        <div style="padding: 10px;">
            <form id="frmReports" runat="server">
                <div>
                    <div class="filter-date">
                        <div style="float: left;">
                            <a href="reports.aspx" class="site-button home-button"><span>Back to Reports</span></a>
                        </div>
                        <div style="float: left; border-left: 1px solid #000; margin: 0 10px; height: 89%;">
                        </div>
                        <div style="float: left;">
                            <select id="drpSave" name="drpSave" class="textbox report-date-option">
                                <option value="1" selected>Xls</option>
                                <option value="0" >Csv</option>
                                <option value="3">Pdf</option>
                            </select>
                        </div>
                        <div style="float: left;">
                            <a href="#" onclick="return DownloadReport()" class="save-icon"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
                        </div>
                        <div style="float: left; border-left: 1px solid #000; margin: 0 10px 0 5px; height: 89%;">
                        </div>
                        <div style="float: left;">
                            <select id="drpItemType" name="drpItemType" onchange="drpItemType_OnChange();" class="textbox report-date-option">
                                <option value="0">Item Type:</option>
                                <option value="All" selected="selected">All</option>
                                <option value="Stock" selected="selected">Stock</option>
                                <option value="Non-Stock">Non-Stock</option>
                                <option value="Kit">Kit</option>
                            </select>
                        </div>
                        <div style="float: left; position: relative; padding-left: 10px;" id="divCostCenter">
                            <span style="padding-right: 10px;">Cost Center:</span>
                            <asp:Literal ID="ltCostCenter" runat="server" />
                        </div>
                        <div style="float: left; position: relative; padding-left: 10px;" id="divDateFrom">
                            <i class="fa fa-calendar calendar" aria-hidden="true"></i>
                            <span style="padding-right: 10px;">From</span>
                            <input type="text" id="txtDateFrom" name="txtDateFrom" class="textbox report-date" />
                        </div>
                        <div style="float: left; position: relative; padding-left: 10px;" id="divDateTo">
                            <i class="fa fa-calendar calendar" aria-hidden="true"></i>
                            <span style="padding-right: 10px;">to</span>
                            <input type="text" id="txtDateTo" name="txtDateTo" class="textbox report-date" />
                        </div>
                        <div style="float: left; padding: 0 0 0 7px;">
                            <button type="submit" name="btnGo" id="btnGo" class="site-button report-go-button" onclick="return setValue();">Go</button>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div>
                    <asp:Literal ID="ltPageContent" runat="server"></asp:Literal>
                </div>
                <div style="padding-left: 10px; font-size: 18px;">
                    <div id="modalpopup">
                        <div id="modalpopup-content">
                            <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                            <div id="modalpopup-text">Loading...</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </asp:Panel>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtDateFrom").val(localStorage.getItem("txtDateFrom"));
            $("#txtDateTo").val(localStorage.getItem("txtDateTo"));
            $("#drpCostCenter").val(localStorage.getItem("drpCostCenter"));
            if (localStorage.getItem("drpCostCenter") == null) {
                $("#drpCostCenter").val("All");
            }
            if (localStorage.getItem("txtDateFrom") == null) {
                $("#txtDateFrom").val("01/07/2018");
            }
            else {
                $("#txtDateFrom").val(localStorage.getItem("txtDateFrom"));
            }
            if (localStorage.getItem("txtDateTo") == null) {
                var date = new Date();
                date = new Intl.DateTimeFormat('en-AU').format(date);
                $("#txtDateTo").val(date);
            }
            else {
                $("#txtDateTo").val(localStorage.getItem("txtDateTo"));
            }

            if (localStorage.getItem("drpItemType") == null) {
                localStorage.setItem("drpItemType", "Stock");
            }
            else {
                $("#drpItemType").val(localStorage.getItem("drpItemType"));
            }
            localStorage.removeItem("txtDateFrom");
            localStorage.removeItem("txtDateTo");
            localStorage.removeItem("drpItemType");
            localStorage.removeItem("drpCostCenter");
        });

        function setValue() {
            localStorage.setItem("txtDateFrom", $("#txtDateFrom").val());
            localStorage.setItem("txtDateTo", $("#txtDateTo").val());
            localStorage.setItem("drpItemType", $("#drpItemType").val());
            localStorage.setItem("drpCostCenter", $("#drpCostCenter").val());
            ShowModalPopup("This will take at least 1 minute to load...");
            return true;
        }

        function DownloadReport() {
            ShowModalPopup("Processing...");
            var param = new Object();
            param.Type = $("#drpSave").val();
            param.StartDate = $("#txtDateFrom").val();
            param.EndDate = $("#txtDateTo").val();
            param.CostCenter = $("#drpCostCenter").val();
            $.ajax({
                type: "POST",
                url: "../ActionService.asmx/DownloadReport",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    window.open(result.d);
                    CloseModalPopup();
                },
                fail: function (result) {
                    CloseModalPopup();
                    ShowModalPopup("A problem was encountered creating the report.");
                }
            });
            return false;
        }
    </script>

    <script type="text/javascript">
        function ShowModalPopup(message) {
            var height = $(window).height();
            var width = $(window).width();
            var popup = $("#modalpopup-content");
            var posTop = (height / 2 - popup.height() / 2) - 70;
            var posLeft = (width / 2 - popup.width() / 2) - 150;

            $("#modalpopup-content").css({
                "margin-top": posTop,
                "margin-left": posLeft
            });

            $("#modalpopup-text").html(message);
            $("#modalpopup").css({ "display": "block" });
        }

        </script>

</body>
</html>
