﻿using InterpriseSuiteEcommerceCommon;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Xml.Linq;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System.Data.SqlClient;
using System.Data;

namespace InterpriseSuiteEcommerce
{
    public partial class soh : System.Web.UI.Page
    {
        Customer ThisCustomer = Customer.Current;
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.CacheControl = "private";
            Response.Expires = 0;
            Response.AddHeader("pragma", "no-cache");
            ServiceFactory.GetInstance<IRequestCachingService>()
                          .PageNoCache();

            InitializePageContent();
        }

        private void InitializePageContent()
        {
            var content = new StringBuilder();
            content.AppendLine("<select id=\"drpCostCenter\" name=\"drpCostCenter\" class=\"textbox report-cost-center\">");
            content.AppendFormat("<option value=\"{0}\">{0}</option>", "All");
            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader rs = DB.GetRSFormat(con, "SELECT CostCenterCode_DEV004817 FROM SGECostCenter_DEV004817 ORDER BY CostCenterCode_DEV004817 ASC"))
                {
                    while (rs.Read())
                    {
                        content.AppendFormat("<option value=\"{0}\">{0}</option>", DB.RSField(rs, "CostCenterCode_DEV004817"));
                    }
                }
            }
            content.AppendLine("</select>");
            ltCostCenter.Text = content.ToString();

            string dateFrom = Request.Form["txtDateFrom"], dateTo = Request.Form["txtDateTo"], type = Request.Form["drpItemType"], costCenter = Request.Form["drpCostCenter"];
            if (dateFrom == null || dateFrom == "")
            {
                dateFrom = String.Format("07/01/2018");
            }
            else
            {
                dateFrom = DateTime.ParseExact(dateFrom, "d", System.Globalization.CultureInfo.GetCultureInfo("en-au")).Date.ToString();
            }
            if (dateTo == null || dateTo == "")
            {
                dateTo = String.Format("{0:MM/dd/yyyy}", DateTime.Now.Date);
            }
            else
            {
                dateTo = DateTime.ParseExact(dateTo, "d", System.Globalization.CultureInfo.GetCultureInfo("en-au")).Date.ToString();
            }
            if (string.IsNullOrEmpty(type))
            {
                type = "Stock";
            }
            if (string.IsNullOrEmpty(costCenter))
            {
                costCenter = "All";
            }
            List<XmlPackageParam> runtimeParams = new List<XmlPackageParam>();
            runtimeParams.Add(new XmlPackageParam("DateFrom", dateFrom));
            runtimeParams.Add(new XmlPackageParam("DateTo", dateTo));
            runtimeParams.Add(new XmlPackageParam("Type", type));
            runtimeParams.Add(new XmlPackageParam("WarehouseCode", "INGLEBURN-STOCK"));
            runtimeParams.Add(new XmlPackageParam("SkinID", "99"));
            runtimeParams.Add(new XmlPackageParam("CostCenter", costCenter));
            //runtimeParams.Add(new XmlPackageParam("CostCenter", "Harvey Norman Cost Centre"));

            var package = new XmlPackage2("page.adminsohreport.xml.config", runtimeParams);
            ltPageContent.Text = package.TransformString();
        }
    }
}