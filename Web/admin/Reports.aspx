<%@ Page Language="C#" MasterPageFile="~/admin/default.master" AutoEventWireup="true" CodeFile="Reports.aspx.cs" Inherits="Reports" Title="Reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pnlMain" runat="Server">
    <link href="mvwres:1-DevExpress.Web.ASPxEditors.Css.Default.css" rel="stylesheet" type="text/css" />
    <link href="mvwres:1-DevExpress.Web.ASPxEditors.Css.Default.css" rel="stylesheet" type="text/css" />
    <link href="mvwres:1-DevExpress.Web.ASPxEditors.Css.Default.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.min.css" />
    <script src="//code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/55068458dc.js"></script>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" />
    <style>
        body {
            font-family: Roboto;
            font-weight: 400;
            font-size: 14px;
        }

        #modalpopup, #messagebox, #confirmationbox {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 999; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        #modalpopup-content {
            background-color: #f2f2f2;
            border-radius: 10px;
            box-shadow: 0 1px 5px #000;
            padding: 30px 30px;
            text-align: center;
            vertical-align: middle;
            min-height: 125px;
            min-width: 200px;
            font-size: 15px;
            display: inline-table;
        }

            #modalpopup-content i {
                font-size: 40px;
            }

        #modalpopup-text {
            padding-top: 5px;
        }

        .filter-date {
            background-color: #f0f0f0;
            border: 1px solid #a8a8a8;
            box-sizing: border-box;
            float: left;
            padding: 3px 3px 0 10px;
            height: 32px;
            border-left: none;
        }

        .hide {
            display: none;
        }
    </style>

    <script>
        $(function () {
            $("#txtDateFrom").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#txtDateTo").datepicker({ dateFormat: 'dd/mm/yy' });
        });
    </script>
    <asp:Panel ID="pnlReports" runat="server" CssClass="createform">
        <div class="form-header">Reports</div>
        <div class="form-horizontal">
            <div class="menu-group">
                <ul id="menu">
                    <li><a href="reports.aspx?report=RPT-000197&t=home">Sales by Category - Detail</a></li>
                    <li><a href="reports.aspx?report=RPT-000193&t=home">Sales by Category - Summary</a></li>
                    <li><a href="reports.aspx?report=RPT-000195&t=home">Sales by Invoice - Detail</a></li>
                    <li><a href="reports.aspx?report=RPT-000198&t=home">Sales by Invoice - Summary</a></li>
                    <li><a href="reports.aspx?report=RPT-000199&t=home">SB Sales Order - Detail</a></li>
                    <li><a href="soh.aspx?t=home">SOH</a></li>
                </ul>
            </div>
            <div class="report-content">
                <div class="filter-date">
                    <div style="float: left;">
                        <span style="padding-right: 10px;">Cost Center:</span>
                        <asp:Literal ID="ltCostCenter" runat="server" />
                        <%--<select id="drpDate" name="drpDate" onchange="drpDate_OnChange();" class="textbox report-date-option">
                            <option value="1">Select a Cost Center</option>
                        </select>--%>
                    </div>
                    <div style="float: left; position: relative; padding-left: 10px;" id="divDateFrom">
                        <i class="fa fa-calendar calendar" id="iDateFrom"></i>
                        <span style="padding-right: 10px;">From</span>
                        <input type="text" id="txtDateFrom" name="txtDateFrom" class="textbox report-date" />
                    </div>
                    <div style="float: left; position: relative; padding-left: 10px;" id="divDateTo">
                        <i class="fa fa-calendar calendar" id="iDateTo"></i>
                        <span style="padding-right: 10px;">to</span>
                        <input type="text" id="txtDateTo" name="txtDateTo" class="textbox report-date" />
                    </div>
                    <div style="float: left; padding: 2px 0 0 7px;">
                        <button type="submit" name="btnGo" id="btnGo" class="site-button report-go-button" onclick="return setValue();">Go</button>
                    </div>
                </div>
                <dxwc:ReportToolbar ID="ReportToolbar1" runat="server" ReportViewer="<%# ViewerReport %>"
                    ShowDefaultButtons="False">
                    <Items>
                        <dxwc:ReportToolbarButton ItemKind="PrintReport" ToolTip="Print the report" />
                        <dxwc:ReportToolbarButton ItemKind="PrintPage" ToolTip="Print the current page" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton Enabled="False" ItemKind="FirstPage" ToolTip="First Page" />
                        <dxwc:ReportToolbarButton Enabled="False" ItemKind="PreviousPage" ToolTip="Previous Page" />
                        <dxwc:ReportToolbarLabel Text="Page" />
                        <dxwc:ReportToolbarComboBox ItemKind="PageNumber" Width="50">
                        </dxwc:ReportToolbarComboBox>
                        <dxwc:ReportToolbarLabel Text="of" />
                        <dxwc:ReportToolbarTextBox ItemKind="PageCount" Width="35px" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton ItemKind="NextPage" ToolTip="Next Page" />
                        <dxwc:ReportToolbarButton ItemKind="LastPage" ToolTip="Last Page" />
                        <dxwc:ReportToolbarSeparator />
                        <dxwc:ReportToolbarButton ItemKind="SaveToDisk" ToolTip="Export a report and save it to the disk" />
                        <dxwc:ReportToolbarButton ItemKind="SaveToWindow" ToolTip="Export a report and show it in a new window" />
                        <dxwc:ReportToolbarComboBox ItemKind="SaveFormat" Width="80">
                            <Elements>
                                <dxwc:ListElement Text="Xls" Value="csv" />
                                <dxwc:ListElement Text="Pdf" Value="pdf" />
                                <%--<dxwc:ListElement Text="Xls" Value="xls" />--%>
                                <dxwc:ListElement Text="Rtf" Value="rtf" />
                                <dxwc:ListElement Text="Mht" Value="mht" />
                                <dxwc:ListElement Text="Text" Value="txt" />
                                <dxwc:ListElement Text="Image" Value="png" />
                            </Elements>
                        </dxwc:ReportToolbarComboBox>
                    </Items>
                    <ClientSideEvents ItemClick="function(s, e) {
	                        if(e.item.name =='Home' ) 
                                window.location.href = 'portal.aspx';
                                                                }" />
                    <Styles>
                        <LabelStyle>
                            <Margins MarginLeft="3px" MarginRight="3px" />
                        </LabelStyle>
                    </Styles>
                </dxwc:ReportToolbar>
                <div>
                    <dxwc:ReportViewer ID="ViewerReport" runat="server" OnCacheReportDocument="ViewerReport_CacheReportDocument"></dxwc:ReportViewer>
                </div>
                <div style="margin-top: 10px;">
                    <asp:Literal ID="ltReport" runat="server" Visible="false" />
                </div>
            </div>
        </div>
        <div style="clear: both;"></div>
        <div style="padding-left: 10px; font-size: 18px;">
            <div id="modalpopup">
                <div id="modalpopup-content">
                    <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                    <div id="modalpopup-text">Loading...</div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            function ShowModalPopup(message) {
                var height = $(window).height();
                var width = $(window).width();
                var popup = $("#modalpopup-content");
                var posTop = (height / 2 - popup.height() / 2) - 70;
                var posLeft = (width / 2 - popup.width() / 2) - 150;

                $("#modalpopup-content").css({
                    "margin-top": posTop,
                    "margin-left": posLeft
                });

                $("#modalpopup-text").html(message);
                $("#modalpopup").css({ "display": "block" });
            }

        </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#txtDateFrom").val(localStorage.getItem("txtDateFrom"));
                $("#txtDateTo").val(localStorage.getItem("txtDateTo"));
                $("#drpCostCenter").val(localStorage.getItem("drpCostCenter"));
                if (localStorage.getItem("drpCostCenter") == null) {
                    $("#drpCostCenter").val("All");
                }

                if (localStorage.getItem("txtDateFrom") == null) {
                    var date = new Date();
                    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
                    date = new Intl.DateTimeFormat('en-AU').format(date);
                    firstDay = new Intl.DateTimeFormat('en-AU').format(firstDay);
                    $("#txtDateTo").val(date);
                    $("#txtDateFrom").val(firstDay);
                }
                localStorage.removeItem("txtDateFrom");
                localStorage.removeItem("txtDateTo");
                localStorage.removeItem("drpCostCenter");
            });

            function setValue() {
                localStorage.setItem("txtDateFrom", $("#txtDateFrom").val());
                localStorage.setItem("txtDateTo", $("#txtDateTo").val());
                localStorage.setItem("drpCostCenter", $("#drpCostCenter").val());
                ShowModalPopup("Processing...");
                return true;
            }
    </script>
    </asp:Panel>
</asp:Content>
