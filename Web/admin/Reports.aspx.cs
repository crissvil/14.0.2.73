
using System;
using System.Data;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using InterpriseSuiteEcommerceCommon;
using System.Data.SqlClient;
using DevExpress.XtraReports.Web;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System;
using System.Web;
using System.Text;

public partial class Reports : System.Web.UI.Page
{
    #region Methods

    #region GetMaxRequestLength
    /// <summary>
    /// Gets the maximum number of kilobytes the server can accept in the input type=file
    /// </summary>
    /// <returns></returns>
    private int GetMaxRequestLength()
    {
        HttpRuntimeSection section = ConfigurationManager.GetSection("system.web/httpRuntime") as HttpRuntimeSection;
        if (null != section)
        {
            return section.MaxRequestLength;
        }

        return 0;
    }
    #endregion

    private void InitializeContent()
    {
        var content = new StringBuilder();
        content.AppendLine("<select id=\"drpCostCenter\" name=\"drpCostCenter\" class=\"textbox report-date-option\">");
        content.AppendFormat("<option value=\"{0}\">{0}</option>", "All");
        using (SqlConnection con = DB.NewSqlConnection())
        {
            con.Open();
            using (IDataReader rs = DB.GetRSFormat(con, "SELECT CostCenterCode_DEV004817 FROM SGECostCenter_DEV004817 ORDER BY CostCenterCode_DEV004817 ASC"))
            {
                while (rs.Read())
                {
                    content.AppendFormat("<option value=\"{0}\">{0}</option>", DB.RSField(rs, "CostCenterCode_DEV004817"));
                }
            }
        }
        content.AppendLine("</select>");
        ltCostCenter.Text = content.ToString();
    }

    protected void ViewerReport_CacheReportDocument(object sender, CacheReportDocumentEventArgs e)
    {
        e.Key = Guid.NewGuid().ToString();
        Page.Session[e.Key] = e.SaveDocumentToMemoryStream();
    }

    #endregion

    #region Event Handlers

    #region OnInit
    /// <summary>
    /// 
    /// </summary>
    /// <param name="e"></param>
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        var reportcode = HttpContext.Current.Request.QueryString["report"];
        var costCenter = System.Web.HttpContext.Current.Request.Form["drpCostCenter"];
        var dateFrom = System.Web.HttpContext.Current.Request.Form["txtDateFrom"];
        var dateTo = System.Web.HttpContext.Current.Request.Form["txtDateTo"];
        if (dateFrom != null && dateFrom != "")
        {
            dateFrom = DateTime.ParseExact(dateFrom, "d", System.Globalization.CultureInfo.GetCultureInfo("en-au")).Date.ToString();
        }
        if (dateTo != null && dateTo != "")
        {
            dateTo = DateTime.ParseExact(dateTo, "d", System.Globalization.CultureInfo.GetCultureInfo("en-au")).Date.ToString();
        }
        Customer thisCustomer = Customer.Current;
        InitializeContent();
        try
        {
            switch (reportcode)
            {
                case "RPT-000197":
                    {
                        ViewerReport.Report = InterpriseHelper.SalesbyCategoryDetail(reportcode, costCenter, dateFrom, dateTo);
                        break;
                    }
                case "RPT-000193":
                    {
                        ViewerReport.Report = InterpriseHelper.SalesbyCategorySummary(reportcode, costCenter, dateFrom, dateTo);
                        break;
                    }
                case "RPT-000195":
                    {
                        ViewerReport.Report = InterpriseHelper.SalesbyInvoiceDetail(reportcode, costCenter, dateFrom, dateTo);
                        break;
                    }
                case "RPT-000198":
                    {
                        ViewerReport.Report = InterpriseHelper.SalesbyInvoiceSummary(reportcode, costCenter, dateFrom, dateTo);
                        break;
                    }
                case "RPT-000199":
                    {
                        ViewerReport.Report = InterpriseHelper.SBSalesOrderDetail(reportcode, costCenter, dateFrom, dateTo);
                        break;
                    }
            }
            
            ViewerReport.AutoSize = true;
            ltReport.Visible = false;
            ViewerReport.Visible = true;
        }
        catch (Exception ex)
        {
            ltReport.Text = "No data";
            ltReport.Visible = true;
            ViewerReport.Visible = false;
        }
    }

    #endregion
    

}



