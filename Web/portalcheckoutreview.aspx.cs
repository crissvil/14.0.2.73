﻿using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.DataAccess;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using InterpriseSuiteEcommerceGateways;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace InterpriseSuiteEcommerce
{
    public partial class portalcheckoutreview : SkinBase
    {
        #region Declaration

        InterpriseShoppingCart cart = null;
        PayPalExpress pp;

        #endregion

        #region DomainServices

        INavigationService _navigationService = null;
        ICustomerService _customerService = null;
        IShoppingCartService _shoppingCartService = null;
        IAppConfigService _appConfigService = null;

        #endregion

        #region Events

        protected override void OnInit(EventArgs e)
        {
            RegisterDomainServices();
            InitializeComponent();

            if (AppLogic.AppConfigBool("MaxMind.Enabled"))
            {
                ctrlScript.ShowMaxMind = true;
            }

            base.OnInit(e);
        }

        protected void Page_Load(object sender, System.EventArgs e)
        {
            this.PageNoCache();
            this.RequireSecurePage();

            _customerService.DoIsOver13Checking();
            _customerService.DoIsCreditOnHoldChecking();
            this.SETitle = "Welcome to the smartportal";
            // -----------------------------------------------------------------------------------------------
            // NOTE ON PAGE LOAD LOGIC:
            // We are checking here for required elements to allowing the customer to stay on this page.
            // Many of these checks may be redundant, and they DO add a bit of overhead in terms of db calls, but ANYTHING really
            // could have changed since the customer was on the last page. Remember, the web is completely stateless. Assume this
            // page was executed by ANYONE at ANYTIME (even someone trying to break the cart). 
            // It could have been yesterday, or 1 second ago, and other customers could have purchased limitied inventory products, 
            // coupons may no longer be valid, etc, etc, etc...
            // -----------------------------------------------------------------------------------------------
            ThisCustomer.RequireCustomerRecord();

            if (ThisCustomer.IsNotRegistered)
            {
                if (!AppLogic.AppConfigBool("PasswordIsOptionalDuringCheckout") && !AppLogic.AppConfigBool("Checkout.UseOnePageCheckout"))
                {
                    _navigationService.NavigateToUrl("createaccount.aspx?checkout=true");
                }

                ThisCustomer.EMail = ServiceFactory.GetInstance<ICustomerService>().GetAnonEmail();
            }

            _customerService.DoRegisteredCustomerShippingAndBillingAddressChecking();

            SectionTitle = AppLogic.GetString("checkoutreview.aspx.1");
            cart = new InterpriseShoppingCart(base.EntityHelpers, SkinID, ThisCustomer, CartTypeEnum.ShoppingCart, String.Empty, false, true);

            var delete = ProcessDelete();

            //remove all order options if those are the only items left in the cart
            bool blnRemoveAllOrderOptions = (cart.CartItems.Count() > 0) && !cart.CartItems.Any(m => !m.IsCheckoutOption);
            if (blnRemoveAllOrderOptions)
            {
                //the only items in the cart are order options, so delete everything
                var shoppingCartService = ServiceFactory.GetInstance<IShoppingCartService>();
                shoppingCartService.ClearCartByLoggedInCustomerCartType(CartTypeEnum.ShoppingCart);
                cart.CartItems.Clear();
                //remove all reserved
                shoppingCartService.ClearCartReservationByCurrentlyLoggedInCustomer();
            }

            if (cart.IsEmpty())
            {
                _navigationService.NavigateToUrl("placeorder.aspx");
            }

            if (cart.InventoryTrimmed)
            {
                pnlErrorMsg.Visible = true;
                ErrorMsgLabel.Text = AppLogic.GetString("shoppingcart.aspx.1");
            }

            string couponCode = string.Empty;
            string couponErrorMessage = string.Empty;

            if (!cart.MeetsMinimumOrderAmount(AppLogic.AppConfigUSDecimal("CartMinOrderAmount")))
            {
                _navigationService.NavigateToShoppingCartRestLinkBack();
            }

            if (!cart.MeetsMinimumOrderQuantity(AppLogic.AppConfigUSInt("MinCartItemsBeforeCheckout")))
            {
                _navigationService.NavigateToShoppingCartRestLinkBack();
            }

            if (cart.HasRegistryItemButParentRegistryIsRemoved() || cart.HasRegistryItemsRemovedFromRegistry())
            {
                cart.RemoveRegistryItemsHasDeletedRegistry();
                cart.RemoveRegistryItemsHasBeenDeletedInRegistry();
                _navigationService.NavigateToShoppingCartRestLinkBackWithErroMessage(AppLogic.GetString("editgiftregistry.error.18"));
            }

            if (cart.HasRegistryItemsAndOneOrMoreItemsHasZeroInNeed())
            {
                _navigationService.NavigateToShoppingCartRestLinkBackWithErroMessage(AppLogic.GetString("editgiftregistry.error.15"));
            }

            if (cart.HasRegistryItemsAndOneOrMoreItemsExceedsToTheInNeedQuantity())
            {
                _navigationService.NavigateToShoppingCartRestLinkBackWithErroMessage(AppLogic.GetString("editgiftregistry.error.14"));
            }

            //_customerService.DoShippingAddressModificationChecking();
            _shoppingCartService.DoHasAppliedInvalidGiftCodesChecking(cart);
            _shoppingCartService.DoHasAppliedInvalidLoyaltyPointsChecking(cart);
            _shoppingCartService.DoHasAppliedInvalidCreditMemosChecking(cart);

            if (!IsPostBack || delete)
            {
                if (!SetShippingMethod())
                {
                    pnlErrorMsg.Visible = true;
                    ErrorMsgLabel.Text = "An error was encountered. No Shipping option selected. Please contact support. <a href=\"portal.aspx\">Click here.</a>";
                }
                if (!SetPaymentTerm())
                {
                    pnlErrorMsg.Visible = true;
                    ErrorMsgLabel.Text = "An error was encountered. No Payment option selected. Please contact support. <a href=\"portal.aspx\">Click here.</a>";
                }
                AddShippingItem();
                AddPalletFeeCartoonsItem();
                AddPalletFeeSatchelItem();
            }
            InitializePageContent();
        }

        private void InitializeComponent()
        {
            string ordertext = AppLogic.GetString("custom.text.21");
            this.btnChargeAccount1.Click += btnChargeAccount1_Click;
            this.btnChargeAccount2.Click += btnChargeAccount1_Click;
            this.btnContinueShoppingTop.Click += btnContinueShopping_Click;
            this.btnUpdateCart.Click += btnUpdateCart_Click;
            switch (ThisCustomer.CostCenter)
            {
                case "Smartbag-PREMIUM CUE":
                case "Smartbag-PREMIUM APG":
                case "Smartbag-PREMIUM DION LEE":
                    btnChargeAccount1.Text = AppLogic.GetString("portal.aspx.10");
                    btnChargeAccount2.Text = AppLogic.GetString("portal.aspx.10");
                    ordertext = btnChargeAccount1.Text;
                    ltOrderName.Visible = true;
                    litComments.Text = AppLogic.GetString("portal.aspx.47");
                    break;
            }
            checkoutreviewaspx6.Text = string.Format(AppLogic.GetString("portal.aspx.44"), ordertext);
        }

        void btnChargeAccount1_Click(object sender, EventArgs e)
        {
            ProcessCheckout();
        }

        void btnUpdateCart_Click(object sender, EventArgs e)
        {
            List<KeyValuePair<int, string>> cartIdsAndunitMeasureCodes = new List<KeyValuePair<int, string>>();
            for (int i = 0; i <= Request.Form.Count - 1; i++)
            {
                string fld = Request.Form.Keys[i];
                string fldval = Request.Form[Request.Form.Keys[i]];
                int recID;
                string quantity;
                if (fld.StartsWith("Quantity"))
                {
                    if (fldval.StartsWith(Localization.GetNumberDecimalSeparatorLocaleString(cart.ThisCustomer.LocaleSetting)))
                    {
                        fldval = fldval.Insert(0, Localization.GetNumberZeroLocaleString(cart.ThisCustomer.LocaleSetting));
                    }
                    if (Regex.IsMatch(fldval, AppLogic.AllowedQuantityWithDecimalRegEx(cart.ThisCustomer.LocaleSetting), RegexOptions.Compiled))
                    {
                        recID = Localization.ParseUSInt(fld.Substring("Quantity".Length + 1));
                        quantity = fldval;
                        decimal iquan = Convert.ToDecimal(quantity);

                        if (iquan < 0) { iquan = 0; }
                        cart.SetItemQuantity(recID, iquan);
                    }
                    else
                    {
                        ErrorMsgLabel.Text += "The item quantity must have a valid value.";
                    }
                }
            }
        }

        void btnContinueShopping_Click(object sender, EventArgs e)
        {
            _navigationService.NavigateToUrl("placeorder.aspx");
        }

        protected override void OnUnload(EventArgs e)
        {
            if (cart != null)
            {
                cart.Dispose();
            }
            base.OnUnload(e);
        }

        #endregion

        #region Methods

        private void RegisterDomainServices()
        {
            _navigationService = ServiceFactory.GetInstance<INavigationService>();
            _customerService = ServiceFactory.GetInstance<ICustomerService>();
            _shoppingCartService = ServiceFactory.GetInstance<IShoppingCartService>();
            _appConfigService = ServiceFactory.GetInstance<IAppConfigService>();
        }

        private bool IsPayPalCheckout
        {
            get
            {
                return (Request.QueryString["PayPal"] ?? bool.FalseString) == bool.TrueString && Request.QueryString["token"] != null;
            }
        }

        private void InitializePageContent()
        {
            decimal gCardAllocate = Decimal.Zero;
            decimal gCertAllocate = Decimal.Zero;
            decimal loyaltyPointsAllocate = Decimal.Zero;
            decimal creditMemosAllocate = Decimal.Zero;

            if (cart.HasMultipleShippingAddresses() || cart.HasRegistryItems())
            {

                var splittedCarts = cart.SplitIntoMultipleOrdersByDifferentShipToAddresses();
                foreach (var splitCart in splittedCarts)
                {
                    splitCart.BuildSalesOrderDetails(true);
                    OrderSummary.Text += splitCart.RenderHTMLLiteral(new DefaultShoppingCartPageLiteralRenderer(RenderType.Company, "page.portalcheckoutreview.xml.config", gCardAllocate, gCertAllocate, loyaltyPointsAllocate, creditMemosAllocate, ""));
                    gCardAllocate += splitCart.GiftCardsTotalCreditAllocated;
                    gCertAllocate += splitCart.GiftCertsTotalCreditAllocated;
                    loyaltyPointsAllocate += splitCart.LoyaltyPointsCreditAllocated;
                    creditMemosAllocate += splitCart.CreditMemosCreditAllocated;
                }
            }
            else if (cart.HasMultipleShippingMethod())
            {
                var ordersWithDifferentShipping = _shoppingCartService.SplitShippingMethodsInMultipleOrders();
                foreach (var order in ordersWithDifferentShipping)
                {
                    order.BuildSalesOrderDetails(true);
                    OrderSummary.Text += order.RenderHTMLLiteral(new DefaultShoppingCartPageLiteralRenderer(RenderType.Company, "page.portalcheckoutreview.xml.config", gCardAllocate, gCertAllocate, loyaltyPointsAllocate, creditMemosAllocate, ""));
                    gCardAllocate += order.GiftCardsTotalCreditAllocated;
                    gCertAllocate += order.GiftCertsTotalCreditAllocated;
                    loyaltyPointsAllocate += order.LoyaltyPointsCreditAllocated;
                    creditMemosAllocate += order.CreditMemosCreditAllocated;
                }
            }
            else
            {
                //If the shopping cart contains only Electronic Downloads or Services then pass a "false" parameter for computeFreight.
                if (cart.IsNoShippingRequired())
                {
                    cart.BuildSalesOrderDetails(false, true, "");
                }
                else
                {
                    cart.BuildSalesOrderDetails(true);
                }

                string couponCode = String.Empty;
                if (!ThisCustomer.CouponCode.IsNullOrEmptyTrimmed()) couponCode = ThisCustomer.CouponCode;

                OrderSummary.Text = cart.RenderHTMLLiteral(new DefaultShoppingCartPageLiteralRenderer(RenderType.Company, "page.portalcheckoutreview.xml.config", couponCode));
            }

            // Show only the "Edit Address" link for registered customer and if appconfig: ShowEditAddressLinkOnCheckOutReview = true
            if (ThisCustomer.IsRegistered && _appConfigService.ShowEditAddressLinkOnCheckOutReview)
            {
                EditBillingAddress.Text = AppLogic.GetString("editaddress.aspx.1");

                if (_appConfigService.AllowShipToDifferentThanBillTo)
                {
                    EditShippingAddress.Text = AppLogic.GetString("editaddress.aspx.1");
                }
                else
                {
                    ordercs57.Visible = false;
                }
            }

            switch (ThisCustomer.CostCenter)
            {
                case "Smartbag-PREMIUM APG":
                case "Smartbag-PREMIUM CUE":
                case "Smartbag-PREMIUM DION LEE":
                    litBillingAddress.Text = ThisCustomer.PrimaryBillingAddress.DisplayString(true, true, true, false, "<div class='height-5'></div>");
                    break;
                default:
                    litBillingAddress.Text = ThisCustomer.PrimaryShippingAddress.DisplayString(true, true, true, false, "<div class='height-5'></div>");
                    break;
            }


            if (cart.HasPickupItem())
            {
                pnlShippingPickMessage.Visible = true;
            }

            if (cart.HasMultipleShippingAddresses() || (cart.HasRegistryItems() && cart.CartItems.Count > 1))
            {
                litShippingAddress.Text = "<br/>Multiple Ship Addresses";
            }
            else
            {
                Address shippingAddress = null;

                //added for PayPal ADDRESSOVERRIDE  
                if (IsPayPalCheckout && !AppLogic.AppConfigBool("PayPalCheckout.OverrideAddress"))
                {
                    if (!cart.HasShippableComponents())
                    {
                        shippingAddress = ThisCustomer.PrimaryShippingAddress;
                    }
                    else
                    {
                        pp = new PayPalExpress();
                        var GetPayPalDetails = pp.GetExpressCheckoutDetails(Request.QueryString["token"]).GetExpressCheckoutDetailsResponseDetails;
                        shippingAddress = new Address()
                        {
                            Name = GetPayPalDetails.PayerInfo.Address.Name,
                            Address1 = GetPayPalDetails.PayerInfo.Address.Street1 + (GetPayPalDetails.PayerInfo.Address.Street2 != String.Empty ? Environment.NewLine : String.Empty) + GetPayPalDetails.PayerInfo.Address.Street2,
                            City = GetPayPalDetails.PayerInfo.Address.CityName,
                            State = GetPayPalDetails.PayerInfo.Address.StateOrProvince,
                            PostalCode = GetPayPalDetails.PayerInfo.Address.PostalCode,
                            Country = AppLogic.ResolvePayPalAddressCode(GetPayPalDetails.PayerInfo.Address.CountryName.ToString()),
                            CountryISOCode = AppLogic.ResolvePayPalAddressCode(GetPayPalDetails.PayerInfo.Address.Country.ToString()),
                            Phone = GetPayPalDetails.PayerInfo.ContactPhone ?? ThisCustomer.PrimaryShippingAddress.Phone
                        };
                    }
                }
                else
                {
                    if (cart.OnlyShippingAddressIsNotCustomerDefault())
                    {
                        var item = cart.FirstItem();
                        shippingAddress = Address.Get(ThisCustomer, AddressTypes.Shipping, item.m_ShippingAddressID, item.GiftRegistryID);
                    }
                    else
                    {
                        shippingAddress = ThisCustomer.PrimaryShippingAddress;
                    }
                }

                if (_appConfigService.AllowShipToDifferentThanBillTo)
                {
                    litShippingAddress.Text = GetShipToList(shippingAddress);
                }
                else
                {
                    ordercs57.Visible = false;
                }
            }
        }

        private string GetPaymentMethod(Address BillingAddress)
        {
            var sPmtMethod = new StringBuilder();
            var paymentInfo = PaymentTermDTO.Find(ThisCustomer.PaymentTermCode);

            //  We should have a default payment method
            //  For debugging purposes, have this check here
            if (string.IsNullOrEmpty(paymentInfo.PaymentMethod))
            {
                throw new InvalidOperationException("No payment method defined!");
            }

            if (!cart.IsSalesOrderDetailBuilt)
            {
                if (cart.IsNoShippingRequired())
                {
                    cart.BuildSalesOrderDetails(false, true, "", true);
                }
                else
                {
                    cart.BuildSalesOrderDetails(true);
                }
            }

            if ((ThisCustomer.PaymentTermCode.Trim().Equals("PURCHASE ORDER", StringComparison.InvariantCultureIgnoreCase)) ||
                (ThisCustomer.PaymentTermCode.Trim().Equals("REQUEST QUOTE", StringComparison.InvariantCultureIgnoreCase)))
            {
                sPmtMethod.Append(ThisCustomer.PaymentTermCode.ToUpperInvariant());
            }
            else
            {
                switch (paymentInfo.PaymentMethod)
                {
                    case DomainConstants.PAYMENT_METHOD_CREDITCARD:

                        if ((cart.GetOrderTotal() == decimal.Zero) && (AppLogic.AppConfigBool("SkipPaymentEntryOnZeroDollarCheckout")))
                        {
                            sPmtMethod.Append(AppLogic.GetString("checkoutpayment.aspx.8"));
                        }
                        else
                        {
                            sPmtMethod.AppendFormat("{0} ({1})", Security.HtmlEncode(paymentInfo.PaymentMethod), HttpUtility.HtmlEncode(ThisCustomer.PaymentTermCode));
                            sPmtMethod.Append("<br/>");
                            sPmtMethod.Append("<table class=\"payment-method\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
                            sPmtMethod.Append("<tr><td>");
                            sPmtMethod.Append(AppLogic.GetString("checkoutreview.aspx.10"));
                            sPmtMethod.Append("</td><td>");
                            sPmtMethod.Append(BillingAddress.CardName);
                            sPmtMethod.Append("</td></tr>");
                            sPmtMethod.Append("<tr><td>");
                            sPmtMethod.Append(AppLogic.GetString("checkoutreview.aspx.11"));
                            sPmtMethod.Append("</td><td>");
                            DataView dt = AppLogic.GetCustomerCreditCardType("");
                            string cardTypeDescription = string.Empty;
                            try
                            {
                                cardTypeDescription = dt.Table.Select(string.Format("CreditCardType = '{0}'", BillingAddress.CardType))[0]["CreditCardTypeDescription"].ToString();
                            }
                            catch
                            {
                                cardTypeDescription = BillingAddress.CardType;
                            }
                            sPmtMethod.Append(cardTypeDescription);
                            sPmtMethod.Append("</td></tr>");
                            sPmtMethod.Append("<tr><td>");
                            sPmtMethod.Append(AppLogic.GetString("checkoutreview.aspx.12"));
                            sPmtMethod.Append("</td><td>");
                            sPmtMethod.Append(BillingAddress.CardNumberMaskSafeDisplayFormat);
                            sPmtMethod.Append("</td></tr>");
                            sPmtMethod.Append("<tr><td>");
                            sPmtMethod.Append(AppLogic.GetString("checkoutreview.aspx.13"));
                            sPmtMethod.Append("</td><td>");
                            sPmtMethod.Append(BillingAddress.CardExpirationMonth.PadLeft(2, '0') + "/" + BillingAddress.CardExpirationYear);
                            sPmtMethod.Append("</td></tr>");
                            sPmtMethod.Append("</table>");
                        }
                        break;
                    case DomainConstants.PAYMENT_METHOD_CASH:
                    case DomainConstants.PAYMENT_METHOD_CHECK:
                    case DomainConstants.PAYMENT_METHOD_WEBCHECKOUT:

                        if ((cart.GetOrderTotal() == Decimal.Zero) && (AppLogic.AppConfigBool("SkipPaymentEntryOnZeroDollarCheckout")))
                        {
                            sPmtMethod.Append(AppLogic.GetString("checkoutpayment.aspx.8"));
                        }
                        else
                        {
                            sPmtMethod.AppendFormat("{0} ({1})", paymentInfo.PaymentMethod.ToHtmlEncode(), ThisCustomer.PaymentTermCode.ToHtmlEncode());
                        }
                        break;
                    default:
                        throw new InvalidOperationException("Invalid Payment method!");
                }
            }

            return sPmtMethod.ToString();
        }

        private bool ProcessDelete()
        {
            string[] formkeys = Request.Form.AllKeys;
            if (formkeys.Any(k => k.Contains("bt_Delete")))
            {
                this.PageNoCache();
                ThisCustomer.RequireCustomerRecord();

                // update cart quantities:
                List<KeyValuePair<int, string>> cartIdsAndunitMeasureCodes = new List<KeyValuePair<int, string>>();
                for (int i = 0; i <= Request.Form.Count - 1; i++)
                {
                    string fld = Request.Form.Keys[i];
                    string fldval = Request.Form[Request.Form.Keys[i]];
                    int recID;
                    string quantity;
                    if (fld.StartsWith("Quantity"))
                    {
                        if (fldval.StartsWith(Localization.GetNumberDecimalSeparatorLocaleString(cart.ThisCustomer.LocaleSetting)))
                        {
                            fldval = fldval.Insert(0, Localization.GetNumberZeroLocaleString(cart.ThisCustomer.LocaleSetting));
                        }
                        recID = Localization.ParseUSInt(fld.Substring("Quantity".Length + 1));
                        quantity = fldval;
                        decimal iquan = Convert.ToDecimal(quantity);

                        if (iquan < 1)
                        {
                            iquan = 0;
                            cart.SetItemQuantity(recID, iquan);
                        }
                    }
                }
                ValidateShippingItem();
                return true;
            }
            return false;
        }

        private void ProcessCheckout()
        {
            if (!cart.IsEmpty())
            {
                _shoppingCartService.CheckStockAvailabilityDuringCheckout(cart.HasNoStockPhasedOutItem, cart.HaNoStockAndNoOpenPOItem);
                //check discountinued

                var discontinuedItems = cart.CartItems.Where(c => c.Status.ToLowerInvariant() == "D".ToLowerInvariant())
                                            .Select(itm => itm.m_ShoppingCartRecordID)
                                            .AsParallel().ToList();
                if (discontinuedItems.Count > 0)
                {
                    discontinuedItems.ForEach(recId => { cart.RemoveItem(recId); });
                    _navigationService.NavigateToShoppingCartWitErroMessage(AppLogic.GetString("checkoutpayment.aspx.cs.4").ToUrlEncode());
                }
            }

            //Double check if the shipping item is included on cart.
            AddShippingItem();
            AddPalletFeeCartoonsItem();
            AddPalletFeeSatchelItem();

            string sOrderNotes = CommonLogic.CleanLevelOne(OrderNotes.Text);
            string OrderNumber = String.Empty;
            // ----------------------------------------------------------------
            // Process The Order:
            // ----------------------------------------------------------------
            if (ThisCustomer.PaymentTermCode.IsNullOrEmptyTrimmed() || ThisCustomer.PaymentTermCode != "Invoice")
            {
                pnlErrorMsg.Visible = true;
                ErrorMsgLabel.Text = "An error was encountered. No Payment option selected. Please contact support. <a href=\"portal.aspx\">Click here.</a>";
            }
            else if (cart.FirstItem().m_ShippingMethod != "Invoice")
            {
                pnlErrorMsg.Visible = true;
                ErrorMsgLabel.Text = "An error was encountered. No Shipping option selected. Please contact support. <a href=\"portal.aspx\">Click here.</a>";
            }
            else if (sOrderNotes.Length < 1 && (ThisCustomer.CostCenter == "Smartbag-PREMIUM APG" || ThisCustomer.CostCenter == "Smartbag-PREMIUM CUE" ||
                ThisCustomer.CostCenter == "Smartbag-PREMIUM DION LEE"))
            {
                pnlErrorMsg.Visible = true;
                ErrorMsgLabel.Text = "Please input your first and last name in the comments below to confirm the order.";
            }
            else
            {
                // validate gift codes, loyalty points, and credit memos applied
                DoOtherPaymentsChecking();

                if (ThisCustomer.IsRegistered)
                {
                    //check the length of order notes
                    //should not exceed 500 characters including spaces
                    if (sOrderNotes.Length > DomainConstants.ORDER_NOTE_MAX_LENGTH)
                    {
                        sOrderNotes = sOrderNotes.Substring(0, DomainConstants.ORDER_NOTE_MAX_LENGTH);
                    }
                    DB.ExecuteSQL(
                        String.Format("UPDATE Customer SET Notes = {0} WHERE CustomerCode = {1}",
                        sOrderNotes.ToDbQuote(),
                        ThisCustomer.CustomerCode.ToDbQuote())
                    );
                    ThisCustomer.Notes = sOrderNotes;
                }

                string receiptCode = string.Empty;
                string status = string.Empty, multiorder = string.Empty;

                bool hasMultipleShippingMethod = cart.HasMultipleShippingMethod();
                bool hasMultipleShippingAddress = cart.HasMultipleShippingAddresses();
                if (hasMultipleShippingAddress || cart.HasRegistryItems() || hasMultipleShippingMethod)	// Paypal will never hit this
                {
                    List<InterpriseShoppingCart> splittedCarts = null;
                    bool gatewayAuthFailed = false;

                    if (hasMultipleShippingAddress || cart.HasRegistryItems())
                    {
                        splittedCarts = cart.SplitIntoMultipleOrdersByDifferentShipToAddresses();

                    }
                    else if (hasMultipleShippingMethod)
                    {
                        splittedCarts = _shoppingCartService.SplitShippingMethodsInMultipleOrders().ToList();
                    }

                    for (int ctr = 0; ctr < splittedCarts.Count; ctr++)
                    {
                        var splitCart = splittedCarts[ctr];
                        try
                        {
                            splitCart.BuildSalesOrderDetails();
                        }
                        catch (InvalidOperationException ex)
                        {
                            if (ex.Message == AppLogic.GetString("shoppingcart.cs.35"))
                            {
                                Response.Redirect("shoppingcart.aspx?resetlinkback=1&discountvalid=false");
                            }
                            else { throw ex; }
                        }
                        catch (Exception ex) { throw ex; }

                        var currentItem = splitCart.FirstItem();

                        var shippingAddress = !currentItem.GiftRegistryID.HasValue ? Address.Get(ThisCustomer, AddressTypes.Shipping, currentItem.m_ShippingAddressID) : ThisCustomer.PrimaryShippingAddress;

                        splitCart.ClearAlreadyAppliedOtherPayment = (ctr == splittedCarts.Count - 1); // clear other payment if last cart to be placed order

                        string processedSalesOrderCode = String.Empty;
                        string processedReceiptCode = String.Empty;
                        // NOTE:
                        //  3DSecure using Sagepay Gateway is not supported on multiple shipping orders
                        //  We will revert to the regular IS gateway defined on the WebStore
                        status = splitCart.PlaceOrder(null,
                                    ThisCustomer.PrimaryBillingAddress,
                                    shippingAddress,
                                    ref processedSalesOrderCode,
                                    ref processedReceiptCode,
                                    false,
                                    true,
                                    false);

                        OrderNumber = processedSalesOrderCode;
                        receiptCode = processedReceiptCode;

                        if (status == AppLogic.ro_INTERPRISE_GATEWAY_AUTHORIZATION_FAILED)
                        {
                            gatewayAuthFailed = true;

                            if (ctr == 0)
                            {
                                ThisCustomer.IncrementFailedTransactionCount();
                                if (ThisCustomer.FailedTransactionCount >= AppLogic.AppConfigUSInt("MaxFailedTransactionCount"))
                                {
                                    cart.ClearTransaction();
                                    ThisCustomer.ResetFailedTransactionCount();

                                    _navigationService.NavigateToOrderFailed();
                                }

                                ThisCustomer.ClearTransactions(false);

                                if (cart.HasRegistryItems())
                                {
                                    _navigationService.NavigateToCheckOutPayment();
                                }

                                if (cart.HasOverSizedItemWithPickupShippingMethod() || cart.HasPickupItem())
                                {
                                    _navigationService.NavigateToCheckOutPayment();
                                }

                                if (_appConfigService.CheckoutUseOnePageCheckout)
                                {
                                    Response.Redirect("portalcheckoutreview.aspx?paymentterm=" + ThisCustomer.PaymentTermCode + "&errormsg=" + Server.UrlEncode(status));
                                }
                                else
                                {
                                    Response.Redirect("portalcheckoutreview.aspx?paymentterm=" + ThisCustomer.PaymentTermCode + "&errormsg=" + Server.UrlEncode(status));
                                }
                            }
                        }

                        // NOTE :
                        //  Should handle cases when 1 or more orders failed the payment processor 
                        //  if using a payment gateway on credit card
                        multiorder = multiorder + "," + OrderNumber;

                        if (!gatewayAuthFailed)
                        {
                            if (splitCart.HasRegistryItems())
                            {
                                DoRegistryQuantityDeduction(splitCart.CartItems);
                            }
                        }

                    }

                    if (multiorder != string.Empty) OrderNumber = multiorder.Remove(0, 1);

                    if (!gatewayAuthFailed)
                    {
                        cart.ClearTransaction();
                    }
                }
                else
                {
                    var billingAddress = ThisCustomer.PrimaryBillingAddress;
                    Address shippingAddress = null;

                    //added for PayPal ADDRESSOVERRIDE  
                    if (IsPayPalCheckout && !AppLogic.AppConfigBool("PayPalCheckout.OverrideAddress"))
                    {
                        if (!cart.HasShippableComponents())
                        {
                            shippingAddress = ThisCustomer.PrimaryShippingAddress;
                        }
                        else
                        {
                            pp = new PayPalExpress();
                            var GetPayPalDetails = pp.GetExpressCheckoutDetails(Request.QueryString["token"]).GetExpressCheckoutDetailsResponseDetails;
                            shippingAddress = new Address()
                            {
                                Name = GetPayPalDetails.PayerInfo.Address.Name,
                                Address1 = GetPayPalDetails.PayerInfo.Address.Street1 + (GetPayPalDetails.PayerInfo.Address.Street2 != String.Empty ? Environment.NewLine : String.Empty) + GetPayPalDetails.PayerInfo.Address.Street2,
                                City = GetPayPalDetails.PayerInfo.Address.CityName,
                                State = GetPayPalDetails.PayerInfo.Address.StateOrProvince,
                                PostalCode = GetPayPalDetails.PayerInfo.Address.PostalCode,
                                Country = AppLogic.ResolvePayPalAddressCode(GetPayPalDetails.PayerInfo.Address.CountryName.ToString()),
                                CountryISOCode = AppLogic.ResolvePayPalAddressCode(GetPayPalDetails.PayerInfo.Address.Country.ToString()),
                                Phone = GetPayPalDetails.PayerInfo.ContactPhone ?? ThisCustomer.PrimaryShippingAddress.Phone
                            };
                        }
                    }
                    else
                    {
                        // Handle the scenario wherein the items in the cart
                        // does not ship to the customer's primary shipping address
                        if (cart.OnlyShippingAddressIsNotCustomerDefault())
                        {
                            var item = cart.FirstItem();
                            shippingAddress = Address.Get(ThisCustomer, AddressTypes.Shipping, item.m_ShippingAddressID, item.GiftRegistryID);
                        }
                        else
                        {
                            shippingAddress = ThisCustomer.PrimaryShippingAddress;
                        }
                    }

                    if (!cart.IsSalesOrderDetailBuilt)
                    {
                        cart.BuildSalesOrderDetails();
                    }

                    Gateway gatewayToUse = null;

                    try
                    {
                        if (IsPayPalCheckout)
                        {
                            //Insert PayPal call here for response - For authorize and capture of order from paypal inside IS
                            pp = new PayPalExpress();

                            var paypalDetails = pp.GetExpressCheckoutDetails(Request.QueryString["token"]).GetExpressCheckoutDetailsResponseDetails;
                            var doExpressCheckoutResp = pp.DoExpressCheckoutPayment(paypalDetails.Token, paypalDetails.PayerInfo.PayerID, OrderNumber, cart);
                            string result = String.Empty;
                            if (doExpressCheckoutResp.Errors != null && !doExpressCheckoutResp.Errors[0].ErrorCode.IsNullOrEmptyTrimmed())
                            {
                                if (AppLogic.AppConfigBool("ShowGatewayError"))
                                {
                                    result = String.Format(AppLogic.GetString("shoppingcart.aspx.27"), doExpressCheckoutResp.Errors[0].ErrorCode, doExpressCheckoutResp.Errors[0].LongMessage);
                                }
                                else
                                {
                                    result = AppLogic.GetString("shoppingcart.aspx.28");
                                }

                                _navigationService.NavigateToShoppingCartWitErroMessage(result.ToUrlEncode());
                                return;
                            }
                            else
                            {
                                var payPalResp = new GatewayResponse(String.Empty)
                                {
                                    AuthorizationCode = doExpressCheckoutResp.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].TransactionID,
                                    TransactionResponse = doExpressCheckoutResp.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].PaymentStatus.ToString(),
                                    Details = doExpressCheckoutResp.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].PaymentStatus.ToString(),
                                    AuthorizationTransID = doExpressCheckoutResp.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].TransactionID
                                };

                                status = cart.PlaceOrder(gatewayToUse, billingAddress, shippingAddress, ref OrderNumber, ref receiptCode, true, true, payPalResp, IsPayPalCheckout, false);

                                ThisCustomer.ThisCustomerSession["paypalFrom"] = String.Empty;
                                ThisCustomer.ThisCustomerSession["notesFromPayPal"] = String.Empty;
                                ThisCustomer.ThisCustomerSession["anonymousCustomerNote"] = String.Empty;
                            }
                        }
                        else
                        {
                            status = cart.PlaceOrder(gatewayToUse, billingAddress, shippingAddress, ref OrderNumber, ref receiptCode, true, true, null, !IsPayPalCheckout, false);
                        }
                    }
                    catch (Exception ex)
                    {
                        ThisCustomer.ThisCustomerSession["paypalFrom"] = String.Empty;
                        ThisCustomer.ThisCustomerSession["notesFromPayPal"] = String.Empty;
                        ThisCustomer.ThisCustomerSession["anonymousCustomerNote"] = String.Empty;

                        if (ex.Message == "Unable to instantiate Default Credit Card Gateway")
                        {
                            cart.ClearLineItems();
                            Response.Redirect("pageError.aspx?Parameter=" + "An Error Occured while Authorizing your Credit Card, However your order has been Placed.");
                        }

                        Response.Redirect("pageError.aspx?Parameter=" + Server.UrlEncode(ex.Message));
                    }

                    if (status == AppLogic.ro_3DSecure)
                    {
                        _navigationService.NavigateToSecureForm();
                    }
                    if (status != AppLogic.ro_OK)
                    {
                        ThisCustomer.IncrementFailedTransactionCount();
                        if (ThisCustomer.FailedTransactionCount >= AppLogic.AppConfigUSInt("MaxFailedTransactionCount"))
                        {
                            cart.ClearTransaction();
                            ThisCustomer.ResetFailedTransactionCount();

                            _navigationService.NavigateToOrderFailed();
                        }

                        ThisCustomer.ClearTransactions(false);

                        if (cart.HasRegistryItems())
                        {
                            _navigationService.NavigateToCheckOutPayment();
                        }

                        if (cart.HasOverSizedItemWithPickupShippingMethod() || cart.HasPickupItem())
                        {
                            _navigationService.NavigateToCheckOutPayment();
                        }

                        if (_appConfigService.CheckoutUseOnePageCheckout)
                        {
                            Response.Redirect("checkout1.aspx?paymentterm=" + ThisCustomer.PaymentTermCode + "&errormsg=" + Server.UrlEncode(status));
                        }
                        else
                        {
                            Response.Redirect("checkoutpayment.aspx?paymentterm=" + ThisCustomer.PaymentTermCode + "&errormsg=" + Server.UrlEncode(status));
                        }
                    }

                }

                AppLogic.ClearCardNumberInSession(ThisCustomer);
                ThisCustomer.ClearTransactions(true);

                string PM = AppLogic.CleanPaymentMethod(ThisCustomer.PaymentMethod);
                bool multipleAttachment = false;
                if (OrderNumber.IndexOf(',') != -1)
                {
                    multipleAttachment = true;
                }

                foreach (string salesOrderToEmail in OrderNumber.Split(','))
                {
                    if (ThisCustomer.PaymentTermCode.ToUpper() != "REQUEST QUOTE" && ThisCustomer.PaymentTermCode.ToUpper() != "PURCHASE ORDER")
                    {
                        AppLogic.SendCompanyOrderEMail(ThisCustomer, cart, salesOrderToEmail, false, PM, true, multipleAttachment);
                    }
                    else
                    {
                        AppLogic.SendCompanyOrderEMail(ThisCustomer, cart, salesOrderToEmail, false, PM, multipleAttachment);
                    }
                }
                AppLogic.CreateCompanyCSV(ThisCustomer, cart, OrderNumber);
                Response.Redirect("portalorderconfirmation.aspx?ordernumber={0}".FormatWith(OrderNumber.ToUrlEncode()));
            }
        }

        private void DoRegistryQuantityDeduction(CartItemCollection cartItems)
        {
            foreach (var cartItem in cartItems)
            {
                if (!cartItem.GiftRegistryID.HasValue) continue;

                decimal quatityToremove = cartItem.m_Quantity;

                //to avoid negative value upon deduction
                decimal? trueQuantity = cartItem.RegistryItemQuantity;
                if (trueQuantity < cartItem.m_Quantity) { quatityToremove = trueQuantity.Value; }

                GiftRegistryDA.DeductGiftRegistryItemQuantity(cartItem.GiftRegistryID.Value, cartItem.RegistryItemCode.Value, quatityToremove, cartItem.m_Quantity);
            }
        }

        private void DoOtherPaymentsChecking()
        {
            _shoppingCartService.DoHasAppliedInvalidGiftCodesChecking(cart);
            _shoppingCartService.DoHasAppliedInvalidLoyaltyPointsChecking(cart);
            _shoppingCartService.DoHasAppliedInvalidCreditMemosChecking(cart);

            DoGiftCodeOwnershipChecking();
        }

        private void DoGiftCodeOwnershipChecking()
        {
            var giftCodes = _shoppingCartService.GetAppliedGiftCodes(false)
                                                .ToList();
            if (giftCodes.Count == 0) return;

            var customer = ServiceFactory.GetInstance<IAuthenticationService>()
                                         .GetCurrentLoggedInCustomer();

            bool hasInvalidGiftCode = giftCodes.Where(x => x.IsActivated && !x.BillToCode.IsNullOrEmptyTrimmed())
                                               .Any(x => x.BillToCode != customer.CustomerCode);
            if (hasInvalidGiftCode)
            {
                _shoppingCartService.ClearAppliedGiftCodes();
                string message = AppLogic.GetString("checkoutreview.aspx.17");
                if (_appConfigService.CheckoutUseOnePageCheckout)
                {
                    _navigationService.NavigateToCheckout1WithErrorMessage(message);
                }
                else
                {
                    _navigationService.NavigateToCheckOutPaymentWithErrorMessage(message);
                }
            }
        }

        private bool SetShippingMethod()
        {
            bool output = false;
            var availableShippingMethods = cart.GetShippingMethods(ThisCustomer.PrimaryShippingAddress);
            //Check if cart contains non-stock items only then return true
            if (availableShippingMethods.Count <= 0 && cart.CartItems.Where(item => item.IsStock).Count() <= 0 && cart.CartItems.Where(item => item.IsService).Count() >= 1)
            {
                output = true;
            }
            var realTimeRateGUID = Guid.NewGuid();
            for (int ctr = 0; ctr < availableShippingMethods.Count; ctr++)
            {
                if (availableShippingMethods[ctr].Code == "Invoice")
                {
                    if (availableShippingMethods[ctr].FreightCalculation == 1 || availableShippingMethods[ctr].FreightCalculation == 2)
                    {
                        cart.SetCartShippingMethod(availableShippingMethods[ctr].Code, String.Empty, realTimeRateGUID);
                        ServiceFactory.GetInstance<IShippingService>()
                                      .SetRealTimeRateRecord(availableShippingMethods[ctr].Code, availableShippingMethods[ctr].Freight.ToString(), realTimeRateGUID.ToString(), false);
                        cart.FirstItem().RealTimeRateId = realTimeRateGUID;
                        output = true;
                    }
                    else
                    {
                        cart.SetCartShippingMethod(availableShippingMethods[ctr].Code);
                        output = true;
                    }
                    break;
                }
            }
            return output;
        }

        private bool SetPaymentTerm()
        {
            bool output = false;
            IEnumerable<PaymentTermDTO> paymentTermOptions = PaymentTermDTO.GetOnlinePaymentTerms(ThisCustomer.ContactCode, ThisCustomer.PrimaryShippingAddress);
            foreach (var paymentterm in paymentTermOptions)
            {
                if (paymentterm.PaymentTermCode == "Invoice")
                {
                    InterpriseHelper.UpdateCustomerPaymentTerm(ThisCustomer, paymentterm.PaymentTermCode);
                    output = true;
                    break;
                }
            }
            //var paymentterm = paymentTermOptions.First(x => x.PaymentTermCode == "Invoice");
            //InterpriseHelper.UpdateCustomerPaymentTerm(ThisCustomer, paymentterm.PaymentTermCode);
            return output;
        }

        /// <summary>
        /// Manually add shipping item to cart
        /// </summary>
        private void AddShippingItem()
        {
            if (ThisCustomer.CostCenter != "Harvey Norman Cost Center" && ThisCustomer.CostCenter != "Harvey Norman Cost Centre")
            {
                return;
            }
            string itemCode = string.Empty, deliveryitem = AppLogic.AppConfig("custom.hn.delivery.item"),
                palletFeeCartonsItem = AppLogic.AppConfig("custom.hn.pallet.fee.cartons.item"), palletFeeSatchelItem = AppLogic.AppConfig("custom.hn.pallet.fee.satchel.item");
            int counter = 0;
            bool exists = false;
            //Check if there is/are items that are not required for pick&pack. If yes then add delivery item
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT 1 FROM EcommerceShoppingCart A INNER JOIN InventoryItemWebOption B ON A.ItemCode = B.ItemCode INNER JOIN InventoryItem C ON C.ItemCode = A.ItemCode WHERE ContactCode = {0} AND ISNULL(B.PalletFeeCartons_C, 0) = 0  AND ISNULL(B.PalletFeeSatchel_C, 0) = 0 AND Itemname NOT IN ({1},{2},{3})", DB.SQuote(ThisCustomer.ContactCode), DB.SQuote(palletFeeCartonsItem), DB.SQuote(palletFeeSatchelItem), DB.SQuote(deliveryitem)))
                {
                    if (reader.Read())
                    {
                        exists = true;
                    }
                }
            }
            if (exists)
            {
                exists = false;
                using (var con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (var reader = DB.GetRSFormat(con, "SELECT Counter, ItemCode FROM InventoryItem with (NOLOCK) WHERE ItemName = {0}", DB.SQuote(deliveryitem)))
                    {
                        if (reader.Read())
                        {
                            itemCode = reader.ToRSField("ItemCode");
                            counter = reader.ToRSFieldInt("Counter");
                        }
                    }
                }

                foreach (CartItem cartItem in cart.CartItems)
                {
                    if (cartItem.ItemCode == itemCode)
                    {
                        exists = true;
                        break;
                    }
                }

                if (!exists)
                {
                    UnitMeasureInfo uom = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);
                    cart.AddItem(ThisCustomer, ThisCustomer.PrimaryShippingAddressID, itemCode, counter, 1, uom.Code, CartTypeEnum.ShoppingCart);
                    cart = new InterpriseShoppingCart(base.EntityHelpers, SkinID, ThisCustomer, CartTypeEnum.ShoppingCart, String.Empty, false, true);
                }
            }
        }

        private void ValidateShippingItem()
        {
            if (ThisCustomer.CostCenter != "Harvey Norman Cost Center" && ThisCustomer.CostCenter != "Harvey Norman Cost Centre")
            {
                return;
            }
            string deliveryItem = AppLogic.AppConfig("custom.hn.delivery.item"), palletFeeCartonsItem = AppLogic.AppConfig("custom.hn.pallet.fee.cartons.item"), palletFeeSatchelItem = AppLogic.AppConfig("custom.hn.pallet.fee.satchel.item");
            bool exists = false;
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT 1 FROM EcommerceShoppingCart A INNER JOIN InventoryItemWebOption B ON A.ItemCode = B.ItemCode INNER JOIN InventoryItem C ON C.ItemCode = A.ItemCode " +
                    "WHERE ContactCode = {0} AND Itemname NOT IN ({1},{2},{3})", DB.SQuote(ThisCustomer.ContactCode), DB.SQuote(palletFeeCartonsItem), DB.SQuote(palletFeeSatchelItem), DB.SQuote(deliveryItem)))
                {
                    if (reader.Read())
                    {
                        exists = true;
                    }
                }
            }
            if (!exists)
            {
                foreach (CartItem cartItem in cart.CartItems)
                {
                    if (cartItem.ItemName == deliveryItem)
                    {
                        cart.RemoveItem(cartItem.m_ShoppingCartRecordID);
                        break;
                    }
                }
            }
        }

        private void AddPalletFeeCartoonsItem()
        {
            string itemCode = string.Empty, palletFeeCartonsItem = AppLogic.AppConfig("custom.hn.pallet.fee.cartons.item");
            int counter = 0;
            bool addItemPalleteFeeCartoons = false;

            //Check if Pick & Pack item is needed to be inserted
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT 1 FROM EcommerceShoppingCart A INNER JOIN InventoryItemWebOption B ON A.ItemCode = B.ItemCode WHERE ContactCode = {0} AND ISNULL(B.PalletFeeCartons_C, 0) = 1", DB.SQuote(ThisCustomer.ContactCode)))
                {
                    if (reader.Read())
                    {
                        addItemPalleteFeeCartoons = true;
                    }
                }
            }

            if (addItemPalleteFeeCartoons)
            {
                using (var con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (var reader = DB.GetRSFormat(con, "SELECT Counter, ItemCode FROM InventoryItem with (NOLOCK) WHERE ItemName = {0}", DB.SQuote(palletFeeCartonsItem)))
                    {
                        if (reader.Read())
                        {
                            itemCode = reader.ToRSField("ItemCode");
                            counter = reader.ToRSFieldInt("Counter");
                        }
                    }
                }
                bool exists = false;
                foreach (CartItem cartItem in cart.CartItems)
                {
                    if (cartItem.ItemCode == itemCode)
                    {
                        exists = true;
                        break;
                    }
                }

                if (!exists)
                {
                    UnitMeasureInfo uom = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);
                    cart.AddItem(ThisCustomer, ThisCustomer.PrimaryShippingAddressID, itemCode, counter, 1, uom.Code, CartTypeEnum.ShoppingCart);
                    cart = new InterpriseShoppingCart(base.EntityHelpers, SkinID, ThisCustomer, CartTypeEnum.ShoppingCart, String.Empty, false, true);
                }
            }
            else
            {
                foreach (CartItem cartItem in cart.CartItems)
                {
                    if (cartItem.ItemName == palletFeeCartonsItem)
                    {
                        cart.RemoveItem(cartItem.m_ShoppingCartRecordID);
                        break;
                    }
                }
            }
        }

        private void AddPalletFeeSatchelItem()
        {
            string itemCode = string.Empty, palletFeeSatchelItem = AppLogic.AppConfig("custom.hn.pallet.fee.satchel.item");
            int counter = 0;
            bool addItemPalleteFeeSatchel = false;

            //Check if Pick & Pack item is needed to be inserted
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT 1 FROM EcommerceShoppingCart A INNER JOIN InventoryItemWebOption B ON A.ItemCode = B.ItemCode WHERE ContactCode = {0} AND ISNULL(B.PalletFeeSatchel_C, 0) = 1", DB.SQuote(ThisCustomer.ContactCode)))
                {
                    if (reader.Read())
                    {
                        addItemPalleteFeeSatchel = true;
                    }
                }
            }

            if (addItemPalleteFeeSatchel)
            {
                using (var con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (var reader = DB.GetRSFormat(con, "SELECT Counter, ItemCode FROM InventoryItem with (NOLOCK) WHERE ItemName = {0}", DB.SQuote(palletFeeSatchelItem)))
                    {
                        if (reader.Read())
                        {
                            itemCode = reader.ToRSField("ItemCode");
                            counter = reader.ToRSFieldInt("Counter");
                        }
                    }
                }
                bool exists = false;
                foreach (CartItem cartItem in cart.CartItems)
                {
                    if (cartItem.ItemCode == itemCode)
                    {
                        exists = true;
                        break;
                    }
                }

                if (!exists)
                {
                    UnitMeasureInfo uom = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);
                    cart.AddItem(ThisCustomer, ThisCustomer.PrimaryShippingAddressID, itemCode, counter, 1, uom.Code, CartTypeEnum.ShoppingCart);
                    cart = new InterpriseShoppingCart(base.EntityHelpers, SkinID, ThisCustomer, CartTypeEnum.ShoppingCart, String.Empty, false, true);
                }
            }
            else
            {
                foreach (CartItem cartItem in cart.CartItems)
                {
                    if (cartItem.ItemName == palletFeeSatchelItem)
                    {
                        cart.RemoveItem(cartItem.m_ShoppingCartRecordID);
                        break;
                    }
                }
            }
        }

        private string GetShipToList(Address shippingAddress)
        {
            bool multiShipping = false;
            StringBuilder output = new StringBuilder();
            output.Append("<div style=\"text-align:right;padding-bottom:5px;\">");
            output.Append("<span style=\"right:16px;\">");
            output.Append("<select id=\"drpShipToList\" name=\"drpShipToList\" class=\"textbox-border\" onChange=\"ShipToList_OnChange();\">");
            string sql = string.Format("SELECT B.ShipToCode, B.ShipToName FROM CRMContactShipTo_C A INNER JOIN CustomerShipTo B ON A.ShipToCode = B.ShipToCode WHERE A.ContactCode = {0} AND B.CustomerCode = {1} ORDER BY B.ShipToName", DB.SQuote(ThisCustomer.ContactCode), DB.SQuote(ThisCustomer.CustomerCode));
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var rs = DB.GetRSFormat(con, sql))
                {
                    while (rs.Read())
                    {
                        if (rs.ToRSField("ShipToCode") == ThisCustomer.PrimaryShippingAddressID)
                        {
                            output.AppendFormat("<option value={0} selected>{1}</option>", rs.ToRSField("ShipToCode"), rs.ToRSField("ShipToName"));
                        }
                        else
                        {
                            output.AppendFormat("<option value={0}>{1}</option>", rs.ToRSField("ShipToCode"), rs.ToRSField("ShipToName"));
                        }
                        multiShipping = true;
                    }
                }
            }
            output.Append("</select>");
            output.Append("</span>");
            output.Append("</div>");
            output.Append("<div class=\"clear\"></div>");
            output.AppendLine("<div id=\"shiptolist\">");
            if (!multiShipping)
            {
                output.Clear();
                output.AppendLine(shippingAddress.DisplayString(true, true, true, false, "<div class='height-5'></div>"));
            }
            else
            {
                output.AppendLine(shippingAddress.DisplayString(true, true, false, false, "<div class='height-5'></div>"));
                
            }
            output.AppendLine("</div>");
            return output.ToString();
        }
        #endregion
    }
}