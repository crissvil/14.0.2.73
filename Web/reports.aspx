﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="reports.aspx.cs" Inherits="InterpriseSuiteEcommerce.reports" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SOH Report</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.min.css" />
    <link rel="stylesheet" href="skins/skin_99/bootstrap/css/bootstrap.min.css" media="screen" />
    <link rel="stylesheet" href="skins/skin_99/style.css" type="text/css" />
    <link rel="stylesheet" href="skins/skin_99/custom.css" type="text/css" />
    <link rel="stylesheet" href="skins/skin_99/portal.css" type="text/css" />
    <script src="//code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
    <script src="https://use.fontawesome.com/55068458dc.js"></script>
    <%--<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>--%>
    <script>
        $(function () {
            $("#txtDateFrom").datepicker({dateFormat:'dd/mm/yy'});
            $("#txtDateTo").datepicker({ dateFormat: 'dd/mm/yy' });
            $("#txtAsOf").datepicker({ dateFormat: 'dd/mm/yy' });
        });
    </script>

    <style>
        body {
            font-family: Arial, Roboto;
            font-weight: 400;
            font-size: 14px;
        }
    </style>
</head>
<body style="overflow: scroll !important;">
    <asp:Panel ID="pnlContent" runat="server">
        <div style="padding: 10px;">
            <form id="frmReports" runat="server">
                <div>
                    <div class="filter-date">
                        <div style="float: left;">
                            <a href="portal.aspx" class="site-button home-button"><i class="fa fa-home home"></i><span>Home</span></a>
                        </div>
                        <div style="float: left;border-left: 1px solid #000;margin: 0 10px;height: 89%;">
                        </div>
                        <div style="float: left;">
                            <select id="drpSave" name="drpSave" class="textbox report-date-option">
                                <option value="0" selected>csv</option>
                                <option value="1">excel</option>
                                <option value="3">pdf</option>
                            </select>
                        </div>
                        <div style="float: left;">
                            <a href="#" onclick="return DownloadReport()" class="save-icon"><i class="fa fa-floppy-o" aria-hidden="true"></i></a>
                        </div>
                        <div style="float: left;border-left: 1px solid #000;margin: 0 10px 0 5px;height: 89%;">
                        </div>
                        <div style="float: left;">
                            <select id="drpDate" name="drpDate" onchange="drpDate_OnChange();" class="textbox report-date-option">
                                <option value="1">Filter Date:</option>
                            </select>
                        </div>
                        <div style="float: left; position: relative; padding-left: 10px;" id="divDateFrom">
                            <i class="fa fa-calendar calendar" aria-hidden="true"></i>
                            <span style="padding-right: 10px;">From</span>
                        <input type="text" id="txtDateFrom" name="txtDateFrom" class="textbox report-date"/>
                        </div>
                        <div style="float: left; position: relative; padding-left: 10px;" id="divDateTo">
                            <i class="fa fa-calendar calendar" aria-hidden="true"></i>
                            <span style="padding-right: 10px;">to</span>
                        <input type="text" id="txtDateTo" name="txtDateTo" class="textbox report-date" />
                        </div>
                        <div style="float: left; position: relative; padding-left: 10px;" id="divAsOf" class="hide">
                            <i class="fa fa-calendar calendar" aria-hidden="true" style="right: 0; position: absolute; pointer-events: none; padding: 5px;"></i>
                            <input type="text" id="txtAsOf" name="txtAsOf" class="textbox report-date" />
                        </div>
                        <div style="float: left; padding: 0 0 0 7px;">
                            <button type="submit" name="btnGo" id="btnGo" class="site-button report-go-button" onclick="return setValue();">Go</button>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
                <div>
                    <asp:Literal ID="ltPageContent" runat="server"></asp:Literal>
                </div>
                <div style="padding-left: 10px; font-size: 18px;">
                    <div id="modalpopup">
                        <div id="modalpopup-content">
                            <i class="fa fa-spinner fa-pulse" aria-hidden="true"></i>
                            <div id="modalpopup-text">Loading...</div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </asp:Panel>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#txtDateFrom").val(localStorage.getItem("txtDateFrom"));
            $("#txtDateTo").val(localStorage.getItem("txtDateTo"));
            $("#txtAsOf").val(localStorage.getItem("txtAsOf"));
            if (localStorage.getItem("drpDate") == null) {
                var date = new Date();
                date = new Intl.DateTimeFormat('en-AU').format(date);
                $("#txtDateTo").val(date);
                $("#txtDateFrom").val("01/07/2017");
                localStorage.setItem("drpDate", $("#drpDate").val());
            }
            else
            {
                $("#drpDate").val(localStorage.getItem("drpDate"));
            }
            if (localStorage.getItem("drpDate") == 0) {
                $("#divDateFrom").addClass("hide");
                $("#divDateTo").addClass("hide");
                $("#divAsOf").removeClass("hide");
            }
            localStorage.removeItem("txtDateFrom");
            localStorage.removeItem("txtDateTo");
            localStorage.removeItem("txtAsOf");
            localStorage.removeItem("drpDate");
        });

        function ShowModalPopup(message) {
            var height = $(window).height();
            var width = $(window).width();
            var popup = $("#modalpopup-content");
            var posTop = (height / 2 - popup.height() / 2) - 70;
            var posLeft = (width / 2 - popup.width() / 2) - 150;

            $("#modalpopup-content").css({
                "margin-top": posTop,
                "margin-left": posLeft
            });

            $("#modalpopup-text").html(message);
            $("#modalpopup").css({ "display": "block" });
        }

        function CloseModalPopup() {
            $("#modalpopup").css({ "display": "none" });
        }

        function setValue() {
            if ($("#drpDate").val() == "0") {
                localStorage.removeItem("txtDateFrom");
                localStorage.removeItem("txtDateTo");
                localStorage.setItem("txtAsOf", $("#txtAsOf").val());
            }
            else if ($("#drpDate").val() == "1") {
                localStorage.removeItem("txtAsOf");
                localStorage.setItem("txtDateFrom", $("#txtDateFrom").val());
                localStorage.setItem("txtDateTo", $("#txtDateTo").val());
            }
            localStorage.setItem("drpDate", $("#drpDate").val());
            ShowModalPopup("Processing...");
            return true;
        }

        function drpDate_OnChange() {
            if ($("#drpDate").val() == "0") {
                $("#divDateFrom").addClass("hide");
                $("#divDateTo").addClass("hide");
                $("#divAsOf").removeClass("hide");
            }
            else if ($("#drpDate").val() == "1") {
                $("#divAsOf").addClass("hide");
                $("#divDateFrom").removeClass("hide");
                $("#divDateTo").removeClass("hide");
            }
        }

        function DownloadReport() {
            ShowModalPopup("Processing...");
            var param = new Object();
            param.Type = $("#drpSave").val();
            param.StartDate = $("#txtDateFrom").val();
            param.EndDate = $("#txtDateTo").val();
            param.CostCenter = "";
            $.ajax({
                type: "POST",
                url: "ActionService.asmx/DownloadReport",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    window.open(result.d);
                    CloseModalPopup();
                },
                fail: function (result) {
                    CloseModalPopup();
                    ShowModalPopup("A problem was encountered creating the report.");
                }
            });
            //CloseModalPopup();
            return false;
        }
    </script>
</body>
</html>
