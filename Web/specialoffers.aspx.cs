// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using InterpriseSuiteEcommerceCommon;
using System.Web.UI;

namespace InterpriseSuiteEcommerce
{
	/// <summary>
	/// Summary description for recentadditions.
	/// </summary>
    public partial class specialoffers : SkinBase
	{
		protected void Page_Load(object sender, System.EventArgs e)
		{
            if (AppLogic.AppConfigBool("GoNonSecureAgain"))
            {
                SkinBase.GoNonSecureAgain();
            }

            // this may be overwridden by the XmlPackage below!
            SectionTitle = AppLogic.GetString("recentadditions.aspx.1", true);

            // set the Customer context, and set the SkinBase context, so meta tags to be set if they are not blank in the XmlPackage results
            XmlPackage1.SetContext = this;
		}
        protected override void RegisterScriptsAndServices(System.Web.UI.ScriptManager manager)
        {
            manager.Services.Add(new ServiceReference("~/actionservice.asmx"));
            manager.CompositeScript.Scripts.Add(new ScriptReference("jscripts/product_ajax.js"));
            manager.CompositeScript.Scripts.Add(new ScriptReference("jscripts/kitProduct_ajax.js"));
            manager.CompositeScript.Scripts.Add(new ScriptReference("jscripts/productcompare.js"));
            manager.LoadScriptsBeforeUI = false;

        }
	}
}
