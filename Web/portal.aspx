﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="portal.aspx.cs" Inherits="InterpriseSuiteEcommerce.portal" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Portal</title>
</head>
<body>
    <form id="frmPortal" runat="server">
        <ise:Topic runat="server" ID="HeaderMsg" TopicName="Portal Homepage"  />
        <div style="float:right;width:35%;">
            <asp:Literal ID="litPromo" runat="server" />
        </div>
    </form>
</body>
</html>
