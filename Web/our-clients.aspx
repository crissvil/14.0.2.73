﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="our-clients.aspx.cs" Inherits="InterpriseSuiteEcommerce.our_clients" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls.Validators" TagPrefix="ise" %>
<%@ Register Assembly="InterpriseSuiteEcommerceControls" Namespace="InterpriseSuiteEcommerceControls" TagPrefix="ise" %>
<%@ Register TagPrefix="ise" TagName="Topic" Src="TopicControl.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server"><title>Some Of Our Clients</title></head>
<body>
<div class="clear-both height-5">&nbsp;</div>
<form id="frmOurClients" runat="server" action=>
    <asp:Panel ID="pnlPageContentWrapper" runat="server">
        <h1>Some Of Our Clients</h1>
		
		<table id="clientTable" style="width: 100%;">
            <tbody>
                <tr>
                    <td>
                    <div style="text-align: center;"><img alt="Snap On" title="Snap On" style="border: 0pt none;" src="images/clients-logo/SnapOnLogo.gif"></div>
                    </td>
                    <td>
                    <div style="text-align: center;"><img alt="City Beach" title="City Beach" style="border: 0pt none;" src="images/clients-logo/CityBeachLogo.gif"></div>
                    </td>
                    <td>
                    <div style="text-align: center;"><img alt="Caltex" title="Caltex" style="border: 0pt none;" src="images/clients-logo/CaltexLogo.gif"></div>
                    </td>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/DomayneLogo.gif" alt="Domayne" title="Domayne"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/HarveyNormanLogo.gif" alt="Harvey Norman" title="Harvey Norman"></div>
                    </td>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/BoseLogo.gif" alt="The Bose Store" title="The Bose Store"></div>
                    </td>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/KrispyKremeLogo.gif" alt="Krispy Kreme Donuts" title="Krispy Kreme Donuts"></div>
                    </td>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/FujiXeroxLogo.gif" alt="Fuji Xerox" title="Fuji Xerox"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/BPLogo.gif" alt="BP" title="BP"></div>
                    </td>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/ThalgoLogo.gif" alt="Thalgo" title="Thalgo"></div>
                    </td>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/MazdaLogo.gif" alt="Mazda" title="Mazda"></div>
                    </td>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/HondaCareLogo.gif" alt="Honda Dedicated Care" title="Honda Dedicated Care"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/ToyotaGenuineLogo.gif" alt="Toyota Genuine Care" title="Toyota Genuine Care"></div>
                    </td>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/SuzanneGraeLogo.gif" alt="Suzanne Grae" title="Suzanne Grae"></div>
                    </td>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/ToysRUsLogo.gif" alt="Toys R Us" title="Toys R Us"></div>
                    </td>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/SandPLogo.gif" alt="S And P" title="S and P"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                    <div style="text-align: center;"><img src="images/clients-logo/WurthLogo.gif" alt="Wurth" title="Wurth"></div>
                    </td>
                    <td style="text-align: center;"><img alt="Cellarbrations" src="images/clients-logo/clients_img01.gif" style="border: 0pt none;"></td>
                    <td style="text-align: center;"><img alt="Beaurepaires" src="images/clients-logo/clients_img02.gif" style="border: 0pt none;"></td>
                    <td style="text-align: center;"><img alt="CUE" src="..images/clients-logo/clients_img03.png" style="border: 0pt none;"></td>
                </tr>
                <tr>
                    <td style="text-align: center;"><img alt="SOLO" src="images/clients-logo/clients_img04.png" style="border: 0pt none;"></td>
                    <td style="text-align: center;"><img alt="ECOYA" src="images/clients-logo/clients_img05.png" style="border: 0pt none;"></td>
                    <td style="text-align: center;"><img alt="Air New Zealand" src="images/clients-logo/air-nz.png"></td>
                    <td style="text-align: center;"><img alt="BILLABONG" src="images/clients-logo/billabong.png"></td>
                </tr>
            </tbody>
        </table>
		
    </asp:Panel>
</form>
</body>
</html>