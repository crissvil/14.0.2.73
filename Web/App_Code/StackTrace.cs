﻿using System;
using System.Linq;
using Interprise.Facade.Customer;
using Interprise.Framework.Customer.DatasetGateway;
using InterpriseSuiteEcommerceCommon;
using System.Data;
using Interprise.Facade.Base;
using Interprise.Framework.Base.DatasetComponent;
using System.Data.SqlClient;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using System.Collections.Generic;

/// <summary>
/// Summary description for StackTrace
/// </summary>
public class BuildSalesOrderDetails
{
    private SalesOrderDatasetGateway _gatewaySalesOrderDataset;
    private SalesOrderFacade _facadeSalesOrder;
    private Customer ThisCustomer;
    private List<CustomCart> items;
    private CustomCart item;
    private Address shippingAddress;
    private Address billingAddress;
    public BuildSalesOrderDetails(Customer thisCustomer, Address billing, Address shipping,  List<CustomCart> cart)
	{
        ThisCustomer = thisCustomer;
        items = cart;
        item = cart[0];
        billingAddress = billing;
        shippingAddress = shipping;
        int retries = 3;
        for (int ctr = 0; ctr < retries; ctr++)
        {
            try
            {
                _gatewaySalesOrderDataset = new SalesOrderDatasetGateway();
                _facadeSalesOrder = new SalesOrderFacade(_gatewaySalesOrderDataset);
                BuildSalesOrderDetailsCore();
                break;
            }
            catch
            {
                if (_gatewaySalesOrderDataset != null) { _gatewaySalesOrderDataset.Dispose(); }
                    if (_facadeSalesOrder != null) { _facadeSalesOrder.Dispose(); }

                    _gatewaySalesOrderDataset = null;
                    _facadeSalesOrder = null;

                    // if this is the last retry, bubble up the error
                    if (ctr + 1 == retries)
                    {
                        throw;
                    }
            }
        }
	}

    private void BuildSalesOrderDetailsCore()
    {
        _facadeSalesOrder.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.Quote;
        var shipToCode = (ThisCustomer.IsNotRegistered) ? ThisCustomer.AnonymousShipToCode : ThisCustomer.PrimaryShippingAddressID;

        var shipToDataset = new DataSet();
        var facadeListControl = new ListControlFacade();
        var shipToBaseDataset = new BaseDataset();

        //deadlock
        int nooftries = 0;
        while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
        {
            try
            {
                shipToDataset = facadeListControl.ReadSearchResultsData("CustomerShipToView",
                                                                        string.Format("ShipToCode = {0}", DB.SQuote(shipToCode)),
                                                                        1,
                                                                        false,
                                                                        string.Empty,
                                                                        ref shipToBaseDataset, true);
                break;
            }
            catch (SqlException ex)
            {
                if (ex.ErrorCode == 1205)
                {
                    nooftries += 1;
                }
                else
                {
                    throw;
                }
            }
        }
        if (shipToBaseDataset.Tables["CustomerShipToView"].Rows.Count == 0) return;
        var rowShipTo = shipToBaseDataset.Tables["CustomerShipToView"].Rows[0];
        string contactCode = String.Empty;
        string contactFullName = String.Empty;
        // retrieve the contact info
        contactCode = ThisCustomer.ContactCode; 
        contactFullName = ThisCustomer.ContactFullName;

        /***************************************************************
        *  1. Make Sales Order
        * *************************************************************/
        string msg = String.Empty;

        if (_facadeSalesOrder.CreateSalesOrder(_facadeSalesOrder.TransactionType,
                                        rowShipTo,
                                        contactCode,
                                        contactFullName,
                                        null,
                                        ref msg,
                                        String.Empty,
                                        String.Empty, null, null, true, true, true))
        {
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].InternalNotes = item.Comments;
            if (ThisCustomer.IsNotRegistered)
            {
                if (!ThisCustomer.ThisCustomerSession["anonymousCompany"].IsNullOrEmptyTrimmed())
                {
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].InternalNotes += "\r\n" + "Anonymous Company Name: " + ThisCustomer.ThisCustomerSession["anonymousCompany"];
                }
            }
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].WebSiteCode = InterpriseHelper.ConfigInstance.WebSiteCode;
            
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToName = billingAddress.Name;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToAddress = billingAddress.Address1;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToCity = billingAddress.City;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToState = billingAddress.State;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToPostalCode = billingAddress.PostalCode;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToCounty = billingAddress.County;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToCountry = billingAddress.Country;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToPhone = billingAddress.Phone;

            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToName = billingAddress.Name;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToAddress = shippingAddress.Address1;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToCity = shippingAddress.City;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToState = shippingAddress.State;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToPostalCode = shippingAddress.PostalCode;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToCounty = shippingAddress.County;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToCountry = shippingAddress.Country;
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToEmail = shippingAddress.EMail;

            AddLineItems();
            //ComputeFreightCharge();
        }
    }

    private void AddLineItems()
    {
        //Query the item
        string customerCode, uom = string.Empty;
        foreach(CustomCart item in items)
        {
            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "select UnitMeasureCode from InventoryUnitMeasure WHERE ItemCode = {0} AND IsBase = 1", DB.SQuote(item.ItemCode)))
                {
                    if (reader.Read())
                    {
                        uom = reader.ToRSField("UnitMeasureCode");
                    }
                }
            }

            decimal promotionalPrice = 0;
            customerCode = CommonLogic.IIF(ThisCustomer.IsRegistered, ThisCustomer.CustomerCode, ThisCustomer.AnonymousCustomerCode);

            item.Price = InterpriseHelper.GetSalesPriceAndTax(customerCode, item.ItemCode, ThisCustomer.CurrencyCode, item.Quantity, uom, true, ref promotionalPrice);

            using (var facadeList = new ListControlFacade())
            {
                using (var datasetItem = new DataSet())
                {
                    var itemBaseDataset = new BaseDataset();

                    //var umInfo = InterpriseHelper.GetItemUnitMeasure(item.ItemCode, item.UnitMeasureCode);
                    string query = string.Format("ItemCode = {0} AND (WarehouseCode = {1} or WarehouseCode IS NULL) AND UnitMeasureCode = {2}",
                                    DB.SQuote(item.ItemCode),
                                    DB.SQuote(ThisCustomer.WarehouseCode),
                                    DB.SQuote(uom));

                    //deadlock
                    int nooftries = 0;
                    while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
                    {
                        try
                        {
                            facadeList.ReadSearchResultsData("SaleItemView", query, 1, false, string.Empty, ref itemBaseDataset, true);
                            break;
                        }
                        catch (SqlException ex)
                        {
                            if (ex.ErrorCode == 1205)
                            {
                                nooftries += 1;
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }

                    if (itemBaseDataset.Tables["SaleItemView"] != null &&
                        itemBaseDataset.Tables["SaleItemView"].Rows.Count > 0)
                    {
                        var itemRow = itemBaseDataset.Tables["SaleItemView"].Rows[0];
                        var dvSalesOrderItem = new DataView(_gatewaySalesOrderDataset.CustomerSalesOrderDetailView);
                        var lineItemRow = dvSalesOrderItem.AddNew();

                        switch ((string)itemRow["ItemType"])
                        {
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_STOCK:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_NON_STOCK:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_SERVICE:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ELECTRONIC_DOWNLOAD:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ASSEMBLY:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CARD:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CERTIFICATE:
                                string error = string.Empty;
                                if (_facadeSalesOrder.AssignInventoryItem(itemRow, ref lineItemRow, ref error))
                                {
                                    lineItemRow["QuantityOrdered"] = item.Quantity;
                                    lineItemRow["Colour_C"] = item.Colour;
                                    _facadeSalesOrder.AssignQuantityOrdered(lineItemRow, _facadeSalesOrder.TransactionType);
                                    bool byTotalQty = false;
                                    _facadeSalesOrder.SetSalesPrice(lineItemRow, ref byTotalQty);

                                    if (ServiceFactory.GetInstance<IAppConfigService>().UseWebStorePricing)
                                    {
                                        lineItemRow["SalesPriceRate"] = Convert.ToDecimal(item.UnitPrice);

                                        _facadeSalesOrder.AssignDefaults(new object[][] {
	                                                new object[] {
		                                                _gatewaySalesOrderDataset.CustomerSalesOrderDetailView.SalesPriceColumn.ColumnName,
		                                                Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(_gatewaySalesOrderDataset.CustomerSalesOrderView[0].ExchangeRate, Convert.ToDecimal(item.Price), true, String.Empty, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.CostPrice)
	                                                },
	                                                new object[] {
		                                                _gatewaySalesOrderDataset.CustomerSalesOrderDetailView.PricingColumn.ColumnName,
		                                                InterpriseHelper.ConfigInstance.UserInfo.UserName
	                                                }}, ref lineItemRow);
                                        _facadeSalesOrder.ComputeKitItemsSalesPrice(lineItemRow.Row);
                                    }
                                    _facadeSalesOrder.Compute(lineItemRow.Row, _facadeSalesOrder.TransactionType);
                                }
                                break;
                        }

                        lineItemRow["WebSiteCode"] = InterpriseHelper.ConfigInstance.WebSiteCode;
                    }
                    // Explicitly dereference here
                    itemBaseDataset.Dispose();
                    itemBaseDataset = null;
                }
            }
        }
    }

    private void ComputeFreightCharge(Address shippingAddress)
    {
        string customerCode, shiptoCode = string.Empty;

        customerCode = CommonLogic.IIF(ThisCustomer.IsRegistered, ThisCustomer.CustomerCode, ThisCustomer.AnonymousCustomerCode);

        using (SqlConnection con = DB.NewSqlConnection())
        {
            con.Open();
            using (IDataReader reader = DB.GetRSFormat(con, "SELECT TOP 1 ShippingMethod FROM CustomerShipTo WHERE ShipToCode IN (select DefaultShipToCode from Customer WHERE CustomerCode={0})", DB.SQuote(customerCode)))
            {
                if (reader.Read())
                {
                    shiptoCode = reader.ToRSField("ShippingMethod");
                }
            }
        }

        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShippingMethod = shiptoCode;
        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShippingMethodCode = shiptoCode;

        //decimal freight = Decimal.Zero;
        //decimal freightRate = Decimal.Zero;
        var cart = ServiceFactory.GetInstance<IShoppingCartService>().New(CartTypeEnum.ShoppingCart, true);
        //var shippingMethod = cart.GetShippingMethods(shippingAddress);

        //if (shippingMethod != null && shippingMethod.Count > 0)
        //{
        //    for (int i = 0; i < shippingMethod.Count; i++)
        //    {
        //        if (i == 0 || shippingMethod[i].ForOversizedItem)
        //        { freightRate += shippingMethod[i].Freight; }
        //    }

        //    if (cart.IsSalesOrderDetailBuilt)
        //    {
        //        decimal exchangeRate = _facadeSalesOrder.GetExchangerate(ThisCustomer.CurrencyCode);
        //        freight = _facadeSalesOrder.ConvertCurrency(exchangeRate, freightRate, true, ThisCustomer.CurrencyCode);
        //        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].Freight = freight;
        //        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].FreightRate = freightRate;
        //        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].IsFreightOverwrite = true;
        //    }
        //}
        _facadeSalesOrder.ComputeFreight();

        var soDataset = (SalesOrderDatasetGateway)_facadeSalesOrder.CurrentDataset;
        //Modify each due date in line item 
        foreach (DataRow drow in soDataset.CustomerSalesOrderDetailView.Rows)
        {
            drow.BeginEdit();
            drow[soDataset.CustomerSalesOrderDetailView.DueDateColumn.ColumnName] = Interprise.Framework.Base.Shared.Const.MAX_SMALLDATETIME;
            drow.EndEdit();
        }

        //Modify shipping date
        soDataset.CustomerSalesOrderView[0].BeginEdit();
        if (HasAllocatedQuantity())
        {
            //Cart has allocated quantities, return shipping date to the current date...
            soDataset.CustomerSalesOrderView[0][soDataset.CustomerSalesOrderView.ShippingDateColumn.ColumnName] = DateTime.Now;
        }
        else
        {
            //Remodify ship date to retrieve the latest possible shipping date based on the reservation dates...
            //_facadeSalesOrder.LatestShippingDate throws an exception.
            //soDataset.CustomerSalesOrderView[0][soDataset.CustomerSalesOrderView.ShippingDateColumn.ColumnName] = _facadeSalesOrder.LatestShippingDate;
        }
        soDataset.CustomerSalesOrderView[0].EndEdit();

        //object disposal/dereference
        soDataset.Dispose();
        soDataset = null;
    }

    private bool HasAllocatedQuantity()
    {
        var soDataset = (SalesOrderDatasetGateway)_facadeSalesOrder.CurrentDataset;

        bool result = soDataset.CustomerSalesOrderDetailView.Rows
                            .OfType<SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow>()
                            .Any(row => row.QuantityAllocated > 0);

        soDataset.Dispose();
        soDataset = null;

        return result;
    }

    private void TagOrder(string salesOrderCode)
    {
        // mark this order
        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode = salesOrderCode;
        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].POCode = "Web Order";
        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SourceCode = "Internet";
        if (_facadeSalesOrder.TransactionType == Interprise.Framework.Base.Shared.Enum.TransactionType.Quote)
        {
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0]["CostCenter_DEV004817"] = "Smartbag-CUSTOM";
        }
        else
        {
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0]["CostCenter_DEV004817"] = ThisCustomer.ThisCustomerSession["CostCenter"];
        }
        if (ThisCustomer.IsRegistered)
        {
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ContactCode = ThisCustomer.ContactCode;
        }
    }

    private void UpdateBillingAndShippingInformation()
    {
        //***************************************************************
        //  Update the Bill To and Ship To Information
        //  Incase the customer has multiple billto's and shipto's
        //***************************************************************
        var salesOrderRow = _gatewaySalesOrderDataset.CustomerSalesOrderView[0];
        // set the payment term based on the customer...
        salesOrderRow.PaymentTermCode = ThisCustomer.PaymentTermCode;
        // billing information                    
        salesOrderRow.BillToName = billingAddress.Name;
        salesOrderRow.BillToAddress = billingAddress.Address1;
        salesOrderRow.BillToCity = billingAddress.City;
        salesOrderRow.BillToCounty = billingAddress.County;
        salesOrderRow.BillToState = billingAddress.State;
        salesOrderRow.BillToPostalCode = billingAddress.PostalCode;
        salesOrderRow.BillToCountry = billingAddress.Country;
        salesOrderRow.BillToPhone = billingAddress.Phone;

        if (!billingAddress.Plus4.IsNullOrEmptyTrimmed())
        {
            salesOrderRow.BillToPlus4 = Convert.ToInt32(billingAddress.Plus4);
        }

            // shipping information
            salesOrderRow.ShipToName = shippingAddress.Name;
            salesOrderRow.ShipToAddress = shippingAddress.Address1;
            salesOrderRow.ShipToCity = shippingAddress.City;
            salesOrderRow.ShipToCounty = shippingAddress.County;
            salesOrderRow.ShipToState = shippingAddress.State;
            salesOrderRow.ShipToPostalCode = shippingAddress.PostalCode;
            salesOrderRow.ShipToCountry = shippingAddress.Country;
            salesOrderRow.ShipToPhone = shippingAddress.Phone;

        if (!shippingAddress.Plus4.IsNullOrEmptyTrimmed())
        {
            salesOrderRow.ShipToPlus4 = Convert.ToInt32(shippingAddress.Plus4);
        }

        //if (ThisCustomer.IsNotRegistered)
        //{
        //        salesOrderRow.ShippingMethodGroup = shippingAddress.ShippingMethodGroup;
        //        salesOrderRow.PaymentTermGroup = shippingAddress.PaymentTermGroup;
        //}
    }

    public string PlaceOrder(ref string salesOrderCode, Address billing, Address shipping)
    {
        billingAddress = billing;
        shippingAddress = shipping;

        //Optional if already set to Quote
        _facadeSalesOrder.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.Quote;
        if (CommonLogic.IsStringNullOrEmpty(salesOrderCode))
        {
            salesOrderCode = _facadeSalesOrder.GenerateDocumentCode(_facadeSalesOrder.TransactionType.ToString());
        }
        ComputeFreightCharge(shippingAddress);
        TagOrder(salesOrderCode);
        //UpdateBillingAndShippingInformation();

        SaveSalesOrderAndAllocate(salesOrderCode);

        ThisCustomer.ThisCustomerSession.ClearVal("anonymousCompany");

        return AppLogic.ro_OK;
    }

    private void SaveSalesOrderAndAllocate(string salesOrderCode)
    {
        //get so dataset instance
        var soDataset = (SalesOrderDatasetGateway)_facadeSalesOrder.CurrentDataset;

        // Clear Reservation transaction - if sales order code equal to temporary document code
        _facadeSalesOrder.ClearTransactionReservation(soDataset.CustomerSalesOrderView[0].SalesOrderCode.ToString() == Interprise.Framework.Base.Shared.Const.TEMPORARY_DOCUMENTCODE);

        //Call for INITIAL allocation of items on the order
        _facadeSalesOrder.AllocateStock();

        //Retry saving if concurrency error was encountered otherwise, simply throw the exception.
            int nooftries = 0;
            while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
            {
                try
                {
                    string[] relatedTables = _facadeSalesOrder.get_RelatedTables(_facadeSalesOrder.TransactionType);
                    string[][] commands = _facadeSalesOrder.CreateParameterSet(relatedTables);

                    //Set this property to skip stock deallocation. This should fix the deadlock issue.
                    foreach (DataRow drow in soDataset.CustomerSalesOrderDetailView.Rows)
                    {
                        drow.BeginEdit();

                        if (_facadeSalesOrder.TransactionType == Interprise.Framework.Base.Shared.Enum.TransactionType.Quote)
                        {
                            drow[soDataset.CustomerSalesOrderDetailView.SourceDocumentTypeColumn.ColumnName] = Interprise.Framework.Base.Shared.Enum.TransactionType.Quote.ToString();
                        }

                        if (System.Convert.ToDecimal(Interprise.Framework.Base.Shared.Common.IsNull(drow["QuantityReserved"], 0)) > 0)
                        {
                            drow[soDataset.CustomerSalesOrderDetailView.DueDateColumn.ColumnName] = _facadeSalesOrder.LatestShippingDate;
                        }
                        else
                        {
                            drow[soDataset.CustomerSalesOrderDetailView.DueDateColumn.ColumnName] = DateTime.Now;
                        }

                        drow.EndEdit();
                    }//Check if transaction is Quote Then Clear reservation
                    if (_facadeSalesOrder.TransactionType == Interprise.Framework.Base.Shared.Enum.TransactionType.Quote)
                    {
                        //Change Header Type to Quote
                        foreach (DataRow drow in soDataset.CustomerSalesOrderView.Rows)
                        {
                            drow.BeginEdit();
                            drow[soDataset.CustomerSalesOrderView.TypeColumn.ColumnName] = Interprise.Framework.Base.Shared.Enum.TransactionType.Quote.ToString();
                            drow.EndEdit();
                        }

                        _facadeSalesOrder.IsSkipStockDeallocation = false;
                        _facadeSalesOrder.ClearTransactionReservation(true);
                    }
                    else
                    {
                        _facadeSalesOrder.IsSkipStockDeallocation = true;
                    }

                    _facadeSalesOrder.UpdateDataSet(commands, _facadeSalesOrder.TransactionType, string.Empty, false);
                    break;
                }
                catch (DBConcurrencyException)
                {
                    nooftries += 1;
                    SaveSalesOrderAndAllocate(_facadeSalesOrder.CurrentDataset.Tables["CustomerSalesOrderView"].Rows[0]["SalesOrderCode"].ToString());
                }
                catch (Exception)
                {
                    //???
                    throw;
                }
            }
    }
}

public class CustomCart
{
    string m_ItemCode, m_Colour, m_Comments = string.Empty;
    decimal m_Price, m_UnitPrice, m_Quantity;
    public CustomCart()
    {

    }

    public string ItemCode
    {
        get { return m_ItemCode; }
        set { m_ItemCode = value; }
    }

    public string Comments
    {
        get { return m_Comments; }
        set { m_Comments = value; }
    }

    public decimal Price
    {
        get { return m_Price; }
        set { m_Price = value; }
    }

    public decimal UnitPrice
    {
        get { return m_UnitPrice; }
        set { m_UnitPrice = value; }
    }

    public decimal Quantity 
    {
        get { return m_Quantity; }
        set { m_Quantity = value; }
    }

    public string Colour
    {
        get { return m_Colour; }
        set { m_Colour = value; }
    }
}