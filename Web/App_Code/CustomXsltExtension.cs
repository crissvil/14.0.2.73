﻿// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------

using InterpriseSuiteEcommerceCommon;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Web;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Xml.XPath;
using System.Data;
using System.Data.SqlClient;
using InterpriseSuiteEcommerceCommon.DataAccess;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System.Xml.Linq;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.Web;
using InterpriseSuiteEcommerceCommon.Tool;

/// <summary>
/// Use this class for custom XMLPackage Extension Functions.
/// *********************************************************
/// How to call this extension function from XMLPackage:
/// 1. Uncomment <!--<add name="custom" namespace="urn:custom" type="CustomXsltExtension, app_code"></add>--> on web.config
/// 2. Open any XMLPackage, find the code <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ise="urn:ise" exclude-result-prefixes="ise">
/// 3. Replace the code with <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ise="urn:ise" xmlns:custom="urn:custom" exclude-result-prefixes="ise">
/// 4. To call HelloWorld() on XMLPackage use this code <xsl:value-of select="custom:HelloWorld()" disable-output-escaping="yes" /> 
/// 5. Open browser and browse the page where the XMLPackage is called.
/// </summary>
public class CustomXsltExtension
{
    private List<ItemUnitMeasuresData> unitMeasures = new List<ItemUnitMeasuresData>();

    public CustomXsltExtension()
    {

    }

    #region Helper Template Codes

    //XML package helper template name
    //Populated during call of XmlPackage2
    //value will be the <XmlHelperPackage name=""> from node of an xmlpagckage(ex simpleproduct.xml.config)
    //Located after the query node
    public string XmlPackageHelperTemplate
    {
        get
        {
            string sXmlPackage = XmlPackageHelperTemplateList.ElementAt(XmlPackageHelperTemplateIndex);
            XmlPackageHelperTemplateIndex = 0;
            return sXmlPackage;
        }
    }

    public int XmlPackageHelperTemplateIndex { get; set; }

    public IEnumerable<string> XmlPackageHelperTemplateList { get; set; }

    private bool IsUsingHelperTemplate
    {
        get
        {
            return (XmlPackageHelperTemplateList != null && XmlPackageHelperTemplateList.Count() > 0);
        }
    }

    public virtual void SetXmlPackageHelperTemplate(string index)
    {
        var IV = new InterpriseSuiteEcommerceCommon.XSLTExtensionBase.InputValidator("SetXmlPackageHelperTemplate");
        XmlPackageHelperTemplateIndex = IV.ValidateInt("Index", index);
    }

    #endregion

    //Custom
    internal struct PriceListData
    {
        internal string PriceLevelCode;
        internal Dictionary<string, PriceListUnitMeasureData> UnitMeasures;

        internal PriceListData(string priceLevelCode)
        {
            PriceLevelCode = priceLevelCode;
            UnitMeasures = new Dictionary<string, PriceListUnitMeasureData>();
        }
    }

    internal struct PriceListUnitMeasureData
    {
        internal string UnitMeasureCode;
        internal string UnitMeasureDescription;
        internal decimal UnitMeasureQuantity;
        internal List<PriceListQuantityRangeData> QuantityRanges;

        internal PriceListUnitMeasureData(string unitMeasureCode, string unitMeasureDescription, decimal unitMeasureQuantity)
        {
            this.UnitMeasureCode = unitMeasureCode;
            this.UnitMeasureDescription = unitMeasureDescription;
            this.UnitMeasureQuantity = unitMeasureQuantity;
            this.QuantityRanges = new List<PriceListQuantityRangeData>();
        }
    }

    internal struct PriceListQuantityRangeData
    {
        internal decimal MinQuantity;
        internal decimal MaxQuantity;
        internal decimal Discount;
        internal decimal UnitSellingPrice;
    }

    public string GetProductPricingLevelExpress(string itemCode)
    {
        itemCode = HttpUtility.UrlDecode(itemCode);

        Customer thisCustomer = Customer.Current;
        string response = String.Empty;

        //Do not execute code if pricing level is not applicable
        if (thisCustomer.PricingLevel == String.Empty)
        {
            return response;
        }
        bool hasPricingLevel;

        // Customization

        var priceList = new PriceListData(thisCustomer.PricingLevel);

        int quantityDecimalPlaces = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetInventoryDecimalPlacesPreference();

        var itemDefaultUM = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);

        hasPricingLevel = false;

        string customerCode = CommonLogic.IIF(thisCustomer.IsNotRegistered, thisCustomer.AnonymousCustomerCode, thisCustomer.CustomerCode);
        // Get the pricing table discounts...

        using (var con = DB.NewSqlConnection())
        {
            string unitMeasureCode = string.Empty;
            string unitMeasureDescription = string.Empty;
            decimal unitMeasureQuantity;
            string currencyCode = string.Empty;
            decimal minQuantity;
            decimal maxQuantity;
            decimal discount;
            decimal unitSellingPrice;

            con.Open();
            using (var reader = DB.GetRSFormat(con, "exec eCommerceGetItemPricingLevel @ItemCode = {0}, @CustomerCode = {1}", DB.SQuote(itemCode), DB.SQuote(customerCode)))
            {
                while (reader.Read())
                {
                    hasPricingLevel = true;

                    unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                    unitMeasureDescription = DB.RSField(reader, "UnitMeasureDescription");
                    unitMeasureQuantity = DB.RSFieldDecimal(reader, "UnitMeasureQty");
                    currencyCode = DB.RSField(reader, "CurrencyCode");
                    minQuantity = DB.RSFieldDecimal(reader, "MinQuantity");
                    maxQuantity = DB.RSFieldDecimal(reader, "MaxQuantity");
                    discount = DB.RSFieldDecimal(reader, "Discount");
                    unitSellingPrice = DB.RSFieldDecimal(reader, "SalesPrice");

                    PriceListUnitMeasureData unitMeasure;

                    if (!priceList.UnitMeasures.ContainsKey(unitMeasureCode))
                    {
                        priceList.UnitMeasures.Add(unitMeasureCode, new PriceListUnitMeasureData(unitMeasureCode, unitMeasureDescription, unitMeasureQuantity));
                    }

                    unitMeasure = priceList.UnitMeasures[unitMeasureCode];

                    var quantityRange = new PriceListQuantityRangeData()
                    {
                        MinQuantity = minQuantity,
                        MaxQuantity = maxQuantity,
                        Discount = discount,
                        UnitSellingPrice = unitSellingPrice
                    };

                    unitMeasure.QuantityRanges.Add(quantityRange);
                }
            }


        }

        var productInfo = ServiceFactory.GetInstance<IProductService>().GetProductInfoViewForShowProduct(itemCode);
        if (productInfo != null && productInfo.ItemType.EqualsIgnoreCase("kit") && priceList.UnitMeasures.Count == 0)
        {
            var customer = ServiceFactory.GetInstance<IAuthenticationService>()
                                         .GetCurrentLoggedInCustomer();

            PriceListUnitMeasureData unitMeasure;

            if (!priceList.UnitMeasures.ContainsKey(itemDefaultUM.Code))
            {

                using (SqlConnection con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (IDataReader reader = DB.GetRSFormat(con, "SELECT UnitMeasureDescription, UnitMeasureQuantity FROM SystemUnitMeasure with (NOLOCK) WHERE UnitMeasureCode = {0}", DB.SQuote(itemDefaultUM.Code)))
                    {
                        if (reader.Read())
                        {
                            hasPricingLevel = true;
                            priceList.UnitMeasures.Add(itemDefaultUM.Code, new PriceListUnitMeasureData(itemDefaultUM.Code, DB.RSField(reader, "UnitMeasureDescription"), DB.RSFieldDecimal(reader, "UnitMeasureQuantity")));
                        }

                    }
                }
            }

            unitMeasure = priceList.UnitMeasures[itemDefaultUM.Code];
            decimal kitDiscount = ServiceFactory.GetInstance<IProductService>().GetKitDiscount(itemCode);

            decimal minQuantity = 1, maxQuantity = 1;

            var itemWebOption = ServiceFactory.GetInstance<IProductService>().GetWebOption(itemCode, true);
            minQuantity = itemWebOption.MinOrderQuantity == null || itemWebOption.MinOrderQuantity == 0 ? 1 : itemWebOption.MinOrderQuantity;
            maxQuantity = itemWebOption.RestrictedQuantities.Max();
            maxQuantity = maxQuantity == null || maxQuantity == 0 ? minQuantity : maxQuantity;
            decimal unitSellingPrice = InterpriseHelper.InventoryKitPackagePrice(itemCode, customer.CurrencyCode).ToDecimal() * (unitMeasure.UnitMeasureQuantity * minQuantity);
            var quantityRange = new PriceListQuantityRangeData()
            {
                MinQuantity = minQuantity,
                MaxQuantity = minQuantity,
                Discount = kitDiscount,
                UnitSellingPrice = unitSellingPrice
            };

            unitMeasure.QuantityRanges.Add(quantityRange);

        }

        // render the html
        var output = new StringBuilder();

        if (priceList.UnitMeasures.Count > 0)
        {


            // render the child items..
            foreach (PriceListUnitMeasureData unitMeasure in priceList.UnitMeasures.Values)
            {
                if (unitMeasure.UnitMeasureCode != itemDefaultUM.Code) continue;
                decimal finalPrice = 1;

                output.Append("    <ul>");

                for (int ctr = 0; ctr < 2; ctr++)
                {
                    var quantityRange = unitMeasure.QuantityRanges[ctr];

                    # region Get Price

                    decimal price = quantityRange.UnitSellingPrice;

                    if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                    {
                        decimal priceRate, cost, costRate, vat;
                        priceRate = price;
                        string currencyCode = string.Empty;
                        bool byTotalQuantity = false;
                        decimal x_salesPrice = decimal.Zero;
                        bool isSalesPriceInBasecurrency = false;
                        decimal regularPrice = decimal.Zero;
                        decimal promotionalPrice = decimal.Zero;
                        string pricing = string.Empty;
                        decimal percent = decimal.Zero;
                        decimal discount = decimal.Zero;
                        decimal categoryDiscount = decimal.Zero;
                        string customerItemCode = string.Empty;
                        string customerItemDescription = string.Empty;
                        string inventoryItemDescription = string.Empty;
                        decimal basePricingCost = decimal.Zero;
                        decimal baseAverageCost = decimal.Zero;
                        bool isInventorySpecialPriceExpired = false;
                        bool isCustomerSpecialPriceExpired = false;

                        Interprise.Facade.Customer.BaseSalesOrderFacade.GetPrice(
                                string.Empty,
                                ref currencyCode,
                                itemCode,
                                unitMeasure.UnitMeasureCode,
                                unitMeasure.UnitMeasureQuantity,
                                DateTime.Today,
                                decimal.Zero,
                                decimal.Zero,
                                Interprise.Framework.Base.Shared.Enum.DefaultPricing.None,
                                Interprise.Framework.Base.Shared.Enum.PricingMethod.None,
                                string.Empty,
                                ref byTotalQuantity,
                                ref x_salesPrice,
                                ref isSalesPriceInBasecurrency,
                                ref regularPrice,
                                ref promotionalPrice,
                                ref pricing,
                                ref percent,
                                ref discount,
                                ref categoryDiscount,
                                ref customerItemCode,
                                ref customerItemDescription,
                                ref inventoryItemDescription,
                                ref basePricingCost,
                                ref baseAverageCost,
                                ref isInventorySpecialPriceExpired,
                                ref isCustomerSpecialPriceExpired);

                        cost = baseAverageCost;
                        decimal exchangeRate = Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(thisCustomer.CurrencyCode);
                        costRate = Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(exchangeRate, cost, false, thisCustomer.CurrencyCode, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.Total);

                        if (thisCustomer.IsNotRegistered)
                        {
                            customerCode = thisCustomer.AnonymousCustomerCode;
                        }

                        vat = Interprise.Facade.Customer.ItemTaxFacade.CalculateTax(customerCode,
                                itemCode,
                                unitMeasure.UnitMeasureCode,
                                price,
                                priceRate,
                                cost,
                                costRate,
                                decimal.One);

                        if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            price += vat;
                        }
                    }

                    # endregion Get Price

                    string uom = unitMeasure.UnitMeasureDescription;
                    if (uom.ToLower() == "ctn")
                    {
                        uom = (quantityRange.MinQuantity > 1) ? "Cartons" : "Carton";
                    }

                    output.Append("<li class='li-price-range'>    <div class='product-price-range'>");
                    // the quanty range columns
                    # region the quanty range column
                    output.Append("    <div class='product-item-unit-quantity'>");
                    if (ctr + 1 == unitMeasure.QuantityRanges.Count && unitMeasure.QuantityRanges.Count > 1)
                    {
                        output.AppendFormat("        {0}+{1}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                    }
                    else
                    {
                        if (ctr == 0)//if (quantityRange.MinQuantity == 1 && quantityRange.MaxQuantity == 2)
                        {
                            output.AppendFormat("        {0} {1}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                        }
                        else
                        {
                            output.AppendFormat("        {0} - {1} {2}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), Localization.ParseLocaleDecimal(quantityRange.MaxQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                        }

                    }
                    output.Append("    </div>");

                    # endregion the quanty range column

                    #region Price

                    output.Append("    <div class='product-item-price-range'>");
                    //output.AppendFormat("        {0} <span class='product-item-price-range-each'>EACH</span>", price.ToCustomerCurrency());
                    output.AppendFormat("{0}", price.ToCustomerCurrency());
                    if (AppLogic.AppConfigBool("VAT.Enabled"))
                    {
                        if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            output.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.38") + "</span>\n");
                        }
                        else
                        {
                            output.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.37") + "</span>\n");
                        }
                    }
                    output.Append("    </div>");

                    finalPrice = price;

                    if (unitMeasure.QuantityRanges.Count < 2)
                        finalPrice = finalPrice / quantityRange.MinQuantity;

                    #endregion Price

                    #region "PerItem"

                    decimal pricePer = (finalPrice / unitMeasure.UnitMeasureQuantity).ToCustomerRoundedCurrency();
                    string pricePerString = pricePer.ToCustomerCurrency();
                    if (pricePer < 1)
                    {
                        var arrayStr = pricePer.ToString().Split('.');
                        if (arrayStr.Length == 2) { pricePerString = "*." + arrayStr[1] + "¢"; }
                        else if (arrayStr.Length == 1) { pricePerString = "*." + arrayStr[0] + "¢"; }
                        else { pricePerString = pricePer.ToCustomerCurrency(); }
                    }

                    //output.Append("    <div class='product-item-price-per'>");
                    //output.Append(String.Format("{0} per {1}*", pricePerString, unitMeasure.UnitMeasureDescription));
                    //output.Append("    </div>");

                    #endregion "PerItem"

                    output.Append("    </div> </li>");

                    if (unitMeasure.QuantityRanges.Count < 2) break;

                }

                output.Append("    </ul>");

            }

            response = output.ToString();
        }

        // Customization


        //response = InterpriseHelper.GetInventoryPricingLevelTable(Customer.Current, itemCode, out hasPricingLevel);

        if (!hasPricingLevel)
        {
            return string.Empty;
        }

        return response;
    }

    public string GetEntityPricingLevelExpress(string itemCode)
    {
        itemCode = HttpUtility.UrlDecode(itemCode);

        Customer thisCustomer = Customer.Current;
        string response = String.Empty;

        //Do not execute code if pricing level is not applicable
        if (thisCustomer.PricingLevel == String.Empty)
        {
            return response;
        }
        bool hasPricingLevel;

        // Customization

        var priceList = new PriceListData(thisCustomer.PricingLevel);

        int quantityDecimalPlaces = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetInventoryDecimalPlacesPreference();

        var itemDefaultUM = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);

        hasPricingLevel = false;

        string customerCode = CommonLogic.IIF(thisCustomer.IsNotRegistered, thisCustomer.AnonymousCustomerCode, thisCustomer.CustomerCode);
        // Get the pricing table discounts...

        using (var con = DB.NewSqlConnection())
        {
            string unitMeasureCode = string.Empty;
            string unitMeasureDescription = string.Empty;
            decimal unitMeasureQuantity;
            string currencyCode = string.Empty;
            decimal minQuantity;
            decimal maxQuantity;
            decimal discount;
            decimal unitSellingPrice;

            con.Open();
            using (var reader = DB.GetRSFormat(con, "exec eCommerceGetItemPricingLevel @ItemCode = {0}, @CustomerCode = {1}", DB.SQuote(itemCode), DB.SQuote(customerCode)))
            {
                while (reader.Read())
                {
                    hasPricingLevel = true;

                    unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                    unitMeasureDescription = DB.RSField(reader, "UnitMeasureDescription");
                    unitMeasureQuantity = DB.RSFieldDecimal(reader, "UnitMeasureQty");
                    currencyCode = DB.RSField(reader, "CurrencyCode");
                    minQuantity = DB.RSFieldDecimal(reader, "MinQuantity");
                    maxQuantity = DB.RSFieldDecimal(reader, "MaxQuantity");
                    discount = DB.RSFieldDecimal(reader, "Discount");
                    unitSellingPrice = DB.RSFieldDecimal(reader, "SalesPrice");

                    PriceListUnitMeasureData unitMeasure;

                    if (!priceList.UnitMeasures.ContainsKey(unitMeasureCode))
                    {
                        priceList.UnitMeasures.Add(unitMeasureCode, new PriceListUnitMeasureData(unitMeasureCode, unitMeasureDescription, unitMeasureQuantity));
                    }

                    unitMeasure = priceList.UnitMeasures[unitMeasureCode];

                    var quantityRange = new PriceListQuantityRangeData()
                    {
                        MinQuantity = minQuantity,
                        MaxQuantity = maxQuantity,
                        Discount = discount,
                        UnitSellingPrice = unitSellingPrice
                    };

                    unitMeasure.QuantityRanges.Add(quantityRange);
                }
            }


        }

        var productInfo = ServiceFactory.GetInstance<IProductService>().GetProductInfoViewForShowProduct(itemCode);
        var itemWebOption = ServiceFactory.GetInstance<IProductService>().GetWebOption(itemCode, true);
        if (productInfo != null && productInfo.ItemType.EqualsIgnoreCase("kit") && priceList.UnitMeasures.Count == 0)
        {
            var customer = ServiceFactory.GetInstance<IAuthenticationService>()
                                         .GetCurrentLoggedInCustomer();

            PriceListUnitMeasureData unitMeasure;

            if (!priceList.UnitMeasures.ContainsKey(itemDefaultUM.Code))
            {

                using (SqlConnection con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (IDataReader reader = DB.GetRSFormat(con, "SELECT UnitMeasureDescription, UnitMeasureQuantity FROM SystemUnitMeasure with (NOLOCK) WHERE UnitMeasureCode = {0}", DB.SQuote(itemDefaultUM.Code)))
                    {
                        if (reader.Read())
                        {
                            hasPricingLevel = true;
                            priceList.UnitMeasures.Add(itemDefaultUM.Code, new PriceListUnitMeasureData(itemDefaultUM.Code, DB.RSField(reader, "UnitMeasureDescription"), DB.RSFieldDecimal(reader, "UnitMeasureQuantity")));
                        }

                    }
                }
            }

            unitMeasure = priceList.UnitMeasures[itemDefaultUM.Code];
            decimal kitDiscount = ServiceFactory.GetInstance<IProductService>().GetKitDiscount(itemCode);

            decimal minQuantity = 1, maxQuantity = 1;

            minQuantity = itemWebOption.MinOrderQuantity == null || itemWebOption.MinOrderQuantity == 0 ? 1 : itemWebOption.MinOrderQuantity;
            maxQuantity = itemWebOption.RestrictedQuantities.Max();
            maxQuantity = maxQuantity == null || maxQuantity == 0 ? minQuantity : maxQuantity;
            decimal unitSellingPrice = InterpriseHelper.InventoryKitPackagePrice(itemCode, customer.CurrencyCode).ToDecimal() * (unitMeasure.UnitMeasureQuantity * minQuantity);
            var quantityRange = new PriceListQuantityRangeData()
            {
                MinQuantity = minQuantity,
                MaxQuantity = minQuantity,
                Discount = kitDiscount,
                UnitSellingPrice = unitSellingPrice
            };

            unitMeasure.QuantityRanges.Add(quantityRange);
        }

        // render the html
        var output = new StringBuilder();
        if (priceList.UnitMeasures.Count > 0)
        {
            // render the child items..
            foreach (PriceListUnitMeasureData unitMeasure in priceList.UnitMeasures.Values)
            {
                if (unitMeasure.UnitMeasureCode != itemDefaultUM.Code) continue;
                decimal finalPrice = 1;

                output.Append("    <ul>");

                for (int ctr = 0; ctr < 2; ctr++)
                {
                    var quantityRange = unitMeasure.QuantityRanges[ctr];

                    # region Get Price

                    decimal price = quantityRange.UnitSellingPrice;

                    if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                    {
                        decimal priceRate, cost, costRate, vat;
                        priceRate = price;
                        string currencyCode = string.Empty;
                        bool byTotalQuantity = false;
                        decimal x_salesPrice = decimal.Zero;
                        bool isSalesPriceInBasecurrency = false;
                        decimal regularPrice = decimal.Zero;
                        decimal promotionalPrice = decimal.Zero;
                        string pricing = string.Empty;
                        decimal percent = decimal.Zero;
                        decimal discount = decimal.Zero;
                        decimal categoryDiscount = decimal.Zero;
                        string customerItemCode = string.Empty;
                        string customerItemDescription = string.Empty;
                        string inventoryItemDescription = string.Empty;
                        decimal basePricingCost = decimal.Zero;
                        decimal baseAverageCost = decimal.Zero;
                        bool isInventorySpecialPriceExpired = false;
                        bool isCustomerSpecialPriceExpired = false;

                        Interprise.Facade.Customer.BaseSalesOrderFacade.GetPrice(
                                string.Empty,
                                ref currencyCode,
                                itemCode,
                                unitMeasure.UnitMeasureCode,
                                unitMeasure.UnitMeasureQuantity,
                                DateTime.Today,
                                decimal.Zero,
                                decimal.Zero,
                                Interprise.Framework.Base.Shared.Enum.DefaultPricing.None,
                                Interprise.Framework.Base.Shared.Enum.PricingMethod.None,
                                string.Empty,
                                ref byTotalQuantity,
                                ref x_salesPrice,
                                ref isSalesPriceInBasecurrency,
                                ref regularPrice,
                                ref promotionalPrice,
                                ref pricing,
                                ref percent,
                                ref discount,
                                ref categoryDiscount,
                                ref customerItemCode,
                                ref customerItemDescription,
                                ref inventoryItemDescription,
                                ref basePricingCost,
                                ref baseAverageCost,
                                ref isInventorySpecialPriceExpired,
                                ref isCustomerSpecialPriceExpired);

                        cost = baseAverageCost;
                        decimal exchangeRate = Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(thisCustomer.CurrencyCode);
                        costRate = Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(exchangeRate, cost, false, thisCustomer.CurrencyCode, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.Total);

                        if (thisCustomer.IsNotRegistered)
                        {
                            customerCode = thisCustomer.AnonymousCustomerCode;
                        }

                        vat = Interprise.Facade.Customer.ItemTaxFacade.CalculateTax(customerCode,
                                itemCode,
                                unitMeasure.UnitMeasureCode,
                                price,
                                priceRate,
                                cost,
                                costRate,
                                decimal.One);

                        if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            price += vat;
                        }
                    }

                    # endregion Get Price

                    string uom = unitMeasure.UnitMeasureDescription;
                    if (uom.ToLower() == "ctn" || uom.ToLower() == "carton")
                    {
                        uom = (quantityRange.MinQuantity > 1) ? "Cartons" : "Carton";
                    }

                    output.Append("<li class='li-price-range'>    <div class='product-price-range-express'>");
                    #region Price
                    output.Append("    <div class='category-item-price-range'>");
                    //output.AppendFormat("        {0} <span class='entity-item-price-range-each'>EACH</span>", price.ToCustomerCurrency());
                    output.AppendFormat("{0}*", price.ToCustomerCurrency());
                    if (AppLogic.AppConfigBool("VAT.Enabled"))
                    {
                        if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            output.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.38") + "</span>\n");
                        }
                        else
                        {
                            output.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.37") + "</span>\n");
                        }
                    }
                    output.Append("    </div>");
                    finalPrice = price;
                    if (unitMeasure.QuantityRanges.Count < 2)
                        finalPrice = finalPrice / quantityRange.MinQuantity;
                    #endregion Price

                    // the quanty range columns
                    # region the quanty range column
                    output.Append("    <div class='category-item-unit-quantity'>");
                    if (ctr + 1 == unitMeasure.QuantityRanges.Count && unitMeasure.QuantityRanges.Count > 1)
                    {
                        output.AppendFormat("        {0}+{1}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                    }
                    else
                    {
                        if (ctr == 0)//if (quantityRange.MinQuantity == 1 && quantityRange.MaxQuantity == 2)
                        {
                            output.AppendFormat("        {0} {1}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                        }
                        else
                        {
                            output.AppendFormat("        {0} - {1} {2}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), Localization.ParseLocaleDecimal(quantityRange.MaxQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                        }
                    }
                    output.Append("    </div>");
                    # endregion the quanty range column

                    #region "PerItem"
                    decimal pricePer = (finalPrice / unitMeasure.UnitMeasureQuantity).ToCustomerRoundedCurrency();
                    string pricePerString = pricePer.ToCustomerCurrency();
                    if (pricePer < 1)
                    {
                        var arrayStr = pricePer.ToString().Split('.');
                        if (arrayStr.Length == 2) { pricePerString = "*." + arrayStr[1] + "¢"; }
                        else if (arrayStr.Length == 1) { pricePerString = "*." + arrayStr[0] + "¢"; }
                        else { pricePerString = pricePer.ToCustomerCurrency(); }
                    }

                    output.Append("    <div class='category-item-price-per'>");
                    output.Append(String.Format("{0} per {1}*", unitMeasure.UnitMeasureQuantity.ToNumberFormat(), unitMeasure.UnitMeasureDescription));
                    output.Append("    </div>");

                    #endregion "PerItem"
                    output.Append("<div class='express-print-text'>" + itemWebOption.PrintInfo + "</div>");
                    output.Append("    </div> </li>");

                    if (unitMeasure.QuantityRanges.Count < 2) break;

                }

                output.Append("    </ul>");

            }

            response = output.ToString();
        }

        // Customization


        //response = InterpriseHelper.GetInventoryPricingLevelTable(Customer.Current, itemCode, out hasPricingLevel);

        if (!hasPricingLevel)
        {
            return string.Empty;
        }

        return response;
    }

    public string GetPricingLevelExpressPrint(string itemCode)
    {
        itemCode = HttpUtility.UrlDecode(itemCode);

        Customer thisCustomer = Customer.Current;
        string response = String.Empty;

        //Do not execute code if pricing level is not applicable
        if (thisCustomer.PricingLevel == String.Empty)
        {
            return response;
        }
        bool hasPricingLevel;

        // Customization

        var priceList = new PriceListData(thisCustomer.PricingLevel);

        int quantityDecimalPlaces = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetInventoryDecimalPlacesPreference();

        var itemDefaultUM = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);

        hasPricingLevel = false;

        string customerCode = CommonLogic.IIF(thisCustomer.IsNotRegistered, thisCustomer.AnonymousCustomerCode, thisCustomer.CustomerCode);
        // Get the pricing table discounts...

        using (var con = DB.NewSqlConnection())
        {
            string unitMeasureCode = string.Empty;
            string unitMeasureDescription = string.Empty;
            decimal unitMeasureQuantity;
            string currencyCode = string.Empty;
            decimal minQuantity;
            decimal maxQuantity;
            decimal discount;
            decimal unitSellingPrice;

            con.Open();
            using (var reader = DB.GetRSFormat(con, "exec eCommerceGetItemPricingLevel @ItemCode = {0}, @CustomerCode = {1}", DB.SQuote(itemCode), DB.SQuote(customerCode)))
            {
                while (reader.Read())
                {
                    hasPricingLevel = true;

                    unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                    unitMeasureDescription = DB.RSField(reader, "UnitMeasureDescription");
                    unitMeasureQuantity = DB.RSFieldDecimal(reader, "UnitMeasureQty");
                    currencyCode = DB.RSField(reader, "CurrencyCode");
                    minQuantity = DB.RSFieldDecimal(reader, "MinQuantity");
                    maxQuantity = DB.RSFieldDecimal(reader, "MaxQuantity");
                    discount = DB.RSFieldDecimal(reader, "Discount");
                    unitSellingPrice = DB.RSFieldDecimal(reader, "SalesPrice");

                    PriceListUnitMeasureData unitMeasure;

                    if (!priceList.UnitMeasures.ContainsKey(unitMeasureCode))
                    {
                        priceList.UnitMeasures.Add(unitMeasureCode, new PriceListUnitMeasureData(unitMeasureCode, unitMeasureDescription, unitMeasureQuantity));
                    }

                    unitMeasure = priceList.UnitMeasures[unitMeasureCode];

                    var quantityRange = new PriceListQuantityRangeData()
                    {
                        MinQuantity = minQuantity,
                        MaxQuantity = maxQuantity,
                        Discount = discount,
                        UnitSellingPrice = unitSellingPrice
                    };

                    unitMeasure.QuantityRanges.Add(quantityRange);
                }
            }
        }

        var productInfo = ServiceFactory.GetInstance<IProductService>().GetProductInfoViewForShowProduct(itemCode);
        var itemWebOption = ServiceFactory.GetInstance<IProductService>().GetWebOption(itemCode, true);
        if (productInfo != null && productInfo.ItemType.EqualsIgnoreCase("kit") && priceList.UnitMeasures.Count == 0)
        {
            var customer = ServiceFactory.GetInstance<IAuthenticationService>()
                                         .GetCurrentLoggedInCustomer();

            PriceListUnitMeasureData unitMeasure;

            if (!priceList.UnitMeasures.ContainsKey(itemDefaultUM.Code))
            {

                using (SqlConnection con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (IDataReader reader = DB.GetRSFormat(con, "SELECT UnitMeasureDescription, UnitMeasureQuantity FROM SystemUnitMeasure with (NOLOCK) WHERE UnitMeasureCode = {0}", DB.SQuote(itemDefaultUM.Code)))
                    {
                        if (reader.Read())
                        {
                            hasPricingLevel = true;
                            priceList.UnitMeasures.Add(itemDefaultUM.Code, new PriceListUnitMeasureData(itemDefaultUM.Code, DB.RSField(reader, "UnitMeasureDescription"), DB.RSFieldDecimal(reader, "UnitMeasureQuantity")));
                        }

                    }
                }
            }

            unitMeasure = priceList.UnitMeasures[itemDefaultUM.Code];
            decimal kitDiscount = ServiceFactory.GetInstance<IProductService>().GetKitDiscount(itemCode);

            decimal minQuantity = 1, maxQuantity = 1;

            minQuantity = itemWebOption.MinOrderQuantity == null || itemWebOption.MinOrderQuantity == 0 ? 1 : itemWebOption.MinOrderQuantity;
            //Check if MinOrderQuantity is smaller than minimum RestrictedQuantity then use minimum RestrictedQuantity.
            if (itemWebOption.RestrictedQuantities.Count > 0)
            {
                minQuantity = CommonLogic.IIF(minQuantity > itemWebOption.RestrictedQuantities.Min(), minQuantity, itemWebOption.RestrictedQuantities.Min());
            }
            maxQuantity = itemWebOption.RestrictedQuantities.Count == 0 ? 0 : itemWebOption.RestrictedQuantities.Max();

            maxQuantity = maxQuantity == null || maxQuantity == 0 ? minQuantity : maxQuantity;
            //decimal unitSellingPrice = InterpriseHelper.InventoryKitPackagePrice(itemCode, customer.CurrencyCode, (unitMeasure.UnitMeasureQuantity * minQuantity));
            decimal unitSellingPrice = InterpriseHelper.InventoryKitPackagePriceExpressPrint(itemCode);
            var quantityRange = new PriceListQuantityRangeData()
            {
                MinQuantity = minQuantity,
                MaxQuantity = maxQuantity,
                Discount = kitDiscount,
                UnitSellingPrice = unitSellingPrice
            };

            unitMeasure.QuantityRanges.Add(quantityRange);

        }

        // render the html
        var output = new StringBuilder();

        if (priceList.UnitMeasures.Count > 0)
        {
            // render the child items..
            foreach (PriceListUnitMeasureData unitMeasure in priceList.UnitMeasures.Values)
            {
                if (unitMeasure.UnitMeasureCode != itemDefaultUM.Code) continue;
                decimal finalPrice = 1;
                for (int ctr = 0; ctr < 2; ctr++)
                {

                    var quantityRange = unitMeasure.QuantityRanges[ctr];

                    # region Get Price

                    decimal price = quantityRange.UnitSellingPrice;

                    if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                    {
                        decimal priceRate, cost, costRate, vat;
                        priceRate = price;
                        string currencyCode = string.Empty;
                        bool byTotalQuantity = false;
                        decimal x_salesPrice = decimal.Zero;
                        bool isSalesPriceInBasecurrency = false;
                        decimal regularPrice = decimal.Zero;
                        decimal promotionalPrice = decimal.Zero;
                        string pricing = string.Empty;
                        decimal percent = decimal.Zero;
                        decimal discount = decimal.Zero;
                        decimal categoryDiscount = decimal.Zero;
                        string customerItemCode = string.Empty;
                        string customerItemDescription = string.Empty;
                        string inventoryItemDescription = string.Empty;
                        decimal basePricingCost = decimal.Zero;
                        decimal baseAverageCost = decimal.Zero;
                        bool isInventorySpecialPriceExpired = false;
                        bool isCustomerSpecialPriceExpired = false;

                        Interprise.Facade.Customer.BaseSalesOrderFacade.GetPrice(
                                string.Empty,
                                ref currencyCode,
                                itemCode,
                                unitMeasure.UnitMeasureCode,
                                unitMeasure.UnitMeasureQuantity,
                                DateTime.Today,
                                decimal.Zero,
                                decimal.Zero,
                                Interprise.Framework.Base.Shared.Enum.DefaultPricing.None,
                                Interprise.Framework.Base.Shared.Enum.PricingMethod.None,
                                string.Empty,
                                ref byTotalQuantity,
                                ref x_salesPrice,
                                ref isSalesPriceInBasecurrency,
                                ref regularPrice,
                                ref promotionalPrice,
                                ref pricing,
                                ref percent,
                                ref discount,
                                ref categoryDiscount,
                                ref customerItemCode,
                                ref customerItemDescription,
                                ref inventoryItemDescription,
                                ref basePricingCost,
                                ref baseAverageCost,
                                ref isInventorySpecialPriceExpired,
                                ref isCustomerSpecialPriceExpired);

                        cost = baseAverageCost;
                        decimal exchangeRate = Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(thisCustomer.CurrencyCode);
                        costRate = Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(exchangeRate, cost, false, thisCustomer.CurrencyCode, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.Total);

                        if (thisCustomer.IsNotRegistered)
                        {
                            customerCode = thisCustomer.AnonymousCustomerCode;
                        }

                        vat = Interprise.Facade.Customer.ItemTaxFacade.CalculateTax(customerCode,
                                itemCode,
                                unitMeasure.UnitMeasureCode,
                                price,
                                priceRate,
                                cost,
                                costRate,
                                decimal.One);

                        if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            price += vat;
                        }

                        if (unitMeasure.QuantityRanges.Count < 2) break;
                    }

                    # endregion Get Price

                    #region Price

                    output.Append("    <div class='entity-item-price-range'>");
                    output.AppendFormat("       From {0}", price.ToCustomerCurrency());

                    if (AppLogic.AppConfigBool("VAT.Enabled"))
                    {
                        if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            output.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.38") + "</span>\n");
                        }
                        else
                        {
                            output.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.37") + "</span>\n");
                        }
                    }
                    output.Append("    </div>");

                    finalPrice = price;

                    #endregion Price


                    string uom = unitMeasure.UnitMeasureDescription;
                    if (uom.ToLower() == "ctn" || uom.ToLower() == "carton")
                    {
                        uom = (quantityRange.MinQuantity > 1) ? "Cartons" : "Carton";
                    }

                    // the quanty range columns
                    # region the quanty range column
                    output.Append("    <div class='entity-item-unit-quantity'>");
                    output.Append("        per bag*");
                    //if (ctr + 1 == unitMeasure.QuantityRanges.Count && unitMeasure.QuantityRanges.Count > 1)
                    //{
                    //    output.AppendFormat("        {0}+{1}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                    //}
                    //else
                    //{
                    //    if (ctr == 0)//if (quantityRange.MinQuantity == 1 && quantityRange.MaxQuantity == 2)
                    //    {
                    //        output.AppendFormat("        {0} {1}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                    //    }
                    //    else
                    //    {
                    //        output.AppendFormat("        {0} - {1} {2}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), Localization.ParseLocaleDecimal(quantityRange.MaxQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                    //    }

                    //}
                    output.Append("    </div>");

                    # endregion the quanty range column

                    if (unitMeasure.QuantityRanges.Count < 2)
                    {
                        finalPrice = finalPrice / quantityRange.MinQuantity;
                        break;
                    }
                }


                // the Unit Measure row Header
                # region Unit Measure row Header
                if (unitMeasure.UnitMeasureQuantity >= 1)
                {
                    decimal pricePer = (finalPrice / (unitMeasure.UnitMeasureQuantity)).ToCustomerRoundedCurrency();
                    string pricePerString = pricePer.ToCustomerCurrency();

                    if (pricePer < 1)
                    {
                        var arrayStr = pricePer.ToString().Split('.');
                        if (arrayStr.Length == 2) { pricePerString = arrayStr[1] + "¢"; }
                        else if (arrayStr.Length == 1) { pricePerString = arrayStr[0] + "¢"; }
                        else { pricePerString = pricePer.ToCustomerCurrency(); }
                    }


                    string uom = unitMeasure.UnitMeasureDescription;
                    if (uom.ToLower() == "ctn")
                    {
                        uom = "Carton";
                    }

                    output.Append("<div class='hidden-item-per-price' id='hidden-item-per-price_" + itemCode + "'>" + HttpUtility.HtmlEncode(pricePerString) + "</div>");
                    output.Append("<div class='express-print-text'>" + itemWebOption.PrintInfo + "</div>");
                    //output.Append("<div class='entity-item-unit-measure-base'>");
                    //output.AppendFormat("        {1}&nbsp;per&nbsp;{0}", HttpUtility.HtmlEncode(uom), HttpUtility.HtmlEncode(unitMeasure.UnitMeasureQuantity.ToNumberFormat()));
                    //output.Append("    </div>");
                }
                # endregion Unit Selling Unit Measure row Header

            }

            response = output.ToString();
        }

        // Customization


        //response = InterpriseHelper.GetInventoryPricingLevelTable(Customer.Current, itemCode, out hasPricingLevel);

        if (!hasPricingLevel)
        {
            return string.Empty;
        }

        return response;
    }

    public string GetPricingLevel(string itemCode)
    {
        itemCode = HttpUtility.UrlDecode(itemCode);

        Customer thisCustomer = Customer.Current;
        string response = String.Empty;

        //Do not execute code if pricing level is not applicable
        if (thisCustomer.PricingLevel == String.Empty)
        {
            return response;
        }
        bool hasPricingLevel;

        // Customization

        var priceList = new PriceListData(thisCustomer.PricingLevel);

        int quantityDecimalPlaces = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetInventoryDecimalPlacesPreference();

        var itemDefaultUM = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);

        hasPricingLevel = false;

        string customerCode = CommonLogic.IIF(thisCustomer.IsNotRegistered, thisCustomer.AnonymousCustomerCode, thisCustomer.CustomerCode);
        // Get the pricing table discounts...

        using (var con = DB.NewSqlConnection())
        {
            string unitMeasureCode = string.Empty;
            string unitMeasureDescription = string.Empty;
            decimal unitMeasureQuantity;
            string currencyCode = string.Empty;
            decimal minQuantity;
            decimal maxQuantity;
            decimal discount;
            decimal unitSellingPrice;

            con.Open();
            using (var reader = DB.GetRSFormat(con, "exec eCommerceGetItemPricingLevel @ItemCode = {0}, @CustomerCode = {1}", DB.SQuote(itemCode), DB.SQuote(customerCode)))
            {
                while (reader.Read())
                {
                    hasPricingLevel = true;

                    unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                    unitMeasureDescription = DB.RSField(reader, "UnitMeasureDescription");
                    unitMeasureQuantity = DB.RSFieldDecimal(reader, "UnitMeasureQty");
                    currencyCode = DB.RSField(reader, "CurrencyCode");
                    minQuantity = DB.RSFieldDecimal(reader, "MinQuantity");
                    maxQuantity = DB.RSFieldDecimal(reader, "MaxQuantity");
                    discount = DB.RSFieldDecimal(reader, "Discount");
                    unitSellingPrice = DB.RSFieldDecimal(reader, "SalesPrice");

                    PriceListUnitMeasureData unitMeasure;

                    if (!priceList.UnitMeasures.ContainsKey(unitMeasureCode))
                    {
                        priceList.UnitMeasures.Add(unitMeasureCode, new PriceListUnitMeasureData(unitMeasureCode, unitMeasureDescription, unitMeasureQuantity));
                    }

                    unitMeasure = priceList.UnitMeasures[unitMeasureCode];

                    var quantityRange = new PriceListQuantityRangeData()
                    {
                        MinQuantity = minQuantity,
                        MaxQuantity = maxQuantity,
                        Discount = discount,
                        UnitSellingPrice = unitSellingPrice
                    };

                    unitMeasure.QuantityRanges.Add(quantityRange);
                }
            }
        }

        // render the html
        var output = new StringBuilder();

        if (priceList.UnitMeasures.Count > 0)
        {
            // render the child items..
            foreach (PriceListUnitMeasureData unitMeasure in priceList.UnitMeasures.Values)
            {
                if (unitMeasure.UnitMeasureCode != itemDefaultUM.Code) continue;
                decimal finalPrice = 1;
                for (int ctr = 0; ctr < 2; ctr++)
                {

                    var quantityRange = unitMeasure.QuantityRanges[ctr];

                    # region Get Price

                    decimal price = quantityRange.UnitSellingPrice;

                    if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                    {
                        decimal priceRate, cost, costRate, vat;
                        priceRate = price;
                        string currencyCode = string.Empty;
                        bool byTotalQuantity = false;
                        decimal x_salesPrice = decimal.Zero;
                        bool isSalesPriceInBasecurrency = false;
                        decimal regularPrice = decimal.Zero;
                        decimal promotionalPrice = decimal.Zero;
                        string pricing = string.Empty;
                        decimal percent = decimal.Zero;
                        decimal discount = decimal.Zero;
                        decimal categoryDiscount = decimal.Zero;
                        string customerItemCode = string.Empty;
                        string customerItemDescription = string.Empty;
                        string inventoryItemDescription = string.Empty;
                        decimal basePricingCost = decimal.Zero;
                        decimal baseAverageCost = decimal.Zero;
                        bool isInventorySpecialPriceExpired = false;
                        bool isCustomerSpecialPriceExpired = false;

                        Interprise.Facade.Customer.BaseSalesOrderFacade.GetPrice(
                                string.Empty,
                                ref currencyCode,
                                itemCode,
                                unitMeasure.UnitMeasureCode,
                                unitMeasure.UnitMeasureQuantity,
                                DateTime.Today,
                                decimal.Zero,
                                decimal.Zero,
                                Interprise.Framework.Base.Shared.Enum.DefaultPricing.None,
                                Interprise.Framework.Base.Shared.Enum.PricingMethod.None,
                                string.Empty,
                                ref byTotalQuantity,
                                ref x_salesPrice,
                                ref isSalesPriceInBasecurrency,
                                ref regularPrice,
                                ref promotionalPrice,
                                ref pricing,
                                ref percent,
                                ref discount,
                                ref categoryDiscount,
                                ref customerItemCode,
                                ref customerItemDescription,
                                ref inventoryItemDescription,
                                ref basePricingCost,
                                ref baseAverageCost,
                                ref isInventorySpecialPriceExpired,
                                ref isCustomerSpecialPriceExpired);

                        cost = baseAverageCost;
                        decimal exchangeRate = Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(thisCustomer.CurrencyCode);
                        costRate = Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(exchangeRate, cost, false, thisCustomer.CurrencyCode, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.Total);

                        if (thisCustomer.IsNotRegistered)
                        {
                            customerCode = thisCustomer.AnonymousCustomerCode;
                        }

                        vat = Interprise.Facade.Customer.ItemTaxFacade.CalculateTax(customerCode,
                                itemCode,
                                unitMeasure.UnitMeasureCode,
                                price,
                                priceRate,
                                cost,
                                costRate,
                                decimal.One);

                        if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            price += vat;
                        }

                        if (unitMeasure.QuantityRanges.Count < 2) break;
                    }

                    # endregion Get Price

                    #region Price

                    output.Append("    <div class='entity-item-price-range'>");
                    output.AppendFormat("        {0}", price.ToCustomerCurrency());

                    if (AppLogic.AppConfigBool("VAT.Enabled"))
                    {
                        if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            output.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.38") + "</span>\n");
                        }
                        else
                        {
                            output.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.37") + "</span>\n");
                        }
                    }
                    output.Append("    </div>");

                    finalPrice = price;

                    #endregion Price

                    string uom = unitMeasure.UnitMeasureDescription;
                    if (uom.ToLower() == "ctn" || uom.ToLower() == "carton")
                    {
                        uom = (quantityRange.MinQuantity > 1) ? "Cartons" : "Carton";
                    }

                    // the quanty range columns
                    # region the quanty range column
                    output.Append("    <div class='entity-item-unit-quantity'>");
                    if (ctr + 1 == unitMeasure.QuantityRanges.Count && unitMeasure.QuantityRanges.Count > 1)
                    {
                        output.AppendFormat("        {0}+{1}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                    }
                    else
                    {
                        if (ctr == 0)//if (quantityRange.MinQuantity == 1 && quantityRange.MaxQuantity == 2)
                        {
                            output.AppendFormat("        {0} {1}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                        }
                        else
                        {
                            output.AppendFormat("        {0} - {1} {2}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), Localization.ParseLocaleDecimal(quantityRange.MaxQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                        }

                    }
                    output.Append("    </div>");

                    # endregion the quanty range column

                    if (unitMeasure.QuantityRanges.Count < 2) break;
                }


                // the Unit Measure row Header
                # region Unit Measure row Header
                if (unitMeasure.UnitMeasureQuantity >= 1)
                {
                    decimal pricePer = (finalPrice / unitMeasure.UnitMeasureQuantity).ToCustomerRoundedCurrency();
                    string pricePerString = pricePer.ToCustomerCurrency();
                    if (pricePer < 1)
                    {
                        var arrayStr = pricePer.ToString().Split('.');
                        if (arrayStr.Length == 2) { pricePerString = arrayStr[1] + "¢"; }
                        else if (arrayStr.Length == 1) { pricePerString = arrayStr[0] + "¢"; }
                        else { pricePerString = pricePer.ToCustomerCurrency(); }
                    }

                    string uom = unitMeasure.UnitMeasureDescription;
                    if (uom.ToLower() == "ctn")
                    {
                        uom = "Carton";
                    }


                    output.Append("<div class='hidden-item-per-price' id='hidden-item-per-price_" + itemCode + "'>" + HttpUtility.HtmlEncode(pricePerString) + "</div>");
                    output.Append("<div class='entity-item-unit-measure-base'>");
                    output.AppendFormat("        {1}&nbsp;per&nbsp;{0}", HttpUtility.HtmlEncode(uom), HttpUtility.HtmlEncode(unitMeasure.UnitMeasureQuantity.ToNumberFormat()));
                    output.Append("    </div>");
                }
                # endregion Unit Selling Unit Measure row Header

            }

            response = output.ToString();
        }

        // Customization


        //response = InterpriseHelper.GetInventoryPricingLevelTable(Customer.Current, itemCode, out hasPricingLevel);

        if (!hasPricingLevel)
        {
            return string.Empty;
        }

        return response;
    }

    private PriceListData GetProductPricingLevelData(string itemCode)
    {
        Customer thisCustomer = Customer.Current;
        string response = String.Empty;
        var priceList = new PriceListData(thisCustomer.PricingLevel);
        //Do not execute code if pricing level is not applicable
        if (thisCustomer.PricingLevel == String.Empty)
        {
            return priceList;
        }

        // Customization
        int quantityDecimalPlaces = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetInventoryDecimalPlacesPreference();



        string customerCode = CommonLogic.IIF(thisCustomer.IsNotRegistered, thisCustomer.AnonymousCustomerCode, thisCustomer.CustomerCode);
        // Get the pricing table discounts...

        using (var con = DB.NewSqlConnection())
        {
            string unitMeasureCode = string.Empty;
            string unitMeasureDescription = string.Empty;
            decimal unitMeasureQuantity;
            string currencyCode = string.Empty;
            decimal minQuantity;
            decimal maxQuantity;
            decimal discount;
            decimal unitSellingPrice;

            con.Open();
            using (var reader = DB.GetRSFormat(con, "exec eCommerceGetItemPricingLevel @ItemCode = {0}, @CustomerCode = {1}", DB.SQuote(itemCode), DB.SQuote(customerCode)))
            {
                while (reader.Read())
                {
                    unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                    unitMeasureDescription = DB.RSField(reader, "UnitMeasureDescription");
                    unitMeasureQuantity = DB.RSFieldDecimal(reader, "UnitMeasureQty");
                    currencyCode = DB.RSField(reader, "CurrencyCode");
                    minQuantity = DB.RSFieldDecimal(reader, "MinQuantity");
                    maxQuantity = DB.RSFieldDecimal(reader, "MaxQuantity");
                    discount = DB.RSFieldDecimal(reader, "Discount");
                    unitSellingPrice = DB.RSFieldDecimal(reader, "SalesPrice");

                    PriceListUnitMeasureData unitMeasure;

                    if (!priceList.UnitMeasures.ContainsKey(unitMeasureCode))
                    {
                        priceList.UnitMeasures.Add(unitMeasureCode, new PriceListUnitMeasureData(unitMeasureCode, unitMeasureDescription, unitMeasureQuantity));
                    }

                    unitMeasure = priceList.UnitMeasures[unitMeasureCode];

                    var quantityRange = new PriceListQuantityRangeData()
                    {
                        MinQuantity = minQuantity,
                        MaxQuantity = maxQuantity,
                        Discount = discount,
                        UnitSellingPrice = unitSellingPrice
                    };

                    unitMeasure.QuantityRanges.Add(quantityRange);
                }
            }
        }
        return priceList;
    }

    public string GetProductPricingLevel(string itemCode)
    {
        itemCode = HttpUtility.UrlDecode(itemCode);

        Customer thisCustomer = Customer.Current;
        string response = String.Empty;

        //Do not execute code if pricing level is not applicable
        if (thisCustomer.PricingLevel == String.Empty)
        {
            return response;
        }
        bool hasPricingLevel;

        // Customization

        var priceList = new PriceListData(thisCustomer.PricingLevel);

        int quantityDecimalPlaces = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetInventoryDecimalPlacesPreference();

        var itemDefaultUM = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);

        hasPricingLevel = false;

        string customerCode = CommonLogic.IIF(thisCustomer.IsNotRegistered, thisCustomer.AnonymousCustomerCode, thisCustomer.CustomerCode);
        // Get the pricing table discounts...

        using (var con = DB.NewSqlConnection())
        {
            string unitMeasureCode = string.Empty;
            string unitMeasureDescription = string.Empty;
            decimal unitMeasureQuantity;
            string currencyCode = string.Empty;
            decimal minQuantity;
            decimal maxQuantity;
            decimal discount;
            decimal unitSellingPrice;

            con.Open();
            using (var reader = DB.GetRSFormat(con, "exec eCommerceGetItemPricingLevel @ItemCode = {0}, @CustomerCode = {1}", DB.SQuote(itemCode), DB.SQuote(customerCode)))
            {
                while (reader.Read())
                {
                    hasPricingLevel = true;

                    unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                    unitMeasureDescription = DB.RSField(reader, "UnitMeasureDescription");
                    unitMeasureQuantity = DB.RSFieldDecimal(reader, "UnitMeasureQty");
                    currencyCode = DB.RSField(reader, "CurrencyCode");
                    minQuantity = DB.RSFieldDecimal(reader, "MinQuantity");
                    maxQuantity = DB.RSFieldDecimal(reader, "MaxQuantity");
                    discount = DB.RSFieldDecimal(reader, "Discount");
                    unitSellingPrice = DB.RSFieldDecimal(reader, "SalesPrice");

                    PriceListUnitMeasureData unitMeasure;

                    if (!priceList.UnitMeasures.ContainsKey(unitMeasureCode))
                    {
                        priceList.UnitMeasures.Add(unitMeasureCode, new PriceListUnitMeasureData(unitMeasureCode, unitMeasureDescription, unitMeasureQuantity));
                    }

                    unitMeasure = priceList.UnitMeasures[unitMeasureCode];

                    var quantityRange = new PriceListQuantityRangeData()
                    {
                        MinQuantity = minQuantity,
                        MaxQuantity = maxQuantity,
                        Discount = discount,
                        UnitSellingPrice = unitSellingPrice
                    };

                    unitMeasure.QuantityRanges.Add(quantityRange);
                }
            }


        }

        // render the html
        var output = new StringBuilder();

        if (priceList.UnitMeasures.Count > 0)
        {


            // render the child items..
            foreach (PriceListUnitMeasureData unitMeasure in priceList.UnitMeasures.Values)
            {
                if (unitMeasure.UnitMeasureCode != itemDefaultUM.Code) continue;
                decimal finalPrice = 1;

                output.Append("    <ul>");

                for (int ctr = 0; ctr < 2; ctr++)
                {
                    var quantityRange = unitMeasure.QuantityRanges[ctr];

                    # region Get Price

                    decimal price = quantityRange.UnitSellingPrice;

                    if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                    {
                        decimal priceRate, cost, costRate, vat;
                        priceRate = price;
                        string currencyCode = string.Empty;
                        bool byTotalQuantity = false;
                        decimal x_salesPrice = decimal.Zero;
                        bool isSalesPriceInBasecurrency = false;
                        decimal regularPrice = decimal.Zero;
                        decimal promotionalPrice = decimal.Zero;
                        string pricing = string.Empty;
                        decimal percent = decimal.Zero;
                        decimal discount = decimal.Zero;
                        decimal categoryDiscount = decimal.Zero;
                        string customerItemCode = string.Empty;
                        string customerItemDescription = string.Empty;
                        string inventoryItemDescription = string.Empty;
                        decimal basePricingCost = decimal.Zero;
                        decimal baseAverageCost = decimal.Zero;
                        bool isInventorySpecialPriceExpired = false;
                        bool isCustomerSpecialPriceExpired = false;

                        Interprise.Facade.Customer.BaseSalesOrderFacade.GetPrice(
                                string.Empty,
                                ref currencyCode,
                                itemCode,
                                unitMeasure.UnitMeasureCode,
                                unitMeasure.UnitMeasureQuantity,
                                DateTime.Today,
                                decimal.Zero,
                                decimal.Zero,
                                Interprise.Framework.Base.Shared.Enum.DefaultPricing.None,
                                Interprise.Framework.Base.Shared.Enum.PricingMethod.None,
                                string.Empty,
                                ref byTotalQuantity,
                                ref x_salesPrice,
                                ref isSalesPriceInBasecurrency,
                                ref regularPrice,
                                ref promotionalPrice,
                                ref pricing,
                                ref percent,
                                ref discount,
                                ref categoryDiscount,
                                ref customerItemCode,
                                ref customerItemDescription,
                                ref inventoryItemDescription,
                                ref basePricingCost,
                                ref baseAverageCost,
                                ref isInventorySpecialPriceExpired,
                                ref isCustomerSpecialPriceExpired);

                        cost = baseAverageCost;
                        decimal exchangeRate = Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(thisCustomer.CurrencyCode);
                        costRate = Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(exchangeRate, cost, false, thisCustomer.CurrencyCode, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.Total);

                        if (thisCustomer.IsNotRegistered)
                        {
                            customerCode = thisCustomer.AnonymousCustomerCode;
                        }

                        vat = Interprise.Facade.Customer.ItemTaxFacade.CalculateTax(customerCode,
                                itemCode,
                                unitMeasure.UnitMeasureCode,
                                price,
                                priceRate,
                                cost,
                                costRate,
                                decimal.One);

                        if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            price += vat;
                        }
                    }

                    # endregion Get Price

                    string uom = unitMeasure.UnitMeasureDescription;
                    if (uom.ToLower() == "ctn" || uom.ToLower() == "carton")
                    {
                        uom = (quantityRange.MinQuantity > 1) ? "Cartons" : "Carton";
                    }

                    output.Append("<li class='li-price-range'>    <div class='product-price-range'>");
                    // the quanty range columns
                    # region the quanty range column
                    output.Append("    <div class='entity-item-unit-quantity'>");
                    if (ctr + 1 == unitMeasure.QuantityRanges.Count)
                    {
                        output.AppendFormat("        {0}+{1}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                    }
                    else
                    {
                        if (ctr == 0)//if (quantityRange.MinQuantity == 1 && quantityRange.MaxQuantity == 2)
                        {
                            output.AppendFormat("        {0} {1}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                        }
                        else
                        {
                            output.AppendFormat("        {0} - {1} {2}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), Localization.ParseLocaleDecimal(quantityRange.MaxQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                        }

                    }
                    output.Append("    </div>");

                    # endregion the quanty range column

                    #region Price

                    output.Append("    <div class='entity-item-price-range'>");
                    output.AppendFormat("        {0} <span class='entity-item-price-range-each'>EACH</span>", price.ToCustomerCurrency());

                    if (AppLogic.AppConfigBool("VAT.Enabled"))
                    {
                        if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            output.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.38") + "</span>\n");
                        }
                        else
                        {
                            output.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.37") + "</span>\n");
                        }
                    }
                    output.Append("    </div>");

                    finalPrice = price;

                    #endregion Price

                    #region "PerItem"

                    decimal pricePer = (price / unitMeasure.UnitMeasureQuantity).ToCustomerRoundedCurrency();
                    string pricePerString = pricePer.ToCustomerCurrency();
                    if (pricePer > 0)
                    {
                        var arrayStr = pricePer.ToString().Split('.');
                        if (arrayStr.Length == 2) { pricePerString = "*." + arrayStr[1] + "¢"; }
                        else if (arrayStr.Length == 1) { pricePerString = "*." + arrayStr[0] + "¢"; }
                        else { pricePerString = pricePer.ToCustomerCurrency(); }
                    }

                    output.Append("    <div class='entity-item-price-per'>");
                    output.Append(String.Format("{0} per {1}*", pricePerString, unitMeasure.UnitMeasureDescription));
                    output.Append("    </div>");

                    #endregion "PerItem"

                    output.Append("    </div> </li>");

                }

                output.Append("    </ul>");

            }

            response = output.ToString();
        }

        // Customization


        //response = InterpriseHelper.GetInventoryPricingLevelTable(Customer.Current, itemCode, out hasPricingLevel);

        if (!hasPricingLevel)
        {
            return string.Empty;
        }

        return response;
    }

    public string GetPricingLevelStickers(string itemCode)
    {
        itemCode = HttpUtility.UrlDecode(itemCode);

        Customer thisCustomer = Customer.Current;
        string response = String.Empty;

        //Do not execute code if pricing level is not applicable
        if (thisCustomer.PricingLevel == String.Empty)
        {
            return response;
        }
        bool hasPricingLevel;

        // Customization

        var priceList = new PriceListData(thisCustomer.PricingLevel);

        int quantityDecimalPlaces = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetInventoryDecimalPlacesPreference();

        var itemDefaultUM = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);

        hasPricingLevel = false;

        string customerCode = CommonLogic.IIF(thisCustomer.IsNotRegistered, thisCustomer.AnonymousCustomerCode, thisCustomer.CustomerCode);
        // Get the pricing table discounts...

        using (var con = DB.NewSqlConnection())
        {
            string unitMeasureCode = string.Empty;
            string unitMeasureDescription = string.Empty;
            decimal unitMeasureQuantity;
            string currencyCode = string.Empty;
            decimal minQuantity;
            decimal maxQuantity;
            decimal discount;
            decimal unitSellingPrice;

            con.Open();
            using (var reader = DB.GetRSFormat(con, "exec eCommerceGetItemPricingLevel @ItemCode = {0}, @CustomerCode = {1}", DB.SQuote(itemCode), DB.SQuote(customerCode)))
            {
                while (reader.Read())
                {
                    hasPricingLevel = true;

                    unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                    unitMeasureDescription = DB.RSField(reader, "UnitMeasureDescription");
                    unitMeasureQuantity = DB.RSFieldDecimal(reader, "UnitMeasureQty");
                    currencyCode = DB.RSField(reader, "CurrencyCode");
                    minQuantity = DB.RSFieldDecimal(reader, "MinQuantity");
                    maxQuantity = DB.RSFieldDecimal(reader, "MaxQuantity");
                    discount = DB.RSFieldDecimal(reader, "Discount");
                    unitSellingPrice = DB.RSFieldDecimal(reader, "SalesPrice");

                    PriceListUnitMeasureData unitMeasure;

                    if (!priceList.UnitMeasures.ContainsKey(unitMeasureCode))
                    {
                        priceList.UnitMeasures.Add(unitMeasureCode, new PriceListUnitMeasureData(unitMeasureCode, unitMeasureDescription, unitMeasureQuantity));
                    }

                    unitMeasure = priceList.UnitMeasures[unitMeasureCode];

                    var quantityRange = new PriceListQuantityRangeData()
                    {
                        MinQuantity = minQuantity,
                        MaxQuantity = maxQuantity,
                        Discount = discount,
                        UnitSellingPrice = unitSellingPrice
                    };

                    unitMeasure.QuantityRanges.Add(quantityRange);
                }
            }
        }

        var productInfo = ServiceFactory.GetInstance<IProductService>().GetProductInfoViewForShowProduct(itemCode);
        if (productInfo != null && productInfo.ItemType.EqualsIgnoreCase("kit") && priceList.UnitMeasures.Count == 0)
        {
            var customer = ServiceFactory.GetInstance<IAuthenticationService>()
                                         .GetCurrentLoggedInCustomer();

            PriceListUnitMeasureData unitMeasure;

            if (!priceList.UnitMeasures.ContainsKey(itemDefaultUM.Code))
            {

                using (SqlConnection con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (IDataReader reader = DB.GetRSFormat(con, "SELECT UnitMeasureDescription, UnitMeasureQuantity FROM SystemUnitMeasure with (NOLOCK) WHERE UnitMeasureCode = {0}", DB.SQuote(itemDefaultUM.Code)))
                    {
                        if (reader.Read())
                        {
                            hasPricingLevel = true;
                            priceList.UnitMeasures.Add(itemDefaultUM.Code, new PriceListUnitMeasureData(itemDefaultUM.Code, DB.RSField(reader, "UnitMeasureDescription"), DB.RSFieldDecimal(reader, "UnitMeasureQuantity")));
                        }

                    }
                }
            }

            unitMeasure = priceList.UnitMeasures[itemDefaultUM.Code];
            decimal kitDiscount = ServiceFactory.GetInstance<IProductService>().GetKitDiscount(itemCode);

            decimal minQuantity = 1, maxQuantity = 1;

            var itemWebOption = ServiceFactory.GetInstance<IProductService>().GetWebOption(itemCode);
            minQuantity = itemWebOption.MinOrderQuantity == null || itemWebOption.MinOrderQuantity == 0 ? 1 : itemWebOption.MinOrderQuantity;
            maxQuantity = itemWebOption.RestrictedQuantities.Count == 0 ? 0 : itemWebOption.RestrictedQuantities.Max();

            maxQuantity = maxQuantity == null || maxQuantity == 0 ? minQuantity : maxQuantity;
            decimal unitSellingPrice = InterpriseHelper.InventoryKitPackagePrice(itemCode, customer.CurrencyCode).ToDecimal() * (unitMeasure.UnitMeasureQuantity * minQuantity);
            var quantityRange = new PriceListQuantityRangeData()
            {
                MinQuantity = minQuantity,
                MaxQuantity = minQuantity,
                Discount = kitDiscount,
                UnitSellingPrice = unitSellingPrice
            };

            unitMeasure.QuantityRanges.Add(quantityRange);

        }

        // render the html
        var output = new StringBuilder();

        if (priceList.UnitMeasures.Count > 0)
        {
            // render the child items..
            foreach (PriceListUnitMeasureData unitMeasure in priceList.UnitMeasures.Values)
            {
                if (unitMeasure.UnitMeasureCode != itemDefaultUM.Code) continue;
                decimal finalPrice = 1;
                for (int ctr = 0; ctr < 2; ctr++)
                {

                    var quantityRange = unitMeasure.QuantityRanges[ctr];

                    # region Get Price

                    decimal price = quantityRange.UnitSellingPrice;

                    if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                    {
                        decimal priceRate, cost, costRate, vat;
                        priceRate = price;
                        string currencyCode = string.Empty;
                        bool byTotalQuantity = false;
                        decimal x_salesPrice = decimal.Zero;
                        bool isSalesPriceInBasecurrency = false;
                        decimal regularPrice = decimal.Zero;
                        decimal promotionalPrice = decimal.Zero;
                        string pricing = string.Empty;
                        decimal percent = decimal.Zero;
                        decimal discount = decimal.Zero;
                        decimal categoryDiscount = decimal.Zero;
                        string customerItemCode = string.Empty;
                        string customerItemDescription = string.Empty;
                        string inventoryItemDescription = string.Empty;
                        decimal basePricingCost = decimal.Zero;
                        decimal baseAverageCost = decimal.Zero;
                        bool isInventorySpecialPriceExpired = false;
                        bool isCustomerSpecialPriceExpired = false;

                        Interprise.Facade.Customer.BaseSalesOrderFacade.GetPrice(
                                string.Empty,
                                ref currencyCode,
                                itemCode,
                                unitMeasure.UnitMeasureCode,
                                unitMeasure.UnitMeasureQuantity,
                                DateTime.Today,
                                decimal.Zero,
                                decimal.Zero,
                                Interprise.Framework.Base.Shared.Enum.DefaultPricing.None,
                                Interprise.Framework.Base.Shared.Enum.PricingMethod.None,
                                string.Empty,
                                ref byTotalQuantity,
                                ref x_salesPrice,
                                ref isSalesPriceInBasecurrency,
                                ref regularPrice,
                                ref promotionalPrice,
                                ref pricing,
                                ref percent,
                                ref discount,
                                ref categoryDiscount,
                                ref customerItemCode,
                                ref customerItemDescription,
                                ref inventoryItemDescription,
                                ref basePricingCost,
                                ref baseAverageCost,
                                ref isInventorySpecialPriceExpired,
                                ref isCustomerSpecialPriceExpired);

                        cost = baseAverageCost;
                        decimal exchangeRate = Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(thisCustomer.CurrencyCode);
                        costRate = Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(exchangeRate, cost, false, thisCustomer.CurrencyCode, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.Total);

                        if (thisCustomer.IsNotRegistered)
                        {
                            customerCode = thisCustomer.AnonymousCustomerCode;
                        }

                        vat = Interprise.Facade.Customer.ItemTaxFacade.CalculateTax(customerCode,
                                itemCode,
                                unitMeasure.UnitMeasureCode,
                                price,
                                priceRate,
                                cost,
                                costRate,
                                decimal.One);

                        if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            price += vat;
                        }

                        if (unitMeasure.QuantityRanges.Count < 2) break;
                    }

                    # endregion Get Price

                    #region Price

                    output.Append("    <div class='entity-item-price-range'>");
                    output.AppendFormat("        {0}", price.ToCustomerCurrency());

                    if (AppLogic.AppConfigBool("VAT.Enabled"))
                    {
                        if (thisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            output.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.38") + "</span>\n");
                        }
                        else
                        {
                            output.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.37") + "</span>\n");
                        }
                    }
                    output.Append("    </div>");

                    finalPrice = price;

                    #endregion Price


                    string uom = unitMeasure.UnitMeasureDescription;
                    if (uom.ToLower() == "ctn" || uom.ToLower() == "carton")
                    {
                        uom = (quantityRange.MinQuantity > 1) ? "Cartons" : "Carton";
                    }

                    // the quanty range columns
                    # region the quanty range column
                    output.Append("    <div class='entity-item-unit-quantity-stickers'>");
                    output.AppendFormat("    {0} per {1}", Localization.ParseLocaleDecimal(unitMeasure.UnitMeasureQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                    //if (ctr + 1 == unitMeasure.QuantityRanges.Count && unitMeasure.QuantityRanges.Count > 1)
                    //{
                    //    output.AppendFormat("        {0}+{1}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                    //}
                    //else
                    //{
                    //    if (ctr == 0)
                    //    {
                    //        output.AppendFormat("        {0} {1}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                    //    }
                    //    else
                    //    {
                    //        output.AppendFormat("        {0} - {1} {2}", Localization.ParseLocaleDecimal(quantityRange.MinQuantity, Customer.Current.LocaleSetting), Localization.ParseLocaleDecimal(quantityRange.MaxQuantity, Customer.Current.LocaleSetting), HttpUtility.HtmlEncode(uom));
                    //    }

                    //}
                    output.Append("    </div>");

                    # endregion the quanty range column

                    if (unitMeasure.QuantityRanges.Count < 2)
                    {
                        finalPrice = finalPrice / quantityRange.MinQuantity;
                        break;
                    }
                }


                // the Unit Measure row Header
                # region Unit Measure row Header
                if (unitMeasure.UnitMeasureQuantity >= 1)
                {
                    decimal pricePer = (finalPrice / (unitMeasure.UnitMeasureQuantity)).ToCustomerRoundedCurrency();
                    string pricePerString = pricePer.ToCustomerCurrency();

                    if (pricePer < 1)
                    {
                        var arrayStr = pricePer.ToString().Split('.');
                        if (arrayStr.Length == 2) { pricePerString = arrayStr[1] + "¢"; }
                        else if (arrayStr.Length == 1) { pricePerString = arrayStr[0] + "¢"; }
                        else { pricePerString = pricePer.ToCustomerCurrency(); }
                    }

                    string uom = unitMeasure.UnitMeasureDescription;
                    if (uom.ToLower() == "ctn")
                    {
                        uom = "Carton";
                    }

                    output.Append("<div class='hidden-item-per-price' id='hidden-item-per-price_" + itemCode + "'>" + HttpUtility.HtmlEncode(pricePerString) + "</div>");
                    //output.Append("<div class='entity-item-unit-measure-base'>");
                    //output.AppendFormat("        {1}&nbsp;per&nbsp;{0}", HttpUtility.HtmlEncode(uom), HttpUtility.HtmlEncode(unitMeasure.UnitMeasureQuantity.ToNumberFormat()));
                    //output.Append("    </div>");
                }
                # endregion Unit Selling Unit Measure row Header

            }

            response = output.ToString();
        }

        if (!hasPricingLevel)
        {
            return string.Empty;
        }

        return response;
    }

    internal struct ItemUnitMeasuresData
    {
        internal string ItemCode;
        internal string UnitMeasureCode;
        internal decimal Length;
        internal decimal Width;
        internal decimal Height;
        internal decimal Quantity;
        internal string Description;
    }

    public virtual void LoadBatchItemUnitMeasures(XPathNodeIterator MLContent)
    {

        var codes = new List<string>();
        var ids = new List<string>();
        string str = string.Empty;

        while (MLContent.MoveNext())
        {
            var currentNode = MLContent.Current;
            string itemCode = currentNode.SelectSingleNode("./ItemCode").InnerXml;
            string counter = currentNode.SelectSingleNode("./Counter").InnerXml;

            if (!itemCode.IsNullOrEmptyTrimmed() && !codes.Any(c => c == itemCode))
            {
                codes.Add(itemCode);
            }

        }

        if (codes.Count() > 0)
        {
            string itemCodes = String.Empty;
            if (codes.Count() > 0) { itemCodes = String.Join(",", codes.Select(c => c.ToDbQuote())); }
            else { itemCodes = "NULL"; }

            using (var con = DB.NewSqlConnection())
            {
                string itemcode = string.Empty;
                string unitMeasureCode = string.Empty;
                decimal length;
                decimal width;
                decimal height;
                con.Open();
                str = String.Format("Select ItemCode, UnitMeasureCode, ISNULL(LengthInCentimeters,0) * 10 AS LengthInMillimeters, ISNULL(WidthInCentimeters,0) * 10 AS WidthInMillimeters, ISNULL(HeightInCentimeters,0) * 10 AS HeightInMillimeters from InventoryUnitMeasure where DefaultSelling = 1 AND ItemCode IN ({0})", itemCodes);
                using (var reader = DB.GetRSFormat(con, str))
                {
                    while (reader.Read())
                    {
                        itemcode = DB.RSField(reader, "ItemCode");
                        unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                        length = DB.RSFieldDecimal(reader, "LengthInMillimeters");
                        width = DB.RSFieldDecimal(reader, "WidthInMillimeters");
                        height = DB.RSFieldDecimal(reader, "HeightInMillimeters");

                        if (unitMeasures.Count > 0)
                        {
                            if (unitMeasures.Where(x => x.ItemCode == itemcode && x.UnitMeasureCode == unitMeasureCode).Count() > 0) continue;
                        }

                        unitMeasures.Add(new ItemUnitMeasuresData()
                        {
                            ItemCode = itemcode,
                            UnitMeasureCode = unitMeasureCode,
                            Length = length,
                            Width = width,
                            Height = height,
                        });

                    }
                }
            }
        }
    }

    public string DisplayItemDimensions(string itemCode)
    {
        string returnString = String.Empty;

        ItemUnitMeasuresData itemUnitMeasure = unitMeasures.FirstOrDefault(x => x.ItemCode.EqualsIgnoreCase(itemCode));
        if (itemUnitMeasure.Equals(null)) return returnString;


        string dimension_format = "";
        if (itemUnitMeasure.Height > 0)
        {
            dimension_format += " {0}mm (h)";
        }

        if (itemUnitMeasure.Width > 0)
        {
            dimension_format += " x {1}mm (w)";
        }

        if (itemUnitMeasure.Length > 0)
        {
            dimension_format += " x  {2}mm (d)";
        }

        returnString = String.Format("<span> " + dimension_format + " </span>", itemUnitMeasure.Height.ToCustomerRoundedMonetary(), itemUnitMeasure.Width.ToCustomerRoundedMonetary(), itemUnitMeasure.Length.ToCustomerRoundedMonetary());

        return returnString;
    }

    public string DisplayProductDimensions(string itemCode)
    {
        string returnString = String.Empty;
        ItemUnitMeasuresData itemUnitMeasure = new ItemUnitMeasuresData();
        using (var con = DB.NewSqlConnection())
        {
            string itemcode = string.Empty;
            string unitMeasureCode = string.Empty;
            string description = string.Empty;
            decimal qty;
            decimal length;
            decimal width;
            decimal height;
            con.Open();
            string str = String.Format("Select ItemCode, IUM.UnitMeasureCode, IUM.UnitMeasureQty, SU.UnitMeasureDescription, ISNULL(LengthInCentimeters,0) * 10 AS LengthInMillimeters, ISNULL(WidthInCentimeters,0) * 10 AS WidthInMillimeters, ISNULL(HeightInCentimeters,0) * 10 AS HeightInMillimeters from InventoryUnitMeasure IUM LEFT JOIN SystemUnitMeasure SU ON IUM.UnitMeasureCode = SU.UnitMeasureCode where IUM.DefaultSelling = 1 AND IUM.ItemCode IN ({0})", itemCode.ToDbQuote());
            using (var reader = DB.GetRSFormat(con, str))
            {
                while (reader.Read())
                {
                    itemcode = DB.RSField(reader, "ItemCode");
                    unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                    length = DB.RSFieldDecimal(reader, "LengthInMillimeters");
                    width = DB.RSFieldDecimal(reader, "WidthInMillimeters");
                    height = DB.RSFieldDecimal(reader, "HeightInMillimeters");
                    description = DB.RSField(reader, "UnitMeasureDescription");
                    qty = DB.RSFieldDecimal(reader, "UnitMeasureQty");

                    if (unitMeasures.Count > 0)
                    {
                        if (unitMeasures.Where(x => x.ItemCode == itemcode && x.UnitMeasureCode == unitMeasureCode).Count() > 0) continue;
                    }

                    itemUnitMeasure = new ItemUnitMeasuresData()
                    {
                        ItemCode = itemcode,
                        UnitMeasureCode = unitMeasureCode,
                        Length = length,
                        Width = width,
                        Height = height,
                        Description = description,
                        Quantity = qty
                    };

                }
            }
        }


        if (itemUnitMeasure.ItemCode.IsNullOrEmptyTrimmed()) return returnString;

        string dimension_format = "";
        if (itemUnitMeasure.Height > 0)
        {
            dimension_format += " {0}mm(h)";
        }

        if (itemUnitMeasure.Width > 0)
        {
            dimension_format += " x {1}mm(w)";
        }

        if (itemUnitMeasure.Length > 0)
        {
            dimension_format += " x {2}mm(d)";
        }

        returnString = String.Format(
            "<div class='product-dimensions'><span> " + dimension_format + " </span></div><div class='product-per-unit-measure'><span>{4} per {3}</span></div>",
            itemUnitMeasure.Height.ToCustomerRoundedMonetary(), itemUnitMeasure.Width.ToCustomerRoundedMonetary(), itemUnitMeasure.Length.ToCustomerRoundedMonetary(),
             HttpUtility.HtmlEncode(itemUnitMeasure.Description), HttpUtility.HtmlEncode(itemUnitMeasure.Quantity.ToNumberFormat()));

        return returnString;
    }

    public virtual string ProductNavLinks(string sProductID, string sEntityID, string sEntityName, string sEntitySEName, string sSortByLooks, string sUseGraphics, string sIncludeUpLink)
    {

        InterpriseSuiteEcommerceCommon.XSLTExtensionBase.InputValidator IV = new InterpriseSuiteEcommerceCommon.XSLTExtensionBase.InputValidator("ProductNavLinks");
        String ProductID = IV.ValidateString("ProductID", sProductID);
        String EntityID = IV.ValidateString("EntityID", sEntityID);
        String EntityName = IV.ValidateString("EntityName", sEntityName);
        String EntitySEName = IV.ValidateString("EntitySEName", sEntitySEName);
        bool SortByLooks = IV.ValidateBool("SortByLooks", sSortByLooks);
        bool UseGraphics = IV.ValidateBool("UseGraphics", sUseGraphics);
        bool IncludeUpLink = IV.ValidateBool("IncludeUpLink", sIncludeUpLink);
        string result = String.Empty;
        string SEName = String.Empty;
        StringBuilder tmpS = new StringBuilder("");

        var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)HttpContext.Current.User).ThisCustomer;

        if (EntityName.Trim() == "")
        {
            EntityName = "CATEGORY";
        }

        int NumProducts = 0;
        NumProducts = AppLogic.LookupHelper(EntityName).GetNumEntityObjects(EntityID, true, true);
        if (NumProducts > 1)
        {
            string PreviousProductID = AppLogic.GetProductSequence("previous", ProductID, EntityID, EntityName, 0, SortByLooks, true, true, out SEName);
            if (PreviousProductID.TryParseInt().Value > 0)
            {
                tmpS.Append("<a class=\"ProductNavLink\" href=\"" + SE.MakeProductAndEntityLink(EntityName, PreviousProductID, EntityID, SEName) + "\">" + AppLogic.GetString("showproduct.aspx.4").ToHtmlEncode() + "</a>");
            }
        }

        if (NumProducts > 1)
        {
            string NextProductID = AppLogic.GetProductSequence("next", ProductID, EntityID, EntityName, 0, SortByLooks, true, true, out SEName);
            if (NextProductID.TryParseInt().Value > 0)
            {
                tmpS.Append("&nbsp;|&nbsp;<a class=\"ProductNavLink\" href=\"" + SE.MakeProductAndEntityLink(EntityName, NextProductID, EntityID, SEName) + "\">" + AppLogic.GetString("showproduct.aspx.6").ToHtmlEncode() + "</a>&nbsp;");
            }
        }
        result = tmpS.ToString();

        return result;
    }

    public virtual bool LoadSubstituteProducts(string itemCode)
    {
        var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)HttpContext.Current.User).ThisCustomer;
        InterpriseSuiteEcommerceCommon.XSLTExtensionBase.InputValidator IV = new InterpriseSuiteEcommerceCommon.XSLTExtensionBase.InputValidator("SubstituteProducts");
        string ProductID = IV.ValidateString("ItemCode", itemCode);
        substituteItems = ProductDA.GetProductSubstitutes(ProductID, ThisCustomer)
                                           .Take(100)
                                           .ToList();
        bool hasSubtitute = substituteItems.Count > 0;
        substituteItems.ForEach(x => x.ItemCode = AppLogic.GetItemCodeByCounter(x.Counter));
        return hasSubtitute;
    }
    private List<Product> substituteItems = new List<Product>() { };

    public virtual string GetSubstituteProducts(string sitemCode)
    {
        var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)HttpContext.Current.User).ThisCustomer;
        InterpriseSuiteEcommerceCommon.XSLTExtensionBase.InputValidator IV = new InterpriseSuiteEcommerceCommon.XSLTExtensionBase.InputValidator("SubstituteProducts");
        string ProductID = IV.ValidateString("ItemCode", sitemCode);
        return this.ShowInventorySubstituteOptions(sitemCode, ThisCustomer);
    }

    private String ShowInventorySubstituteOptions(String itemCode, Customer thisCustomer)
    {
        String RelatedProductList = String.Empty;
        string ImgFilename = string.Empty;
        string itemHref = string.Empty;
        bool existing = false;
        bool exists = false;

        StringBuilder tmpS = new StringBuilder(10000);
        string displayFormat = AppLogic.AppConfig("SubstituteProductsFormat");

        // NOTE:
        //  Because of the Cache API uses unique keys cache entries
        //  We SHOULD LEAVE OUT the CurrentDate parameters since DateTime.Now
        //  is NON-Deterministic, it will return a new unique value EVERYTIME it's called
        //  Hence adding it to our cache key would work out against us since we'll be adding
        //  a NEW cache entry EVERYTIME.
        if (substituteItems.Count == 0) { LoadSubstituteProducts(itemCode); }

        if (substituteItems.Count == 0) { return tmpS.ToString(); }

        string format =
            "<div class='product-thumb transition maxheight1'>" +
                 "<div class='sale'>Sale</div>" +
                    "<div class='image'>" +
                      "<a href='{0}'>" +
                        "<img src='{1}'/>" +
                      "</a>" +
                    "</div>" +
                    "<div class='caption'>" +
                          "<div class='product-per-unit-measure'> " +
                            "<a href='{0}'>{2}</a> " +
                          "</div>" +
                          "<div class='description'> {3} </div>" +
            // price
                          "<div class='substitute-price'> " +
                              "<div><span class='substitute-price-from'>FROM*</span><br/>" +
                              "<span class='substitute-price-price'>{4}</span></div>" +
                              "<div class='substitute-price-per-um'><span class='substitute-price-um'>PER</span><br/>" +
                              "<span class='substitute-price-um'>{5}</span></div>" +
                          "</div>" +
                    "</div>" +
           "</div>";

        string format2 =
           "<div class='product-thumb transition maxheight1'>" +
                "<div class='sale'>Sale</div>" +
                   "<div class='image'>" +
                     "<a href='{0}'>" +
                       "<img src='{1}'/>" +
                     "</a>" +
                   "</div>" +
                   "<div class='caption'>" +
                         "<div class='product-per-unit-measure'> " +
                           "<a href='{0}'>{2}</a> " +
                         "</div>" +
                         "<div class='description'> {3} </div>" +
                   "</div>" +
          "</div>";
        // href , img , displayname, description

        foreach (var item in substituteItems)
        {
            string displayName = (!item.ItemDescription.IsNullOrEmptyTrimmed()) ? item.ItemDescription : item.ItemName;
            string itemlink = SE.MakeProductLink(item.Counter.ToString(), displayName);
            string itemDescription = String.Empty;
            if (!item.WebDescription.IsNullOrEmptyTrimmed() && AppLogic.ReplaceImageURLFromAssetMgr) { itemDescription = item.WebDescription.Replace("../images", "images"); }
            else { itemDescription = item.ExtendedDescription; }

            string subItemCode = InterpriseHelper.GetInventoryItemCode(item.Counter);
            string imgFileName = ProductImage.GetImageFileNameFromInventory("AND IsDefaultIcon=1", subItemCode, string.Empty);
            string imgUrl = AppLogic.LocateImageFilenameUrl("Product", subItemCode, "icon", imgFileName, AppLogic.AppConfigBool("Watermark.Enabled"), out exists);

            decimal price = 0;
            UnitMeasureInfo itemDefaultUM = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(item.ItemCode);
            PriceListData pricelist = GetProductPricingLevelData(item.ItemCode);
            string unitDescription = String.Empty;
            if (itemDefaultUM.Code != null)
            {
                List<PriceListUnitMeasureData> um = pricelist.UnitMeasures.Where(x => x.Key.ToString().Equals(itemDefaultUM.Code, StringComparison.CurrentCultureIgnoreCase)).Select(x => x.Value).ToList();
                if (um != null)
                {
                    var priceLists = um.Where(x => x.UnitMeasureCode.EqualsIgnoreCase(itemDefaultUM.Code)).Select(x => x.QuantityRanges);
                    var desc = um.Where(x => x.UnitMeasureCode.EqualsIgnoreCase(itemDefaultUM.Code)).Select(x => x.UnitMeasureDescription).FirstOrDefault(); ;
                    if (priceLists != null && desc != null)
                    {
                        var priceRange = priceLists.Select(x => x.FirstOrDefault(y => y.MinQuantity >= 3)).FirstOrDefault();
                        price = priceRange.UnitSellingPrice;
                        unitDescription = desc;
                    }
                }
            }
            if (price == 0)
            {
                tmpS.AppendFormat(format2, itemlink, imgUrl, displayName, HttpUtility.HtmlEncode(itemDescription));
            }
            else
            {
                tmpS.AppendFormat(format, itemlink, imgUrl, displayName, HttpUtility.HtmlEncode(itemDescription), price.ToCustomerCurrency(), unitDescription);
            }

        }

        return tmpS.ToString();
    }

    private void ComputePriceAndTax(Product item, Customer thisCustomer, ref decimal itemPrice, ref decimal itemVat)
    {
        var UnitMeasureCode = InterpriseHelper.GetItemDefaultUnitMeasure(item.ItemCode);
        var ThisCustomer = thisCustomer;
        decimal price = decimal.Zero;
        decimal vat = decimal.Zero;
        string shipToCode = (ThisCustomer.IsRegistered) ? ThisCustomer.DefaultShippingCode : String.Empty;
        switch (item.ItemType)
        {
            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT:
                //KitComposition kitCartComposition = KitComposition.FromCart(ThisCustomer,
                //                                        this.m_CartType,
                //                                        this.ItemCode,
                //                                        this.Id);

                //kitCartComposition.Quantity = Convert.ToDecimal(this.m_Quantity * this.UnitMeasureQty);
                //kitCartComposition.UnitMeasureCode = this.UnitMeasureCode;

                //_price = kitCartComposition.GetSalesPrice(ref vat, shipToCode);
                //_taxRate = vat;
                break;
            default:
                bool withVat = (ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive);

                string itemCode = item.ItemCode;
                if (item.ItemType == "Matrix Item")
                {
                    itemCode = AppLogic.GetItemCode(itemCode);
                }

                decimal promotionalPrice = decimal.Zero;

                price = InterpriseHelper.GetSalesPriceAndVat(ThisCustomer.CustomerCode,
                            itemCode,
                            ThisCustomer.CurrencyCode,
                            Convert.ToDecimal(1),
                            UnitMeasureCode.Code,
                            withVat,
                            ref promotionalPrice,
                            ref vat, new UnitMeasureInfo { Code = string.Empty }, ThisCustomer, shipToCode);

                if (promotionalPrice != decimal.Zero)
                {
                    price = promotionalPrice;
                }

                itemPrice = price;
                itemVat = vat;

                break;
        }

    }

    public virtual string ProductMatrixControl(int itemCounter, string itemCode, string itemType)
    {
        string output = String.Empty;
        var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)HttpContext.Current.User).ThisCustomer;
        if (!itemType.EqualsIgnoreCase(Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP)) { return output; }

        var xml = new XElement(DomainConstants.XML_ROOT_NAME);
        xml.Add(new XElement(DomainConstants.XML_SECTION_TYPE, "CUSTOM_MATRIX_CONTROL"));
        xml.Add(new XElement("ITEM_COUNTER", itemCounter));
        xml.Add(new XElement("SKIN_ID", ThisCustomer.SkinID));

        var matrixAtributes = ServiceFactory.GetInstance<IProductService>().GetMatrixAttributes(itemCode);
        var matrixAttibutesGroups = matrixAtributes.GroupBy(m => new { m.AttributeCode, m.AttributeDescription });

        var xmlMatrixAttributes = new XElement("MATRIX_ATTRIBUTES");
        int counter = 1;
        foreach (var group in matrixAttibutesGroups)
        {
            string groupCode = Security.JavascriptEscapeClean(group.Key.AttributeCode.ToHtmlDecode());

            var xmlMatrixAttribute = new XElement("MATRIX_ATTRIBUTE");
            xmlMatrixAttribute.Add(new XElement("CODE", groupCode));
            xmlMatrixAttribute.Add(new XElement("DESC", group.Key.AttributeDescription.ToHtmlDecode()));
            xmlMatrixAttribute.Add(new XElement("COUNTER", counter));

            var xmlMatrixAttributeValues = new XElement("VALUES");

            foreach (var item in group)
            {
                string code = item.AttributeValueCode.ToUrlEncode();

                var xmlMatrixAttributeValue = new XElement("VALUE");
                xmlMatrixAttributeValue.Add(new XElement("CODE", code));
                xmlMatrixAttributeValue.Add(new XElement("DESC", item.AttributeValueDescription.ToHtmlDecode()));
                xmlMatrixAttributeValues.Add(xmlMatrixAttributeValue);
            }

            xmlMatrixAttribute.Add(xmlMatrixAttributeValues);
            xmlMatrixAttributes.Add(xmlMatrixAttribute);
            counter++;
        }
        xml.Add(xmlMatrixAttributes);

        // selected matrix item
        //var matrixItems = MatrixItemData.GetMatrixItems(itemCounter, itemCode, true);
        var matrixItems = MatrixItemData.GetNonStockMatrixItems(itemCounter, itemCode, true);
        if (matrixItems.FirstOrDefault() != null)
        {
            xml.Add(new XElement("SELECTED_MATRIX_ITEMCODE", matrixItems.FirstOrDefault().ItemCode));

            ItemWebOption settings = null;
            if (TempWebOptionSettings == null) { settings = ItemWebOption.GetWebOption(matrixItems.FirstOrDefault().ItemCode); }
            else { settings = TempWebOptionSettings; }
            xml.Add(new XElement("INITIALQUANTITY", settings.MinimumOrderQuantity.ToNumberFormat()));
        }

        // get accessories
        //var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)HttpContext.Current.User).ThisCustomer;
        var xmlAccessoryItems = new XElement("ACCESSORY_ITEMS");
        string query = string.Format("exec EcommerceGetAccessoryItems @CustomerCode = {0}, @WebSiteCode = {1}, @ItemCode = {2}, @LanguageCode = {3}, @CurrentDate = {4}, @ProductFilterID = {5}, @ContactCode = {6}",
                 DB.SQuote(ThisCustomer.CustomerCode),
                 DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode),
                 DB.SQuote(itemCode),
                 DB.SQuote(ThisCustomer.LanguageCode),
                 DateTime.Now.ToDateTimeStringForDB().ToDbQuote(),
                 DB.SQuote(ThisCustomer.ProductFilterID),
                 DB.SQuote(ThisCustomer.ContactCode));

        DataSet ds = DB.GetDS(query, false);

        if (ds.Tables[0] != null)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                var xmlItem = new XElement("ACCESSORY_ITEM");
                string displayName = DB.RowField(dr, "ItemDescription");
                string subItemCode = DB.RowField(dr, "ItemCode");
                string subItemCounter = DB.RowField(dr, "Counter");
                if (CommonLogic.IsStringNullOrEmpty(displayName)) { displayName = DB.RowField(dr, "ItemName"); }
                xmlItem.Add(new XElement("ITEM_NAME", displayName));
                xmlItem.Add(new XElement("ITEM_CODE", subItemCode));
                xmlItem.Add(new XElement("ITEM_COUNTER", subItemCounter));
                xmlItem.Add(new XElement("ITEM_LINK", SE.MakeProductLink(subItemCounter, CommonLogic.Ellipses(displayName, 20, false))));
                xmlItem.Add(new XElement("ITEM_DISPLAYNAME", CommonLogic.Ellipses(displayName, 20, false)));
                xmlAccessoryItems.Add(xmlItem);
            }
        }
        xml.Add(xmlAccessoryItems);

        //int matrixCounter = CommonLogic.QueryStringUSInt(DomainConstants.QUERY_STRING_KEY_MATRIX_ID);
        //if (matrixCounter > 0)
        //{

        //    var matrixInfo = ServiceFactory.GetInstance<IProductService>()
        //                                   .GetMatrixItemInfo(matrixCounter);
        //    xml.Add(new XElement("SELECTED_MATRIX_ITEMCODE", matrixInfo.MatrixItemCode));
        //}

        var xmlpackage = new XmlPackage2("helper.product.xml.config", xml);
        output = xmlpackage.TransformString();
        return output;
    }

    public virtual string NonStockDisplayProductImage(int itemCounter, string itemCode, string itemType, string seAltText)
    {
        var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)HttpContext.Current.User).ThisCustomer;
        if (this.IsUsingHelperTemplate && CurrentContext.IsRequestingFromMobileMode(ThisCustomer))
        {
            return NonStockDisplayProductImage(itemCounter, itemCode, itemType, seAltText, true);
        }

        var output = new StringBuilder();
        string sAltText = string.Empty;
        //Additional img prooerties...
        string seTitle = string.Empty;
        string seAltText2 = string.Empty;

        if (!seAltText.IsNullOrEmptyTrimmed())
        {
            sAltText = seAltText;
        }

        //get the title of images
        output.Append("<script type=\"text/javascript\" language=\"Javascript\" >");
        output.Append(string.Format("var imgTitleArray{0} = new Array;", itemCounter));

        string languageCode = AppLogic.GetLanguageCode(ThisCustomer.LocaleSetting);
        var attributes = AppLogic.GetSEImageAttributeByItemCode(itemCode, "LARGE", languageCode);
        foreach (var item in attributes)
        {
            output.Append(string.Format("imgTitleArray{0}.push(\"" + item.Title.Replace("''", "'") + "\");", itemCounter));
        }

        output.Append("</script>");

        output.AppendFormat("<div id=\"pnlImage_{0}\"  align=\"center\" >\n", itemCounter);

        var attObject = AppLogic.GetSEImageAttributesByObject(itemCode, languageCode);
        seAltText2 = attObject.MediumAlt;
        seTitle = attObject.MediumTitle;

        output.AppendFormat("<a id=\"lnkLarge_{0}\" class=\"cloud-zoom\"><img data-contentKey=\"{3}\"  data-contentEntityType=\"product\" data-contentCounter='{4}' data-contentType=\"image\" class='product-image-for-matrix-options content' id=\"imgProduct_{0}\" alt=\"{1}\" title=\"{2}\" /></a>\n", itemCounter, attObject.MediumAlt, attObject.MediumTitle, itemCode, itemCounter);

        output.Append("<br />\n");

        // render the multiple images...
        output.AppendFormat("<div id=\"pnlImageMultiples_{0}\" class=\"image-pager\" >\n", itemCounter);

        for (int ctr = 0; ctr < 10; ctr++)
        {
            output.AppendFormat("<img id=\"imgMultiple_{0}_{1}\" style=\"display:none;cursor:hand;cursor:pointer;\" border=\"0\" alt=\"{2}\" title=\"{3}\" />", itemCounter, ctr, ThisCustomer.SkinID, (ctr + 1), seAltText2, seTitle);
        }
        output.Append("</div>\n");

        seAltText2 = attObject.LargeAlt;
        seTitle = attObject.LargeTitle;

        List<MatrixItemData> matrixItems = null;
        List<MatrixItemData> onlyMatrixItemsWithSwatches = null;

        if (itemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP)
        {
            matrixItems = MatrixItemData.GetNonStockMatrixItems(itemCounter, itemCode, true);

            string swatchOrientation = string.Empty;
            int swatchGroupSize, swatchSpacing;
            swatchGroupSize = swatchSpacing = 0;

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT img.SwatchOrientation, img.SwatchGroupSize, img.SwatchSpacing FROM InventoryMatrixGroup img with (NOLOCK) INNER JOIN InventoryItem i with (NOLOCK) ON i.ItemCode = img.ItemCode WHERE i.ItemCode = {0}", DB.SQuote(itemCode)))
                {
                    if (reader.Read())
                    {
                        swatchOrientation = DB.RSField(reader, "SwatchOrientation");
                        swatchGroupSize = DB.RSFieldInt(reader, "SwatchGroupSize");
                        swatchSpacing = DB.RSFieldInt(reader, "SwatchSpacing");
                    }
                }
            }

            // place default values
            if (string.IsNullOrEmpty(swatchOrientation)) swatchOrientation = "horizontal";
            if (swatchGroupSize <= 0) swatchGroupSize = 2;
            if (swatchSpacing <= 0) swatchSpacing = 0;

            onlyMatrixItemsWithSwatches = matrixItems
                                            .Where(item => null != item.ImageData && item.ImageData.swatch.exists)
                                            .ToList();

            if (onlyMatrixItemsWithSwatches.Count > 0)
            {
                var rows = new List<List<MatrixItemData>>();

                if (swatchOrientation.Equals("horizontal", StringComparison.InvariantCultureIgnoreCase))
                {
                    int colCtr = 0;
                    int rowCtr = 0;

                    foreach (var item in onlyMatrixItemsWithSwatches)
                    {
                        if (colCtr == 0)
                        {
                            rows.Add(new List<MatrixItemData>());
                        }

                        rows[rowCtr].Add(item);
                        colCtr += 1;
                        if (colCtr == swatchGroupSize)
                        {
                            colCtr = 0;
                            rowCtr++;
                        }
                    }
                }
                else
                {
                    for (int ctr = 0; ctr < swatchGroupSize; ctr++)
                    {
                        var columns = new List<MatrixItemData>();
                        rows.Add(columns);
                    }

                    int rowCtr = 0;

                    foreach (var item in onlyMatrixItemsWithSwatches)
                    {
                        rows[rowCtr].Add(item);
                        rowCtr += 1;
                        if (rowCtr == rows.Count)
                        {
                            rowCtr = 0;
                        }
                    }
                }

                output.Append("<br />\n");

                output.AppendFormat("<div id=\"pnlImageSwatch_{0}\" >\n", itemCounter);

                output.AppendFormat("<table id=\"tblImageSwatch_{0}\" border=\"0\" cellspacing=\"{1}\" >\n", itemCounter, swatchSpacing);

                foreach (var columns in rows)
                {
                    output.Append("<tr>\n");

                    foreach (var item in columns)
                    {
                        AppLogic.GetSEImageAttributes(item.ItemCode, "MEDIUM", languageCode, ref seTitle, ref seAltText2);
                        output.AppendFormat("        <td> <img id=\"imgSwatch_{0}_{1}\" style=\"cursor:hand;cursor:pointer;\" alt=\"{2}\" title=\"{3}\" /> </td>\n", itemCounter, item.Counter, seAltText2, seTitle);
                    }

                    output.Append("</tr>\n");
                }

                output.Append("</table>");

                output.Append("</div>\n");
            }

        }

        output.Append("</div>\n");

        var script = new StringBuilder();

        script.AppendLine();
        script.Append("<script type=\"text/javascript\" language=\"Javascript\" >\n");
        script.Append("$add_windowLoad(\n");
        script.Append(" function() { ");

        script.AppendFormat("    var product = ise.Products.ProductController.getProduct({0});\n", itemCounter);
        script.AppendFormat("    var imageControl = new ise.Products.ImageControl({0}, 'imgProduct_{0}');\n", itemCounter);
        script.AppendFormat("    imageControl.setProduct(product);\n");

        // micro image settings...
        script.AppendFormat("    imageControl.setUseMicroImages({0});\n", AppLogic.AppConfigBool("UseImagesForMultiNav").ToString().ToLowerInvariant());
        script.AppendFormat("    imageControl.setHandleHover({0});\n", AppLogic.AppConfigBool("UseRolloverForMultiNav").ToString().ToLowerInvariant());

        for (int ctr = 0; ctr < 10; ctr++)
        {
            int number = ctr + 1;
            string src = src = string.Format("skins/skin_{0}/images/im{1}.gif", ThisCustomer.SkinID, number);

            script.AppendFormat("    var imgMul_{0} = new ise.Products.ImageMultipleControl('imgMultiple_{1}_{0}', {0}, '{2}');\n", ctr, itemCounter, src);
            script.AppendFormat("    imageControl.registerMultipleControl(imgMul_{0});\n", ctr);
        }

        script.AppendFormat("    var lnkLarge = new ise.Products.LargeImageLinkControl('lnkLarge_{0}');\n", itemCounter);
        script.AppendFormat("    imageControl.setLargeImageControl(lnkLarge);\n");

        if (itemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP && null != onlyMatrixItemsWithSwatches)
        {
            for (int ctr = 0; ctr < onlyMatrixItemsWithSwatches.Count; ctr++)
            {
                var matrixItem = onlyMatrixItemsWithSwatches[ctr];
                script.AppendFormat("    var imgSwatch_{0} = new ise.Products.ImageSwatchControl({2}, 'imgSwatch_{1}_{2}', '{3}');\n", ctr, itemCounter, matrixItem.Counter, matrixItem.ItemCode.ToUrlEncode());
                script.AppendFormat("    imageControl.registerSwatchControl(imgSwatch_{0});\n", ctr);
            }
        }

        script.AppendFormat("    imageControl.arrangeDisplay();\n");

        script.Append(" }\n");
        script.Append(");\n");
        script.Append("</script>\n");
        script.AppendLine();

        output.Append(script.ToString());

        return output.ToString();
    }

    public virtual string NonStockDisplayProductImage(int itemCounter, string itemCode, string itemType, string seAltText, bool useXmlDesign)
    {
        var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)HttpContext.Current.User).ThisCustomer;
        var xml = new XElement(DomainConstants.XML_ROOT_NAME);
        xml.Add(new XElement(DomainConstants.XML_SECTION_TYPE, XMLSectionType.DISPLAY_PRODUCTIMAGE));
        xml.Add(new XElement("ITEM_COUNTER", itemCounter));
        xml.Add(new XElement("SKIN_ID", ThisCustomer.SkinID));
        xml.Add(new XElement("ITEM_TYPE", itemType));

        string output = string.Empty;
        string sAltText = seAltText;
        string seTitle = string.Empty;
        string seAltText2 = string.Empty;

        AppLogic.GetSEImageAttributes(itemCode, "MEDIUM", AppLogic.GetLanguageCode(ThisCustomer.LocaleSetting), ref seTitle, ref sAltText);
        var xmlImage = new XElement("IMAGE");
        xmlImage.Add(new XElement("ALT_TEXT", sAltText));
        xmlImage.Add(new XElement("TITLE_TEXT", seTitle));
        xml.Add(xmlImage);

        AppLogic.GetSEImageAttributes(itemCode, "MEDIUM", AppLogic.GetLanguageCode(ThisCustomer.LocaleSetting), ref seTitle, ref sAltText);
        var multipleImage = new XElement("MULTIPLE_IMAGE");
        multipleImage.Add(new XElement("ALT_TEXT", sAltText));
        multipleImage.Add(new XElement("TITLE_TEXT", seTitle));
        xml.Add(multipleImage);

        AppLogic.GetSEImageAttributes(itemCode, "MEDIUM", AppLogic.GetLanguageCode(ThisCustomer.LocaleSetting), ref seTitle, ref sAltText);
        var largeImage = new XElement("LARGE_IMAGE");
        largeImage.Add(new XElement("ALT_TEXT", sAltText));
        largeImage.Add(new XElement("TITLE_TEXT", seTitle));
        xml.Add(largeImage);

        List<MatrixItemData> matrixItems = null;
        List<MatrixItemData> onlyMatrixItemsWithSwatches = null;

        if (itemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP)
        {
            matrixItems = MatrixItemData.GetNonStockMatrixItems(itemCounter, itemCode, true);

            string swatchOrientation = string.Empty;
            int swatchGroupSize, swatchSpacing;
            swatchGroupSize = swatchSpacing = 0;

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT img.SwatchOrientation, img.SwatchGroupSize, img.SwatchSpacing FROM InventoryMatrixGroup img with (NOLOCK) INNER JOIN InventoryItem i with (NOLOCK) ON i.ItemCode = img.ItemCode WHERE i.ItemCode = {0}", DB.SQuote(itemCode)))
                {
                    if (reader.Read())
                    {
                        swatchOrientation = DB.RSField(reader, "SwatchOrientation");
                        swatchGroupSize = DB.RSFieldInt(reader, "SwatchGroupSize");
                        swatchSpacing = DB.RSFieldInt(reader, "SwatchSpacing");
                    }
                }
            }

            // place default values
            if (string.IsNullOrEmpty(swatchOrientation)) swatchOrientation = "horizontal";
            if (swatchGroupSize <= 0) swatchGroupSize = 2;
            if (swatchSpacing <= 0) swatchSpacing = 0;

            onlyMatrixItemsWithSwatches = matrixItems.FindAll(item => { return item.ImageData != null && item.ImageData.swatch.exists; });
            if (onlyMatrixItemsWithSwatches.Count > 0)
            {
                //lambda anonimous method
                onlyMatrixItemsWithSwatches.ForEach(m =>
                {
                    var swatitems = new XElement("JSON_SWATCHITEM");
                    AppLogic.GetSEImageAttributes(m.ItemCode, "MEDIUM", AppLogic.GetLanguageCode(ThisCustomer.LocaleSetting), ref seTitle, ref seAltText2);
                    m.AltText = seAltText2;
                    m.Title = seTitle;

                    swatitems.Add(new XElement("IMG_ALT_TEXT", m.AltText));
                    swatitems.Add(new XElement("IMG_TITLE_TEXT", m.Title));
                    swatitems.Add(new XElement("ITEM_COUNTER", m.Counter));
                    swatitems.Add(new XElement("COUNTER", itemCounter));
                    xml.Add(swatitems);
                });

            }

        }

        xml.Add(new XElement("USEIMAGESFORMULTINAV", AppLogic.AppConfigBool("UseImagesForMultiNav").ToString().ToLowerInvariant()));
        xml.Add(new XElement("USEROLLOVERFORMULTINAV", AppLogic.AppConfigBool("USEROLLOVERFORMULTINAV").ToString().ToLowerInvariant()));

        var iteratorXml = new XElement("ITERATOR");
        var iterator = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        iterator.ForEach(ctr => iteratorXml.Add(new XElement("CTR", ctr)));
        xml.Add(iteratorXml);

        if (onlyMatrixItemsWithSwatches != null)
        {
            var MatrixItems = onlyMatrixItemsWithSwatches
                                .Select(m => new CustomMatrixItem
                                {
                                    MatrixCounter = m.Counter,
                                    MatrixCode = m.ItemCode
                                })
                                .ToArray();
            string json = JSONHelper.Serialize<IEnumerable<CustomMatrixItem>>(MatrixItems);
            xml.Add(new XElement("JSON_MATRIX_ITEMS", json));
        }

        var xmlpackage = new XmlPackage2(this.XmlPackageHelperTemplate, xml);
        return xmlpackage.TransformString();
    }

    public string MakeItemLink(string itemCode)
    {
        return InterpriseHelper.MakeItemLink(itemCode);
    }

    private decimal GetInitialAddToCartQuantity()
    {
        //if the default isn't set, use one
        decimal intRetVal = 1;

        if (AppLogic.AppConfig("DefaultAddToCartQuantity").Length > 0)
        {
            intRetVal = AppLogic.AppConfigUSDecimal("DefaultAddToCartQuantity");
        }

        return intRetVal;
    }

    #region DisplayAddToCartForm

    private ItemWebOption TempWebOptionSettings = null;
    public string DisplayAddToCartForm(int itemCounter, string itemCode, string itemType, string align, bool useXmlDesign)
    {
        var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)HttpContext.Current.User).ThisCustomer;
        ItemWebOption settings = null;
        if (TempWebOptionSettings == null) { settings = ItemWebOption.GetWebOption(itemCode); }
        else { settings = TempWebOptionSettings; }

        //Check for wholesale parameter.
        //Hide add to cart buttons if CBMode is true
        //Hide Add to cart and wishlist button if ShowItemPriceWhenLogin is true
        bool isWholesaleOnlySite = (!AppLogic.AppConfigBool("UseWebStorePricing") && (AppLogic.AppConfigBool("WholesaleOnlySite") && ThisCustomer.DefaultPrice.ToLower() != "wholesale"));
        bool isShowItemPriceWhenLogin = (AppLogic.AppConfigBool("ShowItemPriceWhenLogin") && ThisCustomer.IsNotRegistered);
        if (!AppLogic.AppConfigBool("ShowBuyButtons") ||
            isWholesaleOnlySite ||
            AppLogic.IsCBNMode() ||
            isShowItemPriceWhenLogin)
        {
            settings.ShowBuyButton = false;
        }

        var xml = new XElement(DomainConstants.XML_ROOT_NAME);
        xml.Add(new XElement(DomainConstants.XML_SECTION_TYPE, XMLSectionType.DISPLAY_ADDTOCARTFORM));
        xml.Add(new XElement("ITEM_TYPE", itemType));
        xml.Add(new XElement("ITEM_COUNTER", itemCounter));
        xml.Add(new XElement("SKIN_ID", ThisCustomer.SkinID));

        xml.Add(new XElement("SHOW_BUY_BUTTON", settings.ShowBuyButton));
        xml.Add(new XElement("IS_WHOLESALE_ONSITE", AppLogic.AppConfigBool("WholesaleOnlySite")));
        xml.Add(new XElement("IS_CUSTOMER_DEFAULT_PRICE_NOT_WHOLESALE", ThisCustomer.DefaultPrice.ToLower() != "wholesale"));
        xml.Add(new XElement("IS_SHOW_ITEM_PRICE_WHEN_LOGIN", isShowItemPriceWhenLogin.ToString().ToLowerInvariant()));
        xml.Add(new XElement("USE_WEBSTORE_PRICING", AppLogic.AppConfigBool("UseWebStorePricing")));

        string uom = CommonLogic.QueryStringCanBeDangerousContent(DomainConstants.QUERY_STRING_KEY_UOM);
        if (!uom.IsNullOrEmptyTrimmed()) { xml.Add(new XElement("SELECTED_UOM", uom.ToUpperInvariant())); }

        bool? enabledGiftRegistry = AppLogic.AppConfigBool("GiftRegistry.Enabled");
        if (ThisCustomer.GiftRegistries.Count() > 0 && (enabledGiftRegistry.HasValue && enabledGiftRegistry.Value))
        {
            bool showGiftRegistry = (enabledGiftRegistry.Value && ThisCustomer.IsRegistered);

            //Shipping method oversize checking happens during matrix attribute selection event
            if (itemType != "Matrix Group")
            {
                var unitMeasures = ServiceFactory.GetInstance<IInventoryRepository>()
                                                 .GetItemBaseUnitMeasures(itemCode);

                var defaultUm = unitMeasures.FirstOrDefault();
                if (defaultUm != null)
                {
                    var shippingMethodOverSize = ServiceFactory.GetInstance<IShippingService>()
                                                               .GetOverSizedItemShippingMethod(itemCode, defaultUm.Code);
                    if (shippingMethodOverSize != null)
                    {
                        showGiftRegistry = (shippingMethodOverSize.FreightChargeType.ToUpperInvariant() != DomainConstants.PICKUP_FREIGHT_CHARGE_TYPE);
                    }
                }
            }

            xml.Add(new XElement("SHOW_GIFTREGISTRY_BUTTON", showGiftRegistry.ToStringLower()));

            var customerGiftRegistries = ThisCustomer.GiftRegistries;
            customerGiftRegistries.ForEach(item =>
            {
                var registries = new XElement("REGISTRIES");
                registries.Add(new XElement("TEXT", item.Title));
                registries.Add(new XElement("VALUE", item.RegistryID.ToString().ToLower()));
                xml.Add(registries);
            });
        }

        if (settings.IsCallToOrder)
        {
            xml.Add(new XElement("CALL_TO_ORDER_TEXT", AppLogic.GetString("common.cs.20")));
        }

        if (itemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ELECTRONIC_DOWNLOAD)
        {
            // don't allow file to be downloaded if not yet mapped
            var download = DownloadableItem.FindByItemCode(itemCode);
            if (download == null || !download.IsPhysicalFileExisting())
            {
                xml.Add(new XElement("NO_AVAILABLE_DOWNLOAD_TEXT", AppLogic.GetString("shoppingcart.cs.39")));
            }

            //Per defect #75; If the customer is anonymous don't allow them to buy downloadable items.
            if (ThisCustomer.IsNotRegistered)
            {
                xml.Add(new XElement("CUSTOMER_NOT_REGISTERED_TEXT", AppLogic.GetString("shoppingcart.cs.40")));
            }
        }

        var output = new StringBuilder();
        string action = string.Empty;

        if (settings.RequiresRegistration && ThisCustomer.IsNotRegistered)
        {
            action = MakeItemLink(itemCode);
        }
        else
        {
            action = "addtocart.aspx?returnurl={0}".FormatWith((CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables(DomainConstants.QUERY_STRING)).ToUrlEncode());
        }

        xml.Add(new XElement("FORM_ACTION", action));

        bool itemTypeIsKitAndWeAreInEditMode = false;
        var kitCartID = Guid.Empty;

        if (itemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
        {
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT PricingType FROM InventoryKit with (NOLOCK) WHERE ItemKitCode = {0}", DB.SQuote(itemCode)))
                {
                    if (reader.Read())
                    {
                        string pricingType = DB.RSField(reader, "PricingType");
                        xml.Add(new XElement("KIT_PRICING_TYPE", pricingType));
                    }
                }
            }

            string kitCartIDFromQueryString = CommonLogic.QueryStringCanBeDangerousContent("kcid");
            // check if we are in edit mode for this kit item
            if (!CommonLogic.IsStringNullOrEmpty(kitCartIDFromQueryString) &&
                CommonLogic.IsValidGuid(kitCartIDFromQueryString))
            {
                itemTypeIsKitAndWeAreInEditMode = true;
                kitCartID = new Guid(kitCartIDFromQueryString);

                xml.Add(new XElement("ITEM_KIT_IS_EDITMODE", itemTypeIsKitAndWeAreInEditMode));
                xml.Add(new XElement("KIT_CART_ID", kitCartID));
            }
        }

        bool hideUnitMeasure = AppLogic.AppConfigBool("HideUnitMeasure");
        xml.Add(new XElement("SHOW_MEASURE", !hideUnitMeasure));
        if (settings.ShowBuyButton)
        {
            if (itemTypeIsKitAndWeAreInEditMode)
            {
                xml.Add(new XElement("EDITCART_BUTTON_CAPTION", AppLogic.GetString("shoppingcart.cs.33", true)));
            }
            else
            {
                xml.Add(new XElement("ADDTOCART_BUTTON_CAPTION", AppLogic.GetString("AppConfig.CartButtonPrompt", true)));
            }
        }

        xml.Add(new XElement("SHOWWISHLISTBUTTON", AppLogic.AppConfigBool("ShowWishListButton").ToStringLower()));
        xml.Add(new XElement("WISHLIST_CAPTION", AppLogic.GetString("AppConfig.WishButtonPrompt", true)));

        bool ignoreStockLevel = AppLogic.AppConfigBool("Inventory.LimitCartToQuantityOnHand");

        string quantityValiadtionMessage = AppLogic.GetString("common.cs.22", true);
        if (AppLogic.IsAllowFractional)
        {
            quantityValiadtionMessage += String.Format("\\n" +
            AppLogic.GetString("common.cs.26", true),
            AppLogic.InventoryDecimalPlacesPreference.ToString());
        }

        var jSONAddToCartParamDTO = new JSONAddToCartParamDTO
        {
            ItemCounter = itemCounter,
            ItemCode = itemCode,
            AttributeNotAvailableText = AppLogic.GetString("showproduct.aspx.29", true),
            ProductNoEnuoughStockText = AppLogic.GetString("showproduct.aspx.30", true),
            QuantityText = AppLogic.GetString("showproduct.aspx.31", true),
            UnitMeasureText = AppLogic.GetString("showproduct.aspx.32", true),
            OrderLessThanText = AppLogic.GetString("showproduct.aspx.36", true),
            NoEnoughStockText = AppLogic.GetString("showproduct.aspx.42", true),
            SelectedItemNoEnoughStockText = AppLogic.GetString("showproduct.aspx.43", true),
            NotificationAvailabilityText = AppLogic.GetString("showproduct.aspx.46", true),
            EnterNoQuantityText = quantityValiadtionMessage,
            SpecifyQuantityText = AppLogic.GetString("common.cs.24", true),
            IgnoreStockLevel = ignoreStockLevel,
            HideUnitMeasure = hideUnitMeasure
        };


        // Edit MOde just for kits
        decimal initialQuantity = 0;
        string unitMeasureCode = string.Empty;

        initialQuantity = settings.MinimumOrderQuantity;//GetInitialAddToCartQuantity();

        if (itemTypeIsKitAndWeAreInEditMode)
        {
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT Quantity, UnitMeasureCode, CartType FROM EcommerceShoppingCart with (NOLOCK) WHERE ShoppingCartRecGuid = '{0}'", kitCartID))
                {
                    if (reader.Read())
                    {
                        initialQuantity = DB.RSFieldDecimal(reader, "Quantity");
                        unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                    }
                }
            }

            jSONAddToCartParamDTO.UnitMeasureCode = unitMeasureCode;
        }

        jSONAddToCartParamDTO.InitialQuantity = initialQuantity;

        string jsonFormat = JSONHelper.Serialize<JSONAddToCartParamDTO>(jSONAddToCartParamDTO);
        xml.Add(new XElement("CART_JSON_SCRIPT", jsonFormat));
        var xmlpackage = new XmlPackage2("helper.product.xml.config", xml);
        return xmlpackage.TransformString();
    }

    #endregion

    public string DisplayAddToQuote(int itemCounter, string itemCode, string itemType, string align)
    {
        var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)HttpContext.Current.User).ThisCustomer;
        ItemWebOption settings = null;
        if (TempWebOptionSettings == null) { settings = ItemWebOption.GetWebOption(itemCode); }
        else { settings = TempWebOptionSettings; }

        //Check for wholesale parameter.
        //Hide add to cart buttons if CBMode is true
        //Hide Add to cart and wishlist button if ShowItemPriceWhenLogin is true
        bool isWholesaleOnlySite = (!AppLogic.AppConfigBool("UseWebStorePricing") && (AppLogic.AppConfigBool("WholesaleOnlySite") && ThisCustomer.DefaultPrice.ToLower() != "wholesale"));
        bool isShowItemPriceWhenLogin = (AppLogic.AppConfigBool("ShowItemPriceWhenLogin") && ThisCustomer.IsNotRegistered);
        if (!AppLogic.AppConfigBool("ShowBuyButtons") ||
            isWholesaleOnlySite ||
            AppLogic.IsCBNMode() ||
            isShowItemPriceWhenLogin)
        {
            settings.ShowBuyButton = false;
        }

        var xml = new XElement(DomainConstants.XML_ROOT_NAME);
        xml.Add(new XElement(DomainConstants.XML_SECTION_TYPE, "CUSTOM_DISPLAY_ADDTOQUOTEFORM"));
        xml.Add(new XElement("ITEM_TYPE", itemType));
        xml.Add(new XElement("ITEM_COUNTER", itemCounter));
        xml.Add(new XElement("SKIN_ID", ThisCustomer.SkinID));

        xml.Add(new XElement("SHOW_BUY_BUTTON", settings.ShowBuyButton));
        xml.Add(new XElement("IS_WHOLESALE_ONSITE", AppLogic.AppConfigBool("WholesaleOnlySite")));
        xml.Add(new XElement("IS_CUSTOMER_DEFAULT_PRICE_NOT_WHOLESALE", ThisCustomer.DefaultPrice.ToLower() != "wholesale"));
        xml.Add(new XElement("IS_SHOW_ITEM_PRICE_WHEN_LOGIN", isShowItemPriceWhenLogin.ToString().ToLowerInvariant()));
        xml.Add(new XElement("USE_WEBSTORE_PRICING", AppLogic.AppConfigBool("UseWebStorePricing")));

        string uom = CommonLogic.QueryStringCanBeDangerousContent(DomainConstants.QUERY_STRING_KEY_UOM);
        if (!uom.IsNullOrEmptyTrimmed()) { xml.Add(new XElement("SELECTED_UOM", uom.ToUpperInvariant())); }

        bool? enabledGiftRegistry = AppLogic.AppConfigBool("GiftRegistry.Enabled");
        if (ThisCustomer.GiftRegistries.Count() > 0 && (enabledGiftRegistry.HasValue && enabledGiftRegistry.Value))
        {
            bool showGiftRegistry = (enabledGiftRegistry.Value && ThisCustomer.IsRegistered);

            //Shipping method oversize checking happens during matrix attribute selection event
            if (itemType != "Matrix Group")
            {
                var unitMeasures = ServiceFactory.GetInstance<IInventoryRepository>()
                                                 .GetItemBaseUnitMeasures(itemCode);

                var defaultUm = unitMeasures.FirstOrDefault();
                if (defaultUm != null)
                {
                    var shippingMethodOverSize = ServiceFactory.GetInstance<IShippingService>()
                                                               .GetOverSizedItemShippingMethod(itemCode, defaultUm.Code);
                    if (shippingMethodOverSize != null)
                    {
                        showGiftRegistry = (shippingMethodOverSize.FreightChargeType.ToUpperInvariant() != DomainConstants.PICKUP_FREIGHT_CHARGE_TYPE);
                    }
                }
            }

            xml.Add(new XElement("SHOW_GIFTREGISTRY_BUTTON", showGiftRegistry.ToStringLower()));

            var customerGiftRegistries = ThisCustomer.GiftRegistries;
            customerGiftRegistries.ForEach(item =>
            {
                var registries = new XElement("REGISTRIES");
                registries.Add(new XElement("TEXT", item.Title));
                registries.Add(new XElement("VALUE", item.RegistryID.ToString().ToLower()));
                xml.Add(registries);
            });
        }

        if (settings.IsCallToOrder)
        {
            xml.Add(new XElement("CALL_TO_ORDER_TEXT", AppLogic.GetString("common.cs.20")));
        }

        if (itemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ELECTRONIC_DOWNLOAD)
        {
            // don't allow file to be downloaded if not yet mapped
            var download = DownloadableItem.FindByItemCode(itemCode);
            if (download == null || !download.IsPhysicalFileExisting())
            {
                xml.Add(new XElement("NO_AVAILABLE_DOWNLOAD_TEXT", AppLogic.GetString("shoppingcart.cs.39")));
            }

            //Per defect #75; If the customer is anonymous don't allow them to buy downloadable items.
            if (ThisCustomer.IsNotRegistered)
            {
                xml.Add(new XElement("CUSTOMER_NOT_REGISTERED_TEXT", AppLogic.GetString("shoppingcart.cs.40")));
            }
        }

        var output = new StringBuilder();
        string action = string.Empty;

        if (settings.RequiresRegistration && ThisCustomer.IsNotRegistered)
        {
            action = MakeItemLink(itemCode);
        }
        else
        {
            action = "addtocart.aspx?returnurl={0}".FormatWith((CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables(DomainConstants.QUERY_STRING)).ToUrlEncode());
        }

        xml.Add(new XElement("FORM_ACTION", action));

        bool itemTypeIsKitAndWeAreInEditMode = false;
        var kitCartID = Guid.Empty;

        if (itemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
        {
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT PricingType FROM InventoryKit with (NOLOCK) WHERE ItemKitCode = {0}", DB.SQuote(itemCode)))
                {
                    if (reader.Read())
                    {
                        string pricingType = DB.RSField(reader, "PricingType");
                        xml.Add(new XElement("KIT_PRICING_TYPE", pricingType));
                    }
                }
            }

            string kitCartIDFromQueryString = CommonLogic.QueryStringCanBeDangerousContent("kcid");
            // check if we are in edit mode for this kit item
            if (!CommonLogic.IsStringNullOrEmpty(kitCartIDFromQueryString) &&
                CommonLogic.IsValidGuid(kitCartIDFromQueryString))
            {
                itemTypeIsKitAndWeAreInEditMode = true;
                kitCartID = new Guid(kitCartIDFromQueryString);

                xml.Add(new XElement("ITEM_KIT_IS_EDITMODE", itemTypeIsKitAndWeAreInEditMode));
                xml.Add(new XElement("KIT_CART_ID", kitCartID));
            }
        }

        bool hideUnitMeasure = AppLogic.AppConfigBool("HideUnitMeasure");
        xml.Add(new XElement("SHOW_MEASURE", !hideUnitMeasure));
        if (settings.ShowBuyButton)
        {
            if (itemTypeIsKitAndWeAreInEditMode)
            {
                xml.Add(new XElement("EDITCART_BUTTON_CAPTION", AppLogic.GetString("shoppingcart.cs.33", true)));
            }
            else
            {
                xml.Add(new XElement("ADDTOCART_BUTTON_CAPTION", AppLogic.GetString("AppConfig.CartButtonPrompt", true)));
            }
        }

        xml.Add(new XElement("SHOWWISHLISTBUTTON", AppLogic.AppConfigBool("ShowWishListButton").ToStringLower()));
        xml.Add(new XElement("WISHLIST_CAPTION", AppLogic.GetString("AppConfig.WishButtonPrompt", true)));

        bool ignoreStockLevel = AppLogic.AppConfigBool("Inventory.LimitCartToQuantityOnHand");

        string quantityValiadtionMessage = AppLogic.GetString("common.cs.22", true);
        if (AppLogic.IsAllowFractional)
        {
            quantityValiadtionMessage += String.Format("\\n" +
            AppLogic.GetString("common.cs.26", true),
            AppLogic.InventoryDecimalPlacesPreference.ToString());
        }

        var jSONAddToCartParamDTO = new JSONAddToCartParamDTO
        {
            ItemCounter = itemCounter,
            ItemCode = itemCode,
            AttributeNotAvailableText = AppLogic.GetString("showproduct.aspx.29", true),
            ProductNoEnuoughStockText = AppLogic.GetString("showproduct.aspx.30", true),
            QuantityText = AppLogic.GetString("showproduct.aspx.31", true),
            UnitMeasureText = AppLogic.GetString("showproduct.aspx.32", true),
            OrderLessThanText = AppLogic.GetString("showproduct.aspx.36", true),
            NoEnoughStockText = AppLogic.GetString("showproduct.aspx.42", true),
            SelectedItemNoEnoughStockText = AppLogic.GetString("showproduct.aspx.43", true),
            NotificationAvailabilityText = AppLogic.GetString("showproduct.aspx.46", true),
            EnterNoQuantityText = quantityValiadtionMessage,
            SpecifyQuantityText = AppLogic.GetString("common.cs.24", true),
            IgnoreStockLevel = ignoreStockLevel,
            HideUnitMeasure = hideUnitMeasure
        };


        // Edit MOde just for kits
        decimal initialQuantity = 0;
        string unitMeasureCode = string.Empty;

        initialQuantity = settings.MinimumOrderQuantity;//GetInitialAddToCartQuantity();

        if (itemTypeIsKitAndWeAreInEditMode)
        {
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT Quantity, UnitMeasureCode, CartType FROM EcommerceShoppingCart with (NOLOCK) WHERE ShoppingCartRecGuid = '{0}'", kitCartID))
                {
                    if (reader.Read())
                    {
                        initialQuantity = DB.RSFieldDecimal(reader, "Quantity");
                        unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                    }
                }
            }

            jSONAddToCartParamDTO.UnitMeasureCode = unitMeasureCode;
        }

        jSONAddToCartParamDTO.InitialQuantity = initialQuantity;

        string jsonFormat = JSONHelper.Serialize<JSONAddToCartParamDTO>(jSONAddToCartParamDTO);
        xml.Add(new XElement("CART_JSON_SCRIPT", jsonFormat));
        xml.Add(new XElement("INITIALQUANTITY", initialQuantity.ToNumberFormat()));

        var xmlpackage = new XmlPackage2("helper.product.xml.config", xml);
        return xmlpackage.TransformString();
    }

    public string iDisplayDimensionHTML(string itemCode)
    {

        string returnString = String.Empty;

        ItemUnitMeasuresData itemUnitMeasure = new ItemUnitMeasuresData();
        using (var con = DB.NewSqlConnection())
        {
            string itemcode = string.Empty;
            string unitMeasureCode = string.Empty;
            string description = string.Empty;
            decimal qty;
            decimal length;
            decimal width;
            decimal height;
            con.Open();
            string str = String.Format("Select ItemCode, IUM.UnitMeasureCode, IUM.UnitMeasureQty, SU.UnitMeasureDescription, ISNULL(LengthInCentimeters,0) * 10 AS LengthInMillimeters, ISNULL(WidthInCentimeters,0) * 10 AS WidthInMillimeters, ISNULL(HeightInCentimeters,0) * 10 AS HeightInMillimeters from InventoryUnitMeasure IUM LEFT JOIN SystemUnitMeasure SU ON IUM.UnitMeasureCode = SU.UnitMeasureCode where IUM.DefaultSelling = 1 AND IUM.ItemCode IN ({0})", itemCode.ToDbQuote());
            using (var reader = DB.GetRSFormat(con, str))
            {
                while (reader.Read())
                {
                    itemcode = DB.RSField(reader, "ItemCode");
                    unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                    length = DB.RSFieldDecimal(reader, "LengthInMillimeters");
                    width = DB.RSFieldDecimal(reader, "WidthInMillimeters");
                    height = DB.RSFieldDecimal(reader, "HeightInMillimeters");
                    description = DB.RSField(reader, "UnitMeasureDescription");
                    qty = DB.RSFieldDecimal(reader, "UnitMeasureQty");

                    if (unitMeasures.Count > 0)
                    {
                        if (unitMeasures.Where(x => x.ItemCode == itemcode && x.UnitMeasureCode == unitMeasureCode).Count() > 0) continue;
                    }

                    itemUnitMeasure = new ItemUnitMeasuresData()
                    {
                        ItemCode = itemcode,
                        UnitMeasureCode = unitMeasureCode,
                        Length = length,
                        Width = width,
                        Height = height,
                        Description = description,
                        Quantity = qty
                    };

                }
            }
        }


        if (itemUnitMeasure.ItemCode.IsNullOrEmptyTrimmed()) return returnString;
        bool has_dimension = false;

        if (itemUnitMeasure.Height > 0)
        {
            returnString = itemUnitMeasure.Height.ToCustomerRoundedCurrency() + "mm ";
            has_dimension = true;
        }

        if (itemUnitMeasure.Width > 0)
        {
            returnString += "x " + itemUnitMeasure.Width.ToCustomerRoundedCurrency() + "mm ";
            has_dimension = true;
        }

        if (itemUnitMeasure.Length > 0)
        {
            returnString += "x " + itemUnitMeasure.Length.ToCustomerRoundedCurrency() + "mm ";
            has_dimension = true;
        }

        if (has_dimension)
        {

            return "<tr><td style='padding:5px;' class='text-section-title'>DIMENSIONS (HxWxD)</td><td style='width:12px;text-align:center'>:</td><td><span>" + returnString + "</span></td></tr>";
        }

        return "";

    }

    public string DisplayProductDimensions(string itemCode, bool isDisplayMinimumOrder)
    {
        string returnString = String.Empty;

        if (isDisplayMinimumOrder)
        {

            var itemWebOption = ServiceFactory.GetInstance<IProductService>().GetWebOption(itemCode);
            return itemWebOption.MinOrderQuantity == null || itemWebOption.MinOrderQuantity == 0 ? "1" : itemWebOption.MinOrderQuantity.ToString("0.#");

        }

        ItemUnitMeasuresData itemUnitMeasure = new ItemUnitMeasuresData();
        using (var con = DB.NewSqlConnection())
        {
            string itemcode = string.Empty;
            string unitMeasureCode = string.Empty;
            string description = string.Empty;
            decimal qty;
            decimal length;
            decimal width;
            decimal height;
            con.Open();
            string str = String.Format("Select ItemCode, IUM.UnitMeasureCode, IUM.UnitMeasureQty, SU.UnitMeasureDescription, ISNULL(LengthInCentimeters,0) * 10 AS LengthInMillimeters, ISNULL(WidthInCentimeters,0) * 10 AS WidthInMillimeters, ISNULL(HeightInCentimeters,0) * 10 AS HeightInMillimeters from InventoryUnitMeasure IUM LEFT JOIN SystemUnitMeasure SU ON IUM.UnitMeasureCode = SU.UnitMeasureCode where IUM.DefaultSelling = 1 AND IUM.ItemCode IN ({0})", itemCode.ToDbQuote());
            using (var reader = DB.GetRSFormat(con, str))
            {
                while (reader.Read())
                {
                    itemcode = DB.RSField(reader, "ItemCode");
                    unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                    length = DB.RSFieldDecimal(reader, "LengthInMillimeters");
                    width = DB.RSFieldDecimal(reader, "WidthInMillimeters");
                    height = DB.RSFieldDecimal(reader, "HeightInMillimeters");
                    description = DB.RSField(reader, "UnitMeasureDescription");
                    qty = DB.RSFieldDecimal(reader, "UnitMeasureQty");

                    if (unitMeasures.Count > 0)
                    {
                        if (unitMeasures.Where(x => x.ItemCode == itemcode && x.UnitMeasureCode == unitMeasureCode).Count() > 0) continue;
                    }

                    itemUnitMeasure = new ItemUnitMeasuresData()
                    {
                        ItemCode = itemcode,
                        UnitMeasureCode = unitMeasureCode,
                        Length = length,
                        Width = width,
                        Height = height,
                        Description = description,
                        Quantity = qty
                    };

                }
            }
        }


        if (itemUnitMeasure.ItemCode.IsNullOrEmptyTrimmed()) return returnString;

        if (itemUnitMeasure.Height > 0)
        {
            returnString = itemUnitMeasure.Height.ToCustomerRoundedCurrency() + "mm ";
        }

        if (itemUnitMeasure.Width > 0)
        {
            returnString += "x " + itemUnitMeasure.Width.ToCustomerRoundedCurrency() + "mm ";
        }

        if (itemUnitMeasure.Length > 0)
        {
            returnString += "x " + itemUnitMeasure.Length.ToCustomerRoundedCurrency() + "mm ";
        }

        returnString = "<span>" + returnString + "</span>";
        return returnString;
    }

    public string displayUnitSellingPrice(string itemCode, string itemType, string sales_price, string quantity, string uomqty, string CategoryCode)
    {

        sales_price = sales_price.Replace("$", string.Empty);
        sales_price = sales_price.Replace(",", string.Empty);
        quantity = quantity.Replace(",", string.Empty);
        decimal price = 0;
        //This will handle divided by zero problem.
        try
        {
            //Differentiate express bag computation from white bag and sticker bundles
            if (itemType == "Kit" && (CategoryCode != "white bag and sticker bundles" && CategoryCode != "brown bag and sticker bundles"))
            {
                price = Convert.ToDecimal(sales_price) / (Convert.ToDecimal(quantity) / Convert.ToDecimal(uomqty));
            }
            else
            {
                price = Convert.ToDecimal(sales_price) / Convert.ToDecimal(quantity);
            }
        }
        catch { }

        decimal formatted_price = price.ToCustomerRoundedCurrency();
        return Convert.ToString(formatted_price.ToCustomerCurrency());
    }

    public string displayProductSize(string itemCode, string split)
    {

        string ProductSize_C = string.Empty, height = string.Empty, width = string.Empty, depth = string.Empty, dimensionLabel = string.Empty;
        StringBuilder formattedProductSize = new StringBuilder();
        using (var con = DB.NewSqlConnection())
        {
            con.Open();
            string str = String.Format("SELECT ProductSize_C FROM InventoryItem WHERE ItemCode = {0}", itemCode.ToDbQuote());
            using (var reader = DB.GetRSFormat(con, str))
            {
                if (reader.Read())
                {
                    ProductSize_C = DB.RSField(reader, "ProductSize_C");

                }
            }
        }

        string[] separator = ProductSize_C.Split('x');
        if (separator.Length >= 1)
        {
            //height = CommonLogic.IIF(string.IsNullOrEmpty(split[0]) == true, " N/A", " " + split[0].TrimStart(' '));
            if (string.IsNullOrEmpty(separator[0]) != true)
            {
                dimensionLabel += "(H";
                formattedProductSize.Append("<b>Height: </b>");
                formattedProductSize.Append(separator[0].TrimStart(' '));
            }
        }
        if (separator.Length >= 2)
        {
            //widht = CommonLogic.IIF(string.IsNullOrEmpty(split[1]) == true, " N/A", " " + split[1].TrimStart(' '));
            if (string.IsNullOrEmpty(separator[1]) != true)
            {
                dimensionLabel += "xW";
                formattedProductSize.Append("<br/><b>Width: </b>");
                formattedProductSize.Append(separator[1].TrimStart(' '));
            }
        }
        if (separator.Length >= 3)
        {
            //depth = CommonLogic.IIF(string.IsNullOrEmpty(split[2]) == true, " N/A", " " + split[2].TrimStart(' '));
            if (string.IsNullOrEmpty(separator[2]) != true)
            {
                dimensionLabel += "xD";
                formattedProductSize.Append("<br/><b>Depth: </b>");
                formattedProductSize.Append(separator[2].TrimStart(' '));
            }
        }

        //formattedProductSize = "<b>Height:</b>" + height + "<br/><b>Width:</b>" + widht + "<br/><b>Depth:</b>" + depth;
        if (split.ToLower() != "true")
        {
            formattedProductSize.Clear();
            formattedProductSize.Append(ProductSize_C);
            if (dimensionLabel != "")
            {
                dimensionLabel += ")";
                formattedProductSize.Append("&nbsp;");
                formattedProductSize.Append(dimensionLabel);
            }
        }
        return formattedProductSize.ToString();
    }

    public string DisplayProductSizeFullColour(string itemCode, string split)
    {
        string ProductSize_C = string.Empty, height = string.Empty, width = string.Empty, depth = string.Empty, dimensionLabel = string.Empty;
        StringBuilder formattedProductSize = new StringBuilder();
        using (var con = DB.NewSqlConnection())
        {
            con.Open();
            string str = String.Format("SELECT ProductSize_C FROM InventoryItem WHERE ItemCode = {0}", itemCode.ToDbQuote());
            using (var reader = DB.GetRSFormat(con, str))
            {
                if (reader.Read())
                {
                    ProductSize_C = DB.RSField(reader, "ProductSize_C");

                }
            }
        }

        string[] separator = ProductSize_C.Split('x');
        if (separator.Length == 1)
        {
            formattedProductSize.Append(separator[0].TrimStart(' '));
        }
        if (separator.Length > 1)
        {
            if (string.IsNullOrEmpty(separator[0]) != true)
            {
                dimensionLabel += "(H";
                formattedProductSize.Append("<b>Height: </b>");
                formattedProductSize.Append(separator[0].TrimStart(' '));
            }
        }
        if (separator.Length >= 2)
        {
            if (string.IsNullOrEmpty(separator[1]) != true)
            {
                dimensionLabel += "xW";
                formattedProductSize.Append("<br/><b>Width: </b>");
                formattedProductSize.Append(separator[1].TrimStart(' '));
            }
        }
        if (separator.Length >= 3)
        {
            if (string.IsNullOrEmpty(separator[2]) != true)
            {
                dimensionLabel += "xD";
                formattedProductSize.Append("<br/><b>Depth: </b>");
                formattedProductSize.Append(separator[2].TrimStart(' '));
            }
        }

        if (split.ToLower() != "true")
        {
            formattedProductSize.Clear();
            formattedProductSize.Append(ProductSize_C);
            if (dimensionLabel != "")
            {
                dimensionLabel += ")";
                formattedProductSize.Append("&nbsp;");
                formattedProductSize.Append(dimensionLabel);
            }
        }
        return formattedProductSize.ToString();
    }
    private string FormatDimension(string value)
    {
        string formatvalue = string.Empty;

        if (string.IsNullOrEmpty(value) == true || value == null)
        {
            formatvalue = " N/A";
        }
        else
        {
            formatvalue = " " + value.TrimStart(' ');
        }
        return formatvalue;
    }

    public string getBlogUrl(int counter)
    {

        string id = counter.ToString();
        string alias = AppLogic.AppConfig("custom.blog.alias_" + id);
        if (alias.IsNullOrEmptyTrimmed())
        {
            return "blog?showarticle=" + id;
        }

        return alias;

    }

    public string GetCategorySummary(string categoryCode)
    {
        string text = string.Empty;

        using (var con = DB.NewSqlConnection())
        {
            con.Open();
            string str = String.Format("SELECT Summary FROM SystemCategoryWebOptionDescription WHERE LanguageCode = {0} AND CategoryCode = {1}", Customer.Current.LanguageCode.ToDbQuote(), categoryCode.ToDbQuote());
            using (var reader = DB.GetRSFormat(con, str))
            {
                if (reader.Read())
                {
                    text = reader.ToRSField("Summary");
                }
            }
        }
        return text;
    }

    public string GetCategoryFooterText(string categoryCode)
    {
        string text = string.Empty;

        using (var con = DB.NewSqlConnection())
        {
            con.Open();
            string str = String.Format("SELECT FooterText_C FROM SystemCategory WHERE CategoryCode = {0}", categoryCode.ToDbQuote());
            using (var reader = DB.GetRSFormat(con, str))
            {
                if (reader.Read())
                {
                    text = reader.ToRSField("FooterText_C");
                }
            }
        }
        return text;
    }

    public string NonStockMatrixControl(string itemCounter, string itemCode)
    {
        string output = String.Empty;

        var xml = new XElement(DomainConstants.XML_ROOT_NAME);
        xml.Add(new XElement(DomainConstants.XML_SECTION_TYPE, XMLProductSectionType.NON_STOCK_MATRIX_CONTROL));
        xml.Add(new XElement("ITEM_COUNTER", itemCounter));

        var matrixAtributes = ServiceFactory.GetInstance<IProductService>().GetNonStockMatrixAttributes(itemCounter, itemCode);
        var matrixAttibutesGroups = matrixAtributes.GroupBy(m => new { m.AttributeCode, m.AttributeDescription });

        var xmlMatrixAttributes = new XElement("MATRIX_ATTRIBUTES");
        int counter = 1;
        foreach (var group in matrixAttibutesGroups)
        {
            string groupCode = Security.JavascriptEscapeClean(group.Key.AttributeCode.ToHtmlDecode());

            var xmlMatrixAttribute = new XElement("MATRIX_ATTRIBUTE");
            xmlMatrixAttribute.Add(new XElement("CODE", groupCode));
            xmlMatrixAttribute.Add(new XElement("DESC", group.Key.AttributeDescription.ToHtmlDecode()));
            xmlMatrixAttribute.Add(new XElement("COUNTER", counter));

            var xmlMatrixAttributeValues = new XElement("VALUES");

            foreach (var item in group)
            {
                string code = item.AttributeValueCode.ToUrlEncode();

                var xmlMatrixAttributeValue = new XElement("VALUE");
                xmlMatrixAttributeValue.Add(new XElement("CODE", code));
                xmlMatrixAttributeValue.Add(new XElement("DESC", item.AttributeValueDescription.ToHtmlDecode()));
                xmlMatrixAttributeValues.Add(xmlMatrixAttributeValue);
            }

            xmlMatrixAttribute.Add(xmlMatrixAttributeValues);
            xmlMatrixAttributes.Add(xmlMatrixAttribute);
            counter++;
        }
        xml.Add(xmlMatrixAttributes);

        // selected matrix item
        int matrixCounter = CommonLogic.QueryStringUSInt(DomainConstants.QUERY_STRING_KEY_MATRIX_ID);
        if (matrixCounter > 0)
        {
            var matrixInfo = ServiceFactory.GetInstance<IProductService>()
                                           .GetMatrixItemInfo(matrixCounter);
            xml.Add(new XElement("SELECTED_MATRIX_ITEMCODE", matrixInfo.MatrixItemCode));
        }

        var xmlpackage = new XmlPackage2("helper.product.xml.config", xml);
        output = xmlpackage.TransformString();
        return output;
    }

    public string GetNonStockMatrixDefault(string ItemCounter, string ItemCode, string ItemType)
    {
        return AppLogic.GetNonStockMatrixDefault(ItemCounter, ItemCode, ItemType);
    }

    public string DisplayItemQuantity(string ItemCounter, string ItemCode)
    {
        string restrictedQuantitiesValue = string.Empty;
        StringBuilder output = new StringBuilder();
        Customer thisCustomer = Customer.Current;
        if ((thisCustomer.CostCenter != "Smartbag-PREMIUM APG" && thisCustomer.CostCenter != "Smartbag-PREMIUM CUE" && thisCustomer.CostCenter != "Smartbag-PREMIUM DION LEE") || thisCustomer.ThisCustomerSession["JobRole"] != "Admin")
        {
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT RestrictedQuantity FROM InventoryItemWebOption WHERE ItemCode = {0}", DB.SQuote(ItemCode)))
                {
                    if (reader.Read())
                    {
                        restrictedQuantitiesValue = reader.ToRSField("RestrictedQuantity");
                    }
                }
            }
        }
        if (!restrictedQuantitiesValue.IsNullOrEmptyTrimmed())
        {
            string[] quantityValues = restrictedQuantitiesValue.Split(',');
            if (quantityValues.Length > 0)
            {
                output.Append("<div class=\"qty-select-col-values\">");
                output.AppendFormat("<select size=\"1\" class=\"showproduct_limit-restricted-qty\" id=\"Quantity_{0}\" name=\"Quantity_{0}\"><option value=\"0\">Select</option>", ItemCounter);
                foreach (string quantityValue in quantityValues)
                {
                    decimal quantity = 0;
                    if (decimal.TryParse(quantityValue, out quantity))
                    {
                        output.AppendFormat("<option value=\"{0}\">{0}</option>", quantity);
                    }
                }
                output.Append("</select>");
                output.Append("</div>");
            }
        }
        else
        {
            output.Append("<div class=\"qty-text-col-values\">");
            output.AppendFormat("<input class=\"inputQuantityLimit\" id=\"Quantity_{0}\" name=\"Quantity_{0}\" maxlength=\"14\" type=\"text\">", ItemCounter);
            output.Append("</div>");
        }
        return output.ToString();
    }

    public string GetContactList()
    {
        return AppLogic.GetContactList();
    }

    public string GetPortalUOMHTML(string ItemCode)
    {
        Customer thisCustomer = Customer.Current;
        StringBuilder output = new StringBuilder();
        var itemDefaultUM = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(ItemCode);
        output.Append("<div class=\"uom\">");
        output.AppendLine("");
        if (thisCustomer.CostCenter == "Smartbag-PREMIUM CUE")
        {
            output.AppendFormat("Quantity per box: {0}", Localization.ParseLocaleDecimal(itemDefaultUM.Quantity, thisCustomer.LocaleSetting));
        }
        else
        {
            output.AppendFormat("UOM: {0}", Localization.ParseLocaleDecimal(itemDefaultUM.Quantity, thisCustomer.LocaleSetting));
        }
        output.AppendLine("</div>");
        output.Append("<div class=\"uom\">");
        //if (itemDefaultUM.Code.ToLower().Contains("ctn"))
        //{
        //    output.AppendLine("Type: Cartons");
        //}
        //else
        //{
        //    output.AppendLine("Type: " + itemDefaultUM.Description);
        //}
        output.AppendLine("Type: " + itemDefaultUM.Description);
        output.AppendLine("</div>");

        return output.ToString();
    }

    public string IsCompanyAdmin()
    {
        Customer thisCustomer = Customer.Current;
        return CommonLogic.IIF(thisCustomer.ThisCustomerSession["JobRole"].ToLowerInvariant() == "admin", "true", "false");
    }

    public string CostCenter()
    {
        Customer thisCustomer = Customer.Current;
        return thisCustomer.CostCenter.ToLowerInvariant();
    }

    public string ShowOutofStockText(string ItemCode, int Quantity, string ItemType)
    {
        string output = string.Empty;
        if (ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_NON_STOCK ||
            ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_SERVICE ||
            ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ELECTRONIC_DOWNLOAD ||
            ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CARD ||
            ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CERTIFICATE) { return output; }
        if (!AppLogic.AppConfigBool("Inventory.LimitCartToQuantityOnHand"))
        {
            var thisCustomer = ServiceFactory.GetInstance<IAuthenticationService>().GetCurrentLoggedInCustomer();
            var freeStock = InterpriseHelper.InventoryFreeStock(ItemCode, thisCustomer);
            var remainingStock = 0;
            remainingStock = freeStock - Quantity;
            if (remainingStock < Quantity)
            {
                output = AppLogic.GetString("portal.aspx.12");
            }
        }
        return output.ToString();
    }

    public static string GetProductPromo()
    {
        Customer thisCustomer = Customer.Current;
        string imagePath = string.Empty, fullPath = string.Empty, logo = string.Empty;
        imagePath = "images/CompanyLogo/" + thisCustomer.CustomerCode + "-productpromo.png";
        if (CommonLogic.FileExists(imagePath))
        {
            logo = string.Format("<a href=\"placeorder.aspx?SearchTerm=HNH00453\" /><img src=\"{0}\" /></a>", imagePath);
        }
        imagePath = "images/CompanyLogo/" + thisCustomer.CustomerCode + "-productpromo.jpg";
        if (CommonLogic.FileExists(imagePath))
        {
            logo = string.Format("<a href=\"placeorder.aspx?SearchTerm=HNH00453\" /><img src=\"{0}\" /></a>", imagePath);
        }
        return logo;
    }

    public static string GetExpressPrintQtyDropdown(string ItemCode, string ID, string Qty)
    {
        Customer thisCustomer = Customer.Current;
        var output = new StringBuilder();
        var text = string.Empty;
        var select = string.Empty;
        var value = decimal.Zero;
        var valueText = string.Empty;
        Qty = Qty.Replace(",", string.Empty);
        output.AppendFormat("<select size=\"1\" class=\"showproduct_limit-restricted-qty\" id=\"Quantity_{0}\" name=\"Quantity_{0}\">", ID);
        output.AppendFormat("<option value=0>Select</option>");
        using (var con = DB.NewSqlConnection())
        {
            con.Open();
            using (var reader = DB.GetRSFormat(con, "GetExpressKitQtyDropdown {0}", DB.SQuote(ItemCode)))
            {
                while (reader.Read())
                {
                    text = reader.ToRSField("PricingAttribute_C");
                    value = reader.ToRSFieldDecimal("MaxQuantity");
                    valueText = value.ToString("G29");
                    if (text.Length < 1)
                    {
                        text = Localization.ParseLocaleDecimal(value, thisCustomer.LocaleSetting);
                    }
                    if (Qty == valueText)
                    {
                        output.AppendFormat("<option value=\"{0}\" selected>{1}</option>", valueText, text);
                    }
                    else
                    {
                        output.AppendFormat("<option value=\"{0}\">{1}</option>", valueText, text);
                    }
                }
            }
        }
        output.AppendLine("</select>");
        return output.ToString();
    }

    public static string CategoryLink(string ItemCode)
    {
        Customer thisCustomer = Customer.Current;
        var hlp = AppLogic.LookupHelper("Category");
        var SourceEntityID = EntityHelper.GetProductsFirstEntity(ItemCode, "Category").ToString();
        return SE.MakeEntityLink("Category", SourceEntityID, "");
    }

    public string DisplayDefaultUOMQuantity (string ItemCode)
    {
        var itemDefaultUM = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(ItemCode);
        return Localization.ParseLocaleDecimal(itemDefaultUM.Quantity, Customer.Current.LocaleSetting);
    }
}