﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

[assembly: AssemblyProduct("Connected Business")]
[assembly: AssemblyCompany("Connected Business")]
[assembly: AssemblyCopyright("Copyright © 2014 Connected Business, All rights reserved.")]
[assembly: AssemblyTrademark("")]

[assembly: CLSCompliant(true)]
[assembly: ComVisibleAttribute(false)]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]


[assembly: AssemblyVersionAttribute("14.0.2.73")]
[assembly: AssemblyFileVersionAttribute("14.0.2.73")]
[assembly: AssemblyInformationalVersion("14.0.2.73")]
