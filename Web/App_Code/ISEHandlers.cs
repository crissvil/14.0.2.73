// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Linq;
using System.Xml;
using System.Web;
using System.Text;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.DataAccess;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using com.paypal.soap.api;
using InterpriseSuiteEcommerceGateways;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Tool;
using InterpriseSuiteEcommerceControls.Validators;
using System.Web.SessionState;
using System.IO;
using System.Collections.Generic;




#region Login Customer


/// <summary>
/// Add this line <add name="loginCustomer.aspx_*" path="loginCustomer.aspx" verb="*" type="LoginCustomer" preCondition="integratedMode,runtimeVersionv4.0"/>
/// to web.config in  /<system.webServer>/<handlers>
/// </summary>
public class LoginCustomer : IHttpHandler, IRequiresSessionState
{
    public bool IsReusable
    {
        get { return true; }
    }
    private INavigationService _navigationService = null;
    private IStringResourceService _stringResourceService = null;
    private IShoppingCartService _shoppingCartService = null;
    private IProductService _productService = null;
    private IAuthenticationService _authenticationService = null;
    private void InitializeDomainServices()
    {

    }
    public LoginCustomer()
    {
        _navigationService = ServiceFactory.GetInstance<INavigationService>();
        _stringResourceService = ServiceFactory.GetInstance<IStringResourceService>();
        _shoppingCartService = ServiceFactory.GetInstance<IShoppingCartService>();
        _productService = ServiceFactory.GetInstance<IProductService>();
        _authenticationService = ServiceFactory.GetInstance<IAuthenticationService>();
    }



    public void ProcessRequest(HttpContext context)
    {
        context.Response.CacheControl = "private";
        context.Response.Expires = 0;
        context.Response.AddHeader("pragma", "no-cache");

        string email = CommonLogic.FormCanBeDangerousContent("email");
        string password = CommonLogic.FormCanBeDangerousContent("password");
        bool remember = CommonLogic.FormCanBeDangerousContent("remember").ToLower() == "on";


        if (AppLogic.AppConfigBool("SecurityCodeRequiredOnStoreLogin"))
        {
            string errorMessage = _stringResourceService.GetString("signin.aspx.22", true)
                                                        .FormatWith(String.Empty, String.Empty);
            if (context.Session == null)
            {
                context.Response.Redirect("signin.aspx?error=captcha");
                return;
            }
            if (context.Session["SecurityCode"] != null)
            {
                string sCode = context.Session["SecurityCode"].ToString();
                string fCode = CommonLogic.FormCanBeDangerousContent("captcha");
                bool codeMatch = false;

                if (AppLogic.AppConfigBool("Captcha.CaseSensitive"))
                {
                    if (fCode.Equals(sCode))
                        codeMatch = true;
                }
                else
                {
                    if (fCode.Equals(sCode, StringComparison.InvariantCultureIgnoreCase))
                        codeMatch = true;
                }

                if (!codeMatch)
                {
                    //this.pErrorMessage.InnerText = errorMessage;
                    //this.divErrorContainer.Visible = true;
                    //this.txtCaptcha.Value = String.Empty;
                    //this.imgCaptcha.Src = "Captcha.ashx?id=1";
                    //return;

                    context.Response.Redirect("signin.aspx?error=captcha");
                    return;
                }
            }
            else
            {
                //this.pErrorMessage.InnerText = errorMessage;
                //this.divErrorContainer.Visible = true;
                //this.txtCaptcha.Value = String.Empty;
                //this.imgCaptcha.Src = "Captcha.ashx?id=1";
                context.Response.Redirect("signin.aspx?error=captcha");
                return;
            }
        }



        //this.pErrorMessage.InnerText = AppLogic.GetString("signin.aspx.21", true);
        //this.divErrorContainer.Visible = true;
        var _EmailValidator = new RegularExpressionInputValidator
            (
            new System.Web.UI.WebControls.TextBox { Text = email },
            CommonLogic.IIF(CommonLogic.GetWebsiteType() == WebsiteType.Company, "", DomainConstants.EmailRegExValidator),
            AppLogic.GetString("signin.aspx.21", true));
        _EmailValidator.Validate();


        if (_EmailValidator.IsValid)
        {
            var status = _authenticationService.Login(email, password, remember);

            if (!status.IsValid)
            {
                if (status.IsAccountExpired)
                {
                    //this.pErrorMessage.InnerText = _stringResourceService.GetString("signin.aspx.message.1", true);
                    //this.lnkContactUs.InnerText = _stringResourceService.GetString("menu.Contact", true);
                    //this.lnkContactUs.Visible = true;
                    //this.divErrorContainer.Visible = true;
                    context.Response.Redirect("signin.aspx?error=expiredaccount");

                }
                else
                {
                    //this.pErrorMessage.InnerText = _stringResourceService.GetString("signin.aspx.20", true);
                    //this.divErrorContainer.Visible = true;

                    context.Response.Redirect("signin.aspx?error=invalidlogin");
                }
                return;
            }


            context.Response.Redirect(context.Request.UrlReferrer.ToString());
            //var customerWithValidLogin = _authenticationService.GetCurrentLoggedInCustomer();
            //string sReturnURL = _authenticationService.GetRedirectUrl(customerWithValidLogin.ContactGUID.ToString(), remember);

        }
        else
        {
            context.Response.Redirect("signin.aspx?error=invalidemail");
        }
    }
}

#endregion

#region AddtoCart

public class AddtoCart : IHttpHandler
{
    #region Declaration

    private INavigationService _navigationService = null;
    private IStringResourceService _stringResourceService = null;
    private IShoppingCartService _shoppingCartService = null;
    private IProductService _productService = null;

    #endregion

    public bool IsReusable
    {
        get { return true; }
    }

    private void InitializeDomainServices()
    {
        _navigationService = ServiceFactory.GetInstance<INavigationService>();
        _stringResourceService = ServiceFactory.GetInstance<IStringResourceService>();
        _shoppingCartService = ServiceFactory.GetInstance<IShoppingCartService>();
        _productService = ServiceFactory.GetInstance<IProductService>();
    }

    public void ProcessRequest(HttpContext context)
    {
        InitializeDomainServices();

        context.Response.CacheControl = "private";
        context.Response.Expires = 0;
        context.Response.AddHeader("pragma", "no-cache");

        var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)context.User).ThisCustomer;
        ThisCustomer.RequireCustomerRecord();

        string ReturnURL = CommonLogic.QueryStringCanBeDangerousContent("ReturnURL");
        if (ReturnURL.IndexOf("<script>", StringComparison.InvariantCultureIgnoreCase) != -1)
        {
            throw new ArgumentException("SECURITY EXCEPTION");
        }

        //Anonymous users should not be allowed to used WishList, they must register first.
        if (ThisCustomer.IsNotRegistered)
        {
            string ErrMsg = string.Empty;

            if (CommonLogic.FormNativeInt("IsWishList") == 1 || CommonLogic.QueryStringUSInt("IsWishList") == 1)
            {
                ErrMsg = AppLogic.GetString("signin.aspx.19");
                context.Response.Redirect("signin.aspx?ErrorMsg=" + ErrMsg + "&ReturnUrl=" + Security.UrlEncode(ReturnURL));
            }
        }

        string ShippingAddressID = "ShippingAddressID".ToQueryString(); // only used for multi-ship
        if (ShippingAddressID.IsNullOrEmptyTrimmed())
        {
            ShippingAddressID = CommonLogic.FormCanBeDangerousContent("ShippingAddressID");
        }

        if (ShippingAddressID.IsNullOrEmptyTrimmed() && !ThisCustomer.PrimaryShippingAddressID.IsNullOrEmptyTrimmed())
        {
            ShippingAddressID = ThisCustomer.PrimaryShippingAddressID;
        }

        string externalSource = "ExternalSource".ToQueryString();

        string itemCode = "ItemCode".ToQueryString();

        string ProductID = "ProductID".ToQueryString();
        if (ProductID.IsNullOrEmptyTrimmed())
        {
            ProductID = CommonLogic.FormCanBeDangerousContent("ProductID");
        }
        if (ProductID.IsNullOrEmptyTrimmed())
        {
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT Counter FROM InventoryItem with (NOLOCK) WHERE ItemCode = {0}", DB.SQuote(itemCode)))
                {
                    if (reader.Read())
                    {
                        ProductID = reader.ToRSFieldInt("Counter").ToString();
                    }
                }
            }
        }

        bool isVDP = false, addArtworkItem = false;
        string vdpDocumentCode = string.Empty;
        using (var con = DB.NewSqlConnection())
        {
            con.Open();
            using (var reader = DB.GetRSFormat(con, "SELECT IsVDP_DEV004817, VDPDocumentCode_DEV004817, AddArtworkFee_C FROM InventoryItem A with (NOLOCK) " +
                "INNER JOIN InventoryItemWebOption B with (NOLOCK) ON A.ItemCode = B.ItemCode WHERE A.Counter = {0}", DB.SQuote(ProductID)))
            {
                if (reader.Read())
                {
                    isVDP = reader.ToRSFieldBool("IsVDP_DEV004817");
                    vdpDocumentCode = reader.ToRSField("VDPDocumentCode_DEV004817");
                    addArtworkItem = reader.ToRSFieldBool("AddArtworkFee_C");
                }
            }
        }

        // check if the item being added is matrix group
        // look for the matrix item and use it as itemcode instead
        if (!string.IsNullOrEmpty(CommonLogic.FormCanBeDangerousContent("MatrixItem")))
        {
            itemCode = CommonLogic.FormCanBeDangerousContent("MatrixItem");
        }

        bool itemExisting = false;
        string defaultUnitMeasure = string.Empty;

        if (itemCode.IsNullOrEmptyTrimmed() || externalSource == "1")
        {
            int itemCounter = 0;
            if (!ProductID.IsNullOrEmptyTrimmed() &&
                int.TryParse(ProductID, out itemCounter) &&
                itemCounter > 0)
            {
                var validItemCodeAndBaseUnitMeasure = ServiceFactory.GetInstance<IProductService>().GetValidItemCodeAndBaseUnitMeasureById(itemCounter);
                if (validItemCodeAndBaseUnitMeasure != null)
                {
                    itemExisting = true;
                    itemCode = validItemCodeAndBaseUnitMeasure.ItemCode;
                    defaultUnitMeasure = validItemCodeAndBaseUnitMeasure.UnitMeasureCode;
                }
            }
        }
        else
        {
            // verify we have a valid item code
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT i.ItemCode FROM InventoryItem i with (NOLOCK) WHERE i.ItemCode = {0}", DB.SQuote(itemCode)))
                {
                    itemExisting = reader.Read();

                    if (itemExisting)
                    {
                        itemCode = reader.ToRSField("ItemCode");
                    }
                }
            }
        }

        if (!itemExisting)
        {
            _navigationService.NavigateToShoppingCartWitErroMessage(_stringResourceService.GetString("shoppingcart.cs.62"));
        }

        if (ThisCustomer.IsNotRegistered)
        {
            var item = _productService.GetInventoryItem(itemCode);
            if (item != null)
            {
                // do not allow unregistered customer to add giftcard and giftcertificate item to cart
                if (item.ItemType.EqualsIgnoreCase(Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CARD) ||
                    item.ItemType.EqualsIgnoreCase(Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CERTIFICATE))
                {
                    string message = AppLogic.GetString("signin.aspx.23");
                    _navigationService.NavigateToSignin(message);
                }
            }
        }

        // get the unit measure code
        string unitMeasureCode = "UnitMeasureCode".ToQueryString();
        if (unitMeasureCode.IsNullOrEmptyTrimmed())
        {
            unitMeasureCode = CommonLogic.FormCanBeDangerousContent("UnitMeasureCode");
        }

        // check if the unit measure is default so that we won't have to check
        // if the unit measure specified is valid...
        if (false.Equals(unitMeasureCode.Equals(defaultUnitMeasure, StringComparison.InvariantCultureIgnoreCase)))
        {
            bool isValidUnitMeasureForThisItem = false;

            // if no unit measure was passed use DefaultSelling
            string sqlQuery = string.Empty;
            if (unitMeasureCode.IsNullOrEmptyTrimmed())
            {
                sqlQuery = String.Format("SELECT UnitMeasureCode FROM InventoryUnitMeasure with (NOLOCK) WHERE ItemCode= {0} AND DefaultSelling = 1", DB.SQuote(itemCode));
            }
            else
            {
                sqlQuery = String.Format("SELECT UnitMeasureCode FROM InventoryUnitMeasure with (NOLOCK) WHERE ItemCode= {0} AND UnitMeasureCode = {1}", DB.SQuote(itemCode), DB.SQuote(unitMeasureCode));
            }

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, sqlQuery))
                {
                    isValidUnitMeasureForThisItem = reader.Read();

                    if (isValidUnitMeasureForThisItem)
                    {
                        // maybe mixed case specified, just set..
                        unitMeasureCode = reader.ToRSField("UnitMeasureCode");
                    }
                }
            }

            if (!isValidUnitMeasureForThisItem)
            {
                GoNextPage(context);
            }
        }
        decimal Quantity = CommonLogic.FormLocaleDecimal("Quantity", ThisCustomer.LocaleSetting);//CommonLogic.QueryStringUSDecimal("Quantity");

        if (Quantity == 0) { Quantity = CommonLogic.FormNativeDecimal("Quantity"); }
        
        if (Quantity == 0) { Quantity = "Quantity".ToQueryString().ToDecimal(); }

        if (Quantity == 0) { Quantity = 1; }

        Quantity = CommonLogic.RoundQuantity(Quantity);

        // Now let's check the shipping address if valid if specified
        if (ShippingAddressID != ThisCustomer.PrimaryShippingAddressID)
        {
            if (ThisCustomer.IsRegistered)
            {
                bool shippingAddressIsValidForThisCustomer = false;

                using (var con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (var reader = DB.GetRSFormat(con, "SELECT ShipToCode FROM CustomerShipTo with (NOLOCK) WHERE CustomerCode = {0} AND IsActive = 1 AND ShipToCode = {1}", DB.SQuote(ThisCustomer.CustomerCode), DB.SQuote(ShippingAddressID)))
                    {
                        shippingAddressIsValidForThisCustomer = reader.Read();

                        if (shippingAddressIsValidForThisCustomer)
                        {
                            // maybe mixed case, just set...
                            ShippingAddressID = reader.ToRSField("ShipToCode");
                        }
                    }
                }

                if (!shippingAddressIsValidForThisCustomer)
                {
                    GoNextPage(context);
                }
            }
            else
            {
                ShippingAddressID = ThisCustomer.PrimaryShippingAddressID;
            }
        }

        var CartType = CartTypeEnum.ShoppingCart;
        var giftRegistryItemType = GiftRegistryItemType.vItem;
        if (CommonLogic.FormNativeInt("IsWishList") == 1 || CommonLogic.QueryStringUSInt("IsWishList") == 1)
        {
            CartType = CartTypeEnum.WishCart;
        }
        else if (CommonLogic.FormNativeInt("IsAddToGiftRegistry") == 1 || CommonLogic.QueryStringUSInt("IsAddToGiftRegistry") == 1)
        {
            CartType = CartTypeEnum.GiftRegistryCart;
        }
        else if (CommonLogic.FormNativeInt("IsAddToGiftRegistryOption") == 1 || CommonLogic.QueryStringUSInt("IsAddToGiftRegistryOption") == 1)
        {
            CartType = CartTypeEnum.GiftRegistryCart;
            giftRegistryItemType = GiftRegistryItemType.vOption;
        }
        
        Guid cartRecGuid = new Guid();
        ShoppingCart cart = null;
        bool itemIsARegistryItem = false;
        if (!itemCode.IsNullOrEmptyTrimmed())
        {
            #region " --GIFTREGISTRY-- "

            if (CartType == CartTypeEnum.GiftRegistryCart)
            {
                CheckOverSizedItemForGiftRegistry(itemCode);

                Guid? registryID = CommonLogic.FormCanBeDangerousContent("giftregistryOptions").TryParseGuid();
                if (registryID.HasValue)
                {
                    var selectedGiftRegistry = ThisCustomer.GiftRegistries.FindFromDb(registryID.Value);
                    if (selectedGiftRegistry != null)
                    {
                        bool isKit = AppLogic.IsAKit(itemCode);
                        KitComposition preferredComposition = null;
                        GiftRegistryItem registryItem = null;

                        if (isKit)
                        {
                            preferredComposition = KitComposition.FromForm(ThisCustomer, itemCode);
                            var registrytems = selectedGiftRegistry.GiftRegistryItems.Where(giftItem => giftItem.ItemCode == itemCode &&
                                                                                     giftItem.GiftRegistryItemType == giftRegistryItemType);
                            Guid? matchedRegitryItemCode = null;
                            //Do this routine to check if there are kit items
                            //matched the selected kit items from the cart in the registry items
                            foreach (var regitm in registrytems)
                            {
                                regitm.IsKit = true;
                                var compositionItems = regitm.GetKitItemsFromComposition();

                                if (compositionItems.Count() == 0) continue;

                                var arrItemCodes = compositionItems.Select(item => item.ItemCode)
                                                                   .ToArray();
                                var preferredItemCodes = preferredComposition.Compositions.Select(kititem => kititem.ItemCode);
                                var lst = arrItemCodes.Except(preferredItemCodes);

                                //has match
                                if (lst.Count() == 0)
                                {
                                    matchedRegitryItemCode = regitm.RegistryItemCode;
                                    break;
                                }
                            }

                            if (matchedRegitryItemCode.HasValue)
                            {
                                registryItem = selectedGiftRegistry.GiftRegistryItems.FirstOrDefault(giftItem => giftItem.RegistryItemCode == matchedRegitryItemCode);
                            }
                        }

                        //if not kit item get the item as is
                        if (registryItem == null && !isKit)
                        {
                            registryItem = selectedGiftRegistry.GiftRegistryItems.FirstOrDefault(giftItem => giftItem.ItemCode == itemCode &&
                                                                                     giftItem.GiftRegistryItemType == giftRegistryItemType);
                        }

                        if (registryItem != null)
                        {
                            registryItem.Quantity += Quantity;
                            registryItem.UnitMeasureCode = unitMeasureCode;
                            selectedGiftRegistry.GiftRegistryItems.UpdateToDb(registryItem);
                        }
                        else
                        {
                            registryItem = new GiftRegistryItem()
                            {
                                GiftRegistryItemType = giftRegistryItemType,
                                RegistryItemCode = Guid.NewGuid(),
                                ItemCode = itemCode,
                                Quantity = Quantity,
                                RegistryID = registryID.Value,
                                UnitMeasureCode = unitMeasureCode
                            };

                            selectedGiftRegistry.GiftRegistryItems.AddToDb(registryItem);
                        }

                        if (isKit && preferredComposition != null)
                        {
                            registryItem.ClearKitItemsFromComposition();
                            preferredComposition.AddToGiftRegistry(registryID.Value, registryItem.RegistryItemCode);
                        }

                        HttpContext.Current.Response.Redirect(string.Format("~/editgiftregistry.aspx?{0}={1}", DomainConstants.GIFTREGISTRYPARAMCHAR, registryID.Value));
                    }
                }
                GoNextPage(context);
            }

            #endregion

            CartRegistryParam registryCartParam = null;
            if (AppLogic.AppConfigBool("GiftRegistry.Enabled"))
            {
                registryCartParam = new CartRegistryParam()
                {
                    RegistryID = CommonLogic.FormGuid("RegistryID"),
                    RegistryItemCode = CommonLogic.FormGuid("RegistryItemCode")
                };
            }

            if (registryCartParam != null && registryCartParam.RegistryID.HasValue && registryCartParam.RegistryItemCode.HasValue)
            {
                ShippingAddressID = GiftRegistryDA.GetPrimaryShippingAddressCodeOfOwnerByRegistryID(registryCartParam.RegistryID.Value);
                itemIsARegistryItem = true;

                //Automatically clear the itemcart with warehouse code if added a registry item.
                _shoppingCartService.ClearCartWarehouseCodeByCustomer();
            }

            cart = new ShoppingCart(null, 1, ThisCustomer, CartType, string.Empty, false, true, string.Empty);
            if (Quantity > 0)
            {
                if (AppLogic.IsAKit(itemCode))
                {
                    var preferredComposition = KitComposition.FromForm(ThisCustomer, CartType, itemCode);

                    if (preferredComposition == null)
                    {
                        int itemCounter = 0;
                        int.TryParse(ProductID, out itemCounter);
                        var kitData = KitItemData.GetKitComposition(ThisCustomer, itemCounter, itemCode, false);

                        var kitContents = new StringBuilder();
                        foreach (var kitGroup in kitData.Groups)
                        {
                            if (kitContents.Length > 0) { kitContents.Append(","); }

                            var selectedItems = new StringBuilder();
                            int kitGroupCounter = kitGroup.Id;

                            var selectedKitItems = kitGroup.Items.Where(i => i.IsSelected == true);

                            foreach (var item in selectedKitItems)
                            {
                                if (selectedItems.Length > 0) { selectedItems.Append(","); }

                                //note: since we are adding the kit counter and kit item counter in KitItemData.GetKitComposition (stored proc. EcommerceGetKitItems)
                                //as "kit item counter", we'll reverse the process in order to get the "real kit item counter"

                                int kitItemCounter = item.Id - itemCounter;
                                selectedItems.Append(kitGroupCounter.ToString() + DomainConstants.KITCOMPOSITION_DELIMITER + kitItemCounter.ToString());
                            }
                            kitContents.Append(selectedItems.ToString());
                        }
                        preferredComposition = KitComposition.FromComposition(kitContents.ToString(), ThisCustomer, CartType, itemCode);
                    }

                    preferredComposition.PricingType = CommonLogic.FormCanBeDangerousContent("KitPricingType");

                    if (CommonLogic.FormBool("IsEditKit") &&
                        !CommonLogic.IsStringNullOrEmpty(CommonLogic.FormCanBeDangerousContent("KitCartID")) &&
                        InterpriseHelper.IsValidGuid(CommonLogic.FormCanBeDangerousContent("KitCartID")))
                    {
                        cartRecGuid = new Guid(CommonLogic.FormCanBeDangerousContent("KitCartID"));
                        preferredComposition.CartID = cartRecGuid;
                    }
                    cartRecGuid = cart.AddItem(ThisCustomer, ShippingAddressID, itemCode, int.Parse(ProductID), Quantity, unitMeasureCode, CartType, preferredComposition, registryCartParam, CartTypeEnum.ShoppingCart, false, "", "", vdpDocumentCode);
                }
                else
                {
                    cartRecGuid = cart.AddItem(ThisCustomer, ShippingAddressID, itemCode, int.Parse(ProductID), Quantity, unitMeasureCode, CartType, null, registryCartParam, CartTypeEnum.ShoppingCart, false, "", "", vdpDocumentCode);
                }
                if (addArtworkItem)
                {
                    cart.AddArtworkFeeItem();
                }
            }

            string RelatedProducts = CommonLogic.QueryStringCanBeDangerousContent("relatedproducts").Trim();
            string UpsellProducts = CommonLogic.FormCanBeDangerousContent("UpsellProducts").Trim();
            string combined = string.Concat(RelatedProducts, UpsellProducts);

            if (combined.Length != 0 && CartType == CartTypeEnum.ShoppingCart)
            {
                string[] arrUpsell = combined.Split(',');
                foreach (string s in arrUpsell)
                {
                    string PID = s.Trim();
                    if (PID.Length == 0) { continue; }

                    int UpsellProductID;
                    try
                    {
                        UpsellProductID = Localization.ParseUSInt(PID);
                        if (UpsellProductID != 0)
                        {
                            string ItemCode = InterpriseHelper.GetInventoryItemCode(UpsellProductID);
                            string itemUnitMeasure = string.Empty;

                            using (var con = DB.NewSqlConnection())
                            {
                                con.Open();
                                using (var reader = DB.GetRSFormat(con, "SELECT ium.UnitMeasureCode FROM InventoryItem i with (NOLOCK) INNER JOIN InventoryUnitMeasure ium with (NOLOCK) ON i.ItemCode = ium.ItemCode AND IsBase = 1 WHERE i.ItemCode = {0}", DB.SQuote(ItemCode)))
                                {
                                    if (reader.Read())
                                    {
                                        itemUnitMeasure = DB.RSField(reader, "UnitMeasureCode");
                                    }
                                }
                            }

                            cartRecGuid = cart.AddItem(ThisCustomer, ShippingAddressID, ItemCode, UpsellProductID, 1, itemUnitMeasure, CartType);
                        }
                    }
                    catch { }
                }
            }

            // check if has query string for add service here.
            bool addService = CommonLogic.QueryStringBool("addservice"); //CommonLogic.FormCanBeDangerousContent("addservice").Trim();

            if (addService && CartType == CartTypeEnum.ShoppingCart)
            {
                string ItemCode = AppLogic.AppConfig("ExpressPrintServiceItem"); //InterpriseHelper.GetInventoryItemCode(serviceProductId);
                try
                {
                    if (ItemCode.Length > 0)
                    {

                        string itemUnitMeasure = string.Empty;
                        int serviceProductId = 0;
                        using (var con = DB.NewSqlConnection())
                        {
                            con.Open();
                            using (var reader = DB.GetRSFormat(con, "SELECT ium.UnitMeasureCode, i.Counter FROM InventoryItem i with (NOLOCK) INNER JOIN InventoryUnitMeasure ium with (NOLOCK) ON i.ItemCode = ium.ItemCode AND IsBase = 1 WHERE i.ItemCode = {0}", DB.SQuote(ItemCode)))
                            {
                                if (reader.Read())
                                {
                                    itemUnitMeasure = DB.RSField(reader, "UnitMeasureCode");
                                    serviceProductId = reader.ToRSFieldInt("Counter");
                                }
                            }
                        }
                        if (serviceProductId != 0)
                            cartRecGuid = cart.AddItem(ThisCustomer, ShippingAddressID, ItemCode, serviceProductId, 1, itemUnitMeasure, CartType);
                    }
                }
                catch { }
            }

        }
        if (isVDP == true)
        {
            CartType = CartTypeEnum.Design;
        }

        //This code handles data coming from an external source adding to cart an item manually
        string vdpCustomizationCode = "CustomizationCode".ToQueryString();
        if (externalSource == "1")
        {
            CartType = CartTypeEnum.ShoppingCart;
            DB.ExecuteSQL(string.Format("UPDATE EcommerceShoppingCart SET VDPCustomisationCode_DEV004817 = {0}, VDPType_C = 1 WHERE ShoppingCartRecGUID = {1}", DB.SQuote(vdpCustomizationCode), DB.SQuote(cartRecGuid.ToString())));
            string json = "{ \"recid\": \"" + cartRecGuid + "\" }";
            context.Response.ContentType = "application/json";
            context.Response.Write(json);
            return;
        }

        GoNextPage(context, itemIsARegistryItem, CartType, ThisCustomer, vdpDocumentCode, cartRecGuid.ToString(), Quantity);
    }

    private void CheckOverSizedItemForGiftRegistry(string itemCode)
    {
        var unitMeasures = ServiceFactory.GetInstance<IInventoryRepository>()
                                                 .GetItemBaseUnitMeasures(itemCode);
        var defaultUm = unitMeasures.FirstOrDefault();
        if (defaultUm != null)
        {
            var shippingMethodOverSize = ServiceFactory.GetInstance<IShippingService>()
                                                       .GetOverSizedItemShippingMethod(itemCode, defaultUm.Code);
            if (shippingMethodOverSize != null && shippingMethodOverSize.FreightChargeType.ToUpperInvariant() == DomainConstants.PICKUP_FREIGHT_CHARGE_TYPE)
            {
                throw new ArgumentException("Securit Error: Pickup Oversized item cannot be added as Gift Registry Item");
            }
        }
    }

    private void GoNextPage(HttpContext context, bool itemIsARegistryItem = false, CartTypeEnum cartType = CartTypeEnum.ShoppingCart, Customer ThisCustomer = null, string vdpDocumentCode = "", string cartGUID = "", decimal quantity = 0)
    {
        string ReturnURL = CommonLogic.QueryStringCanBeDangerousContent("ReturnURL");
        if (ReturnURL.IndexOf("<script>", StringComparison.InvariantCultureIgnoreCase) != -1)
        {
            throw new ArgumentException("SECURITY EXCEPTION");
        }

        if (CommonLogic.FormNativeInt("IsWishList") == 1 || CommonLogic.QueryStringUSInt("IsWishList") == 1)
        {
            cartType = CartTypeEnum.WishCart;
        }

        bool isAddRegistryItem = (cartType == CartTypeEnum.ShoppingCart && itemIsARegistryItem);
        if ((isAddRegistryItem) ||
                ("STAY".Equals(AppLogic.AppConfig("AddToCartAction"), StringComparison.InvariantCultureIgnoreCase) && ReturnURL.Length != 0))
        {
            string addedParam = string.Empty;
            if (isAddRegistryItem)
            {
                addedParam = "&" + DomainConstants.NOTIFICATION_QRY_STRING_PARAM + "=" + AppLogic.GetString("editgiftregistry.aspx.48");
            }
            context.Response.Redirect(ReturnURL + addedParam);
        }
        else
        {
            if (ReturnURL.Length == 0)
            {
                ReturnURL = string.Empty;
                if (context.Request.UrlReferrer != null)
                {
                    ReturnURL = context.Request.UrlReferrer.AbsoluteUri; // could be null
                }
                if (ReturnURL == null)
                {
                    ReturnURL = string.Empty;
                }
            }
            if (cartType == CartTypeEnum.WishCart)
            {
                context.Response.Redirect("wishlist.aspx?ReturnUrl=" + Security.UrlEncode(ReturnURL));
            }
            if (cartType == CartTypeEnum.GiftRegistryCart)
            {
                context.Response.Redirect("giftregistry.aspx?ReturnUrl=" + Security.UrlEncode(ReturnURL));
            }
            if (cartType == CartTypeEnum.Design)
            {
                if (CommonLogic.GetWebsiteType() == WebsiteType.Smartbag)
                {
                    context.Response.Redirect(string.Format("expressprint.aspx?dcode={0}&recid={1}&qty={2}&referrer=shoppingcart.aspx", vdpDocumentCode, cartGUID, quantity));
                }
                else if (CommonLogic.GetWebsiteType() == WebsiteType.Company)
                {
                    context.Response.Redirect(string.Format("design.aspx?dcode={0}&recid={1}&referrer=shoppingcart.aspx", vdpDocumentCode, cartGUID));
                }
            }
            context.Response.Redirect("ShoppingCart.aspx?add=true&ReturnUrl=" + Security.UrlEncode(ReturnURL));
        }
    }
}
#endregion

#region "PortalAddToCart"
public class PortalAddtoCart : IHttpHandler
{
    public bool IsReusable
    {
        get { return false; }
    }

    public void ProcessRequest(HttpContext context)
    {
        context.Response.CacheControl = "private";
        context.Response.Expires = 0;
        context.Response.AddHeader("pragma", "no-cache");

        Customer ThisCustomer = ((InterpriseSuiteEcommercePrincipal)context.User).ThisCustomer;
        ThisCustomer.RequireCustomerRecord();

        string itemName = context.Request.Form["ItemName"];

        if (ThisCustomer.IsNotRegistered)
        {
            string ReturnURL = CommonLogic.QueryStringCanBeDangerousContent("ReturnURL");
            string ErrMsg = string.Empty;
            ErrMsg = AppLogic.GetString("signin.aspx.19");
            context.Response.Redirect("login.aspx?ErrorMsg=" + ErrMsg + "&ReturnUrl=" + Security.UrlEncode(ReturnURL));
        }

        //Uncomment to enable add to cart
        //InterpriseShoppingCart cart = new InterpriseShoppingCart(null, 1, ThisCustomer, CartTypeEnum.ShoppingCart, String.Empty, false, true);

        //foreach (string key in context.Request.Form.AllKeys)
        //{
        //    try
        //    {
        //        if (!key.StartsWith("Counter")) { continue; }
        //        string counter = context.Request.Form[key];
        //        string itemCode = context.Request.Form["ItemCode_" + counter];
        //        string qty = context.Request.Form["Quantity_" + counter];
        //        if (qty.Trim().Length > 0)
        //        {
        //            if (qty.ToDecimal() > 0)
        //            {
        //                UnitMeasureInfo? umInfo = null;
        //                umInfo = InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);
        //                cart.AddItem(ThisCustomer, ThisCustomer.PrimaryShippingAddressID, itemCode, Convert.ToInt16(counter), qty.ToDecimal(), umInfo.Value.Code, CartTypeEnum.ShoppingCart);
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        // do nothing, add the items that we can
        //    }
        //}
        context.Response.Redirect("portalcheckoutreview.aspx");
    }
}
#endregion

#region ExecXmlPackage
/// <summary>
/// Outputs the raw package results along with setting any http headers specified in the package.
/// The package transform output method needs to match the Content-Type http header or you may not get the results you expect
/// </summary>
public class ExecXmlPackage : IHttpHandler
{
    public bool IsReusable
    {
        get { return true; }
    }

    public void ProcessRequest(HttpContext context)
    {
        string pn = CommonLogic.QueryStringCanBeDangerousContent("xmlpackage");
        Customer ThisCustomer = ((InterpriseSuiteEcommercePrincipal)context.User).ThisCustomer;
        try
        {
            using (XmlPackage2 p = new XmlPackage2(pn, ThisCustomer, ThisCustomer.SkinID, "", XmlPackageParam.FromString(""), "", true))
            {
                if (!p.AllowEngine)
                {
                    context.Response.Write("This XmlPackage is not allowed to be run from the engine.  Set the package element's allowengine attribute to true to enable this package to run.");
                }
                else
                {
                    if (p.HttpHeaders != null)
                    {
                        foreach (XmlNode xn in p.HttpHeaders)
                        {
                            string headername = xn.Attributes["headername"].InnerText;
                            string headervalue = xn.Attributes["headervalue"].InnerText;
                            context.Response.AddHeader(headername, headervalue);
                        }
                    }
                    string output = p.TransformString();
                    context.Response.AddHeader("Content-Length", output.Length.ToString());
                    context.Response.Write(output);
                }
            }
        }
        catch (Exception ex)
        {
            context.Response.Write(ex.Message + "<br/><br/>");
            Exception iex = ex.InnerException;
            while (iex != null)
            {
                context.Response.Write(ex.Message + "<br/><br/>");
                iex = iex.InnerException;
            }
        }
    }
}
#endregion

#region PaypalExpressCheckoutPostback

public class PaypalExpressCheckoutPostback : IHttpHandler
{
    public bool IsReusable
    {
        get { return true; }
    }

    public void ProcessRequest(HttpContext context)
    {

        if (Customer.Current.ThisCustomerSession["paypalfrom"] == "onlinepayment")
        {
            context.Response.Redirect(String.Format("payment.aspx?invoicecode={0}&PayPal=True&token={1}&amount={2}", context.Request.QueryString["invoicecode"], context.Request.QueryString["token"], context.Request.QueryString["amount"]));
        }

        var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)context.User).ThisCustomer;

        var m_PayPalExpress = new PayPalExpress();
        //Get PayPal info
        var PayPalDetails = m_PayPalExpress.GetExpressCheckoutDetails(context.Request.QueryString["token"]).GetExpressCheckoutDetailsResponseDetails;
        var paypalShippingAddress = Address.New(ThisCustomer, AddressTypes.Shipping);

        if (PayPalDetails.PayerInfo.Address.Name.IsNullOrEmptyTrimmed() && (PayPalDetails.PayerInfo.Address.Street1.IsNullOrEmptyTrimmed() || PayPalDetails.PayerInfo.Address.Street2.IsNullOrEmptyTrimmed()) &&
            PayPalDetails.PayerInfo.Address.CityName.IsNullOrEmptyTrimmed() && PayPalDetails.PayerInfo.Address.StateOrProvince.IsNullOrEmptyTrimmed() && PayPalDetails.PayerInfo.Address.PostalCode.IsNullOrEmptyTrimmed() &&
            PayPalDetails.PayerInfo.Address.CountryName.ToString().IsNullOrEmptyTrimmed() || PayPalDetails.PayerInfo.ContactPhone.IsNullOrEmptyTrimmed())
        {
            paypalShippingAddress = ThisCustomer.PrimaryShippingAddress;
        }
        else
        {
            string streetAddress = PayPalDetails.PayerInfo.Address.Street1 + (!PayPalDetails.PayerInfo.Address.Street2.IsNullOrEmptyTrimmed() ? Environment.NewLine : String.Empty) + PayPalDetails.PayerInfo.Address.Street2;
            string sql = String.Empty;
            if (ThisCustomer.IsRegistered)
            {
                sql = String.Format("SELECT COUNT(ShipToCode) AS N FROM CustomerShipTo where Address = {0} and City = {1} and State = {2} and PostalCode = {3} and Country = {4} and ShipToName = {5} and CustomerCode = {6}",
                                streetAddress.ToDbQuote(), PayPalDetails.PayerInfo.Address.CityName.ToDbQuote(), PayPalDetails.PayerInfo.Address.StateOrProvince.ToDbQuote(), PayPalDetails.PayerInfo.Address.PostalCode.ToDbQuote(),
                                AppLogic.ResolvePayPalAddressCode(PayPalDetails.PayerInfo.Address.CountryName).ToString().ToDbQuote(), PayPalDetails.PayerInfo.Address.Name.ToDbQuote(), ThisCustomer.CustomerCode.ToDbQuote());
            }
            else
            {
                sql = String.Format("SELECT COUNT(1) AS N FROM EcommerceAddress where ShipToAddress = {0} and ShipToCity = {1} and ShipToState = {2} and ShipToPostalCode = {3} and ShipToCountry = {4} and ShipToName = {5} and CustomerID = {6}",
                                streetAddress.ToDbQuote(), PayPalDetails.PayerInfo.Address.CityName.ToDbQuote(), PayPalDetails.PayerInfo.Address.StateOrProvince.ToDbQuote(), PayPalDetails.PayerInfo.Address.PostalCode.ToDbQuote(),
                                AppLogic.ResolvePayPalAddressCode(PayPalDetails.PayerInfo.Address.CountryName).ToString().ToDbQuote(), PayPalDetails.PayerInfo.Address.Name.ToDbQuote(), ThisCustomer.CustomerCode.ToDbQuote());

                paypalShippingAddress.EMail = ThisCustomer.IsRegistered ? ThisCustomer.EMail : ServiceFactory.GetInstance<ICustomerService>().GetAnonEmail();
                paypalShippingAddress.Name = PayPalDetails.PayerInfo.Address.Name;
                paypalShippingAddress.Address1 = PayPalDetails.PayerInfo.Address.Street1 + (PayPalDetails.PayerInfo.Address.Street2 != String.Empty ? Environment.NewLine : String.Empty) + PayPalDetails.PayerInfo.Address.Street2;
                paypalShippingAddress.City = PayPalDetails.PayerInfo.Address.CityName;
                paypalShippingAddress.State = PayPalDetails.PayerInfo.Address.StateOrProvince;
                paypalShippingAddress.PostalCode = PayPalDetails.PayerInfo.Address.PostalCode;
                paypalShippingAddress.Country = AppLogic.ResolvePayPalAddressCode(PayPalDetails.PayerInfo.Address.CountryName.ToString());
                paypalShippingAddress.ResidenceType = ThisCustomer.PrimaryShippingAddress.ResidenceType;
                paypalShippingAddress.Phone = PayPalDetails.PayerInfo.ContactPhone ?? String.Empty;

            }

            int isAddressExists = DB.GetSqlN(sql);

            if (AppLogic.AppConfigBool("PayPalCheckout.RequireConfirmedAddress") || isAddressExists == 0)
            {
                ServiceFactory.GetInstance<ICustomerService>()
                              .AssignPayPalExpressCheckoutNoteInSalesOrderNote();
            }
        }

        ThisCustomer.PrimaryShippingAddress = paypalShippingAddress;
        paypalShippingAddress.Save();

        string redirectUrl = String.Empty;

        //Checking for redirectURL of PayPal -- Express Checkout button in Shopping Cart page or PayPal Radio Button in Payment Page

        if (Customer.Current.ThisCustomerSession["paypalfrom"] == "shoppingcart" || Customer.Current.ThisCustomerSession["paypalfrom"] == "checkoutanon")
        {
            redirectUrl = "checkoutshipping.aspx?PayPal=True&token=" + context.Request.QueryString["token"];
        }
        else
        {
            if (AppLogic.AppConfigBool("Checkout.UseOnePageCheckout"))
            {
                if (!AppLogic.AppConfigBool("Checkout.UseOnePageCheckout.UseFinalReviewOrderPage"))
                {
                    //Insert PayPal call here for response - For authorize and capture of order from paypal inside IS
                    ThisCustomer.ThisCustomerSession["paypalfrom"] = "onepagecheckout";
                    string OrderNumber = String.Empty;
                    string status = String.Empty;
                    string receiptCode = String.Empty;
                    var billingAddress = ThisCustomer.PrimaryBillingAddress;
                    Address shippingAddress = null;
                    var cart = new InterpriseShoppingCart(null, ThisCustomer.SkinID, ThisCustomer, CartTypeEnum.ShoppingCart, String.Empty, false, true);
                    if (cart.IsNoShippingRequired())
                    {
                        cart.BuildSalesOrderDetails(false, true);
                    }
                    else
                    {
                        cart.BuildSalesOrderDetails();
                    }

                    if (!AppLogic.AppConfigBool("PayPalCheckout.OverrideAddress"))
                    {
                        if (!cart.HasShippableComponents())
                        {
                            shippingAddress = ThisCustomer.PrimaryShippingAddress;
                        }
                        else
                        {
                            if (ThisCustomer.IsRegistered)
                            {
                                var GetShippingAddress = new Address()
                                {
                                    Name = PayPalDetails.PayerInfo.Address.Name,
                                    Address1 = PayPalDetails.PayerInfo.Address.Street1 + (PayPalDetails.PayerInfo.Address.Street2 != String.Empty ? Environment.NewLine : String.Empty) + PayPalDetails.PayerInfo.Address.Street2,
                                    City = PayPalDetails.PayerInfo.Address.CityName,
                                    State = PayPalDetails.PayerInfo.Address.StateOrProvince,
                                    PostalCode = PayPalDetails.PayerInfo.Address.PostalCode,
                                    Country = AppLogic.ResolvePayPalAddressCode(PayPalDetails.PayerInfo.Address.CountryName.ToString()),
                                    CountryISOCode = AppLogic.ResolvePayPalAddressCode(PayPalDetails.PayerInfo.Address.Country.ToString()),
                                    Phone = PayPalDetails.PayerInfo.ContactPhone ?? String.Empty
                                };
                                shippingAddress = GetShippingAddress;
                            }
                            else
                            {
                                shippingAddress = paypalShippingAddress;
                            }
                        }
                    }

                    var doExpressCheckoutResp = m_PayPalExpress.DoExpressCheckoutPayment(PayPalDetails.Token, PayPalDetails.PayerInfo.PayerID, OrderNumber, cart);
                    string result = String.Empty;
                    if (doExpressCheckoutResp.Errors != null && !doExpressCheckoutResp.Errors[0].ErrorCode.IsNullOrEmptyTrimmed())
                    {
                        if (AppLogic.AppConfigBool("ShowGatewayError"))
                        {
                            result = String.Format(AppLogic.GetString("shoppingcart.aspx.27"), doExpressCheckoutResp.Errors[0].ErrorCode, doExpressCheckoutResp.Errors[0].LongMessage);
                        }
                        else
                        {
                            result = AppLogic.GetString("shoppingcart.aspx.28");
                        }

                        context.Response.Redirect("shoppingcart.aspx?ErrorMsg=" + result.ToUrlEncode(), false);
                        return;
                    }
                    else
                    {
                        Gateway gatewayToUse = null;
                        var payPalResp = new GatewayResponse(String.Empty)
                        {
                            AuthorizationCode = doExpressCheckoutResp.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].TransactionID,
                            TransactionResponse = doExpressCheckoutResp.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].PaymentStatus.ToString(),
                            Details = doExpressCheckoutResp.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].PaymentStatus.ToString(),
                            AuthorizationTransID = doExpressCheckoutResp.DoExpressCheckoutPaymentResponseDetails.PaymentInfo[0].TransactionID
                        };

                        InterpriseHelper.UpdateCustomerPaymentTerm(ThisCustomer, DomainConstants.PAYMENT_METHOD_CREDITCARD);
                        status = cart.PlaceOrder(gatewayToUse, billingAddress, shippingAddress, ref OrderNumber, ref receiptCode, true, true, payPalResp, true, false);

                        ThisCustomer.ThisCustomerSession["paypalFrom"] = String.Empty;
                        ThisCustomer.ThisCustomerSession["notesFromPayPal"] = String.Empty;
                        ThisCustomer.ThisCustomerSession["anonymousCustomerNote"] = String.Empty;

                        if (status != AppLogic.ro_OK)
                        {
                            ThisCustomer.IncrementFailedTransactionCount();
                            if (ThisCustomer.FailedTransactionCount >= AppLogic.AppConfigUSInt("MaxFailedTransactionCount"))
                            {
                                cart.ClearTransaction();
                                ThisCustomer.ResetFailedTransactionCount();
                                context.Response.Redirect("orderfailed.aspx");
                            }
                            ThisCustomer.ClearTransactions(false);
                            context.Response.Redirect("checkout1.aspx?paymentterm=" + ThisCustomer.PaymentTermCode + "&errormsg=" + status.ToUrlEncode());
                        }

                        AppLogic.ClearCardNumberInSession(ThisCustomer);
                        ThisCustomer.ClearTransactions(true);

                        context.Response.Redirect(String.Format("orderconfirmation.aspx?ordernumber={0}", OrderNumber.ToUrlEncode()));
                    }
                }
                else
                {
                    InterpriseHelper.UpdateCustomerPaymentTerm(ThisCustomer, DomainConstants.PAYMENT_METHOD_CREDITCARD);
                    redirectUrl = "checkoutreview.aspx?PayPal=True&token=" + context.Request.QueryString["token"];
                }
            }
            else
            {
                InterpriseHelper.UpdateCustomerPaymentTerm(ThisCustomer, DomainConstants.PAYMENT_METHOD_CREDITCARD);
                redirectUrl = "checkoutreview.aspx?PayPal=True&token=" + context.Request.QueryString["token"];
            }
        }
        string filename = String.Format("{0}\\{1}_{2}_{3}_paypal.txt", HttpContext.Current.Server.MapPath("~"), ThisCustomer.SkinID,
                                               ThisCustomer.LocaleSetting,
                                               InterpriseHelper.ConfigInstance.WebSiteCode);
        if (CommonLogic.FileExists(filename)) { File.Delete(filename); }
        if (!CommonLogic.FileExists(filename))
        {
            using (StreamWriter _testData = new StreamWriter(filename, true))
            {
                _testData.WriteLine(redirectUrl + " : " + DateTime.Now.ToShortDateString() + DateTime.Now.ToShortTimeString()); // Write the file.
            }
        }

        context.Response.Redirect(redirectUrl);
    }

}
#endregion

#region SagePayNotification

public class SagePayNotification : IHttpHandler
{
    public bool IsReusable
    {
        get { return true; }
    }

    public void ProcessRequest(HttpContext context)
    {
        var requestPost = context.Request.Form;

        string responseStatus = String.Empty;
        string redirectURL = String.Empty;
        bool isUseOnePageCheckout = ServiceFactory.GetInstance<IAppConfigService>().CheckoutUseOnePageCheckout;
        var thisCustomer = ServiceFactory.GetInstance<IAuthenticationService>().LoginFromSagePay(CommonLogic.QueryStringCanBeDangerousContent("contactguid"));

        if (requestPost["Status"] == DomainConstants.SAGEPAY_RESPONSE_STATUS_OK)
        {
            string securityKey = String.Format("\r\n" + "SecurityKey={0}", thisCustomer.ThisCustomerSession["SecurityKey"]);
            var sagepayResponse = new GatewayResponse(String.Empty)
            {
                AuthorizationCode = requestPost["TxAuthNo"],
                TransactionResponse = requestPost.ToString().ToUrlDecode().Replace("&", "\r\n") + securityKey,
                Details = requestPost["StatusDetail"],
                AuthorizationTransID = requestPost["VPSTxId"],
                AVSResult = requestPost["AVSCV2"],
                CV2Result = requestPost["CV2Result"],
                Status = requestPost["Status"],
                ExpirationMonth = requestPost["ExpiryDate"].Substring(0, 2),
                ExpirationYear = requestPost["ExpiryDate"].Substring(2, 2)
            };

            string sagepayResponseJSON = ServiceFactory.GetInstance<ICryptographyService>().SerializeToJson<GatewayResponse>(sagepayResponse);
            thisCustomer.ThisCustomerSession["sagepayResponseJSON"] = sagepayResponseJSON;

            responseStatus = DomainConstants.SAGEPAY_RESPONSE_STATUS_OK;
            redirectURL = "sagepayredirect.aspx";
        }
        else if (requestPost["Status"] == DomainConstants.SAGEPAY_RESPONSE_STATUS_ABORT)
        {
            responseStatus = DomainConstants.SAGEPAY_RESPONSE_STATUS_OK;
            if (isUseOnePageCheckout)
            {
                redirectURL = "checkout1.aspx";
            }
            else
            {
                redirectURL = "checkoutpayment.aspx";
            }
        }
        else if (requestPost["Status"] == DomainConstants.SAGEPAY_RESPONSE_STATUS_NOTAUTHED)
        {
            responseStatus = DomainConstants.SAGEPAY_RESPONSE_STATUS_OK;
            if (isUseOnePageCheckout)
            {
                redirectURL = String.Format("checkout1.aspx?errormsg={0}", String.Format(AppLogic.GetString("checkout1.aspx.110", AppLogic.GetCurrentSkinID(), Customer.Current.LocaleSetting, true),
                                                                                        requestPost["Status"] + "+" + requestPost["StatusDetail"]).ToUrlEncode());
            }
            else
            {
                redirectURL = String.Format("checkoutpayment.aspx?errormsg={0}", String.Format(AppLogic.GetString("checkoutpayment.aspx.66", AppLogic.GetCurrentSkinID(), Customer.Current.LocaleSetting, true),
                                                                                        requestPost["Status"] + "+" + requestPost["StatusDetail"]).ToUrlEncode());
            }
        }
        else
        {
            responseStatus = DomainConstants.SAGEPAY_RESPONSE_STATUS_INVALID;
            if (isUseOnePageCheckout)
            {
                redirectURL = String.Format("checkout1.aspx?errormsg={0}", String.Format(AppLogic.GetString("checkout1.aspx.110", AppLogic.GetCurrentSkinID(), Customer.Current.LocaleSetting, true),
                                                                                        requestPost["Status"] + "+" + requestPost["StatusDetail"]).ToUrlEncode());
            }
            else
            {
                redirectURL = String.Format("checkoutpayment.aspx?errormsg={0}", String.Format(AppLogic.GetString("checkoutpayment.aspx.66", AppLogic.GetCurrentSkinID(), Customer.Current.LocaleSetting, true),
                                                                                        requestPost["Status"] + "+" + requestPost["StatusDetail"]).ToUrlEncode());
            }
        }

        context.Response.Clear();
        context.Response.ClearHeaders();
        context.Response.ContentType = "text/plain";
        context.Response.Write(String.Format("Status={0}\n", responseStatus));

        if (!thisCustomer.ThisCustomerSession["IsRequestingFromMobile"].IsNullOrEmptyTrimmed())
        {
            context.Response.Write(String.Format("RedirectURL={0}{1}", CurrentContext.FullyQualifiedMobileApplicationPath() + "/", redirectURL));
        }
        else
        {
            context.Response.Write(String.Format("RedirectURL={0}{1}", CurrentContext.FullyQualifiedApplicationPath(), redirectURL));
        }
    }
}

#endregion

#region SagePayOrderCreation

public class SagePayOrderCreation : IHttpHandler
{
    public bool IsReusable
    {
        get { return true; }
    }

    public void ProcessRequest(HttpContext context)
    {
        var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)context.User).ThisCustomer;

        var cart = new InterpriseShoppingCart(null, 1, ThisCustomer, CartTypeEnum.ShoppingCart, String.Empty, false, true);
        cart.BuildSalesOrderDetails();

        var deserializedSagePayResponse = ServiceFactory.GetInstance<ICryptographyService>().DeserializeJson<GatewayResponse>(ThisCustomer.ThisCustomerSession["sagepayResponseJSON"]);

        string orderNumber = String.Empty;
        string receiptCode = String.Empty;
        string orderStatus = String.Empty;
        var billingAddress = ThisCustomer.PrimaryBillingAddress;
        var shippingAddress = ThisCustomer.PrimaryShippingAddress;

        billingAddress.CardExpirationMonth = deserializedSagePayResponse.ExpirationMonth;
        billingAddress.CardExpirationYear = System.Threading.Thread.CurrentThread
                                                                   .CurrentCulture.Calendar.ToFourDigitYear(int.Parse(deserializedSagePayResponse.ExpirationYear)).ToString();

        InterpriseHelper.UpdateCustomerPaymentTerm(ThisCustomer, ServiceFactory.GetInstance<IAppConfigService>().SagePayPaymentTerm);
        orderStatus = cart.PlaceOrder(null, billingAddress, shippingAddress, ref orderNumber, ref receiptCode, true, true, deserializedSagePayResponse, true, false);

        if (orderStatus == AppLogic.ro_OK)
        {
            ThisCustomer.ClearTransactions(true);
            context.Response.Redirect(String.Format("orderconfirmation.aspx?ordernumber={0}", orderNumber.ToUrlEncode()));
        }
        else
        {
            if (ServiceFactory.GetInstance<IAppConfigService>().CheckoutUseOnePageCheckout)
            {
                context.Response.Redirect(String.Format("checkout1.aspx?paymentTerm={0}&errormsg={1}", ServiceFactory.GetInstance<IAppConfigService>().SagePayPaymentTerm, orderStatus.ToUrlEncode()));
            }
            else
            {
                context.Response.Redirect(String.Format("checkoutpayment.aspx?paymentTerm={0}&errormsg={1}", ServiceFactory.GetInstance<IAppConfigService>().SagePayPaymentTerm, orderStatus.ToUrlEncode()));
            }
        }
    }
}

#endregion

#region Create Quote
public class CreateQuote : IHttpHandler
{
    public bool IsReusable
    {
        get { return true; }
    }

    public void ProcessRequest(HttpContext context)
    {
        string OrderNumber = string.Empty, receiptCode = string.Empty, paymentTerm = string.Empty;
        var ThisCustomer = ((InterpriseSuiteEcommercePrincipal)context.User).ThisCustomer;
        Address shippingAddress = null, billingAddress = null;
        shippingAddress = ThisCustomer.PrimaryShippingAddress;
        shippingAddress.Name = CommonLogic.FormCanBeDangerousContent("FirstName") + " " + CommonLogic.FormCanBeDangerousContent("LastName");
        shippingAddress.Company = CommonLogic.FormCanBeDangerousContent("Company");
        shippingAddress.Address1 = CommonLogic.FormCanBeDangerousContent("WorkAddress");
        shippingAddress.City = CommonLogic.FormCanBeDangerousContent("WorkCity");
        shippingAddress.State = CommonLogic.FormCanBeDangerousContent("WorkState");
        shippingAddress.PostalCode = CommonLogic.FormCanBeDangerousContent("WorkZip");
        shippingAddress.Phone = CommonLogic.FormCanBeDangerousContent("WorkPhone");
        shippingAddress.EMail = CommonLogic.FormCanBeDangerousContent("EmailAddress");
        shippingAddress.Country = "Australia";
        shippingAddress.ShippingMethodGroup = ThisCustomer.PrimaryShippingAddress.ShippingMethodGroup;
        shippingAddress.PaymentTermGroup = ThisCustomer.PrimaryShippingAddress.PaymentTermGroup;

        billingAddress = ThisCustomer.PrimaryShippingAddress;
        billingAddress.Name = shippingAddress.Name;
        billingAddress.CardName = shippingAddress.Name;
        billingAddress.Company = shippingAddress.Company;
        billingAddress.Address1 = shippingAddress.Address1;
        billingAddress.City = shippingAddress.City;
        billingAddress.State = shippingAddress.State;
        billingAddress.Country = shippingAddress.Country;
        billingAddress.PostalCode = shippingAddress.PostalCode;
        billingAddress.Phone = shippingAddress.Phone;
        billingAddress.EMail = shippingAddress.EMail;
        var cart = new InterpriseShoppingCart(null, ThisCustomer.SkinID, ThisCustomer, CartTypeEnum.ShoppingCart, String.Empty, false, true);
        var cartitem = new CustomCart();
        var cartitems = new List<CustomCart>();
        cartitem.ItemCode = CommonLogic.FormCanBeDangerousContent("ItemCode");
        cartitem.Quantity = CommonLogic.FormCanBeDangerousContent("ItemQuantity").ToDecimal();
        cartitem.Comments = CommonLogic.FormCanBeDangerousContent("ItemComments");
        cartitem.Colour = CommonLogic.FormCanBeDangerousContent("itemColour");
        cartitems.Add(cartitem);

        //if (cart.IsNoShippingRequired())
        //{
        //    cart.BuildSalesOrderDetails(false, true);
        //}
        //else
        //{
        //    cart.BuildSalesOrderDetails();
        //}

        var buildcart = new BuildSalesOrderDetails(ThisCustomer, billingAddress, shippingAddress, cartitems);

        string status = string.Empty;
        try
        {
            paymentTerm = ThisCustomer.PaymentTermCode;
            ThisCustomer.PaymentTermCode = "REQUEST QUOTE";
            //var status = cart.PlaceOrder(null, ThisCustomer.PrimaryBillingAddress, ThisCustomer.PrimaryShippingAddress, ref OrderNumber, ref receiptCode, true, true, null, false, false);
            status = buildcart.PlaceOrder(ref OrderNumber, billingAddress, shippingAddress);
            ThisCustomer.PaymentTermCode = paymentTerm;
            if (status == AppLogic.ro_OK)
            {
                string PM = AppLogic.CleanPaymentMethod(ThisCustomer.PaymentMethod);
                AppLogic.SendOrderEMail(ThisCustomer, cart, OrderNumber, false, PM, false);
            }
        }
        catch (Exception ex)
        {
            context.Response.Redirect("pageError.aspx?Parameter=" + Security.UrlEncode(ex.Message));
        }
        //context.Response.Redirect("orderconfirmation.aspx?ordernumber=" + Security.UrlEncode("QU-000012"));
        if (status == AppLogic.ro_OK)
        {
            context.Response.Redirect("orderconfirmation.aspx?ordernumber=" + Security.UrlEncode(OrderNumber));
        }
        else
        {
            context.Response.Redirect("pageError.aspx?Parameter=" + Security.UrlEncode("Problem creating a Quote. Please send email with detail request."));
        }
    }
}

#endregion