﻿using DevExpress.XtraReports.Web;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System;

public partial class producthistory : System.Web.UI.Page
{
    Customer thisCustomer = Customer.Current;
    protected void Page_Load(object sender, EventArgs e)
    {
        //RequiresLogin(CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables("QUERY_STRING"));
        ServiceFactory.GetInstance<IRequestCachingService>()
                      .PageNoCache();

        if (thisCustomer.IsNotRegistered)
        {
            Response.Redirect("login.aspx?returnurl=producthistory.aspx");
        }
        InitializeControls();
        var reportcode = AppLogic.AppConfig("custom.hn.producthistory.report");
        string itemName = Request.Form["txtItemName"], storeName = Request.Form["txtStoreName"], movex = Request.Form["txtMovex"], invoiceDateFrom = Request.Form["txtInvoiceDateFrom"], invoiceDateTo = Request.Form["txtInvoiceDateTo"];
        if (itemName == null)
        {
            itemName = "";
            itemName=thisCustomer.ThisCustomerSession["ItemName"];
        }
        else
        {
            thisCustomer.ThisCustomerSession["ItemName"] = itemName;
        }
        
        if (storeName == null)
        {
            storeName = "";
            storeName=thisCustomer.ThisCustomerSession["StoreName"];
        }
        else
        {
            thisCustomer.ThisCustomerSession["StoreName"] = storeName;
        }
        
        if (movex == null)
        {
            movex = "";
            movex=thisCustomer.ThisCustomerSession["Movex"];
        }
        else
        {
            thisCustomer.ThisCustomerSession["Movex"] = movex;
        }
        
        if (invoiceDateFrom == null)
        {
            invoiceDateFrom = "";
            invoiceDateFrom = thisCustomer.ThisCustomerSession["InvoiceDateFrom"];
        }
        else
        {
            thisCustomer.ThisCustomerSession["InvoiceDateFrom"] = invoiceDateFrom;
        }
        if (invoiceDateTo == null)
        {
            invoiceDateTo = "";
            invoiceDateTo = thisCustomer.ThisCustomerSession["InvoiceDateTo"];
        }
        else
        {
            thisCustomer.ThisCustomerSession["InvoiceDateTo"] = invoiceDateTo;
        }
        try
        {
            ViewerReport.Report = InterpriseHelper.LoadProductHistoryReport(reportcode, itemName, storeName, movex, invoiceDateFrom, invoiceDateTo, thisCustomer.CostCenter);
            ViewerReport.AutoSize = true;

        }
        catch (Exception ex)
        {
            ltReport.Text = "No data";
            ltReport.Visible = true;
            thisCustomer.ThisCustomerSession["FirstLoad"] = "false";
            thisCustomer.ThisCustomerSession["ItemName"] = "";
            thisCustomer.ThisCustomerSession["StoreName"] = "";
            thisCustomer.ThisCustomerSession["Movex"] = "";
            thisCustomer.ThisCustomerSession["InvoiceDateFrom"] = "";
            thisCustomer.ThisCustomerSession["InvoiceDateTo"] = "";
        }
        finally
        {
            if (thisCustomer.ThisCustomerSession["FirstLoad"] == "true")
            {
                thisCustomer.ThisCustomerSession["FirstLoad"] = "false";
                thisCustomer.ThisCustomerSession["ItemName"] = "";
                thisCustomer.ThisCustomerSession["StoreName"] = "";
                thisCustomer.ThisCustomerSession["Movex"] = "";
                thisCustomer.ThisCustomerSession["InvoiceDateFrom"] = "";
                thisCustomer.ThisCustomerSession["InvoiceDateTo"] = "";
            }
            else
            {
                thisCustomer.ThisCustomerSession["FirstLoad"] = "true";
            }
        }
    }

    protected void ViewerReport_CacheReportDocument(object sender, CacheReportDocumentEventArgs e)
    {
        e.Key = Guid.NewGuid().ToString();
        Page.Session[e.Key] = e.SaveDocumentToMemoryStream();
    }

    protected void ViewerReport_RestoreReportDocumentFromCache(object sender, RestoreReportDocumentFromCacheEventArgs e)
    {
        //Clear stored value.
        thisCustomer.ThisCustomerSession.ClearVal("ItemName");
        thisCustomer.ThisCustomerSession.ClearVal("StoreName");
        thisCustomer.ThisCustomerSession.ClearVal("Movex");
        thisCustomer.ThisCustomerSession.ClearVal("InvoiceDate");
    }

    private void InitializeControls()
    {
        switch (thisCustomer.CostCenter)
        {
            case "Smartbag-PREMIUM CUE":
            case "Smartbag-PREMIUM DION LEE":
            case "Smartbag-PREMIUM APG":
                ltMovex.Text = "Store ID";
                break;
        }
    }
}