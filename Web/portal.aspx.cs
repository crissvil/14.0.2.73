﻿using InterpriseSuiteEcommerceCommon;
using System;

namespace InterpriseSuiteEcommerce
{
    public partial class portal : SkinBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RequiresLogin(CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables("QUERY_STRING"));
            this.HeaderMsg.SetContext = this;
            this.SETitle = "Welcome to the smartportal";
            litPromo.Text = AppLogic.GetHomeProductPromo(ThisCustomer.CustomerCode);
        }
    }
}