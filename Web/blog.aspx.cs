﻿using InterpriseSuiteEcommerceCommon;
using System;

namespace InterpriseSuiteEcommerce
{
    public partial class blog : SkinBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (AppLogic.AppConfigBool("GoNonSecureAgain"))
            {
                SkinBase.GoNonSecureAgain();
            }
            SectionTitle = AppLogic.GetString("blog.aspx.1");

            // set the Customer context, and set the SkinBase context, so meta tags to be set if they are not blank in the XmlPackage results
            XmlPackage1.SetContext = this;

            var xslt = new XSLTExtensionBase(ThisCustomer, 1);
            xslt.ExtractBody("");
        }
    }
}