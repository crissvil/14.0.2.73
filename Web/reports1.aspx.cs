﻿using DevExpress.XtraReports.Web;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System;
using System.IO;

namespace InterpriseSuiteEcommerce
{
    public partial class reports1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //RequiresLogin(CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables("QUERY_STRING"));
            ServiceFactory.GetInstance<IRequestCachingService>()
                          .PageNoCache();

            Customer thisCustomer = Customer.Current;
            if (thisCustomer.IsNotRegistered)
            {
                Response.Redirect("login.aspx?returnurl=reports.aspx");
            }
            var reportcode = AppLogic.AppConfig("custom.hn.report");
            //RPT-000026 = Item Sale Report Year

            string dateFrom = Request.Form["txtDateFrom"], dateTo = Request.Form["txtDateTo"], asOf = Request.Form["txtAsOf"], type = Request.Form["drpDate"];
            //if (dateFrom == null)
            //{
            //    dateFrom = "";
            //    thisCustomer.ThisCustomerSession["DateFrom"] = dateFrom;
            //}
            //if (dateTo == null)
            //{
            //    dateTo = "";
            //    thisCustomer.ThisCustomerSession["DateTo"] = dateTo;
            //}
            //if (asOf == null)
            //{
            //    asOf = "";
            //    thisCustomer.ThisCustomerSession["AsOf"] = asOf;
            //}

            //This code is required to store date selected value as the page load 2x removing the date values.
            //if (dateFrom != "" || asOf != "")
            //{
            //    thisCustomer.ThisCustomerSession["DateFrom"] = dateFrom;
            //    thisCustomer.ThisCustomerSession["DateTo"] = dateTo;
            //    thisCustomer.ThisCustomerSession["AsOf"] = asOf;
            //}
            //else
            //{
            //    dateFrom = thisCustomer.ThisCustomerSession["DateFrom"];
            //    dateTo = thisCustomer.ThisCustomerSession["DateTo"];
            //    asOf = thisCustomer.ThisCustomerSession["AsOf"];
            //}

            try
            {
                //ViewerReport.Report = InterpriseHelper.LoadReport(reportcode, type, CommonLogic.IIF(type == "0", dateFrom, asOf), dateTo);
                ViewerReport.Report = InterpriseHelper.SOHReport();
                ViewerReport.AutoSize = true;
            }
            catch (Exception ex)
            {
                ltReport.Text = "No data";
                ltReport.Visible = true;
                //thisCustomer.ThisCustomerSession["FirstLoad"] = "false";
                //thisCustomer.ThisCustomerSession["DateFrom"] = "";
                //thisCustomer.ThisCustomerSession["DateTo"] = "";
                //thisCustomer.ThisCustomerSession["AsOf"] = "";
            }
            //finally
            //{
            //    if (thisCustomer.ThisCustomerSession["FirstLoad"] == "true")
            //    {
            //        thisCustomer.ThisCustomerSession["FirstLoad"] = "false";
            //        thisCustomer.ThisCustomerSession["DateFrom"] = "";
            //        thisCustomer.ThisCustomerSession["DateTo"] = "";
            //        thisCustomer.ThisCustomerSession["AsOf"] = "";
            //    }
            //    else
            //    {
            //        thisCustomer.ThisCustomerSession["FirstLoad"] = "true";
            //    }
            //}
        }

        protected void ViewerReport_CacheReportDocument(object sender, CacheReportDocumentEventArgs e)
        {
            e.Key = Guid.NewGuid().ToString();
            Page.Session[e.Key] = e.SaveDocumentToMemoryStream();
        }
        protected void ViewerReport_RestoreReportDocumentFromCache(object sender, RestoreReportDocumentFromCacheEventArgs e)
        {
            //Clear stored value.
            Customer thisCustomer = Customer.Current;
            thisCustomer.ThisCustomerSession.ClearVal("DateFrom");
            thisCustomer.ThisCustomerSession.ClearVal("DateTo");
        }
    }
}