<%@ Page Language="c#" Inherits="InterpriseSuiteEcommerce.sitemap" CodeFile="sitemap.aspx.cs" %>
<%@ Register TagPrefix="ise" TagName="XmlPackage" src="XmlPackageControl.ascx" %>
<html>
<head>
</head>
<body>
    <form id="frmSitemap" method="post" runat="server">
        <h1>Sitemap</h1>
        <h3><a href="">Products</a></h3>
       <asp:Panel runat="server" ID="PackagePanel" Visible="True">
        <ise:xmlpackage id="XmlPackage1" runat="server"
            enforcedisclaimer="true" enforcepassword="True" enforcesubscription="true" allowsepropogation="True" />
        </asp:Panel>
        <asp:Panel runat="server" ID="EntityPanel" Visible="False">
            <asp:Literal runat="Server" ID="Literal1" />
        </asp:Panel>
    </form>
</body>
</html>
