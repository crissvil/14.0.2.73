﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="designit.aspx.cs" Inherits="InterpriseSuiteEcommerce.designit" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <style type="text/css">
        body {
            font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        }

        .loader-wrapper {
            margin: auto;
            text-align: center;
            width: 400px;
            height: 180px;
            position: fixed;
            top: -100px;
            left: 0;
            right: 0;
            bottom: 0;
        }

            .loader-wrapper span {
                display: block;
            }

        .loaderanimated {
            animation-duration: 1s;
            animation-fill-mode: both;
            width: 100px;
        }

            .loaderanimated.infinite {
                animation-iteration-count: infinite;
            }

        @keyframes loaderbounce {
            from, 20%, 53%, 80%, to {
                animation-timing-function: cubic-bezier(0.215, 0.61, 0.355, 1);
                transform: translate3d(0, 0, 0);
            }

            40%, 43% {
                animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);
                transform: translate3d(0, -30px, 0);
            }

            70% {
                animation-timing-function: cubic-bezier(0.755, 0.05, 0.855, 0.06);
                transform: translate3d(0, -15px, 0);
            }

            90% {
                transform: translate3d(0, -4px, 0);
            }
        }

        .loaderbounce {
            animation-name: bounce;
            transform-origin: center bottom;
        }
    </style>
    <link rel="stylesheet" href="https://s3-ap-southeast-2.amazonaws.com/smartbag-dev/composer-0.0.1.css" />
</head>
<body>
    <div class="loader-wrapper">
        <img class="loaderanimated loaderbounce infinite" src="https://smartbag-edit-assets.s3-ap-southeast-2.amazonaws.com/images/smartbag-logo.svg" />
        <span>Please wait, something awesome is loading.</span>
    </div>
    <div id="wysiwyg"></div>

    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script type="text/javascript" src="https://s3-ap-southeast-2.amazonaws.com/smartbag-dev/composer-0.0.1.js"></script>
    <script type="text/javascript">
        var formSubmitting = false;

        window.onload = function () {
            window.addEventListener("beforeunload", function (e) {
                if (formSubmitting) {
                    return undefined;
                }

                var confirmationMessage = 'If you leave this page, changes will be lost.';

                (e || window.event).returnValue = confirmationMessage;
                return confirmationMessage;
            });
        };

        let urlParams = new URLSearchParams(window.location.search);
        let templateId = urlParams.get('dcode');
        let customizationId = urlParams.get('ccode');
        let recid = urlParams.get('recid');
        let qty = urlParams.get('qty');

        if (templateId !== null && templateId !== "") {
            axios.post('https://866i7txb31.execute-api.ap-southeast-2.amazonaws.com/prod/customization/new', {
                templateId: templateId,
                apiKey: '6df901c2-ddfd-4a36-b47a-27463dc2f877',
                apiSecret: '3154dc86-d799-424a-84e7-239258ec6fb6'
            })
            .then(function (response) {

                var editor = new Composer.Editor({
                    customizationId: response.data.customizationId,
                    token: '281bae60-322d-11e8-88f2-e511b83fea25', // token - access token which allows access to the api, will expire

                    quantity: qty,

                    prevPage: 'expressprint.aspx?dcode=' + templateId,

                    isEdit: false,

                    onLoaded: function (sender) {
                        // this event will be triggered when the editor
                        // setup and initialization has completed
                        // parameter: sender - instance of the Editor class

                        // once the setup and resources have loaded
                        // create the WYSIWYG html

                        sender.setWYSIWYG('wysiwyg');
                    },
                    onCustomizationSubmit: function (sender) {
                        // this method will be triggered when 
                        // the customization is submitted
                        // parameteter: sender - instance of the Editor class
                        formSubmitting = true;
                        var url = 'designit.aspx?ccode=' + sender.customizationId + '&recid=' + recid + '&qty=' + sender.quantity + '&process=save';
                        window.location = url;
                        // save sender.customizationId here
                        console.log(sender);
                    },
                });
            })
            .catch(function (error) {
                console.log(error);
            });
        } else if (customizationId !== null && customizationId !== "") {

            var editor = new Composer.Editor({
                customizationId: customizationId,
                token: '281bae60-322d-11e8-88f2-e511b83fea25',

                quantity: qty,

                prevPage: 'expressprint.aspx?ccode=' + customizationId,

                isEdit: true,

                onLoaded: function (sender) {
                    // this event will be triggered when the editor
                    // setup and initialization has completed
                    // parameter: sender - instance of the Editor class

                    // once the setup and resources have loaded
                    // create the WYSIWYG html

                    sender.setWYSIWYG('wysiwyg');
                },
                onCustomizationSubmit: function (sender) {
                    // this method will be triggered when 
                    // the customization is submitted
                    // parameteter: sender - instance of the Editor class
                    formSubmitting = true;
                    var url = 'designit.aspx?ccode=' + sender.customizationId + '&recid=' + recid + '&qty=' + sender.quantity + '&process=edit';
                    window.location = url;
                    // save sender.customizationId here
                    console.log(sender);
                },
            });
        }

    </script>
</body>
</html>
