﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Tool;

namespace InterpriseSuiteEcommerceGateways
{
    public class SagePayPayment
    {
        public static string NextPageURL { get; set; }
        public static string ServerPaymentResultURL { get; set; }
        public static IAppConfigService _appConfigService = ServiceFactory.GetInstance<IAppConfigService>();

        public static class SagePaySettings
        {
            public static string VendorName
            {
                get { return _appConfigService.SagePayVendorName; }
            }

            public static string TransactionMode
            {
                get { return _appConfigService.SagePayTransactionMode; }
            }

            public static string SagePayPaymentURL
            {
                get
                {
                    string sagePayPaymentURL = String.Empty;

                    if (TransactionMode.ToUpperInvariant() == "TEST")
                    {
                        sagePayPaymentURL = "https://test.sagepay.com/gateway/service/vspserver-register.vsp";
                    }
                    else
                    {
                        sagePayPaymentURL = "https://live.sagepay.com/gateway/service/vspserver-register.vsp";
                    }

                    return sagePayPaymentURL;
                }
            }
        }

        public static string SetSagePayServerPaymentRequest(InterpriseShoppingCart cart)
        {
            string validAddress = ValidateAddress();

            if (!validAddress.IsNullOrEmptyTrimmed())
            {
                return validAddress;
            }

            if (SagePaySettings.VendorName.IsNullOrEmptyTrimmed())
            {
                if (CurrentContext.IsRequestingFromMobileMode(Customer.Current))
                {
                    return String.Format("checkoutpayment.aspx?ErrorMsg={0}", ServiceFactory.GetInstance<IStringResourceService>().GetString("mobile.checkoutpayment.aspx.18", true).ToUrlEncode());
                }
                else
                {
                    if (ServiceFactory.GetInstance<IAppConfigService>().CheckoutUseOnePageCheckout)
                    {
                        return String.Format("checkout1.aspx?ErrorMsg={0}", ServiceFactory.GetInstance<IStringResourceService>().GetString("checkout1.aspx.122", true).ToUrlEncode());
                    }
                    else
                    {
                        return String.Format("checkoutpayment.aspx?ErrorMsg={0}", ServiceFactory.GetInstance<IStringResourceService>().GetString("checkoutpayment.aspx.81", true).ToUrlEncode());
                    }
                }
            }

            var requestCommand = new StringBuilder();
            requestCommand.Append("VPSProtocol=" + "3.00");
            requestCommand.Append("&TxType=" + "DEFERRED");
            requestCommand.Append("&Vendor=" + SagePaySettings.VendorName);
            requestCommand.Append("&VendorTxCode=" + GenerateVendorTxCode());
            requestCommand.Append("&Amount=" + cart.GetOrderBalance());
            requestCommand.Append("&Currency=" + cart.ThisCustomer.CurrencyCode);
            requestCommand.Append("&Description=" + (_appConfigService.StoreName + " Order").ToUrlEncode());

            string billingAddressSalutation = String.Empty;
            string billingAddressFirstName = String.Empty;
            string billingAddressMiddleName = String.Empty;
            string billingAddressLastName = String.Empty;
            string billingAddressSuffix = String.Empty;

            string shippingAddressSalutation = String.Empty;
            string shippingAddressFirstName = String.Empty;
            string shippingAddressMiddleName = String.Empty;
            string shippingAddressLastName = String.Empty;
            string shippingAddressSuffix = String.Empty;

            if (Customer.Current.IsNotRegistered)
            {
                Customer.Current.PrimaryBillingAddress.CountryISOCode = AppLogic.GetCountryTwoLetterISOCode(Customer.Current.PrimaryBillingAddress.Country);
            }

            string cleanedCustomerBillingName = Customer.Current.PrimaryBillingAddress.Name.ReplaceSpecialCharacters();
            string cleanedCustomerShippingName = Customer.Current.PrimaryShippingAddress.Name.ReplaceSpecialCharacters();

            ServiceFactory.GetInstance<ICustomerService>().ParseName(cleanedCustomerBillingName,
                                                                     ref billingAddressSalutation,
                                                                     ref billingAddressFirstName,
                                                                     ref billingAddressMiddleName,
                                                                     ref billingAddressLastName,
                                                                     ref billingAddressSuffix);

            ServiceFactory.GetInstance<ICustomerService>().ParseName(cleanedCustomerShippingName,
                                                                     ref shippingAddressSalutation,
                                                                     ref shippingAddressFirstName,
                                                                     ref shippingAddressMiddleName,
                                                                     ref shippingAddressLastName,
                                                                     ref shippingAddressSuffix);

            requestCommand.Append("&BillingSurname=" + ((billingAddressLastName.IsNullOrEmptyTrimmed()) ? Customer.Current.LastName : billingAddressLastName));
            requestCommand.Append("&BillingFirstnames=" + ((billingAddressLastName.IsNullOrEmptyTrimmed()) ? Customer.Current.FirstName : billingAddressFirstName));
            requestCommand.Append("&BillingAddress1=" + Customer.Current.PrimaryBillingAddress.Address1.ToUrlEncode());
            requestCommand.Append("&BillingCity=" + Customer.Current.PrimaryBillingAddress.City);
            requestCommand.Append("&BillingPostCode=" + TrimPostalCode(Customer.Current.PrimaryBillingAddress.PostalCode).ToUrlEncode());
            requestCommand.Append("&BillingCountry=" + Customer.Current.PrimaryBillingAddress.CountryISOCode);
            if (Customer.Current.PrimaryBillingAddress.IsWithState)
            {
                requestCommand.Append("&BillingState=" + Customer.Current.PrimaryBillingAddress.State);
            }

            requestCommand.Append("&DeliverySurname=" + ((shippingAddressLastName.IsNullOrEmptyTrimmed()) ? Customer.Current.LastName : shippingAddressLastName));
            requestCommand.Append("&DeliveryFirstnames=" + ((shippingAddressLastName.IsNullOrEmptyTrimmed()) ? Customer.Current.FirstName : shippingAddressFirstName));
            requestCommand.Append("&DeliveryAddress1=" + Customer.Current.PrimaryShippingAddress.Address1.ToUrlEncode());
            requestCommand.Append("&DeliveryCity=" + Customer.Current.PrimaryShippingAddress.City);
            requestCommand.Append("&DeliveryPostCode=" + TrimPostalCode(Customer.Current.PrimaryShippingAddress.PostalCode).ToUrlEncode());
            requestCommand.Append("&DeliveryCountry=" + Customer.Current.PrimaryShippingAddress.CountryISOCode);
            if (Customer.Current.PrimaryShippingAddress.IsWithState)
            {
                requestCommand.Append("&DeliveryState=" + Customer.Current.PrimaryShippingAddress.State);
            }

            if (IsOrderTotalEqual(cart))
            {
                requestCommand.Append("&BasketXML=" + BasketXML(cart));
            }

            requestCommand.Append("&Profile=" + "NORMAL");

            string contactGUID = String.Empty;
            if (Customer.Current != null)
            {
                contactGUID = "?contactguid=" + Customer.Current.ContactGUID;
            }

            requestCommand.Append("&NotificationURL=" + CurrentContext.FullyQualifiedApplicationPath() + ("sagepaynotification.aspx" + contactGUID));

            var encoding = new ASCIIEncoding();
            var requestData = encoding.GetBytes(requestCommand.ToString());
            try
            {
                string rawResponseString = String.Empty;

                var myRequest = (HttpWebRequest)WebRequest.Create(SagePaySettings.SagePayPaymentURL);
                myRequest.Method = "POST";
                myRequest.ContentType = "application/x-www-form-urlencoded";
                myRequest.ContentLength = requestData.Length;
                var newStream = myRequest.GetRequestStream();
                // Send the data.
                newStream.Write(requestData, 0, requestData.Length);
                newStream.Close();
                // get the response
                var myResponse = myRequest.GetResponse();
                using (var sr = new StreamReader(myResponse.GetResponseStream()))
                {
                    rawResponseString = sr.ReadToEnd();
                    sr.Close();
                }
                myResponse.Close();

                string status = String.Empty;
                string statusDetail = String.Empty;
                var statusArray = rawResponseString.Split(new String[1] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = statusArray.GetLowerBound(0); i <= statusArray.GetUpperBound(0); i++)
                {
                    var lastKeyPair = statusArray[i].Split(new char[] { '=' }, 2, StringSplitOptions.None);
                    switch (lastKeyPair[0].ToLowerInvariant())
                    {
                        case "statusdetail": statusDetail = lastKeyPair[1];
                            break;
                        case "securitykey": Customer.Current.ThisCustomerSession["SecurityKey"] = lastKeyPair[1];
                            break;
                        case "status": status = lastKeyPair[1];
                            break;
                        case "nexturl": NextPageURL = lastKeyPair[1];
                            break;
                    }
                }

                if (status != DomainConstants.SAGEPAY_RESPONSE_STATUS_OK)
                {
                    string redirectPage = String.Empty;
                    if (ServiceFactory.GetInstance<IAppConfigService>().CheckoutUseOnePageCheckout)
                    {
                        redirectPage = String.Format("checkout1.aspx?errormsg={0}", String.Format(ServiceFactory.GetInstance<IStringResourceService>().GetString("checkout1.aspx.110", true), status + "+" + statusDetail).ToUrlEncode());
                    }
                    else
                    {
                        redirectPage = String.Format("checkoutpayment.aspx?errormsg={0}", String.Format(ServiceFactory.GetInstance<IStringResourceService>().GetString("checkoutpayment.aspx.66", true), status + "+" + statusDetail).ToUrlEncode());
                    }
                    NextPageURL = String.Format("{0}", redirectPage);
                }

                if (CurrentContext.IsRequestingFromMobileMode(Customer.Current))
                {
                    Customer.Current.ThisCustomerSession["IsRequestingFromMobile"] = "true";
                }

                return NextPageURL;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GenerateVendorTxCode()
        {
            var random = new Random();

            string vendorName = _appConfigService.SagePayVendorName;

            TimeSpan ts = (DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc));
            // 18 char max -13 chars - 6 chars
            return string.Format("{0}-{1}-{2}",
                vendorName.Substring(0, Math.Min(18, vendorName.Length)),
                (long)ts.TotalMilliseconds, random.Next(100000, 999999));
        }

        private static string BasketXML(InterpriseShoppingCart cart)
        {
            decimal totalDiscountPerItem = Decimal.Zero;
            decimal totalOrderDiscount = Decimal.Zero;

            var basketXml = new StringBuilder();
            basketXml.Append("<basket>");
            foreach (var cartItem in cart.CartItems)
            {
                basketXml.Append("<item>");
                basketXml.AppendFormat("<description>{0}</description>", cartItem.ItemDescription.ReplaceSpecialCharacters());
                basketXml.AppendFormat("<quantity>{0}</quantity>", Localization.ParseLocaleDecimal(cartItem.m_Quantity, cartItem.ThisCustomer.LocaleSetting));

                var lineItemRow = cartItem.AssociatedLineItemRow;
                decimal vat = Decimal.Zero;
                decimal salesPriceRate = Decimal.Zero;


                if (cart.HasCoupon())
                {
                    totalDiscountPerItem = lineItemRow.ExtPriceRate.ToCustomerRoundedCurrency();
                }

                if (lineItemRow.QuantityOrdered > 1)
                {
                    lineItemRow["QuantityOrdered"] = 1;
                    cart.SalesOrderFacade.ComputeTransactionDetail(lineItemRow, Interprise.Framework.Base.Shared.Enum.TransactionType.SalesOrder);
                    lineItemRow["QuantityOrdered"] = cartItem.m_Quantity;
                }

                salesPriceRate = lineItemRow.SalesPriceRate.ToCustomerRoundedCurrency();
                vat = lineItemRow.SalesTaxAmountRate.ToCustomerRoundedCurrency();
                totalOrderDiscount += totalDiscountPerItem;

                basketXml.AppendFormat("<unitNetAmount>{0}</unitNetAmount>", salesPriceRate);
                basketXml.AppendFormat("<unitTaxAmount>{0}</unitTaxAmount>", vat);
                basketXml.AppendFormat("<unitGrossAmount>{0}</unitGrossAmount>", (salesPriceRate + vat).ToCustomerRoundedCurrency());
                basketXml.AppendFormat("<totalGrossAmount>{0}</totalGrossAmount>", ((salesPriceRate + vat) * cartItem.m_Quantity).ToCustomerRoundedCurrency());
                basketXml.Append("</item>");
            }

            basketXml.AppendFormat("<deliveryNetAmount>{0}</deliveryNetAmount>", cart.SalesOrderDataset.CustomerSalesOrderView[0].FreightRate.ToCustomerRoundedCurrency());
            basketXml.AppendFormat("<deliveryTaxAmount>{0}</deliveryTaxAmount>", cart.SalesOrderDataset.CustomerSalesOrderView[0].FreightTaxRate.ToCustomerRoundedCurrency());
            basketXml.AppendFormat("<deliveryGrossAmount>{0}</deliveryGrossAmount>", (cart.SalesOrderDataset.CustomerSalesOrderView[0].FreightRate + cart.SalesOrderDataset.CustomerSalesOrderView[0].FreightTaxRate).ToCustomerRoundedCurrency());

            if (cart.HasCoupon())
            {
                basketXml.Append("<discounts>");
                basketXml.Append("<discount>");
                basketXml.AppendFormat("<fixed>{0}</fixed>", totalOrderDiscount.ToCustomerRoundedCurrency());
                basketXml.AppendFormat("<description>{0}</description>", cart.ThisCustomer.CouponCode);
                basketXml.Append("</discount>");
                basketXml.Append("</discounts>");
            }

            basketXml.Append("</basket>");
            return basketXml.ToString();
        }

        private static bool IsOrderTotalEqual(InterpriseShoppingCart cart)
        {
            decimal orderTotal = Decimal.Zero;
            decimal totalOrderDiscount = Decimal.Zero;
            decimal totalDiscountPerItem = Decimal.Zero;

            foreach (var cartItem in cart.CartItems)
            {
                var cartItemRow = cartItem.AssociatedLineItemRow;

                if (cart.HasCoupon())
                {
                    totalDiscountPerItem = cartItemRow.ExtPriceRate.ToCustomerRoundedCurrency();
                }

                if (cartItemRow.QuantityOrdered > 1)
                {
                    cartItemRow.QuantityOrdered = 1;
                    cart.SalesOrderFacade.ComputeTransactionDetail(cartItemRow, Interprise.Framework.Base.Shared.Enum.TransactionType.SalesOrder);
                    cartItemRow.QuantityOrdered = cartItem.m_Quantity;
                }

                decimal netAmount = cartItemRow.SalesPriceRate.ToCustomerRoundedCurrency();
                decimal originalAmount = cartItemRow.SalesPriceRate.ToCustomerRoundedCurrency() * cartItemRow.QuantityOrdered.ToCustomerRoundedCurrency();
                decimal taxAmount = cartItemRow.SalesTaxAmountRate.ToCustomerRoundedCurrency();

                decimal grossAmount = netAmount + taxAmount;
                decimal totalGrossAmount = grossAmount * cartItem.m_Quantity;

                totalOrderDiscount += totalDiscountPerItem;
            }

            decimal deliveryGrossAmount = cart.SalesOrderDataset.CustomerSalesOrderView[0].FreightRate.ToCustomerRoundedCurrency() + cart.SalesOrderDataset.CustomerSalesOrderView[0].FreightTaxRate.ToCustomerRoundedCurrency();
            decimal cartTotal = orderTotal + deliveryGrossAmount - totalOrderDiscount;

            return cartTotal == cart.GetOrderBalance();
        }

        private static string TrimPostalCode(string customerPostalCode)
        {
            string trimmedPostalCode = String.Empty;
            if (customerPostalCode.Length > 10)
            {
                if (customerPostalCode.Contains(" "))
                {
                    string[] postalCodeArray = customerPostalCode.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string postalCode in postalCodeArray)
                    {
                        trimmedPostalCode += postalCode.FirstOrDefault();
                    }
                }
                else
                {
                    trimmedPostalCode = customerPostalCode.Substring(0, 10);
                }
            }
            else
            {
                trimmedPostalCode = customerPostalCode;
            }

            return trimmedPostalCode;
        }

        private static string ValidateAddress()
        {
            var customerBillingAddress = Customer.Current.PrimaryBillingAddress;
            var customerShippingAddress = Customer.Current.PrimaryShippingAddress;
            var requiredFields = new StringBuilder();

            if (customerBillingAddress.Name.IsNullOrEmptyTrimmed())
            {
                requiredFields.AppendLine("[" + ServiceFactory.GetInstance<IStringResourceService>().GetString("createaccount.aspx.39", true) + "]");
            }
                
            if (customerBillingAddress.Address1.IsNullOrEmptyTrimmed())
            {
                requiredFields.Append("[" + ServiceFactory.GetInstance<IStringResourceService>().GetString("createaccount.aspx.40", true) + "]");
            }

            if (customerBillingAddress.City.IsNullOrEmptyTrimmed())
            {
                requiredFields.Append("[" + ServiceFactory.GetInstance<IStringResourceService>().GetString("createaccount.aspx.69", true) + "]");
            }

            if (customerBillingAddress.IsWithState)
            {
                if (customerBillingAddress.State.IsNullOrEmptyTrimmed())
                {
                    requiredFields.Append("[" + ServiceFactory.GetInstance<IStringResourceService>().GetString("address.cs.23", true) + "]");
                }
            }

            if (customerBillingAddress.PostalCode.IsNullOrEmptyTrimmed())
            {
                requiredFields.Append("[" + ServiceFactory.GetInstance<IStringResourceService>().GetString("createaccount.aspx.70", true) + "]");
            }

            if (customerBillingAddress.CountryISOCode.IsNullOrEmptyTrimmed())
            {
                requiredFields.Append("[" + ServiceFactory.GetInstance<IStringResourceService>().GetString("createaccount.aspx.103", true) + "]");
            }

            if (customerShippingAddress.Name.IsNullOrEmptyTrimmed())
            {
                requiredFields.Append("[" + ServiceFactory.GetInstance<IStringResourceService>().GetString("createaccount.aspx.58", true) + "]");
            }
                
            if (customerShippingAddress.Address1.IsNullOrEmptyTrimmed())
            {
                requiredFields.Append("[" + ServiceFactory.GetInstance<IStringResourceService>().GetString("createaccount.aspx.59", true) + "]");
            }

            if (customerShippingAddress.City.IsNullOrEmptyTrimmed())
            {
                requiredFields.Append("[" + ServiceFactory.GetInstance<IStringResourceService>().GetString("createaccount.aspx.71", true) + "]");
            }

            if (customerShippingAddress.IsWithState)
            {
                if (customerShippingAddress.State.IsNullOrEmptyTrimmed())
                {
                    requiredFields.Append("[" + ServiceFactory.GetInstance<IStringResourceService>().GetString("address.cs.22", true) + "]");
                }
            }

            if (customerShippingAddress.PostalCode.IsNullOrEmptyTrimmed())
            {
                requiredFields.Append("[" + ServiceFactory.GetInstance<IStringResourceService>().GetString("createaccount.aspx.72", true) + "]");
            }

            if (customerShippingAddress.CountryISOCode.IsNullOrEmptyTrimmed())
            {
                requiredFields.Append("[" + ServiceFactory.GetInstance<IStringResourceService>().GetString("createaccount.aspx.103", true) + "]");
            }

            if (!requiredFields.IsNullOrEmptyTrimmed() && requiredFields.Length > 0)
            {
                if (CurrentContext.IsRequestingFromMobileMode(Customer.Current))
                {
                    return String.Format("checkoutpayment.aspx?ErrorMsg={0}", String.Format(ServiceFactory.GetInstance<IStringResourceService>().GetString("mobile.checkoutpayment.aspx.17", true), requiredFields).ToUrlEncode());
                }
                else
                {
                    if (ServiceFactory.GetInstance<IAppConfigService>().CheckoutUseOnePageCheckout)
                    {
                        return String.Format("checkout1.aspx?ErrorMsg={0}", String.Format(ServiceFactory.GetInstance<IStringResourceService>().GetString("checkout1.aspx.121", true), requiredFields).ToUrlEncode());
                    }
                    else
                    {
                        return String.Format("checkoutpayment.aspx?ErrorMsg={0}", String.Format(ServiceFactory.GetInstance<IStringResourceService>().GetString("checkoutpayment.aspx.80", true), requiredFields).ToUrlEncode());
                    }
                }
            }
            else
            {
                return String.Empty;
            }
        }
    }
}
