// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using InterpriseSuiteEcommerceCommon;
using System.Web.UI.WebControls;

namespace InterpriseSuiteEcommerceControls.Validators
{
    public class DropDownValidator : InputValidator
    {
        #region Variable Declaration
        
        public const int DEFAULT_INVALID_INDEX = 0;
        private int _invalidIndex = DEFAULT_INVALID_INDEX;
        private DropDownList _controlToValidate = null;

        #endregion

        public DropDownValidator(DropDownList controlToValidate, string errorMessage)
            : this(controlToValidate, DEFAULT_INVALID_INDEX, errorMessage)
        {
        }

        public DropDownValidator(DropDownList controlToValidate, int invalidIndex, string errorMessage)
            : this(controlToValidate, invalidIndex, errorMessage, null)
        { 
        }

        public DropDownValidator(DropDownList controlToValidate, int invalidIndex, string errorMessage, InputValidator next)
            : base(errorMessage, next)
        {
            _controlToValidate = controlToValidate;
            _invalidIndex = invalidIndex;
        }

        public override bool SupportsClientSideValidation
        {
            get { return true; }
        }

        protected override bool EvaluateIsValid()
        {
            return _controlToValidate.SelectedIndex != _invalidIndex;
        }

        public override string RenderInitialization()
        {
            return string.Format(
                    "new ise.Validators.DropDownListValidator('{0}', {1}, {2}, '{3}')",
                    _controlToValidate.ClientID,
                    _invalidIndex,
                    _controlToValidate.Items.Count,
                    this.ErrorMessage,
                    RenderChainedValidatorInitialization()
                );
        }
    }
}

