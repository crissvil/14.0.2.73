// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InterpriseSuiteEcommerceControls.Validators
{
    public class RegularExpressionInputValidator : InputValidator
    {
        private TextBox _controlToValidate;
        private string _regExp = string.Empty;

        public RegularExpressionInputValidator(TextBox controlToValidate, string expression, string errorMessage)
            : this(controlToValidate, expression, errorMessage, null)
        {
        }

        public RegularExpressionInputValidator(TextBox controlToValidate, string expression, string errorMessage, InputValidator next)
            : base(errorMessage, next)
        {
            _controlToValidate = controlToValidate;
            _regExp = expression;
        }

        public override bool SupportsClientSideValidation
        {
            get { return false; }
        }

        protected override bool EvaluateIsValid()
        {
            if (!string.IsNullOrEmpty(_regExp))
            {
                Regex regEx = new Regex(_regExp);
                return regEx.IsMatch(_controlToValidate.Text);
            }

            return true;
        }
    }
}
