// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InterpriseSuiteEcommerceControls.Validators
{
    public class CompareInputValidator : InputValidator
    {
        private TextBox _controlToValidate;
        private TextBox _controlToCompareWith;

        public CompareInputValidator(TextBox controlToValidate, TextBox controlToCompareWith, string errorMessage)
            : this(controlToValidate, controlToCompareWith, errorMessage, null)
        {
        }

        public CompareInputValidator(TextBox controlToValidate, TextBox controlToCompareWith, string errorMessage, InputValidator next)
            : base(errorMessage, next)
        {
            _controlToValidate = controlToValidate;
            _controlToCompareWith = controlToCompareWith;
        }

        public override bool SupportsClientSideValidation
        {
            get { return true; }
        }

        protected override bool EvaluateIsValid()
        {
            return _controlToValidate.Text == _controlToCompareWith.Text;
        }

        public override string RenderInitialization()
        {
            return
                string.Format(
                    "new ise.Validators.CompareValidator('{0}', '{1}', '{2}', {3})",
                    _controlToValidate.ClientID,
                    _controlToCompareWith.ClientID,
                    this.ErrorMessage,
                    RenderChainedValidatorInitialization()
                );
        }
    }
}
