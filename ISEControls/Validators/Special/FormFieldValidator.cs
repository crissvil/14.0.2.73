// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.IO;
using InterpriseSuiteEcommerceCommon;

namespace InterpriseSuiteEcommerceControls.Validators.Special
{
    public class FormFieldValidator : InputValidator
    {
        private string _fieldId = string.Empty;

        public FormFieldValidator(string fieldId, string errorMessage) : this(fieldId, errorMessage, null){}

        public FormFieldValidator(string fieldId, string errorMessage, InputValidator next) : base(errorMessage, next)
        {
        }

        public override bool SupportsClientSideValidation
        {
            get { return true; }
        }

        protected override bool EvaluateIsValid()
        {
            if (null != HttpContext.Current.Request.Form[_fieldId])
            {
                return !CommonLogic.IsStringNullOrEmpty(HttpContext.Current.Request.Form[_fieldId]);
            }
            else
            {
                return false;
            }
        }

        public override string RenderInitialization()
        {
            return base.RenderInitialization();
        }
    }
}
