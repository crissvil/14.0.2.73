// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using InterpriseSuiteEcommerceCommon;
using System.Web.UI.WebControls;

namespace InterpriseSuiteEcommerceControls.Validators.Special
{
    public class CreditCardNumberByTypeValidator : InputValidator
    {
        #region Variable Declaration

        private TextBox _controlToValidate = null;
        private DropDownList _controlToValidateTypeWith = null;
        private string _unKnownCardTypeErrorMessage = string.Empty;
        private string _noCardNumberProvidedErrorMessage = string.Empty;
        private string _cardNumberInvalidFormatErrorMessage = string.Empty;
        private string _cardNumberInvalidErrorMessage = string.Empty;
        private string _cardNumberInAppropriateNumberOfDigitsErrorMessage = string.Empty;

        #endregion

        public CreditCardNumberByTypeValidator(TextBox controlToValidate, DropDownList controlToValidateTypeWith, string unKnownCardTypeErrorMessage, string noCardNumberProvidedErrorMessage, string cardNumberInvalidFormatErrorMessage, string cardNumberInvalidErrorMessage, string cardNumberInAppropriateNumberOfDigitsErrorMessage)
            : this(controlToValidate, controlToValidateTypeWith,unKnownCardTypeErrorMessage, noCardNumberProvidedErrorMessage, cardNumberInvalidFormatErrorMessage, cardNumberInvalidErrorMessage, cardNumberInAppropriateNumberOfDigitsErrorMessage, null) { }

        public CreditCardNumberByTypeValidator(TextBox controlToValidate, DropDownList controlToValidateTypeWith, string unKnownCardTypeErrorMessage, string noCardNumberProvidedErrorMessage, string cardNumberInvalidFormatErrorMessage, string cardNumberInvalidErrorMessage, string cardNumberInAppropriateNumberOfDigitsErrorMessage, InputValidator next) : base(string.Empty, next)
        {
            _controlToValidate = controlToValidate;
            _controlToValidateTypeWith = controlToValidateTypeWith;

            _unKnownCardTypeErrorMessage = unKnownCardTypeErrorMessage;
            _noCardNumberProvidedErrorMessage = noCardNumberProvidedErrorMessage;
            _cardNumberInvalidFormatErrorMessage = cardNumberInvalidFormatErrorMessage;
            _cardNumberInvalidErrorMessage = cardNumberInvalidErrorMessage;
            _cardNumberInAppropriateNumberOfDigitsErrorMessage = cardNumberInAppropriateNumberOfDigitsErrorMessage;
        }

        public override bool SupportsClientSideValidation
        {
            get 
            { 
                // NOTE :
                //  We don't have the code for verifying a credit card number
                //  for a particular credit card type yet, let's have the 
                //  validation done at the client side for now.
                return false;
            }
        }

        protected override bool EvaluateIsValid()
        {
            bool isValid = true;

            if (!_controlToValidate.ReadOnly)
            {
                isValid = InterpriseHelper.IsValidCreditCardNumber(_controlToValidate.Text);
            }

            if (!isValid)
            {
                this.ErrorMessage = _cardNumberInvalidFormatErrorMessage;// "Credit Card number invalid";
            }
            
            // NOTE :
            //  We don't have the code for verifying a credit card number
            //  for a particular credit card type yet, let's have the 
            //  validation done at the client side for now.
            return isValid;
        }

        public override string RenderInitialization()
        {
            return
                string.Format(
                    "new ise.Validators.CardNumberValidator('{0}', '{1}', {2}, {3})",
                    _controlToValidate.ClientID,
                    _controlToValidateTypeWith.ClientID,
                    string.Format("[ '{0}', '{1}', '{2}', '{3}', '{4}' ]", _unKnownCardTypeErrorMessage, _noCardNumberProvidedErrorMessage, _cardNumberInvalidFormatErrorMessage, _cardNumberInvalidErrorMessage, _cardNumberInAppropriateNumberOfDigitsErrorMessage),
                    RenderChainedValidatorInitialization()
                );
        }
    }
}
