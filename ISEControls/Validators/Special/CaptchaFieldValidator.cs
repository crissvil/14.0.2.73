// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using InterpriseSuiteEcommerceCommon;

namespace InterpriseSuiteEcommerceControls.Validators.Special
{
    public class CaptchaInputValidator : InputValidator
    {
        private ITextControl _controlToValidate;

        public CaptchaInputValidator(ITextControl controlToValidate, string errorMessage)
            : base(errorMessage)
        {
            _controlToValidate = controlToValidate;
        }
        
        public override bool SupportsClientSideValidation
        {
            get { return false; }
        }

        protected override bool EvaluateIsValid()
        {
            HttpContext ctx = HttpContext.Current;
            if (null != ctx)
            {
                return _controlToValidate.Text == ctx.Session["SecurityCode"].ToString();
            }

            return true;
        }
    }
}
