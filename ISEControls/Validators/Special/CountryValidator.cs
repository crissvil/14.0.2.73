﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace InterpriseSuiteEcommerceControls.Validators.Special
{
    public class CountryValidator: InputValidator
    {
        #region Variable Declaration
        private int _invalidIndex = 0;
        private DropDownList _controlToValidate = null;
        #endregion

        public CountryValidator(DropDownList controlToValidate, int invalidIndex, string errorMessage)
            : this(controlToValidate, invalidIndex, errorMessage, null)
        {
        }

        public CountryValidator(DropDownList controlToValidate, int invalidIndex, string errorMessage, InputValidator next): base(errorMessage, next)
        {
            _controlToValidate = controlToValidate;
            _invalidIndex = invalidIndex;
        }

        public override bool SupportsClientSideValidation
        {
            get { return true; }
        }

        protected override bool EvaluateIsValid()
        {
            return _controlToValidate.SelectedIndex != _invalidIndex;
        }

        public override string RenderInitialization()
        {
            return
                string.Format(
                    "new ise.Validators.CountryValidator('{0}', '{1}', {2}, {3})",
                    _controlToValidate.ClientID,
                    this.ErrorMessage,
                    _invalidIndex,
                    RenderChainedValidatorInitialization()
                );
        }
    }
}
