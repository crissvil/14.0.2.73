﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceControls.Validators.Special
{
    public class InStorePickupShppingMethodValidator : InputValidator
    {
        public InStorePickupShppingMethodValidator(HiddenField controlShippingMethod, Control controlShippingMethodFreightType, Control controlHiddenWarehouse, string inStoreErrorMessage, string message)
            : this(controlShippingMethod, controlShippingMethodFreightType, controlHiddenWarehouse, null, inStoreErrorMessage, message)
        {
            ShippingMethod = controlShippingMethod;
            FreightType = controlShippingMethodFreightType;
            Warehouse = controlHiddenWarehouse;
            InStoreErrorMessage = inStoreErrorMessage;
        }

        public InStorePickupShppingMethodValidator(Control controlToValidate, Control controlToValidateAgainst, Control controlHiddenWarehouse, InputValidator inputValidator, string inStoreErrorMessage, string message)
            : base(message, inputValidator)
        {
            ShippingMethod = controlToValidate;
            FreightType = controlToValidateAgainst;
            Warehouse = controlHiddenWarehouse;
            InStoreErrorMessage = inStoreErrorMessage;
        }

        public Control ShippingMethod 
        {
            get;
            set;
        }
        public Control FreightType
        {
            get;
            set;
        }
        public Control Warehouse
        {
            get;
            set;
        }
        public string InStoreErrorMessage { get; set; }

        protected override bool EvaluateIsValid()
        {
            if (ShippingMethod == null) return false;

            var shippingMethod = (ShippingMethod as HiddenField);
            if (shippingMethod.Value.IsNullOrEmptyTrimmed())
            {
                return false;
            }

            if (FreightType != null)
            {
                var freightType = (FreightType as HiddenField);
                if (freightType.Value == "Pick Up")
                {
                    var hiddenWareHouse = (Warehouse as HiddenField);
                    if (hiddenWareHouse != null)
                    {
                        if (hiddenWareHouse.IsNullOrEmptyTrimmed()) return false;
                        ErrorMessage = InStoreErrorMessage;
                    }
                }
            }

            return true;
        }

        public override bool SupportsClientSideValidation
        {
            get { return true; }
        }

        public override string RenderInitialization()
        {
            if (FreightType != null && Warehouse != null)
            {
                return String.Format(
                    "new ise.Validators.InStorePickupShppingMethodValidator('{0}', '{1}', '{2}', '{3}', '{4}' , {5})",
                    ShippingMethod.ClientID,
                    FreightType.ClientID,
                    Warehouse.ClientID,
                    this.InStoreErrorMessage,
                    this.ErrorMessage,
                    RenderChainedValidatorInitialization()
                );
            }
            else
            {
                return String.Format(
                        "new ise.Validators.InStorePickupShppingMethodValidator('{0}', null, null, '{1}' , {2})",
                        ShippingMethod.ClientID,
                        this.ErrorMessage,
                        RenderChainedValidatorInitialization()
                    );
            }
        }

    }
}
