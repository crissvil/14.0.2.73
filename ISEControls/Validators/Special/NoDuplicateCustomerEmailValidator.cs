// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using InterpriseSuiteEcommerceCommon;
using System.Data;
using System.ComponentModel;

namespace InterpriseSuiteEcommerceControls.Validators.Special
{
    public class NoDuplicateCustomerEmailValidator : InputValidator
    {
        private ITextControl _controlToValidate = null;

        public NoDuplicateCustomerEmailValidator(ITextControl controlToValidate, string errorMessage) 
            : this(controlToValidate, errorMessage, null)
        {
        }

        public NoDuplicateCustomerEmailValidator(ITextControl controlToValidate, string errorMessage, InputValidator next)
            : base(errorMessage, next)
        {
            _controlToValidate = controlToValidate;
        }

        public override bool SupportsClientSideValidation
        {
            get { return false; }
        }

        protected override bool EvaluateIsValid()
        {
            return !Customer.IsEmailAlreadyInUse(_controlToValidate.Text);
        }

        
    }
}
