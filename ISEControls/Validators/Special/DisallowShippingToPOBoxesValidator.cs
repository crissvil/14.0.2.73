// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InterpriseSuiteEcommerceCommon;

namespace InterpriseSuiteEcommerceControls.Validators.Special
{
    public class DisallowShippingToPOBoxesValidator : InputValidator
    {
        private TextBox _controlToValidate;

        public DisallowShippingToPOBoxesValidator(TextBox controlToValidate, string errorMessage)
            : base(errorMessage)
        {
            _controlToValidate = controlToValidate;
        }
        
        public override bool SupportsClientSideValidation
        {
            get { return true; }
        }

        protected override bool EvaluateIsValid()
        {
            String address = _controlToValidate.Text;
            address = address.Replace(" ", "").Trim().Replace(".", "");
            bool isPOBoxAddress = (address.StartsWith("po", StringComparison.InvariantCultureIgnoreCase) || address.StartsWith("box", StringComparison.InvariantCultureIgnoreCase) || address.IndexOf("postoffice") != -1 || address.IndexOf("postbox") != -1 || address.IndexOf("pobox") != -1);
            bool rejectDueToPOBoxAddress = (isPOBoxAddress && AppLogic.AppConfigBool("DisallowShippingToPOBoxes"));

            return !rejectDueToPOBoxAddress;
        }

        public override string RenderInitialization()
        {
            return string.Format(
                    "new ise.Validators.DisallowShippingToPOBoxesValidator('{0}', '{1}', {2})",
                    _controlToValidate.ClientID,
                    this.ErrorMessage.Replace("'", "\\'"),
                    RenderChainedValidatorInitialization());
        }
    }
}
