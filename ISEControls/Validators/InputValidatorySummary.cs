// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Security;

namespace InterpriseSuiteEcommerceControls.Validators
{
    public class InputValidatorSummary : Panel
    {
        private HyperLink _lnkFocus = new HyperLink();
        private Panel _errorPanel = new Panel();
        private PlaceHolder _errorList = new PlaceHolder();
        private List<string> _errors = new List<string>();
        private bool _register = true;

        private string _clientID = string.Empty;

        public override string ClientID
        {
            get
            {
                return _clientID;
            }
        }

        public bool Register
        {
            get { return _register; }
            set { _register = value; }
        }

        protected override void CreateChildControls()
        {
            _lnkFocus.ID = this.ID + "_Focus";
            _lnkFocus.NavigateUrl = "javascript:void(null);";
            Controls.Add(_lnkFocus);

            _clientID = base.ClientID;
            string id = this.ClientID + "_Board";
            _errorPanel.ID = id;

            _errorList.Controls.Add(new LiteralControl(string.Format("<ul id=\"{0}\" ></ul>", id + "_Errors")));
            _errorPanel.Controls.Add(_errorList);

            Controls.Add(_errorPanel);
            
            base.CreateChildControls();
        }

        protected override void Render(HtmlTextWriter writer)
        {
            if (!this.DesignMode && this.Register)
            {
                writer.WriteLine("<script type=\"text/javascript\" language=\"Javascript\">");
                writer.WriteLine("Event.observe(window, 'load', ");
                writer.WriteLine("    function() {");
                writer.WriteLine(string.Format("        ise.Validators.ValidationController.registerValidationSummary({0});", this.RenderInitialization()));
                writer.WriteLine("    });");
                writer.WriteLine("</script>");
            }

            base.Render(writer);
        }

        public string RenderInitialization()
        {
            return string.Format("new ise.Validators.ValidationSummary('{0}')", this.ClientID);
        }

        public void DisplayErrorMessage(string errorMessage)
        {
            EnsureChildControls();

            _errors.Add(errorMessage);

            _errorList.Controls.Clear();

            _errorList.Controls.Add(new LiteralControl(string.Format("<ul id=\"{0}\">", _errorPanel.ID + "_Errors")));
            foreach(string error in _errors )
            {
                _errorList.Controls.Add(new LiteralControl(string.Format("<li>{0}</li>", InterpriseSuiteEcommerceCommon.Security.HtmlEncode(error))));
            }
            _errorList.Controls.Add(new LiteralControl("</ul>"));
        }

        public void HandleValidationErrorEvent(object sender, ValidationErrorEventArgs e)
        {
            DisplayErrorMessage(e.ErrorMessage);
        }
    }
}
