// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.ComponentModel;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.JSONLib;
using System.Text.RegularExpressions;
using System.IO;
using InterpriseSuiteEcommerceControls.Validators;

namespace InterpriseSuiteEcommerceControls.mobile
{
    public class AddNewAddressControl : CompositeControl
    {
        public enum RenderMode
        {
            Link,
            Button,
        }

        public enum DisplayMode
        {
            Span,
            Div
        }

        #region Variable Declaration

        protected const string VALIDATORS_CATEGORY = "Validators Category";
        private const string ADDRESS_PANEL_CSS_CLASS = "AddressPanelCssClass";
        private const string ADD_NEW_LINK_CAPTION = "AddNewLinkCaption";
        private const string SAVE_CAPTION = "SaveCaption";
        private const string CANCEL_CAPTION = "CancelCaption";

        private AddressControl2 _addressControl = null;
        private InputValidatorSummary _errorSummary = null;
        private Panel _pnlMain = null;
        private Panel _pnlAddress = null;

        #endregion

        public AddNewAddressControl()
        {
            _pnlMain = new Panel();
            _pnlAddress = new Panel();
            _addressControl = new AddressControl2();
            _errorSummary = new InputValidatorSummary();
            _errorSummary.Register = false;
            _addressControl.ErrorSummaryControl = _errorSummary;

            AssignClientReferenceIds();
        }

        private void AssignClientReferenceIds()
        {
            _pnlAddress.ID = "Content";
        }

        public void Databind() 
        {
            AddressControl.DataBind();     
        }

        public AddressControl2 AddressControl
        {
            get { return _addressControl; }
            set 
            {
                _addressControl = value;
            }
        }

        [Browsable(true), Category(VALIDATORS_CATEGORY)]
        public string SaveCaption
        {
            get
            {
                object savedValue = ViewState[SAVE_CAPTION];
                if (null == savedValue) { return string.Empty; }

                return savedValue.ToString();
            }
            set
            {
                ViewState[SAVE_CAPTION] = value;
            }
        }

        [Browsable(true), Category(VALIDATORS_CATEGORY)]
        public string CancelCaption
        {
            get
            {
                object savedValue = ViewState[CANCEL_CAPTION];
                if (null == savedValue) { return string.Empty; }

                return savedValue.ToString();
            }
            set
            {
                ViewState[CANCEL_CAPTION] = value;
            }
        }

        [Browsable(true), Category(VALIDATORS_CATEGORY)]
        public string AddNewCaption
        {
            get
            {
                object savedValue = ViewState[ADD_NEW_LINK_CAPTION];
                if (null == savedValue) { return string.Empty; }

                return savedValue.ToString();
            }
            set
            {
                ViewState[ADD_NEW_LINK_CAPTION] = value;
            }
        }

        [Browsable(true), Category(VALIDATORS_CATEGORY)]
        public string AddressPanelCssClass
        {
            get
            {
                object savedValue = ViewState[ADDRESS_PANEL_CSS_CLASS];
                if (null == savedValue) { return string.Empty; }

                return savedValue.ToString();
            }
            set
            {
                ViewState[ADDRESS_PANEL_CSS_CLASS] = value;
            }
        }

        protected override void CreateChildControls()
        {
            if (null == _addressControl)
            {
                Controls.Add(new LiteralControl("Address control not set!!!"));
            }

            var pnlMain = new Panel();
            Controls.Add(pnlMain);

            var pnlAddNewLink = new Panel();
            pnlAddNewLink.ID = "pnlAddNew";

            var addNewLink = new Label();
            addNewLink.Text = this.AddNewCaption;
            addNewLink.ID = "AddNew";

            var lnkAddNewCaption = new HtmlAnchor();
            lnkAddNewCaption.Attributes.Add("class", "kitdetaillink");
            lnkAddNewCaption.Attributes.Add("href", "javascript:void(0);");
            lnkAddNewCaption.Controls.Add(addNewLink);

            pnlAddNewLink.Controls.Add(lnkAddNewCaption);
            pnlAddNewLink.CssClass = "AddNewAddressLink";
            pnlMain.Controls.Add(pnlAddNewLink);

            _pnlAddress.CssClass = "AddNewAddress box";
            _pnlAddress.Attributes["style"] = "display:none;";
            _pnlAddress.Controls.Add(_errorSummary);
            _pnlAddress.Controls.Add(_addressControl);

            pnlMain.Controls.Add(_pnlAddress);

            var saveLink = new HyperLink()
            {
                NavigateUrl = "javascript:void(0);",
                ID = "Save",
                CssClass = "button addSpace"
            };

            var cancelLink = new HyperLink()
            {
                NavigateUrl = "javascript:void(0);",
                ID = "Cancel",
                CssClass = "button"
            };

            var cancelLabel = new Label();
            cancelLabel.CssClass = "slide";
            cancelLabel.Text = this.CancelCaption;
            cancelLink.Controls.Add(cancelLabel);

            var saveLabel = new Label();
            saveLabel.CssClass = "slide";
            saveLabel.Text = this.SaveCaption;
            saveLink.Controls.Add(saveLabel);

            var pnlCommand = new Panel()
            {
                ID = "pnlCommand",
                CssClass = "AddNewAddressCommand button_layout",
            };

            pnlCommand.Controls.Add(saveLink);
            pnlCommand.Controls.Add(cancelLink);

            _pnlAddress.Controls.Add(pnlCommand);
        }

        public override void RenderControl(HtmlTextWriter writer)
        {
            base.RenderControl(writer);

            StringBuilder script = new StringBuilder();

            script.Append("<script type=\"text/javascript\" language=\"Javascript\" >\n");
            script.Append("$add_windowLoad(\n");                
            script.Append(" function() { \n");
            script.AppendFormat(" var ctrlNewAddress = ise.Controls.AddNewAddressController.registerControl('{0}');\n", this.ClientID);
            script.AppendFormat(" ctrlNewAddress.setAddressControlId('{0}');\n", _addressControl.ClientID);
            script.Append(" }\n");
            script.Append(");\n");
            script.Append("</script>\n");

            writer.Write(script.ToString());
        }
    }
}
