// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Text;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using InterpriseSuiteEcommerceControls;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.JSONLib;

namespace InterpriseSuiteEcommerceControls.mobile
{
    public class AddressSelectorControl : DropDownList
    {
        #region Variable Declaration
        
        private List<Address> _addresses = null;
        private const string DISPLAY_LENGTH = "DisplayLength";
        private const int DEFAULT_ADDRESS_DISPLAY_LENGTH = 50;

        #endregion

        #region Constructor

        public AddressSelectorControl()
        {
        }

        #endregion

        #region Properties

        public List<Address> AddressesDataSource
        {
            get { return _addresses; }
            set 
            { 
                _addresses = value;
                PopulateAddresses();
            }
        }

        public string SelectedAddressID
        {
            get { return this.SelectedAddress.AddressID; }
            set 
            {
                foreach (ListItem item in this.Items)
                {
                    if (item.Value == value)
                    {
                        item.Selected = true;
                        break;
                    }
                }
            }
        }

        public Address SelectedAddress
        {
            get
            {
                if (null == _addresses || _addresses.Count != this.Items.Count)
                {
                    return null;
                }

                return _addresses[this.SelectedIndex];
            }
        }

        public int DisplayLength
        {
            get 
            {
                if (null != ViewState[DISPLAY_LENGTH])
                {
                    return (int)ViewState[DISPLAY_LENGTH];

                }

                return DEFAULT_ADDRESS_DISPLAY_LENGTH;
            }
            set
            {
                ViewState[DISPLAY_LENGTH] = value;
            }
        }

        #endregion

        #region Methods

        private void PopulateAddresses()
        {
            Items.Clear();
            foreach (Address thisAddress in _addresses)
            {
                Items.Add(
                    new ListItem(
                        Security.HtmlEncode(thisAddress.Full),
                        HttpUtility.UrlEncode(thisAddress.AddressID)
                    )
                );
            }
        }

        protected override void CreateChildControls()
        {
            base.CreateChildControls();
        }

        public override void RenderControl(HtmlTextWriter writer)
        {
            if (null != _addresses && 
                _addresses.Count > 0)
            {
                JSONSerializer serializer = new JSONSerializer(SerializeOption.Properties | SerializeOption.WithSerializedAttributeOnly);
                string addresses = serializer.SerializeArray(this._addresses);

                StringBuilder script = new StringBuilder();

                script.Append("<script type=\"text/javascript\" >\n");
                script.Append("$add_windowLoad(\n");                
                script.Append(" function() { ");
                script.AppendFormat(" var reg = ise.Controls.AddressSelectorController.registerControl('{0}');\n", this.ClientID);

                script.Append("   if(reg){\n");
                script.AppendFormat("      reg.setAddresses({0});\n", addresses);
                script.Append("   }\n");

                script.Append(" }\n");
                script.Append(");\n");
                script.Append("</script>\n");

                writer.Write(script.ToString());
            }

            base.RenderControl(writer);
        }

        #endregion

    }
}
