<?xml version="1.0" standalone="yes"?>
<package displayname="Order Confirmation Page" version="2.1" debug="false">
  <!-- ###################################################################################################### -->
  <!-- Licensed by Interprise Solutions.					                          -->
  <!-- http://www.InterpriseSolutions.com														                                          -->
  <!-- For details on this license please visit  the product homepage at the URL above.		                    -->
  <!-- THE ABOVE NOTICE MUST REMAIN INTACT.                                                                   -->
  <!-- ###################################################################################################### -->
  <query name="Orders" rowElementName="Order">
    <sql>
      <![CDATA[exec eCommerceMultiOrder @OrderNumber]]>
    </sql>
    <queryparam paramname="@OrderNumber" paramtype="runtime" requestparamname="OrderNumber" sqlDataType="varchar" defvalue="0" validationpattern="" />
  </query>
  <PackageTransform>
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ise="urn:ise" exclude-result-prefixes="ise">
      <xsl:output method="html" omit-xml-declaration="yes" indent="no" encoding="utf-8" />
      <xsl:param name="LocaleSetting" select="/root/Runtime/LocaleSetting" />
      <xsl:param name="WebConfigLocaleSetting" select="/root/Runtime/WebConfigLocaleSetting" />
      <xsl:param name="UseSSL">
        <xsl:value-of select="ise:AppConfigBool('UseSSL')" />
      </xsl:param>
      <xsl:param name="CacheMenus">
        <xsl:value-of select="ise:AppConfigBool('CacheMenus')" />
      </xsl:param>
      <xsl:param name="OrderNumber">
        <xsl:value-of select="/root/Runtime/OrderNumber" />
      </xsl:param>
      <xsl:param name="CustomerGuid">
        <xsl:value-of select="/root/Runtime/CustomerGuid" />
      </xsl:param>
      <xsl:param name="StoreURL">
        <xsl:value-of select="/root/Runtime/StoreUrl" />
      </xsl:param>
      <xsl:param name="SalesOrderStage">
        <xsl:value-of select="/root/Runtime/SalesOrderStage" />
      </xsl:param>
      <xsl:template match="/">
        <xsl:param name="CustomerID">
          <xsl:value-of select="/root/Orders/Order/CustomerCode" />
        </xsl:param>
        <xsl:param name="OrderTotal">
          <xsl:value-of select="/root/Orders/Order/OrderTotal" />
        </xsl:param>
        <xsl:param name="PaymentMethod">
          <xsl:value-of select="/root/Orders/Order/PaymentMethod" />
        </xsl:param>
        <xsl:param name="PMCleaned">
          <xsl:choose>
            <xsl:when test="$OrderTotal>0">
              <xsl:value-of select="ise:CleanPaymentMethod($PaymentMethod)" />
            </xsl:when>
            <xsl:otherwise>N/A</xsl:otherwise>
          </xsl:choose>
        </xsl:param>
        <xsl:param name="ReceiptURL">
          <xsl:value-of select="$StoreURL" />receipt.aspx?ordernumber=<xsl:value-of select="ise:UrlEncode($OrderNumber)" />
        </xsl:param>
        <xsl:param name="UploadURL">
          <xsl:value-of select="$StoreURL" />UploadLogo.aspx?OrderId=<xsl:value-of select="ise:UrlEncode($OrderNumber)" />
        </xsl:param>
        <xsl:param name="IncludeGoogleTrackingCode">
          <xsl:value-of select="ise:AppConfig('IncludeGoogleTrackingCode')" />
        </xsl:param>
        <xsl:param name="IncludeOvertureTrackingCode">
          <xsl:value-of select="ise:AppConfig('IncludeOvertureTrackingCode')" />
        </xsl:param>
        <xsl:param name="GatewayAuthorizationFailed">
          <xsl:value-of select="ise:ToLower(/root/QueryString/authfailed)" />
        </xsl:param>
        <xsl:param name="IncludeHeader">
          <xsl:value-of select="/root/Runtime/IncludeHeader" />
        </xsl:param>
        <xsl:param name="IncludeFooter">
          <xsl:value-of select="/root/Runtime/IncludeFooter" />
        </xsl:param>
        <xsl:param name="WriteFailedTransaction">
          <xsl:value-of select="/root/Runtime/WriteFailedTransaction" />
        </xsl:param>
        <xsl:param name="MovexCode">
          <xsl:value-of select="/root/Runtime/MovexCode" />
        </xsl:param>
        <div style="padding-left:70px;">
          <div style="line-height:54px;width:736px;font-size:50px;font-weight:400;color:#3399ff;margin:35px 0 60px;">Thanks for your order</div>
          <div style="font-size:18px;font-weight:500;padding-bottom:20px;">
            <xsl:value-of select="ise:StringResource('orderconfirmation.aspx.9',$LocaleSetting)" disable-output-escaping="yes" />
          </div>
          <xsl:if test="ise:ToLower(/root/Orders/Order/IsVoided) = 'false'">
            <div style="font-size:17px;font-weight:400;padding-bottom:10px;">
              <xsl:value-of select="ise:StringResource('orderconfirmation.aspx.10',$LocaleSetting)" disable-output-escaping="yes" />
              <img src="images/spacer.gif" width="5" height="1" />
              <span style="font-size:17px;font-weight:500">
                <xsl:value-of select="$OrderNumber" />
              </span>
            </div>
            <div style="font-size:17px;font-weight:400;padding-bottom:10px;">
              <xsl:value-of select="ise:StringResource('custom.text.14',$LocaleSetting)" disable-output-escaping="yes" />
              <img src="images/spacer.gif" width="5" height="1" />
              <span style="font-size:17px;font-weight:500">
                <xsl:value-of select="/root/Runtime/CompanyName" />
              </span>
            </div>
            <xsl:if test="ise:ToLower(/root/Runtime/CustomerIsRegistered) = 'true'">
              <div style="font-size:17px;font-weight:400;padding-bottom:10px;">
                <xsl:value-of select="ise:StringResource('custom.text.13',$LocaleSetting)" disable-output-escaping="yes" />
                <img src="images/spacer.gif" width="5" height="1" />
                <span style="font-size:17px;font-weight:500">
                  <xsl:value-of select="/root/Runtime/ContactCode" />
                </span>
              </div>
            </xsl:if>
            <xsl:if test="string-length($MovexCode) &gt; 0">
              <div style="font-size:17px;font-weight:400;padding-bottom:10px;">
                <xsl:value-of select="ise:StringResource('custom.text.15',$LocaleSetting)" disable-output-escaping="yes" />
                <img src="images/spacer.gif" width="5" height="1" />
                <span style="font-size:17px;font-weight:500">
                  <xsl:value-of select="$MovexCode" />
                </span>
              </div>
            </xsl:if>
          </xsl:if>
          <div align="left" style="padding-top:60px;">
            <img src="{concat('skins/Skin_', ise:SkinID(), '/images/delivery-van.jpg')}" />
            <span style="font-size:18px;font-weight:500;padding-left:5px;vertical-align:middle;">DELIVERY:</span>
          </div>
          <div style="font-size:17px;font-weight:400;padding-bottom:30px;padding-left:8px;line-height:20px;">
            Please allow 2-4 working days.
            <br/>
            Allow an additional 2-3 days if you are in a regional area.
          </div>
          <xsl:if test="$WriteFailedTransaction = 'true'">
            <p>
              <b style="color:red;">
                <xsl:value-of select="ise:StringResource('orderconfirmation.aspx.17',$LocaleSetting)" disable-output-escaping="yes" />
              </b>
            </p>
          </xsl:if>
          <div class="height-20 height-20"></div>
        </div>
      </xsl:template>
    </xsl:stylesheet>
  </PackageTransform>
  <SearchEngineSettings>
    <SectionTitle actionType="transform">
      <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ise="urn:ise">
        <xsl:output method="html" omit-xml-declaration="yes" />
        <xsl:param name="LocaleSetting" select="/root/Runtime/LocaleSetting" />
        <xsl:param name="WebConfigLocaleSetting" select="/root/Runtime/WebConfigLocaleSetting" />
        <xsl:param name="Prompt">
          <xsl:value-of select="ise:StringResource('orderconfirmation.aspx.1', $LocaleSetting)" />
        </xsl:param>
        <xsl:template match="/">
          <xsl:value-of select="$Prompt" />
        </xsl:template>
      </xsl:stylesheet>
    </SectionTitle>
  </SearchEngineSettings>
</package>
