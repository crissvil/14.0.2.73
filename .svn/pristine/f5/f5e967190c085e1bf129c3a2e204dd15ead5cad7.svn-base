﻿using System;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using System.Collections.Generic;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IShoppingCartService
    {
        void SetItemQuantity(int cartRecordID, decimal Quantity);
        void DeleteShoppingCartRecord(int cartRecordID);
        void DeleteShoppingCartItemReservation(string itemCode);
        void ClearKitItems(string itemCode, Guid cartId);
        void ClearEcommerceCartShippingAddressIDForAnonCustomer();

        /// <summary>
        /// Removes the KitComposition Referenced by 1 of the Cart Line Item,
        /// Removes the Cart Line Item,
        /// Removes the Customer Cart Customer Reservation
        /// </summary>
        /// <param name="arrayCardGuidIds">
        /// Array of ShoppingcarRecGuid
        /// </param>
        void ClearLineItems(string[] arrayCardGuidIds);

        /// <summary>
        /// Removes the KitComposition Referenced by 1 of the Cart Line Item,
        /// Removes the Cart Line Item,
        /// </summary>
        /// <param name="arrayCardGuidIds">
        /// Array of ShoppingcarRecGuid
        /// </param>
        void ClearLineItemsAndKitComposition(string[] arrayCardGuidIds);

        EcommerceShoppingCartModel GetEcommerceShoppingCartByCartRecordId(int cartRecordID);
        void ClearCartReservationByCurrentlyLoggedInCustomer();
        void ClearCartByLoggedInCustomerCartType(CartTypeEnum cartType);

        IEnumerable<ShoppingCartGiftEmailCustomModel> GetShoppingCartGiftEmails();
        void CreateShoppingCartGiftEmail(int shoppingCartRecID, string itemCode, int lineNum, string email);
        void CreateShoppingCartGiftEmail(List<ShoppingCartGiftEmailCustomModel> giftEmails);
        void UpdateShoppingCartGiftEmail(int giftEmailID, string emailRecipient);
        void UpdateShoppingCartGiftEmail(List<ShoppingCartGiftEmailCustomModel> giftEmails);
        void DeleteShoppingCartGiftEmailTopRecords(int shoppingCartRecID, int topRecords);
        void CleanupShoppingCartGiftEmail();
        void CreateCustomerCartGiftEmail(string documentCode, List<ShoppingCartGiftEmailCustomModel> giftEmails);

        InterpriseShoppingCart New(CartTypeEnum cartType);
        InterpriseShoppingCart New(CartTypeEnum cartType, bool loadFromDb);

        /// <summary>
        /// Load only item specific in the shopping cart
        /// </summary>
        /// <param name="cartType">shopping cart type</param>
        /// <param name="loadFromDb">true to read the DB</param>
        /// <param name="itemCode">The specific item to load</param>
        /// <returns>InterpriseShoppingCart</returns>
        InterpriseShoppingCart New(CartTypeEnum cartType, bool loadFromDb, string itemCode);
        InterpriseShoppingCart New(CartTypeEnum cartType, string originalRecurringOrderNumber, bool onlyLoadRecurringItemsThatAreDue, bool LoadDatafromDB, string pagename = "", string itemCode = "");
        IEnumerable<InterpriseShoppingCart> SplitShippingMethodsInMultipleOrders();

        void SaveShippingInfoAndContinueCheckout(IEnumerable<CartShippingDataToSplit> cartShippingDataToSplit);
        void ClearCartWarehouseCodeByCustomer();

        void DoIsEmptyChecking(InterpriseShoppingCart cart);
        void DoIsEmptyTrimmedChecking(InterpriseShoppingCart cart);
        void DoHasCouponAndIsCouponValidChecking(InterpriseShoppingCart cart);
        void DoMeetsMinOrderAmountChecking(InterpriseShoppingCart cart);
        void DoMeetsMinOrderQuantityChecking(InterpriseShoppingCart cart);
        void DoIsNoShippingRequiredAndCartHasMultipleShippingAddress(InterpriseShoppingCart cart);
        void UpdateAllocatedQty(decimal allocatedQty, string itemCode, string unitMeasureCode);
        void UpdateAllocatedQty(IEnumerable<CustomerSalesOrderDetailViewDTO> items);

        /// <summary>
        /// Check if there is/are kit component in the shopping cart that is out of stock and no open PO when HideOutOfStockProducts config is set to true
        /// </summary>
        /// <param name="itemKitCode">Item Code of the Kit Item</param>
        /// <param name="itemId">Cart Record Id of the Kit Item</param>
        /// <returns></returns>
        bool HasNoStockAndNoOpenPOComponent(string itemKitCode, string itemId);
        void CacheShoppingCartObject();
        string BuildMiniCart();
        void UpdateShoppingCartInStoreShippingInfo(int shoppingCartRecId, string warehouseCode, Guid realTimeRateGuid);
        bool IsCartHasGiftRegistryItem();
        bool IsCartHasStorePickupItem();
        bool UpdateInStorePickupCartItemStock();
        
        void CheckStockAvailabilityDuringCheckout(bool isOutOfStockAndPhaseOut, bool isOutOfStockAndWithoutOpenPO);

        #region Credit Memos, Loyalty Points, Gift Codes

        bool IsValidGiftCode(string giftCode);
        bool IsGiftCodeOwnedByCustomer(string giftCode);

        /// <summary>
        /// get giftcodes that are not yet owned by customer but will be upon placing of order
        /// </summary>
        IEnumerable<GiftCodeCustomModel> GetAdditionalGiftCodes();
        IEnumerable<CustomerCreditCustomModel> GetAppliedCreditMemos();
        decimal GetAppliedLoyaltyPoints();
        void AddAdditionalGiftCode(string giftCode);
        void RemoveAdditionalGiftCode(string giftCode);
        void ApplyGiftCodes(string giftCodesSerialized);
        void ApplyCreditMemos(string creditCodesSerialized);
        void ApplyLoyaltyPoints(string points);
        void ClearAppliedGiftCodes();
        void ClearAppliedLoyaltyPoints();
        void ClearAppliedCreditMemos();
        IEnumerable<GiftCodeCustomModel> GetAppliedGiftCodes(bool withGrouping);
        void DoHasAppliedInvalidGiftCodesChecking(InterpriseShoppingCart cart);
        void DoHasAppliedInvalidLoyaltyPointsChecking(InterpriseShoppingCart cart);
        void DoHasAppliedInvalidCreditMemosChecking(InterpriseShoppingCart cart);

        #endregion

        void DoRecomputeCartItemsPrice();

        void DeleteEcommerceCustomerCartRecord(int cartType, DateTime ageDate);

        void MergeDuplicateCartItems(InterpriseShoppingCart cart);

        IEnumerable<ItemReservationCustomModel> GetItemReservations();

        void UpdateAllocatedQuantities(CartItemCollection cartItems);
    }
}
