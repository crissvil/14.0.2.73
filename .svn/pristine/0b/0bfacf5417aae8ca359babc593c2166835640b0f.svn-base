﻿using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Collections.Generic;
using System.Linq;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
using System;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class ShippingRepository : RepositoryBase, IShippingRepository
    {
        public bool NoShippingMethodRequiredShippingMethodExisting(string shippingMethodCode)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadShippingMethodCodeRequiredChecking, shippingMethodCode.ToDbQuote()))
            {
                return reader.Read();
            }
        }

        public string GetShippingMethodGroupByShipToCode( string shipToCode)
        { 
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetShippingMethodGroupByShipToCode, shipToCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                  return DB.RSField(reader, "ShippingMethodGroup");
                }
            }
            return string.Empty;
        }

        public bool CheckIfShippingMethodInShippingMethodGroup(string shippingMethodGroup, string shippingMethodCode)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetShippingMethodGroupInSystemShippingMethodGroupDetailByShippingMethodCode, 
                                                                             shippingMethodGroup.ToDbQuote(), shippingMethodCode.ToDbQuote()))
            {
                return reader.Read();
            }
        }

        public bool CheckIfShippingMethodGroupExists(string shippingMethodGroup)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetShippingMethodGroupByGroupCode, shippingMethodGroup.ToDbQuote()))
            {
                return reader.Read();
            }
        }

        public void DeleteEcommerceRealtimeReadByContactCode(string contactCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.DeleteEcommerceRealTimeRateByContactCode, contactCode.ToDbQuote());
        }

        public void CreateRealTimeRate(string contactCode, string shippingMethodCode, string freight, string realTimeRateGUID, bool isFromMultipleShipping)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.CreateRealTimeRate, contactCode.ToDbQuote(), shippingMethodCode.ToDbQuote(), freight.ToDbQuote(), realTimeRateGUID.ToDbQuote());
        }

        public ShippingMethodDTOCollection GetCustomerShippingMethods(string customerOrAnonCode, bool isCouponFreeShipping, string contactCode, 
            string shippingMethodCode, string addressId, string residenceType, bool isNotRegistered, string country, string defaultPrice, string postalCode,
            string shippingMethodCodeZerroDollarOrder, bool isShippingRateOndemand, bool isFreeShippingEnabled, string freightDisplayResource, decimal exchangeRate)
        {
            string tempCustomerCode = customerOrAnonCode.ToNullStringOrDbQuote();
            string tempShippingMethodCode = shippingMethodCode.ToNullStringOrDbQuote();
            string tempAddressId = addressId.ToNullStringOrDbQuote();
            string tempPostalCode = !postalCode.IsNullOrEmptyTrimmed()? postalCode.ToDbQuote(): "NULL";

            var lst = new ShippingMethodDTOCollection();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_EcommerceCustomerShippingMethodList, tempCustomerCode, isCouponFreeShipping.ToBit(), contactCode.ToDbQuote(), tempShippingMethodCode, tempAddressId, residenceType.ToDbQuote(), isNotRegistered.ToBit(), country.ToDbQuote(), defaultPrice.ToDbQuote(), tempPostalCode))
            {
                while (reader.Read())
	            {
                    string code = reader.ToRSField("ShippingMethodCode");
                    if(code.ToUpperInvariant() == shippingMethodCodeZerroDollarOrder.ToUpperInvariant()) continue;

                    var model = new ShippingMethodDTO(code, reader.ToRSField("ShippingMethodDescription"))
                    {
                        IsDefault = reader.ToRSFieldBool("IsDefault"),
                        CarrierCode = reader.ToRSField("CarrierCode"),
                        CarrierDescription = reader.ToRSField("CarrierDescription"),
                        PackagingType = reader.ToRSField("PackagingType"),
                        ServiceType = reader.ToRSField("ServiceType")
                    };

                    if (isShippingRateOndemand) 
                    { 
                        if(isFreeShippingEnabled && InterpriseHelper.ShippingMethodCodeBelongsToFreeShippingMethodList(code))
                        {
                            model.Freight = Decimal.Zero;
                            model.FreightDisplay = freightDisplayResource;
                            model.FreightCurrencyCode = String.Empty;
                        }
                    }

                    model.FreightChargeType = reader.ToRSField("FreightChargeType");
                    model.FreightCalculation = reader.ToRSFieldInt("FreightCalculation");
                    model.ChargeType = reader.ToRSFieldInt("ChargeType");
                    model.MiscAmount = reader.ToRSFieldDecimal("MiscAmount");
                    model.MiscAmount = Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(exchangeRate, model.MiscAmount);

                    model.Length = reader.ToRSFieldInt("Length");
                    model.Width = reader.ToRSFieldInt("Width");
                    model.Height = reader.ToRSFieldInt("Height");
                    model.WeightThreshold = reader.ToRSFieldDecimal("WeightThreshold");

                    if (model.FreightCalculation == 1 || model.FreightCalculation == 2)
                    {
                        model.RateID = Guid.NewGuid();
                    }

                    model.IsDummyShippingMethod = false;
                    lst.Add(model);
	            }
            }

            return lst;
        }

        public ShippingMethodOversizedCustomModel GetOverSizedItemShippingMethod(string itemCode, string unitMeasureCode)
        {
            ShippingMethodOversizedCustomModel model = null;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_OverSizedItemShippingMethod, itemCode.ToDbQuote(), unitMeasureCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    model = new ShippingMethodOversizedCustomModel()
                    {
                        ShippingMethodCode = reader.ToRSField("ShippingMethodCode"),
                        FreightChargeType = reader.ToRSField("FreightChargeType")
                    };
                }
            }

            return model;
        }

    }
}