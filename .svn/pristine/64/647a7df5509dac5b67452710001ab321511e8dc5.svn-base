<?xml version="1.0" standalone="yes"?>
<package version="2.1" displayname="Entity Grid with Prices" includeentityhelper="true" debug="false">

  <!-- ###################################################################################################### -->
  <!-- Licensed by Interprise Solutions.                                    -->
  <!-- http://www.InterpriseSolutions.com                                                                     -->
  <!-- For details on this license please visit  the product homepage at the URL above.                       -->
  <!-- THE ABOVE NOTICE MUST REMAIN INTACT.                                                                   -->
  <!-- ###################################################################################################### -->

  <query name="Products" rowElementName="Product">
    <sql>
      <![CDATA[
                exec EcommerceGetProducts
                    @CategoryCode = @CatCode,
                    @DepartmentCode = @DepCode,
                    @ManufacturerCode = @ManCode,
                    @AttributeCode = @AttCode,
                    @localeName = @locale,
                    @pagenum = @pgnum,
                    @pagesize = @pgsize,
                    @StatsFirst = 0,
                    @publishedonly = 1,
                    @sortEntityName = @entityname,
                    @WebSiteCode = @WSCode,
          @CurrentDate = @CurDate,
                @inventoryItemType = @itemType,
                    @ProductFilterID = @ProductFilterID,
                    @AttributeFilter=@AttributeFilter,
                    @SortingOption=@sort,
                    @ContactCode=@ContactCode,
                    @CBMode=@CBMode,
                    @MinPrice=@MinPrice,
                    @MaxPrice=@MaxPrice,
                    @CurrencyCode = @CurrencyCode

            ]]>
    </sql>
    <queryparam paramname="@CatCode" paramtype="runtime" requestparamname="CatCode" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@DepCode" paramtype="runtime" requestparamname="DepCode" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@ManCode" paramtype="runtime" requestparamname="ManCode" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@AttCode" paramtype="runtime" requestparamname="AttCode" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@locale" paramtype="runtime" requestparamname="LocaleSetting" sqlDataType="varchar" defvalue="en-US" validationpattern="" />
    <queryparam paramname="@pgnum" paramtype="request" requestparamname="pagenum" sqlDataType="int" defvalue="1" validationpattern="" />
    <queryparam paramname="@entityname" paramtype="runtime" requestparamname="EntityName" sqlDataType="varchar" defvalue="" validationpattern="" />
    <queryparam paramname="@WSCode" paramtype="runtime" requestparamname="WebSiteCode" sqlDataType="varchar" defvalue="" validationpattern="" />
    <queryparam paramname="@CurDate" paramtype="runtime" requestparamname="CurrentDateTime" sqlDataType="datetime" defvalue="0" validationpattern="" />
    <queryparam paramname="@itemType" paramtype="runtime" requestparamname="ProductTypeFilterID" sqlDataType="int" defvalue="0" validationpattern="" />
    <queryparam paramname="@pgsize" paramtype="runtime" requestparamname="PageSize" sqlDataType="int" defvalue="0" validationpattern="" />
    <queryparam paramname="@ProductFilterID" paramtype="runtime" requestparamname="ProductFilterID" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@AttributeFilter" paramtype="runtime" requestparamname="AttributeFilter" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@sort" paramtype="request" requestparamname="sort" sqlDataType="int" defvalue="1" validationpattern="" />
    <queryparam paramname="@ContactCode" paramtype="runtime" requestparamname="ContactCode" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@CBMode" paramtype="runtime" requestparamname="CBNMode" sqlDataType="bit" defvalue="0" validationpattern="" />
    <queryparam paramname="@MinPrice" paramtype="request" requestparamname="pricemin" sqlDataType="decimal" defvalue="null" validationpattern="" />
    <queryparam paramname="@MaxPrice" paramtype="request" requestparamname="pricemax" sqlDataType="decimal" defvalue="null" validationpattern="" />
    <queryparam paramname="@CurrencyCode" paramtype="runtime" requestparamname="CurrencySetting" sqlDataType="nvarchar" defvalue="" validationpattern="" />
  </query>
  <XmlHelperPackage name="helper.entity.xml.config, helper.product.xml.config" />
  <PackageTransform>

    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ise="urn:ise" xmlns:custom="urn:custom" exclude-result-prefixes="ise">
      <xsl:output method="html" omit-xml-declaration="yes" />
      <xsl:param name="LocaleSetting" select="/root/Runtime/LocaleSetting" />
      <xsl:param name="WebConfigLocaleSetting" select="/root/Runtime/WebConfigLocaleSetting" />
      <xsl:param name="ShowSubcatsInGrid">
        <xsl:value-of select="ise:AppConfig('ShowSubcatsInGrid')" />
      </xsl:param>
      <xsl:param name="SubcatGridCols">
        <xsl:value-of select="/root/EntityHelpers/*[name()=/root/Runtime/EntityName]/descendant::Entity[EntityID=/root/Runtime/EntityCode]/ColWidth" />
      </xsl:param>
      <xsl:param name="EntityName">
        <xsl:value-of select="/root/Runtime/EntityName" />
      </xsl:param>
      <xsl:param name="EntityCode">
        <xsl:value-of select="/root/Runtime/EntityCode" />
      </xsl:param>
      <xsl:param name="EntityID">
        <xsl:value-of select="/root/Runtime/EntityID" />
      </xsl:param>
      <xsl:param name="StickerOrderLink">
        <xsl:value-of select="/root/Runtime/StickerOrderLink" />
      </xsl:param>
      <xsl:param name="WholesaleOnlySite">
        <xsl:value-of select="ise:AppConfigBool('WholesaleOnlySite')" />
      </xsl:param>
      <xsl:param name="SortOption" select="/root/QueryString/sort"/>
      <xsl:param name="BaseURL">
        <xsl:choose>
          <xsl:when test="ise:StrToLower(/root/Runtime/EntityName) = 'category'">
            c-<xsl:value-of select="/root/Runtime/EntityCode" />-<xsl:value-of select="/root/QueryString/sename" />.aspx
          </xsl:when>
          <xsl:when test="ise:StrToLower(/root/Runtime/EntityName) = 'department'">
            d-<xsl:value-of select="/root/Runtime/EntityCode" />-<xsl:value-of select="/root/QueryString/sename" />.aspx
          </xsl:when>
          <xsl:when test="ise:StrToLower(/root/Runtime/EntityName) = 'manufacturer'">
            m-<xsl:value-of select="/root/Runtime/EntityCode" />-<xsl:value-of select="/root/QueryString/sename" />.aspx
          </xsl:when>
          <xsl:when test="ise:StrToLower(/root/Runtime/EntityName) = 'attribute'">
            a-<xsl:value-of select="/root/Runtime/EntityCode" />-<xsl:value-of select="/root/QueryString/sename" />.aspx
          </xsl:when>
          <xsl:when test="ise:StrToLower(/root/Runtime/EntityName) = 'library'">
            l-<xsl:value-of select="/root/Runtime/EntityCode" />-<xsl:value-of select="/root/QueryString/sename" />.aspx
          </xsl:when>
        </xsl:choose>
      </xsl:param>
      <xsl:param name="IsUserRegistered">
        <xsl:choose>
          <xsl:when test="ise:ToLower(/root/Runtime/CustomerIsRegistered) = 'true'">true</xsl:when>
          <xsl:otherwise>false</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <xsl:param name="ShowItemPrice">
        <xsl:choose>
          <xsl:when test="(ise:AppConfigBool('WholesaleOnlySite') = 'true' and ise:IsCustomerRetail() = 'false') or (ise:AppConfigBool('WholesaleOnlySite') = 'false' and ise:IsCustomerRetail() = 'true') or (ise:AppConfigBool('ShowItemPriceWhenLogin') = 'true' and $IsUserRegistered = 'true')">true</xsl:when>
          <xsl:otherwise>false</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <xsl:param name="CurrentPage">
        <xsl:choose>
          <xsl:when test="/root/QueryString/pagenum">
            <xsl:value-of select="/root/QueryString/pagenum" />
          </xsl:when>
          <xsl:otherwise>1</xsl:otherwise>
        </xsl:choose>
      </xsl:param>

      <xsl:param name="VirtualPageValue" select="/root/Runtime/VirtualPageValue" />
      <xsl:variable name="VirtualPageOption" select="/root/Runtime/VirtualPageOption" />
      <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
      <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />

      <xsl:template match="/">
        <xsl:if test="$VirtualPageOption = 0">
          <xsl:if test="$StickerOrderLink = True">
            <div class="row">
              <div class="grid-header-link-to-cat">
                <span><img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/stamp.png')}"></img>&#0160;</span><a href="c-11-stickers-and-stamps.aspx">Click Here</a><span> to order Stickers or Stamps</span>
              </div>
            </div>
          </xsl:if>
          <xsl:value-of select="ise:DisplayEntityPageHeaderDescription2($EntityName, $EntityCode, $EntityCode)" disable-output-escaping="yes" />
        </xsl:if>
        <xsl:call-template name="SubEntity" />
        <xsl:choose>
          <xsl:when test="$VirtualPageOption = 1">
            <xsl:value-of select="ise:Topic($VirtualPageValue)" disable-output-escaping="yes" />
          </xsl:when>
          <xsl:when test="$VirtualPageOption = 2">
            <xsl:value-of select="ise:DisplayExternalPage($VirtualPageValue)" disable-output-escaping="yes" />
          </xsl:when>
          <xsl:when test="count(/root/Products/Product) = 0 and count(/root/EntityHelpers/*[name()=/root/Runtime/EntityName]/descendant::Entity[ParentEntityID=/root/Runtime/EntityCode]) = 0">
            <xsl:value-of select="ise:Topic(concat('empty', /root/Runtime/EntityName, 'text'))" disable-output-escaping="yes" />
          </xsl:when>
          <xsl:when test="count(/root/Products/Product) = 0"></xsl:when>
          <xsl:otherwise>
            <div class="grid">
              <xsl:if test="count(/root/Products/Product) &gt; 0">
                <xsl:value-of select="ise:LoadBatchProductImage('product', 'icon', 'center',  boolean('true'), /root/Products/Product | following-sibling::*[position() &lt; $SubcatGridCols])" />
                <xsl:value-of select="ise:LoadBatchItemWebOptionSettings(/root/Products/Product | following-sibling::*[position() &lt; $SubcatGridCols], 'true')"/>
                <xsl:value-of select="custom:LoadBatchItemUnitMeasures(/root/Products/Product | following-sibling::*[position() &lt; $SubcatGridCols])"/>
                <ul class="productList productSmall">
                  <xsl:apply-templates select="/root/Products/Product" />
                </ul>

                <div class="i-clear-15"></div>
                <div class="i-strong">
                  <xsl:value-of select="custom:GetCategoryFooterText($EntityID)" disable-output-escaping="yes" />
                </div>
                <div class="websummary">
                  <xsl:value-of select="custom:GetCategorySummary($EntityID)" disable-output-escaping="yes" />
                </div>
              </xsl:if>

            </div>

          </xsl:otherwise>
        </xsl:choose>
        
      </xsl:template>

      <xsl:template name="SubEntity">
        <xsl:if test="$VirtualPageOption = 0">
          <xsl:variable name="delta">
            <xsl:choose>
              <xsl:when test="(count(/root/EntityHelpers/*[name()=/root/Runtime/EntityName]/descendant::Entity[ParentEntityID=/root/Runtime/EntityCode]) mod number($SubcatGridCols)) = 0">0</xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="number($SubcatGridCols)-(count(/root/EntityHelpers/*[name()=/root/Runtime/EntityName]/descendant::Entity[ParentEntityID=/root/Runtime/EntityCode]) mod number($SubcatGridCols))" />
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>
          <xsl:variable name="rows" select="ceiling(count(/root/EntityHelpers/*[name()=/root/Runtime/EntityName]/descendant::Entity[ParentEntityID=/root/Runtime/EntityCode]) div number($SubcatGridCols))" />

          <xsl:for-each select="/root/EntityHelpers/*[name()=/root/Runtime/EntityName]/descendant::Entity[ParentEntityID=/root/Runtime/EntityCode]">

            <xsl:variable name="entityposition" select="position()" />

            <xsl:choose>
              <xsl:when test="$ShowSubcatsInGrid = 'true'">
                <xsl:if test="position() mod $SubcatGridCols = 1 or ($SubcatGridCols = 1)">
                  <table border="0" cellpadding="0" cellspacing="4" width="100%">
                    <tr>
                      <xsl:for-each select=". | following-sibling::*[position() &lt; $SubcatGridCols]">
                        <xsl:variable name="scName">
                          <xsl:value-of select="Name" />
                        </xsl:variable>
                        <xsl:variable name="scDisplayName">
                          <xsl:choose>
                            <xsl:when test="string-length(Description)>0">
                              <xsl:value-of select="ise:GetMLValue(Description)" />
                            </xsl:when>
                            <xsl:otherwise>
                              <xsl:value-of select="Name" />
                            </xsl:otherwise>
                          </xsl:choose>
                        </xsl:variable>

                        <xsl:call-template name="SubCatCell">
                          <xsl:with-param name="scName" select="$scName" />
                          <xsl:with-param name="scDisplayName" select="$scDisplayName" />
                        </xsl:call-template>

                        <xsl:if test="ceiling($entityposition div  number($SubcatGridCols)) = $rows and $delta &gt; 0">
                          <xsl:call-template name="FillerCells">
                            <xsl:with-param name="cellCount" select="$delta" />
                          </xsl:call-template>
                        </xsl:if>
                      </xsl:for-each>
                    </tr>
                    <tr>
                      <td height="10" colspan="{$SubcatGridCols}">&#0160;</td>
                    </tr>
                  </table>
                </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <xsl:variable name="scName">
                  <xsl:value-of select="Name" />
                </xsl:variable>
                <xsl:variable name="scDisplayName">
                  <xsl:choose>
                    <xsl:when test="string-length(Description)>0">
                      <xsl:value-of select="ise:GetMLValue(Description)" />
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="Name" />
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:variable>
                <p align="left">
                  <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/redarrow.gif')}"></img>&#0160;
                  <xsl:choose>
                    <xsl:when test="translate(OpenInNewTab, $uppercase, $smallcase) = 'true'">
                      <a href="{VirtualPageValueExternalPage}" target ="_blank">
                        <xsl:value-of select="$scDisplayName" disable-output-escaping="yes" />
                      </a>
                    </xsl:when>
                    <xsl:otherwise>
                      <a href="{ise:EntityLink(EntityID, SEName, $EntityName, 0, '')}">
                        <xsl:value-of select="$scDisplayName" disable-output-escaping="yes" />
                      </a>
                    </xsl:otherwise>
                  </xsl:choose>
                </p>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:for-each>
        </xsl:if>
      </xsl:template>
      <xsl:template name="SubCatCell">
        <xsl:param name="scName" />
        <xsl:param name="scDisplayName"></xsl:param>
        <xsl:param name="URL">
          <xsl:choose>
            <xsl:when test="translate(OpenInNewTab, $uppercase, $smallcase) = 'true'">
              <xsl:value-of select="VirtualPageValueExternalPage"  />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="ise:EntityLink(EntityID, SEName, $EntityName, 0, '')" />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:param>
        <xsl:param name="CellWidth" select="100 div $SubcatGridCols" />
        <xsl:if test="$VirtualPageOption = 0">
          <td align="center" width="{$CellWidth}%" valign="bottom">
            <a href="{$URL}">
              <xsl:value-of select="ise:LookupEntityImage(EntityID, $EntityName, 'icon', 0)" disable-output-escaping="yes" />
            </a>
            <br />
            <a href="{$URL}" class="entity-image-label">
              <xsl:value-of select="$scDisplayName" disable-output-escaping="yes" />
            </a>
          </td>
        </xsl:if>
      </xsl:template>

      <xsl:template match="Product">
        <xsl:variable name="delta">
          <xsl:choose>
            <xsl:when test="(count(/root/Products/Product) mod number($SubcatGridCols)) = 0">0</xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="number($SubcatGridCols)-(count(/root/Products/Product) mod number($SubcatGridCols))" />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:variable name="rows" select="ceiling(count(/root/Products/Product) div number($SubcatGridCols))" />


        <xsl:if test="$SubcatGridCols = 1">
          <xsl:call-template name="ProductCell"></xsl:call-template>
        </xsl:if>

        <xsl:if test="position() mod $SubcatGridCols = 1 and $SubcatGridCols &gt; 1">

          <xsl:for-each select=". | following-sibling::*[position() &lt; $SubcatGridCols]">
            <xsl:call-template name="ProductCell"></xsl:call-template>
          </xsl:for-each>

          <xsl:if test="ceiling(position() div  number($SubcatGridCols)) = $rows and $delta &gt; 0">
            <xsl:call-template name="FillerCells">
              <xsl:with-param name="cellCount" select="$delta" />
            </xsl:call-template>
          </xsl:if>
        </xsl:if>
      </xsl:template>
      <xsl:template name="ProductCell">

        <xsl:param name="pName">
          <xsl:value-of select="ItemName" />
        </xsl:param>
        <xsl:param name="pSalesPromptName">
          <xsl:value-of select="SalesPromptName" />
        </xsl:param>
        <xsl:param name="CellWidth" select="100 div $SubcatGridCols" />
        <xsl:variable name="pDisplayName">
          <xsl:choose>
            <xsl:when test="string-length(ItemDescription)>0">
              <xsl:value-of select="ItemDescription" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="ItemName" />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <xsl:variable name="item_href" select="ise:ProductandEntityLink(Counter, SEName, $EntityCode, $EntityName, 0)" />

        <li class="productItem" style="height: auto;">

          <div class="product-listing-layout-cont">
            <div class="instock-products">
              <div class="product-small-2">
                <div >
                  <xsl:choose>
                    <xsl:when test="IsSale_C='true'">
                      <span class="entity-item-flag-sale i-status-flag">
                        <xsl:attribute name="id">
                          <xsl:value-of select="concat('pnlStockHintBanner_',Counter)"/>
                        </xsl:attribute>
                        <div>
                          <xsl:attribute name="id">
                            <xsl:value-of select="concat('pnlStockHint_',Counter)"/>
                          </xsl:attribute>
                          <xsl:attribute name="name">
                            <xsl:value-of select="concat('pnlStockHint_',Counter)"/>
                          </xsl:attribute>
                          <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/sale.png')}">
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('imgStockHint_',Counter)"/>
                            </xsl:attribute>
                          </img>
                          <xsl:if test="ShowPricePerPiece_C = 'True'">
                            <div id="{concat('lblPricePerPiece_',Counter)}" name="{concat('lblPricePerPiece_',Counter)}" class="price-per-piece">
                              <xsl:value-of select="PricePerPieceLabel_C" disable-output-escaping="yes"/>
                            </div>
                          </xsl:if>
                        </div>
                      </span>
                    </xsl:when>
                    <xsl:otherwise>
                      <span class="i-status-flag">
                        <xsl:attribute name="id">
                          <xsl:value-of select="concat('pnlStockHintBanner_',Counter)"/>
                        </xsl:attribute>
                        <div>
                          <xsl:attribute name="id">
                            <xsl:value-of select="concat('pnlStockHint_',Counter)"/>
                          </xsl:attribute>
                          <xsl:attribute name="name">
                            <xsl:value-of select="concat('pnlStockHint_',Counter)"/>
                          </xsl:attribute>
                          <span>
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('lblStockHint_',Counter)"/>
                            </xsl:attribute>
                          </span>
                          <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/sale.png')}">
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('imgStockHint_',Counter)"/>
                            </xsl:attribute>
                          </img>
                          <xsl:if test="ShowPricePerPiece_C = 'True'">
                            <div id="{concat('lblPricePerPiece_',Counter)}" name="{concat('lblPricePerPiece_',Counter)}" class="price-per-piece">
                              <xsl:value-of select="PricePerPieceLabel_C" disable-output-escaping="yes"/>
                            </div>
                          </xsl:if>
                        </div>
                      </span>
                    </xsl:otherwise>
                  </xsl:choose>

                  <div style="padding: 5px;">
                    <div class="entity-item-price-per">
                      <xsl:attribute name="id">
                        <xsl:value-of select="concat('pnlItemPricePer_',Counter)"/>
                      </xsl:attribute>
                    </div>
                  </div>

                  <div class="entity-item-img">
                    <a href="javascript:void(1);" style="vertical-align:bottom;" class="i-category-images">
                      <xsl:value-of select="ise:DisplayImage('product', Counter, 'icon', SEAltText, 'AltText', 'center', ItemCode, boolean('true'))" disable-output-escaping="yes" />
                      <xsl:if test="IsFeatured = 'True'">
                        <div class="hot-seller-category">
                          <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/hot-seller.png')}">
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('imgHot_',Counter)"/>
                            </xsl:attribute>
                          </img>
                        </div>
                      </xsl:if>
                    </a>
                  </div>

                </div>

                <div class="product-options">

                  <div class="item-title">
                    <a href="{$item_href}" >
                      <xsl:value-of select="ise:Encode($pDisplayName)" disable-output-escaping="yes" />
                    </a>
                  </div>

                  <xsl:choose>
                    <xsl:when test="translate(ItemType, $uppercase, $smallcase)='kit'">
                      <xsl:if test= "translate(HidePriceUntilCart, $uppercase, $smallcase) = 'false'">
                        <xsl:if test="$ShowItemPrice = 'true'">
                          <xsl:value-of select="ise:RegisterProduct(Counter, ItemCode, ItemType, false(), false())" disable-output-escaping="yes" />
                          <div>
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('pnlItemPriceLevelContainer_',Counter)"/>
                            </xsl:attribute>
                            <xsl:value-of select="ise:SetXmlPackageHelperTemplate(1)" disable-output-escaping="yes"/>
                            <xsl:value-of select="custom:GetPricingLevelStickers(ItemCode)" disable-output-escaping="yes" />
                          </div>
                          <div class="entity-item-finish">
                            <xsl:value-of select="StickerInfo_C" disable-output-escaping="yes"/>
                          </div>

                          <script type="text/javascript" language="Javascript">
                            $add_windowLoad(
                            function() {
                            ise.StringResource.registerString('showproduct.aspx.46', 'Please select an option to check availability');
                            ise.StringResource.registerString('showproduct.aspx.47', 'Item(s) Available');
                            var product = ise.Products.ProductController.getProduct(<xsl:value-of select="Counter" disable-output-escaping="yes"/>);
                            var ctrlSmartStockHint = new ise.Products.SmartStockHintControl(<xsl:value-of select="Counter" disable-output-escaping="yes"/>, 'imgStockHint_<xsl:value-of select="Counter" disable-output-escaping="yes"/>');
                            ctrlSmartStockHint.setProduct(product);

                            var id = '<xsl:value-of select="Counter" disable-output-escaping="yes"/>';
                            var labelPrice = $('#' + 'pnlItemPricePer_' + id);
                            if ( labelPrice === undefined) return;
                            <![CDATA[
                                var hasItem = false;
                                for(var ctr=0; ctr < product.groups.length; ctr++) {
                                    if (hasItem){break;}
                                    var group = product.groups[ctr];
                                    var items = group.getSelectedItems();
                                    for(var ictr=0; ictr < items.length; ictr++) {
                                        var item = items[ictr];
                                        if(item) {
                                            hasItem = item.hasAvailableStock();
                                        }
                                        if (hasItem){break;}
                                    }
                                }
                              ]]>

                            if (hasItem) {
                              ctrlSmartStockHint.showInStock();
                              $(labelPrice).css({background: '#fff'});
                              $(labelPrice).css({'margin-top': '20px'});
                            }
                            else {
                              $(labelPrice).text('SORRY, OUT OF STOCK');
                              $(labelPrice).removeClass('entity-item-price-per');
                              $(labelPrice).addClass('entity-item-price-out-of-stock');
                              ctrlSmartStockHint.showOutOfStock();
                            }
                            });
                          </script>
                          
                          <div class="entity-item-dimensions">
                            Bag Size:
                            <br/>
                            <xsl:value-of select="custom:displayProductSize(ItemCode, 'true')" disable-output-escaping="yes" />
                          </div>

                          <div class="productItemCode">
                            <a href="{$item_href}" >
                            <strong>Product Code:</strong>
                            <br/>
                            <span>
                              <xsl:value-of select="ise:Encode(ItemName)" disable-output-escaping="yes" />
                            </span>
                            </a>
                          </div>

                          <div>
                            <xsl:value-of select="ise:SetXmlPackageHelperTemplate(1)" disable-output-escaping="yes"/>
                            <xsl:value-of select="ise:DisplayAddToCartForm(Counter, ItemCode, ItemType, 'h')" disable-output-escaping="yes" />
                          </div>

                        </xsl:if>
                      </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>

                      <xsl:choose>
                        <xsl:when test="translate(ItemType, $uppercase, $smallcase)='matrix group'">
                          <xsl:value-of select="ise:RegisterProduct(Counter, ItemCode, ItemType)" disable-output-escaping="yes" />
                          <div>
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('pnlItemPriceLevelContainer_',Counter)"/>
                            </xsl:attribute>
                            <xsl:value-of select="ise:DisplayPrice(Counter, ItemCode)" disable-output-escaping="yes" />
                          </div>
                          <xsl:value-of select="ise:DisplayStockHint(Counter, ItemCode, ItemType)" disable-output-escaping="yes" />
                          <xsl:value-of select="ise:SetXmlPackageHelperTemplate(1)" disable-output-escaping="yes"/>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:value-of select="ise:RegisterProduct(Counter, ItemCode, ItemType, false(), false())" disable-output-escaping="yes" />
                          <xsl:value-of select="ise:SetXmlPackageHelperTemplate(1)" disable-output-escaping="yes"/>
                          <div>
                            <xsl:attribute name="id">
                              <xsl:value-of select="concat('pnlItemPriceLevelContainer_',Counter)"/>
                            </xsl:attribute>
                            <xsl:value-of select="custom:GetPricingLevelStickers(ItemCode)" disable-output-escaping="yes" />
                          </div>
                          <div class="entity-item-finish">
                            <xsl:value-of select="StickerInfo_C" disable-output-escaping="yes"/>
                          </div>
                        </xsl:otherwise>
                      </xsl:choose>

                      <script type="text/javascript" language="Javascript">
                        $add_windowLoad(
                        function() {
                        ise.StringResource.registerString('showproduct.aspx.46', 'Please select an option to check availability');
                        ise.StringResource.registerString('showproduct.aspx.47', 'Item(s) Available');
                        var product = ise.Products.ProductController.getProduct(<xsl:value-of select="Counter" disable-output-escaping="yes"/>);
                        var ctrlSmartStockHint = new ise.Products.SmartStockHintControl(<xsl:value-of select="Counter" disable-output-escaping="yes"/>, 'imgStockHint_<xsl:value-of select="Counter" disable-output-escaping="yes"/>');
                        ctrlSmartStockHint.setProduct(product);

                        var id = '<xsl:value-of select="Counter" disable-output-escaping="yes"/>';
                        var labelPrice = $('#' + 'pnlItemPricePer_' + id);
                        if ( labelPrice === undefined) return;
                        if (product.getItemType() == 'Non-Stock' ||product.getItemType() == 'Service' || product.getIsDropShip() == true){
                        $(labelPrice).css({background: '#fff'});
                        $(labelPrice).css({'margin-top': '20px'});
                        }
                        else {
                        if ( product.hasAvailableStock()) {
                        $(labelPrice).css({background: '#fff'});
                        $(labelPrice).css({'margin-top': '20px'});
                        }
                        else {
                        $(labelPrice).text('SORRY, OUT OF STOCK');
                        $(labelPrice).removeClass('entity-item-price-per');
                        $(labelPrice).addClass('entity-item-price-out-of-stock');
                        }
                        }
                        $(".i-status-flag").removeClass("i-hide");
                        }
                        );
                      </script>

                      <div class="entity-item-dimensions">
                        Bag Size:
                        <br/>
                        <xsl:value-of select="custom:displayProductSize(ItemCode, 'true')" disable-output-escaping="yes" />
                      </div>

                      <div class="productItemCode">
                        <a href="{$item_href}" >
                          <strong>Product Code:</strong>
                          <br/>
                          <span>
                            <xsl:value-of select="ise:Encode(ItemName)" disable-output-escaping="yes" />
                          </span>
                        </a>
                      </div>

                      <div>

                        <xsl:choose>
                          <xsl:when test="translate(ItemType, $uppercase, $smallcase)='matrix group'">

                            <xsl:value-of select="ise:ProductMatrixControl(Counter, ItemCode, ItemType)" disable-output-escaping="yes" />

                            <xsl:value-of select="ise:SetXmlPackageHelperTemplate(1)" disable-output-escaping="yes"/>

                            <xsl:value-of select="ise:DisplayAddToCartForm(Counter, ItemCode, ItemType, 'h')" disable-output-escaping="yes" />
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="ise:SetXmlPackageHelperTemplate(1)" disable-output-escaping="yes"/>

                            <xsl:value-of select="ise:DisplayAddToCartForm(Counter, ItemCode, ItemType, 'h')" disable-output-escaping="yes" />
                          </xsl:otherwise>
                        </xsl:choose>
                      </div>

                    </xsl:otherwise>
                  </xsl:choose>

                </div>

              </div>
            </div>
          </div>

        </li>
      </xsl:template>

      <xsl:template name="FillerCells">

        <xsl:param name="cellCount" />
        <xsl:param name="CellWidth" select="100 div $SubcatGridCols" />
        <li class="productItem" style="height: auto;"/>
        <xsl:if test="$cellCount > 1">
          <xsl:call-template name="FillerCells">
            <xsl:with-param name="cellCount" select="$cellCount - 1" />
          </xsl:call-template>
        </xsl:if>

      </xsl:template>

    </xsl:stylesheet>
  </PackageTransform>
</package>