﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web;

namespace InterpriseSuiteEcommerceCommon.Tool
{
    public enum ProcessStatus
    { 
        Started = 0,
        Finished = 1
    }

    public static class GcThreadProcessor
    {
        private static Dictionary<Guid, int> _results = new Dictionary<Guid, int>();

        public delegate void LongProcess(object paramWrapper);

        public static void ThreadStart(LongProcess del, object paramWrapper)
        {
            if (!Exists((paramWrapper as ParamWrapper).ID)) return;
            
            var thread = new Thread(new ParameterizedThreadStart(del));
            thread.Start(paramWrapper);
        }

        public static void Finalize(Guid guid)
        {
            if (!Exists(guid)) return;
            _results[guid] = (int)ProcessStatus.Finished;
        }

        public static bool IsCompleted(Guid guid)
        {
            return _results[guid] == (int)ProcessStatus.Finished;
        }

        public static void Remove(Guid guid)
        {
            if (!Exists(guid)) return;
            _results.Remove(guid);
        }

        public static void Add(Guid guid)
        {
            if (Exists(guid)) return;
            _results.Add(guid, (int)ProcessStatus.Started);
        }

        public static bool Exists(Guid customerGuid)
        {
            return _results.ContainsKey(customerGuid);
        }
    }

    public class ParamWrapper
    {
        public Guid ID { get; set; }
        public HttpContext CurrentContext { get; set; }
        public string XmlData { get; set; }
        public System.Xml.XmlNode XmlCustomerInfo { get; set; }
    }
}
