<?xml version="1.0" standalone="yes"?>
<package version="2.1" displayname="Simple Product" includeentityhelper="true" debug="false">

  <!-- ###################################################################################################### -->
  <!-- Licensed by Interprise Solutions.                                                                      -->
  <!-- http://www.InterpriseSolutions.com                                                                     -->
  <!-- For details on this license please visit  the product homepage at the URL above.                       -->
  <!-- THE ABOVE NOTICE MUST REMAIN INTACT.                                                                   -->
  <!-- ###################################################################################################### -->

  <query name="Products" rowElementName="Product">
    <sql>
      <![CDATA[
                exec eCommerceProductInfo @ItemCode, @LanguageCode, @UserCode, @WebSiteCode, @CurrentDate, @ProductFilterID, @ContactCode
            ]]>
    </sql>
    <queryparam paramname="@ItemCode" paramtype="runtime" requestparamname="ItemCode" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@LanguageCode" paramtype="runtime" requestparamname="LocaleSetting" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@UserCode" paramtype="runtime" requestparamname="UserCode" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@WebSiteCode" paramtype="runtime" requestparamname="WebSiteCode" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@CurrentDate" paramtype="runtime" requestparamname="CurrentDateTime" sqlDataType="datetime" defvalue="0" validationpattern="" />
    <queryparam paramname="@ProductFilterID" paramtype="runtime" requestparamname="ProductFilterID" sqlDataType="nvarchar" defvalue="" validationpattern="" />
    <queryparam paramname="@ContactCode" paramtype="runtime" requestparamname="ContactCode" sqlDataType="nvarchar" defvalue="" validationpattern="" />
  </query>

  <XmlHelperPackage name="helper.product.xml.config" />

  <PackageTransform>
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ise="urn:ise" xmlns:custom="urn:custom" exclude-result-prefixes="ise">
      <xsl:output method="html" omit-xml-declaration="yes" />
      <xsl:param name="LocaleSetting" select="/root/Runtime/LocaleSetting" />
      <xsl:param name="WebConfigLocaleSetting" select="/root/Runtime/WebConfigLocaleSetting" />
      <xsl:param name="XmlPackageName" select="/root/System/XmlPackageName" />
      <xsl:param name="SecID">
        <xsl:choose>
          <xsl:when test="count(/root/QueryString/departmentid) &gt; 0">
            <xsl:value-of select="/root/QueryString/departmentid" />
          </xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <xsl:param name="CatID">
        <xsl:choose>
          <xsl:when test="count(/root/QueryString/categoryid) &gt; 0">
            <xsl:value-of select="/root/QueryString/categoryid" />
          </xsl:when>
          <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <xsl:variable name="pProductUrl">
        <xsl:value-of select="concat(/root/System/StoreUrl,/root/System/RequestedPage)"/>
      </xsl:variable>

      <xsl:template match="/">
        <xsl:comment>980BA288-5A98-4D23-9D55-95974CA001AB</xsl:comment>
        <xsl:choose>
          <xsl:when test="count(root/Products/Product) &gt; 1">
            <xsl:for-each select="/root/Products/Product[position()=1]">
              <xsl:call-template name="MultiVariant" />
            </xsl:for-each>
          </xsl:when>
          <xsl:otherwise>
            <xsl:apply-templates select="/root/Products/Product" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:template>
      <xsl:template name="MultiVariant">
        <xsl:param name="pName">
          <xsl:choose>
            <xsl:when test="count(Name/ml/locale[@name=$LocaleSetting])!=0">
              <xsl:value-of select="Name/ml/locale[@name=$LocaleSetting]" />
            </xsl:when>
            <xsl:when test="count(Name/ml/locale[@name=$WebConfigLocaleSetting]) !=0 ">
              <xsl:value-of select="Name/ml/locale[@name=$WebConfigLocaleSetting]" />
            </xsl:when>
            <xsl:when test="count(Name/ml)=0">
              <xsl:value-of select="Name" />
            </xsl:when>
          </xsl:choose>
        </xsl:param>
        <xsl:param name="pDescription">
          <xsl:choose>
            <xsl:when test="count(Description/ml/locale[@name=$LocaleSetting])!=0">
              <xsl:value-of select="Description/ml/locale[@name=$LocaleSetting]" />
            </xsl:when>
            <xsl:when test="count(Description/ml/locale[@name=$WebConfigLocaleSetting])!=0">
              <xsl:value-of select="Description/ml/locale[@name=$WebConfigLocaleSetting]" />
            </xsl:when>
            <xsl:when test="count(Description/ml)=0">
              <xsl:value-of select="Description" />
            </xsl:when>
          </xsl:choose>
        </xsl:param>
        <xsl:variable name="pDisplayName">
          <xsl:choose>
            <xsl:when test="string-length(ItemDescription)>0">
              <xsl:value-of select="ItemDescription" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="ItemName" />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="ise:RegisterProduct(Counter, ItemCode, ItemType)" disable-output-escaping="yes" />
        <table border="0" width="100%" cellpadding="4" cellspacing="0">
          <tr>
            <div class="grid-header-link-to-product">
              <span>
                <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/stamp.png')}"></img>&#0160;
              </span>
              <a href="c-11-stickers-and-stamps.aspx">Click Here</a>
              <span> to order Stickers or Stamps</span>
            </div>
          </tr>
          <tr>
            <td align="left" valign="top">
              <xsl:value-of select="ise:DisplayProductImage(Counter, ItemCode, ItemType, SEAltText)" disable-output-escaping="yes" />
            </td>
            <td align="left" valign="top" width="100%">
              <div>
                <table width="100%" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="100%" align="left" valign="middle">
                      <span class="ProductNameText">
                        1
                        <xsl:value-of select="ise:Encode($pDisplayName)" disable-output-escaping="yes" />
                      </span>
                      <br/>
                      <br/>
                      <span class="entity-item-price-per">
                        <xsl:value-of select="ise:Encode(ItemCode)" disable-output-escaping="yes" />
                      </span>
                    </td>
                    <td align="right" valign="Middle">
                      <!--<nobr>
                        <xsl:value-of select="ise:ProductNavLinks(Counter, /root/Runtime/EntityCode, /root/Runtime/EntityName, /root/EntityHelpers/*[name()=/root/Runtime/EntityName]/descendant::Entity[EntityID=/root/Runtime/EntityID]/SEName, 0, 1, 1)" disable-output-escaping="yes" />
                      </nobr>-->
                    </td>
                  </tr>
                </table>
              </div>
              <div>
                <br />
              </div>
              <div>
                <b>
                  <font color="red">
                    Display of multi-variant products is not supported by this XmlPackage.<br />
                    <br />XmlPackage=<xsl:value-of select="$XmlPackageName" />
                  </font>
                </b>
              </div>
            </td>
          </tr>
        </table>
      </xsl:template>
      <xsl:template match="Product">
        <xsl:param name="pName">
          <xsl:value-of select="ItemName" />
        </xsl:param>
        <xsl:param name="pDescription">
          <xsl:choose>
            <xsl:when test="string-length(WebDescription)>0">
              <xsl:value-of select="WebDescription" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="ItemDescription" />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:param>
        <xsl:param name="pSalesPromptName">
          <xsl:value-of select="SalesPromptName" />
        </xsl:param>
        <xsl:variable name="pDisplayName">
          <xsl:choose>
            <xsl:when test="string-length(ItemDescription)>0">
              <xsl:value-of select="ItemDescription" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="ItemName" />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>
        <xsl:choose>
          <xsl:when test="ItemType='Matrix Group'">
            <xsl:value-of select="ise:RegisterProduct(Counter, ItemCode, ItemType)" disable-output-escaping="yes" />
            <table border="0" width="100%" cellpadding="4" cellspacing="0">
              <tr>
                <div class="grid-header-link-to-product">
                  <span>
                    <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/stamp.png')}"></img>&#0160;
                  </span>
                  <a href="c-11-stickers-and-stamps.aspx">Click Here</a>
                  <span> to order Stickers or Stamps</span>
                </div>
              </tr>
              <tr>
                <td align="left" valign="top">
                  <xsl:value-of select="ise:DisplayProductImage(Counter, ItemCode, ItemType, SEAltText)" disable-output-escaping="yes" />
                </td>
                <td align="left" valign="top" width="100%">
                  <div>
                    <table width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                        <td width="100%" align="left" valign="middle">
                          <div>
                            <div>
                              <span class="product-item-flag-sale">
                                <xsl:attribute name="id">
                                  <xsl:value-of select="concat('pnlStockHintBanner_',Counter)"/>
                                </xsl:attribute>
                                <div>
                                  <xsl:attribute name="id">
                                    <xsl:value-of select="concat('pnlStockHint_',Counter)"/>
                                  </xsl:attribute>
                                  <xsl:attribute name="name">
                                    <xsl:value-of select="concat('pnlStockHint_',Counter)"/>
                                  </xsl:attribute>
                                  <span>
                                    <xsl:attribute name="id">
                                      <xsl:value-of select="concat('lblStockHint_',Counter)"/>
                                    </xsl:attribute>
                                  </span>
                                </div>
                                <xsl:if test="ShowPricePerPiece_C = 'True' or ShowPricePerPiece_C = 'true'">
                                  <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/sale.png')}">
                                    <xsl:attribute name="id">
                                      <xsl:value-of select="concat('imgStockHint_',Counter)"/>
                                    </xsl:attribute>
                                  </img>
                                  <div id="{concat('lblPricePerPiece_',Counter)}" name="{concat('lblPricePerPiece_',Counter)}" class="price-per-piece">
                                    <xsl:value-of select="PricePerPieceLabel_C" disable-output-escaping="yes"/>
                                  </div>
                                </xsl:if>
                              </span>
                            </div>
                            <br/>
                            <br/>
                            <br/>
                            <br/>
                            <span class="ProductNameText">
                              <xsl:value-of select="ise:Encode($pDisplayName)" disable-output-escaping="yes" />
                            </span>
                          </div>
                          <span class="ProductNameText">
                            3
                            <xsl:value-of select="ise:Encode($pDisplayName)" disable-output-escaping="yes" />
                          </span>
                          <br/>
                          <br/>
                          <span class="entity-item-price-per">
                            <xsl:value-of select="ise:Encode(ItemCode)" disable-output-escaping="yes" />
                          </span>
                        </td>
                        <td align="right" valign="Middle">
                          <!--<nobr>
                            <xsl:value-of select="ise:ProductNavLinks(Counter, /root/Runtime/EntityCode, /root/Runtime/EntityName, /root/EntityHelpers/*[name()=/root/Runtime/EntityName]/descendant::Entity[EntityID=/root/Runtime/EntityID]/SEName, 0, 1, 1)" disable-output-escaping="yes" />
                          </nobr>-->
                        </td>
                      </tr>
                    </table>
                  </div>
                  <div>
                    <br />
                  </div>
                  <div>
                    <b>
                      <font color="red">
                        Display of Pack Products is not supported by this XmlPackage.<br />
                        <br />XmlPackage=<xsl:value-of select="$XmlPackageName" />
                      </font>
                    </b>
                  </div>
                </td>
              </tr>
            </table>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="ise:RegisterProduct(Counter, ItemCode, ItemType)" disable-output-escaping="yes" />
            <div class="grid-header-link-to-product">
              <span>
                <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/stamp.png')}"></img>&#0160;
              </span>
              <a href="c-11-stickers-and-stamps.aspx">Click Here</a>
              <span> to order Stickers or Stamps</span>
            </div>
            <h1 class="product-title">
              <xsl:value-of select="ise:Encode($pDisplayName)" disable-output-escaping="yes" />
            </h1>
            <!--Product Image-->
            <div style="padding: 5px;">
              <xsl:choose>
              <xsl:when test="ItemType='Kit' or ItemType='Stock'">
                <div class="product-item-flag-sale">
                  <xsl:attribute name="id">
                    <xsl:value-of select="concat('pnlStockHintBanner_',Counter)"/>
                  </xsl:attribute>
                  <div>
                    <xsl:attribute name="id">
                      <xsl:value-of select="concat('pnlStockHint_',Counter)"/>
                    </xsl:attribute>
                    <xsl:attribute name="name">
                      <xsl:value-of select="concat('pnlStockHint_',Counter)"/>
                    </xsl:attribute>
                    <span>
                      <xsl:attribute name="id">
                        <xsl:value-of select="concat('lblStockHint_',Counter)"/>
                      </xsl:attribute>
                    </span>
                  </div>
                  <xsl:if test="ShowPricePerPiece_C = 'True' or ShowPricePerPiece_C = 'true'">
                    <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/sale.png')}">
                      <xsl:attribute name="id">
                        <xsl:value-of select="concat('imgStockHint_',Counter)"/>
                      </xsl:attribute>
                    </img>
                    <div id="{concat('lblPricePerPiece_',Counter)}" name="{concat('lblPricePerPiece_',Counter)}" class="price-per-piece">
                      <xsl:value-of select="PricePerPieceLabel_C" disable-output-escaping="yes"/>
                    </div>
                  </xsl:if>
                </div>
                
                <script type="text/javascript" language="Javascript">
                  $add_windowLoad(
                  function() {
                  var product = ise.Products.ProductController.getProduct(<xsl:value-of select="Counter" disable-output-escaping="yes"/>);
                  var id = '<xsl:value-of select="Counter" disable-output-escaping="yes"/>';
                  var pnlStockHintBanner = $('#' + 'pnlStockHintBanner_' + id);

                  if ( pnlStockHintBanner === undefined) return;

                  if (product.getItemType()=='Kit'){
                  <![CDATA[
                    var hasItem = false;
                    for(var ctr=0; ctr < product.groups.length; ctr++) {
                      if (hasItem){break;}
                          
                      var group = product.groups[ctr];
                      var items = group.getSelectedItems();
                          
                      for(var ictr=0; ictr < items.length; ictr++) {
                          
                        var item = items[ictr];
                            
                        if(item) {
                          hasItem = item.hasAvailableStock();
                        }
                            
                        if (hasItem){break;}
                      }
                    }
                  ]]>

                  if( hasItem ){
                  <!--pnlStockHintBanner.text('SALE')-->
                  }
                  else {

                  pnlStockHintBanner.text('SORRY, OUT OF STOCK')
                  pnlStockHintBanner.removeClass('product-item-flag-sale');
                  pnlStockHintBanner.addClass('productpage-item-flag-out-of-stock');

                  }
                  }
                  else{
                  if (product.hasAvailableStock()) {
                  <!--pnlStockHintBanner.text('SALE')-->
                  }
                  else {
                  pnlStockHintBanner.text('SORRY, OUT OF STOCK')
                  pnlStockHintBanner.removeClass('product-item-flag-sale');
                  pnlStockHintBanner.addClass('productpage-item-flag-out-of-stock');
                  }
                  }
                  }
                  );
                </script>
              </xsl:when>
              <xsl:otherwise>
              </xsl:otherwise>
            </xsl:choose>
            </div>
            <div class="showproduct-product-image">
              <xsl:value-of select="ise:DisplayProductImage(Counter, ItemCode, ItemType, SEAltText)" disable-output-escaping="yes" />
              <xsl:if test="IsFeatured = 'True' or IsFeatured = 'true'">
                <div class="hot-seller-product">
                  <img border="0" src="{concat('skins/skin_', ise:SkinID(), '/images/hot-seller.png')}">
                    <xsl:attribute name="id">
                      <xsl:value-of select="concat('imgHot_',Counter)"/>
                    </xsl:attribute>
                  </img>
                </div>
              </xsl:if>
            </div>
            <!--Product Info-->
            <div class="showproduct-product-info">

              <div>
                <!-- added handler of item name or decription if browser is currently on edit mode 
                 note: editing is only allowed to item description or web description
                -->
                <xsl:choose>
                  <xsl:when test="ise:IsInEditingMode()">
                    <xsl:choose>
                      <xsl:when test="string-length(WebDescription)>0">
                        <div class="content  editable-content" data-itemCode="{ItemCode}" data-contentType="item-webdescription">
                          <div class="edit-pencil"></div>
                          <div class="item-web-description-value" style="color:#fff !important">
                            <xsl:value-of select="$pDescription" disable-output-escaping="yes" />
                          </div>
                          <div class="clear-both"></div>
                        </div>
                      </xsl:when>
                      <xsl:otherwise>
                        <div class="content  editable-content" data-itemCode="{ItemCode}" data-contentType="item-description">
                          <div class="edit-pencil"></div>
                          <div class="string-value" style="color:#fff !important">
                            <xsl:value-of select="ItemDescription" />
                          </div>
                          <div class="clear-both"></div>
                        </div>
                      </xsl:otherwise>

                    </xsl:choose>
                  </xsl:when>
                  <xsl:otherwise>

                  </xsl:otherwise>
                </xsl:choose>
              </div>
              <div>
                <span id="divMatrixWebDescription">
                  <xsl:value-of select="$pDescription" disable-output-escaping="yes" />
                </span>
                <div class="height-25"></div>
                <div>
                  <span id="divDimension">
                    <b>DIMENSIONS: </b>
                    <xsl:value-of select="custom:displayProductSize(ItemCode, 'false')" disable-output-escaping="yes" />
                  </span>
                </div>
                <!--<div class="height-13"></div>
                <div>
                  <span id="divMinimumOrder">
                    <b>MINIMUM ORDER: </b>
                    <xsl:value-of select="custom:DisplayProductDimensions(ItemCode, 1)" disable-output-escaping="yes" />
                  </span>
                </div>-->
                <div class="height-13"></div>
                <div>
                  <span id="divProductCode">
                    <b>PRODUCT CODE: </b>
                    <xsl:value-of select="ise:Encode(ItemName)" disable-output-escaping="yes" />
                  </span>
                </div>
                <div class="height-20"></div>
                <xsl:value-of select="custom:GetProductPricingLevelExpress(ItemCode)" disable-output-escaping="yes" />
                <div class="height-13"></div>
                <div>
                  <span id="divBagsperCarton">
                    <b>BAGS PER CARTON: </b>
                    <xsl:value-of select="ise:Encode(BagperCarton_C)" disable-output-escaping="yes" />
                  </span>
                </div>
                <div id="add-to-cart-container">
                  <xsl:value-of select="ise:DisplayAddToCartForm(Counter, ItemCode, ItemType, 'h')" disable-output-escaping="yes" />
                </div>
              </div>
            </div>

            <!--<table border="0" width="100%" cellpadding="4" cellspacing="0">
              <tr>
              </tr>
              <tr>
                <td align="left" valign="top" width="100%">
                  <div style="float:left;margin-right: 29px;width:400px;">
                    <div>
                      <table width="100%" cellpadding="0" cellspacing="0">
                        <tr>
                          <td width="100%" align="left" valign="middle">
                            <xsl:value-of select="ise:ProductShareControl(Counter,$CatID,$pDisplayName,$pProductUrl,ItemDescription)" disable-output-escaping="yes" />
                          </td>
                        </tr>
                      </table>
                    </div>
                    <div>
                    </div>
                  </div>
                  <div style="float:left">
                  </div>
                </td>
              </tr>
            </table>-->
          </xsl:otherwise>
        </xsl:choose>
      </xsl:template>
    </xsl:stylesheet>
  </PackageTransform>
</package>