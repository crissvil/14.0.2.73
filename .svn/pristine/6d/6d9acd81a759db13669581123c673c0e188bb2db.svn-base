﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.DataAccess;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.Integration.Interprise.Admin
{
    public class WebStats
    {
        public int Orders { get; set; }
        public int PrevOrders { get; set; }
        public decimal Revenue { get; set; }
        public decimal PrevRevenue { get; set; }
    }

    public class Dashboard
    {
        public static IEnumerable<WebStats> GetWebStats(DateRangeType rangeType)
        {
            var stats = new List<WebStats>();
            var dateToday = DateTime.Today;
            var dateStart = new DateTime();
            var dateEnd = new DateTime();
            var dateStartPrev = new DateTime();
            var dateEndPrev = new DateTime();

            switch (rangeType)
            {
                case DateRangeType.Date:
                    dateStart = new DateTime(dateToday.Year, dateToday.Month, dateToday.Day);
                    dateEnd = dateStart.AddDays(1).AddSeconds(-1);
                    dateStartPrev = dateStart.AddDays(-1);
                    dateEndPrev = dateStartPrev.AddDays(1).AddSeconds(-1);
                    break;
                case DateRangeType.Week:
                    dateStart = dateToday.AddDays(-((int)dateToday.DayOfWeek));
                    dateEnd = dateStart.AddDays(7).AddSeconds(-1);
                    dateStartPrev = dateStart.AddDays(-7);
                    dateEndPrev = dateStartPrev.AddDays(7).AddSeconds(-1);
                    break;
                case DateRangeType.Month:
                    dateStart = new DateTime(dateToday.Year, dateToday.Month, 1);
                    dateEnd = dateStart.AddMonths(1).AddSeconds(-1);
                    dateStartPrev = dateStart.AddMonths(-1);
                    dateEndPrev = dateStartPrev.AddMonths(1).AddSeconds(-1);
                    break;
                case DateRangeType.Year:
                    dateStart = new DateTime(dateToday.Year, 1, 1);
                    dateEnd = dateStart.AddYears(1).AddSeconds(-1);
                    dateStartPrev = dateStart.AddYears(-1);
                    dateEndPrev = dateStartPrev.AddYears(1).AddSeconds(-1);
                    break;
                default:
                    break;
            }

            var currentOrders = CustomerDA.GetWebSalesOrders().Where(s => s.SalesOrderDate.Between(dateStart, dateEnd)).Count();
            var currentRevenue = CustomerDA.GetWebInvoice().Where(r => r.InvoiceDate.Between(dateStart, dateEnd)).Sum(r => r.Total);
            var prevOrders = CustomerDA.GetWebSalesOrders().Where(s => s.SalesOrderDate.Between(dateStartPrev, dateEndPrev)).Count();
            var prevRevenue = CustomerDA.GetWebInvoice().Where(r => r.InvoiceDate.Between(dateStartPrev, dateEndPrev)).Sum(r => r.Total);

            stats.Add(new WebStats
            {
                Orders = currentOrders,
                Revenue = currentRevenue,
                PrevOrders = prevOrders,
                PrevRevenue = prevRevenue
            });

            return stats;
        }
    }
}
