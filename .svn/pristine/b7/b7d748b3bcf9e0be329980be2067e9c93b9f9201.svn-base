﻿using InterpriseSuiteEcommerce;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace InterpriseSuiteEcommerce
{
    public partial class UploadLogo : SkinBase
    {
        #region DomainServices

        IAuthenticationService _authenticationServie = null;
        INavigationService _navigationService = null;
        IOrderService _orderService = null;
        ICryptographyService _cryptographyService = null;

        #endregion

        #region Initialize

        protected override void OnInit(EventArgs e)
        {
            InitializeDomainServices();
            PerformPageAccessLogic();
            PageNoCache();
            RequireSecurePage();
            InitializeContent();
            base.OnInit(e);
        }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #endregion

        #region Initialize

        string fullname, fname, lname, email = string.Empty;

        private void InitializeDomainServices()
        {
            _authenticationServie = ServiceFactory.GetInstance<IAuthenticationService>();
            _navigationService = ServiceFactory.GetInstance<INavigationService>();
            _orderService = ServiceFactory.GetInstance<IOrderService>();
            _cryptographyService = ServiceFactory.GetInstance<ICryptographyService>();
        }
        private void PerformPageAccessLogic()
        {
            // check if logged in customer
            if (!ThisCustomer.IsNotRegistered &&
                !AppLogic.AppConfigBool("PasswordIsOptionalDuringCheckout") &&
                !AppLogic.AppConfigBool("Checkout.UseOnePageCheckout"))
            {
                RequiresLogin(CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables("QUERY_STRING"));
            }

        }
        private void InitializeContent()
        {
            // this may be overridden by the XmlPackage below!
            SectionTitle = "Upload Logo";

            // clear anything that should not be stored except for immediate usage:
            Address BillingAddress = new Address();
            BillingAddress.LoadByCustomer(ThisCustomer, AddressTypes.Billing, ThisCustomer.PrimaryBillingAddressID);

            String CustomerID = ThisCustomer.CustomerCode;
            String OrderNumber = CommonLogic.QueryStringCanBeDangerousContent("OrderId", true);

            //don't allow the customer any further if they dont own this order.
            foreach (string salesOrderToCheck in OrderNumber.Split(','))
            {
                if (ThisCustomer.IsUnregisteredAnonymous ||
                !ThisCustomer.OwnsThisOrder(salesOrderToCheck))
                {
                    Response.Redirect(SE.MakeDriverLink("ordernotfound"));
                }
            }

            //Assign anonymous id as customer id for report generation.
            if (!OrderNumber.IsNullOrEmptyTrimmed())
            {
                fname = ThisCustomer.FirstName;
                lname = ThisCustomer.LastName;
                email = ThisCustomer.EMail;

                if (ThisCustomer.IsNotRegistered)
                {
                    using (SqlConnection con = DB.NewSqlConnection())
                    {
                        con.Open();
                        using (IDataReader reader = DB.GetRSFormat(con, "SELECT ShipToName, ShipToEmail from CustomerSalesOrder where SalesOrderCode = {0}", DB.SQuote(OrderNumber)))
                        {
                            if (reader.Read())
                            {
                                fullname = reader.ToRSField("ShipToName");
                                string[] sArr = SplitFullNameIntoNameAndSurname(fullname);
                                fname = sArr[0]; lname = sArr[1];
                                email = reader.ToRSField("ShipToEmail");
                            }
                        }
                    }
                }
            }

        }

        public static string[] SplitFullNameIntoNameAndSurname(string pFullName)
        {
            string[] NameSurname = new string[2];
            string[] NameSurnameTemp = pFullName.Split(' ');
            for (int i = 0; i < NameSurnameTemp.Length; i++)
            {
                if (i < NameSurnameTemp.Length - 1)
                {
                    if (!string.IsNullOrEmpty(NameSurname[0]))
                        NameSurname[0] += " " + NameSurnameTemp[i];
                    else
                        NameSurname[0] += NameSurnameTemp[i];
                }
                else
                    NameSurname[1] = NameSurnameTemp[i];
            }
            return NameSurname;
        }

        public string GetSalesOrderCode()
        {
            string salesOrderCode = CommonLogic.QueryStringCanBeDangerousContent("OrderId", true);
            return salesOrderCode;
        }

        public string GetFirstName()
        {
            return fname;
        }
        public string GetLasttName()
        {
            return lname;
        }
        public string GetEmail()
        {
            return email;
        }
        #endregion
    }
}