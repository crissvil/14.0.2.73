﻿using System.Web;
using System.Web.Caching;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;

namespace InterpriseSuiteEcommerceCommon.Tool
{
    public sealed class RequestCachingEngine : ICaching
    {
        //Return null if does not exist
        public object GetItem(string key)
        {
            if (HttpContext.Current == null) return null;
            return HttpContext.Current.Items[key];
        }

        //Return null if does not exist
        public T GetItem<T>(string key) where T : class
        {
            if (HttpContext.Current == null) return null;

            return HttpContext.Current.Items[key] as T;
        }

        public void SetItem(string key, object value, int defaultMinutes = 10)
        {
            if (HttpContext.Current == null) return;

            if (GetItem(key) != null)
            {
                HttpContext.Current.Items[key] = value;
            }
            else
            {
                AddItem(key, value, defaultMinutes);
            }
        }

        public object AddItem(string key, object value, int minutes)
        {
            if (HttpContext.Current == null) return null;

            HttpContext.Current.Items.Add(key, value);
            return null;
        }

        public object AddItem(string key, object value, System.DateTime expiration, CacheDependency cachedependency = null)
        {
            if (HttpContext.Current == null) return null;

            HttpContext.Current.Items.Add(key, value);
            return null;
        }

        public void AddItem(string key, object value, CacheDependency cachedependency = null)
        {
            if (HttpContext.Current == null) return;

            HttpContext.Current.Items.Add(key, value);
        }

        public int ItemsCount
        {
            get { return HttpContext.Current.Cache.Count; }
        }

        public object RemoveItem(string key)
        {
            if (HttpContext.Current == null) return null;

            HttpContext.Current.Items.Remove(key);
            return null;
        }

        public bool Exist(string key)
        {
            return GetItem(key) != null;
        }

        public static void Reset(string key)
        {
            if (HttpContext.Current == null) return;

            HttpContext.Current.Items.Remove(key);
        }

        public void Reset()
        {
            if (HttpContext.Current == null) return;

            var cache = HttpContext.Current.Items;
            var keys = new List<string>();
            keys.AddRange(cache.OfType<DictionaryEntry>()
                               .Select(e => e.Key.ToString())
                               .ToArray());
            keys.ForEach(k => { cache.Remove(k); });
        }

    }

    public sealed class ApplicationCachingEngine : ICaching
    {
        //Return null if does not exist
        public object GetItem(string key)
        {
            return HttpRuntime.Cache[key];
        }

        //Return null if does not exist
        public T GetItem<T>(string key) where T : class
        {
            return HttpRuntime.Cache[key] as T;
        }

        public void SetItem(string key, object value, int defaultMinutes = 10)
        {
            if (GetItem(key) != null)
            {
                HttpRuntime.Cache[key] = value;
            }
            else
            {
                AddItem(key, value, defaultMinutes);
            }
        }

        public object AddItem(string key, object value, int minutes)
        {
            return AddItem(key, value, DateTime.Now.AddMinutes(minutes));
        }

        public void AddItem(string key, object value, CacheDependency cachedependency = null)
        {
            HttpRuntime.Cache.Insert(key, value, cachedependency);
        }

        public object AddItem(string key, object value, System.DateTime expiration, CacheDependency cachedependency = null)
        {
            return HttpRuntime.Cache.Add(key, value, cachedependency, expiration, System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
        }

        public int ItemsCount
        {
            get { return HttpRuntime.Cache.Count; }
        }

        public object RemoveItem(string key)
        {
            return HttpRuntime.Cache.Remove(key);
        }

        public bool Exist(string key)
        {
            return GetItem(key) != null;
        }

        public static void Reset(string key)
        {
            HttpRuntime.Cache.Remove(key);
        }

        public void Reset()
        {
            var cache = HttpRuntime.Cache;
            var keys = new List<string>();
            keys.AddRange(cache.OfType<DictionaryEntry>()
                               .Select(e => e.Key.ToString())
                               .ToArray());
            keys.ForEach(k => { cache.Remove(k); });
        }
    }

    public sealed class SessionEngine : ICaching 
    {
        public object GetItem(string key)
        {
            return HttpContext.Current.Session[key];
        }

        public T GetItem<T>(string key) where T : class
        {
            return HttpContext.Current.Session[key] as T;
        }

        public void SetItem(string key, object value, int defaultMinutes = 10)
        {
            HttpContext.Current.Session.Add(key, value);
        }

        public object AddItem(string key, object value, int minutes)
        {
            HttpContext.Current.Session.Add(key, value);
            return value;
        }

        public void AddItem(string key, object value, CacheDependency cachedependency = null)
        {
            HttpContext.Current.Session.Add(key, value);
        }

        public object AddItem(string key, object value, DateTime expiration, CacheDependency cachedependency = null)
        {
            HttpContext.Current.Session.Add(key, value);
            return value;
        }

        public int ItemsCount
        {
            get { return HttpContext.Current.Session.Count; }
        }

        public object RemoveItem(string key)
        {
            HttpContext.Current.Session.Remove(key);
            return null;
        }

        public bool Exist(string key)
        {
            return (GetItem(key) != null);
        }

        public void Reset()
        {
            HttpContext.Current.Session.Abandon();
            HttpContext.Current.Session.Clear();
        }
    }

    public interface ICaching
    {
        object GetItem(string key);
        T GetItem<T>(string key) where T : class;
        void SetItem(string key, object value, int defaultMinutes = 10);
        object AddItem(string key, object value, int minutes);
        void AddItem(string key, object value, CacheDependency cachedependency = null);
        object AddItem(string key, object value, System.DateTime expiration, CacheDependency cachedependency = null);
        int ItemsCount { get; }
        object RemoveItem(string key);
        bool Exist(string key);
        void Reset();
    }

    public sealed class CachingFactory
    {
        public static ICaching RequestCachingEngineInstance
        {
            get
            {
                return new RequestCachingEngine();
            }
        }

        public static ICaching ApplicationCachingEngineInstance
        {
            get
            {
                return new ApplicationCachingEngine();
            }
        }

        public static ICaching CurrentInstance
        {
            get {
                return HttpContext.Current.Session["CurrentCacheEngine"] as ICaching;
            }
        }

        public static void InitializeCacheEngine(CachingOption option)
        {
            var selectedCache = (option == CachingOption.CacheOnHTTPContext) ? 
                                    RequestCachingEngineInstance : 
                                    ApplicationCachingEngineInstance;
            HttpContext.Current.Session["CurrentCacheEngine"] = selectedCache;
        }
    }

}
