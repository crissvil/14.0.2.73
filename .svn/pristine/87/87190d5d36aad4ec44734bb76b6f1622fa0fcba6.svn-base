<?xml version="1.0" standalone="yes"?>
<package displayname="Order Confirmation Page" version="2.1" debug="false">
  <!-- ###################################################################################################### -->
  <!-- Licensed by Interprise Solutions.					                          -->
  <!-- http://www.InterpriseSolutions.com														                                          -->
  <!-- For details on this license please visit  the product homepage at the URL above.		                    -->
  <!-- THE ABOVE NOTICE MUST REMAIN INTACT.                                                                   -->
  <!-- ###################################################################################################### -->
  <query name="Orders" rowElementName="Order">
    <sql>
      <![CDATA[exec eCommerceMultiOrder @OrderNumber]]>
    </sql>
    <queryparam paramname="@OrderNumber" paramtype="runtime" requestparamname="OrderNumber" sqlDataType="varchar" defvalue="0" validationpattern="" />
  </query>
  <PackageTransform>
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ise="urn:ise" exclude-result-prefixes="ise">
      <xsl:output method="html" omit-xml-declaration="yes" indent="no" encoding="utf-8" />
      <xsl:param name="LocaleSetting" select="/root/Runtime/LocaleSetting" />
      <xsl:param name="WebConfigLocaleSetting" select="/root/Runtime/WebConfigLocaleSetting" />
      <xsl:param name="UseSSL">
        <xsl:value-of select="ise:AppConfigBool('UseSSL')" />
      </xsl:param>
      <xsl:param name="CacheMenus">
        <xsl:value-of select="ise:AppConfigBool('CacheMenus')" />
      </xsl:param>
      <xsl:param name="OrderNumber">
        <xsl:value-of select="/root/Runtime/OrderNumber" />
      </xsl:param>
      <xsl:param name="CustomerGuid">
        <xsl:value-of select="/root/Runtime/CustomerGuid" />
      </xsl:param>
      <xsl:param name="StoreURL">
        <xsl:value-of select="/root/Runtime/StoreUrl" />
      </xsl:param>
      <xsl:param name="SalesOrderStage">
        <xsl:value-of select="/root/Runtime/SalesOrderStage" />
      </xsl:param>
      <xsl:template match="/">
        <xsl:param name="CustomerID">
          <xsl:value-of select="/root/Orders/Order/CustomerCode" />
        </xsl:param>
        <xsl:param name="OrderTotal">
          <xsl:value-of select="/root/Orders/Order/OrderTotal" />
        </xsl:param>
        <xsl:param name="PaymentMethod">
          <xsl:value-of select="/root/Orders/Order/PaymentMethod" />
        </xsl:param>
        <xsl:param name="PMCleaned">
          <xsl:choose>
            <xsl:when test="$OrderTotal>0">
              <xsl:value-of select="ise:CleanPaymentMethod($PaymentMethod)" />
            </xsl:when>
            <xsl:otherwise>N/A</xsl:otherwise>
          </xsl:choose>
        </xsl:param>
        <xsl:param name="ReceiptURL">
          <xsl:value-of select="$StoreURL" />receipt.aspx?ordernumber=<xsl:value-of select="ise:UrlEncode($OrderNumber)" />
        </xsl:param>
        <xsl:param name="UploadURL">
          <xsl:value-of select="$StoreURL" />UploadLogo.aspx?OrderId=<xsl:value-of select="ise:UrlEncode($OrderNumber)" />
        </xsl:param>
        <xsl:param name="IncludeGoogleTrackingCode">
          <xsl:value-of select="ise:AppConfig('IncludeGoogleTrackingCode')" />
        </xsl:param>
        <xsl:param name="IncludeOvertureTrackingCode">
          <xsl:value-of select="ise:AppConfig('IncludeOvertureTrackingCode')" />
        </xsl:param>
        <xsl:param name="GatewayAuthorizationFailed">
          <xsl:value-of select="ise:ToLower(/root/QueryString/authfailed)" />
        </xsl:param>
        <xsl:param name="IncludeHeader">
          <xsl:value-of select="/root/Runtime/IncludeHeader" />
        </xsl:param>
        <xsl:param name="IncludeFooter">
          <xsl:value-of select="/root/Runtime/IncludeFooter" />
        </xsl:param>
        <xsl:param name="WriteFailedTransaction">
          <xsl:value-of select="/root/Runtime/WriteFailedTransaction" />
        </xsl:param>
        <div class="receipt-header">
          <h1>Retail receipt of purchase</h1>
          <p>Thank you for your Smartbag purchase. You will receive your invoice/tax receipt within a day or two.</p>
        </div>
        <div class="receipt-content">
          <div class="box-1">
            <div class="imahe">
              <img src="{concat('skins/Skin_', ise:SkinID(), '/images/img1.jpg')}" />
            </div>
            <p class="box-title">Unprinted Bags</p>
            <div class="box-details">
              <p>Please allow 1-3 working days for delivery from payment received in metro areas on eastern seaboard.</p>
              <p>Allow an additional 2-5 days if you are in a regional area.</p>
              <p style="font-size:13px;"><i>(Outer WA deliveries may take up to 7 days from payment receipt).</i></p>
            </div>
          </div>
          <div class="box-2">
            <div class="imahe">
              <img src="{concat('skins/Skin_', ise:SkinID(), '/images/img2.jpg')}" />
            </div>
            <p class="box-title">Express Print Orders</p>
            <div class="box-details">
              <p class="mgb-35">For Express Print orders please  upload your logo here.</p>
              <br/>
              <p><a href="{$UploadURL}" class="site-button orderconfirmation-upload-button" style="margin:0;">Upload Logo or Image</a></p>
              <p class="mgt-25">Please allow 7-10 working days for delivery from proof approval/invoice payment.</p>
              <p>Allow an additional 2-5 days if you are in a regional area.</p>
                <p>Please note, Print is 1 colour on 1 side of the bag only.</p>
                <p>Any Questions? Call us on <br/>
                <span class="strong">1300 874 559</span></p>
              <div class="height-15"></div>
            </div>
          </div>
          <div class="box-3">
            <div class="imahe">
              <img src="{concat('skins/Skin_', ise:SkinID(), '/images/img3.jpg')}" />
            </div>
            <p class="box-title">Stickers</p>
            <div class="box-details">
              <p class="mgb-17">For full colour print adhesive sticker orders, please upload your logo or message.</p>
              <p></p>
              <p class="box-details-upload-button-top-space"><a href="{$UploadURL}" class="site-button orderconfirmation-upload-button" style="margin:0;">Upload Logo or Image</a></p>
              <p class="mgt-25">Please allow 5-10 working days for printing and delivery from proof approval.</p>
              <p>
                <span class="strong">Please note,<i> bags and stickers arrive separately.</i></span>
              </p>
              <p>Allow an additional 2-5 days if you are in a regional area. </p>
              <p>Any Questions? Call us on <br/>
              <span class="strong">1300 874 559</span>
              </p>
            </div>
          </div>
          <div class="box-4">
            <div class="imahe">
              <img src="{concat('skins/Skin_', ise:SkinID(), '/images/img4.jpg')}" />
            </div>
            <p class="box-title">Stamps</p>
            <div class="box-details">
              <p class="mgb-17">For custom-made traditional rubber stamp orders, please upload your logo or message.</p>
              <p><a href="{$UploadURL}" class="site-button orderconfirmation-upload-button" style="margin:0;">Upload Logo or Image</a></p>
              <p class="mgt-25">Please allow 7-10 working days for production and delivery from proof approval.</p>
              <p><span class="strong">Please note,<i> bags and stamps arrive separately.</i></span></p>
              <p>Stamp pad not included.</p>
              <p>Allow an additional 2-5 days if you are in a regional area.</p>
            </div>  
          </div>
        </div>  
        <div class="receipt-footer">
          <p>For simple artwork/ logo we can use most files. <b>High resolution EPS, PDF, PSD or JPG </b>format is preferable, especially for more complex work.</p>
          <p>We can work with gif or word doc files to extract your logo however we will advise if the pixels and image quality is not suitable for quality production.</p>
          <p style="color:#fc8204;">If in doubt, send the best file you have and Smartbag will advise.</p>
        </div>
      </xsl:template>
    </xsl:stylesheet>
  </PackageTransform>
  <SearchEngineSettings>
    <SectionTitle actionType="transform">
      <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ise="urn:ise">
        <xsl:output method="html" omit-xml-declaration="yes" />
        <xsl:param name="LocaleSetting" select="/root/Runtime/LocaleSetting" />
        <xsl:param name="WebConfigLocaleSetting" select="/root/Runtime/WebConfigLocaleSetting" />
        <xsl:param name="Prompt">
          <xsl:value-of select="ise:StringResource('orderconfirmation.aspx.1', $LocaleSetting)" />
        </xsl:param>
        <xsl:template match="/">
          <xsl:value-of select="$Prompt" />
        </xsl:template>
      </xsl:stylesheet>
    </SectionTitle>
  </SearchEngineSettings>
</package>
