﻿<?xml version="1.0" encoding="utf-8"?>
<package displayname="Order Receipt" version="2.1" debug="flase">

  <!-- ###################################################################################################### -->
  <!-- Licensed by Interprise Solutions.					                          -->
  <!-- http://www.InterpriseSolutions.com														                                          -->
  <!-- For details on this license please visit  the product homepage at the URL above.		                    -->
  <!-- THE ABOVE NOTICE MUST REMAIN INTACT.                                                                   -->
  <!-- ###################################################################################################### -->

  <query name="OrderData" rowElementName="Data">
    <sql>
      <![CDATA[
                SELECT SalesOrderCode, BillToCode, CONVERT(VARCHAR, CONVERT(DATE, DateCreated), 103) AS DateCreated, ShippingMethodCode, convert(decimal(10,2), Total) OrderTotal, 
                convert(decimal(10,2), TotalRate) OrderTotalRate, PaymentTermCode FROM CustomerSalesOrder with (NOLOCK) WHERE SalesOrderCode = @ordernumber
            ]]>
    </sql>
    <queryparam paramname="@ordernumber" paramtype="runtime" requestparamname="ordernumber" sqlDataType="nvarchar" defvalue="" validationpattern="" />
  </query>
  <PackageTransform>
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ise="urn:ise" exclude-result-prefixes="ise">
      <xsl:output method="html" omit-xml-declaration="yes" encoding="utf-8" />
      <xsl:param name="LocaleSetting" select="/root/Runtime/LocaleSetting" />
      <xsl:param name="OrderNumber" select="/root/Runtime/ordernumber" />
      <xsl:param name="StoreURL">
        <xsl:value-of select="/root/Runtime/StoreUrl" />
      </xsl:param>
      <xsl:param name="custom">
        <xsl:value-of select="concat($StoreURL, 'skins/skin_', ise:SkinID(), '/custom.css')" />
      </xsl:param>
      <xsl:param name="Logo">
        <xsl:value-of select="concat($StoreURL, 'skins/skin_', ise:SkinID(), '/images/logo.png')" />
      </xsl:param>
      <xsl:param name="Dollar">
        <xsl:value-of select="concat($StoreURL, 'skins/skin_', ise:SkinID(), '/images/email-dollar.png')" />
      </xsl:param>
      <xsl:param name="LeftRadius">
        <xsl:value-of select="concat($StoreURL, 'skins/skin_', ise:SkinID(), '/images/left-radius.png')" />
      </xsl:param>
      <xsl:param name="RightRadius">
        <xsl:value-of select="concat($StoreURL, 'skins/skin_', ise:SkinID(), '/images/right-radius.png')" />
      </xsl:param>
      <xsl:template match="/">
        <html xmlns:v="urn:schemas-microsoft-com:vml"
        xmlns:o="urn:schemas-microsoft-com:office:office"
        xmlns:w="urn:schemas-microsoft-com:office:word"
        xmlns:m="http://schemas.microsoft.com/office/2004/12/omml"
        xmlns="http://www.w3.org/TR/REC-html40">

          <head>
            <meta http-equiv="Content-Type" content="text/html; charset=windows-1252"/>
            <meta name="ProgId" content="Word.Document"/>
            <meta name="Generator" content="Microsoft Word 15"/>
            <meta name="Originator" content="Microsoft Word 15"/>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" />
            <title>
              <xsl:value-of select="ise:AppConfig('StoreName')" disable-output-escaping="yes" />&#0160;<xsl:value-of select="ise:StringResource('common.cs.7', $LocaleSetting)" disable-output-escaping="yes" />
              <xsl:if test="/root/Order/OrderInfo/PaymentMethod = 'REQUEST QUOTE'"> (REQUEST FOR QUOTE)</xsl:if>
            </title>
          </head>

          <body lang="EN-US" link="#0563C1" vlink="#954F72" style='font-family:Roboto;color:#7D7D7D;tab-interval:.5in;-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale;'>
            <div class='WordSection1'>
              <div align='center' style='text-align:center;margin-bottom:50px;'>
                <span style='mso-no-proof:yes'>
                  <img src="{$Logo}" />
                </span>
                <o:p></o:p>
              </div>

              <table class='MsoTableGrid' border='0' cellspacing='0' cellpadding='0' width="100%" style='width:100.0%;border-collapse:collapse;border:none;mso-yfti-tbllook:1184;'>
                <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;'>
                  <td width="2%" valign="top" style='width:2%;'>
                    <img src="{$LeftRadius}" />
                  </td>
                  <td width="96%" valign="top" style='width:96%;background:#71C690;'>

                  </td>
                  <td width="2%" valign="top" style='width:2%;'>
                    <img src="{$RightRadius}" />
                  </td>
                </tr>
                <tr style='mso-yfti-irow:1'>
                  <td width="2%" valign="top" style='width:2%;background:#71C690;height:82.75pt'>
                    <p class='MsoNormal' align='center' style='text-align:center'>
                      <span style='font-family:Roboto;color:#7D7D7D'>
                        <o:p>&#160;</o:p>
                      </span>
                    </p>
                  </td>
                  <td width="93%" valign="top" style='width:96%;background:#71C690;height:82.75pt'>
                    <div align='center' style='text-align:center;margin-top:68px;'>
                      <span style='font-family:Roboto;color:#7D7D7D;mso-no-proof:yes'>
                        <img src="{$Dollar}" />
                      </span>
                      <span style='font-family:Roboto;color:#7D7D7D'>
                        <o:p></o:p>
                      </span>
                    </div>
                    <div align='center' style='text-align:center;margin-top:55px;margin-bottom:71px;'>
                      <span style='font-size:47px;font-weight:300;font-family:Roboto;color:white;mso-themecolor:background1'>
                        Retail Order Receipt<o:p></o:p>
                      </span>
                    </div>
                  </td>
                  <td width="2%" valign="top" style='width:2%;background:#71C690;height:82.75pt'>
                    <p class='MsoNormal' align='center' style='text-align:center'>
                      <span style='font-family:Roboto;color:#7D7D7D'>
                        <o:p>&#160;</o:p>
                      </span>
                    </p>
                  </td>
                </tr>
                <tr style='mso-yfti-irow:2'>
                  <td width="100%" colspan="3" valign="top" style='width:100.0%;'>
                    <table class='MsoTableGrid' border='0' cellspacing='0' cellpadding='0' width="100%" style='width:100.0%;border-collapse:collapse;border:none;border-top:solid #71C690 1.0pt;border-left:solid #71C690 1.0pt;border-bottom:solid #71C690 1.0pt;border-right:solid #71C690 1.0pt;mso-border-alt:solid #71C690 .5pt; mso-yfti-tbllook:1184;mso-border-insideh: .5pt solid #71C690;mso-border-insidev:.5pt solid #71C690;mso-border-top-alt:solid #71C690 .5pt;mso-border-bottom-alt:solid #71C690 .5pt; mso-border-left-alt:solid #71C690 .5pt;mso-border-right-alt:solid #71C690 .5pt;'>
                      <tr>
                        <td width="100%" colspan="3" valign="top" style='width:100.0%;border-bottom: none;mso-border-bottom-alt:none;'>
                          <div style='margin-top:50px;margin-left:50px;margin-right:50px;'>
                            <span style='font-size:17px;font-family:Roboto;color:#7D7D7D;font-weight:500;'>
                              Thank you for your order.<o:p></o:p>
                            </span>
                          </div>
                          <div style='margin-top:15px;margin-left:50px;margin-right:50px;'>
                            <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                              Your order will be sent to you as soon as possible.<o:p></o:p>
                            </span>
                          </div>
                          <div style='margin-top:15px;margin-left:50px;margin-right:50px;'>
                            <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                              If you have any questions about your order, ring 1300 874 559<o:p></o:p>
                            </span>
                          </div>
                          <div style='margin-top:15px;margin-left:50px;margin-right:50px;'>
                            <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                              If you are a VIP member, simply log into the site by <a href="{$StoreURL}/signin.aspx" style="text-decoration: underline; font-size:18px; font-weight:500;color:#fa920c;">clicking here</a> and receive the most up to date information on your order.<o:p></o:p>
                            </span>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td width="100%" colspan="3" valign="top" style='width:100.0%;border-bottom: none;mso-border-bottom-alt:none;border-top:none;mso-border-top-alt:none;'>
                          <div align='center'>
                            <p class='MsoNormal' align='center' style='text-align:center'>
                              <span style='font-family:Roboto;color:#7D7D7D'>
                                <o:p>&#160;</o:p>
                              </span>
                            </p>
                            <p class='MsoNormal' align='center' style='text-align:center'>
                              <span style='font-family:Roboto;color:#7D7D7D'>
                                <o:p>&#160;</o:p>
                              </span>
                            </p>
                            <table class='MsoTableGrid' border='1' cellspacing='0' cellpadding='0' width="50%"
                             style='width:50.0%;border-collapse:collapse;border:none;mso-border-alt:solid #9A9A9A .5pt; mso-yfti-tbllook:1184;mso-padding-alt:0in 5.4pt 0in 5.4pt;mso-border-insideh: .5pt solid #9A9A9A;mso-border-insidev:.5pt solid #9A9A9A'>
                              <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes'>
                                <td width='346' valign='top' style='width:1148.15pt;border:solid #9A9A9A 1.0pt; mso-border-alt:solid #9A9A9A .5pt;background:#E1E1E1;padding:0in 5.4pt 0in 5.4pt'>
                                  <div align='center' style='text-align:center;margin-top:20px;margin-bottom:20px;'>
                                    <span style='font-size:19px;font-weight:500;font-family:Roboto;color:#7D7D7D;'>
                                      Smartbag Retail Order Receipt<o:p></o:p>
                                    </span>
                                  </div>
                                </td>
                              </tr>
                              <tr style='mso-yfti-irow:1;'>
                                <td width='346' valign='top' style='width:1148.15pt;border:solid #9A9A9A 1.0pt; border-top:none;mso-border-top-alt:solid #9A9A9A .5pt;mso-border-alt:solid #9A9A9A .5pt; padding:0in 5.4pt 0in 5.4pt'>
                                  <div style='margin-left:30px;margin-top:30px;'>
                                    <span style='font-size:17px;font-weight:500;font-family:Roboto;color:#7D7D7D'>
                                      Order Number:&#160;<o:p></o:p>
                                    </span>
                                    <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                                      <xsl:value-of select="/root/OrderData/Data/SalesOrderCode" />
                                      <o:p></o:p>
                                    </span>
                                  </div>
                                  <div style='margin-left:30px;margin-top:10px;'>
                                    <span style='font-size:17px;font-weight:500;font-family:Roboto;color:#7D7D7D'>
                                      Order Total:&#160;<o:p></o:p>
                                    </span>
                                    <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                                      <xsl:value-of select="ise:FormatCurrency(/root/OrderData/Data/OrderTotalRate, /root/System/CurrencySetting)" disable-output-escaping="yes" />
                                      <o:p></o:p>
                                    </span>
                                  </div>
                                  <div style='margin-left:30px;margin-top:10px;'>
                                    <span style='font-size:17px;font-weight:500;font-family:Roboto;color:#7D7D7D'>
                                      Order Date:&#160;<o:p></o:p>
                                    </span>
                                    <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                                      <xsl:value-of select="/root/OrderData/Data/DateCreated" />
                                      <o:p></o:p>
                                    </span>
                                  </div>
                                  <div style='margin-left:30px;margin-top:10px;'>
                                    <span style='font-size:17px;font-weight:500;font-family:Roboto;color:#7D7D7D'>
                                      Payment Method:&#160;<o:p></o:p>
                                    </span>
                                    <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                                      <xsl:value-of select="/root/OrderData/Data/PaymentTermCode" />
                                      <o:p></o:p>
                                    </span>
                                  </div>
                                  <div style='margin-left:30px;margin-top:10px;margin-bottom:20px;'>
                                    <span style='font-size:17px;font-weight:500;font-family:Roboto;color:#7D7D7D'>
                                      Shipping Method:&#160;<o:p></o:p>
                                    </span>
                                    <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                                      <xsl:value-of select="/root/OrderData/Data/ShippingMethodCode" />
                                      <o:p></o:p>
                                    </span>
                                  </div>
                                </td>
                              </tr>
                            </table>
                          </div>
                        </td>
                      </tr>
                      <tr style='mso-yfti-irow:2;mso-yfti-lastrow:yes'>
                        <td width="100%" colspan="3" valign="top" style='width:100.0%;border-top:none;mso-border-top-alt:none;'>
                          <div style='margin-top:50px;margin-left:50px;margin-right:50px;'>
                            <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                              You will shortly receive an invoice. You may pay either.<o:p></o:p>
                            </span>
                          </div>
                          <div style='margin-top:15px;margin-left:50px;margin-right:50px;'>
                            <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                              a) Electronic Transfer - Smart Group Enterprises Pty LTD, BSB: 012 341 Account#: 108208292<o:p></o:p>
                            </span>
                          </div>
                          <div style='margin-top:10px;margin-left:50px;margin-right:50px;'>
                            <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                              b) Credit Card (we only accept Visa, Mastercard, AMEX and Paypal) - Please phone, fax or email your details<o:p></o:p>
                            </span>
                          </div>
                          <div style='margin-top:10px;margin-left:50px;margin-right:50px;'>
                            <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                              c) Check - Make cheque out to Smartgroup Enterprises. Note that we do not process until the funds have cleared<o:p></o:p>
                            </span>
                          </div>
                          <div style='margin-top:5px;margin-left:50px;margin-right:50px;'>
                            <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                              Mail to: Smart Group Enterprises, PO Box 792, Ingleburn NSW 1890<o:p></o:p>
                            </span>
                          </div>
                          <div style='margin-top:10px;margin-left:50px;margin-right:50px;'>
                            <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                              d) Pay Online. If you are Smartbag VIP, you just login to your account and visit Account > Profile > Open Invoices<o:p></o:p>
                            </span>
                          </div>
                          <div style='margin-top:15px;margin-left:50px;margin-right:50px;'>
                            <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                              Once we have received your payment, we will arrange your order to be dispatched.<o:p></o:p>
                            </span>
                          </div>
                          <div style='margin-top:15px;margin-left:50px;margin-bottom:20px;margin-right:50px;'>
                            <span style='font-size:17px;font-family:Roboto;color:#7D7D7D'>
                              Smartbag cannot accept responsibility for any delay beyond our control.<o:p></o:p>
                            </span>
                          </div>
                          <p class='MsoNormal' align='center' style='text-align:center'>
                            <span style='font-family:Roboto;color:#7D7D7D'>
                              <o:p>&#160;</o:p>
                            </span>
                          </p>
                          <p class='MsoNormal' align='center' style='text-align:center'>
                            <span style='font-family:Roboto;color:#7D7D7D'>
                              <o:p>&#160;</o:p>
                            </span>
                          </p>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>

              <div align='center' style='text-align:center;margin-top:50px;color: #fa920c;'>
                <span>
                  <a href="{$StoreURL}/receipt.aspx?OrderNumber={$OrderNumber}" style="text-decoration: underline; font-size:18px; font-weight:500;color:#fa920c;">View online receipt</a>
                </span>
                <span style="font-size:18px;">
                  <span style='mso-spacerun:yes'>  </span>(You have to login on the website first to view the order)<o:p></o:p>
                </span>
              </div>

              <div align='center' style='text-align:center;margin-top:30px;'>
                <span style='font-size:17px;font-weight:700;font-family:Roboto;color:#7D7D7D'>
                  Smartbag Pty Ltd<o:p></o:p>
                </span>
              </div>
              <div align='center' style='text-align:center;margin-top:8px;'>
                <span style='font-size:17px;font-weight:300;font-family:Roboto;color:#7D7D7D'>
                  Unit 20,19 Aero Road<o:p></o:p>
                </span>
              </div>
              <div align='center' style='text-align:center;margin-top:8px;'>
                <span style='font-size:17px;font-weight:300;font-family:Roboto;color:#7D7D7D'>
                  INGLEBURN, NSW, 2565<o:p></o:p>
                </span>
              </div>
              <div align='center' style='text-align:center;margin-top:8px;'>
                <span style='font-size:17px;font-weight:300;font-family:Roboto;color:#7D7D7D'>
                  Australia<o:p></o:p>
                </span>
              </div>
              <div align='center' style='text-align:center;margin-top:8px;'>
                <span style='font-size:17px;font-weight:300;font-family:Roboto;color:#7D7D7D'>
                  PH : 1300 874 559<o:p></o:p>
                </span>
              </div>
              <div align='center' style='text-align:center;margin-top:8px;'>
                <span style='font-size:17px;font-weight:300;font-family:Roboto;color:#7D7D7D'>
                  FAX : +61 2 9605 1711<o:p></o:p>
                </span>
              </div>
              <div align='center' style='text-align:center;margin-top:8px;margin-bottom:30px;'>
                <span style='font-size:17px;font-weight:300;font-family:Roboto;color:#7D7D7D'>
                  EMAIL : sales@smartbag.com.au<o:p></o:p>
                </span>
              </div>
            </div>
          </body>
        </html>
      </xsl:template>
    </xsl:stylesheet>
  </PackageTransform>
</package>



