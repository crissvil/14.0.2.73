﻿<?xml version="1.0" standalone="yes"?>
<package version="2.1" displayname="Categories Section" includeentityhelper="true">
  <!-- ###################################################################################################### -->
  <!-- Licensed by Interprise Solutions.					                -->
  <!-- http://www.InterpriseSolutions.com														                -->
  <!-- For details on this license please visit  the product homepage at the URL above.		                -->
  <!-- THE ABOVE NOTICE MUST REMAIN INTACT.                                                                   -->
  <!-- ###################################################################################################### -->

  <PackageTransform>
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ise="urn:ise" xmlns:custom="urn:custom" exclude-result-prefixes="ise">
      <xsl:output method="html" omit-xml-declaration="yes" />

      <xsl:param name="CategoryID">
        <xsl:choose>
          <xsl:when test="/root/System/PageName = 'showcategory.aspx' and boolean(/root/QueryString/categoryid)">
            <xsl:value-of select="/root/QueryString/categoryid" />
          </xsl:when>
          <xsl:when test="(/root/System/PageName = 'showcategory.aspx' or /root/System/PageName = 'showproduct.aspx') and boolean(/root/Cookies/lastviewedentityinstanceid) and /root/Cookies/lastviewedentityname = 'Category'">
            <xsl:value-of select="/root/Cookies/lastviewedentityinstanceid" />
          </xsl:when>
          <!--Smartbag customization-->
          <!--Make the left menu select a category where the product is map-->
          <xsl:when test="/root/System/PageName = 'showproduct.aspx'">
            <xsl:value-of select="/root/System/ProductEntityID" />
          </xsl:when>
          <!--Set Portal product ID-->
          <xsl:when test="/root/System/PageName = 'placeorder.aspx'">0</xsl:when>
          <!--End customization-->
          <xsl:otherwise>-1</xsl:otherwise>
        </xsl:choose>
      </xsl:param>
      <xsl:param name="AncestorID">
        <xsl:for-each select="/root/EntityHelpers/Company//Entity[EntityID = $CategoryID]">
          <xsl:value-of select="ancestor::*/EntityID" />
        </xsl:for-each>
      </xsl:param>
      <xsl:param name="ParentID">
        <xsl:for-each select="/root/EntityHelpers/Company//Entity[EntityID = $CategoryID]">
          <xsl:value-of select="parent::*/EntityID" />
        </xsl:for-each>
      </xsl:param>
      <xsl:param name="PageName">
        <xsl:value-of select="ise:ToLower(/root/System/PageName)" />
      </xsl:param>

      <xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
      <xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />

      <xsl:template match="/">
        <ul id="menu">
          <xsl:attribute name="data-isadmin">
            <xsl:value-of select="custom:IsCompanyAdmin()" />
          </xsl:attribute>
          <xsl:attribute name="data-costcenter">
            <xsl:value-of select="custom:CostCenter()" />
          </xsl:attribute>
          <li>
            <a href="portal.aspx">
              <xsl:if test="$PageName = 'portal.aspx'">
                <xsl:attribute name="class">menu-selected</xsl:attribute>
              </xsl:if>
              Home
            </a>
          </li>
          <li class="portal-admin-menu i-hide">
            <a href="profile.aspx">
              <xsl:if test="$PageName = 'profile.aspx'">
                <xsl:attribute name="class">menu-selected</xsl:attribute>
              </xsl:if>
              Our Profile
            </a>
          </li>
          <li>
            <a href="placeorder.aspx">
              <xsl:if test="$PageName = 'placeorder.aspx' or $CategoryID &gt; -1">
                <xsl:attribute name="class">menu-selected</xsl:attribute>
              </xsl:if>
              Products
            </a>
            <xsl:if test="$CategoryID &gt; -1">
              <ul class="i-link-sub-menus">
                <xsl:apply-templates select="/root/EntityHelpers/Company/Entity">
                  <xsl:with-param name="prefix" select="''" />
                </xsl:apply-templates>
              </ul>
            </xsl:if>
          </li>
          <li class="portal-admin-menu i-hide">
            <a href="portalapprovals.aspx">
              <xsl:if test="$PageName = 'portalapprovals.aspx'">
                <xsl:attribute name="class">menu-selected</xsl:attribute>
              </xsl:if>Re-order Approvals
            </a>
          </li>
          <li class="portal-admin-menu parent-menu i-hide">
              <xsl:if test="$PageName = 'orderhistory.aspx'">
                <xsl:attribute name="class">menu-selected parent-menu</xsl:attribute>
              </xsl:if>
              REPORTS
          </li>
          <ul class="i-link-sub-menus i-hide">
            <li>
              <a href="orderhistory.aspx?contactGUID=ALL">
                <xsl:if test="$PageName = 'orderhistory.aspx'">
                  <xsl:attribute name="class">sub-menu-selected</xsl:attribute>
                </xsl:if>
                Order History
              </a>
            </li>
            <li class="portal-admin-menu i-hide">
              <a href="producthistory.aspx">
                <xsl:if test="$PageName = 'producthistory.aspx'">
                  <xsl:attribute name="class">sub-menu-selected</xsl:attribute>
                </xsl:if>
                Product History
              </a>
            </li>
            <li class="portal-admin-menu i-hide">
              <a href="soh.aspx">
                <xsl:if test="$PageName = 'soh.aspx'">
                  <xsl:attribute name="class">sub-menu-selected</xsl:attribute>
                </xsl:if>SOH
              </a>
            </li>
            <li class="portal-admin-menu i-hide" id="statement-of-account">
              <a href="statementofaccount.aspx">
                <xsl:if test="$PageName = 'statementofaccount.aspx'">
                  <xsl:attribute name="class">sub-menu-selected</xsl:attribute>
                </xsl:if>Statement of Account
              </a>
            </li>
            <li class="portal-admin-menu i-hide" id="usage-by-product">
              <a href="userlist.aspx">
                <xsl:if test="$PageName = 'userlist.aspx'">
                  <xsl:attribute name="class">sub-menu-selected</xsl:attribute>
                </xsl:if>Usage by Product
              </a>
            </li>
            <li class="portal-admin-menu i-hide" id="usage-forecast">
              <a href="userlist.aspx">
                <xsl:if test="$PageName = 'userlist.aspx'">
                  <xsl:attribute name="class">sub-menu-selected</xsl:attribute>
                </xsl:if>Usage Forecast
              </a>
            </li>
            <li class="portal-admin-menu i-hide">
              <a href="userlist.aspx">
                <xsl:if test="$PageName = 'userlist.aspx'">
                  <xsl:attribute name="class">sub-menu-selected</xsl:attribute>
                </xsl:if>User List
              </a>
            </li>
          </ul>
          <li>
            <a href="signout.aspx">Log Out</a>
          </li>
        </ul>
      </xsl:template>

      <xsl:template match="Entity">
        <xsl:param name="prefix"></xsl:param>
        <xsl:param name="eName" select="ise:GetMLValue(Name)" />
        <xsl:param name="eDescription" select="ise:GetMLValue(Description)" />
        <xsl:param name="entityID" select="EntityID" />
        <li>
          <a href="{concat('c-',EntityID,'-',ise:FormatStringForLink(SEName),'.aspx')}">
            <xsl:if test="EntityID = $CategoryID">
              <xsl:attribute name="class">sub-menu-selected</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="$eDescription" />
          </a>
          <!-- recursive call to build child nodes -->
          <xsl:if test ="count(child::Entity)&gt;0 and (EntityID = $CategoryID or descendant::Entity/EntityID = $CategoryID)">
            <ul class="i-link-sub-menus">
              <xsl:apply-templates select="Entity">
                <xsl:with-param name="prefix" select="concat($prefix, '')" />
              </xsl:apply-templates>
            </ul>
          </xsl:if>
        </li>
      </xsl:template>
    </xsl:stylesheet>
  </PackageTransform>
</package>