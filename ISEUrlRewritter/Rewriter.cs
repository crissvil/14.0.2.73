// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Web;
using System.Xml;
using System.Configuration;
using System.Text.RegularExpressions;

namespace InterpriseSuiteEcommerceUrlRewriter
{

    public class Rewriter : IConfigurationSectionHandler
    {
        protected XmlNode m_Rules = null;

        protected Rewriter() { }

        public string GetSubstitution(string sPath)
		{
            sPath = HttpContext.Current.Server.UrlDecode(sPath);
			foreach(XmlNode n in m_Rules.SelectNodes("rule"))
			{
                String url = String.Empty;
                if (n.Attributes["url"] != null)
                {
                    url = n.Attributes["url"].Value;
                }
                else
				{
				    url = n.SelectSingleNode("url/text()").Value; // backwards compatibility
				}
				
                if (Regex.IsMatch(sPath, url, RegexOptions.Compiled | RegexOptions.IgnoreCase))
				{
                    String rewrite = String.Empty;
                    if (n.Attributes["rewrite"] != null)
                    {
                        rewrite = n.Attributes["rewrite"].Value;
                    }
                    else
                    {
                        rewrite = n.SelectSingleNode("rewrite/text()").Value; // backwards compatibility
                    }
                    String tmp = Regex.Replace(sPath, url, rewrite, RegexOptions.Compiled | RegexOptions.IgnoreCase).Trim();
					if(tmp.EndsWith("&"))
					{
						tmp = tmp.Substring(0,tmp.Length-1);
					}
					return tmp;
				}
			}
			return sPath;
		}

        public static void Process()
        {
            Rewriter r = (Rewriter)System.Web.Configuration.WebConfigurationManager.GetSection("system.web/urlrewrites");

            string s = r.GetSubstitution(HttpContext.Current.Request.Url.PathAndQuery);

            if (s.Length > 0)
            {
                HttpContext.Current.RewritePath(s);
            }
        }

        public object Create(object parent, object configContext, XmlNode section)
        {
            m_Rules = section;
            return this;
        }
    }
    
}
