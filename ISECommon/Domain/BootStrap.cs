﻿using System;
using StructureMap;
using StructureMap.Configuration.DSL;
using InterpriseSuiteEcommerceCommon.Domain.Registries;
using StructureMap.Graph;
using System.Linq;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Domain;

public class BootStrapper
{
    public static void BootStrap()
    {
        ObjectFactory.Initialize(x =>
        {
            x.Scan(config =>
            {
                config.TheCallingAssembly();
                config.IgnoreStructureMapAttributes();
                config.LookForRegistries();
                config.Convention<SiteTypeScanner>();
            }); 

            x.UseDefaultStructureMapConfigFile = false;
        });
    }

}