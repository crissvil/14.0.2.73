﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System.Web;
using System.Text.RegularExpressions;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Tool;

namespace InterpriseSuiteEcommerceCommon.Domain.Presentation
{
    public class GlobalApplicationPresenter
    {
        private IAppConfigService _appConfigService = null;
        private IProductService _productService = null;
        private IDbScriptConfigService _dbScriptConfigService = null;
        private IShippingService _shippingService = null;
        private IPaymentTermService _paymentTermService = null;
        private ICronService _cronService = null;
        private INavigationService _navigationService = null;

        public GlobalApplicationPresenter() : this(
                   ServiceFactory.GetInstance<IAppConfigService>(),
                   ServiceFactory.GetInstance<IProductService>(),
                   ServiceFactory.GetInstance<IDbScriptConfigService>(),
                   ServiceFactory.GetInstance<IShippingService>(),
                   ServiceFactory.GetInstance<IPaymentTermService>(),
                   ServiceFactory.GetInstance<ICronService>(),
                   ServiceFactory.GetInstance<INavigationService>())
        {
        }

        public GlobalApplicationPresenter(
            IAppConfigService appConfigService,
            IProductService productService,
            IDbScriptConfigService dbScriptConfigService,
            IShippingService shippingService,
            IPaymentTermService paymentTermService,
            ICronService cronService,
            INavigationService navigationService)
        {
            _appConfigService = appConfigService;
            _productService = productService;
            _dbScriptConfigService = dbScriptConfigService;
            _shippingService = shippingService;
            _paymentTermService = paymentTermService;
            _cronService = cronService;
            _navigationService = navigationService;
        }

        public void ApplicationStart()
        {
            AppStartLogger.WriteLine("Starting InterpriseSuiteEcommerce...");
            HttpContext.Current.Server.ScriptTimeout = 50000; // on an app start, there could be a LOT to do...so don't let the page die.
            AppStartLogger.ResetLog();

            AppStartLogger.WriteLine("AppLogic.InterpriseSuiteEcommerce...");

            Regex.CacheSize = 50;

            AppStartLogger.WriteLine("Determine Trust Level...");
            AppLogic.TrustLevel = _appConfigService.DetermineTrustLevel();
            AppStartLogger.WriteLine("Determine Trust Level OK.");

            AppStartLogger.WriteLine("Application.Clear...");
            HttpContext.Current.Application.Clear();
            AppStartLogger.WriteLine("Application.Clear OK.");

            AppStartLogger.WriteLine("Application.RemoveAll...");
            HttpContext.Current.Application.RemoveAll();
            AppStartLogger.WriteLine("Application.RemoveAll OK.");

            AppStartLogger.WriteLine("Testing WebsiteCode...");
            if (_appConfigService.WebSiteCode.IsNullOrEmptyTrimmed())
            {
                throw new ArgumentNullException("WebsiteCode is not specified. Set this using your Configuration Tool.");
            }
            AppStartLogger.WriteLine("Testing WebsiteCode...OK");

            AppStartLogger.WriteLine("Try Load Sql Scripts from XML");
            _dbScriptConfigService.Initialize();
            AppStartLogger.WriteLine("Testing Sql Scripts from XML OK");

            AppStartLogger.WriteLine("AppConfig Load...");
            AppLogic.AppConfigTable = _appConfigService.GetAllAppConfig();
            AppStartLogger.WriteLine("AppConfig Load OK.");

            AppLogic.ro_SkinCookieName = String.Concat("SkinID_", _appConfigService.WebSiteCode);

            AppStartLogger.WriteLine("Load StringResources...");
            AppLogic.StringResourceTable = _appConfigService.GetAllStringResource();
            AppStartLogger.WriteLine("Load StringResources OK.");

            AppStartLogger.WriteLine("Load RedirectURLs");
            RedirectLogic.redirectURL = _appConfigService.GetRedirectURL();
            AppStartLogger.WriteLine("Load RedirectURLs OK.");

            AppStartLogger.WriteLine("Load Cached Settings...");
            var preference = _appConfigService.GetInventoryPreference();
            AppLogic.IsAllowFractional = preference.IsAllowFractional;
            AppLogic.InventoryDecimalPlacesPreference = preference.InventoryDecimalPlacesPreference;
            AppStartLogger.WriteLine("Load Cached Settings OK.");

            AppStartLogger.WriteLine("Setup 49...");
            AppLogic.ReplaceImageURLFromAssetMgr = _appConfigService.ReplaceImageURLFromAssetMgr;
            AppLogic.CachingOn = _appConfigService.CacheMenus;

            AppLogic.NumProductsInDB = _productService.GetProductCount();

            // Clear Cache
            AppStartLogger.WriteLine("Clear Customer Session...");
            //AppLogic.ClearSession();
            _appConfigService.CleanEcommerceCustomerRecords();
            AppStartLogger.WriteLine("Clear Customer Session Ok");

            AppLogic.ImageFilenameCache.Clear();

            if (_appConfigService.DBSQLServerLocaleSetting.IsNullOrEmptyTrimmed())
            {
                throw new ArgumentException("You must enter a valid locale setting in the web.config for the DBSQLServerLocaleSetting!!!");
            }

            _shippingService.EnsureNoShippingMethodRequiredIsExisting();
            _paymentTermService.EnsureNoPaymentTermRequiredIsExisting();

            // starting the Interprise Configuration to retrieve the connection string
            // we won't do anything here, just touch the config property to initialize it at this point.
            AppStartLogger.WriteLine("Testing DBConn...");
            _appConfigService.TryConnectToDB();
            //string conn = InterpriseHelper.ConfigInstance.OnlineCompanyConnectionString;
            AppStartLogger.WriteLine("DBConn OK.");

            AppStartLogger.WriteLine("Testing Website Count...");
            if (_appConfigService.GetAllWebSiteCount() == 0)
            {
                throw new ArgumentOutOfRangeException(String.Format("Specified WebSiteCode({0}) is nonexistent. Reset this using your Configuration Tool.", _appConfigService.WebSiteCode));
            }
            AppStartLogger.WriteLine("Testing Website Count...OK");

            AppStartLogger.WriteLine("AppLogic.ApplicationStart OK.");
            AppStartLogger.WriteLine("InterpriseSuiteEcommerce Started OK.");
            AppLogic.AppIsStarted = true;

            //Disable registry as its not being used to run website faster
            //if (_appConfigService.GiftRegistryEnabled)
            //{
            //    _cronService.TryStartExpiredRegistryRemoval();
            //}

            _cronService.TryStartExpiredEcommerceCustomerSessionRemoval();
        }

        public void PreRequestStart()
        {
            if (!AppLogic.AppIsStarted)
            {
                _navigationService.NavigateToUrl("restarting.htm");
            }

            if (_appConfigService.SiteDownForMaintenance)
            {
                string url = _appConfigService.SiteDownForMaintenancePage;
                if (url.Length == 0)
                {
                    url = _appConfigService.SiteDownForMaintenanceURL;
                }
                if (url.Length == 0)
                {
                    url = "default.htm";
                }
                _navigationService.NavigateToUrl(url);
            }
        }

    }
}
