﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IDataManager : IDisposable
    {
        void ExecuteSQL(string sql);
        void ExecuteSQL(string sql, params object[] queryParams);
        void ExecuteSQLByQueryID(string dbQueryId);
        int ExecuteSQLByQueryID(string dbQueryId, params object[] queryParams);

        IDataReader ExecuteSQLReturnReader(string sql);
        IDataReader ExecuteSQLReturnReader(string sql, params object[] queryParams);
        IDataReader ExecuteSQLByQueryIDReturnReader(string dbQueryId, params object[] queryParams);
        IDataReader ExecuteSQLByQueryIDReturnReader(string dbQueryId);

        DataTable ExecuteSQLReturnTable(string sql);
        DataTable ExecuteSQLByQueryIDReturnTable(string dbQueryId, params object[] queryParams);

        DataSet ExecuteSQLReturnDataset(string sql, string dataSetName);

        DataSet ExecuteSQLReturnDataset(string sql, string dataSetName, string cacheName, DateTime duration, bool cachEnable = false);
        DataSet ExecuteSQLReturnDataset(string sql, string dataSetName, string cacheName, bool cachEnable = false);

        T ExecuteSQLScalar<T>(string sql);
        T ExecuteSQLByQueryIDScalar<T>(string dbQueryId);
        T ExecuteSQLByQueryIDScalar<T>(string dbQueryId, params object[] queryParams);

        void ExecuteSP(string sql, SqlParameter[] param);
        IDataReader ExecuteSPReturnReader(string sql, SqlParameter[] param);
        DataTable ExecuteSPReturnTable<T>(string sql, SqlParameter[] param);
        T ExecuteSPReturnScalar<T>(string sql, SqlParameter[] param);
        void TryConnectToDB();

        /// <summary>
        /// Get the DBQuery sql statement
        /// </summary>
        /// <param name="id">DBQuery ID specified in db.xml.config</param>
        /// <returns></returns>
        string GetQueryConfig(string id);

        /// <summary>
        /// Get the DBQuery sql statement
        /// </summary>
        /// <param name="id">DBQuery ID specified in db.xml.config</param>
        /// <param name="parameters">String.Format Param Args</param>
        /// <returns>query value</returns>
        string GetQueryConfig(string id, params object[] parameters);
    }
}