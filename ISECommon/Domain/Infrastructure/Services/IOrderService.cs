﻿using System.Collections.Generic;
using System.Web;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IOrderService
    {
        SalesOrderHistoryCollection GetCustomerSalesOrders(int pages, int current);
        bool IsVoidedCustomerInvoice(string invoiceCode);
        bool IsVoidedCustomerSalesOrder(string salesOrderCode);

        CustomerInvoiceCustomModel GetCustomerInvoice(string invoiceCode);
        IEnumerable<CustomerInvoiceCustomModel> GetCustomerInvoices();
        IEnumerable<CustomerInvoiceItemCustomModel> GetCustomerInvoiceItems(string invoiceCode);
        CustomerRMACustomModel GetCustomerRMA(string rmaCode);
        IEnumerable<CustomerRMACustomModel> GetCustomerRMAs();
        IEnumerable<CustomerRMAItemCustomModel> GetCustomerRMAItems(string rmaCode);
    }
}
