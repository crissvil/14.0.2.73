﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Model;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface ILocalizationService
    {
        string FormatDecimal(decimal valueToFormat, int places);
        string FormatDecimal(string valueToFormat, string places);

        string FormatDecimalToNumber(decimal value);
        string FormatDecimalToNumber(decimal value, int decimalPlaces);

        CultureInfo GetCustomerCultureInfo();
        string GetSystemCurrencyModelToJSON();
        SystemCurrencyModel GetSystemCurrencyModel();
    }
}
