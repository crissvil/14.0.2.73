﻿using InterpriseSuiteEcommerceCommon.Domain.Model;
namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IWarehouseService
    {
        string GetStorePickupWarehouseStoreHoursToJSON(string warehouseCode);
        InventoryWarehouseModel GetWarehouseByCodeCachedPerRequest(string warehouseCode);
        string GetWarehouseByCodeToJSON(string warehouseCode);
    }
}
