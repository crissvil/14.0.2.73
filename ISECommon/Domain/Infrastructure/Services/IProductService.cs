﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using InterpriseSuiteEcommerceCommon.DTO;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IProductService
    {
        bool IsAKit(string productId);
        int GetProductCount();
        int GetCheckOutOptionCount(ref string xmlString);
        InventoryProductModel GetInventoryItem(string itemCode);
        IEnumerable<InventoryProductModel> GetInventoryItems();
        IEnumerable<InventoryProductModel> GetInventoryItemsWithNoImages();
        IEnumerable<InventoryOverrideImageModel> GetItemImages(string itemCode);
        IEnumerable<InventoryCategoryModel> GetInventoryCategories();
        IEnumerable<SystemCategoryModel> GetSystemCategories();
        decimal GetKitDiscount(string itemKitCode);
        IEnumerable<SelectedKitComponentModel> GetSelectedKitComponents(string itemKitCode, string currencyCode, string cartId, bool showDefault);
        ValidCartItemCustomModel GetValidItemCodeAndBaseUnitMeasureById(int itemId);
        string GetMatrixItemCode(string itemCode);
        string GetItemDefaultImageFilenameBySize(string itemCode, ImageSize size);
        SystemEntityModel GetVirtualPageSettings(int entityId, string entityName);
        IEnumerable<MatrixItemWebOptionDescCustomModel> GetMatrixItemWebOptionDescription(string itemCode);
        IEnumerable<MatrixItemWebOptionDescCustomModel> GetNonStockMatrixItemWebOptionDescription(IEnumerable<string> itemCodes);
        ECommerceProductInfoView GetProductInfoViewForShowProduct(string itemCode);
        string GetProductInfoViewForShowProductToJson(string itemCode);
        string GetStorePickupInventoryWarehouseListToJSON(bool isFirstLoad, string itemCode, IList<string> kitComposition, string unitMeasureCode, string postalCode, string city, string state, string country, int nextRecord = 0, byte searchLimit = 10);
        string GetProductRatingHtml(string itemCode);
        string GetStorPickUpInitialInfoToJson(string itemCode, string unitMeasureCode);
        IEnumerable<ItemWebOptionCustomModel> GetWebOptions(IEnumerable<string> itemCodes, bool IsExpressPrint);
        ItemWebOptionCustomModel GetWebOption(string itemCode);
        ItemWebOptionCustomModel GetWebOption(string itemCode, bool IsExpressPrint);
        IEnumerable<UnitMeasureModel> GetItemUnitMeassures(string itemCode, IEnumerable<string> unitMeasureCodes = null);
        InventoryPriceInfoModel GetProductPriceInfo(string itemCode, string unitMeasure, decimal unitMeasureQuantity, decimal quantity, decimal matrixGroupQuantity);
        InventoryMatrixItemModel GetMatrixItemInfo(string matrixItemCode);
        InventoryMatrixItemModel GetMatrixItemInfo(int counter);
        IEnumerable<MatrixAttributeModel> GetMatrixAttributes(string itemCode);
        IEnumerable<MatrixAttributeModel> GetNonStockMatrixAttributes(string counter, string itemCode);
        IEnumerable<FeaturedItemCustomModel> GetFeaturedItems();
        string GetRatingHeaderJSON(string itemCode);
        string GetProductRatings(string itemCode, int nextRecord, int ratingPageSize, int sortBy);
        string GetProductRatingJSON(string itemCode);
        void SaveRating(string itemCode, int rating, string comment);
        IEnumerable<MatrixItemCustomModel> GetMatrixItemDetails(string searchString, string itemCode, int pageSize, int currentPage);

        ItemWebOptionDescCustomModel GetItemWebOptionDescription(int itemCounter);
    }
}
