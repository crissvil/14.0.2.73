﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Security;
using System.Web;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IFormsAuthenticationService
    {
        void SaveAuthenticationCookie(Guid contactGuid, bool createPersitentCookie);
        string GetRedirectUrl(string cookieUserName, bool createPersistentCookie);
        string FormsCookieName { get; }
        string Encrypt(FormsAuthenticationTicket ticket);
        FormsAuthenticationTicket Decrypt(string data);

        FormsAuthenticationTicket CreateFormsAuthTicket(string cookieUserName, bool createPersistentCookie, int timeOut);
        FormsAuthenticationTicket CreateFormsAuthTicket(string cookieUserName, bool createPersistentCookie);
        FormsAuthenticationTicket CreateFormsAuthTicket(string cookieUserName, DateTime issued, DateTime expired, bool createPersistentCookie);
        FormsAuthenticationTicket CreateFormsAuthTicket(int version, string cookieUserName, DateTime issued, DateTime expired, bool createPersistentCookie);

        void SignOut();
        void SignOut(string key);
        string GetCrossDomainValue();
        void SaveCookie(HttpCookie cookie);
        string FormsCookiePath { get; }
        bool RequireSSL { get; }

        HttpCookie CreateAuthCookie(string name, string value, DateTime expiration);
        HttpCookie CreateAuthCookie(string name, string value, DateTime expiration, string domain);
    }
}
