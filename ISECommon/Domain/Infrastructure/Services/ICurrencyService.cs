﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface ICurrencyService
    {
        string FormatCurrency(decimal amount);
        string FormatCurrency(decimal amount, string currencyCode);
        string GetCurrencySymbol();
        NumberFormatInfo GetNumberFormatInfoByCurrencyCode(string code);
        decimal RoundCurrencyByCustomer(decimal amount);
        decimal RoundMonetaryByCustomer(decimal amount);
        string RawCurrencyByCustomer(decimal amount);
    }
}
