﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using InterpriseSuiteEcommerceCommon.DTO;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IPaymentTermService
    {
        void EnsureNoPaymentTermRequiredIsExisting();

        void EnsureNoPaymentTermRequiredIsAssociatedWithCurrentCustomer(string currentCustomerPaymentTermGroup);

        string[] GetPrefferedGatewayInfo();
        string[] GetPaypalGatewayInfo();
        string[] GetGatewayInterface();
        IEnumerable<PaymentTermDTO> GetPaymentTermOptionsWithoutSagePay(IEnumerable<PaymentTermDTO> options);
    }
}
