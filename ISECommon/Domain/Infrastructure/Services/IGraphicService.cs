﻿using System.Drawing;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IGraphicService
    {
        void Resize(Image origPhoto, string newImageFileWithPath, int width, int height);
    }
}
