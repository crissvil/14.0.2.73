﻿using System.Text;
using System.Collections;
using System.Collections.Generic;
using InterpriseSuiteEcommerceCommon.DTO;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface ICryptographyService
    {
        Hashtable DeserializeSessionParams(string paramXmlValue);
        string SerializeSessionParams(Hashtable sessionParamObject);
        string GetMD5Hash(string stringToHash);
        byte[] InterpriseGenerateSalt();
        byte[] InterpriseGenerateVector();
        string InterpriseEncryption(string value, byte[] salt, byte[] vector);
        string InterpriseDecryption(string value, string salt, string vector);
        bool ValidatePassword(string dbPassword, string inputPassword, string salt, string vector);

        string SerializeToJson<T>(T obj);

        string SerializeToJson<T>(T obj, Encoding encodingType);

        T DeserializeJson<T>(string json);

        string GenerateRandomCode(int codeLength);
        string SerializeMaxMindResponse(MaxMindAPI.MINFRAUD maxmindObject);

    }
}
