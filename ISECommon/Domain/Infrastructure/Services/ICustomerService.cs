﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface ICustomerService
    { 
        void ClearCustomerCoupon(string customerCode, bool isCustomerRegistered);
        bool IsEmailAlreadyInUse(string email, Guid notByThisCustomer);
        Customer MakeAnonymous();
        Customer Find(Guid contactGuid);
        bool ValidateContactSubscription(string contactCode);
        void ClearCustomerSession(Guid customerGuid);
        void CreateContactSiteLog(string contactCode, string details);
        AnonLatestContactGuidAndCustomerCode MakeAnonCustomerRecord();
        void UpdateCustomer(CustomerInfo customerInfo);
        string GenerateRequestCodeForActiveShopper();

        void MakeDefaultAddress(string newAddressId, AddressTypes addressType);
        void MakeDefaultAddress(string newAddressId, AddressTypes addressType, bool byPassEditAddressChecking);
        void MakeDefaultAddress(string contactCode, string oldAddressId, string newAddressId, AddressTypes addressType, bool byPassEditAddressChecking);

        /// <summary>
        /// True if customer shipping addresses been modified(update, add, delete)
        /// </summary>
        /// <returns></returns>
        bool IsCustomerShippingAddressEdited();

        /// <summary>
        /// This will check if the customer modify it's shipping address during checkout and redirect to shopping cart with specific error.
        /// </summary>
        void DoShippingAddressModificationChecking();
        void DoRegisteredCustomerShippingAndBillingAddressChecking();

        void DoIsOver13Checking();
        void DoIsOver13Checking(bool includeRegisterdCustomer);

        void DoIsCreditOnHoldChecking();

        void ClearAddressCheckingKey();
        void UpdateCustomerShipTo(Address shipToAddress);
        void UpdateCustomerShipTo(Address shipToAddress, bool byPassEditAddressChecking);
        void UpdateCustomerShipTo(Customer thisCustomer, Address billToAddress, bool byPassEditAddressChecking, bool useCustParam);
                void UpdateCustomerBillTo(Address billToAddress);
        void UpdateCustomerBillTo(Address billToAddress, bool byPassEditAddressChecking);
        void UpdateCustomerBillTo(Customer thisCustomer, Address billToAddress, bool byPassEditAddressChecking, bool useCustParam);

        CustomerLoyaltyPointsCustomModel GetLoyaltyPoints();
        decimal GetRedemptionMultiplier();
        decimal GetPurchaseMultiplier();

        void DoIsNotRegisteredChecking();
        void DoMobileIsNotRegisteredChecking();
        void DoIsNotRegisteredAndPasswordIsOptionalChecking();
        void DoIsRegisteredAndHasPrimaryBillingAddress();

        void UpdateCustomerRequiredAge();

        void AssignAnonymousCustomerEmailAddressInSalesOrderNote();
        void AssignPayPalExpressCheckoutNoteInSalesOrderNote();
        string GetAnonEmail();

        IEnumerable<CustomerCreditCustomModel> GetCustomerCredits();
        IEnumerable<CustomerCreditCustomModel> GetCustomerCreditMemos();
        IEnumerable<CustomerCreditCustomModel> GetCustomerCreditMemosWithRemainingBalance();

        void ParseName(string fullName, ref string salutation, ref string firstName, ref string middleName, ref string lastName, ref string suffix);
        string FixSpacing(string fullName);
        int GetWordCount(string name);
        string ExtractFirstWord(ref string contactName);
        string ExtractLastWord(ref string contactName);
        string ExtractSalutation(ref string contactName);

        #region Gift Codes

        /// <summary>
        /// get giftcodes owned by customer
        /// </summary>
        /// <returns>gift codes</returns>
        IEnumerable<GiftCodeCustomModel> GetCustomerGiftCodes();

        #endregion

        bool HasChangedVATSetting(VatDefaultSetting newVATSetting);

        bool IsCustomerSubscribeToProductNotification(string itemCode, int notificationType);

        bool IsCustomerEmailNotAvailable(string emailAddress);

        bool IsLeadEmailNotAvailable(string emailAddress);

        bool IsIgnoreStockLevels();

        void TryCreateEcommerceCustomerInfo();
    }
}
