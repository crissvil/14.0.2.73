﻿namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IMailService
    {
        void SendMailRequest(string subject, 
                            string body, 
                            bool useHTML, 
                            string fromemailacccode, 
                            string fromname, 
                            string toaddress, 
                            string toname, 
                            string bccaddresses, 
                            string ReplyTo, 
                            string orderNumber, 
                            bool createAttachment, 
                            bool multipleAttachment, 
                            bool throwError);
    }
}
