﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface INavigationService
    {
        /// <summary>
        /// Will redirect to default.aspx
        /// </summary>
        void NavigateToDefaultPage();

        void NavigateToUrl(string url);
        void NavigateToUrl(string url, bool endResponse);
        void NavigateToShoppingCartRestLinkBackWithErroMessage(string error);
        void NavigateToShoppingCartWitErroMessage(string error);

        /// <summary>
        /// Will redirect to shoppingcart.aspx?resetlinkback=1
        /// </summary>
        void NavigateToShoppingCartRestLinkBack();

        /// <summary>
        /// Will redirect to shoppingcart.aspx
        /// </summary>
        void NavigateToShoppingCart();

        void NavigateToWebAdminDefaultPage(bool checkForReturnUrl);
        void NavigateToWebAdminDefaultPage();
        void NavigateToWebAdminLoginPage();

        /// <summary>
        /// Will redirect to checkoutshippingmult.aspx
        /// </summary>
        void NavigateToCheckoutMult();

        /// <summary>
        /// Will redirect to checkoutpayment.aspx
        /// </summary>
        void NavigateToCheckOutPayment();
        void NavigateToCheckOutPaymentWithErrorMessage(string error);

        void NavigateToCheckout1();
        void NavigateToCheckout1WithErrorMessage(string error);

        /// <summary>
        /// Will redirect to checkoutshipping.aspx
        /// </summary>
        void NavigateToCheckoutShipping();

        /// <summary>
        /// Will redirect to checkoutgiftemail.aspx
        /// </summary>
        void NavigateToCheckoutGiftEmail();
        void NavigateToPageError(string error);
        void NavigateToCheckoutReview();
        void NavigateToSecureForm();
        void NavigateToOrderConfirmation(string orderCode);
        void NavigateToOrderFailed();

        void NavigateToCheckOutStore();
        void NavigateToProductNotFound();
        void NavigateToAccountPage();
        void NavigateToCheckoutAnon();
        void NavigateToCheckoutAnon(bool isCheckout);

        void NavigateToWishList();
        void NavigateToGiftRegistry();

        void NavigateToSignin(string errorMessage);

        void NavigateToRMA();
        void NavigateToRMA(NotificationStatus status, string message);
        void NavigateToCreateRMA();
        void NavigateToCreateRMA(string message);
    }
}
