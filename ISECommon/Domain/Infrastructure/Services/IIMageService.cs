﻿using System.Drawing;
using InterpriseSuiteEcommerceCommon.DTO;
namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IIMageService
    {
        void MakeMobilePicture(string imgEntityType, string imgFileName, Image origPhoto, string fileExt, string customDirectory = "");
        void MakeMinicartPicture(string imgEntityType, string imgFileName, Image origPhoto, string fileExt);
        string GetProductImageUrlByItemCode(string itemCode, ImageSizeTypes size);
    }
}
