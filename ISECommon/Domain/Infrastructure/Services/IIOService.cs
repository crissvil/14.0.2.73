﻿namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IIOService
    {
        string GetProductImageDirectory();
        string GetManufacturerImageDirectory();
        string GetCategoryImageDirectory();
        string GetDepartmentImageDirectory();
        string GetAttributeImageDirectory();
        string GetCompanyLogoImageDirectory();
        string GetMobileImagePath(string entityName, string size, bool fullPath);
        string GetImagePath(string entityName, string size, bool fullPath);
    }
}
