﻿using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
using InterpriseSuiteEcommerceCommon.DTO;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IShippingService
    {
        void EnsureNoShippingMethodRequiredIsExisting();
        void EnsureNoShippingMethodRequiredIsAssociatedWithCurrentCustomer(bool isRegistered, string anonymousShipToCode, string currentCustomerShippingMethodGroup);
        ShippingMethodDTOCollection GetCustomerShippingMethods(string customerOrAnonCode, bool isCouponFreeShipping, string contactCode, string shippingMethodCode, 
                                                               Address preferredShippingAddress, bool isFreeShippingThresholdEnabled);
        bool IsStorePickUpIncludedInCustomerShippingMethods();
        string GetStorePickupShippingMethodDescription();
        void SetRealTimeRateRecord(string shippingMethodCode, string freight, string realTimeRateGUID, bool isFromMultipleShipping);
        ShippingMethodOversizedCustomModel GetOverSizedItemShippingMethod(string itemCode, string unitMesssureCode);
        string GetOverSizedItemShippingMethodToJson(string itemCode, string unitMeasureCode);
    }
}
