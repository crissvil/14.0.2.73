﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Caching;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IApplicationCachingService : ICaching
    {
    }
}
