﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon;
using System.Web;
using System.Web.Security;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.Authentication;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IAuthenticationService
    {
        void AutoLoginFromCookie();
        Customer GetCurrentLoggedInCustomer();
        bool IsAdminCurrentlyLoggedIn();
        string GetCurrentUserIPAddress();
        LoginStatus Login(string email, string password, bool rememberMe);
        void SignOut();
        void TryLoginTicketWhenNotExistCreateAnon();
        void AuthenticateWebService();
        void AuthenticateAdmin();
        AdminLoginStatus TryLoginAdmin(string userName, string password);
        ISSIUserAccount GetCurrentLoggedInAdmin();
        void SignOutAdmin();
        void ExecuteSignInLogic(Customer foundCustomer, bool isCheckoutAnon);
        RememberMeCustomModel GetRememberMeInfo();
        string GetRedirectUrl(string name, bool persistentCookie);
        Customer FindByEmailAndPassword(string email, string password);
        bool ValidateContactSubscription(string contactCode);
        void SecurityCheck();
        Customer LoginFromSagePay(string contactGUID);
    }
}