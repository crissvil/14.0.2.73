﻿namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IStringResourceService
    {
        string GetString(string key, bool returnTextOnly = true);
        string GetString(string key, string customLocalSettings, bool returnTextOnly = false);
    }
}
