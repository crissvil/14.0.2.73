﻿using InterpriseSuiteEcommerceCommon.DTO;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IThirdPartyProcessingService
    {
        MaxMindMinFraudDetail ParseMinFraudSoapObject(string acceptLanguage, string customerIP, string billingcity, string billingstate, string billingpostalCode, string billingcountry, string emailDomain, string bin, string binName, string binPhone,
                                                      string customerPhone, string licenseKey, string requestedType, string forwardedIP, string emailMD5, string usernameMD5, string passwordMD5, string shippingStreetAddress,
                                                      string shippingCity, string shippingState, string shippingPostalCode, string shippingCountry, string transactionID, string sessionID, string userAgent, string orderAmount,
                                                      string orderCurrency, string shopID, string avsResult, string cvvResult, string transactionType);
        MaxMindMinFraudDetail ParseMinFraudSoapObject(string acceptLanguage, string billingCity, string billingState, string billingPostalCode, string billingCountry, string emailDomain, string bin, string binName, string binPhone,
                                                      string customerPhone, string requestedType, string emailMD5, string usernameMD5, string passwordMD5, string shippingStreetAddress,
                                                      string shippingCity, string shippingState, string shippingPostalCode, string shippingCountry, string transactionID, string sessionID, string userAgent, string orderAmount,
                                                      string shopID, string avsResult, string cvvResult, string transactionType);
    }
}
