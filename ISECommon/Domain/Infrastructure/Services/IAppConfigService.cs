﻿using System.Collections.Generic;
using System.Web;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using InterpriseSuiteEcommerceCommon.DTO;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IAppConfigService
    {
        string WebSiteCode { get; }
        string UserCode { get; }
        bool IsSupportedAlternateCheckout { get; }
        bool AllowCreditHold { get; }
        bool AllowProductFiltering { get; }
        bool ReplaceImageURLFromAssetMgr { get; }
        string WebConfigLocale { get; }
        string OnlineCompanyConnectionString { get; }
        string GetUSLocale();
        System.Data.DataSet GetLocales();
        int CacheDurationMinutes { get; }
        string WeightUnits { get; }
        string GetFeedUrl();
        string GetFeedReferenceCurrencyCode();
        int GetLocaleCurrencyCacheMinutes { get; }
        string GetLocaleCurrencyFeedXmlPackage();
        string MailMeToAddress { get; }
        string StoreName { get; }
        bool UseSSL { get; }
        bool HomeTemplateAsIs { get; }
        bool EventLoggingEnabled { get; }
        bool RedirectLiveToWWW { get; }
        string HomeTemplate { get; }
        int DefaultSkinID { get; }
        string LiveServer { get; }
        bool VATIsEnabled { get; }
        bool PreserveActiveCartOnSignin { get; }
        bool ClearOldCartOnSignin { get; }
        bool SecurityCodeRequiredOnStoreLogin { get; }
        int GetInventoryDecimalPlacesPreference();
        AppConfigs GetAllAppConfig();
        StringResources GetAllStringResource();
        bool CaptchaCaseSensitive { get; }
        int GetAllWebSiteCount();
        bool SiteDownForMaintenance { get; }
        string SiteDownForMaintenancePage { get; }
        string SiteDownForMaintenanceURL { get; }
        void TryConnectToDB();
        bool CacheMenus { get; }
        void ClearCustomerSession();
        bool GiftRegistryEnabled { get; }
        string DBSQLServerLocaleSetting { get; }
        string MobileFolderName { get; }
        string MobileDeviceSupport { get; }
        string ShippingMethodCodeIfFreeShippingIsOn { get; }
        string PaymentTermCodeZeroDollarOrder { get; }
        AspNetHostingPermissionLevel DetermineTrustLevel();
        SortedDictionary<string, string> GetRedirectURL();
        InventoryPreference GetInventoryPreference();
        string ShippingMethodCodeZeroDollarOrder { get; }
        string WebAdminAuthenticationName { get; }
        int MaxBadLogins { get; }
        int BadLoginLockTimeOut { get; }
        double WebAdminLoginTimeOut { get; }
        string WebAdminLoginUrl { get; }
        string WebAdminDefault { get; }
        string MicroStyle { get; }
        string MobileImageStyle { get; }
        string MiniCartStyle { get; }
        bool AllowEmptySkuAddToCart { get; }
        bool ShowCustomerServiceNotesInReceipts { get; }
        bool WebSupportEnabled { get; }
        string WebSupportCodeLength { get; }
        string WebSupportTime { get; }
        bool AllowMultipleShippingAddressPerOrder { get; }
        bool RequireOver13Checked { get; }
        bool PasswordIsOptionalDuringCheckout { get; }
        bool CheckoutUseOnePageCheckout { get; }
        string MaxMindSOAPURL { get; }
        string MaxMindLicenseKey { get; }
        bool GiftCodeEnabled { get; }
        bool PayPalCheckoutOverrideAddress { get; }
        bool PayPalCheckoutRequireConfirmedAddress { get; }
        bool LoyaltyPointsEnabled { get; }
        IEnumerable<SystemCountryModel> GetSystemCountries();
        string GetSystemCountriesToJSON();
        void CleanEcommerceCustomerRecords();
        bool RatingsCanBeDoneByAnons { get; }
        decimal CartMinOrderAmount { get; }
        int MinCartItemsBeforeCheckout { get; }
        bool LiveChatEnabled { get; }
        string LiveChatUrl { get; }
        bool ShowStringResourceKeys { get; }
        bool HideOutOfStockProducts { get; }
        bool ShowEditAddressLinkOnCheckOutReview { get; }
        bool AllowShipToDifferentThanBillTo { get; }
        bool ShowPicsInMiniCart { get; }
        bool LinkToProductPageInCart { get; }
        bool ShowStockHints { get; }
        bool ShowCartDeleteItemButton { get; }
        bool ShowActualInventory { get; }
        bool ShippingRatesOnDemand { get; }
        bool ValidateCardEnabled { get; }
        string OnlinePaymentEmailTo { get; }
        string OnlinePaymentEmailToName { get; }
        string OrderFailedEmailTo { get; }
        string OrderFailedEmailToName { get; }
        string CustomerSupportEmailTo { get; }
        string CustomerSupportEmailToName { get; }
        bool ShowBuyButtons { get; }
        bool ShowFeaturedItem { get; }
        string FeaturedItemsLayout { get; }
        int FeaturedItemsColumn { get; }
        bool FeaturedItemsDisplayAddToCart { get; }
        bool FeaturedItemsDisplayPic { get; }
        bool FeaturedItemsDisplayPrice { get; }
        bool FeaturedItemsDisplayRating { get; }
        string FeaturedItemsXMLPackage { get; }
        bool ShowEntityLoyaltyPoints { get; }
        bool ShowProductLoyaltyPoints { get; }
        string SearchXMLPackage { get; }
        string SearchAdvXMLPackage { get; }
        bool DisallowOrderNotes { get; }
        int NumHomePageSpecials { get; }
        bool WholesaleOnlySite { get; }
        bool ShowItemPriceWhenLogin { get; }
        bool WatermarkIsEnabled { get; }
        string AddToCartAction { get; }
        int RatingPageCount { get; }
        bool LimitCartToQuantityOnHand { get; }
        bool CreditRedemptionIsEnabled { get; }
        bool RatingIsEnabled { get; }
        string CBNCompanyID { get; }
        bool CreateReceiptForAllPaymentType { get; }
        string SagePayTransactionMode { get; }
        string SagePayVendorName { get; }
        string SagePayPaymentTerm { get; }
        bool ShowShipDateInCart { get; }
        bool NotifyOnPriceDropEnabled { get; }
        bool NotifyWhenAvailEnabled { get; }

        bool ShowPicsInCart { get; }

        bool AllowCustomerDuplicateEMailAddresses { get; }
        bool HideUnitMeasure { get; }
        bool UseWebStorePricing { get; }
    }
}
