﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IDateTimeService
    {
        DateTime GetCurrentDate();
        DateTime GetMaxValue();
    }
}
