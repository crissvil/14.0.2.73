﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface ICustomerRepository
    {
        Customer Find(Guid contactGuid, string webSiteCode, bool allowCreditHold, bool allowProductFiltering);
        void MakeAnonymousCustomerRecord(out string customerCode, out Guid contactGuid, string webSiteCode);
        AnonCustomerSettings GetAnonCustomerSettings(string webSiteCode);
        AnonLatestContactGuidAndCustomerCode GetAnonLatestContactGuidAndCustomerCode(string newGuidString);
        string GetAnonEmail(string customerCode);
        bool IsCustomerHasOrders(string CustomerID);
        bool IsCustomerHasAtLeastOneAddress(string CustomerID);
        bool IsCustomerHasUsedCoupon(string couponCode, string customerCode);
        bool AnyCustomerHasUsedCoupon(string couponCode);
        bool CustomerCanAvailOfThisCoupon(string customerCode, string couponCode);
        bool OwnsThisAddress(string customerCode, string shipToCode);
        void ClearCouponForRegisteredCustomer(string customerCode);
        void ClearCouponForAnonCustomer(string customerID);
        Customer FindByEmailAndPassword(string email, string password, string webSiteCode);

        void TryCreateEcommerceCustomerInfo(string email, string websiteCode);

        bool ValidateContactSubscription(string contactCode, DateTime currentDate, DateTime subExpDate, string webSiteCode);

        void ClearCustomerSession(Guid customerGuid, DateTime currentDate);
        void CreateContactSiteLog(string contactCode, string details, DateTime currentDate, string webSiteCode, string userCode);
        string GetCBNCustomerCode(int cbnNetworkId);

        //Gift Registry
        Guid? GetContactCodeByActiveShoppersCode(string code);

        bool IsEmailAlreadyInUse(string email, Guid notByThisCustomer, string webSiteCode);
        bool IsEmailAlreadyInUse(string email, string webSiteCode);
        bool EmailInUse(string email, string CustomerCode);

        string CreateAnonCustomer(string localeSettings, string currencyCode);
        void UpdateEcommerceCustomerCodeToID(string contactGuid);
        string GetUpdatedCustomerIdByContactGuid(string contactGuid);

        void UpdateCustomer(CustomerInfo customerInfo);
        string GetPassword(string contactCode);

        SalesOrderHistoryCollection GetCustomerSalesOrders(int pages, int current, bool showCustomerServiceNotesInReceipts, string websiteCode, Customer thisCustomer);

        bool IsActiveShopperCodeNotUsed(string customerCode);
        void DeleteInActiveShoppers(DateTime currentTime, int timeLimit);
        void AddActiveShoppers(ActiveShopper activeShopper);
        string GetShopperRequestCodeByCustomerCode(string customerCode);

        IEnumerable<SystemWareHouse> GetDealerCustomersAsWareHouse();

        string GetCustomerNotes(string customerCode);
        void UpdateContactDefaultShippingCode(string addressId, string contactCode);
        void UpdateContactDefaultBillingCode(string addressId, string contactCode);

        void ClearCustomerShippingAddressPlus4Field(string code);
        void ClearCustomerBillingAddressPlus4Field(string code);

        RememberMeCustomModel GetRememberMeInfo(Guid contactGuid, string websiteCode);

        CustomerTransactionModel GetCustomerTransaction(string documentCode);
        Address GetCustomerTransactionAddress(string orderNumber, bool isInvoice, bool isBilling);

        bool IsVoidedCustomerInvoice(string invoiceCode);
        bool IsVoidedCustomerSalesOrder(string salesOrderCode);

        void UpdateCustomerRequiredAge(string customerCode);

        CustomerLoyaltyPointsCustomModel GetCustomerLoyaltyPoints(string customerCode);

        CustomerLoyaltyPointsCustomModel GetCustomerLoyaltyPoints(string customerCode, decimal redemptionMultiplier);

        string GetCustomerAdvancePreference(string name);

        string GetTransactionType(string documentCode);

        OpenInvoicesCollection GetCustomerOpenInvoices(int pages, int current, string websiteCode, string contactCode);

        IEnumerable<CustomerCreditCustomModel> GetCustomerCredits(string customerCode);

        string GetSalutation(string name);
        string GetSuffix(string name);

        #region Gift Code

        IEnumerable<GiftCodeCustomModel> GetCustomerGiftCodes(string customerCode);

        #endregion

        CustomerProductNotificationSubscription GetCustomerProductNotificationSubscription(string contactCode, string webSiteCode, string itemCode, string emailAddress);

        bool IsCustomerEmailNotAvailable(string emailAddress, string customerCode);
        
        bool IsLeadEmailNotAvailable(string emailAddress);

        bool IsIgnoreStockLevels();

        CustomerInvoiceCustomModel GetCustomerInvoice(string contactCode, string invoiceCode, string websiteCode);
        IEnumerable<CustomerInvoiceCustomModel> GetCustomerInvoices(string contactCode, string websiteCode);
        IEnumerable<CustomerInvoiceItemCustomModel> GetCustomerInvoiceItems(string contactCode, string invoiceCode, string websiteCode);
        CustomerRMACustomModel GetCustomerRMA(string contactCode, string rmaCode, string websiteCode);
        IEnumerable<CustomerRMACustomModel> GetCustomerRMAs(string contactCode, string websiteCode);
        IEnumerable<CustomerRMAItemCustomModel> GetCustomerRMAItems(string contactCode, string rmaCode, string websiteCode);
    }
}
