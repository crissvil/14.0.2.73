﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface ISystemPostalCodeRepository
    {
        bool IsAddressAlreadyExist(string postalCode, string stateCode, string city, string countryCode);
    }
}
