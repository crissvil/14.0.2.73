﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IRepository
    {
        int GetSqlN(string query);
        string GetSqlS(string Sql);
        int GetSqlNByQueryConfigId(string queryId);
        int GetSqlNByQueryConfigId(string queryId, params object[] parameters);

        void TestDBConnection();

        /// <summary>
        /// Get the DBQuery sql statement
        /// </summary>
        /// <param name="id">DBQuery ID specified in db.xml.config</param>
        /// <returns></returns>
        string GetQueryConfig(string id);

        /// <summary>
        /// Get the DBQuery sql statement
        /// </summary>
        /// <param name="id">DBQuery ID specified in db.xml.config</param>
        /// <param name="parameters">String.Format Param Args</param>
        /// <returns>query value</returns>
        string GetQueryConfig(string id, params object[] parameters);
    }
}
