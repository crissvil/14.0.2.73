﻿using InterpriseSuiteEcommerceCommon.InterpriseIntegration.Authentication;
using System;
namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IUserAccountRepository
    {
        ISSIUserAccount Find(string userName, string websiteCode);
        void ClearSecurity(string userCode);
        void IncrementBadLogin(string userCode, DateTime dt);
        IBadLoginField ReloadBadLoginInfo(string userCode);
        void ResetBadLoginCount(string userName);
        void LockUserAccount(string userName, string lockUnti);
    }
}
