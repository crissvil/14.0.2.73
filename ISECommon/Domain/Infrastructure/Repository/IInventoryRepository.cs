﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IInventoryRepository
    {
        bool IsAKit(string productId);
        int GetProductCount();  
        string GetInventoryItemType(string itemCode);
        int GetInventoryItemCounter(string itemCode);
        int GetCheckOutOptionCount(string websiteCode, string contactCode, string customerCode, bool isNotRegistered, string languageCode, string currentDate, string productFilterId, ref string xmlString);
        InventoryProductModel GetInventoryItem(string languageCode, string itemCode);
        IEnumerable<InventoryProductModel> GetInventoryItems(string languageCode);
        IEnumerable<InventoryProductModel> GetInventoryItemsWithNoImages(string languageCode);
        IEnumerable<InventoryOverrideImageModel> GetItemImages(string websiteCode, string itemCode);
        IEnumerable<InventoryCategoryModel> GetInventoryCategories(string languageCode);
        IEnumerable<SystemCategoryModel> GetSystemCategories(string languageCode);
        decimal GetItemWeight(string itemCode, string unitMeasureCode);
        bool UseCustomerPricingForKit(string itemKitCode);
        decimal GetKitDiscount(string itemKitCode, string contactCode);
        IEnumerable<SelectedKitComponentModel> GetSelectedKitComponents(string itemKitCode, string currencyCode, string websiteCode, string cartId, bool isAnonymous, string customerCode, string anonymousCustomerCode, string contactCode, bool showDefault);
        ValidCartItemCustomModel GetValidItemCodeAndBaseUnitMeasureById(int itemId, bool isAnonymous, string contactCode, string websiteCode, string currentDate, string productFilterID);
        string GetMatrixItemCode(string itemCode);
        SystemEntityModel GetVirtualPageSettings(int entityId, string entityName, string languageCode, string websiteCode);
        IEnumerable<MatrixItemWebOptionDescCustomModel> GetMatrixItemWebOptionDescription(string websiteCode, string languageCode, string itemCode);
        IEnumerable<MatrixItemWebOptionDescCustomModel> GetNonStockMatrixItemWebOptionDescription(string websiteCode, string languageCode, IEnumerable<string> itemCodes);
        ECommerceProductInfoViewCustomModel GetProductInfoViewForShowProduct(string itemCode, string localeSettings, string userCode, string websiteCode, string dateTimeStringFromDB, string productFilterId, string contactCode);
        ItemWebOptionCustomModel GetItemWebOption(string websiteCode, string itemCode, bool IsExpressPrint);
        IEnumerable<ItemWebOptionCustomModel> GetItemWebOptions(string websiteCode, IEnumerable<string> itemCodes, bool IsExpressPrint);
        InventoryPriceInfoModel GetProductPriceInfo(string customerCode, string currencyCode, string itemCode, string unitMeasure, decimal unitMeasureQuantity, decimal quantity, decimal matrixGroupQuantity, string languageCode, string websiteCode);

        IEnumerable<UnitMeasureModel> GetItemUnitMeasuresUnitMeasureList(string itemCode, IEnumerable<string> unitMeasureCodes);
        IEnumerable<UnitMeasureModel> GetItemBaseUnitMeasures(string itemCode);
        UnitMeasureModel GetItemDefaultUnitMeasure(string itemCode);
        InventoryMatrixItemModel GetMatrixItemInfo(string matrixItemCode);
        InventoryMatrixItemModel GetMatrixItemInfo(int counter);
        IEnumerable<MatrixAttributeModel> GetMatrixAttributes(string itemCode, string languageCode);
        IEnumerable<MatrixAttributeModel> GetNonStockMatrixAttributes(string counter, string itemCode, string websiteCode);
        IEnumerable<FeaturedItemCustomModel> GetFeaturedItems(string websiteCode, string localeSetting, string contactCode);
        RatingComputationCustomModel GetRatingComputation(string itemCode, string websiteCode);
        RatingModel GetRating(string itemCode, string customerCode, string contactCode, string websiteCode);
        void SaveRating(RatingModel model);
        IEnumerable<MatrixItemCustomModel> GetMatrixItemDetails(string searchString, string itemCode, int pageSize, int currentPage, string languageCode, string webSiteCode, string contactCode);

        ItemWebOptionDescCustomModel GetItemWebOptionDescription(int itemCounter, string languageCode, string websiteCode);
    }
}
