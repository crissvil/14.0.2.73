﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IGatewayRepository
    {
        GatewayInformationCustomModel GetEcommerceCreditCardGatewayByWebsite(string websiteCode);
        GatewayInformationCustomModel GetEcommerceCreditCardGatewayByPaymentTerm(string paymentTermCode);
        GatewayInformationCustomModel GetEcommercePaypalGatewayInfo(string websiteCode);
        string GetCreditCardGatewayDescription(string gateway);
    }
}
