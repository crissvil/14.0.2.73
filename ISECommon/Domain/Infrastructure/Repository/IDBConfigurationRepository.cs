﻿using System.Collections.Generic;
using System.Data;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Domain.Model;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IDBConfigurationRepository
    {
        DataSet GetLocales();
        AppConfigs GetAllAppConfig(string websiteCode);
        StringResources GetAllStringResource(string webSiteCode);
        int GetAllWebSiteCount(string webSiteCode);
        void TryConnectToDB();
        void ClearCustomerSession();
        SortedDictionary<string, string> GetRedirectURL(string websiteCode);
        InventoryPreference GetInventoryPreference();
        IEnumerable<SystemCountryModel> GetSystemCounties();
        void CleanEcommerceCustomerRecord(string websiteCode);
    }
}
