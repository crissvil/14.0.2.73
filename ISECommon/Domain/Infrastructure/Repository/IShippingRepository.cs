﻿using System.Collections.Generic;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
using InterpriseSuiteEcommerceCommon.DTO;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IShippingRepository
    {
        bool NoShippingMethodRequiredShippingMethodExisting(string shippingMethodCode);
        string GetShippingMethodGroupByShipToCode(string anonymousShipToCode);
        bool CheckIfShippingMethodInShippingMethodGroup(string shippingMethodGroup, string shippingMethodCode);
        bool CheckIfShippingMethodGroupExists(string shippingMethodGroup);
        void DeleteEcommerceRealtimeReadByContactCode(string contactCode);
        void CreateRealTimeRate(string contactCode, string shippingMethodCode, string freight, string realTimeRateGUID, bool isFromMultipleShipping);
        ShippingMethodDTOCollection GetCustomerShippingMethods(string customerOrAnonCode, bool isCouponFreeShipping, string contactCode,
            string shippingMethodCode, string addressId, string residenceType, bool isNotRegistered, string country, string defaultPrice, 
            string postalCode, string shippingMethodCodeZerroDollarOrder, bool isShippingRateOndemand, bool isFreeShippingEnabled, string freightDisplayResource, decimal exchangeRate);
        ShippingMethodOversizedCustomModel GetOverSizedItemShippingMethod(string itemCode, string unitMeasureCode);
    }
}
