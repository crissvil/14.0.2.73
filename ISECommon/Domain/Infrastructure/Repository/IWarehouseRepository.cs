﻿using System.Collections.Generic;
using System.Data;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IWarehouseRepository
    {
        IEnumerable<StorePickupInventoryWarehouseCustomModel> GetInventoryWarehouseForStorePickup(string postalCode, string city, string state, string country, string itemCode, string kitComposition, int kitCompositionLength, string unitMeassureCode, int nextRecord, byte recordLimit);
        IEnumerable<StoreWorkingHoursModel> GetStoreWorkingHours(string warehouseCode);
        IEnumerable<StoreHolidayHours> GetStoreHolidayHours(string warehouseCode);
        DataRow GetWarehouseRowByCode(string warehouseCode);
        InventoryWarehouseModel GetWarehouseByCode(string warehouseCode);
    }
}
