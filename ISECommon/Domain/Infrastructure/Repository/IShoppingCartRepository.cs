﻿using System;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
using System.Collections.Generic;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IShoppingCartRepository
    {
        void UpdateShoppingCartRecordQuantity(int cartRecordID, decimal quantity, string customerCode, string contactCode);
        void DeleteShoppingCartRecord(int cartRecordID, string customerCode, string contactCode);
        void DeleteShoppingCartItemReservation(string itemCode, string contactCode);
        decimal GetTotalCartItemsByCustomer(string CustomerID, CartTypeEnum CartType, string ContactCode, string webSiteCode);
        void UpdateAnonymousCart(CartTypeEnum cartType, string customerCode, string anonimousCode, string shipToCode, string contactCode);
        void ClearCart(CartTypeEnum cartType, string customerId, string contactCode);
        void ClearKitItems(string customerCode, string itemKitCode, Guid cartId);
        void ClearEcommerceCartShippingAddressIDForAnonCustomer(string anonCustomerCode, string WebsiteCode);

        void ClearCartLineItems(string[] arrayOfCartId);
        void ClearCartCustomerReservation(string contactCode);
        void ClearLineItemsKitCompositions(string[] arrayOfCartId);
        EcommerceShoppingCartModel GetEcommerceShoppingCartByShoppingCartRecId(int recId);
        void UpdateShoppingCartShippingAddressIdByOldPrimaryId(string newAddressId, string oldAddressId, string contactCode);
        void UpdateShoppingCartShippingAddressIdForRegistryItems(string newAddressId, string oldAddressId);

        IEnumerable<ShoppingCartGiftEmailCustomModel> GetShoppingCartGiftEmails(string contactCode, string languageCode);
        void CreateShoppingCartGiftEmail(int shoppingCartRecID, string itemCode, int lineNum, string email, string userCode);
        void CreateShoppingCartGiftEmail(List<ShoppingCartGiftEmailCustomModel> giftEmails, string userCode);
        void UpdateShoppingCartGiftEmail(int counter, string emailRecipient, string userCode);
        void UpdateShoppingCartGiftEmail(List<ShoppingCartGiftEmailCustomModel> giftEmails, string userCode);
        void DeleteShoppingCartGiftEmailTopRecords(int shoppingCartRecID, int topRecords);
        void CleanupShoppingCartGiftEmail();
        void CreateCustomerCartGiftEmail(string documentCode, List<ShoppingCartGiftEmailCustomModel> giftEmails, string userCode);

        void UpdateShoppingCartInStoreShippingInfo(int shoppingCartRecId, string shippingMethod, string warehouseCode, Guid realTimeRateGuid);
        void ClearCartWarehouseCodeByCustomer(string customerCodeOrId);
        void UpdateAllocatedQty(decimal allocatedQty, string contactCode, string itemCode, string unitMeasureCode);
        void UpdateAllocatedQty(IEnumerable<CustomerSalesOrderDetailViewDTO> items, string contactCode);

        bool HasNoStockAndNoOpenPOComponent(string itemCode, string currencyCode, string languageCode, string customerCode, string cartID, string contactCode, string websiteCode);
        bool IsCartHasGiftRegistryItem(string customerCode);
        bool IsCartHasStorePickupItem(string customerCode);
        int UpdateInStorePickupCartItemStock(string customerCode, string contactCode, string websiteCode);

        void UpdateAppliedCreditCodesJSON(string creditCodesJSON, Guid contactGuid);
        void ClearAppliedCreditCodesJSON(Guid contactGuid);
        string GetAppliedCreditCodesJSON(Guid contactGuid);

        #region Gift Codes

        IEnumerable<GiftCodeCustomModel> GetGiftCodesDetail(string[] giftcodes);
        IEnumerable<GiftCodeCustomModel> GetGiftCodeDetail(string giftcode);
        void UpdateGiftCode(Guid contactGuid, string giftCodesSerialized);
        void ClearGiftCode(Guid contactGuid);
        string GetGiftCode(Guid contactGuid);

        #endregion

        void DeleteEcommerceCustomerCartRecord(string customerID, string websiteCode, int cartType, DateTime ageDate);

        IEnumerable<ItemReservationCustomModel> GetItemReservation(string contactCode, string customerCode, string websiteCode);

        void UpdateAllocatedQuantities(IEnumerable<ItemReservationCustomModel> items, string contactCode, string customerCode, string websiteCode);
    }
}