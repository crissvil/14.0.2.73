﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IPaymentTermRepository
    {
        bool NoPaymentTermRequiredPaymentTermExisting(string noPaymentPaymentTermCode);
        bool CheckPaymentTermCodeInSystemPaymentTermGroupDetail(string paymentTermCode, string paymentTermGroup);
        string GetGateWayDescriptionByGateway(string gateWay);
    }
}
