﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface IBadLoginField
    {
        int BadLoginCount { get; set; }
        DateTime LastBadLoginDate { get; set; }
        DateTime LockedInUntilDate { get; set; }
    }
}
