﻿using System.Web.Caching;

namespace InterpriseSuiteEcommerceCommon.Domain.Infrastructure
{
    public interface ICaching
    {
        object GetItem(string key);
        T GetItem<T>(string key) where T : class;
        void SetItem(string key, object value, int defaultMinutes = 10);
        object AddItem(string key, object value, int minutes);
        void AddItem(string key, object value, CacheDependency cachedependency = null);
        object AddItem(string key, object value, System.DateTime expiration, CacheDependency cachedependency = null);
        int ItemsCount { get; }
        object RemoveItem(string key);
        bool Exist(string key);
        void Reset();
    }
}
