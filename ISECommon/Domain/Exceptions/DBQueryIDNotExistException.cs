﻿using System;

namespace InterpriseSuiteEcommerceCommon.Domain.Exceptions
{
    public class DBQueryIDNotExistException: Exception
    {
        public DBQueryIDNotExistException() { }
        public DBQueryIDNotExistException(string message) : base(message) { }
    }
}