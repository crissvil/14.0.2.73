﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Exceptions
{
    public class ConnectionStringNotInitializedException : Exception
    {
        public ConnectionStringNotInitializedException() : base("Database connection failed! Use the Configuration tool to set the connection string.")
        {
        }

        public ConnectionStringNotInitializedException(string message) : base(message) { }
    }
}
