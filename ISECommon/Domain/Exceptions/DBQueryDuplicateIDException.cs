﻿using System;

namespace InterpriseSuiteEcommerceCommon.Domain.Exceptions
{
    public class DBQueryDuplicateIDException: Exception
    {
        public DBQueryDuplicateIDException() { }
        public DBQueryDuplicateIDException(string message) : base(message) { }
    }
}