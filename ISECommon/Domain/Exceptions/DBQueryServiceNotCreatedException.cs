﻿using System;

namespace InterpriseSuiteEcommerceCommon.Domain.Exceptions
{
    public class DBQueryServiceNotCreatedException : Exception
    {
        public DBQueryServiceNotCreatedException() { }
        public DBQueryServiceNotCreatedException(string message) : base(message) { }
    }
}
