﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Exceptions
{
    public class ServiceOrRepositoryNotRegisteredException : Exception
    {
        public ServiceOrRepositoryNotRegisteredException() : base("Service or Repository does not exist! Please verify if inherits the ServiceBase for services and RepositoryBase for repositories.")
        {
        }

        public ServiceOrRepositoryNotRegisteredException(string message) : base(message) { }
    }
}
