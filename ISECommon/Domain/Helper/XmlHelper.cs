﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Collections.Generic;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public static class XmlHelper
    {
        public static XDocument ToXDocument(string path)
        {
            return XDocument.Load(path);
        }

        public static IEnumerable<XElement> GetDescendants(XDocument document, string selector)
        {
            return document.Root.Descendants(selector);
        }
        
    }
}
