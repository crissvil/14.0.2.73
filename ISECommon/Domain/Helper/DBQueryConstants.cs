﻿namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class DBQueryConstants
    {
        public const string ReadLocaleSettings = "Read_LocaleSettings";
        public const string ReadAppConfigs = "Read_AppConfigs";
        public const string ReadAllStringResource = "Read_AllStringResource";
        public const string ReadProductCount = "Read_ProductCount";
        public const string DeleteCustomerSession = "Delete_CustomerSession";
        public const string ReadEcommerceActiveShoppersView = "Read_EcommerceActiveShoppersView";
        public const string ReadCustomerTotalCartItems = "Read_TotalCartItemsByCustomer";
        public const string ReadIsEmailAlreadyInUse = "Read_IsEmailAlreadyInUse";
        public const string ReadEmailInUse = "Read_EmailInUse";
        public const string ReadPassword = "Read_Password";
        public const string ReadIsAKit = "Read_IsAKit";
        public const string ReadAnonEmail = "Read_AnonEmail";
        public const string ReadAllWebSiteCount = "Read_AllWebSiteCount";
        public const string CreateCreateAnonCustomer = "Create_CreateAnonCustomer";
        public const string UpdateUpdateCustomerCodeToCustomerId = "Update_UpdateCustomerCodeToCustomerId";
        public const string ReadUpdatedCustomerCodeByContactGuid = "Read_UpdatedCustomerCodeByContactGuid";
        public const string ReadFindCustomer = "Read_FindCustomer";
        public const string ReadOwnsThisAddress = "Read_OwnsThisAddress";
        public const string GetCBNCustomerCode = "Get_CBNCustomerCode";
        public const string ReadEcommerceCheckOutOptions = "Read_EcommerceCheckOutOptions";
        public const string GetUseCustomerPricingForKit = "Get_UseCustomerPricingForKit";
        public const string GetKitDiscount = "Get_KitDiscount";
        public const string GetSelectedKitComponents = "Get_SelectedKitComponents";
        public const string GetValidItemCodeAndBaseUnitMeasureById = "Get_ValidItemCodeAndBaseUnitMeasureById";
        public const string GetVirtualPageSettings = "Get_VirtualPageSettings";
        public const string GetMatrixItemWebOptionDescription = "Get_MatrixItemWebOptionDescription";
        public const string GetNonStockMatrixItemWebOptionDescription = "Get_NonStockMatrixItemWebOptionDescription";

        public const string UpdateECommerceUpdateCustomer = "Update_ECommerceUpdateCustomer";
        public const string UpdateAnonymousCart = "Update_AnonymousCart";
        public const string ReadClearCart = "Read_ClearCart";
        public const string ReadRedirectURL = "Read_RedirectURL";
        public const string ReadInventoryPreferenceForCache = "Read_InventoryPreferenceForCache";
        
        public const string ReadShippingMethodCodeRequiredChecking = "Read_ShippingMethodCodeRequiredChecking";
        public const string GetShippingMethodGroupByShipToCode = "Get_ShippingMethodGroupByShipToCode";
        public const string GetShippingMethodGroupInSystemShippingMethodGroupDetailByShippingMethodCode = "Get_ShippingMethodGroupInSystemShippingMethodGroupDetailByShippingMethodCode";
        public const string GetShippingMethodGroupByGroupCode = "Get_ShippingMethodGroupByGroupCode";
        
        public const string ReadPaymentTermCodeRequiredChecking = "Read_PaymentTermCodeRequiredChecking";
        public const string GetPaymentTermCodeInSystemPaymentTermGroupDetail = "Get_PaymentTermCodeInSystemPaymentTermGroupDetail";
        
        public const string DeleteClearKitItems = "Delete_ClearKitItems";
        public const string GetInventoryItemType = "Get_InventoryItemType";
        public const string GetInventoryItemCounter = "Get_InventoryItemCounter";
        public const string UpdateClearEcommerceCartShippingAddressIDForAnonCustomer = "Update_ClearEcommerceCartShippingAddressIDForAnonCustomer";

        public const string ReadInventoryItem = "Read_InventoryItem";
        public const string ReadInventoryItems = "Read_InventoryItems";
        public const string ReadInventoryItemsWithNoImages = "Read_InventoryItemsWithNoImages";
        public const string ReadInventoryCategories = "Read_InventoryCategories";
        public const string ReadSystemCategories = "Read_SystemCategories";
        public const string ReadItemImages = "Read_ItemImages";
        public const string ReadEcommerceCustomerWebOrder = "Read_EcommerceCustomerWebOrder";
        public const string ReadEcommerceCompanyWebOrder = "Read_EcommerceCompanyWebOrder";
        public const string DeleteShoppingCartItems = "Delete_ShoppingCartItems";
        public const string DeleteShoppingCartCustomerReservation = "Delete_ShoppingCartCustomerReservation";
        public const string DeleteShoppingCartKitComposition = "Delete_ShoppingCartKitComposition";
        public const string ReadEcommerceShoppingCart = "Read_EcommerceShoppingCart";
        public const string GetItemWeight = "Get_ItemWeight";
        public const string GetIsActiveShopperCodeUsed = "Get_IsShopperCodeUsed";
        public const string DeleteInActiveShoppers = "Delete_InActiveShoppers";
        public const string CreateActiveShoppers = "Create_ActiveShoppers";
        public const string GetActiveShopperRequestCodeByCustomerCode = "Get_ActiveShopperRequestCodeByCustomerCode";
        public const string ReadDealersFromCustomerAsWareHouse = "Read_DealersFromCustomerAsWareHouse";
        public const string GetCustomerNotes = "Get_CustomerNotes";
        public const string UpdateCustomerNotes = "Update_CustomerNotes";
        public const string Update_ShoppingCartShippingAddressIdByOldPrimaryId = "Update_ShoppingCartShippingAddressIdByOldPrimaryId";
        public const string Update_ShoppingCartShippingAddressIdForRegistryItems = "Update_ShoppingCartShippingAddressIdForRegistryItems";
        public const string UpdateContactDefaultShippingCode = "Update_ContactDefaultShippingCode";
        public const string UpdateContactDefaultBillingCode = "Update_ContactDefaultBillingCode";
        public const string UpdateCustomerShiptoPlus4ToNull = "Update_CustomerShiptoPlus4ToNull";
        public const string UpdateCustomerBillToPlus4ToNull = "Update_CustomerBillToPlus4ToNull";
        public const string GetEcommerceLoginInfo = "Get_EcommerceLoginInfo";
        public const string GetEcommerceGetRememberMeInfo = "Get_EcommerceGetRememberMeInfo";
        public const string GetMatrixItemCode = "Get_MatrixItemCode";

        public const string ReadEcommerceShoppingCartGiftEmails = "Read_EcommerceShoppingCartGiftEmails";
        public const string CreateEcommerceShoppingCartGiftEmail = "Create_EcommerceShoppingCartGiftEmail";
        public const string UpdateEcommerceShoppingCartGiftEmail = "Update_EcommerceShoppingCartGiftEmail";
        public const string DeleteEcommerceShoppingCartGiftEmailTopRecords = "Delete_EcommerceShoppingCartGiftEmailTopRecords";
        public const string DeleteEcommerceShoppingCartGiftEmail = "Delete_GiftEmail";
        public const string CreateCustomerGiftEmail = "Create_CustomerGiftEmail";

        public const string GetGiftCodesAppliedToShoppingCart = "Get_GiftCodesAppliedToShoppingCart";
        public const string UpdateCustomerGiftCode = "Update_CustomerGiftCode";
        public const string UpdateCustomerGiftCodeToNull = "Update_CustomerGiftCodeToNull";
        public const string ReadGiftCodes = "Read_GiftCodes";
        public const string ReadGiftCodesByCustomerCode = "Read_GiftCodesByCustomerCode";

        public const string GetCustomerInvoiceVoidStatus = "Get_CustomerInvoiceVoidStatus";
        public const string GetCustomerSalesOrderVoidStatus = "Get_CustomerSalesOrderVoidStatus";
        public const string GetCreditCardGatewayDescription = "Get_CreditCardGatewayDescription";

        public const string ReadCustomerTransactionAddress = "Read_CustomerTransactionAddress";
        public const string ReadECommerceCustomerTransaction = "Read_ECommerceCustomerTransaction";
        public const string UpdateCustomerRequiredAge = "Update_CustomerRequiredAge";

        public const string GetEcommerceCreditCardGatewayByWebsite = "Get_EcommerceCreditCardGatewayByWebsite";
        public const string GetEcommerceCreditCardGatewayByPaymentTerm = "Get_EcommerceCreditCardGatewayByPaymentTerm";
        public const string GetEcommercePaypalGatewayInfo = "Get_EcommercePaypalGatewayInfo";

        public const string ReadCustomerLoyaltyPoints = "Read_CustomerLoyaltyPoints";
        public const string GetCustomerAdvancePreference = "Get_CustomerAdvancePreference";

        public const string ReadSystemPostalCodeCount = "Read_SystemPostalCodeCount";
        public const string GetProductInfo = "Get_ProductInfo";
        public const string ReadSystemCountry = "Read_SystemCountry";
        public const string Get_EcommerceSystemWarehouseStorePickup = "Get_EcommerceSystemWarehouseStorePickup";
        public const string ReadEcommerceSystemWarehouseWorkingHours = "Read_EcommerceSystemWarehouseWorkingHours";
        public const string ReadEcommerceSystemWarehouseWorkingHolidays = "Read_EcommerceSystemWarehouseWorkingHolidays";
        public const string DeleteEcommerceRealTimeRateByContactCode = "Delete_EcommerceRealTimeRateByContactCode";
        public const string CreateRealTimeRate = "Create_RealTimeRate";
        public const string UpdateShoppingCartInStoreShippingInfo = "Update_ShoppingCartInStoreShippingInfo";
        public const string UpdateClearCartWarehouseCodeByCustomer = "Update_ClearCartWarehouseCodeByCustomer";
        public const string ReadInventoryWarehouseByWarehouseCode = "Read_InventoryWarehouseByWarehouseCode";

        public const string ReadInventoryItemWebOption = "Read_InventoryItemWebOption";
        public const string UpdateAllocatedQty = "Update_AllocatedQty";
        public const string ReadEcommercePrice = "Read_EcommercePrice";
        public const string GetEcommerceKitCartDetailWhenHideOutOfStock = "Get_EcommerceKitCartDetailWhenHideOutOfStock";
        public const string Read_BaseUnitMeasures = "Read_BaseUnitMeasures";
        public const string Read_DefaultUnitMeasure = "Read_DefaultUnitMeasure";
        public const string Read_UnitMeasuresByUnitMeasureList = "Read_UnitMeasuresByUnitMeasureList";
        public const string Read_EcommerceCustomerShippingMethodList = "Read_EcommerceCustomerShippingMethodList";
        public const string Read_GatewayDescription = "Read_GatewayDescription";
        public const string ReadMatrixItemInfo = "Read_MatrixItemInfo";
        public const string ReadMatrixItemInfoByCounter = "Read_MatrixItemInfoByCounter";
        public const string ReadMatrixAttributes = "Read_MatrixAttributes";
        public const string ReadNonStockMatrixAttributes = "Read_NonStockMatrixAttributes";
        public const string Get_CartHasRegistryItem = "Get_CartHasRegistryItem";
        public const string Get_CartHasPickUpItem = "Get_CartHasPickUpItem";
        public const string Read_FeaturedItems = "Read_FeaturedItems";
        public const string Get_RatingAveByItemCode = "Get_RatingAveByItemCode";
        public const string GetTransactionType = "Get_TransactionType";
        public const string GetReportCode = "Get_ReportCode";
        public const string GetOrderConfirmationReport = "Get_OrderConfirmationReport";
        public const string ReadEcommerceCustomerOpenInvoices = "Read_EcommerceCustomerOpenInvoices";

        public const string Update_InStorePickupCartItemStock = "Update_InStorePickupCartItemStock";
        public const string Read_OverSizedItemShippingMethod = "Read_OverSizedItemShippingMethod";

        public const string Read_CustomerCredit = "Read_CustomerCredit";
        public const string Update_CustomerCreditCode = "Update_CustomerCreditCode";
        public const string Read_CustomerCreditCode = "Read_CustomerCreditCode";
        public const string Update_ClearCustomerCreditCode = "Update_ClearCustomerCreditCode";
        public const string Read_Rating = "Read_Rating";
        public const string CreateUpdate_Rating = "CreateUpdate_Rating";

        public const string Get_Salutation = "Get_Salutation";
        public const string Get_Suffix = "Get_Suffix";
        public const string Get_MatrixItems = "Get_MatrixItems";

        public const string Get_EcommerceCustomerProductNotificationSubscription = "Get_EcommerceCustomerProductNotificationSubscription";

        public const string Read_InventoryItemWebOptionDescription = "Read_InventoryItemWebOptionDescription";

        public const string Delete_EcommerceCustomerCartRecord = "Delete_EcommerceCustomerCartRecord";

        public const string Delete_EcommerceCustomerRecords = "Delete_EcommerceCustomerRecords";

        public const string Read_ItemReservation = "Read_ItemReservation";

        public const string Update_AllocatedQuantity = "Update_AllocatedQuantity";

        public const string Get_IsIgnoreStockLevels = "Get_IsIgnoreStockLevels";

        public const string Read_CustomerInvoice = "Read_CustomerInvoice";
        public const string Read_CustomerInvoices = "Read_CustomerInvoices";
        public const string Read_CustomerInvoiceItems = "Read_CustomerInvoiceItems";
        public const string Read_CustomerRMA = "Read_CustomerRMA";
        public const string Read_CustomerRMAs = "Read_CustomerRMAs";
        public const string Read_CustomerRMAItems = "Read_CustomerRMAItems";

        public const string Get_ExpressKitQtyDropdown = "Get_ExpressKitQtyDropdown";
    }
}