﻿namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class CoreConstants
    {
        public const string DEFAULT_SQL_SCRIPT_CONFIG_NAME = "db.xml.config";
        public const string DEFAULT_SQL_SCRIPT_CONFIG_DIR = "DBConfig";
        public const string DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS = "DbQuery";
        public const string DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS_ID = "id";
        public const string DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS_TYPE = "type";
        public const string DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS_QUERY = "qry";

        public const string DEFAULT_SQL_SCRIPT_CONFIG_CACHE_NAME = "DEFAULT_SQL_SCRIPT_CONFIG_CACHE_NAME";
    }
}
