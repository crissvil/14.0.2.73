﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Xml;
using System.Xml.Linq;
using InterpriseSuiteEcommerceCommon.Domain.Exceptions;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class SqlQueryConfig
    { 
        public string ID { get; set; }
        public string ScriptValue { get; set; }
        public string Type { get; set; }
    }

    public class SqlQueryConfigs : IEnumerable<SqlQueryConfig>
    {
        private List<SqlQueryConfig> _xmlQueries;

        public SqlQueryConfigs()
        {
            _xmlQueries = new List<SqlQueryConfig>();
        }

        public SqlQueryConfig this[string id]
        {
            get 
            {
                return _xmlQueries.FirstOrDefault(qry => qry.ID == id);
            }
        }

        public SqlQueryConfig this[int index]
        {
            get
            {
                return _xmlQueries[index];
            }
        }

        public void AddRange(IEnumerable<SqlQueryConfig> configs)
        {
            _xmlQueries.AddRange(configs);
        }

        public void Add(SqlQueryConfig config)
        {
            _xmlQueries.Add(config);
        }

        public static SqlQueryConfigs LoadSqlScriptConfigs(string path)
        {
            var sqlQueryConfigs = new SqlQueryConfigs();

            if (!File.Exists(path))
            {
                throw new FileNotFoundException("Unable to load DBConfig queries, File Not Found - " + path);
            }

            XDocument doc = null;
            try
            {
                doc = XmlHelper.ToXDocument(path);
            }
            catch (XmlException)
            {
                throw new XmlException("Unable to load DBConfig queries, the file - " + path + 
                                    " is an invalid xml file or the file was unable to be parsed as xml.");
            }

            try
            {
                var descendants = XmlHelper.GetDescendants(doc, CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS);
                if (descendants != null && descendants.Count() > 0)
                {
                    var configs = descendants
                                        .Where(d => !d.Attribute(CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS_ID).Value.IsNullOrEmptyTrimmed() &&
                                                    (!d.Value.IsNullOrEmptyTrimmed() || !d.Attribute(CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS_QUERY).Value.IsNullOrEmptyTrimmed()))
                                        .Select(d => new SqlQueryConfig()
                                        {
                                            ID = d.Attribute(CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS_ID).Value.Trim(),
                                            Type = (d.Attribute(CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS_TYPE) != null) ?
                                                        d.Attribute(CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS_TYPE).Value.Trim().ToLower() :
                                                        String.Empty,
                                            ScriptValue = (d.Value.IsNullOrEmptyTrimmed()) ?
                                                                d.Attribute(CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS_QUERY).Value.Trim() :
                                                                d.Value.Trim()
                                        })
                                        .AsParallel();

                    var grp = configs.GroupBy(g => g.ID)
                                     .Where(g => g.Count() > 1)
                                     .Select(g => g.Key)
                                     .ToArray();

                    if (grp.Count() > 0)
                    {
                        string duplicateIds = String.Join(" , ", grp);
                        throw new DBQueryDuplicateIDException("Unable to load DBConfig queries, one or more entries have the same id. List of ID's: " + duplicateIds);
                    }

                    sqlQueryConfigs.AddRange(configs);
                }
            }
            catch (Exception)
            {
                throw new XmlException("Unable to load DBConfig queries, the file - " + path +
                                        " may have invalid dbquery entry or values.");
            }
            
            return sqlQueryConfigs;
        }

        public static SqlQueryConfig Find(string id, string path)
        {
            SqlQueryConfig configDemanded = null;

            var doc = XmlHelper.ToXDocument(path);
            var descendants = XmlHelper.GetDescendants(doc, CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS);

            if (descendants != null && descendants.Count() > 0)
            {
                var sqlCriptNode = descendants.FirstOrDefault(c => c.Attribute(CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS_ID).Value.Trim() == id);
                if (sqlCriptNode != null)
                {
                    configDemanded = new SqlQueryConfig()
                    {
                        ID = id,
                        ScriptValue = sqlCriptNode.Value.Trim(),
                        Type = sqlCriptNode.Attribute(CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_DESCENDANTS_TYPE).Value.Trim().ToLower()
                    };
                }
            }

            return configDemanded;
        }

        public IEnumerator<SqlQueryConfig> GetEnumerator()
        {
            return _xmlQueries.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return _xmlQueries.GetEnumerator();
        }
    }
}
