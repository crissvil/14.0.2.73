﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Tool;
using InterpriseSuiteEcommerceCommon.DTO;
using System.Drawing;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public static class ImageSynchronizer
    {
        private static ImageType CurrentImageType = ImageType.Item;
        private static string OtherPath = String.Empty;

        public static int ImagePerBatch = 30;

        public static string SynchronizeImages(CustomFileUploadJson status, ImageSyncType syncType)
        {
            var totalImages = (syncType == ImageSyncType.Mobile) ? GetAllImagesToUploadForMobile() : GetAllImagesToUploadForMinicart();
            int total = totalImages.Count();

            status.TotalImages = total;
            if (status.CurrentImageRow != total)
            {
                var nextImagesToUploadPerBatch = totalImages.Where(f => f.Index > status.CurrentImageRow)
                                                            .Take(ImagePerBatch);

                //Start Uploading
                SyncImages(nextImagesToUploadPerBatch, syncType);

                status.CurrentImageRow = nextImagesToUploadPerBatch.Last().Index;
            }

            string uploadedDataStatus = ServiceFactory.GetInstance<ICryptographyService>()
                                                      .SerializeToJson<CustomFileUploadJson>(status);

            //if equal the dispose the object to the cache
            if (status.CurrentImageRow == total)
            {
                DisposeFromMemory();
            }

            return uploadedDataStatus;
        }

        private static void DisposeFromMemory()
        {
            var appCachingService = ServiceFactory.GetInstance<ApplicationCachingEngine>();
            if (appCachingService.Exist(DomainConstants.MOBILE_IMAGE_CACHE_NAME))
            {
                appCachingService.RemoveItem(DomainConstants.MOBILE_IMAGE_CACHE_NAME);
            }
        }

        private static string GetEntityDirectory(ImageType entityType)
        {
            string entityDirectory = string.Empty;
            var ioService = ServiceFactory.GetInstance<IIOService>();

            switch (entityType)
            {
                case ImageType.Item:
                    entityDirectory = ioService.GetProductImageDirectory();
                    break;
                case ImageType.Category:
                    entityDirectory = ioService.GetCategoryImageDirectory();
                    break;
                case ImageType.Department:
                    entityDirectory = ioService.GetDepartmentImageDirectory();
                    break;
                case ImageType.Manufacturer:
                    entityDirectory = ioService.GetManufacturerImageDirectory();
                    break;
                case ImageType.Attribute:
                    entityDirectory = ioService.GetAttributeImageDirectory();
                    break;
            }

            return entityDirectory;
        }

        private static IEnumerable<CustomFileDTO> GetAllImagesToUploadForMobile()
        {
            var lst = new CustomFileDTO[0];
            string otherPath = "mobile";
            var appCachingService = ServiceFactory.GetInstance<ApplicationCachingEngine>();
            if (appCachingService.Exist(DomainConstants.MOBILE_IMAGE_CACHE_NAME))
            {
                lst = appCachingService.GetItem<CustomFileDTO[]>(DomainConstants.MOBILE_IMAGE_CACHE_NAME);
            }
            else
            {
                ImageType imageType = ImageType.Item;
                string entityDirectory = GetEntityDirectory(imageType);
                var products = GetImagesByPath(entityDirectory, otherPath, imageType);

                imageType = ImageType.Attribute;
                entityDirectory = GetEntityDirectory(imageType);
                var attributes = GetImagesByPath(entityDirectory, otherPath, imageType);

                imageType = ImageType.Category;
                entityDirectory = GetEntityDirectory(imageType);
                var categories = GetImagesByPath(entityDirectory, otherPath, imageType);

                imageType = ImageType.Department;
                entityDirectory = GetEntityDirectory(imageType);
                var departments = GetImagesByPath(entityDirectory, otherPath, imageType);

                imageType = ImageType.Manufacturer;
                entityDirectory = GetEntityDirectory(imageType);
                var manufacturer = GetImagesByPath(entityDirectory, otherPath, imageType);

                lst = products.Union(attributes)
                              .Union(categories)
                              .Union(departments)
                              .Union(manufacturer)
                              .AsQueryable()
                              .Select(ReIndexFiles)
                              .ToArray();

                appCachingService.AddItem(DomainConstants.MOBILE_IMAGE_CACHE_NAME, lst);
            }

            return lst;
        }

        private static IEnumerable<CustomFileDTO> GetAllImagesToUploadForMinicart()
        {
            var lst = new CustomFileDTO[0];
            string otherPath = "minicart";
            var appCachingService = ServiceFactory.GetInstance<ApplicationCachingEngine>();
            if (appCachingService.Exist(DomainConstants.MOBILE_IMAGE_CACHE_NAME))
            {
                lst = appCachingService.GetItem<CustomFileDTO[]>(DomainConstants.MINICART_IMAGE_CACHE_NAME);
            }
            else
            {
                ImageType imageType = ImageType.Item;
                string entityDirectory = GetEntityDirectory(imageType);
                var products = GetImagesByPath(entityDirectory, otherPath, imageType);

                lst = products.AsQueryable()
                                   .Select(ReIndexFiles)
                                   .ToArray();

                appCachingService.AddItem(DomainConstants.MINICART_IMAGE_CACHE_NAME, lst);
            }

            return lst;
        }

        private static void SyncImages(IEnumerable<CustomFileDTO> batchToUpload, ImageSyncType syncType)
        {
            foreach (var item in batchToUpload)
            {
                try
                {
                    using (var img = Image.FromFile(item.FullPath))
                    {
                        string extension = item.Ext;
                        if (extension.Contains(".")) { extension = extension.Remove(0, 1); }

                        string imgEntityType = (item.ImageType.ToString() == ImageType.Item.ToString() ? "product" : item.ImageType.ToString().ToLower());

                        switch(syncType)
                        {
                            case ImageSyncType.Mobile:

                                ServiceFactory.GetInstance<IIMageService>()
                                              .MakeMobilePicture(imgEntityType, item.FileNameWithoutExt, img, extension, item.OtherPath);

                                break;
                            case ImageSyncType.Minicart:

                                ServiceFactory.GetInstance<IIMageService>()
                                              .MakeMinicartPicture(imgEntityType, item.FileNameWithoutExt, img, extension);

                                break;
                        }

                        img.Dispose();
                    }
                }
                catch
                {   
                    //Continue uploading the next image in the batch
                    continue;
                }
            }
        }

        private static IEnumerable<CustomFileDTO> GetImagesByPath(string directory, string otherPath, ImageType imageType)
        {
            //Used this instead of looping again to the list of files
            CurrentImageType = imageType;
            OtherPath = otherPath;

            string largeDir = directory + "\\large";
            var largeList = Directory.GetFiles(largeDir)
                                         .AsQueryable()
                                         .Select(SelectFiles)
                                         .Where(IsValidImageFileExtenstions)
                                         .Where(IsNotSpacerImage)
                                         .AsParallel()
                                         .ToList();

            string mediumDir = directory + "\\medium";
            var mediumList = Directory.GetFiles(mediumDir)
                                         .AsQueryable()
                                         .Select(SelectFiles)
                                         .Where(IsValidImageFileExtenstions)
                                         .Where(IsNotSpacerImage)
                                         .AsParallel()
                                         .ToList();

            string iconDir = directory + "\\icon";
            var iconList = Directory.GetFiles(iconDir)
                                         .AsQueryable()
                                         .Select(SelectFiles)
                                         .Where(IsValidImageFileExtenstions)
                                         .Where(IsNotSpacerImage)
                                         .AsParallel()
                                         .ToList();

            var swatchList = new List<CustomFileDTO>();
            var microList = new List<CustomFileDTO>();
            if (imageType == ImageType.Item && otherPath == "mobile")
            {
                string swatch = directory + "\\swatch";
                OtherPath = "swatch";
                swatchList = Directory.GetFiles(swatch)
                                             .AsQueryable()
                                             .Select(SelectFiles)
                                             .Where(IsValidImageFileExtenstions)
                                             .Where(IsNotSpacerImage)
                                             .ToList();

                string microdirectory = directory + "\\micro";
                OtherPath = "micro";
                microList = Directory.GetFiles(microdirectory)
                                             .AsQueryable()
                                             .Select(SelectFiles)
                                             .Where(IsValidImageFileExtenstions)
                                             .Where(IsNotSpacerImage)
                                             .ToList();
            }

            return largeList.Union(mediumList, new CustomUnionComparer())
                            .Union(iconList, new CustomUnionComparer())
                            .Union(swatchList)
                            .Union(microList);
        }

        private static int Incrementor(int index)
        {
            index = index + 1;
            return index;
        }

        private static Expression<Func<CustomFileDTO, bool>> IsValidImageFileExtenstions = f => f.Ext == ".jpg" || f.Ext == ".gif" || f.Ext == ".png";

        private static Expression<Func<CustomFileDTO, bool>> IsNotSpacerImage = f => f.FileName != "spacer" && f.Ext != ".gif";

        private static Expression<Func<string, CustomFileDTO>> SelectFiles = (f) =>
                                new CustomFileDTO
                                {
                                    FullPath = f,
                                    FileName = f.Substring(f.LastIndexOf("\\") + 1),
                                    Ext = f.Substring(f.LastIndexOf(".")),
                                    ImageType = CurrentImageType,
                                    OtherPath = OtherPath,
                                };

        private static Expression<Func<CustomFileDTO, int, CustomFileDTO>> ReIndexFiles = (f, index) =>
                                new CustomFileDTO
                                {
                                    FullPath = f.FullPath,
                                    FileName = f.FileName,
                                    Ext = f.Ext,
                                    ImageType = f.ImageType,
                                    OtherPath = f.OtherPath,
                                    Index = Incrementor(index)
                                };
    }
}