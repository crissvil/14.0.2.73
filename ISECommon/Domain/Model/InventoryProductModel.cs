﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class InventoryProductModel
    {
        public int Counter { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ItemType { get; set; }
        public string ItemDescription { get; set; }
        
        public string ItemURL { get; set; }
        public IEnumerable<InventoryCategoryModel> Categories { get; set; }

        public bool IsDropShip { get; set; }
        public bool IsSpecialOrder { get; set; }
        public bool IsCBN { get; set; }
        public int CBNItemID { get; set; }
    }
}
