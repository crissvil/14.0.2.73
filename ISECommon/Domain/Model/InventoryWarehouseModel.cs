﻿namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class InventoryWarehouseModel
    {
        public string WareHouseCode { get; set; }
        public string WareHouseDescription { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
    }
}
