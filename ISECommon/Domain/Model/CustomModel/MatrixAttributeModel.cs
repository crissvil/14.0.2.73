﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class MatrixAttributeModel
    {
        public string AttributeCode { get; set; }
        public string AttributeDescription { get; set; }
        public string AttributeValueCode { get; set; }
        public string AttributeValueDescription { get; set; }
    }
}
