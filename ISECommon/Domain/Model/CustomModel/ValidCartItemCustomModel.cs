﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class ValidCartItemCustomModel
    {
        public string ItemCode { get; set; }
        public string UnitMeasureCode { get; set; }
    }
}
