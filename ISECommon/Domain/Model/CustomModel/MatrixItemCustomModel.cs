﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class MatrixItemCustomModel
    {
        public int Counter { get; set; }
        public string MatrixItemCode { get; set; }
        public string MatrixItemDescription { get; set; }
        public string MatrixItemName { get; set; }
        public string WebDescription { get; set; }
        public string ZoomOption { get; set; }
        public bool HidePriceUntilCart { get; set; }
        public int ItemCount { get; set; }

        public bool ShowBuyButton { get; set; }
        public bool RequiresRegistration { get; set; }

        public bool IsCallToOrder { get; set; }


    }
}
