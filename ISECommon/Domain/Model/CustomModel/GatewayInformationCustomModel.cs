﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class GatewayInformationCustomModel
    {
        public string Gateway { get; set; }
        public string GatewayAssemblyName { get; set; }
    }
}
