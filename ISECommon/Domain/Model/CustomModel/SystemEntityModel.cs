﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class SystemEntityModel
    {
        public string EntityCode { get; set; }
        public string VirtualType { get; set; }
        public string VirtualPageOption { get; set; }
        public string VirtualPageValueEntity { get; set; }
        public int NewEntityId { get; set; }
        public string VirtualPageValueTopic { get; set; }
        public string VirtualPageValueExternalPage { get; set; }
        public bool OpenInNewTab { get; set; }
    }
}
