﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class CustomerRMACustomModel
    {
        public string RMACode { get; set; }
        
        public string RMAStatus { get; set; }
        public DateTime RMADate { get; set; }
        public string InvoiceCode { get; set; }
        public string SalesOrderCode { get; set; }
        public DateTime SalesOrderDate { get; set; }
        public decimal TotalRate { get; set; }
        public string TotalRateFormatted { get; set; }
        public decimal TotalQuantityReturn { get; set; }
        public string Notes { get; set; }
    }
}
