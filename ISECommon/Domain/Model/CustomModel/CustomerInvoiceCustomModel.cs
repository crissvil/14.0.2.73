﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class CustomerInvoiceCustomModel
    {
        public string InvoiceCode { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string SalesOrderCode { get; set; }
        public DateTime SalesOrderDate { get; set; }
        public decimal TotalRate { get; set; }
        public string TotalRateFormatted { get; set; }
        public string CurrencyCode { get; set; }
        public bool IsPosted { get; set; }
        public bool IsShipped { get; set; }
    }
}
