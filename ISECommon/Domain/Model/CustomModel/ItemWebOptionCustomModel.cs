﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class ItemWebOptionCustomModel
    {
        public int ItemCounter { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ZoomOption { get; set; }
        public bool ShowSaleBanner { get; set; }
        public bool IsDropShip { get; set; }
        public bool RequiresRegistration { get; set; }
        public bool ShowBuyButton { get; set; }
        public bool IsCallToOrder { get; set; }
        public bool HidePriceUntilCart { get; set; }
        public bool ShowPurchasedItemsFromSameCategory  { get; set; }
        public bool ShowViewedItemsFromSameCategory  { get; set; }
        public decimal MinOrderQuantity { get; set; }
        public List<decimal> RestrictedQuantities = new List<decimal>();
        public List<string> RestrictedQuantitiesText = new List<string>();
        public bool IsDontEarnPoints { get; set; }
        public string Dimension { get; set; }
        public string MaterialType { get; set; }
        public string PrintInfo { get; set; }
    }
}
