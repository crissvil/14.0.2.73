﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class ItemWebOptionDescCustomModel
    {
        public int Counter { get; set; }
        public string ItemName { get; set; }
        public string ItemDescription { get; set; }
        public string WebDescription { get; set; }
    }
}
