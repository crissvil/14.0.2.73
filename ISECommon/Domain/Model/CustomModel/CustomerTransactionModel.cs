﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class CustomerTransactionModel
    {
        public Guid ContactGUID { get; set; }

        public string CustomerCode { get; set; }
        public string CustomerName{get;set;}
        public string CurrencyCode { get; set; }
        public string Type { get; set; }      
        
        public decimal Outstanding { get; set; }
        public decimal Total { get; set; }

        public DateTime DocumentDate { get; set; }
        public DateTime DueDate { get; set; }

        public bool IsPosted { get; set; }
        public bool IsVoided { get; set; }
        public bool IsPaid { get; set; }
    }

}
