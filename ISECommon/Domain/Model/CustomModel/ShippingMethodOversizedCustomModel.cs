﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel
{
    public class ShippingMethodOversizedCustomModel
    {
        public string FreightChargeType { get; set; }
        public string ShippingMethodCode { get; set; }
    }
}
