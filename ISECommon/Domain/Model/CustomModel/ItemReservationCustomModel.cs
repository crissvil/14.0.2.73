﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class ItemReservationCustomModel
    {
        public DateTime DueDate { get; set; }
        public string ItemCode { get; set; }
        public string UnitMeasureCode { get; set; }
        public decimal UnitMeasureQty { get; set; }
        public decimal AllocatedQty { get; set; }
        public decimal ReservedQty { get; set; }
        public int CartRecordID { get; set; }
    }
}
