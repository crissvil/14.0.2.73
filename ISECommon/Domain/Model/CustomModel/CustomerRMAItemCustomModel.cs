﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class CustomerRMAItemCustomModel
    {
        public string ItemDescription { get; set; }
        public string UPCCode { get; set; }
        public string UnitMeasureCode { get; set; }
        public decimal QuantityOrdered { get; set; }
        public decimal QuantityShipped { get; set; }
    }
}
