﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class MatrixItemWebOptionDescCustomModel
    {
        public int Counter { get; set; }
        public string ItemCode { get; set; }
        public string Summary { get; set; }
        public string WebDescription { get; set; }
        public string Warranty { get; set; }
    }
}
