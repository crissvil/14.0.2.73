﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class CustomerCreditCustomModel
    {
        public string CreditCode { get; set; }
        public string Type { get; set; }
        public decimal CreditAllocatedRate { get; set; }
        public decimal CreditReservedRate { get; set; }
        public decimal CreditAmountRate { get; set; }
        public decimal CreditAvailableRate { get; set; }
        public decimal CreditRemainingBalance { get; set; }
        public decimal CreditAppliedInShoppingCart { get; set; }
        public string CreditRemainingBalanceFormatted { get; set; }
    }

    public class CustomerCreditAppliedModel
    {
        public string CreditCode { get; set; }
        public decimal CreditAppliedInShoppingCart { get; set; }
    }
}
