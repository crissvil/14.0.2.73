﻿using InterpriseSuiteEcommerceCommon.DTO;
using System;

namespace InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel
{
    public class StorePickupInventoryWarehouseCustomModel
    {
        public long RowNumber { get; set; }
        public string WareHouseCode { get; set; }
        public string WareHouseDescription { get; set; }
        public decimal FreeStock { get; set; }
        public string Telephone { get; set; }
        public string TelephoneLocalNumber { get; set; }
        public string TelephoneExtension { get; set; }
        public string Fax { get; set; }
        public string FaxLocalNumber { get; set; }
        public string FaxExtension { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public Coordinate Coordinate { get; set; }
        public AddressModel Address { get; set; }
    }
}
