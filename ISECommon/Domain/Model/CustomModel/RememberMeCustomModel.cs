﻿namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class RememberMeCustomModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string Vector { get; set; }
        public string DecryptedPassword { get; set; }
    }
}
