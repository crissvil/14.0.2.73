﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class CustomerLoyaltyPointsCustomModel
    {
        public decimal AccumulatedPoints { get; set; }
        public decimal OutstandingPoints { get; set; }
        public decimal RedeemedPoints { get; set; }
        public decimal ReservedPoints { get; set; }

        public decimal RemainingPoints { get; set; }
        public string RemainingPointsFormatted { get; set; }
        public decimal MonetizedRemainingPoints { get; set; }
        public string MonetizedRemainingPointsFormatted { get; set; }
    }
}
