﻿namespace InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel
{
    public class RatingComputationCustomModel
    {
        public decimal Average { get; set; }
        public int Count { get; set; }
        public int Total { get; set; }
        public decimal Great { get; set; }
        public decimal Good { get; set; }
        public decimal OK { get; set; }
        public decimal BAD { get; set; }
        public decimal TERRIBLE { get; set; }
    }
}
