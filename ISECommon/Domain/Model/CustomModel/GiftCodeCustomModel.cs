﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel
{
    public class GiftCodeCustomModel
    {
        public string Type { get; set; }
        public string BillToCode { get; set; }
        public string SerialCode { get; set; }
        public string CreditCode { get; set; }
        public string CreditAvailableFormatted { get; set; }
        public decimal CreditAvailable { get; set; }
        public decimal CreditAmountRate { get; set; }
        public decimal CreditReservedRate { get; set; }
        public decimal CreditAllocatedRate { get; set; }
        public decimal CreditAvailableRate { get; set; }
        public bool IsActivated { get; set; }
        public bool IsValid { get; set; }
        public bool IsOwned { get; set; }

        public decimal AmountApplied { get; set; }
    }

    public class GiftCodeAmountAppliedModel
    {
        public string SerialCode { get; set; }
        public decimal AmountApplied { get; set; }
    }
}
