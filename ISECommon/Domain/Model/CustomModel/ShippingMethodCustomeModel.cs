﻿using System;

namespace InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel
{
    public class ShippingMethodCustomeModel
    {
        #region Constructor

        public ShippingMethodCustomeModel(string code, string description)
        {
            Code = code;
            Description = description;
            RateID = Guid.Empty;
        }

        #endregion

        #region Properties

        public string Code { get; set; }

        public string Description { get; set; }

        public decimal Freight { get; set; }

        public string FreightDisplay { get; set; }

        public Guid RateID { get; set; }

        public string FreightCurrencyCode { get; set; }

        public bool IsError { get; set; }

        public bool IsDefault { get; set; }

        public string CarrierCode { get; set; }

        public string CarrierDescription { get; set; }

        public string PackagingType { get; set; }

        public string ServiceType { get; set; }

        public int FreightCalculation { get; set; }

        public int ChargeType { get; set; }

        public decimal MiscAmount { get; set; }

        public decimal ReturnedRate { get; set; }

        public bool ForOversizedItem { get; set; }

        public string OversizedItemName { get; set; }

        public int Length { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public decimal WeightThreshold { get; set; }

        public bool IsDummyShippingMethod { get; set; }

        public string FreightChargeType { get; set; }

        #endregion  
    }
}
