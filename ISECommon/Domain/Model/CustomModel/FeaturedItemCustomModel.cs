﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpriseSuiteEcommerceCommon.Domain.CustomModel
{
    public class FeaturedItemCustomModel
    {
        public int Counter { get; set; }

        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public string ItemName { get; set; }
        public string ItemType { get; set; }
        public string Summary { get; set; }

        public bool ShowBuyButton { get; set; }
        public bool HidePriceUntilCart { get; set; }

    }
}
