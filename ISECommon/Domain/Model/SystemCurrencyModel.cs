﻿using System.Collections.Generic;
namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class SystemCurrencyModel
    {
        public string CurrencySymbol { get; set; }
        public int CurrencyPositivePattern { get; set; }
        public int CurrencyNegativePattern { get; set; }
        public string CurrencyDecimalSeparator { get; set; }
        public int CurrencyDecimalDigits { get; set; }
        public string CurrencyGroupSeparator { get; set; }
        public IEnumerable<int> CurrencyGroupSizes { get; set; }
        public string NumberDecimalSeparator { get; set; }
        public string NumberGroupSeparator { get; set; }
        public int NumberDecimalDigits { get; set; }
    }
}
