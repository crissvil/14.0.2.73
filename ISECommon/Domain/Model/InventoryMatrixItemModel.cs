﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class InventoryMatrixItemModel
    {
        public int Counter { get; set; }
        public string ItemCode { get; set; }
        public string MatrixItemCode { get; set; }
    }
}
