﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class InventoryOverrideImageModel
    {
        public int ImageIndex { get; set; }
        public string FileName { get; set; }
        public string ItemCode { get; set; }
        public bool IsDefaultIcon { get; set; }
        public bool IsDefaultMedium { get; set; }
    }
}
