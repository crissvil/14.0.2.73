﻿using System;
namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class StoreHolidayHours
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public WorkingHour WorkingDay { get; set; }
    }
}
