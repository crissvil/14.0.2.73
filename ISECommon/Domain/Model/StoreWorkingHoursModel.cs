﻿using System;
namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class StoreWorkingHoursModel
    {
        public StoreWorkingHoursModel() {}

        public string Day { get; set; } 
        public WorkingHour WorkingHour { get; set; }
    }

    public class WorkingHour
    {
        public DateTime Opening { get; set; }
        public DateTime Closing { get; set; }
        public bool Close { get; set; }
    }
}
