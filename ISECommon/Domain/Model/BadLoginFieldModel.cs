﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class BadLoginFieldModel: IBadLoginField
    {
        public int BadLoginCount { get; set; }
        public DateTime LastBadLoginDate { get; set; }
        public DateTime LockedInUntilDate { get; set; }
    }
}
