﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class SelectedKitComponentModel
    {
        public string ItemCode { get; set; }
        public decimal Quantity { get; set; }
        public string UnitMeasureCode { get; set; }
        public decimal TotalRate { get; set; }
    }
}
