﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class EcommerceShoppingCartModel
    {
        public int ShoppingCartRecId { get; set; }
        public Guid ShoppingCartRecGuid { get; set; }
        public string ItemCode { get; set; }
        public decimal Quantity { get; set; }
        public int RequiresCount { get; set; }
        public CartTypeEnum CartType { get; set; }
    }
}
