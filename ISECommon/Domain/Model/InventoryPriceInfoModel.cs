﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class InventoryPriceInfoModel
    {
        public string ItemCode { get; set; }
        public string UMCode { get; set; }
        public bool IsByTotalQty {get; set;}
        public decimal SalesPrice {get; set;}
        public bool IsSalePriceInBaseCurrency {get; set;}
        public decimal RegularPrice { get; set; }
        public decimal PromotionalPrice { get; set; }
        public string Pricing { get; set; }
        public decimal Percent { get; set; }
        public decimal Discount{ get; set; }
        public decimal CategoryDiscount { get; set; }
        public string CustomerItemCode { get; set; }
        public string CustomerItemDescription { get; set; }
        public decimal BasePricingCost { get; set; }
        public decimal BaseAverageCost { get; set; }
        public bool IsInventorySpecialPriceExpired { get; set; }
        public bool IsCustomerSpecialPriceExpired { get; set; }
        public bool IsDeductPotentialDiscount { get; set; }
        public decimal PotentialDiscount { get; set; }
        public string ItemType { get; set; }      
    }
}
