﻿namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class RatingModel
    {
        public int ID { get; set; }
        public string Comment { get; set; }
        public int Rating { get; set; }
        public bool HasComment { get; set; }
        public bool IsFilthy { get; set; }
        public bool IsROTD { get; set; }
        public string MLID { get; set; }
        public int FoundHelpful { get; set; }
        public int FoundNotHelpful { get; set; }
        public System.DateTime Created { get; set; }

        public string CustomerCode { get; set; }
        public string ContactCode { get; set; }
        public string WebsiteCode { get; set; }

        public string ItemDescription { get; set; }
        public string ItemCode { get; set; }
        public string ImageURL { get; set; }
    }
}
