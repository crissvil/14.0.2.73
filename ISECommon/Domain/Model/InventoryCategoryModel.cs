﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class InventoryCategoryModel
    {
        public string ItemCode { get; set; }
        public string CategoryCode { get; set; }
        public string Description { get; set; }
    }
}
