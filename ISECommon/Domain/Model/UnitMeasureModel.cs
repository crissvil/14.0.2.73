﻿namespace InterpriseSuiteEcommerceCommon.Domain.Model
{
    public class UnitMeasureModel
    {
        public string Code { get; set; }
        public decimal Quantity { get; set; }
        public decimal Length { get; set; }
        public decimal Width { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
        public bool IsBase { get; set; }
    }
}
