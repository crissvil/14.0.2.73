﻿using System;
using System.Web;
using System.Linq;
using System.Web.Security;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.Authentication;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using System.Collections.Generic;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class AuthenticationService : ServiceBase, IAuthenticationService
    {
        private readonly ICustomerRepository _customerRepository = null;
        private readonly IShoppingCartRepository _shoppingCartRepository = null;
        private readonly IFormsAuthenticationService _formsAuthenticationService = null;
        private readonly IAppConfigService _appConfigService = null;
        private readonly ICryptographyService _cryptoGraphyService = null;
        private readonly IDateTimeService _dateTimeService = null;
        private readonly IUserAccountRepository _userAccountRepository = null;
        private readonly INavigationService _navigationService = null;
        private readonly IHttpContextService _httpContextService = null;

        public AuthenticationService(ICustomerRepository customerRepository, 
                                     IShoppingCartRepository shoppingCartRepository,
                                     IFormsAuthenticationService formsAuthenticationService,
                                     IAppConfigService appConfigService,
                                     ICryptographyService cryptoGraphyService,
                                     IDateTimeService dateTimeService,
                                     IUserAccountRepository userAccountRepository,
                                     INavigationService navigationService,
                                     IHttpContextService httpContextService
            )
        {
            _customerRepository = customerRepository;
            _shoppingCartRepository = shoppingCartRepository;
            _formsAuthenticationService = formsAuthenticationService;
            _appConfigService = appConfigService;
            _cryptoGraphyService = cryptoGraphyService;
            _dateTimeService = dateTimeService;
            _userAccountRepository = userAccountRepository;
            _navigationService = navigationService;
            _httpContextService = httpContextService;
        }

        public void AutoLoginFromCookie()
        {
            if (HttpContext.Current == null) 
            { 
                throw new HttpException("Method: AutoLoginFromCookie: \n\n Message: Unable to login, HttpContext.Current is not initialized");
            }

            var currentContext = HttpContext.Current;
            var authcookie = FormsAuthentication.FormsCookieName.ParseCookieRequest();

            Customer thisCustomer = null;
            if (authcookie == null)
            {
                thisCustomer = Customer.MakeAnonymous();
                thisCustomer.RequireCustomerRecord();
            }
            else
            {
                try
                {
                    var ticket = FormsAuthentication.Decrypt(authcookie.Value);
                    if (!ticket.Name.IsNullOrEmptyTrimmed() || ticket.Name.IsValidGuid())
                    {
                        var contactGuid = new Guid(ticket.Name);
                        thisCustomer = Customer.Find(contactGuid);

                        if (thisCustomer == null)
                        {
                            thisCustomer = Customer.MakeAnonymous();
                        }

                        thisCustomer.RequireCustomerRecord();
                        FormsAuthentication.SetAuthCookie(thisCustomer.ContactGUID.ToString(), true);

                        var cookie = FormsAuthentication.FormsCookieName.ParseCookieResponse();
                        cookie.Expires = DateTime.Now.Add(new TimeSpan(1, 0, 0, 0));
                    }
                    else
                    {
                        FormsAuthentication.SignOut();
                        thisCustomer = Customer.MakeAnonymous();
                    }
                }
                catch (Exception)
                {
                    FormsAuthentication.SignOut();
                    currentContext.User = new InterpriseSuiteEcommercePrincipal(Customer.MakeAnonymous());
                    throw;
                }
            }

            currentContext.User = new InterpriseSuiteEcommercePrincipal(thisCustomer);
        }

        public Customer GetCurrentLoggedInCustomer()
        {
            if (HttpContext.Current == null) { throw new HttpException("Method: GetCurrentLoggedInCustomer: \n\n Message: Unable to get current customer, HttpContext.Current is not initialized"); }

            var principal = (HttpContext.Current.User as InterpriseSuiteEcommercePrincipal);

            if (principal == null) return null;

            return principal.ThisCustomer;
        }

        public ISSIUserAccount GetCurrentLoggedInAdmin()
        {
            var cookie = _appConfigService.WebAdminAuthenticationName.ParseCookieRequest();
            if (cookie == null) { return null; }

            string userName = cookie.Value;
            var ticket = _formsAuthenticationService.Decrypt(userName);
            return _userAccountRepository.Find(ticket.Name, _appConfigService.WebSiteCode);
        }

        public bool IsAdminCurrentlyLoggedIn()
        {
            return (GetCurrentLoggedInAdmin() != null);
        }

        public string GetCurrentUserIPAddress() 
        {
            // check request headers for client IP Address
            string ipAddr = "HTTP_X_FORWARDED_FOR".ToServerVariables().Trim(); // ssl cluster

            bool isIpNull = ipAddr.IsNullOrEmptyTrimmed();
            bool isIpUnknown = ipAddr.Equals("unknown", StringComparison.OrdinalIgnoreCase);

            if (isIpNull || isIpUnknown) ipAddr = "HTTP_X_CLUSTER_CLIENT_IP".ToServerVariables().Trim(); // non-ssl cluster
            if (isIpNull || isIpUnknown) ipAddr = "HTTP_CLIENT_IP".ToServerVariables().Trim(); // proxy
            if (isIpNull || isIpUnknown) ipAddr = "REMOTE_ADDR".ToServerVariables().Trim(); // non-cluster

            // proxies can return a comma-separated list, so use the left-most (furthest downstream) one
            return ipAddr.Split(',')[0].Trim();
        }

        public LoginStatus Login(string email, string password, bool rememberMe)
        {
            var foundCustomer = _customerRepository.FindByEmailAndPassword(email, password, _appConfigService.WebSiteCode);

            DateTime currentDate = _dateTimeService.GetCurrentDate();

            if (foundCustomer == null) 
            {
                return new LoginStatus(false, false);
            }

            bool validateSubscription = _customerRepository.ValidateContactSubscription(foundCustomer.ContactCode, currentDate, currentDate, _appConfigService.WebSiteCode);

            if (!validateSubscription)
            {
                return new LoginStatus(false, true);
            }

            var rememberMeCookie = new HttpCookie(DomainConstants.REMEMBERME_COOKIE_NAME);
            _httpContextService.CurrentResponse.Cookies.Remove(DomainConstants.REMEMBERME_COOKIE_NAME);

            //check if remember me
            if (rememberMe)
            {
                rememberMeCookie.Value = foundCustomer.ContactGUID.ToString();
                rememberMeCookie.Expires = currentDate.AddDays(30);
            }
            else
            {
                rememberMeCookie.Expires = currentDate.AddDays(-1);
            }

            _httpContextService.CurrentResponse.Cookies.Add(rememberMeCookie);

            var oldCustomer = GetCurrentLoggedInCustomer();
            _customerRepository.ClearCustomerSession(oldCustomer.ContactGUID, currentDate);

            foundCustomer.FullModeInMobile = oldCustomer.FullModeInMobile; //save the last record of fullmode
            oldCustomer.ThisCustomerSession["ContactID"] = foundCustomer.ContactGUID.ToString();

            //Customization
            oldCustomer.Type = foundCustomer.Type;
            if (foundCustomer.EMail != email)
            {
                foundCustomer.ThisCustomerSession["AlternateEmail"] = email;
            }
            else
            {
                foundCustomer.ThisCustomerSession.ClearVal("AlternateEmail");
            }

            ExecuteSignInLogic(foundCustomer, oldCustomer.IsNotRegistered);

            _formsAuthenticationService.SaveAuthenticationCookie(foundCustomer.ContactGUID, rememberMe);
            _customerRepository.CreateContactSiteLog(foundCustomer.ContactCode, "Login", currentDate, _appConfigService.WebSiteCode, _appConfigService.UserCode);

            return new LoginStatus(true, false);
        }

        public Customer FindByEmailAndPassword(string email, string password)
        {
            return _customerRepository
                    .FindByEmailAndPassword(email, password, _appConfigService.WebSiteCode);
        }

        public void ExecuteSignInLogic(Customer foundCustomer, bool isCheckoutAnon)
        {
            var oldCustomer = GetCurrentLoggedInCustomer();
            decimal totalCartItems = _shoppingCartRepository.GetTotalCartItemsByCustomer(
                                        oldCustomer.CustomerCode, CartTypeEnum.ShoppingCart, oldCustomer.ContactCode, _appConfigService.WebSiteCode);


            //if true, the customer's shopping cart is cleared every time they log into the site. 
            //This can prevent 'old' stuff from being left in the cart from prior site visits. 
            //We tend to like to preserve the cart...so they can still order what they added even a month ago or more.
            if (_appConfigService.ClearOldCartOnSignin)
            {
                _shoppingCartRepository.ClearCart(CartTypeEnum.ShoppingCart, foundCustomer.CustomerID.ToString(), foundCustomer.ContactCode);
            }

            //This setting controls what happens to cart items when an 'anon' customer (someone who is NOT logged in) logs in. 
            //If true, their 'anon' cart is preserved and migrated to their 'logged in' cart. If false, their 'anon' cart is cleared upon signin. 
            //Best setting is usually TRUE,so if they are using the site not logged in, add items to their cart, and then log-in, those items are still in their cart.
            if (totalCartItems > 0 && !_appConfigService.PreserveActiveCartOnSignin)
            {
                _shoppingCartRepository.ClearCart(CartTypeEnum.ShoppingCart, oldCustomer.CustomerID.ToString(), oldCustomer.ContactCode);
            }

            // Copy anon cart items to login customer cart
            if (totalCartItems > 0 && _appConfigService.PreserveActiveCartOnSignin && !oldCustomer.CustomerID.IsNullOrEmptyTrimmed())
            {
                string shippingAddress = String.Empty;
                if (isCheckoutAnon)
                {
                    shippingAddress = foundCustomer.PrimaryShippingAddressID;
                }
                _shoppingCartRepository.UpdateAnonymousCart(CartTypeEnum.ShoppingCart, foundCustomer.CustomerCode, oldCustomer.CustomerCode, shippingAddress, foundCustomer.ContactCode);
            }
            
        }

        public void TryLoginTicketWhenNotExistCreateAnon()
        {
            if (HttpContext.Current == null)
            {
                throw new HttpException("Method: TryLoginTicketWhenNotExistCreateAnon: \n\n Message: Unable to login, HttpContext.Current is not initialized");
            }

            Customer customer = null;
            var authcookie = _formsAuthenticationService.FormsCookieName.ParseCookieRequest();
            if (authcookie == null)
            {
                customer = _customerRepository.Find(Guid.Empty, _appConfigService.WebSiteCode,
                                                                _appConfigService.AllowCreditHold,
                                                                _appConfigService.AllowProductFiltering);
                customer.RequireCustomerRecord();
                HttpContext.Current.User = new InterpriseSuiteEcommercePrincipal(customer);
            }
            else 
            {
                string guidValue = authcookie.Value;
                try
                {
                    var ticket = _formsAuthenticationService.Decrypt(guidValue);
                    if (!ticket.Name.IsNullOrEmptyTrimmed() || !CommonLogic.IsValidGuid(ticket.Name))
                    {
                        customer = _customerRepository.Find(new Guid(ticket.Name), _appConfigService.WebSiteCode,
                                                                                   _appConfigService.AllowCreditHold,
                                                                                   _appConfigService.AllowProductFiltering);

                        if (customer == null)
                        {
                            customer = _customerRepository.Find(Guid.Empty, _appConfigService.WebSiteCode,
                                                                            _appConfigService.AllowCreditHold,
                                                                            _appConfigService.AllowProductFiltering);
                        }
                        customer.RequireCustomerRecord();
                    }
                    else
                    {
                        _formsAuthenticationService.SignOut();
                        customer = _customerRepository.Find(Guid.Empty, _appConfigService.WebSiteCode,
                                                                        _appConfigService.AllowCreditHold,
                                                                        _appConfigService.AllowProductFiltering);
                    }
                    HttpContext.Current.User = new InterpriseSuiteEcommercePrincipal(customer);
                }
                catch (Exception)
                {
                    _formsAuthenticationService.SignOut();
                    customer = _customerRepository.Find(Guid.Empty, _appConfigService.WebSiteCode,
                                                                    _appConfigService.AllowCreditHold,
                                                                    _appConfigService.AllowProductFiltering);
                    HttpContext.Current.User = new InterpriseSuiteEcommercePrincipal(customer);
                }
            }
            
        }

        public void SignOut()
        {
            _customerRepository.CreateContactSiteLog(GetCurrentLoggedInCustomer().ContactCode, "Logout",
                                                      _dateTimeService.GetCurrentDate(),
                                                      _appConfigService.WebSiteCode,
                                                      _appConfigService.UserCode);

            _formsAuthenticationService.SignOut();
        }

        /// <summary>
        /// Replace the InterpriseSuiteEcommerceCommon.Security.AuthenticateWebService
        /// </summary>
        public void AuthenticateWebService()
        {
            string message = DomainConstants.SERVICE_TOKEN_FAILED;
            string token = HttpContext.Current.Request.Headers[DomainConstants.SERVICE_TOKEN];
            if (token.IsNullOrEmptyTrimmed()) throw new Exception(message);

            var customer = GetCurrentLoggedInCustomer();
            try
            {
                string code = _cryptoGraphyService.GetMD5Hash(customer.CustomerCode);
                if (token != code) throw new Exception(message);
            }
            catch (Exception)
            {
                throw new Exception(message);
            }
        }

        public void AuthenticateAdmin()
        {
            if (!IsAdminCurrentlyLoggedIn())
            {
                _navigationService.NavigateToWebAdminLoginPage();
            }
        }

        public AdminLoginStatus TryLoginAdmin(string userName, string password)
        {
            var status = new AdminLoginStatus()
            {
                IsLocked = false,
                IsSuccess = false
            };

            if (userName.IsNullOrEmptyTrimmed() && 
                password.IsNullOrEmptyTrimmed()) 
            { 
                return status;
            }

            if (userName.ToLowerInvariant().Trim() == "webadmin")
            {
                return status;
            }

            var account = _userAccountRepository.Find(userName, _appConfigService.WebSiteCode);

            if (account == null)
            {
                return status;
            }

            if (account.IsLocked)
            {
                status.IsLocked = true;
                return status;
            }

            bool isAuthenticated = _cryptoGraphyService.ValidatePassword(account.PasswordCypher, 
                                                                         password, 
                                                                         account.SaltBase64, 
                                                                         account.IvBase64);
            if(isAuthenticated)
            {
                _userAccountRepository.ClearSecurity(userName);

                DateTime currentDate = _dateTimeService.GetCurrentDate();
                var ticket = _formsAuthenticationService.CreateFormsAuthTicket(userName, currentDate, currentDate.AddMinutes(_appConfigService.WebAdminLoginTimeOut), false);
                _formsAuthenticationService.SaveCookie(_formsAuthenticationService.CreateAuthCookie(_appConfigService.WebAdminAuthenticationName, 
                                                                                                    _formsAuthenticationService.Encrypt(ticket),
                                                                                                    ticket.Expiration));
                status.IsLocked = false;
                status.IsSuccess = true;
            }
            else
            {
                var badLoginInfo = _userAccountRepository.ReloadBadLoginInfo(userName);
                if (!((badLoginInfo.BadLoginCount + 1) >= _appConfigService.MaxBadLogins))
                {
                    _userAccountRepository.IncrementBadLogin(userName, _dateTimeService.GetCurrentDate());
                    status.IsLocked = false;
                    status.IsSuccess = false;
                }
                else
                {
                    _userAccountRepository.ResetBadLoginCount(userName);

                    string lockTimeOut = Localization.DateTimeStringForDB(_dateTimeService.GetCurrentDate().AddMinutes(_appConfigService.BadLoginLockTimeOut));
                    _userAccountRepository.LockUserAccount(userName, lockTimeOut);

                    status.IsLocked = true;
                    status.IsSuccess = false;
                }
            }

            return status;
        }

        public void SignOutAdmin()
        {
            _formsAuthenticationService.SignOut(_appConfigService.WebAdminAuthenticationName);
            _navigationService.NavigateToWebAdminLoginPage();
        }

        public RememberMeCustomModel GetRememberMeInfo()
        {
            RememberMeCustomModel returnModel = null;
            var cookie = _httpContextService.CurrentRequest.Cookies[DomainConstants.REMEMBERME_COOKIE_NAME];
            if (cookie != null)
            {
                Guid? value = cookie.Value.TryParseGuid();
                if (value.HasValue)
                {
                    returnModel = _customerRepository.GetRememberMeInfo(value.Value, _appConfigService.WebSiteCode);
                    if (returnModel != null)
                    {
                        returnModel.DecryptedPassword = _cryptoGraphyService.InterpriseDecryption(returnModel.Password, returnModel.Salt, returnModel.Vector);
                    }
                }
            }
            return returnModel;
        }

        public string GetRedirectUrl(string name, bool persistentCookie)
        {
            return _formsAuthenticationService.GetRedirectUrl(name, persistentCookie);
        }

        public bool ValidateContactSubscription(string contactCode)
        {
            DateTime currentDate = _dateTimeService.GetCurrentDate();
            return _customerRepository.ValidateContactSubscription(contactCode, currentDate, currentDate, _appConfigService.WebSiteCode);
        }

        public void SecurityCheck()
        {
            var keys = new string[] { "<script>" };

            if (keys.Any(k => _httpContextService.CurrentRequest
                                                 .QueryString
                                                 .ToString()
                                                 .ToUrlDecode()
                                                 .ToLowerInvariant()
                                                 .Contains(k)))
            {
                throw new ArgumentException("SECURITY EXCEPTION");
            }
        }
        public Customer LoginFromSagePay(string contactGuid)
        {
            if (HttpContext.Current == null)
            {
                throw new HttpException("Method: LoginFromSagePay: \n\n Message: Unable to login, HttpContext.Current is not initialized");
            }

            var currentContext = HttpContext.Current;

            var thisCustomer = Customer.Find(new Guid(contactGuid));

            if (thisCustomer == null)
            {
                thisCustomer = Customer.MakeAnonymous();
            }

            thisCustomer.RequireCustomerRecord();
            FormsAuthentication.SetAuthCookie(thisCustomer.ContactGUID.ToString(), true);

            var cookie = FormsAuthentication.FormsCookieName.ParseCookieResponse();
            cookie.Expires = DateTime.Now.Add(new TimeSpan(1, 0, 0, 0));

            currentContext.User = new InterpriseSuiteEcommercePrincipal(thisCustomer);

            return thisCustomer;
        }

    }
}