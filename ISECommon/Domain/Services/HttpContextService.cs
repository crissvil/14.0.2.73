﻿using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System.Web;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class HttpContextService : ServiceBase, IHttpContextService
    {
        public string ApplicationPath
        {
            get
            {
                try 
	            {	        
		            return HttpContext.Current.Request.ApplicationPath;    
	            }
	            catch (HttpException)
	            {
                    return HttpRuntime.AppDomainAppVirtualPath;
	            }
            }
        }

        public HttpRequest CurrentRequest
        {
            get { return HttpContext.Current.Request; }
        }

        public HttpResponse CurrentResponse
        {
            get { return HttpContext.Current.Response; }
        }
    }
}