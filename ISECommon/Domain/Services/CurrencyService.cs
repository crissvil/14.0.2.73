﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class CurrencyService : ServiceBase, ICurrencyService
    {
        private readonly IAuthenticationService _authenticationService = null;
        private readonly IRequestCachingService _requestCachingService = null;

        public CurrencyService(IAuthenticationService authenticationService, IRequestCachingService requestCachingService)
        {
            _authenticationService = authenticationService;
            _requestCachingService = requestCachingService;
        }

        public string FormatCurrency(decimal amount)
        {
            return FormatCurrency(amount, _authenticationService.GetCurrentLoggedInCustomer().CurrencyCode);
        }

        public string FormatCurrency(decimal amount, string currencyCode)
        {
            var format = GetNumberFormatInfoByCurrencyCode(currencyCode);
            return amount.ToString("C", format);
        }

        public decimal RoundCurrencyByCustomer(decimal amount)
        {
            decimal returnValue = Decimal.Zero;
            var formatInfo = GetNumberFormatInfoByCurrencyCode(_authenticationService.GetCurrentLoggedInCustomer().CurrencyCode);
            decimal? rounded = amount.ToString("F", formatInfo).TryParseDecimal();
            if (rounded.HasValue) returnValue = rounded.Value;

            return returnValue;
        }

        public NumberFormatInfo GetNumberFormatInfoByCurrencyCode(string code)
        {
            var formatInfo = _requestCachingService.GetItem<NumberFormatInfo>(code);
            if (formatInfo == null)
            {
                formatInfo = Currency.GetCurrencyFormat(code);
                _requestCachingService.AddItem(code, formatInfo);
            }

            return formatInfo;
        }

        public string GetCurrencySymbol()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return Currency.GetSymbol(customer.CurrencyCode);
        }

        public string RawCurrencyByCustomer(decimal amount)
        {
            var formatInfo = GetNumberFormatInfoByCurrencyCode(_authenticationService.GetCurrentLoggedInCustomer().CurrencyCode);
            return amount.ToString("F{0}".FormatWith(formatInfo.CurrencyDecimalDigits), formatInfo);
        }

        public decimal RoundMonetaryByCustomer(decimal amount)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return InterpriseHelper.RoundMonetary(amount, customer.CurrencyCode);
        }
    }
}
