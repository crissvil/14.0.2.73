﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class RequestCachingService : ServiceBase, IRequestCachingService
    {
        //Return null if does not exist
        public object GetItem(string key)
        {
            if (HttpContext.Current == null) return null;
            return HttpContext.Current.Items[key];
        }

        //Return null if does not exist
        public T GetItem<T>(string key) where T : class
        {
            if (HttpContext.Current == null) return null;

            return HttpContext.Current.Items[key] as T;
        }

        public void SetItem(string key, object value, int defaultMinutes = 10)
        {
            if (HttpContext.Current == null) return;

            if (GetItem(key) != null)
            {
                HttpContext.Current.Items[key] = value;
            }
            else
            {
                AddItem(key, value, defaultMinutes);
            }
        }

        public object AddItem(string key, object value, int minutes)
        {
            if (HttpContext.Current == null) return null;

            HttpContext.Current.Items.Add(key, value);
            return null;
        }

        public object AddItem(string key, object value, System.DateTime expiration, CacheDependency cachedependency = null)
        {
            if (HttpContext.Current == null) return null;

            HttpContext.Current.Items.Add(key, value);
            return null;
        }

        public void AddItem(string key, object value, CacheDependency cachedependency = null)
        {
            if (HttpContext.Current == null) return;

            HttpContext.Current.Items.Add(key, value);
        }

        public int ItemsCount
        {
            get { return HttpContext.Current.Cache.Count; }
        }

        public object RemoveItem(string key)
        {
            if (HttpContext.Current == null) return null;

            HttpContext.Current.Items.Remove(key);
            return null;
        }

        public bool Exist(string key)
        {
            return GetItem(key) != null;
        }

        public static void Reset(string key)
        {
            if (HttpContext.Current == null) return;

            HttpContext.Current.Items.Remove(key);
        }

        public void Reset()
        {
            if (HttpContext.Current == null) return;

            var cache = HttpContext.Current.Items;
            var keys = new List<string>();
            keys.AddRange(cache.OfType<DictionaryEntry>()
                               .Select(e => e.Key.ToString())
                               .ToArray());
            keys.ForEach(k => { cache.Remove(k); });
        }

        public void PageNoCache()
        {
            HttpContext.Current.Response.CacheControl = "private";
            HttpContext.Current.Response.Expires = 0;
            HttpContext.Current.Response.AddHeader("pragma", "no-cache");
        }
    }
}
