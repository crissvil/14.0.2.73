﻿using System;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class ThirdPartyProcessingService : ServiceBase, IThirdPartyProcessingService
    {
        IAppConfigService _appConfigService = null;
        IAuthenticationService _authenticationService = null;
        ICryptographyService _cryptographyService = null;

        public ThirdPartyProcessingService(IAppConfigService appConfigService,
                                           IAuthenticationService authenticationService,
                                           ICryptographyService cryptographyService)
        {
            _appConfigService = appConfigService;
            _authenticationService = authenticationService;
            _cryptographyService = cryptographyService;
        }

        public MaxMindMinFraudDetail ParseMinFraudSoapObject(string acceptLanguage, string customerIP, string billingcity, string billingstate, string billingpostalCode, string billingcountry, string emailDomain, string bin, string binName, string binPhone,
                                                             string customerPhone, string licenseKey, string requestedType, string forwardedIP, string emailMD5, string usernameMD5, string passwordMD5, string shippingStreetAddress,
                                                             string shippingCity, string shippingState, string shippingPostalCode, string shippingCountry, string transactionID, string sessionID, string userAgent, string orderAmount,
                                                             string orderCurrency, string shopID, string avsResult, string cvvResult, string transactionType)
        {
            var maxMind = new MaxMindAPI.minfraudWebService();
            maxMind.Url = _appConfigService.MaxMindSOAPURL;

            var maxMindResponse = maxMind.minfraud_soap14(acceptLanguage,
                                        customerIP,
                                        billingcity,
                                        billingstate,
                                        billingpostalCode,
                                        billingcountry,
                                        emailDomain,
                                        bin,
                                        binName,
                                        binPhone,
                                        customerPhone,
                                        licenseKey,
                                        requestedType,
                                        forwardedIP,
                                        emailMD5,
                                        usernameMD5,
                                        passwordMD5,
                                        shippingStreetAddress,
                                        shippingCity,
                                        shippingState,
                                        shippingPostalCode,
                                        shippingCountry,
                                        transactionID,
                                        sessionID,
                                        userAgent,
                                        orderAmount,
                                        orderCurrency,
                                        shopID,
                                        avsResult,
                                        cvvResult,
                                        transactionType);

            string minfraudDetails = _cryptographyService.SerializeMaxMindResponse(maxMindResponse);
            minfraudDetails = minfraudDetails.Replace(" xmlns=\"http://www.maxmind.com/maxmind_soap/minfraud_soap\"", "");

            var minFraudDetail = new MaxMindMinFraudDetail();
            minFraudDetail.FraudDetails = minfraudDetails;
            minFraudDetail.RiskScore = Localization.ParseUSDecimal(maxMindResponse.riskScore);

            return minFraudDetail;
        }

        public MaxMindMinFraudDetail ParseMinFraudSoapObject(string acceptLanguage, string billingCity, string billingState, string billingPostalCode, string billingCountry, string emailDomain, string bin, string binName, string binPhone,
                                                             string customerPhone, string requestedType, string emailMD5, string usernameMD5, string passwordMD5, string shippingStreetAddress,
                                                             string shippingCity, string shippingState, string shippingPostalCode, string shippingCountry, string transactionID, string sessionID, string userAgent, string orderAmount,
                                                             string shopID, string avsResult, string cvvResult, string transactionType)
        {
            var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            string maxMindLicenseKey = _appConfigService.MaxMindLicenseKey;

            string customerIP = thisCustomer.LastIPAddress;
            string forwardedIP = "HTTP_X_FORWARDED_FOR".ToServerVariables();
            string requestIP = "REMOTE_ADDR".ToServerVariables();

            if (customerIP.Length == 0)
            {
                customerIP = "REMOTE_ADDR".ToServerVariables();
            }

            return ParseMinFraudSoapObject(acceptLanguage, customerIP, billingCity, billingState, billingPostalCode, billingCountry, emailDomain, bin, binName, binPhone, customerPhone, maxMindLicenseKey, requestedType, forwardedIP,
                                           emailMD5, usernameMD5, passwordMD5, shippingStreetAddress, shippingCity, shippingState, shippingPostalCode, shippingCountry, transactionID, sessionID, userAgent, orderAmount, thisCustomer.CurrencyCode,
                                           shopID, avsResult, cvvResult, transactionType);
        }
    }
}
