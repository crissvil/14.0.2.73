﻿using System;
using System.Collections;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System.Security.Cryptography;
using Interprise.Licensing.Base.Services;
using System.Runtime.Serialization.Json;
using System.IO;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class CryptographyService : ServiceBase, ICryptographyService 
    {
        private IDateTimeService _dateTimeService;
        private char[] alphabet = "ABCD6ABCD67890EFGHJKLMNPABCD67890EFGHJKLMNP67896789789ABCD67890EFGHJKLMNP67890EFGHJKLMNP67890BCDEFGHJKLMNPRSTRSTUVWXYZ1234567890".ToCharArray();
        private Random rand = new Random();

        public CryptographyService(IDateTimeService dateTimeService)
        {
            _dateTimeService = dateTimeService;
        }

        public Hashtable DeserializeSessionParams(string paramXmlValue)
        {
            var sessionParamObject = new Hashtable();

            var x = new XmlDocument();
            x.LoadXml(paramXmlValue);

            foreach (XmlNode n in x.SelectNodes("//param"))
            {
                if (n.Attributes["name"] == null) continue;

                DateTime expireon = _dateTimeService.GetMaxValue();

                if (n.Attributes["expireon"] != null)
                {
                    expireon = DateTime.Parse(n.Attributes["expireon"].InnerText);
                }

                if (expireon > _dateTimeService.GetCurrentDate())
                {
                    sessionParamObject.Add(n.Attributes["name"].InnerText.ToLowerInvariant(), 
                        new SessionParam(n.Attributes["name"].InnerText, n.Attributes["val"].InnerText, expireon));
                }
            }

            return sessionParamObject;
        }

        public string SerializeSessionParams(Hashtable sesssionParam)
        {
            var sb = new StringBuilder("<params>", 1024);
            foreach (string s in sesssionParam.Keys)
            {
                var sp = (SessionParam)sesssionParam[s];
                sb.Append("<param name=\"" + XmlCommon.XmlEncodeAttribute(s) + "\" val=\"" + 
                                             XmlCommon.XmlEncodeAttribute(sp.ParamValue) + "\" " +
                                             ((sp.ExpireOn == DateTime.MaxValue)? "": ("expireon=\"" + Localization.DateTimeStringForDB(sp.ExpireOn) + "\"")) + " />");
            }
            sb.Append("</params>");
            return sb.ToString();
        }

        public string GetMD5Hash(string stringToHash)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            var md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            var data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(stringToHash));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            var sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }

        public byte[] InterpriseGenerateSalt()
        {
            return InterpriseHelper.GenerateSalt();
        }

        public byte[] InterpriseGenerateVector()
        {
            return InterpriseHelper.GenerateVector();
        }

        public string InterpriseEncryption(string value, byte[] salt, byte[] vector)
        {
            return InterpriseHelper.Encryption(value, salt, vector);
        }

        public string InterpriseDecryption(string value, string salt, string vector)
        {
            byte[] pwd = Convert.FromBase64String(value);
            byte[] slt = Convert.FromBase64String(salt);
            byte[] iv = Convert.FromBase64String(vector);

            return InterpriseHelper.Decryption(pwd, slt, iv);
        }

        public bool ValidatePassword(string dbPassword, string inputPassword, string salt, string vector)
        {
            var crypto = new CryptoServiceProvider();

            byte[] _salt = Convert.FromBase64String(salt);
            byte[] _vector = Convert.FromBase64String(vector);

            string encrypted = crypto.Encrypt(inputPassword, _salt, _vector);
            return dbPassword.Equals(encrypted);
        }

        public string SerializeToJson<T>(T obj)
        {
            var serializer = new DataContractJsonSerializer(obj.GetType());
            var ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.Default.GetString(ms.ToArray());
            ms.Dispose();
            return retVal;
        }

        public string SerializeToJson<T>(T obj, Encoding encodingType)
        {
            var serializer = new DataContractJsonSerializer(obj.GetType());
            var ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = encodingType.GetString(ms.ToArray());
  
            ms.Dispose();
            return retVal;
        }

        public T DeserializeJson<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            var ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            var serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            ms.Dispose();
            return obj;
        }

        public string GenerateRandomCode(int codeLength)
        {
            char[] randChars = randomAlphabetChars(codeLength);
            char[] formattedChars = new char[codeLength];
            for (int i = 0; i < randChars.Length; i++)
            {
                formattedChars[i] = randChars[i];
            }
            return new string(formattedChars);
        }

        private char[] randomAlphabetChars(int length)
        {
            char[] newChars = new char[length];
            for (int i = 0; i < length; i++)
                newChars[i] = alphabet[(int)Math.Truncate(rand.NextDouble() * 1000) % alphabet.Length];
            return newChars;
        }

        public string SerializeMaxMindResponse(MaxMindAPI.MINFRAUD maxmindObject)
        {
            MemoryStream stream = null;
            TextWriter writer = null;
            try
            {
                stream = new MemoryStream(); // read xml in memory
                writer = new StreamWriter(stream, Encoding.Unicode);
                // get serialise object
                XmlSerializer serializer = new XmlSerializer(typeof(MaxMindAPI.MINFRAUD));
                serializer.Serialize(writer, maxmindObject); // read object
                int count = (int)stream.Length; // saves object in memory stream
                byte[] arr = new byte[count];
                stream.Seek(0, SeekOrigin.Begin);
                // copy stream contents in byte array
                stream.Read(arr, 0, count);
                UnicodeEncoding utf = new UnicodeEncoding(); // convert byte array to string
                return utf.GetString(arr).Trim();
            }
            catch
            {
                return string.Empty;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Close();
                }
                if (writer != null)
                {
                    writer.Close();
                }
            }
        }
    }
}