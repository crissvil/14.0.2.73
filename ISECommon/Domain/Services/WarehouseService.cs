﻿using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.DTO;
using System.Linq;
using System;
using InterpriseSuiteEcommerceCommon.Domain.Model;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class WarehouseService : ServiceBase, IWarehouseService
    {
        private readonly IAppConfigService _appConfigService = null;
        private readonly IAuthenticationService _authenticationService = null;
        private readonly IDateTimeService _dataTimeService = null;
        private readonly ICryptographyService _cryptographyService = null;
        private readonly IWarehouseRepository _warehouseRepository = null;
        private readonly IRequestCachingService _requestCachingService = null;

        public WarehouseService(IAppConfigService appConfigService,
                              IAuthenticationService authenticationService,
                              IDateTimeService dataTimeService,
                              ICryptographyService cryptographyService,
                              IWarehouseRepository warehouseRepository,
                              IRequestCachingService requestCachingService) 
        {
            _appConfigService = appConfigService;
            _authenticationService = authenticationService;
            _dataTimeService = dataTimeService;
            _cryptographyService = cryptographyService;
            _warehouseRepository = warehouseRepository;
            _requestCachingService = requestCachingService;
        }

        public string GetStorePickupWarehouseStoreHoursToJSON(string warehouseCode)
        {
            var workingHours = _warehouseRepository.GetStoreWorkingHours(warehouseCode);

            WareHouseWorkingHoursHeader header = null;
            if (workingHours != null)
            {
                header = new WareHouseWorkingHoursHeader();
                header.WorkingHours = workingHours.Select(w => new StoreWorkingHoursDTO()
                {
                    Day = w.Day,
                    WorkingHour = new WorkingHourDTO()
                    {
                        Opening = (w.WorkingHour.Opening == DateTime.MinValue) ? String.Empty : w.WorkingHour.Opening.ToShortTimeString(),
                        Closing = (w.WorkingHour.Closing == DateTime.MinValue) ? String.Empty : w.WorkingHour.Closing.ToShortTimeString(),
                        Close = w.WorkingHour.Close
                    }
                });
            }

            var holidays = _warehouseRepository.GetStoreHolidayHours(warehouseCode);
            if (holidays.Count() > 0)
            {
                if (header == null) header = new WareHouseWorkingHoursHeader();

                header.Holidays = holidays.Select(h => new StoreHolidayHourDTO()
                {
                    Date = (h.Date == DateTime.MinValue) ? String.Empty : h.Date.ToShortDateString(),
                    Name = h.Name,
                    WorkingDay = new WorkingHourDTO()
                    {
                        Opening = (h.WorkingDay.Opening == DateTime.MinValue) ? String.Empty : h.WorkingDay.Opening.ToShortTimeString(),
                        Closing = (h.WorkingDay.Closing == DateTime.MinValue) ? String.Empty : h.WorkingDay.Closing.ToShortTimeString(),
                        Close = h.WorkingDay.Close
                    }
                });

            }

            if (header == null) return String.Empty;

            return _cryptographyService.SerializeToJson<WareHouseWorkingHoursHeader>(header);

        }

        public InventoryWarehouseModel GetWarehouseByCodeCachedPerRequest(string warehouseCode)
        {
            var model = _requestCachingService.GetItem<InventoryWarehouseModel>(warehouseCode);
            if (model == null)
            {
                model = _warehouseRepository.GetWarehouseByCode(warehouseCode);
                _requestCachingService.AddItem(warehouseCode, model);
            }

            return model;
        }

        public string GetWarehouseByCodeToJSON(string warehouseCode)
        {
            string json = String.Empty;
            var model = _warehouseRepository.GetWarehouseByCode(warehouseCode);

            var tranform = new StorePickupInventoryWarehousetTransform()
            {
                WareHouseDescription = model.WareHouseDescription,
                WareHouseCode = model.WareHouseCode,
                Address = new LiteAddressInfo()
                {
                    Address = model.Address,
                    City = model.City,
                    Country = model.Country,
                    PostalCode = model.PostalCode,
                    State = model.State
                }
            };

            if(model != null)
            {
                json = _cryptographyService.SerializeToJson<StorePickupInventoryWarehousetTransform>(tranform);
            }
            return json;
        }
    }
}
