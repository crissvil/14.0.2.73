﻿using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Tool;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class CronService : ServiceBase, ICronService
    {
        public void TryStartExpiredRegistryRemoval()
        {
            if (!Cron.CronRegistryExpirationRemoval.IsAlive)
            {
                Cron.CronRegistryExpirationRemoval.Start();
            }
        }

        public void TryStartExpiredEcommerceCustomerSessionRemoval()
        {
            if (!Cron.CronExpiredEcommerceCustomerSessionRemoval.IsAlive)
            {
                Cron.CronExpiredEcommerceCustomerSessionRemoval.Start();
            }
        }
    }
}