﻿using System;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Threading;
using Interprise.Framework.Customer.DatasetGateway;
using Interprise.Facade.Customer;
using Interprise.Framework.Base.Shared;
using InterpriseSuiteEcommerceCommon.DataAccess;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using System.Collections.Generic;
using InterpriseSuiteEcommerceCommon.Tool;
using System.Linq;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    /// <summary>
    /// Warning: This service should be reference to Authentication Service to avoid Bidirectional Dependency Problem
    /// </summary>
    public class CustomerService : ServiceBase, ICustomerService
    {
        ICustomerRepository _customerRepository = null;
        IAppConfigService _appConfigService = null;
        IDateTimeService _dateTimeService = null;
        ICryptographyService _cryptographyService = null;
        IAuthenticationService _authenticationService = null;
        IShoppingCartRepository _shoppingCartRepository = null;
        INavigationService _navigationService = null;
        IStringResourceService _stringResourceService = null;
        IRequestCachingService _requestCachingService = null;
        ICurrencyService _currencyService = null;

        public CustomerService(ICustomerRepository customerRepository,
                               IAppConfigService appConfigService,
                               IDateTimeService dateTimeService,
                               ICryptographyService cryptographyService,
                               IAuthenticationService authenticationService,
                               IShoppingCartRepository shoppingCartRepository,
                               INavigationService navigationService,
                               IStringResourceService stringResourceService,
                               IRequestCachingService requestCachingService,
                               ICurrencyService currencyService)
        {
            _customerRepository = customerRepository;
            _appConfigService = appConfigService;
            _dateTimeService = dateTimeService;
            _cryptographyService = cryptographyService;
            _authenticationService = authenticationService;
            _shoppingCartRepository = shoppingCartRepository;
            _navigationService = navigationService;
            _stringResourceService = stringResourceService;
            _requestCachingService = requestCachingService;
            _currencyService = currencyService;
        }

        public void ClearCustomerCoupon(string customerCode, bool isCustomerRegistered)
        {
            if (isCustomerRegistered)
            {
                _customerRepository.ClearCouponForRegisteredCustomer(customerCode);
            }
            else
            {
                _customerRepository.ClearCouponForAnonCustomer(customerCode);
            }
        }

        public bool IsEmailAlreadyInUse(string email, Guid notByThisCustomer)
        {
            return _customerRepository
                        .IsEmailAlreadyInUse(email, notByThisCustomer, _appConfigService.WebSiteCode);
        }

        public Customer MakeAnonymous()
        {
            return _customerRepository.Find(Guid.Empty,
                                        _appConfigService.WebSiteCode,
                                        _appConfigService.AllowCreditHold,
                                        _appConfigService.AllowProductFiltering);
        }

        public Customer Find(Guid contactGuid)
        {
            return _customerRepository.Find(contactGuid,
                                        _appConfigService.WebSiteCode,
                                        _appConfigService.AllowCreditHold,
                                        _appConfigService.AllowProductFiltering);
        }

        public bool ValidateContactSubscription(string contactCode)
        {
            return _customerRepository.ValidateContactSubscription(contactCode,
                                                            _dateTimeService.GetCurrentDate(),
                                                            _dateTimeService.GetCurrentDate(),
                                                            _appConfigService.WebSiteCode);
        }

        public void ClearCustomerSession(Guid customerGuid)
        {
            _customerRepository
                .ClearCustomerSession(customerGuid, _dateTimeService.GetCurrentDate());
        }

        public void CreateContactSiteLog(string contactCode, string details)
        {
            _customerRepository.CreateContactSiteLog(contactCode, details,
                                    _dateTimeService.GetCurrentDate(),
                                    _appConfigService.WebSiteCode,
                                    _appConfigService.UserCode);
        }

        public AnonCustomerSettings GetAnonCustomerSettings()
        {
            return _customerRepository.GetAnonCustomerSettings(_appConfigService.WebSiteCode);
        }

        public AnonLatestContactGuidAndCustomerCode MakeAnonCustomerRecord()
        {
            var settings = _customerRepository.GetAnonCustomerSettings(_appConfigService.WebSiteCode);
            string newContactGuidString = _customerRepository.CreateAnonCustomer(settings.LocaleSettings, settings.CurrencyCode);

            _customerRepository.UpdateEcommerceCustomerCodeToID(newContactGuidString);

            var latestContacGuidAndCode = new AnonLatestContactGuidAndCustomerCode()
            {
                ContactGuid = new Guid(newContactGuidString),
                CustomerCode = _customerRepository.GetUpdatedCustomerIdByContactGuid(newContactGuidString)
            };

            return latestContacGuidAndCode;
        }

        public void UpdateCustomer(CustomerInfo customerInfo)
        {
            string passwordTemp = String.Empty;
            string saltTemp = String.Empty;
            string vectorTemp = String.Empty;

            bool shouldUpdatePassword = !customerInfo.Password.Equals(AppLogic.PasswordValuePlaceHolder);
            if (shouldUpdatePassword)
            {
                byte[] salt = _cryptographyService.InterpriseGenerateSalt();
                byte[] iv = _cryptographyService.InterpriseGenerateVector();
                string passwordCypher = _cryptographyService.InterpriseEncryption(customerInfo.Password, salt, iv);

                passwordTemp = passwordCypher;
                saltTemp = Convert.ToBase64String(salt);
                vectorTemp = Convert.ToBase64String(iv);
            }

            customerInfo.Password = passwordTemp;
            customerInfo.Salt = saltTemp;
            customerInfo.Vector = vectorTemp;

            _customerRepository.UpdateCustomer(customerInfo);
        }

        public string GenerateRequestCodeForActiveShopper()
        {
            string code = String.Empty;

            int? timeLimit = _appConfigService.WebSupportTime.TryParseInt();
            if (!timeLimit.HasValue) { timeLimit = 3; }

            _customerRepository.DeleteInActiveShoppers(_dateTimeService.GetCurrentDate(), timeLimit.Value);

            var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            if (thisCustomer == null) return code;

            if (!_appConfigService.WebSupportEnabled) return code;

            //delay for .05 seconds
            Thread.Sleep(50);

            if (_customerRepository.IsActiveShopperCodeNotUsed(thisCustomer.CustomerCode))
            {
                int? defaultCodeLen = _appConfigService.WebSupportCodeLength.TryParseInt();

                if (!defaultCodeLen.HasValue) { defaultCodeLen = 6; }

                code = _cryptographyService.GenerateRandomCode(defaultCodeLen.Value);
                _customerRepository.AddActiveShoppers(new ActiveShopper()
                {
                    ActiveShopperId = Interprise.Facade.Base.SimpleFacade.Instance.GenerateDocumentCode(Interprise.Framework.Base.Shared.Enum.TransactionType.ECommerceActiveShopper.ToString()),
                    TimeStarted = DateTime.Now,
                    ContactGuid = thisCustomer.ContactGUID,
                    CustomerCode = thisCustomer.CustomerCode,
                    WebsiteCode = InterpriseHelper.ConfigInstance.WebSiteCode,
                    RequestCode = code,
                    DateCreated = _dateTimeService.GetCurrentDate()
                });

            }
            else
            {
                code = _customerRepository.GetShopperRequestCodeByCustomerCode(thisCustomer.CustomerCode);
            }

            return code.ToUpperInvariant();
        }

        public bool IsCustomerAddressesEdited()
        {
            bool retVal = false;

            var currentCustomer = _authenticationService.GetCurrentLoggedInCustomer();

            bool? isShippingEdited = currentCustomer.ThisCustomerSession[DomainConstants.CUSTOMER_SESSION_SHIPPING_ADDRESS_EDITED_KEY]
                                                    .TryParseBool();

            if (isShippingEdited.HasValue)
            {
                retVal = isShippingEdited.Value;
            }

            return retVal;
        }

        public void MakeDefaultAddress(string newAddressId, AddressTypes addressType)
        {
            var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            MakeDefaultAddress(thisCustomer.ContactCode, thisCustomer.PrimaryShippingAddressID, newAddressId, addressType, false);
        }

        public void MakeDefaultAddress(string newAddressId, AddressTypes addressType, bool byPassEditAddressChecking)
        {
            var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            MakeDefaultAddress(thisCustomer.ContactCode, thisCustomer.PrimaryShippingAddressID, newAddressId, addressType, byPassEditAddressChecking);
        }

        public void MakeDefaultAddress(string contactCode, string oldAddressId, string newAddressId, AddressTypes addressType, bool byPassEditAddressChecking)
        {
            switch (addressType)
            {
                case AddressTypes.Billing:

                    _customerRepository.UpdateContactDefaultBillingCode(newAddressId, contactCode);
                    break;

                case AddressTypes.Shipping:

                    _customerRepository.UpdateContactDefaultShippingCode(newAddressId, contactCode);
                    _shoppingCartRepository.UpdateShoppingCartShippingAddressIdByOldPrimaryId(newAddressId, oldAddressId, contactCode);
                    _shoppingCartRepository.UpdateShoppingCartShippingAddressIdForRegistryItems(newAddressId, oldAddressId);

                    break;
            }

            if (!byPassEditAddressChecking)
            {
                //Tag the customer address been edited
                _authenticationService.GetCurrentLoggedInCustomer()
                                      .ThisCustomerSession[DomainConstants.CUSTOMER_SESSION_SHIPPING_ADDRESS_EDITED_KEY] = true.ToString();
            }
        }

        public void DoShippingAddressModificationChecking()
        {
            if (!IsCustomerShippingAddressEdited()) return;

            var customer = _authenticationService.GetCurrentLoggedInCustomer();

            ClearAddressCheckingKey();

            _navigationService.NavigateToShoppingCartRestLinkBackWithErroMessage(_stringResourceService.GetString("shoppingcart.aspx.message.1"));
        }

        public bool IsCustomerShippingAddressEdited()
        {
            bool retVal = false;

            var customer = _authenticationService.GetCurrentLoggedInCustomer();

            bool? isEdited = customer.ThisCustomerSession[DomainConstants.CUSTOMER_SESSION_SHIPPING_ADDRESS_EDITED_KEY]
                                     .TryParseBool();

            if (isEdited.HasValue)
            {
                retVal = isEdited.Value;
            }

            return retVal;
        }

        public void DoRegisteredCustomerShippingAndBillingAddressChecking()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            if (customer.IsRegistered && (customer.PrimaryBillingAddressID.IsNullOrEmptyTrimmed() || customer.PrimaryShippingAddressID.IsNullOrEmptyTrimmed()))
            {
                _navigationService.NavigateToShoppingCartRestLinkBackWithErroMessage(_stringResourceService.GetString("checkoutpayment.aspx.1", true));
            }
        }

        public void DoIsOver13Checking()
        {
            DoIsOver13Checking(false);
        }

        public void DoIsOver13Checking(bool includeRegisterdCustomer)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();

            bool checkForRegisteredCustomer = true;

            if (includeRegisterdCustomer) { checkForRegisteredCustomer = customer.IsRegistered; }

            if (checkForRegisteredCustomer && _appConfigService.RequireOver13Checked && !customer.IsOver13)
            {
                _navigationService.NavigateToShoppingCartWitErroMessage(_stringResourceService.GetString("checkout.over13required", true));
            }
        }

        public void DoIsCreditOnHoldChecking()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();

            if (customer.IsCreditOnHold)
            {
                _navigationService.NavigateToShoppingCart();
            }
        }

        public void ClearAddressCheckingKey()
        {
            _authenticationService.GetCurrentLoggedInCustomer()
                                  .ThisCustomerSession
                                  .ClearVal(DomainConstants.CUSTOMER_SESSION_SHIPPING_ADDRESS_EDITED_KEY);
        }

        public void UpdateCustomerShipTo(Address shipToAddress)
        {
            UpdateCustomerShipTo(null, shipToAddress, false, false);
        }

        public void UpdateCustomerShipTo(Address shipToAddress, bool byPassEditAddressChecking)
        {
            UpdateCustomerShipTo(null, shipToAddress, byPassEditAddressChecking, false);
        }

        public void UpdateCustomerShipTo(Customer thisCustomer, Address shipToAddress, bool byPassEditAddressChecking, bool useCustParam)
        {
            Customer currentCustomer;
            //useCustParam = false;
            if (useCustParam)
            {
                currentCustomer = thisCustomer;
            }
            else
            {
                currentCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            }

            if (shipToAddress.AddressType != AddressTypes.Shipping) return;
            if (!currentCustomer.IsRegistered) return;

            using (var gatewayShipTo = new ShipToDatasetGateway())
            {
                using (var facadeShipTo = new ShipToFacade(gatewayShipTo))
                {
                    facadeShipTo.LoadDataSet(
                        new string[][]{new string[]{
                            CustomerDetailDatasetGateway.CUSTOMERSHIPTOVIEW_TABLE,
                            StoredProcedures.READCUSTOMERSHIPTO}},
                            new string[][] { new string[] { "@CustomerCode", shipToAddress.CustomerCode }, 
                                new string[] { "@ShipToCode", shipToAddress.AddressID }},
                        Interprise.Framework.Base.Shared.Enum.ClearType.Specific,
                        Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online
                    );

                    var shipToRow = gatewayShipTo.CustomerShipToView[0];

                    shipToRow.BeginEdit();
                    shipToRow.ShipToName = shipToAddress.Name;
                    shipToRow.Address = shipToAddress.Address1;
                    shipToRow.City = shipToAddress.City;
                    shipToRow.State = shipToAddress.State;
                    shipToRow.County = shipToAddress.County;
                    shipToRow.Country = shipToAddress.Country;
                    shipToRow.Telephone = shipToAddress.Phone;
                    shipToRow.AddressType = shipToAddress.ResidenceType.ToString();

                    #region Postal Code Handler

                    var parsedPostalCode = InterpriseHelper.ParsePostalCode(shipToAddress.Country, shipToAddress.PostalCode);
                    shipToRow.PostalCode = parsedPostalCode.PostalCode;
                    if (parsedPostalCode.Plus4 > 0)
                    {
                        shipToRow.Plus4 = parsedPostalCode.Plus4;
                    }
                    else
                    {
                        _customerRepository.ClearCustomerShippingAddressPlus4Field(shipToRow.ShipToCode);
                    }

                    #endregion

                    shipToRow.EndEdit();

                    string[][] updateCommandset =
                        new string[][]{
                            new string[]{ 
                            CustomerDetailDatasetGateway.CUSTOMERSHIPTOVIEW_TABLE,
                            StoredProcedures.CREATECUSTOMERSHIPTO,
                            StoredProcedures.UPDATECUSTOMERSHIPTO,
                            StoredProcedures.DELETECUSTOMERSHIPTO},
                        };

                    facadeShipTo.UpdateDataSet(updateCommandset,
                                               Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerShipTo, String.Empty, false);

                    if (!byPassEditAddressChecking)
                    {
                        currentCustomer.ThisCustomerSession[DomainConstants.CUSTOMER_SESSION_SHIPPING_ADDRESS_EDITED_KEY] = true.ToString();
                    }

                }
            }
        }

        public void UpdateCustomerBillTo(Address billToAddress)
        {
            UpdateCustomerBillTo(null, billToAddress, false, false);
        }

        public void UpdateCustomerBillTo(Address billToAddress, bool byPassEditAddressChecking)
        {
            UpdateCustomerBillTo(null, billToAddress, byPassEditAddressChecking, false);
        }

        public void UpdateCustomerBillTo(Customer thisCustomer, Address billToAddress, bool byPassEditAddressChecking, bool useCustParam)
        {
            Customer currentCustomer;
            //useCustParam = false;
            if (useCustParam)
            {
                currentCustomer = thisCustomer;
            }
            else
            {
                currentCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            }

            if (billToAddress.AddressType != AddressTypes.Billing) return;

            if (currentCustomer.IsRegistered)
            {
                using (var gatewayCreditCardDataset = new CreditCardDatasetGateway())
                {
                    using (var facadeCreditCard = new CreditCardFacade(gatewayCreditCardDataset))
                    {
                        // NOTE:
                        //  For some reason, the compiler doesn't seem to recognize
                        //  this overload from CreditCardFacade, it may be caused by the
                        //  last parameter not being CLS Compliant. So we'll downreference
                        //  our needed overloaded method to it's base.
                        //Interprise.Facade.Base.BaseFacade facadeBase = facadeCreditCard;

                        facadeCreditCard.LoadDataSet(new string[][]{ new string[]{
                                CreditCardDatasetGateway.CUSTOMERCREDITCARDVIEW_TABLE,
                                StoredProcedures.READCUSTOMERCREDITCARD,
                                "@CreditCardCode", 
                                billToAddress.AddressID}},
                            Interprise.Framework.Base.Shared.Enum.ClearType.Specific,
                            Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                        // let's make sure we won't be using this anymore
                        //facadeBase = null;

                        var creditCardRow = gatewayCreditCardDataset.CustomerCreditCardView[0];

                        creditCardRow.BeginEdit();

                        bool hasChanges = CustomerDA.HasChangesToAddressInfo(creditCardRow, billToAddress);

                        creditCardRow.CustomerName = billToAddress.CardName;
                        creditCardRow.CustomerCode = billToAddress.CustomerCode;
                        creditCardRow.NameOnCard = billToAddress.CardName;
                        creditCardRow.Address = billToAddress.Address1;
                        creditCardRow.City = billToAddress.City;
                        creditCardRow.State = billToAddress.State;
                        creditCardRow.County = billToAddress.County;
                        creditCardRow.Country = billToAddress.Country;
                        creditCardRow.Telephone = billToAddress.Phone;
                        creditCardRow.Email = billToAddress.EMail;
                        creditCardRow.ResidenceType = billToAddress.ResidenceType.ToString();

                        #region Postal Code Handler

                        var parsedPostalCode = InterpriseHelper.ParsePostalCode(billToAddress.Country, billToAddress.PostalCode);
                        creditCardRow.PostalCode = parsedPostalCode.PostalCode;
                        if (parsedPostalCode.Plus4 > 0)
                        {
                            creditCardRow.Plus4 = parsedPostalCode.Plus4;
                        }
                        else
                        {
                            ServiceFactory.GetInstance<ICustomerRepository>()
                                          .ClearCustomerBillingAddressPlus4Field(billToAddress.AddressID);
                        }

                        #endregion

                        if (!billToAddress.CardNumber.IsNullOrEmptyTrimmed())
                        {
                            creditCardRow.MaskedCardNumber = (billToAddress.CardNumber.StartsWith("X")) ? billToAddress.CardNumber : Interprise.Framework.Base.Shared.Common.MaskCardNumber(billToAddress.CardNumber);
                            creditCardRow.ExpMonth = InterpriseHelper.ToInterpriseExpMonth(billToAddress.CardExpirationMonth);
                            creditCardRow.ExpYear = billToAddress.CardExpirationYear;
                            creditCardRow.StartMonth = (billToAddress.CardStartMonth != null) ? InterpriseHelper.ToInterpriseExpMonth(billToAddress.CardStartMonth) : null;
                            creditCardRow.StartYear = billToAddress.CardStartYear;
                            creditCardRow.CreditCardType = billToAddress.CardType;
                            creditCardRow.CreditCardDescription = (billToAddress.CardDescription == string.Empty) ? "Web Credit Card" : billToAddress.CardDescription;
                        }

                        creditCardRow.EndEdit();

                        bool isCreditCardTokenization = (AppLogic.IsUsingInterpriseGatewayv2() && AppLogic.AppConfigBool("AllowCreditCardInfoSaving"));
                        var creditCardInfo = CreditCardDTO.Find(billToAddress.AddressID);

                        if (creditCardInfo != null)
                        {
                            if (!isCreditCardTokenization && creditCardInfo.Vault != String.Empty && creditCardInfo.RefNo > 0)
                            {
                                //if tokenization is disabled, clear tokenized credit card 

                                creditCardRow.CardNumber = null;
                                creditCardRow.MaskedCardNumber = null;
                                creditCardRow.ExpMonth = null;
                                creditCardRow.ExpYear = null;
                                creditCardRow.CreditCardType = null;
                                creditCardRow.CreditCardSalt = null;
                                creditCardRow.CreditCardIV = null;
                                creditCardRow.CreditCardDescription = null;
                                creditCardRow.InterpriseGatewayRefNo = 0;
                                creditCardRow.Vault = null;
                            }

                            if (isCreditCardTokenization && hasChanges)
                            {
                                //set the ff. properties in order to update customer vault record
                                //in transnational gateway during updatedataset command

                                facadeCreditCard.Vault = creditCardInfo.Vault;
                                facadeCreditCard.PnRefno = creditCardInfo.RefNo.ToString();
                                facadeCreditCard.RequireCreditCardInfo = true;
                            }
                        }

                        string[][] updateCommandset = new string[][]{ new string[]{
                                                            CreditCardDatasetGateway.CUSTOMERCREDITCARDVIEW_TABLE,
                                                            StoredProcedures.CREATECUSTOMERCREDITCARD,
                                                            StoredProcedures.UPDATECUSTOMERCREDITCARD,
                                                            StoredProcedures.DELETECUSTOMERCREDITCARD }};

                        facadeCreditCard.UpdateDataSet(updateCommandset, Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerCreditCard, string.Empty, false);

                        if (!billToAddress.CardType.IsNullOrEmptyTrimmed())
                        {
                            CustomerDA.UpdateCustomerCreditCardType(gatewayCreditCardDataset.CustomerCreditCardView[0].CustomerCode,
                                                                gatewayCreditCardDataset.CustomerCreditCardView[0].CreditCardCode,
                                                                billToAddress.CardType);
                        }

                        if (currentCustomer.PrimaryBillingAddressID == billToAddress.AddressID)
                        {
                            CustomerDA.UpdateCustomerBusinessType(gatewayCreditCardDataset.CustomerCreditCardView[0].CustomerCode, currentCustomer.BusinessType);

                            CustomerDA.UpdateCustomerBusinessTaxNumber(gatewayCreditCardDataset.CustomerCreditCardView[0].CustomerCode, currentCustomer.TaxNumber);
                        }

                        if (!byPassEditAddressChecking)
                        {
                            //Tag the customer address been edited
                            _authenticationService.GetCurrentLoggedInCustomer()
                                                  .ThisCustomerSession[DomainConstants.CUSTOMER_SESSION_SHIPPING_ADDRESS_EDITED_KEY] = true.ToString();
                        }
                    }
                }
            }
            else
            {
                billToAddress.Save();
            }
        }

        public CustomerLoyaltyPointsCustomModel GetLoyaltyPoints()
        {
            decimal multiplier = GetRedemptionMultiplier();
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _customerRepository.GetCustomerLoyaltyPoints(customer.CustomerCode, multiplier);
        }

        public decimal GetRedemptionMultiplier()
        {
            string key = DomainConstants.REDEMPTION_MULTIPLIER;
            decimal multiplier = Decimal.Zero;

            if (_requestCachingService.Exist(key)) { multiplier = _requestCachingService.GetItem(key).ToString().ToDecimal(); }
            else 
            {
                multiplier = _customerRepository.GetCustomerAdvancePreference(key).ToDecimal();
                _requestCachingService.AddItem(key, multiplier);
            }

            return multiplier;
        }

        public decimal GetPurchaseMultiplier()
        {
            string key = DomainConstants.PURCHASE_MULTIPLIER;
            decimal multiplier = Decimal.Zero;

            if (_requestCachingService.Exist(key)) { multiplier = _requestCachingService.GetItem(key).ToString().ToDecimal(); }
            else
            {
                multiplier = _customerRepository.GetCustomerAdvancePreference(key).ToDecimal();
                _requestCachingService.AddItem(key, multiplier);
            }

            return multiplier;
        }

        public void DoIsNotRegisteredAndPasswordIsOptionalChecking()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            if (customer.IsNotRegistered && !_appConfigService.PasswordIsOptionalDuringCheckout)
            {
                _navigationService.NavigateToUrl("createaccount.aspx?checkout=true");
            }
        }

        public void DoIsRegisteredAndHasPrimaryBillingAddress()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            if (customer.IsRegistered && customer.PrimaryBillingAddressID.IsNullOrEmptyTrimmed())
            {
                _navigationService.NavigateToUrl("selectaddress.aspx?add=true&setPrimary=true&checkout=False&addressType=Billing&returnURL=account.aspx");
            }
        }

        public void DoIsNotRegisteredChecking()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            var billingAddress = customer.PrimaryBillingAddress;

            if (customer.IsRegistered) { return; }
            if (billingAddress.FirstName.IsNullOrEmptyTrimmed() ||
                billingAddress.LastName.IsNullOrEmptyTrimmed()) { _navigationService.NavigateToCheckoutAnon(); }
        }

        public void DoMobileIsNotRegisteredChecking()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            var billingAddress = customer.PrimaryBillingAddress;

            if (customer.IsRegistered) { return; }
            if (billingAddress.Name.IsNullOrEmptyTrimmed()) { _navigationService.NavigateToCheckoutAnon(); }
        }

        public void UpdateCustomerRequiredAge()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            _customerRepository.UpdateCustomerRequiredAge(customer.CustomerID);
        }

        public void AssignAnonymousCustomerEmailAddressInSalesOrderNote()
        {
            var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            if (thisCustomer.IsRegistered) return;

            thisCustomer.ThisCustomerSession["anonymousCustomerNote"] = String.Empty;
            if (thisCustomer.EMail.Length > DomainConstants.ORDER_NOTE_MAX_LENGTH)
            {
                thisCustomer.ThisCustomerSession["anonymousCustomerNote"] = thisCustomer.EMail.Substring(0, DomainConstants.ORDER_NOTE_MAX_LENGTH);
            }
            else
            {
                thisCustomer.ThisCustomerSession["anonymousCustomerNote"] = String.Format("Anonymous Customer: {0}", thisCustomer.EMail);
            }
        }

        public void AssignPayPalExpressCheckoutNoteInSalesOrderNote()
        {
            var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            string paypalNote = _stringResourceService.GetString("paypalecpostback.aspx.1", true);
            string orderNote = String.Empty;

            if (thisCustomer.IsNotRegistered)
            {
                thisCustomer.ThisCustomerSession["notesFromPayPal"] = thisCustomer.ThisCustomerSession["anonymousCustomerNote"];
            }

            orderNote = thisCustomer.ThisCustomerSession["notesFromPayPal"];
            if (!orderNote.IsNullOrEmptyTrimmed())
            {
                if (!orderNote.Contains(paypalNote))
                {
                    orderNote += Environment.NewLine + paypalNote;
                    if (orderNote.Length >= DomainConstants.ORDER_NOTE_MAX_LENGTH)
                    {
                        orderNote = orderNote.Substring(0, DomainConstants.ORDER_NOTE_MAX_LENGTH);
                    }
                }
            }
            else
            {
                if (paypalNote.Length > DomainConstants.ORDER_NOTE_MAX_LENGTH)
                {
                    orderNote = paypalNote.Substring(0, DomainConstants.ORDER_NOTE_MAX_LENGTH);
                }
                else
                {
                    orderNote = paypalNote;
                }
            }

            thisCustomer.ThisCustomerSession["notesFromPayPal"] = orderNote;
        }

        public string GetAnonEmail()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            if (!customer.HasCustomerRecord) { return String.Empty; }
            return _customerRepository.GetAnonEmail(customer.CustomerID);
        }

        public IEnumerable<CustomerCreditCustomModel> GetCustomerCredits()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _customerRepository.GetCustomerCredits(customer.CustomerCode);
        }

        public IEnumerable<CustomerCreditCustomModel> GetCustomerCreditMemos()
        {
            return GetCustomerCredits().Where(c => c.Type.EqualsIgnoreCase(CustomerCreditType.CREDIT_MEMO));
        }

        public IEnumerable<CustomerCreditCustomModel> GetCustomerCreditMemosWithRemainingBalance()
        {
            return GetCustomerCreditMemos().Where(c => c.CreditRemainingBalance > Decimal.Zero);
        }

        public void ParseName(string fullName, ref string salutation, ref string firstName, ref string middleName, ref string lastName, ref string suffix)
        {
            salutation = String.Empty;
            firstName = String.Empty;
            lastName = String.Empty;
            middleName = String.Empty;
            suffix = String.Empty;

            string tempSalutation = String.Empty;
            string tempFirstName = String.Empty;
            string tempLastName = String.Empty;
            string tempMiddleName = String.Empty;
            string tempSuffix = String.Empty;

            tempSalutation = ExtractSalutation(ref fullName);

            tempSuffix = ExtractSuffix(ref fullName);

            if (!fullName.IsNullOrEmptyTrimmed())
            {
                if (fullName.Contains(","))
                {
                    tempLastName = ExtractFirstWord(ref fullName);
                    tempFirstName = ExtractFirstWord(ref fullName);
                }
                else
                {
                    int wordCount = GetWordCount(fullName);
                    string specialSalutation = "Sir,Madam,Ma'am";

                    if (wordCount == 1 && (!tempSuffix.IsNullOrEmptyTrimmed() || 
                                          (!tempSalutation.IsNullOrEmptyTrimmed() && 
                                          specialSalutation.ToUpperInvariant().IndexOf(tempSalutation.ToUpperInvariant()) == 0)))
                    {
                        
                        tempLastName = ExtractFirstWord(ref fullName);
                    }
                    else
                    {
                        tempFirstName = ExtractFirstWord(ref fullName);
                        if (!fullName.IsNullOrEmptyTrimmed())
                        {
                            tempLastName = ExtractLastWord(ref fullName);
                        }
                    }
                }
            }

            if (!fullName.IsNullOrEmptyTrimmed())
            {
                tempMiddleName = ExtractLastWord(ref fullName);
            }

            if (!fullName.IsNullOrEmptyTrimmed() && !tempFirstName.IsNullOrEmptyTrimmed())
            {
                tempFirstName = String.Format("{0} {1}", tempFirstName, fullName.Trim());
            }

            if (String.Compare(salutation, tempSalutation, true) != 0)
            {
                salutation = tempSalutation;
            }

            if (String.Compare(firstName, tempFirstName, true) != 0)
            {
                firstName = tempFirstName;
            }

            if (String.Compare(middleName, tempMiddleName, true) != 0)
            {
                middleName = tempMiddleName;
            }

            if (String.Compare(lastName, tempLastName, true) != 0)
            {
                lastName = tempLastName;
            }

            if (String.Compare(suffix, tempSuffix, true) != 0)
            {
                suffix = tempSuffix;
            }
        }

        public string FixSpacing(string fullName)
        {
            if (fullName.IsNullOrEmptyTrimmed())
            {
	            return fullName;
            }

            string name = fullName.Trim();
            int intPos = 0;

            while (intPos < name.Length - 1) {
	            if (name.Substring(intPos, 1) == " ") {
		            if (name.Substring(intPos + 1, 1) == " " | name.Substring(intPos + 1, 1) == ",") {
			            name = name.Remove(intPos, 1);
		            } else {
			            intPos += 1;
		            }
	            } else if (name.Substring(intPos, 1) == ",") {
		            if (name.Substring(intPos + 1, 1) != " ") {
                        name = name.Insert(intPos + 1, " ");
		            } else {
			            intPos += 1;
		            }
	            } else {
		            intPos += 1;
	            }
            }

            return name;
        }

        public int GetWordCount(string name)
        {
            if (name.IsNullOrEmptyTrimmed())
            {
                return 0;
            }

            string tempName = FixSpacing(name);
            string[] nameArray = {};

            nameArray = tempName.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            return nameArray.Length;
        }

        public string ExtractFirstWord(ref string contactName)
        {
            if (contactName.IsNullOrEmptyTrimmed())
            {
                return contactName;
            }

            int firstDelimiterIndex = 0;
            string firstWord = String.Empty;

            contactName = contactName.Trim();
            if (contactName.Contains(","))
            {
                contactName = contactName.Replace(",", " ");
            }
            firstDelimiterIndex = contactName.IndexOf(" ");

            if (firstDelimiterIndex > 0)
            {
                firstWord = contactName.Substring(0, firstDelimiterIndex).Trim();
            }
            else
            {
                firstWord = contactName.Trim();
            }

            if (!firstWord.IsNullOrEmptyTrimmed())
            {
                contactName = contactName.Trim().Substring(firstWord.Length);
            }

            return firstWord;

        }

        public string ExtractLastWord(ref string contactName)
        {
            if (contactName.IsNullOrEmptyTrimmed())
            {
                return contactName;
            }

            int lastSpaceIndex = 0;
            string lastWord = String.Empty;

            contactName = contactName.Trim();

            lastSpaceIndex = contactName.LastIndexOf(" ");
            if (lastSpaceIndex > 0)
            {
                lastWord = contactName.Substring(lastSpaceIndex, contactName.Length - (lastSpaceIndex)).Trim();
            }
            else
            {
                lastWord = contactName;
            }

            if (!contactName.IsNullOrEmptyTrimmed())
            {
                contactName = contactName.Trim().Substring(0, lastSpaceIndex + 1);
            }
            return lastWord;
        }

        public string ExtractSalutation(ref string contactName)
        {
            string salutation = String.Empty;

            if (contactName.IsNullOrEmptyTrimmed())
            {
                return contactName;
            }

            contactName.Trim();

            salutation = _customerRepository.GetSalutation(contactName);

            if (!salutation.IsNullOrEmptyTrimmed())
            {
                if (!contactName.Contains(salutation))
                {
                    if (salutation.LastIndexOf(".") == salutation.Length - 1)
                    {
                        salutation = salutation.Remove(salutation.Length - 1, 1);
                    }
                    else
                    {
                        salutation = salutation + ".";
                    }
                }

                int salutationLength = 0;
                if (contactName.Contains(","))
                {
                    salutationLength = salutation.Length;
                    salutation = contactName.Substring(contactName.ToUpper().IndexOf(salutation.ToUpper(), salutationLength));

                    if (!salutationLength.IsNullOrEmptyTrimmed())
                    {
                        contactName = contactName.Trim().Remove(contactName.ToUpper().IndexOf(salutation.ToUpper(), salutationLength));
                    }
                }
                else
                {
                    salutationLength = contactName.ToUpper().IndexOf(salutation.ToUpper()) + salutation.Length;
                    salutation = contactName.Substring(0, salutationLength);

                    if (!salutation.IsNullOrEmptyTrimmed())
                    {
                        contactName = contactName.Trim().Substring(salutation.Length);
                    }
                }
            }

            return salutation;
        }

        public string ExtractSuffix(ref string contactName)
        {
            string suffix = String.Empty;

            if (contactName.IsNullOrEmptyTrimmed())
            {
                return contactName;
            }

            contactName = contactName.Trim();

            suffix = _customerRepository.GetSuffix(contactName);

            if (!suffix.IsNullOrEmptyTrimmed())
            {
                if (!contactName.Contains(suffix))
                {
                    if (suffix.LastIndexOf(".") == suffix.Length - 1)
                    {
                        suffix = suffix.Remove(suffix.Length - 1, 1);
                    }
                }

                int suffixLength = contactName.Length - contactName.ToUpper().LastIndexOf(suffix.ToUpper());
                suffix = contactName.Substring(contactName.ToUpper().LastIndexOf(suffix.ToUpper()), suffixLength);

                if (!suffix.IsNullOrEmptyTrimmed())
                {
                    contactName = contactName.Trim().Substring(0, contactName.ToUpper().LastIndexOf(suffix.ToUpper()));
                }
            }
            return suffix;
        }

        #region Gift Codes

        public IEnumerable<GiftCodeCustomModel> GetCustomerGiftCodes()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _customerRepository.GetCustomerGiftCodes(customer.CustomerCode)
                                      .GroupBy(code => new { code.SerialCode, code.Type })
                                      .Select(group => new GiftCodeCustomModel
                                      {
                                          SerialCode = group.Key.SerialCode,
                                          Type = group.Key.Type,
                                          CreditAvailable = group.Sum(x => x.CreditAvailable),
                                          CreditAvailableRate = group.Sum(x => x.CreditAvailableRate),
                                          CreditAllocatedRate = group.Sum(x => x.CreditAllocatedRate),
                                          CreditReservedRate = group.Sum(x => x.CreditReservedRate),
                                          CreditAmountRate = group.Sum(x => x.CreditAmountRate),
                                          CreditAvailableFormatted = _currencyService.FormatCurrency(group.Sum(x => x.CreditAvailable)),
                                          IsOwned = true
                                      });
        }

        #endregion

        public bool HasChangedVATSetting(VatDefaultSetting newVATSetting)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return (customer.VATSettingRaw != newVATSetting);
        }

        public bool IsCustomerSubscribeToProductNotification(string itemCode, int notificationType)
        {
            bool hasSubscription = false;
            var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();

            var customerProductNotificationSubscription = _customerRepository.GetCustomerProductNotificationSubscription(thisCustomer.ContactCode, _appConfigService.WebSiteCode, itemCode, thisCustomer.EMail);

            if (notificationType == 1)
            {
                if (customerProductNotificationSubscription.NotifyOnPriceDrop.ToString().ToLowerInvariant() == "true")
                {
                    hasSubscription = true;
                }
            }
            else
            {
                if (customerProductNotificationSubscription.NotifyOnItemAvail.ToString().ToLowerInvariant() == "true")
                {
                    hasSubscription = true;
                }
            }
            return hasSubscription;
        }

        public bool IsCustomerEmailNotAvailable(string emailAddress)
        {
            return _customerRepository.IsCustomerEmailNotAvailable(emailAddress, _authenticationService.GetCurrentLoggedInCustomer().CustomerCode);
        }

        public bool IsLeadEmailNotAvailable(string emailAddress)
        {
            return _customerRepository.IsLeadEmailNotAvailable(emailAddress);
        }


        public bool IsIgnoreStockLevels()
        {
            return _customerRepository.IsIgnoreStockLevels();
        }


        public void TryCreateEcommerceCustomerInfo()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            _customerRepository.TryCreateEcommerceCustomerInfo(customer.EMail, _appConfigService.WebSiteCode);
        }
    }
}