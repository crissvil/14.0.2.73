﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Tool;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class StringResourceService: ServiceBase, IStringResourceService
    {
        private readonly IAuthenticationService _authenticationService = null;
        private readonly IAppConfigService _appConfigService = null;

        public StringResourceService(IAuthenticationService authenticationService, IAppConfigService appConfigService)
        {
            _authenticationService = authenticationService;
            _appConfigService = appConfigService;
        }

        public string GetString(string key, bool returnTextOnly = false)
        {
            return GetString(key, String.Empty, returnTextOnly);
        }

        public string GetString(string key, string customLocalSettings, bool returnTextOnly = false)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();

            string localeSettings = customer.LocaleSetting;
            if (!customLocalSettings.IsNullOrEmptyTrimmed()) { localeSettings = customLocalSettings; }

            var resourceTable = AppLogic.StringResourceTable;
            if (_appConfigService.ShowStringResourceKeys)
            {
                return key;
            }

            if (resourceTable == null || resourceTable.Count == 0)
            {
                AppLogic.StringResourceTable = _appConfigService.GetAllStringResource();
            }

            if (resourceTable == null || resourceTable.Count == 0)
            {
                if (AppLogic.MailServer().Length != 0 &&
                    false == AppLogic.ro_TBD.Equals(AppLogic.MailServer(), StringComparison.InvariantCultureIgnoreCase))
                {
                    try
                    {
                        string Explanation = "This message means that your site is flushing application memory for some (unknown) reason. If you get this e-mail more then very rarely, it could be a big performance impact, and you should check with your hosting provider about this issue. Their server may be running low of RAM or something is causing their application asp.net memory caches to get flushed. This could cause your site to send up to 1000 database queries to the store on every single page load. If our store sends you an e-mail (which is VERY rare), it's not for a minor issue. First, please check with your hosting provider.";
                        AppLogic.SendMail(AppLogic.AppConfig("StoreName") + " string Table Empty Incident", "string Tables Empty, Reloaded at " + Localization.ToNativeDateTimeString(System.DateTime.Now) + ".<br/><br/>" + Explanation, false, AppLogic.AppConfig("MailMe_FromAddress"), AppLogic.AppConfig("MailMe_FromName"), AppLogic.AppConfig("MailMe_ToAddress"), AppLogic.AppConfig("MailMe_ToName"), string.Empty, AppLogic.MailServer());
                    }
                    catch { }
                }
                return key; // they don't even have this locale, return the key as a placeholder
            }

            var sr = resourceTable[localeSettings, key];
            if (sr == null)
            {
                return key;
            }
            else
            {
                //Handles to some pages when Customer has not been initialized
                if (customer != null)
                {
                    bool isEditingMode = customer.IsInEditingMode();
                    bool isAdminLoggedIn = _authenticationService.IsAdminCurrentlyLoggedIn();
                    bool isInMobileMode = CurrentContext.IsRequestingFromMobileMode(customer);

                    //when admin is not loggedin and user not in editing
                    if (returnTextOnly || (!isEditingMode && !isAdminLoggedIn))
                    {
                        return sr.ConfigValue;
                    }
                    //when admin is loggedin and user not in editing
                    else if (!isEditingMode && isAdminLoggedIn)
                    {
                        return sr.ConfigValue;
                    }
                    //when admin is not loggedin but user in editing
                    else if (isEditingMode && !isAdminLoggedIn)
                    {
                        return sr.ConfigValue;
                    }
                    else if (isInMobileMode)
                    {
                        return sr.ConfigValue;
                    }

                    return AppLogic.BuildCmsTemplate(key, sr.ConfigValue);
                }
                else
                {
                    return sr.ConfigValue;
                }
            }


        }
    }
}
