﻿using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;
using System;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class IOService : ServiceBase, IIOService
    {
        public string GetProductImageDirectory()
        {
            return "images/spacer.gif".ToMapPath().Replace("images\\spacer.gif", "images\\product");
        }

        public string GetManufacturerImageDirectory()
        {
            return "images/spacer.gif".ToMapPath().Replace("images\\spacer.gif", "images\\manufacturer");
        }

        public string GetCategoryImageDirectory()
        {
            return "images/spacer.gif".ToMapPath().Replace("images\\spacer.gif", "images\\category");
        }

        public string GetDepartmentImageDirectory()
        {
            return "images/spacer.gif".ToMapPath().Replace("images\\spacer.gif", "images\\department");
        }

        public string GetAttributeImageDirectory()
        {
            return "images/spacer.gif".ToMapPath().Replace("images\\spacer.gif", "images\\attribute");
        }

        public string GetCompanyLogoImageDirectory()
        {
            return "images/spacer.gif".ToMapPath().Replace("images\\spacer.gif", "images\\CompanyLogo");
        }

        public string GetMobileImagePath(string entityName, string size, bool fullPath)
        {
            string path = ("mobile/images/" + entityName + "/{0}/").FormatWith((size.Length > 0) ? size.ToLower() : String.Empty);
            if (fullPath)
            {
                path = path.ToMapPath();
            }
            return path;
        }

        public string GetImagePath(string entityName, string size, bool fullPath)
        {
            string path = ("images/" + entityName + "/{0}/").FormatWith((size.Length > 0) ? size.ToLower() : String.Empty);
            if (fullPath)
            {
                path = path.ToMapPath();
            }
            return path;
        }
    }
}