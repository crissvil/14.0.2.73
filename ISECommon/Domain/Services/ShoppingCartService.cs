﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class ShoppingCartService : ServiceBase, IShoppingCartService
    {
        private readonly IAuthenticationService _authenticationService;
        private readonly IShoppingCartRepository _shoppingCartRepository;
        private readonly IShippingService _shippingService;
        private readonly IAppConfigService _appConfigService;
        private readonly ICurrencyService _currencyService;
        private readonly INavigationService _navigationService;
        private readonly IStringResourceService _stringResourceService;
        private readonly IRequestCachingService _requestCachingService;
        private readonly ICryptographyService _cryptographyService;
        private readonly ICustomerService _customerService;

        public ShoppingCartService(IAuthenticationService authenticationService, 
                                   IShoppingCartRepository shoppingCartRepository,
                                   IAppConfigService appConfigService,
                                   ICurrencyService currencyService,
                                   IShippingRepository shippingRepository,
                                   INavigationService navigationService,
                                   IStringResourceService stringResourceService,
                                   IRequestCachingService requestCachingService,
                                   IShippingService shippingService,
                                   ICryptographyService cryptographyService,      
                                   ICustomerService customerService)
        {
            _authenticationService = authenticationService;
            _shoppingCartRepository = shoppingCartRepository;
            _appConfigService = appConfigService;
            _currencyService = currencyService;
            _navigationService = navigationService;
            _stringResourceService = stringResourceService;
            _requestCachingService = requestCachingService;
            _shippingService = shippingService;
            _cryptographyService = cryptographyService;
            _customerService = customerService;
        }

        public void SetItemQuantity(int cartRecordID, decimal quantity)
        {
            if (quantity == 0)
            {
                DeleteShoppingCartRecord(cartRecordID);
            }
            else
            {
                var customer = _authenticationService.GetCurrentLoggedInCustomer();
                _shoppingCartRepository.UpdateShoppingCartRecordQuantity(cartRecordID, quantity, customer.CustomerCode, customer.ContactCode);
            }
        }

        public void DeleteShoppingCartRecord(int cartRecordID)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            _shoppingCartRepository.DeleteShoppingCartRecord(cartRecordID, customer.CustomerCode, customer.ContactCode);
        }

        public void DeleteShoppingCartItemReservation(string itemCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            _shoppingCartRepository.DeleteShoppingCartItemReservation(itemCode, customer.ContactCode);
        }

        public EcommerceShoppingCartModel GetEcommerceShoppingCartByCartRecordId(int cartRecordID)
        { 
            return _shoppingCartRepository.GetEcommerceShoppingCartByShoppingCartRecId(cartRecordID);
        }

        public void ClearKitItems(string itemCode, Guid cartId)
        {
            var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            _shoppingCartRepository.ClearKitItems(thisCustomer.CustomerCode, itemCode, cartId);
        }

        public void ClearEcommerceCartShippingAddressIDForAnonCustomer()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            _shoppingCartRepository.ClearEcommerceCartShippingAddressIDForAnonCustomer(customer.CustomerCode, _appConfigService.WebSiteCode);
        }

        public void ClearLineItems(string[] arrayCardIds)
        {
            _shoppingCartRepository.ClearLineItemsKitCompositions(arrayCardIds);
            _shoppingCartRepository.ClearCartLineItems(arrayCardIds);
            _shoppingCartRepository.ClearCartCustomerReservation(_authenticationService.GetCurrentLoggedInCustomer().ContactCode);
        }

        public void ClearLineItemsAndKitComposition(string[] arrayCardIds)
        {
            _shoppingCartRepository.ClearLineItemsKitCompositions(arrayCardIds);
            _shoppingCartRepository.ClearCartLineItems(arrayCardIds);
        }

        public void ClearCartReservationByCurrentlyLoggedInCustomer()
        { 
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            _shoppingCartRepository.ClearCartCustomerReservation(customer.ContactCode);
        }

        public void ClearCartByLoggedInCustomerCartType(CartTypeEnum cartType)
        { 
            var currentCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            _shoppingCartRepository.ClearCart(cartType, currentCustomer.CustomerCode, currentCustomer.ContactCode);
        }

        #region Gift Email

        public IEnumerable<ShoppingCartGiftEmailCustomModel> GetShoppingCartGiftEmails()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _shoppingCartRepository.GetShoppingCartGiftEmails(customer.ContactCode, customer.LanguageCode);
        }

        public void CreateShoppingCartGiftEmail(int shoppingCartRecID, string itemCode, int lineNum, string email)
        {
            _shoppingCartRepository.CreateShoppingCartGiftEmail(shoppingCartRecID, itemCode, lineNum, email, _appConfigService.UserCode);
        }

        public void CreateShoppingCartGiftEmail(List<ShoppingCartGiftEmailCustomModel> giftEmails)
        {
            _shoppingCartRepository.CreateShoppingCartGiftEmail(giftEmails, _appConfigService.UserCode );
        }

        public void UpdateShoppingCartGiftEmail(int counter, string emailRecipient)
        {
            _shoppingCartRepository.UpdateShoppingCartGiftEmail(counter, emailRecipient, _appConfigService.UserCode);
        }

        public void UpdateShoppingCartGiftEmail(List<ShoppingCartGiftEmailCustomModel> giftEmails)
        {
            _shoppingCartRepository.UpdateShoppingCartGiftEmail(giftEmails, _appConfigService.UserCode);
        }

        public void DeleteShoppingCartGiftEmailTopRecords(int shoppingCartRecID, int topRecords)
        {
            _shoppingCartRepository.DeleteShoppingCartGiftEmailTopRecords(shoppingCartRecID, topRecords);
        }

        public void CleanupShoppingCartGiftEmail()
        {
            _shoppingCartRepository.CleanupShoppingCartGiftEmail();
        }

        public void CreateCustomerCartGiftEmail(string documentCode, List<ShoppingCartGiftEmailCustomModel> giftEmails)
        {
            _shoppingCartRepository.CreateCustomerCartGiftEmail(documentCode, giftEmails, _appConfigService.UserCode);
        }

        #endregion

        public InterpriseShoppingCart New(CartTypeEnum cartType)
        {
            return New(cartType, String.Empty, false, false, String.Empty);
        }

        public InterpriseShoppingCart New(CartTypeEnum cartType, bool loadFromDb)
        {
            return New(cartType, String.Empty, false, loadFromDb, String.Empty, String.Empty);
        }

        public InterpriseShoppingCart New(CartTypeEnum cartType, bool loadFromDb, string itemCode)
        {
            return New(cartType, String.Empty, false, loadFromDb, String.Empty, itemCode);
        }

        public InterpriseShoppingCart New(CartTypeEnum cartType, string originalRecurringOrderNumber, bool onlyLoadRecurringItemsThatAreDue, bool LoadDatafromDB, string pagename = "", string itemCode = "")
        {
            return new InterpriseShoppingCart(null, _appConfigService.DefaultSkinID, _authenticationService.GetCurrentLoggedInCustomer(), cartType, originalRecurringOrderNumber, onlyLoadRecurringItemsThatAreDue, LoadDatafromDB, pagename, itemCode);
        }

        public void SaveShippingInfoAndContinueCheckout(IEnumerable<CartShippingDataToSplit> cartShippingDataToSplit)
        {
            foreach (var item in cartShippingDataToSplit)
            {
                _shoppingCartRepository.UpdateShoppingCartInStoreShippingInfo(item.CartId, item.ShippingMethod, item.WarehouseCode, item.RealTimeRateGuid);

                if (item.IsRealTime)
                {
                    _shippingService.SetRealTimeRateRecord(item.ShippingMethod, item.Freight, item.RealTimeRateGuid.ToString(), true);
                }

            }

            _navigationService.NavigateToCheckOutPayment();
        }

        public IEnumerable<InterpriseShoppingCart> SplitShippingMethodsInMultipleOrders()
        {
            var currentCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            var orders = new List<InterpriseShoppingCart>();
            var shoppingCart = New(CartTypeEnum.ShoppingCart, true);

            #region "Create Order for Non-ShippableItems"

            var GetAllSpecialItems = shoppingCart.CartItems.Where(c => c.IsService || c.IsDownload)
                                                            .Select(c =>
                                                            {
                                                                var itemToReturn = c.Clone<CartItem>();
                                                                itemToReturn.ShippingMethod = AppLogic.AppConfig("ShippingMethodCodeZeroDollarOrder");
                                                                itemToReturn.ShippingAddressId = currentCustomer.PrimaryShippingAddress.AddressID;
                                                                return itemToReturn;
                                                            });

            if (GetAllSpecialItems.Count() > 0)
            {
                var cartWithNoShipping = New(CartTypeEnum.ShoppingCart);
                cartWithNoShipping.CartItems.AddRange(GetAllSpecialItems);
                cartWithNoShipping.MakeShippingNotRequired(false);
                orders.Add(cartWithNoShipping);
                shoppingCart.CartItems.RemoveAll(c => c.IsService || c.IsDownload);
            }

            #endregion

            foreach (var item in shoppingCart.CartItems)
            {
                InterpriseShoppingCart order = null;
                if (!item.InStoreWarehouseCode.IsNullOrEmptyTrimmed())
                {
                    order = orders.FirstOrDefault(o =>
                    {
                        return o.CartItems.Any(c => c.ShippingMethod.Trim().ToLowerInvariant() == item.ShippingMethod.Trim().ToLowerInvariant() &&
                                                 c.InStoreWarehouseCode.Trim().ToLowerInvariant() == item.InStoreWarehouseCode.Trim().ToLowerInvariant());
                    });
                }
                else
                {
                    order = orders.FirstOrDefault(o =>
                    {
                        return o.CartItems.Any(c => c.ShippingMethod == item.ShippingMethod);
                    });
                }

                if (order == null)
                {
                    var newCart = New(CartTypeEnum.ShoppingCart);
                    newCart.CartItems.Add(item.Clone());
                    orders.Add(newCart);
                }
                else
                {
                    order.CartItems.Add(item.Clone());
                }
            }

            return orders;
        }

        public void ClearCartWarehouseCodeByCustomer()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            string code = customer.GetValidCustomerCodeForShoppingCartRecord();
            _shoppingCartRepository.ClearCartWarehouseCodeByCustomer(code);
        }

        public void DoIsEmptyChecking(InterpriseShoppingCart cart)
        {
            if (cart.IsEmpty()) { _navigationService.NavigateToShoppingCartRestLinkBack(); }
        }

        public void DoIsEmptyTrimmedChecking(InterpriseShoppingCart cart)
        {
            if (cart.InventoryTrimmed) { _navigationService.NavigateToShoppingCartRestLinkBackWithErroMessage(
                                            _stringResourceService.GetString("shoppingcart.aspx.1", true)); 
            }
        }

        public void DoHasCouponAndIsCouponValidChecking(InterpriseShoppingCart cart)
        {
            string couponCode = String.Empty;
            string errorMessage = String.Empty;
            var customer = _authenticationService.GetCurrentLoggedInCustomer();

            if (cart.HasCoupon(ref couponCode) && !cart.IsCouponValid(customer, couponCode, ref errorMessage))
            {
                _navigationService.NavigateToUrl("shoppingcart.aspx?resetlinkback=1&discountvalid=false");
            }
        }

        public void DoMeetsMinOrderAmountChecking(InterpriseShoppingCart cart)
        {
            if (!cart.MeetsMinimumOrderAmount(_appConfigService.CartMinOrderAmount)) { _navigationService.NavigateToShoppingCartRestLinkBack(); }
        }

        public void DoMeetsMinOrderQuantityChecking(InterpriseShoppingCart cart)
        {
            if (!cart.MeetsMinimumOrderQuantity(_appConfigService.MinCartItemsBeforeCheckout)) { _navigationService.NavigateToShoppingCartRestLinkBack(); }
        }

        public void DoIsNoShippingRequiredAndCartHasMultipleShippingAddress(InterpriseShoppingCart cart)
        {
            if (!cart.IsNoShippingRequired() &&
                cart.HasMultipleShippingAddresses() &&
                cart.NumItems() <= AppLogic.MultiShipMaxNumItemsAllowed() &&
                cart.NumItems() > 1)
            {
                _navigationService.NavigateToCheckoutMult();
            }
        }

        public void UpdateAllocatedQty(decimal allocatedQty, string itemCode, string unitMeasureCode)
        {
            var currentCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            _shoppingCartRepository.UpdateAllocatedQty(allocatedQty, currentCustomer.ContactCode, itemCode, unitMeasureCode);
        }

        public void UpdateAllocatedQty(IEnumerable<CustomerSalesOrderDetailViewDTO> items)
        {
            var currentCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            _shoppingCartRepository.UpdateAllocatedQty(items, currentCustomer.ContactCode);
        }

        public bool HasNoStockAndNoOpenPOComponent(string itemCode, string itemId)
        {
            var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            bool isCurrencyIncludedForInventorySelling = InterpriseHelper.IsCurrencyIncludedForInventorySelling(thisCustomer.CurrencyCode);
            bool currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem =
            InterpriseHelper.CurrencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYet(thisCustomer.CurrencyCode, itemCode);

            return _shoppingCartRepository.HasNoStockAndNoOpenPOComponent(itemCode, (isCurrencyIncludedForInventorySelling && !currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem) ?
                                                                                     thisCustomer.CurrencyCode : Currency.GetHomeCurrency(),
                                                                                     thisCustomer.LocaleSetting, thisCustomer.CustomerCode, itemId, thisCustomer.ContactCode, _appConfigService.WebSiteCode);

        }

        public void CacheShoppingCartObject()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            InterpriseShoppingCart cart = null;

            if (_requestCachingService.Exist("Minicart"))
            {
                _requestCachingService.RemoveItem("Minicart");
            }

            cart = New(CartTypeEnum.ShoppingCart, true);

            // merge duplicate item/s
            if (cart.CartItems.Count > 0) { MergeDuplicateCartItems(cart); }

            cart.BuildSalesOrderDetails(false, true, customer.CouponCode, true);
            _requestCachingService.AddItem("Minicart", cart);
        }

        public string BuildMiniCart()
        {
            CacheShoppingCartObject();
            var cart = ServiceFactory.GetInstance<IRequestCachingService>()
                                     .GetItem<InterpriseShoppingCart>("Minicart");

            string output = String.Empty;

            var root = new XElement(DomainConstants.XML_ROOT_NAME);

            if (cart.IsEmpty())
            {
                string cartEmptyText = String.Empty;
                if (cart.CartType == CartTypeEnum.ShoppingCart)
                {
                    var t1 = new Topic("EmptyMinicartText", cart.ThisCustomer.LocaleSetting, cart.SkinID, null);
                    output = t1.Contents;
                }

                if (cart.CartType == CartTypeEnum.WishCart)
                {
                    var t1 = new Topic("EmptyWishListText", cart.ThisCustomer.LocaleSetting, cart.SkinID, null);
                    output = t1.Contents;
                }

                root.Add(new XElement("EMPTY_CART_TEXT", cartEmptyText));
            }
            else
            {
                var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();

                bool vatEnabled = _appConfigService.VATIsEnabled;
                bool vatInclusive = (cart.ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive);
                bool showPicsInMiniCart = _appConfigService.ShowPicsInMiniCart;
                bool showLinkBack = _appConfigService.LinkToProductPageInCart;
                bool isCouponTypeOrders = false;
                decimal computedSubtotal = Decimal.Zero;
                decimal appliedCouponDiscount = Decimal.Zero;
                var couponSetting = cart.GetCartCouponSetting(cart.ThisCustomer.CouponCode);

                if (cart.HasCoupon())
                { isCouponTypeOrders = (couponSetting.m_CouponType == CouponTypeEnum.OrderCoupon); }

                root.Add(new XElement("ISREGISTERED", thisCustomer.IsRegistered));
                root.Add(new XElement("VATINCLUSIVE", vatInclusive.ToStringLower()));
                root.Add(new XElement("VATENABLED", vatEnabled.ToStringLower()));
                root.Add(new XElement("HASCOUPON", cart.HasCoupon()));
                root.Add(new XElement("IS_COUPON_TYPE_ORDERS", isCouponTypeOrders));
                root.Add(new XElement("ISALLOWFRACTIONAL", AppLogic.IsAllowFractional.ToStringLower()));
                root.Add(new XElement("QUANTITYREGEX", AppLogic.GetQuantityRegularExpression(String.Empty, false)));
                root.Add(new XElement("SHOWSTOCKHINTS", _appConfigService.ShowStockHints.ToStringLower()));
                root.Add(new XElement("SHOWACTUALINVENTORY", _appConfigService.ShowActualInventory.ToStringLower()));
                root.Add(new XElement("IS_CART_HAS_STORE_PICKUP_ITEMS_OR_MULTIPLESHIPPING_METHOD", cart.HasMultipleShippingMethod()));
                root.Add(new XElement("IS_CART_HAS_OVERSIZED_ITEMS_WITH_PICKUP_SHIPPING_METHOD", cart.HasOverSizedItemWithPickupShippingMethod()));

                foreach (var cartItem in cart.CartItems)
                {
                    var xmlcartItem = new XElement("CART_ITEMS");
                    xmlcartItem.Add(new XElement("CART_ITEM_ID", cartItem.m_ShoppingCartRecordID));
                    xmlcartItem.Add(new XElement("ITEMTYPE", cartItem.ItemType));
                    xmlcartItem.Add(new XElement("ISOUTOFSTOCK", cartItem.IsOutOfStock.ToStringLower()));
                    xmlcartItem.Add(new XElement("CARTSTATUS", cartItem.Status));
                    xmlcartItem.Add(new XElement("POSTATUS", cartItem.POStatus));
                    xmlcartItem.Add(new XElement("ISDROPSHIP", cartItem.IsDropShip));
                    xmlcartItem.Add(new XElement("ISSPECIALORDER", cartItem.IsSpecialOrder));
                    xmlcartItem.Add(new XElement("ISOVERSIZED", cartItem.IsOverSized));
                    xmlcartItem.Add(new XElement("ISPREPACKED", cartItem.IsPrePacked));
                    xmlcartItem.Add(new XElement("ARTCOSTITEM", AppLogic.GetArtCostItemCode()));
                    xmlcartItem.Add(new XElement("ISFEATURED", cartItem.IsFeatured));

                    string productLinkHref = InterpriseHelper.MakeItemLink(cartItem.ItemCode);
                    string displayName = cartItem.DisplayName.ToHtmlEncode();
                    xmlcartItem.Add(new XElement("PRODUCTLINKNAME", displayName));
                    xmlcartItem.Add(new XElement("LINKBACK", showLinkBack.ToStringLower()));
                    xmlcartItem.Add(new XElement("ISCHECKOUTOPTION", cartItem.IsCheckoutOption.ToStringLower()));

                    //Add this item to the root.
                    root.Add(xmlcartItem);

                    switch (cartItem.ItemType)
                    {
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP:

                        #region "ITEM_TYPE_MATRIX_ITEM"
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM:
                            var matrixInfo = ServiceFactory.GetInstance<IProductService>()
                                                           .GetMatrixItemInfo(cartItem.ItemCode);
                            if (matrixInfo != null)
                            {
                                productLinkHref = InterpriseHelper.MakeItemLink(matrixInfo.ItemCode);
                                productLinkHref = CommonLogic.QueryStringSetParam(productLinkHref, DomainConstants.QUERY_STRING_KEY_MATRIX_ID, matrixInfo.Counter.ToString());
                            }

                            if (showLinkBack && !cartItem.IsCheckoutOption)
                            {
                                xmlcartItem.Add(new XElement("ISCHECKOUTOPTION", false));
                                xmlcartItem.Add(new XElement("PRODUCTLINKHREF", productLinkHref));
                            }

                            bool hasRegistryItem = (cartItem.RegistryItemCode.HasValue && cartItem.GiftRegistryID.HasValue);
                            xmlcartItem.Add(new XElement("ISREGISTRYITEM", hasRegistryItem.ToStringLower()));

                            break;

                        #endregion

                        #region "STOCK"
                        default:
                            if (showLinkBack)
                            {
                                xmlcartItem.Add(new XElement("PRODUCTLINKHREF", productLinkHref));

                                hasRegistryItem = (cartItem.RegistryItemCode.HasValue && cartItem.GiftRegistryID.HasValue);
                                xmlcartItem.Add(new XElement("ISREGISTRYITEM", hasRegistryItem.ToStringLower()));
                            }
                            break;
                        #endregion
                    }

                    #region "Product Picture"

                    if (showPicsInMiniCart)
                    {
                        xmlcartItem.Add(new XElement("SHOWPICSINMINICART", true));
                        var img = ProductImage.Locate("product", cartItem.ItemCounter, "minicart");
                        if (null != img)
                        {
                            string seTitle = "";
                            string seAltText = "";
                            string itemCode = itemCode = InterpriseHelper.GetInventoryItemCode(cartItem.ItemCounter);
                            AppLogic.GetSEImageAttributes(itemCode, "MINICART", AppLogic.GetLanguageCode(cart.ThisCustomer.LocaleSetting), ref seTitle, ref seAltText);

                            xmlcartItem.Add(new XElement("PRODUCTIMAGEPATH", img.src));
                            xmlcartItem.Add(new XElement("PRODUCTIMAGETITLE", seTitle));
                            xmlcartItem.Add(new XElement("PRODUCTIMAGEALT", seAltText));
                        }
                    }

                    #endregion

                    xmlcartItem.Add(new XElement("HAS_MULTIPLE_ADDRESSES", cart.HasMultipleShippingAddresses().ToStringLower()));
                    xmlcartItem.Add(new XElement("FREE_STOCK", cartItem.FreeStock));

                    #region "Input Quantities and Delete"

                    xmlcartItem.Add(new XElement("ISRESTRICTEDQUANTITIES", (cartItem.RestrictedQuantities.Count > 0)));
                    if (cartItem.RestrictedQuantities.Count > 0)
                    {
                        xmlcartItem.Add(new XElement("QUANTITYLISTID", cartItem.m_ShoppingCartRecordID));
                        xmlcartItem.Add(cartItem.RestrictedQuantities
                                    .Select(r => new XElement("RESTRICTEDQUANTITIES",
                                            new XElement("QTY", r),
                                            new XElement("SELECTED", r == cartItem.m_Quantity))));
                    }
                    else
                    {
                        xmlcartItem.Add(new XElement("INPUTQUANTITYID", "Quantity_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                    }

                    xmlcartItem.Add(new XElement("INPUTQUANTITYVALUE", Localization.ParseLocaleDecimal(cartItem.m_Quantity, cart.ThisCustomer.LocaleSetting)));

                    if (_appConfigService.ShowCartDeleteItemButton)
                    {
                        xmlcartItem.Add(new XElement("SHOWCARTDELETEITEMBUTTON", true));
                    }

                    decimal minimumOrderQty = cartItem.m_MinimumQuantity / cartItem.UnitMeasureQty;
                    if (!AppLogic.IsAllowFractional) { minimumOrderQty = Math.Ceiling(minimumOrderQty); }

                    xmlcartItem.Add(new XElement("MINORDERQUANTITYID", "MinOrderQuantity_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                    xmlcartItem.Add(new XElement("MINORDERQUANTITYNAME", "MinOrderQuantity_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                    xmlcartItem.Add(new XElement("MINORDERQUANTITYVALUE", minimumOrderQty));

                    #endregion

                    #region "Subtotal"

                    decimal originalPrice = cartItem.Price.ToCustomerRoundedCurrency();
                    if (vatEnabled && vatInclusive)
                    {
                        originalPrice = (cartItem.Price - cartItem.TaxRate).ToCustomerRoundedCurrency();
                    }
                    // with tax
                    decimal extPrice = cartItem.Price;
                    //added logic to display the non discounted price
                    if (isCouponTypeOrders)
                    {
                        extPrice = originalPrice;
                    }

                    decimal vat = cartItem.TaxRate;

                    //add this if there are coupon
                    decimal coupondiscount = Decimal.Zero;
                    if (!cart.ThisCustomer.CouponCode.IsNullOrEmptyTrimmed())
                    {
                        //original amount (non discounted) - discounted extended price
                        coupondiscount = (cartItem.DiscountAmountAlreadyComputed) ? cartItem.CouponDiscount : cart.GetCartItemCouponDiscount(cartItem);
                        //decimal discountAmount = (coupondiscount == Decimal.Zero) ? coupondiscount : originalPrice - coupondiscount;

                        string discountType = couponSetting.m_DiscountType.ToString();
                        decimal percentageDiscount = decimal.Zero;
                        discountType = (couponSetting.m_DiscountType == CouponDiscountTypeEnum.AmountDiscount) ? Interprise.Framework.Customer.Shared.Const.COUPON_DISCOUNTTYPE_AMOUNT : Interprise.Framework.Customer.Shared.Const.COUPON_DISCOUNTTYPE_PERCENT;

                        if (discountType == Interprise.Framework.Customer.Shared.Const.COUPON_DISCOUNTTYPE_PERCENT)
                        {
                            percentageDiscount = (coupondiscount == Decimal.Zero) ? coupondiscount : couponSetting.m_DiscountPercent;
                        }

                        appliedCouponDiscount += coupondiscount;

                        if (!isCouponTypeOrders)
                        {
                            extPrice -= coupondiscount;
                        }
                    }

                    // subtotal for order
                    computedSubtotal += extPrice;

                    // set flags for computaion
                    cartItem.PriceAndTaxAlreadyComputed = true;
                    cartItem.UnitPriceAlreadyComputed = true;
                    cartItem.DiscountAmountAlreadyComputed = true;

                    // recalculate tax rate for item based on discounted price
                    if (coupondiscount > Decimal.Zero && vat > Decimal.Zero)
                    {
                        // for computation in summary
                        //cartItem.TaxRate = (extPrice * (vat / cartItem.Price.ToCustomerRoundedCurrency())).ToCustomerRoundedCurrency();
                        cartItem.TaxRate = (extPrice * (vat / Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(cartItem.Price)));
                        // for display
                        vat = cartItem.TaxRate;
                    }

                    xmlcartItem.Add(new XElement("PRICEFORMATTED", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(extPrice).ToCustomerCurrency()));

                    if (vatEnabled)
                    {
                        xmlcartItem.Add(new XElement("TAX_RATE_VALUE", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(vat).ToCustomerCurrency()));
                    }
                }
                    #endregion

                //TOTAL COMPUTATION ------------------------------------------------------------------------------------//

                decimal subTotal = computedSubtotal;

                if (isCouponTypeOrders)
                {
                    //if the condition below is satified, computedSubtotal will be used to reflect the applied coupon discount 
                    if (subTotal.Equals(Decimal.Zero) && appliedCouponDiscount > subTotal)
                    {
                        appliedCouponDiscount = computedSubtotal;
                    }
                    else
                    {
                        appliedCouponDiscount = computedSubtotal - cart.GetCouponDiscountedAmount(subTotal);
                        subTotal -= appliedCouponDiscount;
                    }
                    subTotal += appliedCouponDiscount;

                    //total coupon discount applied to the web order
                    root.Add(new XElement("APPLIED_COUPON_DISCOUNT", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(appliedCouponDiscount).ToCustomerCurrency()));
                }                
                
                decimal tax = Decimal.Zero;
                decimal total = Decimal.Zero;

                if (Customer.Current.IsRegistered)
                {
                    //add tax if Inclusive
                    tax = (vatEnabled && vatInclusive) ? Decimal.Zero : Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(cart.GetCartTaxTotal());
                }

                // add tax for final total
                total = subTotal + tax;

                if (isCouponTypeOrders)
                {
                    total -= appliedCouponDiscount;
                }

                string subTotalFormatted = Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(subTotal).ToCustomerCurrency();
                root.Add(new XElement("SUBTOTAL_VALUE", subTotalFormatted));

                root.Add(new XElement("TAX_RATE_VALUE", tax.ToCustomerCurrency()));
                root.Add(new XElement("TOTAL", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(total).ToCustomerCurrency()));

                var xmlpackage = new XmlPackage2("page.minicart.xml.config", root);
                output = xmlpackage.TransformString();
                
            }
            return output;
        }

        public void UpdateShoppingCartInStoreShippingInfo(int shoppingCartRecId, string warehouseCode, Guid realTimeRateGuid)
        {
            string pickupShippingMethodDescription = _shippingService.GetStorePickupShippingMethodDescription();
            _shoppingCartRepository.UpdateShoppingCartInStoreShippingInfo(shoppingCartRecId, pickupShippingMethodDescription, warehouseCode, realTimeRateGuid);
        }

        public bool IsCartHasGiftRegistryItem()
        {
            return _shoppingCartRepository.IsCartHasGiftRegistryItem(_authenticationService.GetCurrentLoggedInCustomer()
                                                                                           .GetValidCustomerCodeForShoppingCartRecord());
        }

        public bool IsCartHasStorePickupItem()
        {
            return _shoppingCartRepository.IsCartHasStorePickupItem(_authenticationService.GetCurrentLoggedInCustomer()
                                                                                           .GetValidCustomerCodeForShoppingCartRecord());
        }

        public bool UpdateInStorePickupCartItemStock()
        {
            bool hasChanged = false;
            if (_appConfigService.LimitCartToQuantityOnHand)
            {
                var customer = _authenticationService.GetCurrentLoggedInCustomer();
                string customerCode = customer.GetValidCustomerCodeForShoppingCartRecord();
                string contactCode = customer.ContactCode;
                string websiteCode = _appConfigService.WebSiteCode;
                hasChanged = _shoppingCartRepository.UpdateInStorePickupCartItemStock(customerCode, contactCode, websiteCode) > 0;
            }
            return hasChanged;
        }

        public void CheckStockAvailabilityDuringCheckout(bool isOutOfStockAndPhaseOut, bool isOutOfStockAndWithoutOpenPO)
        {
            if ((_appConfigService.HideOutOfStockProducts && isOutOfStockAndWithoutOpenPO) || isOutOfStockAndPhaseOut)
            {
                _navigationService.NavigateToShoppingCartRestLinkBack();
            }
        }

        #region Credit Memos, Loyalty Points, Gift Codes
        public bool IsValidGiftCode(string giftCode)
        {
            var giftCodes = _shoppingCartRepository.GetGiftCodeDetail(giftCode)
                                                   .Select(code =>  
                                                   { 
                                                       code.CreditAvailableFormatted = _currencyService.FormatCurrency(code.CreditAvailable);
                                                       return code; 
                                                   })
                                                   .ToList();
            if (giftCodes.Count == 0) { return false; }

            //check if giftcert and remaining balance is zero
            if (giftCodes.First().Type.EqualsIgnoreCase(Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CERTIFICATE))
            {
                decimal balance = giftCodes.Sum(x => x.CreditAvailable);
                if (balance == 0) { return false; }
            }

            //get any activated giftcodes
            var activated = giftCodes.Where(x => x.IsActivated).ToList();
            if (activated.Count > 0)
            {
                //check if customer owns giftcode
                var customer = _authenticationService.GetCurrentLoggedInCustomer();
                return activated.Any(x => x.BillToCode == customer.CustomerCode);
            }
            return true;
        }
        public bool IsGiftCodeOwnedByCustomer(string giftCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _shoppingCartRepository.GetGiftCodeDetail(giftCode)
                                          .Any(x => x.BillToCode == customer.CustomerCode);
        }
        public IEnumerable<GiftCodeCustomModel> GetAdditionalGiftCodes()
        {
            var giftCodes = new List<GiftCodeCustomModel>();
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            var separator = new Char[] { DomainConstants.COMMA_DELIMITER };
            var codes = customer.ThisCustomerSession[DomainConstants.GIFTCODES_ADDITIONAL]
                                         .ToString()
                                         .Split(separator, StringSplitOptions.RemoveEmptyEntries);

            if (codes.Length > 0)
            {
                giftCodes = _shoppingCartRepository.GetGiftCodesDetail(codes)
                                                   .GroupBy(code => new { code.SerialCode, code.Type })
                                                    .Select(group => new GiftCodeCustomModel
                                                    {
                                                        SerialCode = group.Key.SerialCode,
                                                        Type = group.Key.Type,
                                                        CreditAvailable = group.Sum(x => x.CreditAvailable),
                                                        CreditAvailableRate = group.Sum(x => x.CreditAvailableRate),
                                                        CreditAllocatedRate = group.Sum(x => x.CreditAllocatedRate),
                                                        CreditReservedRate = group.Sum(x => x.CreditReservedRate),
                                                        CreditAmountRate = group.Sum(x => x.CreditAmountRate),
                                                        CreditAvailableFormatted = _currencyService.FormatCurrency(group.Sum(x => x.CreditAvailable))
                                                    })
                                                    .ToList();
            }

            return giftCodes;
        }
        public IEnumerable<GiftCodeCustomModel> GetAppliedGiftCodes(bool withGrouping)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            var giftCodes = new List<GiftCodeCustomModel>();

            try
            {
                string json = _shoppingCartRepository.GetGiftCode(customer.ContactGUID);
                if (!json.IsNullOrEmptyTrimmed())
                {
                    var appliedGiftCodes = _cryptographyService.DeserializeJson<List<GiftCodeCustomModel>>(json)
                                                               .Where(x => x.AmountApplied > Decimal.Zero);

                    var existingGiftCodes = new List<GiftCodeCustomModel>();
                    existingGiftCodes.AddRange(_customerService.GetCustomerGiftCodes());
                    existingGiftCodes.AddRange(GetAdditionalGiftCodes());


                    var codes = appliedGiftCodes.Select(x => x.SerialCode).ToArray();
                    giftCodes = _shoppingCartRepository.GetGiftCodesDetail(codes)
                                                       .Where(x => existingGiftCodes.Exists(y => y.SerialCode == x.SerialCode))
                                                       .ToList();

                    if (withGrouping)
                    {
                        giftCodes = giftCodes.GroupBy(code => new { code.SerialCode, code.Type })
                                             .Select(group => new GiftCodeCustomModel
                                             {
                                                 SerialCode = group.Key.SerialCode,
                                                 Type = group.Key.Type,
                                                 CreditAvailable = group.Sum(x => x.CreditAvailable),
                                                 CreditAvailableRate = group.Sum(x => x.CreditAvailableRate),
                                                 CreditAllocatedRate = group.Sum(x => x.CreditAllocatedRate),
                                                 CreditReservedRate = group.Sum(x => x.CreditReservedRate),
                                                 CreditAmountRate = group.Sum(x => x.CreditAmountRate),
                                                 CreditAvailableFormatted = _currencyService.FormatCurrency(group.Sum(x => x.CreditAvailable)),
                                                 AmountApplied = appliedGiftCodes.Where(x => x.SerialCode == group.Key.SerialCode)
                                                                                 .Sum(x => x.AmountApplied)
                                             })
                                            .ToList();
                    }
                    else
                    {
                        // note: when a serialcode is recharged, it will create a new creditcode but with same serialcode
                        // that is why we need compute/allocate amount applied for each creditcode of same serialcode
                        giftCodes = giftCodes.Select(x =>
                        {
                            decimal remaining = appliedGiftCodes.Where(y => y.SerialCode == x.SerialCode).Sum(y => y.AmountApplied);
                            decimal applied = (remaining >= x.CreditAvailable) ? x.CreditAvailable : remaining;
                            x.AmountApplied = applied;
                            foreach (var code in appliedGiftCodes.Where(y => y.SerialCode == x.SerialCode)) { code.AmountApplied -= applied; }
                            return x;
                        }).ToList();
                    }
                }
            }
            catch
            {

            }
            return giftCodes;
        }
        public IEnumerable<CustomerCreditCustomModel> GetAppliedCreditMemos()
        {
            var creditMemos = new List<CustomerCreditCustomModel>();
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            try
            {
                string creditCodesJSON = _shoppingCartRepository.GetAppliedCreditCodesJSON(customer.ContactGUID);
                if (!creditCodesJSON.IsNullOrEmptyTrimmed())
                {
                    creditMemos = _cryptographyService.DeserializeJson<List<CustomerCreditCustomModel>>(creditCodesJSON)
                                                      .Where(x => x.CreditAppliedInShoppingCart > Decimal.Zero)
                                                      .ToList();
                }
            }
            catch
            {
            }
            return creditMemos;
        }
        public decimal GetAppliedLoyaltyPoints()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return customer.ThisCustomerSession[DomainConstants.LOYALTY_POINTS]
                           .ToDecimal();
        }
        public void AddAdditionalGiftCode(string giftCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            var separator = new Char[] { DomainConstants.COMMA_DELIMITER };
            var codes = customer.ThisCustomerSession[DomainConstants.GIFTCODES_ADDITIONAL]
                                         .ToString()
                                         .Split(separator, StringSplitOptions.RemoveEmptyEntries)
                                         .ToList();
            codes.Add(giftCode);
            customer.ThisCustomerSession[DomainConstants.GIFTCODES_ADDITIONAL] = String.Join(",", codes.ToArray());
        }
        public void RemoveAdditionalGiftCode(string giftCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            var separator = new Char[] { DomainConstants.COMMA_DELIMITER };
            var codes = customer.ThisCustomerSession[DomainConstants.GIFTCODES_ADDITIONAL]
                                         .ToString()
                                         .Split(separator, StringSplitOptions.RemoveEmptyEntries)
                                         .ToList();
            codes.Remove(giftCode);
            customer.ThisCustomerSession[DomainConstants.GIFTCODES_ADDITIONAL] = String.Join(",", codes.ToArray());
        }

        public void ApplyGiftCodes(string giftCodesSerialized)
        {
            // if in case the ecommercecustomer table is cleaned-up
            _customerService.TryCreateEcommerceCustomerInfo();

            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            _shoppingCartRepository.UpdateGiftCode(customer.ContactGUID, giftCodesSerialized);
        }
        public void ApplyLoyaltyPoints(string points)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            customer.ThisCustomerSession.SetVal(DomainConstants.LOYALTY_POINTS, points);
        }
        public void ApplyCreditMemos(string creditCodesJSON)
        {
            // if in case the ecommercecustomer table is cleaned-up
            _customerService.TryCreateEcommerceCustomerInfo();

            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            _shoppingCartRepository.UpdateAppliedCreditCodesJSON(creditCodesJSON, customer.ContactGUID);
        }
        
        public void ClearAppliedGiftCodes()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            customer.ThisCustomerSession.ClearVal(DomainConstants.GIFTCODES_ADDITIONAL);
            _shoppingCartRepository.ClearGiftCode(customer.ContactGUID);
        }
        public void ClearAppliedLoyaltyPoints()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            customer.ThisCustomerSession.ClearVal(DomainConstants.LOYALTY_POINTS);
        }
        public void ClearAppliedCreditMemos()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            _shoppingCartRepository.ClearAppliedCreditCodesJSON(customer.ContactGUID);
        }
        public void DoHasAppliedInvalidGiftCodesChecking(InterpriseShoppingCart cart)
        {
            if (cart.HasAppliedInvalidGiftCode())
            {
                if (_appConfigService.CheckoutUseOnePageCheckout)
                {
                    _navigationService.NavigateToCheckout1();
                }
                else
                {
                    _navigationService.NavigateToCheckOutPayment();
                }
            }
        }
        public void DoHasAppliedInvalidLoyaltyPointsChecking(InterpriseShoppingCart cart)
        {
            if (cart.HasAppliedInvalidLoyaltyPoints())
            {
                if (_appConfigService.CheckoutUseOnePageCheckout)
                {
                    _navigationService.NavigateToCheckout1();
                }
                else
                {
                    _navigationService.NavigateToCheckOutPayment();
                }
            }
        }
        public void DoHasAppliedInvalidCreditMemosChecking(InterpriseShoppingCart cart)
        {
            if (cart.HasAppliedInvalidCreditMemo())
            {
                if (_appConfigService.CheckoutUseOnePageCheckout)
                {
                    _navigationService.NavigateToCheckout1();
                }
                else
                {
                    _navigationService.NavigateToCheckOutPayment();
                }
            }
        }

        #endregion       
       
        public void DoRecomputeCartItemsPrice()
        {
            var cart = New(CartTypeEnum.ShoppingCart, true);
            cart.CartItems.ForEach(x => x.RecomputeCartItemPrice());
        }    

        public void DeleteEcommerceCustomerCartRecord(int cartType, DateTime ageDate)
        {
            var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            if (thisCustomer.CustomerID.Length > 0)
            {
                _shoppingCartRepository.DeleteEcommerceCustomerCartRecord(thisCustomer.CustomerID, _appConfigService.WebSiteCode, cartType, ageDate);
            }
        }

        public void MergeDuplicateCartItems(InterpriseShoppingCart cart)
        {
            var lstOfDuplicateItem = cart.CartItems.Where(c => c.ItemType != Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                                                     .GroupBy(x => new { x.ItemCode, x.UnitMeasureCode, x.m_ShippingAddressID, x.ItemCounter })
                                                     .Select(cartItemGroup =>
                                                      new
                                                      {
                                                          ItemCode = cartItemGroup.Key,
                                                          Count = cartItemGroup.Count(),
                                                          TotalQuantity = cartItemGroup.Sum(item => item.m_Quantity)
                                                      })
                                                    .Where(itemGroup => itemGroup.Count > 1).ToList();

            foreach (var duplicateItem in lstOfDuplicateItem)
            {
                var currentCartItems = cart.CartItems.Select(item => item).Where(item =>
                            item.ItemCode == duplicateItem.ItemCode.ItemCode &&
                            item.UnitMeasureCode == duplicateItem.ItemCode.UnitMeasureCode &&
                            item.m_ShippingAddressID == duplicateItem.ItemCode.m_ShippingAddressID &&
                            item.ItemCounter == duplicateItem.ItemCode.ItemCounter).ToList();

                foreach (var currentCartItem in currentCartItems)
                {
                    if (currentCartItems.IndexOf(currentCartItem) == 0)
                    {
                        cart.SetItemQuantity(currentCartItem.m_ShoppingCartRecordID, duplicateItem.TotalQuantity);
                        continue;
                    }
                    cart.RemoveItem(currentCartItem.m_ShoppingCartRecordID);
                }
            }
        }

        public void UpdateAllocatedQuantities(CartItemCollection cartItems)
        {
            bool ignoreStockLevel = _customerService.IsIgnoreStockLevels();
            var reservations = new List<ItemReservationCustomModel>();
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            
            // allocate available freestock per line item in shoppingcart
            foreach (var item in cartItems)
            {
                decimal freeStockBase = Math.Ceiling(item.FreeStock * item.UnitMeasureQty);
                decimal freeStockAllocated = reservations.Where(x => x.ItemCode == item.ItemCode)
                                                         .Select(x => x.AllocatedQty * x.UnitMeasureQty)
                                                         .Sum();

                decimal freeStockAvailable = Math.Floor((freeStockBase - freeStockAllocated) / item.UnitMeasureQty);
                decimal freeStockAllocation = item.m_Quantity;

                if (!ignoreStockLevel) 
                {
                    if (freeStockAvailable > Decimal.Zero)
                    {
                        freeStockAllocation = (freeStockAvailable > item.m_Quantity) ? item.m_Quantity : freeStockAvailable;
                    }
                    else
                    {
                        freeStockAllocation = Decimal.Zero;
                    }
                }

                var reservation = new ItemReservationCustomModel()
                {
                    CartRecordID = item.m_ShoppingCartRecordID,
                    ItemCode = item.ItemCode,
                    UnitMeasureCode = item.UnitMeasureCode,
                    UnitMeasureQty = item.UnitMeasureQty,
                    AllocatedQty = freeStockAllocation
                };
                reservations.Add(reservation);
            }

            if (reservations.Count > 0)
            {
                // batch update shoppingcart item's allocatedqty...
                _shoppingCartRepository.UpdateAllocatedQuantities(reservations, customer.ContactCode, customer.CustomerCode, _appConfigService.WebSiteCode);
            }
        }


        public IEnumerable<ItemReservationCustomModel> GetItemReservations()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _shoppingCartRepository.GetItemReservation(customer.ContactCode, customer.CustomerCode, _appConfigService.WebSiteCode).ToList();
        }
    }
}