﻿using System;
using System.Collections;
using System.Drawing;
using System.IO;
using System.Linq;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.Domain.Services
{
    public class ImageService : ServiceBase, IIMageService
    {
        IIOService _ioService = null;
        IAppConfigService _appconfigService = null;
        IGraphicService _graphicService = null;
        IAuthenticationService _authenticationService = null;

        public ImageService(IIOService ioService, 
                            IAppConfigService appconfigService,
                            IGraphicService graphicService,
                            IAuthenticationService authenticationService)
        {
            _ioService = ioService;
            _appconfigService = appconfigService;
            _graphicService = graphicService;
            _authenticationService = authenticationService;
        }

        public void MakeMobilePicture(string imgEntityType, string imgFileName, Image origPhoto, string fileExt, string customDirectory)
        {
            //if mobile folder does not exist, just create it
            string path = _ioService.GetMobileImagePath(imgEntityType, customDirectory, true);

            var mobilePicFolder = new DirectoryInfo(path);
            if (!mobilePicFolder.Exists) { mobilePicFolder.Create(); }

            int? defaultWidth;
            int? defaultHeight;
            string[] imageSizeArr = null;
            string configValue = string.Empty;
            string mime = string.Empty;

            if (customDirectory == "micro")
            {
                defaultWidth = 40;
                defaultHeight = 40;
                configValue = _appconfigService.MicroStyle;
            }
            else
            {
                defaultWidth = 79;
                defaultHeight = 101;
                configValue = _appconfigService.MobileImageStyle;
            }

            imageSizeArr = configValue.Split(';')
                                      .Where(item => !item.IsNullOrEmptyTrimmed())
                                      .ToArray();

            if (imageSizeArr.Length > 1)
            {
                var hashTable = new Hashtable();
                imageSizeArr.ForEach(val => hashTable.Add(val.Split(':')[0], val.Split(':')[1]));

                defaultWidth = hashTable["width"].TryParseInt();
                defaultHeight = hashTable["height"].TryParseInt();
                if (hashTable.ContainsKey("mime")) mime = hashTable["mime"].ToString();
            }

            if (!defaultWidth.HasValue || defaultWidth < 1) { defaultWidth = 79; }
            if (!defaultHeight.HasValue || defaultHeight < 1) { defaultHeight = 101; }
            if (mime.IsNullOrEmptyTrimmed()) { mime = fileExt; }

            switch (mime)
            {
                case "gif":
                    mime = ".gif";
                    break;
                case "png":
                    mime = ".png";
                    break;
                case "jpg":
                default:
                    mime = ".jpg";
                    break;
            }

            string newImageName = mobilePicFolder + imgFileName + mime;

            using (var photo = origPhoto.Clone() as System.Drawing.Image)
            {
                _graphicService.Resize(photo, newImageName, defaultWidth.Value, defaultHeight.Value);
            }
        }

        public void MakeMinicartPicture(string imgEntityType, string imgFileName, Image origPhoto, string fileExt)
        {
            //if micro folder does not exist, just create it  
            var minicartFolder = new DirectoryInfo(_ioService.GetImagePath("Product", "minicart", true));
            if (!minicartFolder.Exists) { minicartFolder.Create(); }

            var splitConfig = _appconfigService.MiniCartStyle.SplitToHash();

            int microWidth = 50;
            int microHeight = 50;

            string microMIME = string.Empty;

            if (splitConfig.ContainsKey("width"))
                microWidth = Int32.Parse(splitConfig["width"].ToString());
            if (splitConfig.ContainsKey("height"))
                microHeight = Int32.Parse(splitConfig["height"].ToString());
            if (splitConfig.ContainsKey("mime"))
                microMIME = splitConfig["mime"].ToString();

            if (microMIME.IsNullOrEmptyTrimmed()) { microMIME = fileExt; }

            if (microHeight < 1)
                microHeight = 50;
            if (microWidth < 1)
                microWidth = 50;

            switch (microMIME)
            {
                case "png":
                    microMIME = ".png";
                    break;
                case "jpg":
                default:
                    microMIME = ".jpg";
                    break;
            }

            string newImageName = minicartFolder + imgFileName + microMIME;
            //Clone the image to avoid disposing from the other methods
            using (var image = origPhoto.Clone() as System.Drawing.Image)
            {
                _graphicService.Resize(image, newImageName, microWidth, microHeight);
            }
        }

        public string GetProductImageUrlByItemCode(string itemCode, ImageSizeTypes size)
        {
            string retVal = String.Empty;
            var images = ProductImage.LocateDefaultImageInSizes("product", itemCode);

            if (images.Count() > 0)
            {
                var iconImage = images.FirstOrDefault(img => img.ImageSizeType == ImageSizeTypes.icon);
                retVal = iconImage.src;
            }

            return retVal;
        }

    }
}