﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using InterpriseSuiteEcommerceCommon.DTO;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class PaymentTermService : ServiceBase, IPaymentTermService
    {
        IPaymentTermRepository _paymentTermRepository = null;
        IAppConfigService _appConfigService = null;
        IGatewayRepository _gatewayRepository = null;
        IAuthenticationService _authenticationService = null;
        IHttpContextService _httpContextService = null;

        public PaymentTermService(IPaymentTermRepository paymentTermRepository,
                                  IAppConfigService appConfigService,
                                  IGatewayRepository gatewayRepository,
                                  IAuthenticationService authenticationService,
                                  IHttpContextService httpContextService)
        {
            _paymentTermRepository = paymentTermRepository;
            _appConfigService = appConfigService;
            _gatewayRepository = gatewayRepository;
            _authenticationService = authenticationService;
            _httpContextService = httpContextService;
        }

        public void EnsureNoPaymentTermRequiredIsExisting()
        {
            string noPaymentPaymentTermCode = _appConfigService.PaymentTermCodeZeroDollarOrder;
            if (noPaymentPaymentTermCode.IsNullOrEmptyTrimmed())
            {
                throw new Exception("PaymentTermCodeZeroDollarOrder AppConfig is not configured. Please setup a dummy payment term code to mark orders that doesn't require payment!!!");
            }

            bool noPaymentTermRequiredPaymentTermExisting = _paymentTermRepository.NoPaymentTermRequiredPaymentTermExisting(noPaymentPaymentTermCode);
            if (!noPaymentTermRequiredPaymentTermExisting)
            {
                throw new Exception(string.Format("PaymentTermCodeZeroDollarOrder AppConfig:\"{0}\" is not existing in the database!!!", noPaymentPaymentTermCode));
            }
        }

        public void EnsureNoPaymentTermRequiredIsAssociatedWithCurrentCustomer(string currentCustomerPaymentTermGroup)
        {
            string noPaymentPaymentTermCode = _appConfigService.PaymentTermCodeZeroDollarOrder;
            bool noPaymentTermRequiredExistingAndIsAssociatedWithCustomerPaymentTermGroup = false;

            noPaymentTermRequiredExistingAndIsAssociatedWithCustomerPaymentTermGroup = 
                _paymentTermRepository.CheckPaymentTermCodeInSystemPaymentTermGroupDetail(noPaymentPaymentTermCode, currentCustomerPaymentTermGroup);

            if (!noPaymentTermRequiredExistingAndIsAssociatedWithCustomerPaymentTermGroup)
            {
                //this is dynamic facade that can contain any dataset
                Interprise.Facade.Base.ListControlFacade listFacade = new Interprise.Facade.Base.ListControlFacade();

                //create current dataset
                Interprise.Framework.Base.DatasetComponent.BaseDataset listDataset = new Interprise.Framework.Base.DatasetComponent.BaseDataset("GatewayPaymentTermGroup");

                //create datatable
                listDataset.Tables.Add(listFacade.CreateTable("SYSTEMPAYMENTTERMGROUPDETAIL"));

                // set dynamic dataset
                listFacade.SetDataset = listDataset;

                //set default column definitions
                foreach (System.Data.DataTable dt in listDataset.Tables)
                {
                    listFacade.InitializeTable(dt);
                }

                if (listDataset.Tables.Contains("SYSTEMPAYMENTTERMGROUPDETAIL"))
                {
                    //add new row to dataset
                    var detailRow = listDataset.Tables[0].NewRow();
                    detailRow.BeginEdit();
                    detailRow[Interprise.Framework.Base.Shared.Const.SYSTEMPAYMENTTERMGROUPDETAILVIEW_PAYMENTTERMGROUP_COLUMN] = currentCustomerPaymentTermGroup;
                    detailRow[Interprise.Framework.Base.Shared.Const.SYSTEMPAYMENTTERMGROUPDETAILVIEW_PAYMENTTERMCODE_COLUMN] = noPaymentPaymentTermCode;
                    detailRow.EndEdit();
                    listDataset.Tables[0].Rows.Add(detailRow);

                    //insert data to DB
                    listFacade.UpdateDataSet(new string[][] { new string[] { "SYSTEMPAYMENTTERMGROUPDETAIL" } },
                    Interprise.Framework.Base.Shared.Enum.TransactionType.ShippingMethod, "SYSTEMPAYMENTTERMGROUPDETAIL", false);
                }

            }
        
        }

        public string[] GetPrefferedGatewayInfo()
        {
            string[] gatewayInfo = new string[] { string.Empty, string.Empty };
            bool isGatewayDefinedInWebsite = false;

            if (System.Web.HttpContext.Current == null)
            {
                isGatewayDefinedInWebsite = false;
            }

            GatewayInformationCustomModel gatewayInfoModel = null;

            gatewayInfoModel = _gatewayRepository.GetEcommerceCreditCardGatewayByWebsite(_appConfigService.WebSiteCode);

            if (!gatewayInfoModel.IsNullOrEmptyTrimmed())
            {
                isGatewayDefinedInWebsite = true;
                gatewayInfo[0] = gatewayInfoModel.Gateway;
                gatewayInfo[1] = gatewayInfoModel.GatewayAssemblyName;
            }

            if (!isGatewayDefinedInWebsite)
            {
                var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();
                gatewayInfoModel = _gatewayRepository.GetEcommerceCreditCardGatewayByPaymentTerm(thisCustomer.PaymentTermCode);
                gatewayInfo[0] = gatewayInfoModel.Gateway;
                gatewayInfo[1] = gatewayInfoModel.GatewayAssemblyName;
            }

            return gatewayInfo;
        }

        public string[] GetPaypalGatewayInfo()
        {
            var paypalGatewayInfo = _gatewayRepository.GetEcommercePaypalGatewayInfo(_appConfigService.WebSiteCode);

            string[] paypalInfo = new string[] { string.Empty, string.Empty };
            paypalInfo[0] = paypalGatewayInfo.Gateway;
            paypalInfo[1] = paypalGatewayInfo.GatewayAssemblyName;

            return paypalInfo;
        }

        public string[] GetGatewayInterface()
        {
            string[] info = new string[2];
            var request = _httpContextService.CurrentRequest;
            if (request.QueryString["PayPal"] == bool.TrueString && request.QueryString["token"] != null)
            {
                info = GetPaypalGatewayInfo();
            }
            else
            {
                info = GetPrefferedGatewayInfo();
            }

            return info;
        }

        public IEnumerable<PaymentTermDTO> GetPaymentTermOptionsWithoutSagePay(IEnumerable<PaymentTermDTO> paymentTermOptions)
        {
            var paymentTermOptionsWithoutSagePay = paymentTermOptions.Where(item => item.PaymentTermCode != ServiceFactory.GetInstance<IAppConfigService>().SagePayPaymentTerm);
            var newListOfPaymentTermOptions = paymentTermOptionsWithoutSagePay.ToList();
            return newListOfPaymentTermOptions;
        }

    }
}
