﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System.Web.Security;
using System.Web;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class FormsAuthenticationService : ServiceBase, IFormsAuthenticationService
    {
        public string FormsCookieName
        { 
            get
            {
                return FormsAuthentication.FormsCookieName;            
            }
        } 

        public void SaveAuthenticationCookie(Guid contactGuid, bool createPersitentCookie)
        {
             new HttpCookie(FormsCookieName);
            var ticket = CreateFormsAuthTicket(contactGuid.ToString(), DateTime.Now, DateTime.Now.Add(new TimeSpan(1, 0, 0, 0)), createPersitentCookie);
            SaveCookie(CreateAuthCookie(FormsCookieName, Encrypt(ticket), ticket.Expiration));
        }

        public HttpCookie CreateAuthCookie(string name, string value, DateTime expiration)
        {
            return CreateAuthCookie(name, value, expiration, GetCrossDomainValue());
        }

        public HttpCookie CreateAuthCookie(string name, string value, DateTime expiration, string domain)
        {
            return new HttpCookie(name, value)
            {
                HttpOnly = true,
                Path = FormsAuthentication.FormsCookiePath,
                Secure = FormsAuthentication.RequireSSL,
                Domain = domain,
                Expires = expiration,
            };
        }

        public string GetRedirectUrl(string cookieUserName, bool createPersistentCookie)
        {
            return FormsAuthentication.GetRedirectUrl(cookieUserName, createPersistentCookie);
        }

        public FormsAuthenticationTicket CreateFormsAuthTicket(string cookieUserName, bool createPersistentCookie, int timeOut)
        {
            return new FormsAuthenticationTicket(cookieUserName, createPersistentCookie, timeOut);
        }

        public FormsAuthenticationTicket CreateFormsAuthTicket(string cookieUserName, bool createPersistentCookie)
        {
            return new FormsAuthenticationTicket(cookieUserName, createPersistentCookie, FormsAuthentication.Timeout.Minutes);
        }

        public FormsAuthenticationTicket CreateFormsAuthTicket(string cookieUserName, DateTime issued, DateTime expired, bool createPersistentCookie)
        {
            return CreateFormsAuthTicket(1, cookieUserName, issued, expired, createPersistentCookie);
        }

        public FormsAuthenticationTicket CreateFormsAuthTicket(int version, string cookieUserName, DateTime issued, DateTime expired,  bool createPersistentCookie)
        {
            return new FormsAuthenticationTicket(version, cookieUserName, issued, expired, createPersistentCookie, String.Empty);
        }

        public string GetCrossDomainValue()
        {
            string domain = String.Empty;

            string host = CommonLogic.ServerVariables("HTTP_HOST").ToLower();
            bool isDomainIp = System.Text.RegularExpressions.Regex.IsMatch(host, @"^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$");

            if (host.Contains("localhost"))
            {
                domain = null;
            }
            else if (host.Contains(":"))
            {
                domain = host.Substring(0, host.IndexOf(":"));
            }
            else if (isDomainIp)
            {
                if (HttpContext.Current.Request.Browser.Browser.Contains("Opera"))
                {
                    host = null;
                }

                domain = host;
            }
            else if (host.IndexOf("www.") != -1)
            {
                domain = host.Replace("www.", "");
                domain = ".{0}".FormatWith(domain);
            }
            else
            {
                domain = ".{0}".FormatWith(host);
            }

            return domain;
        }

        public void SignOut()
        {
            if (HttpContext.Current == null) return;

            string domain = GetCrossDomainValue();

            DateTime expiration = System.DateTime.Now.AddDays(-1D);

            var currentCookie = new HttpCookie(FormsCookieName);
            currentCookie.Expires = expiration;
            currentCookie.Domain = domain;

            int count = HttpContext.Current.Response.Cookies.Keys.Count;
            for (int i = 0; i < count; i++)
            {
                var itemCookie = HttpContext.Current.Response.Cookies[i];

                if (itemCookie.Name != currentCookie.Name) continue;

                itemCookie.Expires = expiration;
                HttpContext.Current.Response.Cookies.Add(itemCookie);
            }

            count = HttpContext.Current.Request.Cookies.Keys.Count;
            for (int i = 0; i < count; i++)
            {
                var itemCookie = HttpContext.Current.Request.Cookies[i];

                if (itemCookie.Name != currentCookie.Name) continue;
                itemCookie.Expires = expiration;

                var httpCookie = new HttpCookie(currentCookie.Name);
                httpCookie.Expires = expiration;
                httpCookie.Value = itemCookie.Value;
                httpCookie.Domain = itemCookie.Domain;
                HttpContext.Current.Request.Cookies.Add(httpCookie);
                HttpContext.Current.Response.Cookies.Add(httpCookie);

                httpCookie = new HttpCookie(currentCookie.Name);
                httpCookie.Expires = expiration;
                httpCookie.Value = itemCookie.Value;
                httpCookie.Domain = domain;
                HttpContext.Current.Request.Cookies.Add(httpCookie);
                HttpContext.Current.Response.Cookies.Add(httpCookie);
            }

        }

        public void SignOut(string key)
        {
            var cookie = new HttpCookie(key, String.Empty);
            cookie.HttpOnly = true;
            cookie.Path = FormsAuthentication.FormsCookiePath;
            cookie.Expires = DateTime.Now.AddDays(-1D);
            cookie.Secure = FormsAuthentication.RequireSSL;
            cookie.Domain = GetCrossDomainValue();

            HttpContext.Current.Response.Cookies.Remove(key);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public string Encrypt(FormsAuthenticationTicket ticket)
        {
            return FormsAuthentication.Encrypt(ticket);
        }

        public FormsAuthenticationTicket Decrypt(string data)
        {
            return FormsAuthentication.Decrypt(data);
        }

        public void SaveCookie(HttpCookie cookie)
        {
            HttpContext.Current.Response.Cookies.Add(cookie);
        }

        public string FormsCookiePath
        {
            get { return FormsAuthentication.FormsCookiePath; }
        }

        public bool RequireSSL
        {
            get { return FormsAuthentication.RequireSSL; }
        }
    }
}