﻿using System;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class DateTimeService : ServiceBase, IDateTimeService
    {
        public DateTime GetCurrentDate()
        {
            return DateTime.Now;
        }

        public DateTime GetMaxValue()
        {
            return DateTime.MaxValue;
        }
    }
}
