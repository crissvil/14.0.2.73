﻿using System;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.DTO;
using System.Collections.Generic;
using System.Linq;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class OrderService : ServiceBase, IOrderService
    {
        ICustomerRepository _customerRepository = null;
        IAppConfigService _appConfigService = null;
        IAuthenticationService _authenticationService = null;

        public OrderService(ICustomerRepository customerRepository,
                               IAppConfigService appConfigService,
                               IAuthenticationService authenticationService)
        {
            _customerRepository = customerRepository;
            _appConfigService = appConfigService;
            _authenticationService = authenticationService;
        }

        public SalesOrderHistoryCollection GetCustomerSalesOrders(int pages, int current)
        {
            return _customerRepository.GetCustomerSalesOrders(pages, current, _appConfigService.ShowCustomerServiceNotesInReceipts, _appConfigService.WebSiteCode, _authenticationService.GetCurrentLoggedInCustomer());
        }

        public bool IsVoidedCustomerInvoice(string invoiceCode)
        {
            return _customerRepository.IsVoidedCustomerInvoice(invoiceCode);
        }

        public bool IsVoidedCustomerSalesOrder(string salesOrderCode)
        {
            return _customerRepository.IsVoidedCustomerSalesOrder(salesOrderCode);
        }

        #region RMA

        public CustomerInvoiceCustomModel GetCustomerInvoice(string invoiceCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _customerRepository.GetCustomerInvoice(customer.ContactCode, invoiceCode, _appConfigService.WebSiteCode);
        }
        public IEnumerable<CustomerInvoiceCustomModel> GetCustomerInvoices()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _customerRepository.GetCustomerInvoices(customer.ContactCode, _appConfigService.WebSiteCode);
        }
        public IEnumerable<CustomerInvoiceItemCustomModel> GetCustomerInvoiceItems(string invoiceCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _customerRepository.GetCustomerInvoiceItems(customer.ContactCode, invoiceCode, _appConfigService.WebSiteCode);
        }
        public CustomerRMACustomModel GetCustomerRMA(string rmaCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _customerRepository.GetCustomerRMA(customer.ContactCode, rmaCode, _appConfigService.WebSiteCode);
        }
        public IEnumerable<CustomerRMACustomModel> GetCustomerRMAs()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _customerRepository.GetCustomerRMAs(customer.ContactCode, _appConfigService.WebSiteCode);
        }
        public IEnumerable<CustomerRMAItemCustomModel> GetCustomerRMAItems(string rmaCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _customerRepository.GetCustomerRMAItems(customer.ContactCode, rmaCode, _appConfigService.WebSiteCode);
        }

        #endregion
       
    }
}
