﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.DTO;
using Interprise.Facade.Base.CBN;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class ProductService : ServiceBase, IProductService
    {
        private readonly IInventoryRepository _inventoryRepository;
        private readonly IAppConfigService _appConfigService = null;
        private readonly IAuthenticationService _authenticationService = null;
        private readonly IDateTimeService _dataTimeService = null;
        private readonly ICryptographyService _cryptographyService = null;
        private readonly IIMageService _imageService = null;
        private readonly ILocalizationService _localizationService = null;
        private readonly IWarehouseRepository _warehouseRepository = null;

        public ProductService(IInventoryRepository inventoryRepository,
                              IAppConfigService appConfigService,
                              IAuthenticationService authenticationService,
                              IDateTimeService dataTimeService,
                              IShoppingCartService shoppingCartService,
                              IIMageService imageService,
                              ICryptographyService cryptographyService,
                              ILocalizationService localizationService,
                              IWarehouseRepository warehouseRepository)
        {
            _inventoryRepository = inventoryRepository;
            _appConfigService = appConfigService;
            _authenticationService = authenticationService;
            _dataTimeService = dataTimeService;
            _imageService = imageService;
            _cryptographyService = cryptographyService;
            _localizationService = localizationService;
            _warehouseRepository = warehouseRepository;
        }

        public bool IsAKit(string productId)
        {
            return _inventoryRepository.IsAKit(productId);
        }

        public int GetProductCount()
        {
            return _inventoryRepository.GetProductCount();
        }

        public int GetCheckOutOptionCount(ref string xmlString)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetCheckOutOptionCount(_appConfigService.WebSiteCode, customer.ContactCode, customer.CustomerCode, customer.IsNotRegistered,
                                                               customer.LanguageCode, Localization.ToDBDateTimeString(DateTime.Now).ToString(), customer.ProductFilterID, ref xmlString);
        }

        public InventoryProductModel GetInventoryItem(string itemCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetInventoryItem(customer.LanguageCode, itemCode);
        }

        public IEnumerable<InventoryProductModel> GetInventoryItems()
        {
            if (_authenticationService.IsAdminCurrentlyLoggedIn())
            {
                var admin = _authenticationService.GetCurrentLoggedInAdmin();
                return _inventoryRepository.GetInventoryItems(admin.LanguageCode);
            }
            else
            {
                var customer = _authenticationService.GetCurrentLoggedInCustomer();
                return _inventoryRepository.GetInventoryItems(customer.LanguageCode);
            }
        }

        public IEnumerable<InventoryProductModel> GetInventoryItemsWithNoImages()
        {
            if (_authenticationService.IsAdminCurrentlyLoggedIn())
            {
                var admin = _authenticationService.GetCurrentLoggedInAdmin();
                return _inventoryRepository.GetInventoryItemsWithNoImages(admin.LanguageCode);
            }
            else
            {
                var customer = _authenticationService.GetCurrentLoggedInCustomer();
                return _inventoryRepository.GetInventoryItemsWithNoImages(customer.LanguageCode);
            }
        }

        public IEnumerable<InventoryOverrideImageModel> GetItemImages(string itemCode)
        {
            return _inventoryRepository.GetItemImages(_appConfigService.WebSiteCode, itemCode);
        }

        public IEnumerable<InventoryCategoryModel> GetInventoryCategories()
        {
            if (_authenticationService.IsAdminCurrentlyLoggedIn())
            {
                var admin = _authenticationService.GetCurrentLoggedInAdmin();
                return _inventoryRepository.GetInventoryCategories(admin.LanguageCode);
            }
            else
            {
                var customer = _authenticationService.GetCurrentLoggedInCustomer();
                return _inventoryRepository.GetInventoryCategories(customer.LanguageCode);
            }
        }

        public IEnumerable<SystemCategoryModel> GetSystemCategories()
        {
            if (_authenticationService.IsAdminCurrentlyLoggedIn())
            {
                var admin = _authenticationService.GetCurrentLoggedInAdmin();
                return _inventoryRepository.GetSystemCategories(admin.LanguageCode);
            }
            else
            {
                var customer = _authenticationService.GetCurrentLoggedInCustomer();
                return _inventoryRepository.GetSystemCategories(customer.LanguageCode);
            }
        }

        public decimal GetKitDiscount(string itemKitCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetKitDiscount(itemKitCode, customer.ContactCode);
        }

        public IEnumerable<SelectedKitComponentModel> GetSelectedKitComponents(string itemKitCode, string currencyCode, string cartId, bool showDefault)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetSelectedKitComponents(itemKitCode, currencyCode, _appConfigService.WebSiteCode, cartId, customer.IsNotRegistered,
                                        customer.CustomerCode, customer.AnonymousCustomerCode, customer.ContactCode, showDefault);
        }

        public ValidCartItemCustomModel GetValidItemCodeAndBaseUnitMeasureById(int itemId)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetValidItemCodeAndBaseUnitMeasureById(itemId, customer.IsNotRegistered, customer.ContactCode,
                                        _appConfigService.WebSiteCode, Localization.ToDBDateTimeString(DateTime.Now).ToString(), customer.ProductFilterID.ToString());
        }

        public string GetMatrixItemCode(string itemCode)
        {
            return _inventoryRepository.GetMatrixItemCode(itemCode);
        }

        public string GetItemDefaultImageFilenameBySize(string itemCode, ImageSize size)
        {
            var itemImages = _inventoryRepository.GetItemImages(_appConfigService.WebSiteCode, itemCode);
            string fileName = String.Empty;

            if (size == ImageSize.Icon)
            {
                var image = itemImages.FirstOrDefault(item => item.IsDefaultIcon == true);
                if (image != null) { fileName = image.FileName; }
            }
            else
            {
                var image = itemImages.FirstOrDefault(item => item.IsDefaultMedium == true);
                if (image != null) { fileName = image.FileName; }
            }
            return fileName;
        }

        public SystemEntityModel GetVirtualPageSettings(int entityId, string entityName)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetVirtualPageSettings(entityId, entityName, customer.LanguageCode, _appConfigService.WebSiteCode);
        }

        public IEnumerable<MatrixItemWebOptionDescCustomModel> GetMatrixItemWebOptionDescription(string itemCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetMatrixItemWebOptionDescription(_appConfigService.WebSiteCode, customer.LanguageCode, itemCode);
        }

        public IEnumerable<MatrixItemWebOptionDescCustomModel> GetNonStockMatrixItemWebOptionDescription(IEnumerable<string> itemCodes)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetNonStockMatrixItemWebOptionDescription(_appConfigService.WebSiteCode, customer.LanguageCode, itemCodes);
        }

        public ECommerceProductInfoView GetProductInfoViewForShowProduct(string itemCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            var productInfo = _inventoryRepository.GetProductInfoViewForShowProduct(itemCode, customer.LanguageCode, _appConfigService.UserCode, _appConfigService.WebSiteCode,
                                                                  _dataTimeService.GetCurrentDate().ToDateTimeStringForDB(), customer.ProductFilterID, customer.ContactCode);

            if (productInfo == null) return null;

            return new ECommerceProductInfoView
            {

                IsAKit = productInfo.IsAKit,
                IsCBN = productInfo.IsCBN,
                IsMatrix = productInfo.IsMatrix,
                IsPublished = productInfo.IsPublished,
                ItemCode = productInfo.ItemCode,
                ItemDescription = productInfo.ItemDescription,
                ItemName = productInfo.ItemName,
                ItemType = productInfo.ItemType,
                MobileXmlPackage = productInfo.MobileXmlPackage,
                RequiresRegistration = productInfo.RequiresRegistration,
                SEDescription = productInfo.SEDescription,
                SEKeywords = productInfo.SEKeywords,
                SENoScript = productInfo.SENoScript,
                SETitle = productInfo.SETitle,
                Summary = productInfo.Summary,
                Warranty = productInfo.Warranty,
                WebDescription = productInfo.WebDescription,
                XmlPackage = productInfo.XmlPackage,
                CheckOutOption = productInfo.CheckOutOption,
                Counter = productInfo.Counter,
                ExpectedShippingDate = productInfo.ExpectedShippingDate
            };

        }

        public string GetProductInfoViewForShowProductToJson(string itemCode)
        {
            string value = String.Empty;
            var product = GetProductInfoViewForShowProduct(itemCode);
            if (product != null)
            {
                value = _cryptographyService.SerializeToJson<ECommerceProductInfoView>(product);
            }

            return value;
        }

        public string GetStorPickUpInitialInfoToJson(string itemCode, string unitMeasureCode)
        {
            string retValue = String.Empty;

            var currentCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            var primaryAddress = currentCustomer.PrimaryShippingAddress;

            var product = GetProductInfoViewForShowProduct(itemCode);

            var info = new StorePickupAvailabilityInfo();

            info.Address = new LiteAddressInfo()
            {
                Address = primaryAddress.Address1,
                City = primaryAddress.City,
                Country = primaryAddress.Country,
                PostalCode = primaryAddress.PostalCode,
                State = primaryAddress.State,
                Telephone = primaryAddress.Phone
            };

            info.ProductInfo = new LiteProductInfo()
            {
                ID = product.Counter,
                ItemCode = product.ItemCode,
                ItemDescription = product.ItemDescription,
                UnitMeassure = unitMeasureCode,
                ItemName = product.ItemName,
                ImageURL = _imageService.GetProductImageUrlByItemCode(product.ItemCode, ImageSizeTypes.icon),
                RatingHtml = GetProductRatingHtml(product.ItemCode)
            };

            info.JsonCountries = _appConfigService.GetSystemCountriesToJSON();

            return _cryptographyService.SerializeToJson<StorePickupAvailabilityInfo>(info);

        }

        public string GetProductRatingHtml(string itemCode)
        {
            //to be optimized. get only to ave. rating
            var rating = RatingCollection.ForItem(itemCode);
            return CommonLogic.BuildStarsImage(rating.AverageRating, _appConfigService.DefaultSkinID);
        }

        public string GetStorePickupInventoryWarehouseListToJSON(bool isFirstLoad, string itemCode, IList<string> kitComposition, string unitMeasureCode, string postalCode, string city, string state, string country, int nextRecord = 0, byte searchLimit = 10)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();

            if (isFirstLoad)
            {
                postalCode = customer.PrimaryShippingAddress.PostalCode;
                city = String.Empty;
                state = String.Empty;
                country = String.Empty;
            }

            string composition = String.Empty;
            int kitCompositionLen = 0;
            if (kitComposition != null && kitComposition.Count() > 0)
            {
                var arrayOfItemCounter = kitComposition.Select(k => k.Split(new Char[] { '+' })[1]).ToArray();
                composition = String.Join(",", arrayOfItemCounter);
                kitCompositionLen = kitComposition.Count;
            }

            var warehouses = _warehouseRepository.GetInventoryWarehouseForStorePickup(postalCode, city, state, country, itemCode, composition, kitCompositionLen, unitMeasureCode, nextRecord, searchLimit);
            if (warehouses.Count() > 0)
            {
                var list = warehouses.Select(w => new StorePickupInventoryWarehousetTransform()
                {
                    Coordinate = w.Coordinate,
                    RowNumber = w.RowNumber,
                    WareHouseCode = w.WareHouseCode,
                    WareHouseDescription = w.WareHouseDescription,
                    FreeStock = Convert.ToDecimal(_localizationService.FormatDecimal(w.FreeStock, 2)),
                    Distance = 0,
                    Address = new LiteAddressInfo()
                    {
                        Address = w.Address.Address,
                        City = w.Address.City,
                        State = w.Address.State,
                        Country = w.Address.Country,
                        PostalCode = w.Address.PostalCode
                    },
                    Telephone = w.Telephone,
                    TelephoneExtension = w.TelephoneExtension,
                    TelephoneLocalNumber = w.TelephoneLocalNumber,
                    Fax = w.Fax,
                    FaxExtension = w.FaxExtension,
                    FaxLocalNumber = w.FaxLocalNumber,
                    Email = w.Email,
                    Website = w.Website
                }).ToList();

                return _cryptographyService.SerializeToJson<List<StorePickupInventoryWarehousetTransform>>(list);
            }

            //to provide javascript standard checking for array instead of string.empty
            return _cryptographyService.SerializeToJson<IEnumerable<StorePickupInventoryWarehousetTransform>>(new List<StorePickupInventoryWarehousetTransform>());
        }

        public IEnumerable<ItemWebOptionCustomModel> GetWebOptions(IEnumerable<string> itemCodes, bool IsExpressPrint)
        {
            return _inventoryRepository.GetItemWebOptions(_appConfigService.WebSiteCode, itemCodes, IsExpressPrint);
        }

        public ItemWebOptionCustomModel GetWebOption(string itemCode)
        {
            return _inventoryRepository.GetItemWebOption(_appConfigService.WebSiteCode, itemCode, false);
        }
        public ItemWebOptionCustomModel GetWebOption(string itemCode, bool IsExpressPrint)
        {
            return _inventoryRepository.GetItemWebOption(_appConfigService.WebSiteCode, itemCode, IsExpressPrint);
        }

        public IEnumerable<UnitMeasureModel> GetItemUnitMeassures(string itemCode, IEnumerable<string> unitMeasureCodes)
        {
            return (unitMeasureCodes.Count() > 0) ?
                _inventoryRepository.GetItemUnitMeasuresUnitMeasureList(itemCode, unitMeasureCodes) :
                _inventoryRepository.GetItemBaseUnitMeasures(itemCode);
        }

        public InventoryPriceInfoModel GetProductPriceInfo(string itemCode, string unitMeasure, decimal unitMeasureQuantity, decimal quantity, decimal matrixGroupQuantity)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetProductPriceInfo(customer.CurrencyCode, customer.CurrencyCode, itemCode, unitMeasure, unitMeasureQuantity, quantity, matrixGroupQuantity, customer.LanguageCode, _appConfigService.WebSiteCode);
        }

        public InventoryMatrixItemModel GetMatrixItemInfo(string matrixItemCode)
        {
            return _inventoryRepository.GetMatrixItemInfo(matrixItemCode);
        }

        public InventoryMatrixItemModel GetMatrixItemInfo(int counter)
        {
            return _inventoryRepository.GetMatrixItemInfo(counter);
        }

        public IEnumerable<MatrixAttributeModel> GetMatrixAttributes(string itemCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetMatrixAttributes(itemCode, customer.LanguageCode);
        }

        public IEnumerable<MatrixAttributeModel> GetNonStockMatrixAttributes(string counter, string itemCode)
        {
            return _inventoryRepository.GetNonStockMatrixAttributes(counter, itemCode, _appConfigService.WebSiteCode);
        }

        public IEnumerable<FeaturedItemCustomModel> GetFeaturedItems()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetFeaturedItems(_appConfigService.WebSiteCode, customer.LocaleSetting, customer.ContactCode);
        }

        public string GetRatingHeaderJSON(string itemCode)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            string webSiteCode = _appConfigService.WebSiteCode;

            var rating = Rating.ByCustomer(customer, itemCode);

            var ratingHeader = new RatingHeader()
            {
                HasRating = (rating != null),
                Rate = (rating == null) ? 0 : (int)rating.Rate,
                IsRegistered = customer.IsRegistered,
                ContactCode = customer.ContactCode
            };

            var computation = _inventoryRepository.GetRatingComputation(itemCode, webSiteCode);

            if (computation != null)
            {
                ratingHeader.Average = computation.Average;
                ratingHeader.Total = computation.Total;
                ratingHeader.Count = computation.Count;
                ratingHeader.Great = computation.Great;
                ratingHeader.Good = computation.Good;
                ratingHeader.OK = computation.OK;
                ratingHeader.BAD = computation.BAD;
                ratingHeader.TERRIBLE = computation.TERRIBLE;
            }

            return _cryptographyService.SerializeToJson(ratingHeader);
        }

        public string GetProductRatings(string itemCode, int nextRecord, int ratingPageSize, int sortBy)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();

            var ratings = RatingCollection.ForItem(itemCode, customer.GetValidCustomerCodeForShoppingCartRecord(), customer.ContactCode, nextRecord, ratingPageSize, sortBy);
            
            var jsonToRatings = ratings.Items.Select(i => new RatingJson()
            {
                CustomerId = i.CustomerCode,
                ContactId = i.ContactCode,
                FirstName = (i.CustomerCode == "DefaultECommerceShopper")? "Anonymous": i.ContactFirstName,
                LastName = (i.CustomerCode == "DefaultECommerceShopper") ? String.Empty : i.CustomerLastName,
                Salutation = i.ContactSalutationCode,
                Comment = i.Comment,
                Rating = i.ActualRating,
                CreatedOn = i.CreatedOn.ToShortDateString(),
                FoundHelpful = i.HelpfulCount,
                NotHelpful = i.NotHelpfulCount,
                HasRate = i.HasRate,
                Row = i.Row
            }).ToList();

            return _cryptographyService.SerializeToJson(jsonToRatings);
        }

        public string GetProductRating(string itemCode, int nextRecord, int ratingPageSize, int sortBy)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();

            var ratings = RatingCollection.ForItem(itemCode, customer.GetValidCustomerCodeForShoppingCartRecord(), customer.ContactCode, nextRecord, ratingPageSize, sortBy);

            var jsonToRatings = ratings.Items.Select(i => new RatingJson()
            {
                CustomerId = i.CustomerCode,
                ContactId = i.ContactCode,
                FirstName = i.ContactFirstName,
                LastName = i.ContactLastName,
                Salutation = i.ContactSalutationCode,
                Comment = i.Comment,
                Rating = i.ActualRating,
                CreatedOn = i.CreatedOn.ToShortDateString(),
                FoundHelpful = i.HelpfulCount,
                NotHelpful = i.NotHelpfulCount,
                HasRate = i.HasRate,
                Row = i.Row
            }).ToList();

            return _cryptographyService.SerializeToJson(jsonToRatings);
        }

        public string GetProductRatingJSON(string itemCode)
        { 
            string value = String.Empty;
            var thisCustomer = _authenticationService.GetCurrentLoggedInCustomer();

            if (thisCustomer == null) { return value; }
            string imageUrl = _imageService.GetProductImageUrlByItemCode(itemCode, ImageSizeTypes.icon);

            var model = _inventoryRepository.GetRating(itemCode, thisCustomer.CustomerCode, thisCustomer.ContactCode, _appConfigService.WebSiteCode);
            if (model != null)
            {
                model.ImageURL = imageUrl;
            }
            else
            {
                var inventoryModel = _inventoryRepository.GetInventoryItem(thisCustomer.LanguageCode, itemCode);

                model = new RatingModel();
                model.Rating = 0;
                model.ImageURL = imageUrl;
                model.Created = _dataTimeService.GetCurrentDate();
                if(inventoryModel != null) {
                    model.ItemDescription = inventoryModel.ItemDescription;
                }
            }

            value = _cryptographyService.SerializeToJson<RatingModel>(model);

            return value;
        }

        public void SaveRating(string itemCode, int rating, string comment)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            if (customer == null) { return ; }
            var model = new RatingModel()
            {
                Rating = rating,
                Comment = comment,
                HasComment = !comment.IsNullOrEmptyTrimmed(),
                ItemCode = itemCode,
                WebsiteCode = _appConfigService.WebSiteCode,
                CustomerCode = customer.CustomerCode,
                ContactCode = customer.ContactCode,
                Created = _dataTimeService.GetCurrentDate()
            };

            _inventoryRepository.SaveRating(model);

            var productInfo = _inventoryRepository.GetInventoryItem(customer.LanguageCode, itemCode);

            if (productInfo == null) return;
            if (!productInfo.IsCBN) return;

            if (Interprise.Framework.Base.Shared.Common.IsNull(Interprise.Facade.Base.SimpleFacade.Instance.GetField("CBNCompanyID", "CBNProfile"), String.Empty).ToString() == String.Empty) return;
            if (!Convert.ToBoolean(Interprise.Framework.Base.Shared.Common.IsNull(Interprise.Facade.Base.SimpleFacade.Instance.GetField("Status", "CBNProfile"), String.Empty).ToString().ToLowerInvariant() == "Approved".ToLowerInvariant())) return;
            
            CBNBaseFacade CBNFacade = new CBNBaseFacade();
            CBNUploadMethodFacade CBNUploadFacade = new CBNUploadMethodFacade(CBNFacade.GetWoohaaUploadSvcURL(), true);
            var productRatingModel = new ConnectedBusinessNetwork.Contracts.CBNContracts.DataModel.Inventory.ProductRatingModel()
            {
                Comments = model.Comment,
                CompanyId = Convert.ToInt32(InterpriseHelper.ConfigInstance.CompanyInfo.CBNCompanyID),
                CreatedOn = model.Created,
                CustomerEmailAddress = customer.EMail,
                CustomerName = customer.FullName,
                FoundHelpful = 0,
                FoundNotHelpful = 0,
                HasComments = model.HasComment,
                Helpful = 0,
                InventoryItemId = productInfo.CBNItemID,
                IsActive = true,
                IsFilthy = false,
                IsRotd = false,
                MaskedWord = String.Empty,
                Rating = model.Rating,
                RatingCustomerEmailAddress = customer.EMail,
                RatingId = 0,
                RowIndex = 0,
                VotingCustomerEmailAddress = customer.EMail,
                Word = String.Empty
            };

            CBNUploadFacade.SubmitProductReview(productRatingModel);

            CBNFacade.Dispose();
            CBNUploadFacade = null;
        }

        public IEnumerable<MatrixItemCustomModel> GetMatrixItemDetails(string searchString, string itemCode, int pageSize, int currentPage)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetMatrixItemDetails(searchString, itemCode, pageSize, currentPage, customer.LanguageCode, _appConfigService.WebSiteCode, customer.ContactCode);
        }

        public ItemWebOptionDescCustomModel GetItemWebOptionDescription(int itemCounter)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            return _inventoryRepository.GetItemWebOptionDescription(itemCounter, customer.LanguageCode, _appConfigService.WebSiteCode);
        }
    }
}