﻿using System;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System.Web;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class NavigationService : ServiceBase, INavigationService
    {
        private readonly IAppConfigService _appConfigService;

        public NavigationService(IAppConfigService appConfigService) 
        {
            _appConfigService = appConfigService;
        }

        public void NavigateToDefaultPage()
        {
            HttpContext.Current.Response.Redirect("default.aspx");
        }

        public void NavigateToUrl(string url)
        {
            if (HttpContext.Current == null) return;
            HttpContext.Current.Response.Redirect(url);
        }

        public void NavigateToUrl(string url, bool endResponse)
        {
            if (HttpContext.Current == null) return;
            HttpContext.Current.Response.Redirect(url, endResponse);
        }

        public void NavigateToWebAdminDefaultPage(bool checkForReturnUrl)
        {
            string url = _appConfigService.WebAdminDefault;
            if (checkForReturnUrl)
            {
                string returnUrl = "returnurl".ToQueryStringDecode();
                if (!returnUrl.IsNullOrEmptyTrimmed())
                {
                    url = returnUrl;
                }   
            }
            NavigateToUrl(url);
        }

        public void NavigateToWebAdminLoginPage()
        {
            NavigateToUrl(_appConfigService.WebAdminLoginUrl);
        }

        public void NavigateToWebAdminDefaultPage()
        {
            NavigateToWebAdminDefaultPage(false);
        }

        public void NavigateToShoppingCartRestLinkBackWithErroMessage(string error)
        {
            NavigateToUrl("shoppingcart.aspx?resetlinkback=1&errormsg={0}".FormatWith(error.ToUrlEncode()));
        }

        public void NavigateToShoppingCartWitErroMessage(string error)
        {
            NavigateToUrl("shoppingcart.aspx?errormsg={0}".FormatWith(error.ToUrlEncode()));
        }

        public void NavigateToShoppingCartRestLinkBack()
        {
            NavigateToUrl("shoppingcart.aspx?resetlinkback=1");
        }

        public void NavigateToShoppingCart()
        {
            NavigateToUrl("shoppingcart.aspx");
        }

        public void NavigateToCheckoutMult()
        {
            NavigateToUrl("checkoutshippingmult.aspx");
        }

        public void NavigateToCheckOutPayment()
        {
            NavigateToUrl("checkoutpayment.aspx");
        }

        public void NavigateToCheckout1()
        {
            NavigateToUrl("checkout1.aspx");
        }

        public void NavigateToCheckoutShipping()
        {
            NavigateToUrl("checkoutshipping.aspx");
        }

        public void NavigateToCheckoutGiftEmail()
        {
            NavigateToUrl("checkoutgiftemail.aspx");
        }

        public void NavigateToPageError(string error)
        {
            NavigateToUrl("pageError.aspx?Parameter={0}".FormatWith(error.ToUrlEncode()));
        }


        public void NavigateToCheckOutStore()
        {
            NavigateToUrl("checkoutstore.aspx");
        }

        public void NavigateToCheckoutReview()
        {
            NavigateToUrl("checkoutreview.aspx");
        }

        public void NavigateToSecureForm()
        {
            NavigateToUrl("secureform.aspx");
        }

        public void NavigateToOrderConfirmation(string orderCode)
        {
            NavigateToUrl("orderconfirmation.aspx?ordernumber={0}".FormatWith(orderCode.ToUrlEncode()));
        }

        public void NavigateToOrderFailed()
        {
            NavigateToUrl("orderfailed.aspx");
        }

        public void NavigateToProductNotFound()
        {
            NavigateToUrl("ProductNotFound".ToDriverLink());
        }
        public void NavigateToAccountPage()
        {
            NavigateToUrl("account.aspx");
        }

        public void NavigateToCheckoutAnon()
        {
            NavigateToCheckoutAnon(true);   
        }

        public void NavigateToCheckoutAnon(bool isCheckout)
        {
            NavigateToUrl("checkoutanon.aspx?checkout={0}".FormatWith(isCheckout.ToStringLower()));
        }

        public void NavigateToWishList()
        {
            NavigateToUrl("wishlist.aspx");
        }

        public void NavigateToGiftRegistry()
        {
            NavigateToUrl("giftregistry.aspx");
        }

        public void NavigateToSignin(string errorMessage)
        {
            NavigateToUrl("signin.aspx?ErrorMsg={0}".FormatWith(errorMessage));
        }


        public void NavigateToCheckOutPaymentWithErrorMessage(string error)
        {
            NavigateToUrl("checkoutpayment.aspx?errormsg={0}".FormatWith(error));
        }

        public void NavigateToCheckout1WithErrorMessage(string error)
        {
            NavigateToUrl("checkout1.aspx?errormsg={0}".FormatWith(error));
        }

        public void NavigateToRMA()
        {
            NavigateToUrl("rma.aspx");
        }

        public void NavigateToRMA(NotificationStatus status, string message)
        {
            NavigateToUrl("rma.aspx?msgtype={0}&msg={1}".FormatWith(status.ToString().ToLowerInvariant(), message));
        }

        public void NavigateToCreateRMA()
        {
            NavigateToUrl("createrma.aspx");
        }
        public void NavigateToCreateRMA(string message)
        {
            NavigateToUrl("createrma.aspx?msg={0}".FormatWith(message));
        }

    }
}
