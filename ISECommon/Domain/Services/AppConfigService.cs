﻿using System.Configuration;
using System.Web.Configuration;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System.Web;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Collections.Generic;
using System.Linq;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using System;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class AppConfigService : ServiceBase, IAppConfigService
    {
        private readonly IDBConfigurationRepository _dbConfigurationRepository;
        private readonly IApplicationCachingService _applicationCachingService = null;
        private readonly ICryptographyService _cryptographyService = null;
        private readonly IHttpContextService _httpContextService = null;
        private readonly IRequestCachingService _requestCachingService = null;

        public AppConfigService(IDBConfigurationRepository dbConfigurationRepository,
                                IApplicationCachingService applicationCachingService,
                                ICryptographyService cryptographyService,
                                IHttpContextService httpContextService,
                                IRequestCachingService requestCachingService)
        {
            _dbConfigurationRepository = dbConfigurationRepository;
            _applicationCachingService = applicationCachingService;
            _cryptographyService = cryptographyService;
            _httpContextService = httpContextService;
            _requestCachingService = requestCachingService;
        }

        #region CustomConfigs

        public AppConfigs GetAllAppConfig()
        {
            return _dbConfigurationRepository.GetAllAppConfig(WebSiteCode);
        }

        public string WebSiteCode
        {
            get { return InterpriseHelper.ConfigInstance.WebSiteCode; }
        }

        public string OnlineCompanyConnectionString
        {
            get
            {
                return InterpriseHelper.ConfigInstance.OnlineCompanyConnectionString;
            }
        }

        public string UserCode
        {
            get { return InterpriseHelper.ConfigInstance.UserCode; }
        }

        public bool IsSupportedAlternateCheckout
        {
            get
            {
                return !(InterpriseHelper.ConfigInstance.LicenseInfo.ProductEdition.ToUpper() != Interprise.Licensing.Base.Shared.Enum.ProductEdition.ISB.ToString());
            }
        }

        public string WebConfigLocale
        {
            get
            {
                var cultureSection = ConfigurationManager.GetSection("system.web/globalization") as GlobalizationSection;
                return cultureSection.Culture;
            }
        }

        public string GetUSLocale()
        {
            return "en-US";
        }

        public System.Data.DataSet GetLocales()
        {
            return _dbConfigurationRepository.GetLocales();
        }

        public int GetInventoryDecimalPlacesPreference()
        {
            int quantityDecimalPlaces = AppLogic.InventoryDecimalPlacesPreference;
            if (quantityDecimalPlaces < 0) quantityDecimalPlaces = 2;
            return quantityDecimalPlaces;
        }

        public StringResources GetAllStringResource()
        {
            return _dbConfigurationRepository.GetAllStringResource(WebSiteCode);
        }

        public int GetAllWebSiteCount()
        {
            return _dbConfigurationRepository.GetAllWebSiteCount(WebSiteCode);
        }

        public void TryConnectToDB()
        {
            _dbConfigurationRepository.TryConnectToDB();
        }

        public void ClearCustomerSession()
        {
            _dbConfigurationRepository.ClearCustomerSession();
        }

        public System.Collections.Generic.SortedDictionary<string, string> GetRedirectURL()
        {
            return _dbConfigurationRepository.GetRedirectURL(WebSiteCode);
        }

        public InventoryPreference GetInventoryPreference()
        {
            return _dbConfigurationRepository.GetInventoryPreference();
        }

        public IEnumerable<SystemCountryModel> GetSystemCountries()
        {
            var lst = new List<SystemCountryModel>();
            string key = DomainConstants.SYSTEM_COUNTRY_CACHE_KEY;

            if (CacheMenus)
            {
                if (_applicationCachingService.Exist(key))
                {
                    lst = _applicationCachingService.GetItem<List<SystemCountryModel>>(key);
                }
                else
                {
                    lst = _dbConfigurationRepository.GetSystemCounties().ToList();
                    _applicationCachingService.AddItem(key, lst);
                }
            }
            else
            {
                lst = _dbConfigurationRepository.GetSystemCounties().ToList();
            }

            return lst;
        }

        public string GetSystemCountriesToJSON()
        {
            string jsonResult = String.Empty;
            string key = DomainConstants.SYSTEM_COUNTRY_CACHE__JSON_KEY;

            if (CacheMenus)
            {
                if (_applicationCachingService.Exist(key))
                {
                    jsonResult = _applicationCachingService.GetItem<string>(key);
                }
                else
                {
                    jsonResult = _cryptographyService.SerializeToJson(_dbConfigurationRepository.GetSystemCounties().ToList());
                    _applicationCachingService.AddItem(key, jsonResult);
                }
            }
            else
            {
                jsonResult = _cryptographyService.SerializeToJson(_dbConfigurationRepository.GetSystemCounties().ToList());
            }

            return jsonResult;
        }

        public void CleanEcommerceCustomerRecords()
        {
            _dbConfigurationRepository.CleanEcommerceCustomerRecord(WebSiteCode);
        }

        #endregion

        #region Configuration Settings

        /// <summary>
        /// SiteDownForMaintenance
        /// </summary>
        public bool SiteDownForMaintenance
        {
            get
            {
                return CommonLogic.ApplicationBool("SiteDownForMaintenance");
            }
        }

        /// <summary>
        /// SiteDownForMaintenancePage
        /// </summary>
        public string SiteDownForMaintenancePage
        {
            get
            {
                return CommonLogic.Application("SiteDownForMaintenancePage");
            }
        }

        /// <summary>
        /// DBSQLServerLocaleSetting
        /// </summary>
        public string DBSQLServerLocaleSetting
        {
            get
            {
                return CommonLogic.Application("DBSQLServerLocaleSetting");
            }
        }

        /// <summary>
        /// DomainConstants.MobileFolderName
        /// </summary>
        public string MobileFolderName
        {
            get
            {
                return CommonLogic.Application(DomainConstants.MobileFolderName);
            }
        }

        /// <summary>
        /// MobileDeviceSupport
        /// </summary>
        public string MobileDeviceSupport 
        {
            get
            {
                return AppLogic.AppConfig("MobileDeviceSupport");
            }
        }

        public AspNetHostingPermissionLevel DetermineTrustLevel()
        {
            foreach (AspNetHostingPermissionLevel trustLevel in
                   new AspNetHostingPermissionLevel[] {
                AspNetHostingPermissionLevel.Unrestricted,
                AspNetHostingPermissionLevel.High,
                AspNetHostingPermissionLevel.Medium,
                AspNetHostingPermissionLevel.Low,
                AspNetHostingPermissionLevel.Minimal 
            })
            {
                try
                {
                    new AspNetHostingPermission(trustLevel).Demand();
                }
                catch (System.Security.SecurityException)
                {
                    continue;
                }

                return trustLevel;
            }

            return AspNetHostingPermissionLevel.None;
        }

        #region Web Admin Configurations

        public string WebAdminAuthenticationName
        {
            get
            {
                string defaultValue = CommonLogic.Application(DomainConstants.AdminKey);
                if (defaultValue.IsNullOrEmptyTrimmed())
                {
                    defaultValue = DomainConstants.ADMIN_KEY_DEFAULT_VALUE;
                }
                return defaultValue;
            }
        }

        public double WebAdminLoginTimeOut
        {
            get
            {
                double timeOut = CommonLogic.ApplicationNativeDouble(DomainConstants.AdminTimeout);
                if (timeOut == 0)
                {
                    timeOut = DomainConstants.ADMIN_TIMEOUT_DEFAULT_VALUE;
                }
                return timeOut;
            }
        }

        public string WebAdminLoginUrl
        {
            get
            {
                string url = CommonLogic.Application(DomainConstants.AdminLoginUrl);
                if (url.IsNullOrEmptyTrimmed())
                {
                    url = DomainConstants.ADMIN_LOGIN_URL_DEFAULT_VALUE;
                }
                return url;
            }
        }

        public string WebAdminDefault
        {
            get
            {
                string url = CommonLogic.Application(DomainConstants.AdminDefault);
                if (url.IsNullOrEmptyTrimmed())
                {
                    url = DomainConstants.ADMIN_DEFAULT_URL_VALUE;
                }
                return url;
            }
        }

        #endregion

        #endregion

        #region AppConfig

        /// <summary>
        /// DefaultSkinID
        /// </summary>
        public int DefaultSkinID
        {
            get
            {
                int? cachedId = _requestCachingService.GetItem("SKINID").TryParseInt();
                if (cachedId.HasValue)
                {
                    return cachedId.Value;
                }
                else
                {
                    cachedId = _httpContextService.CurrentRequest.QueryString["skinid"].TryParseInt();
                    int value = (cachedId.HasValue) ? cachedId.Value : AppLogic.AppConfigUSInt("DefaultSkinID");
                    _requestCachingService.AddItem("SKINID", value);

                    return value;
                }
            }
        }

        /// <summary>
        /// GiftRegistry.Enabled
        /// </summary>
        public bool GiftRegistryEnabled
        {
            get
            {
                return AppLogic.AppConfigBool("GiftRegistry.Enabled"); 
            }
        }

        /// <summary>
        /// ReplaceImageURLFromAssetMgr
        /// </summary>
        public bool ReplaceImageURLFromAssetMgr
        {
            get { return AppLogic.AppConfigBool("ReplaceImageURLFromAssetMgr"); }
        }

        /// <summary>
        /// CacheMenus
        /// </summary>
        public bool CacheMenus
        {
            get
            {
                return AppLogic.AppConfigBool("CacheMenus");
            }
        }

        /// <summary>
        /// SiteDownForMaintenanceURL
        /// </summary>
        public string SiteDownForMaintenanceURL
        {
            get
            {
                return CommonLogic.Application("SiteDownForMaintenanceURL");
            }
        }

        /// <summary>
        /// SecurityCodeRequiredOnStoreLogin
        /// </summary>
        public bool SecurityCodeRequiredOnStoreLogin
        {
            get
            {
                return AppLogic.AppConfigBool("SecurityCodeRequiredOnStoreLogin");
            }
        }

        /// <summary>
        /// Captcha.CaseSensitive
        /// </summary>
        public bool CaptchaCaseSensitive
        {
            get
            {
                return AppLogic.AppConfigBool("Captcha.CaseSensitive");
            }
        }

        /// <summary>
        /// CacheDurationMinutes
        /// </summary>
        public int CacheDurationMinutes
        {
            get
            {
                int value = AppLogic.AppConfigUSInt("CacheDurationMinutes");
                if (value == 0)
                {
                    value = 20;
                }
                return value;
            }
        }

        /// <summary>
        /// Localization.WeightUnits
        /// </summary>
        public string WeightUnits
        {
            get
            {
                return AppLogic.AppConfig("Localization.WeightUnits");
            }
        }

        /// <summary>
        /// Localization.CurrencyFeedUrl
        /// </summary>
        public string GetFeedUrl()
        {
            return AppLogic.AppConfig("Localization.CurrencyFeedUrl");
        }

        /// <summary>
        /// Localization.CurrencyFeedBaseRateCurrencyCode
        /// </summary>
        public string GetFeedReferenceCurrencyCode()
        {
            return AppLogic.AppConfig("Localization.CurrencyFeedBaseRateCurrencyCode");
        }

        /// <summary>
        /// Localization.CurrencyCacheMinutes
        /// </summary>
        public int GetLocaleCurrencyCacheMinutes
        {
            get
            {
                return AppLogic.AppConfigUSInt("Localization.CurrencyCacheMinutes");
            }
        }

        /// <summary>
        /// Localization.CurrencyFeedXmlPackage
        /// </summary>
        public string GetLocaleCurrencyFeedXmlPackage()
        {
            return AppLogic.AppConfig("Localization.CurrencyFeedXmlPackage");
        }

        /// <summary>
        /// StoreName
        /// </summary>
        public string StoreName
        {
            get
            {
                return AppLogic.AppConfig("StoreName");
            }
        }

        /// <summary>
        /// MailMe_ToAddress
        /// </summary>
        public string MailMeToAddress
        {
            get
            {
                return AppLogic.AppConfig("MailMe_ToAddress");
            }
        }

        /// <summary>
        /// UseSSL
        /// </summary>
        public bool UseSSL
        {
            get
            {
                return AppLogic.AppConfigBool("UseSSL");
            }
        }

        /// <summary>
        /// HomeTemplateAsIs
        /// </summary>
        public bool HomeTemplateAsIs
        {
            get
            {
                return AppLogic.AppConfigBool("HomeTemplateAsIs");
            }
        }

        /// <summary>
        /// EventLoggingEnabled
        /// </summary>
        public bool EventLoggingEnabled
        {
            get
            {
                return AppLogic.AppConfigBool("EventLoggingEnabled");
            }
        }

        /// <summary>
        /// RedirectLiveToWWW
        /// </summary>
        public bool RedirectLiveToWWW
        {
            get
            {
                return AppLogic.AppConfigBool("RedirectLiveToWWW");
            }
        }

        /// <summary>
        /// HomeTemplate
        /// </summary>
        public string HomeTemplate
        {
            get
            {
                return AppLogic.AppConfig("HomeTemplate");
            }
        }

        /// <summary>
        /// LiveServer
        /// </summary>
        public string LiveServer
        {
            get {
                var liveServer = string.Empty;
                if (CommonLogic.GetWebsiteType() == WebsiteType.Smartbag)
                {
                    liveServer = AppLogic.AppConfig("LiveServer"); 
                }
                else if (CommonLogic.GetWebsiteType() == WebsiteType.Company)
                { 
                    liveServer = AppLogic.AppConfig("LiveServer-Company");
                }
                return liveServer;
            }
        }

        /// <summary>
        /// VAT.Enabled
        /// </summary>
        public bool VATIsEnabled
        {
            get
            {
                return AppLogic.AppConfigBool("VAT.Enabled");
            }
        }

        /// <summary>
        /// AllowCreditHold
        /// </summary>
        public bool AllowCreditHold
        {
            get
            {
                return AppLogic.AppConfigBool("AllowCreditHold");
            }
        }

        /// <summary>
        /// AllowProductFiltering
        /// </summary>
        public bool AllowProductFiltering
        {
            get
            {
                return AppLogic.AppConfigBool("AllowProductFiltering");
            }
        }

        /// <summary>
        /// PreserveActiveCartOnSignin
        /// </summary>
        public bool PreserveActiveCartOnSignin
        {
            get 
            {
                return AppLogic.AppConfigBool("PreserveActiveCartOnSignin");
            }
        }

        /// <summary>
        /// ClearOldCartOnSignin
        /// </summary>
        public bool ClearOldCartOnSignin
        {
            get
            {
                return AppLogic.AppConfigBool("ClearOldCartOnSignin");
            }
        }

        /// <summary>
        /// ShippingMethodCodeIfFreeShippingIsOn
        /// </summary>
        public string ShippingMethodCodeIfFreeShippingIsOn
        {
            get { return AppLogic.AppConfig("ShippingMethodCodeIfFreeShippingIsOn"); }
        }

        /// <summary>
        /// ShippingMethodCodeZeroDollarOrder
        /// </summary>
        public string ShippingMethodCodeZeroDollarOrder
        {
            get { return AppLogic.AppConfig("ShippingMethodCodeZeroDollarOrder"); }
        }

        /// <summary>
        /// PaymentTermCodeZeroDollarOrder
        /// </summary>
        public string PaymentTermCodeZeroDollarOrder
        {
            get { return AppLogic.AppConfig("PaymentTermCodeZeroDollarOrder"); }
        }

        /// <summary>
        /// MaxBadLogins
        /// </summary>
        public int MaxBadLogins
        {
            get { return AppLogic.AppConfigNativeInt("MaxBadLogins"); }
        }

        /// <summary>
        /// BadLoginLockTimeOut
        /// </summary>
        public int BadLoginLockTimeOut
        {
            get { return AppLogic.AppConfigUSInt("BadLoginLockTimeOut"); }
        }

        /// <summary>
        /// MicroStyle
        /// </summary>
        public string MicroStyle
        {
            get { return AppLogic.AppConfig("MicroStyle"); }
        }

        /// <summary>
        /// MobileImageStyle
        /// </summary>
        public string MobileImageStyle
        {
            get { return AppLogic.AppConfig("MobileImageStyle"); }
        }

        /// <summary>
        /// MiniCartStyle
        /// </summary>
        public string MiniCartStyle
        {
            get { return AppLogic.AppConfig("MiniCartStyle"); }
        }

        /// <summary>
        /// AllowEmptySkuAddToCart
        /// </summary>
        public bool AllowEmptySkuAddToCart
        {
            get { return AppLogic.AppConfigBool("AllowEmptySkuAddToCart"); }
        }

        /// <summary>
        /// ShowCustomerServiceNotesInReceipts
        /// </summary>
        public bool ShowCustomerServiceNotesInReceipts
        {
            get { return AppLogic.AppConfigBool("ShowCustomerServiceNotesInReceipts"); }
        }

        /// <summary>
        /// WebSupport.Enabled
        /// </summary>
        public bool WebSupportEnabled
        {
            get { return AppLogic.AppConfigBool("WebSupport.Enabled"); }
        }

        /// <summary>
        /// WebSupport.CodeLength
        /// </summary>
        public string WebSupportCodeLength
        {
            get { return AppLogic.AppConfig("WebSupport.CodeLength"); }
        }

        /// <summary>
        /// WebSupport.Time
        /// </summary>
        public string WebSupportTime
        {
            get { return AppLogic.AppConfig("WebSupport.Time"); }
        }

        /// <summary>
        /// AllowMultipleShippingAddressPerOrder
        /// </summary>
        public bool AllowMultipleShippingAddressPerOrder
        {
            get { return AppLogic.AppConfigBool("AllowMultipleShippingAddressPerOrder"); }
        }

        /// <summary>
        /// RequireOver13Checked
        /// </summary>
        public bool RequireOver13Checked
        {
            get { return AppLogic.AppConfigBool("RequireOver13Checked"); }
        }

        /// <summary>
        /// PasswordIsOptionalDuringCheckout
        /// </summary>
        public bool PasswordIsOptionalDuringCheckout
        {
            get { return AppLogic.AppConfigBool("PasswordIsOptionalDuringCheckout"); }
        }

        /// <summary>
        /// Checkout.UseOnePageCheckout
        /// </summary>
        public bool CheckoutUseOnePageCheckout
        {
            get { return AppLogic.AppConfigBool("Checkout.UseOnePageCheckout"); }
        }

        /// <summary>
        /// MaxMind.SOAPURL
        /// </summary>
        public string MaxMindSOAPURL
        {
            get { return AppLogic.AppConfig("MaxMind.SOAPURL"); }
        }

        /// <summary>
        /// MaxMind.LicenseKey
        /// </summary>
        public string MaxMindLicenseKey
        {
            get { return AppLogic.AppConfig("MaxMind.LicenseKey"); }
        }

        /// <summary>
        /// GiftCode.Enabled
        /// </summary>
        public bool GiftCodeEnabled 
        {
            get { return AppLogic.AppConfigBool("GiftCode.Enabled"); }
        }

        /// <summary>
        /// PayPalCheckout.OverrideAddress
        /// </summary>
        public bool PayPalCheckoutOverrideAddress
        {
            get { return AppLogic.AppConfigBool("PayPalCheckout.OverrideAddress"); }
        }

        /// <summary>
        /// PayPalCheckout.RequireConfirmedAddress
        /// </summary>
        public bool PayPalCheckoutRequireConfirmedAddress
        {
            get { return AppLogic.AppConfigBool("PayPalCheckout.RequireConfirmedAddress"); }
        }

        /// <summary>
        /// LoyaltyPoints.Enabled
        /// </summary>
        public bool LoyaltyPointsEnabled
        {
            get { return AppLogic.AppConfigBool("LoyaltyPoints.Enabled"); }
        }

        /// <summary>
        /// RatingsCanBeDoneByAnons
        /// </summary>
        public bool RatingsCanBeDoneByAnons
        {
            get { return AppLogic.AppConfigBool("RatingsCanBeDoneByAnons"); }
        }

        /// <summary>
        /// CartMinOrderAmount
        /// </summary>
        public decimal CartMinOrderAmount
        {
            get { return AppLogic.AppConfigUSDecimal("CartMinOrderAmount"); }
        }

        // MinCartItemsBeforeCheckout
        public int MinCartItemsBeforeCheckout
        {
            get { return AppLogic.AppConfigUSInt("MinCartItemsBeforeCheckout"); }
        }

        /// <summary>
        /// LiveChat.Enabled
        /// </summary>
        public bool LiveChatEnabled
        {
            get { return AppLogic.AppConfigBool("LiveChat.Enabled"); }
        }

        /// <summary>
        /// LiveChat.Url
        /// </summary>
        public string LiveChatUrl
        {
            get { return AppLogic.AppConfig("LiveChat.Url"); }
        }

        /// <summary>
        /// ShowStringResourceKeys
        /// </summary>
        public bool ShowStringResourceKeys
        {
            get { return AppLogic.AppConfigBool("ShowStringResourceKeys"); }
        }

        /// <summary>
        /// HideOutOfStockProducts
        /// </summary>
        public bool HideOutOfStockProducts
        {
            get { return AppLogic.AppConfigBool("HideOutOfStockProducts"); }
        }

        /// <summary>
        /// ShowEditAddressLinkOnCheckOutReview
        /// </summary>
        public bool ShowEditAddressLinkOnCheckOutReview
        {
            get { return AppLogic.AppConfigBool("ShowEditAddressLinkOnCheckOutReview"); }
        }

        /// <summary>
        /// AllowShipToDifferentThanBillTo
        /// </summary>
        public bool AllowShipToDifferentThanBillTo
        {
            get { return AppLogic.AppConfigBool("AllowShipToDifferentThanBillTo"); }
        }

        /// <summary>
        /// ShowPicsInMiniCart
        /// </summary>
        public bool ShowPicsInMiniCart
        {
            get { return AppLogic.AppConfigBool("ShowPicsInMiniCart"); }
        }

        /// <summary>
        /// LinkToProductPageInCart
        /// </summary>
        public bool LinkToProductPageInCart
        {
            get { return AppLogic.AppConfigBool("LinkToProductPageInCart"); }
        }

        /// <summary>
        /// ShowStockHints
        /// </summary>
        public bool ShowStockHints
        {
            get { return AppLogic.AppConfigBool("ShowStockHints"); }
        }

        /// <summary>
        /// ShowCartDeleteItemButton
        /// </summary>
        public bool ShowCartDeleteItemButton
        {
            get { return AppLogic.AppConfigBool("ShowCartDeleteItemButton"); }
        }

        /// <summary>
        /// ShowActualInventory
        /// </summary>
        public bool ShowActualInventory
        {
            get { return AppLogic.AppConfigBool("ShowActualInventory"); }
        }

        /// <summary>
        /// ShippingRatesOnDemand
        /// </summary>
        public bool ShippingRatesOnDemand
        {
            get { return AppLogic.AppConfigBool("ShippingRatesOnDemand"); }
        }

        /// <summary>
        /// ShowFeaturedItem
        /// </summary>
        public bool ShowFeaturedItem
        {
            get { return AppLogic.AppConfigBool("ShowFeaturedItem"); }
        }

        public bool ValidateCardEnabled
        {
            get { return AppLogic.AppConfigBool("ValidateCard.Enabled"); }
        }

        /// <summary>
        /// OnlinePaymentEmailTo
        /// </summary>
        public string OnlinePaymentEmailTo
        {
            get { return AppLogic.AppConfig("OnlinePaymentEmailTo"); }
        }

        /// <summary>
        /// OnlinePaymentEmailToName
        /// </summary>
        public string OnlinePaymentEmailToName
        {
            get { return AppLogic.AppConfig("OnlinePaymentEmailToName"); }
        }

        /// <summary>
        /// OrderFailedEmailTo
        /// </summary>
        public string OrderFailedEmailTo
        {
            get { return AppLogic.AppConfig("OrderFailedEmailTo"); }
        }

        /// <summary>
        /// OrderFailedEmailToName
        /// </summary>
        public string OrderFailedEmailToName
        {
            get { return AppLogic.AppConfig("OrderFailedEmailToName"); }
        }

        /// <summary>
        /// CustomerSupportEmailTo
        /// </summary>
        public string CustomerSupportEmailTo
        {
            get { return AppLogic.AppConfig("CustomerSupportEmailTo"); }
        }

        /// <summary>
        /// CustomerSupportEmailToName
        /// </summary>
        public string CustomerSupportEmailToName
        {
            get { return AppLogic.AppConfig("CustomerSupportEmailToName"); }
        }

        /// <summary>
        /// ShowBuyButtons
        /// </summary>
        public bool ShowBuyButtons
        {
            get { return AppLogic.AppConfigBool("ShowBuyButtons"); }
        }

        /// <summary>
        /// FeaturedItemsDisplayAddToCart
        /// </summary>
        public bool FeaturedItemsDisplayAddToCart
        {
            get { return AppLogic.AppConfigBool("FeaturedItemsDisplayAddToCart"); }
        }
        /// <summary>
        /// FeaturedItemsDisplayPrice
        /// </summary>
        public bool FeaturedItemsDisplayPrice
        {
            get { return AppLogic.AppConfigBool("FeaturedItemsDisplayPrice"); }
        }

        /// <summary>
        /// ShowEntityLoyaltyPoints
        /// </summary>
        public bool ShowEntityLoyaltyPoints
        {
            get { return AppLogic.AppConfigBool("ShowEntityLoyaltyPoints"); }
        }

        /// <summary>
        /// ShowProductLoyaltyPoints
        /// </summary>
        public bool ShowProductLoyaltyPoints
        {
            get { return AppLogic.AppConfigBool("ShowProductLoyaltyPoints"); }
        }

        /// <summary>
        /// SearchXMLPackage
        /// </summary>
        public string SearchXMLPackage
        {
            get { return AppLogic.AppConfig("Search_XMLPackage"); }
        }

        /// <summary>
        /// SearchAdvXMLPackage
        /// </summary>
        public string SearchAdvXMLPackage
        {
            get { return AppLogic.AppConfig("SearchAdv_XMLPackage"); }
        }

        /// <summary>
        /// DisallowOrderNotes
        /// </summary>
        public bool DisallowOrderNotes
        {
            get { return AppLogic.AppConfigBool("DisallowOrderNotes"); }
        }

        /// <summary>
        /// NumHomePageSpecials
        /// </summary>
        public int NumHomePageSpecials
        {
            get { return AppLogic.AppConfigUSInt("NumHomePageSpecials"); }
        }

        /// <summary>
        /// WholesaleOnlySite
        /// </summary>
        public bool WholesaleOnlySite
        {
            get { return AppLogic.AppConfigBool("WholesaleOnlySite"); }
        }

        /// <summary>
        /// ShowItemPriceWhenLogin
        /// </summary>
        public bool ShowItemPriceWhenLogin
        {
            get { return AppLogic.AppConfigBool("ShowItemPriceWhenLogin"); }
        }

        /// <summary>
        /// Watermark.Enabled
        /// </summary>
        public bool WatermarkIsEnabled
        {
            get { return AppLogic.AppConfigBool("Watermark.Enabled"); }
        }

        /// <summary>
        /// FeaturedItemsXMLPackage
        /// </summary>
        public string FeaturedItemsXMLPackage
        {
            get { return AppLogic.AppConfig("FeaturedItemsXMLPackage"); }
        }

        /// <summary>
        /// AddToCartAction
        /// </summary>
        public string AddToCartAction
        {
            get { return AppLogic.AppConfig("AddToCartAction"); }
        }

        /// <summary>
        /// FeaturedItemsLayout
        /// </summary>
        public string FeaturedItemsLayout
        {
            get { return AppLogic.AppConfig("FeaturedItemLayout"); }
        }

        /// <summary>
        /// FeaturedItemsColumn
        /// </summary>
        public int FeaturedItemsColumn
        {
            get { return AppLogic.AppConfigUSInt("FeaturedItemsColumn"); }
        }

        /// <summary>
        /// FeaturedItemsDisplayPic
        /// </summary>
        public bool FeaturedItemsDisplayPic
        {
            get { return AppLogic.AppConfigBool("FeaturedItemsDisplayPic"); }
        }

        /// <summary>
        /// FeaturedItemsDisplayRating
        /// </summary>
        public bool FeaturedItemsDisplayRating
        {
            get { return AppLogic.AppConfigBool("FeaturedItemsDisplayRating"); }
        }
        /// <summary>
        /// RatingPageCount
        /// </summary>
        public int RatingPageCount
        {
            get { return AppLogic.AppConfigUSInt("RatingPageCount"); }
        }
        #endregion

        /// <summary>
        /// Inventory.LimitCartToQuantityOnHand
        /// </summary>
        public bool LimitCartToQuantityOnHand
        {
            get
            {
                return AppLogic.AppConfigBool("Inventory.LimitCartToQuantityOnHand");
            }
        }

        /// <summary>
        /// CreditRedemption.Enabled
        /// </summary>
        public bool CreditRedemptionIsEnabled
        {
            get 
            {
                return AppLogic.AppConfigBool("CreditRedemption.Enabled");
            }
        }

        /// <summary>
        /// Ratings.Enabled
        /// </summary>
        public bool RatingIsEnabled
        {
            get
            {
                return AppLogic.AppConfigBool("Ratings.Enabled");
            }
        }


        public string CBNCompanyID
        {
            get { return Interprise.Connectivity.Database.Configuration.Design.AppConfig.InterpriseConfiguration.Instance.CompanyInfo.CBNCompanyID; }
        }


        /// <summary>
        /// CreateReceiptForAllPaymentType
        /// </summary>
        public bool CreateReceiptForAllPaymentType
        {
            get { return AppLogic.AppConfigBool("CreateReceiptForAllPaymentType"); }
        }

        /// <summary>
        /// SagePay.TransactionMode
        /// </summary>
        public string SagePayTransactionMode
        {
            get { return AppLogic.AppConfig("SagePay.TransactionMode"); }
        }

        /// <summary>
        /// SagePay.VendorName
        /// </summary>
        public string SagePayVendorName
        {
            get { return AppLogic.AppConfig("SagePay.VendorName"); }
        }

        /// <summary>
        /// SagePay.PaymentTerm
        /// </summary>
        public string SagePayPaymentTerm
        {
            get { return AppLogic.AppConfig("SagePay.PaymentTerm"); }
        }

        /// <summary>
        /// ShowShipDateInCart
        /// </summary>
        public bool ShowShipDateInCart
        {
            get { return AppLogic.AppConfigBool("ShowShipDateInCart"); }
        }

        /// <summary>
        /// NotifyOnPriceDrop.Enabled
        /// </summary>
        public bool NotifyOnPriceDropEnabled
        {
            get
            {
                return AppLogic.AppConfigBool("NotifyOnPriceDrop.Enabled");
            }
        }

        /// <summary>
        /// NotifyWhenAvail.Enabled
        /// </summary>
        public bool NotifyWhenAvailEnabled
        {
            get
            {
                return AppLogic.AppConfigBool("NotifyWhenAvail.Enabled");
            }
        }

        /// <summary>
        /// ShowPicsInCart
        /// </summary>
        public bool ShowPicsInCart
        {
            get 
            {
                return AppLogic.AppConfigBool("ShowPicsInCart");
            }
        }

        public bool AllowCustomerDuplicateEMailAddresses
        {
            get
            {
                return AppLogic.AppConfigBool("AllowCustomerDuplicateEMailAddresses");
            }
        }

        public bool HideUnitMeasure
        {
            get
            {
                return AppLogic.AppConfigBool("HideUnitMeasure");
            }

        }

        /// <summary>
        /// UseWebStorePricing
        /// </summary>
        public bool UseWebStorePricing
        {
            get
            {
                return AppLogic.AppConfigBool("UseWebStorePricing");
            }

        }
    }
}