﻿using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.DTO;
using System;
using System.Linq;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using Interprise.Facade.Base;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class ShippingService : ServiceBase, IShippingService
    {
        IShippingRepository _shippingRepository = null;
        IAppConfigService _appConfigService = null;
        IAuthenticationService _authenticationService = null;
        IStringResourceService _stringResourceService = null;
        ICryptographyService _cryptographyService = null;

        public ShippingService(IShippingRepository shippingRepository,
                               IAppConfigService appConfigService,
                               IAuthenticationService authenticationService,
                               IStringResourceService stringResourceService,
                               ICryptographyService cryptographyService)
        {
            _shippingRepository = shippingRepository;
            _appConfigService = appConfigService;
            _authenticationService = authenticationService;
            _stringResourceService = stringResourceService;
            _cryptographyService = cryptographyService;
        }

        public void EnsureNoShippingMethodRequiredIsExisting()
        {
            string noShippingRequiredCode = _appConfigService.ShippingMethodCodeZeroDollarOrder;
            if (noShippingRequiredCode.IsNullOrEmptyTrimmed())
            {
                throw new Exception("ShippingMethodCodeZeroDollarOrder AppConfig is not configured. Please setup a dummy shipping method code to mark orders that doesn't require shipping!!!");
            }

            bool noShippingMethodRequiredShippingMethodExisting = _shippingRepository.NoShippingMethodRequiredShippingMethodExisting(noShippingRequiredCode);
            if (!noShippingMethodRequiredShippingMethodExisting)
            {
                throw new Exception(string.Format("ShippingMethodCodeZeroDollarOrder AppConfig:\"{0}\" is not existing in the database!!!", noShippingRequiredCode));
            }
        }

        public void EnsureNoShippingMethodRequiredIsAssociatedWithCurrentCustomer(bool isRegistered, string anonymousShipToCode, string currentCustomerShippingMethodGroup)
        {
            string noShippingRequiredCode = _appConfigService.ShippingMethodCodeZeroDollarOrder;
            
            string shippingMethodGroupToAssociateWith = string.Empty;

            if (isRegistered)
            {
                shippingMethodGroupToAssociateWith = currentCustomerShippingMethodGroup;
            }
            else
            {
                shippingMethodGroupToAssociateWith = _shippingRepository.GetShippingMethodGroupByShipToCode(anonymousShipToCode);
            }

            if (_shippingRepository.CheckIfShippingMethodInShippingMethodGroup(shippingMethodGroupToAssociateWith, noShippingRequiredCode)) return;

            //Associate this shipping method to ALL groups
            //So that it will be available to all customers when it comes migrating sales orders..
            // check if shippingMethodGroup exists

            bool shippingMethodGroupExist  = _shippingRepository.CheckIfShippingMethodGroupExists(shippingMethodGroupToAssociateWith);


            if (!shippingMethodGroupExist) return;

            //this is dynamic facade that can contain any dataset
            Interprise.Facade.Base.ListControlFacade listFacade = new Interprise.Facade.Base.ListControlFacade();

            //create current dataset
            Interprise.Framework.Base.DatasetComponent.BaseDataset listDataset = new Interprise.Framework.Base.DatasetComponent.BaseDataset("GatewayShippingMethodGroup");

            //create datatable
            listDataset.Tables.Add(listFacade.CreateTable(Interprise.Framework.Base.Shared.Const.SYSTEMSHIPPINGMETHODGROUPDETAIL_TABLE));

            // set dynamic dataset
            listFacade.SetDataset = listDataset;

            //set default column definitions
            foreach (System.Data.DataTable dt in listDataset.Tables)
            {
                listFacade.InitializeTable(dt);
            }

            if (listDataset.Tables.Contains(Interprise.Framework.Base.Shared.Const.SYSTEMSHIPPINGMETHODGROUPDETAIL_TABLE))
            {
                //add new row to dataset
                var detailRow = listDataset.Tables[0].NewRow();
                detailRow.BeginEdit();
                detailRow[Interprise.Framework.Base.Shared.Const.SYSTEMSHIPPINGMETHODGROUPDETAIL_SHIPPINGMETHODGROUP_COLUMN] = shippingMethodGroupToAssociateWith;
                detailRow[Interprise.Framework.Base.Shared.Const.SYSTEMSHIPPINGMETHODGROUPDETAIL_SHIPPINGMETHODCODE_COLUMN] = noShippingRequiredCode;
                detailRow[Interprise.Framework.Base.Shared.Const.SYSTEMSHIPPINGMETHODGROUPDETAIL_FREIGHTCHARGETYPE_COLUMN] = Interprise.Framework.Base.Shared.Const.CALCULATE_FREIGHT_CHARGE_NONE;
                detailRow.EndEdit();
                listDataset.Tables[0].Rows.Add(detailRow);

                //insert data to DB
                listFacade.UpdateDataSet(new string[][]{new string[]{Interprise.Framework.Base.Shared.Const.SYSTEMSHIPPINGMETHODGROUPDETAIL_TABLE,
                Interprise.Framework.Base.Shared.StoredProcedures.CREATESYSTEMSHIPPINGMETHODGROUPDETAIL, 
                Interprise.Framework.Base.Shared.StoredProcedures.UPDATESYSTEMSHIPPINGMETHODGROUPDETAIL, 
                Interprise.Framework.Base.Shared.StoredProcedures.DELETESYSTEMSHIPPINGMETHODGROUPDETAIL}},
                Interprise.Framework.Base.Shared.Enum.TransactionType.ShippingMethod, Interprise.Framework.Base.Shared.Const.SYSTEMSHIPPINGMETHODGROUPDETAIL_TABLE, false);
            }
        
        }

        public ShippingMethodDTOCollection GetCustomerShippingMethods(string customerOrAnonCode, bool isCouponFreeShipping, string contactCode, string shippingMethodCode, 
                                                                      Address preferredShippingAddress, bool isFreeShippingThresholdEnabled)
        {
            var facadeInstance = SimpleFacade.Instance;
            var currentCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            decimal exchangeRate = facadeInstance.GetExchangerate(currentCustomer.CurrencyCode);

            string postalCode = (currentCustomer.IsNotRegistered) ? preferredShippingAddress.PostalCode : String.Empty;

            return _shippingRepository.GetCustomerShippingMethods(
                                                (currentCustomer.IsNotRegistered) ? String.Empty : customerOrAnonCode,
                                                isCouponFreeShipping,
                                                contactCode,
                                                shippingMethodCode,
                                                (currentCustomer.IsNotRegistered) ? String.Empty : preferredShippingAddress.AddressID,
                                                preferredShippingAddress.ResidenceType.ToString(),
                                                currentCustomer.IsNotRegistered,
                                                preferredShippingAddress.Country,
                                                currentCustomer.DefaultPrice,
                                                postalCode,
                                                _appConfigService.ShippingMethodCodeZeroDollarOrder,
                                                _appConfigService.ShippingRatesOnDemand,
                                                isFreeShippingThresholdEnabled,
                                                _stringResourceService.GetString("shoppingcart.aspx.13"),
                                                exchangeRate
                                             );

        }

        public bool IsStorePickUpIncludedInCustomerShippingMethods()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            string contactCode = (customer.IsNotRegistered) ? String.Empty : customer.ContactCode;

            if (customer.PrimaryShippingAddress.IsEmpty()) return false;

            var availableShippingMethods = GetCustomerShippingMethods(customer.CustomerCode, false,
                                            contactCode, String.Empty, customer.PrimaryShippingAddress, false);

            return availableShippingMethods.Any(s => s.FreightChargeType.ToUpperInvariant() == DomainConstants.PICKUP_FREIGHT_CHARGE_TYPE.ToUpperInvariant());
        }

        public string GetStorePickupShippingMethodDescription()
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            string contactCode = (customer.IsNotRegistered) ? String.Empty : customer.ContactCode;

            if (customer.PrimaryShippingAddress.IsEmpty()) return String.Empty;

            var availableShippingMethods = GetCustomerShippingMethods(customer.CustomerCode, false,
                                            contactCode, String.Empty, customer.PrimaryShippingAddress, false);

            var shippingMethod = availableShippingMethods.FirstOrDefault(s => s.FreightChargeType.ToUpperInvariant() == "Pick Up".ToUpperInvariant());

            return (shippingMethod != null)? shippingMethod.Description : String.Empty;
        }

        public void SetRealTimeRateRecord(string shippingMethodCode, string freight, string realTimeRateGUID, bool isFromMultipleShipping)
        {
            var customer = _authenticationService.GetCurrentLoggedInCustomer();
            if (!isFromMultipleShipping)
            {
                _shippingRepository.DeleteEcommerceRealtimeReadByContactCode(customer.ContactCode);
            }

            _shippingRepository.CreateRealTimeRate(customer.ContactCode, shippingMethodCode, freight, realTimeRateGUID, isFromMultipleShipping);
        }

        public ShippingMethodOversizedCustomModel GetOverSizedItemShippingMethod(string itemCode, string unitMesssureCode)
        {
            return _shippingRepository.GetOverSizedItemShippingMethod(itemCode, unitMesssureCode);
        }

        public string GetOverSizedItemShippingMethodToJson(string itemCode, string unitMeasureCode)
        {
            string retVal = String.Empty;
            var model = _shippingRepository.GetOverSizedItemShippingMethod(itemCode, unitMeasureCode);

            if (model != null)
            {
                retVal = _cryptographyService.SerializeToJson<ShippingMethodOversizedCustomModel>(model);
            }
            return retVal;
        }
    }
}