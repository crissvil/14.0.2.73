﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public sealed class ApplicationCachingService : ServiceBase, IApplicationCachingService
    {
        //Return null if does not exist
        public object GetItem(string key)
        {
            return HttpRuntime.Cache[key];
        }

        //Return null if does not exist
        public T GetItem<T>(string key) where T : class
        {
            return HttpRuntime.Cache[key] as T;
        }

        public void SetItem(string key, object value, int defaultMinutes = 10)
        {
            if (GetItem(key) != null)
            {
                HttpRuntime.Cache[key] = value;
            }
            else
            {
                AddItem(key, value, defaultMinutes);
            }
        }

        public object AddItem(string key, object value, int minutes)
        {
            return AddItem(key, value, DateTime.Now.AddMinutes(minutes));
        }

        public void AddItem(string key, object value, CacheDependency cachedependency = null)
        {
            HttpRuntime.Cache.Insert(key, value, cachedependency);
        }

        public object AddItem(string key, object value, System.DateTime expiration, CacheDependency cachedependency = null)
        {
            return HttpRuntime.Cache.Add(key, value, cachedependency, expiration, System.Web.Caching.Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
        }

        public int ItemsCount
        {
            get { return HttpRuntime.Cache.Count; }
        }

        public object RemoveItem(string key)
        {
            return HttpRuntime.Cache.Remove(key);
        }

        public bool Exist(string key)
        {
            return GetItem(key) != null;
        }

        public static void Reset(string key)
        {
            HttpRuntime.Cache.Remove(key);
        }

        public void Reset()
        {
            var cache = HttpRuntime.Cache;
            var keys = new List<string>();
            keys.AddRange(cache.OfType<DictionaryEntry>()
                               .Select(e => e.Key.ToString())
                               .ToArray());
            keys.ForEach(k => { cache.Remove(k); });
        }
    }
}
