﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon;
using System.Globalization;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class LocalizationService : ServiceBase , ILocalizationService
    {
        private readonly IAuthenticationService _authenticationService = null;
        private readonly ICryptographyService _cryptographyService = null;

        public LocalizationService(IAuthenticationService authenticationService,
                                   ICryptographyService cryptographyService)
        {
            _authenticationService = authenticationService;
            _cryptographyService = cryptographyService;
        }

        public string FormatDecimal(decimal valueToFormat, int places)
        {
            return FormatDecimal(valueToFormat.ToString(), places.ToString());
        }

        public string FormatDecimal(string valueToFormat,string places)
        {
            string value = valueToFormat.ToString();
            var validator = new XSLTExtensionBase.InputValidator("FormatDecimal");
            decimal DecimalValue = validator.ValidateDecimal("DecimalValue", value);
            DecimalValue = Localization.ParseDBDecimal(value);
            int FixPlaces = validator.ValidateInt("intFixPlaces", places.ToString());
            DecimalValue = Decimal.Round(DecimalValue, FixPlaces, MidpointRounding.AwayFromZero);
            return DecimalValue.ToString(GetCustomerCultureInfo());
        }

        public CultureInfo GetCustomerCultureInfo()
        {
            return new CultureInfo(_authenticationService.GetCurrentLoggedInCustomer().LocaleSetting);
        }

        public string GetSystemCurrencyModelToJSON()
        {
            return _cryptographyService.SerializeToJson<SystemCurrencyModel>(GetSystemCurrencyModel());
        }

        public SystemCurrencyModel GetSystemCurrencyModel()
        {
            var currentCustomer = _authenticationService.GetCurrentLoggedInCustomer();
            var format = Currency.GetCurrencyFormat(currentCustomer.CurrencyCode);
            return new SystemCurrencyModel()
            {
                CurrencySymbol = format.CurrencySymbol,
                CurrencyPositivePattern = format.CurrencyPositivePattern,
                CurrencyNegativePattern = format.CurrencyNegativePattern,
                CurrencyGroupSizes = format.CurrencyGroupSizes,
                CurrencyGroupSeparator = format.CurrencyGroupSeparator,
                CurrencyDecimalSeparator = format.CurrencyDecimalSeparator,
                CurrencyDecimalDigits = format.CurrencyDecimalDigits,
                NumberDecimalDigits = format.NumberDecimalDigits,
                NumberDecimalSeparator = format.NumberDecimalSeparator,
                NumberGroupSeparator = format.NumberGroupSeparator
            };
        }



        public string FormatDecimalToNumber(decimal value)
        {
            var formatInfo = GetCustomerCultureInfo();
            return value.ToString(StandardNumericFormat.NUMBER, formatInfo);
        }

        public string FormatDecimalToNumber(decimal value, int decimalPlaces)
        {
            string format = StandardNumericFormat.NUMBER + decimalPlaces.ToString();
            var formatInfo = GetCustomerCultureInfo();
            return value.ToString(format, formatInfo);
        }
    }
}
