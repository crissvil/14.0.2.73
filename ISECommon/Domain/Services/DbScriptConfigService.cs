﻿using System;
using System.Linq;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System.Web.Caching;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class DbScriptConfigService : ServiceBase, IDbScriptConfigService
    {
        private readonly IApplicationCachingService _applicationCachingService = null;
        private readonly IHttpContextService _httpContextService = null;

        public DbScriptConfigService(IApplicationCachingService applicationCachingService, IHttpContextService httpContextService)
        {
            _applicationCachingService = applicationCachingService;
            _httpContextService = httpContextService;
        }

        public string Get(string id)
        {
            id = id.Trim();
            string sqlScript = String.Empty;
            var sqlScriptItems = _applicationCachingService.GetItem<SqlQueryConfigs>(CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_CACHE_NAME);
            if (sqlScriptItems != null && sqlScriptItems.Count() > 0)
            {
                var configItem = sqlScriptItems[id] as SqlQueryConfig;
                if (configItem != null)
                {
                    sqlScript = configItem.ScriptValue;
                }
            }
            else
            {
                Initialize();
                sqlScript = Get(id);
            }

            return sqlScript;
        }

        public void Initialize()
        {
            ClearSqlConfigInCache();

            string path = GetDbConfigFilePath();

            var scripts = new SqlQueryConfigs();
            var transactionScripts = SqlQueryConfigs.LoadSqlScriptConfigs(path);
            if (transactionScripts != null)
            {
                scripts.AddRange(transactionScripts);
            }

            _applicationCachingService.AddItem(CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_CACHE_NAME, scripts, DateTime.Now.AddDays(3),
                                                             new CacheDependency(path));
        }

        public int TotalCount
        {
            get 
            {
                var sqlScriptItems = _applicationCachingService.GetItem<SqlQueryConfigs>(CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_CACHE_NAME);
                if (sqlScriptItems != null)
                {
                    return sqlScriptItems.Count();
                }
                return 0;
            }
        }

        public void ClearSqlConfigInCache()
        {
            _applicationCachingService.RemoveItem(CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_CACHE_NAME);
        }

        private string GetDbConfigFilePath()
        {
            return String.Format("{0}\\{1}\\{2}", _httpContextService.ApplicationPath.ToMapPath(), CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_DIR, CoreConstants.DEFAULT_SQL_SCRIPT_CONFIG_NAME);
        }
    }
}