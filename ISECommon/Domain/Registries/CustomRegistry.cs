﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructureMap.Configuration.DSL;
using StructureMap;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Tool;

namespace InterpriseSuiteEcommerceCommon.Domain.Registries
{
    [CLSCompliant(false)]
    public class CustomRegistry : Registry
    {
        public CustomRegistry()
        {
            For<RequestCachingEngine>()
                .Singleton()
                .Use<RequestCachingEngine>();

            For<ApplicationCachingEngine>()
                .Singleton()
                .Use<ApplicationCachingEngine>();
        }
    }
}