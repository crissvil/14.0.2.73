﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructureMap.Configuration.DSL;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Domain;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using StructureMap;

namespace InterpriseSuiteEcommerceCommon.Domain.Registries
{
    [CLSCompliant(false)]
    public class DomainRegistry : Registry
    {
        public DomainRegistry()
        {
            RegisterDataManagerToBecachedPerRequest();
        }

        public void RegisterDataManagerToBecachedPerRequest()
        {
            string connectionString = String.Empty;
            try 
	        {
                connectionString = CommonLogic.ApplicationIS("DBConn");
                For<DataManager>()
                    .HybridHttpOrThreadLocalScoped()
                    .Use(ctx => new DataManager(connectionString, CommonLogic.ApplicationBool("DumpSQL")));
	        }
	        catch (Exception)
	        {
		        throw;
	        }
        }
    }
}
