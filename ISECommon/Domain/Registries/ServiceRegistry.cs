﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using StructureMap.Configuration.DSL;
using StructureMap;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.Domain.Registries
{
    [CLSCompliant(false)]
    public class CustomRegistry : Registry
    {
        public CustomRegistry()
        {
            //For<IMYCUSTOMPLUGIN>()
            //    .Use<CUSTOMPLUGIN_CONCRETE_CLASS>();
        }
    }
}