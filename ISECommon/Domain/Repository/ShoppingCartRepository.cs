﻿using System;
using System.Linq;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using System.Collections.Generic;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
using System.Text;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class ShoppingCartRepository : RepositoryBase, IShoppingCartRepository
    {
        public void UpdateShoppingCartRecordQuantity(int cartRecordID, decimal Quantity, string customerCode, string contactCode)
        {
            string sql = String.Format("UPDATE EcommerceShoppingCart SET Quantity= {0} WHERE ShoppingCartRecID={1} AND CustomerCode={2} AND ContactCode={3}",
                               Localization.ParseDBDecimal(Quantity), cartRecordID.ToString(), customerCode.ToDbQuote(), contactCode.ToDbQuote());
            _dataManager.ExecuteSQL(sql);
        }

        public void DeleteShoppingCartRecord(int cartRecordID, string customerCode, string contactCode)
        {
            string sql = String.Format("DELETE FROM EcommerceShoppingCart where ShoppingCartRecID={0} and CustomerCode={1} and ContactCode={2}",
                                        cartRecordID, customerCode.ToDbQuote() , contactCode.ToDbQuote());
            _dataManager.ExecuteSQL(sql);
        }

        //remove reserve qty from specific item
        public void DeleteShoppingCartItemReservation(string itemCode, string contactCode)
        {
            _dataManager.ExecuteSQL(String.Format("DELETE FROM EcommerceShoppingCartReservation WHERE ContactCode = {0} and ItemCode={1}", contactCode.ToDbQuote(), itemCode.ToDbQuote()));
        }

        //remove reserve qty from specific item
        public void DeleteShoppingReservationByContact(string contactCode)
        {
            _dataManager.ExecuteSQL(
                String.Format("DELETE FROM EcommerceShoppingCartReservation WHERE ContactCode = {0}", contactCode.ToDbQuote()));
        }

        public void DeleteShoppingCartByCartType(string customerCode, string contactCode, CartTypeEnum cartType)
        {
            int cartTypeId = (int)cartType;
            _dataManager.ExecuteSQL(
                String.Format("DELETE FROM EcommerceShoppingCart WHERE CustomerCode = {0} AND ContactCode = {1} AND CartType = {2}",
                    customerCode.ToDbQuote(), contactCode.ToDbQuote(), cartTypeId));
        }

        public decimal GetTotalCartItemsByCustomer(string CustomerID, CartTypeEnum CartType, string ContactCode, string webSiteCode)
        {
            decimal totalCartItem = Decimal.Zero;

            if (CustomerID.Length == 0) return totalCartItem;

            using (var reader = _dataManager.ExecuteSQLReturnReader(String.Format(GetQueryConfig(DBQueryConstants.ReadCustomerTotalCartItems),
                                        ((int)CartType).ToString(), webSiteCode.ToDbQuote(), 
                                        CustomerID.ToString().ToDbQuote(), 
                                        ContactCode.ToDbQuote())))
            {
                if (reader.Read())
                {
                    totalCartItem = reader.ToRSFieldDecimal("NumItems");
                }
            }

            return totalCartItem;
        }

        public void UpdateAnonymousCart(CartTypeEnum cartType, string customerCode, string anonimousCode, string shipToCode, string contactCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateAnonymousCart, (int)cartType, customerCode.ToDbQuote(), anonimousCode.ToDbQuote(), shipToCode.ToDbQuote(), contactCode.ToDbQuote());
        }

        public void ClearCart(CartTypeEnum cartType, string customerId, string contactCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.ReadClearCart, (int)cartType, customerId.ToDbQuote(), contactCode.ToDbQuote());
        }

        public void ClearKitItems(string customerCode, string itemKitCode, Guid cartId)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.DeleteClearKitItems, customerCode.ToDbQuote(), itemKitCode.ToDbQuote(), cartId.ToString().ToDbQuote());
        }

        public void ClearEcommerceCartShippingAddressIDForAnonCustomer(string anonCustomerCode, string WebsiteCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateClearEcommerceCartShippingAddressIDForAnonCustomer, 
                                                anonCustomerCode.ToDbQuote(), 
                                                WebsiteCode.ToDbQuote());
        }

        public void ClearCartLineItems(string[] arrayOfCartGuidId)
        {
            if (arrayOfCartGuidId.Length == 0) return;

            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.DeleteShoppingCartItems,
                                             String.Join(",", arrayOfCartGuidId.Select(i => i.ToDbQuote())));
        }

        public void ClearCartCustomerReservation(string contactCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.DeleteShoppingCartCustomerReservation, contactCode.ToDbQuote());
        }

        public void ClearLineItemsKitCompositions(string[] arrayOfCartGuid)
        {
            if (arrayOfCartGuid.Length == 0) return;

            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.DeleteShoppingCartKitComposition,
                                             String.Join(",", arrayOfCartGuid.Select(i => i.ToDbQuote())));
        }

        public EcommerceShoppingCartModel GetEcommerceShoppingCartByShoppingCartRecId(int recId)
        {
            EcommerceShoppingCartModel model = null;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadEcommerceShoppingCart, recId))
            {
                if (reader.Read())
                {
                    model = new EcommerceShoppingCartModel()
                    {
                        ShoppingCartRecId = reader.ToRSFieldInt("ShoppingCartRecId"),
                        ShoppingCartRecGuid = reader.ToRSFieldTrueGUID("ShoppingCartRecGuid"),
                        ItemCode = reader.ToRSField("ItemCode"),
                        Quantity = reader.ToRSFieldDecimal("Quantity"),
                        RequiresCount = reader.ToRSFieldInt("RequiresCount"),
                        CartType = (CartTypeEnum)reader.ToRSFieldInt("CartType")
                    };
                }
            }
            return model;
        }

        public void UpdateShoppingCartShippingAddressIdByOldPrimaryId(string newAddressId, string oldAddressId, string contactCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.Update_ShoppingCartShippingAddressIdByOldPrimaryId,
                                             newAddressId.ToDbQuote(), oldAddressId.ToDbQuote(), contactCode.ToDbQuote());
        }

        public void UpdateShoppingCartShippingAddressIdForRegistryItems(string newAddressId, string oldAddressId)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.Update_ShoppingCartShippingAddressIdForRegistryItems, newAddressId.ToDbQuote(), oldAddressId.ToDbQuote());
        }

        #region Gift Email

        public IEnumerable<ShoppingCartGiftEmailCustomModel> GetShoppingCartGiftEmails(string contactCode, string languageCode)
        {
            var giftEmails = new List<ShoppingCartGiftEmailCustomModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadEcommerceShoppingCartGiftEmails,
                                                                            contactCode.ToDbQuote(),
                                                                            languageCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var giftEmail = new ShoppingCartGiftEmailCustomModel()
                    {
                        Counter  = reader.ToRSFieldInt("Counter"),
                        ShoppingCartRecID = reader.ToRSFieldInt("ShoppingCartRecID"),
                        LineNum = reader.ToRSFieldInt("LineNum"),
                        ItemCode = reader.ToRSField("ItemCode"),
                        EmailRecipient = reader.ToRSField("EmailRecipient"),
                        ItemCounter = reader.ToRSFieldInt("ItemCounter"),
                        ItemDescription = reader.ToRSField("ItemDescription").ToHtmlEncode()
                    };
                    giftEmails.Add(giftEmail);
                }
            }
            return giftEmails;
        }

        public void CreateShoppingCartGiftEmail(int shoppingCartRecID, string itemCode, int lineNum, string email, string userCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.CreateEcommerceShoppingCartGiftEmail,
                                             Guid.NewGuid().ToString().ToDbQuote(),
                                             shoppingCartRecID,
                                             itemCode.ToDbQuote(),
                                             lineNum,
                                             email.ToDbQuote(),
                                             userCode.ToDbQuote());
        }

        public void CreateShoppingCartGiftEmail(List<ShoppingCartGiftEmailCustomModel> giftEmails, string userCode)
        {
            if (giftEmails.Count == 0) { return; }

            var sql = new StringBuilder();
            foreach (var giftEmail in giftEmails)
            {
                string query = _dataManager.GetQueryConfig(DBQueryConstants.CreateEcommerceShoppingCartGiftEmail,
                                                            Guid.NewGuid().ToString().ToDbQuote(),
                                                            giftEmail.ShoppingCartRecID,
                                                            giftEmail.ItemCode.ToDbQuote(),
                                                            giftEmail.LineNum,
                                                            giftEmail.EmailRecipient.ToDbQuote(),
                                                            userCode.ToDbQuote());
                sql.AppendLine(query);
            }
            _dataManager.ExecuteSQL(sql.ToString());
        }

        public void UpdateShoppingCartGiftEmail(int counter, string emailRecipient, string userCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateEcommerceShoppingCartGiftEmail,
                                             counter,
                                             emailRecipient.ToDbQuote(),
                                             userCode.ToDbQuote());
        }

        public void UpdateShoppingCartGiftEmail(List<ShoppingCartGiftEmailCustomModel> giftEmails, string userCode)
        {
            if (giftEmails.Count == 0) { return; }

            var sql = new StringBuilder();
            foreach (var giftEmail in giftEmails)
            {
                string query = _dataManager.GetQueryConfig(DBQueryConstants.UpdateEcommerceShoppingCartGiftEmail,
                                                            giftEmail.Counter,
                                                            giftEmail.EmailRecipient.ToDbQuote(),
                                                            userCode.ToDbQuote(),
                                                            giftEmail.LineNum);
                sql.AppendLine(query);
            }
            _dataManager.ExecuteSQL(sql.ToString());
        }

        public void DeleteShoppingCartGiftEmailTopRecords(int shoppingCartRecID, int topRecords)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.DeleteEcommerceShoppingCartGiftEmailTopRecords,
                                                                shoppingCartRecID,
                                                                topRecords);
        }

        public void CleanupShoppingCartGiftEmail()
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.DeleteEcommerceShoppingCartGiftEmail);
        }

        public void CreateCustomerCartGiftEmail(string documentCode, List<ShoppingCartGiftEmailCustomModel> giftEmails, string userCode)
        {
            if (giftEmails.Count == 0) { return; }

            var sql = new StringBuilder();
            foreach (var giftEmail in giftEmails)
            {
                string query = _dataManager.GetQueryConfig(DBQueryConstants.CreateCustomerGiftEmail,
                                                            documentCode.ToDbQuote(),
                                                            giftEmail.ItemCode.ToDbQuote(),
                                                            giftEmail.LineNum,
                                                            giftEmail.QuantityOrdered,
                                                            giftEmail.EmailRecipient.ToDbQuote(),
                                                            userCode.ToDbQuote());
                sql.AppendLine(query);
            }
            _dataManager.ExecuteSQL(sql.ToString());
        }

        #endregion

       
       
        public void UpdateShoppingCartInStoreShippingInfo(int shoppingCartRecId, string shippingMethod, string warehouseCode, Guid realTimeRateGuid)
        {
            string realtimeGuid = (realTimeRateGuid == null || realTimeRateGuid == Guid.Empty) ? "NULL" : realTimeRateGuid.ToString().ToDbQuote();
            string tempWareHouseCode = (warehouseCode.IsNullOrEmptyTrimmed())? "NULL": warehouseCode.ToDbQuote();
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateShoppingCartInStoreShippingInfo, shippingMethod.ToDbQuote(), tempWareHouseCode, realtimeGuid, shoppingCartRecId);
        }

        public void ClearCartWarehouseCodeByCustomer(string customerCodeOrId)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateClearCartWarehouseCodeByCustomer, customerCodeOrId.ToDbQuote());
        }

        public void UpdateAllocatedQty(decimal allocatedQty, string contactCode, string itemCode, string unitMeasureCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateAllocatedQty,
                                             allocatedQty, contactCode.ToDbQuote(), itemCode.ToDbQuote(), unitMeasureCode.ToDbQuote());
        }

        public void UpdateAllocatedQty(IEnumerable<CustomerSalesOrderDetailViewDTO> items, string contactCode)
        {
            if (items.Count() == 0) return;

            var sql = new StringBuilder();
            foreach (var item in items)
            {
                string query = _dataManager.GetQueryConfig(DBQueryConstants.UpdateAllocatedQty,
                                                          item.QuantityAllocated,
                                                          contactCode.ToDbQuote(),
                                                          item.ItemCode.ToDbQuote(),
                                                          item.UnitMeasureCode.ToString().ToDbQuote());
                sql.AppendLine(query);
            }
            _dataManager.ExecuteSQL(sql.ToString());
        }

        public bool HasNoStockAndNoOpenPOComponent(string itemCode, string currencyCode, string languageCode, string customerCode, string cartID, string contactCode, string websiteCode)
        {
            bool hasNoStockAndOpenPOComp = false;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetEcommerceKitCartDetailWhenHideOutOfStock,
                                                                            itemCode.ToDbQuote(), currencyCode.ToDbQuote(), languageCode.ToDbQuote(), customerCode.ToDbQuote(),
                                                                            cartID.ToDbQuote(), contactCode.ToDbQuote(), websiteCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    decimal freeStock = Convert.ToDecimal(reader.ToRSFieldDecimal("FreeStock"));
                    string itemType = reader.ToRSField("ItemType");
                    string poStatus = reader.ToRSField("POStatus");
                    string[] itemTypesToCheck = {   Interprise.Framework.Base.Shared.Const.ITEM_TYPE_STOCK.ToLowerInvariant() , 
                                                        Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM.ToLowerInvariant(),
                                                        Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ASSEMBLY.ToLowerInvariant() };
                    string[] poStatusToCheck = { "Open".ToLowerInvariant(), "Partial".ToLowerInvariant() };

                    if (freeStock <= Decimal.Zero && itemTypesToCheck.Contains(itemType.ToLowerInvariant()) && !poStatusToCheck.Contains(poStatus.ToLowerInvariant()))
                    {
                        hasNoStockAndOpenPOComp = true;
                        return hasNoStockAndOpenPOComp;
                    }
                }
            }

            return hasNoStockAndOpenPOComp;
        }

        public bool IsCartHasGiftRegistryItem(string customerCode)
        {
            return GetSqlNByQueryConfigId(DBQueryConstants.Get_CartHasRegistryItem, customerCode.ToDbQuote()) > 0;
        }

        public bool IsCartHasStorePickupItem(string customerCode)
        {
            return GetSqlNByQueryConfigId(DBQueryConstants.Get_CartHasPickUpItem, customerCode.ToDbQuote()) > 0;
        }

        public int UpdateInStorePickupCartItemStock(string customerCode, string contactCode, string websiteCode)
        {
            return _dataManager.ExecuteSQLByQueryID(DBQueryConstants.Update_InStorePickupCartItemStock, customerCode.ToDbQuote(), contactCode.ToDbQuote(), websiteCode.ToDbQuote());
        }


        public void UpdateAppliedCreditCodesJSON(string creditCodesJSON, Guid contactGuid)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.Update_CustomerCreditCode, creditCodesJSON.ToDbQuote(), contactGuid.ToString().ToDbQuote());
        }

        public void ClearAppliedCreditCodesJSON(Guid contactGuid)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.Update_ClearCustomerCreditCode, contactGuid.ToString().ToDbQuote());
        }

        public string GetAppliedCreditCodesJSON(Guid contactGuid)
        {
            string creditCodesJSON = String.Empty;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_CustomerCreditCode, contactGuid.ToString().ToDbQuote()))
            {
                if (reader.Read()) { creditCodesJSON = reader.ToRSField("CreditCode"); }
            }
            return creditCodesJSON;
        }

        #region Gift Codes

        public IEnumerable<GiftCodeCustomModel> GetGiftCodesDetail(string[] giftcodes)
        {
            var quotedCodes = giftcodes.Select(x => x.ToDbQuote());
            string delimitedCodes = String.Join(",", quotedCodes);

            var giftCodes = new List<GiftCodeCustomModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadGiftCodes, delimitedCodes))
            {
                while (reader.Read())
                {
                    var detail = new GiftCodeCustomModel()
                    {
                        Type = reader.ToRSField("Type"),
                        BillToCode = reader.ToRSField("BillToCode"),
                        SerialCode = reader.ToRSField("SerialLotNumber"),
                        CreditCode = reader.ToRSField("CreditCode"),
                        CreditAmountRate = reader.ToRSFieldDecimal("CreditAmountRate"),
                        CreditAllocatedRate = reader.ToRSFieldDecimal("CreditAllocatedRate"),
                        CreditReservedRate = reader.ToRSFieldDecimal("CreditReservedRate"),
                        CreditAvailableRate = reader.ToRSFieldDecimal("CreditAvailableRate"),
                        IsActivated = reader.ToRSFieldBool("IsActivated")
                    };

                    //get exact credit available
                    detail.CreditAvailable = detail.CreditAmountRate - detail.CreditAllocatedRate - detail.CreditReservedRate;

                    giftCodes.Add(detail);
                }
            }
            //note: it is important to sort the result according to the order of param codes
            return giftCodes.OrderBy(x => giftcodes.ToList().IndexOf(x.SerialCode));
        }

        public IEnumerable<GiftCodeCustomModel> GetGiftCodeDetail(string giftcode)
        {
            var giftCodes = new List<GiftCodeCustomModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadGiftCodes, giftcode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var detail = new GiftCodeCustomModel()
                    {
                        Type = reader.ToRSField("Type"),
                        BillToCode = reader.ToRSField("BillToCode"),
                        SerialCode = reader.ToRSField("SerialLotNumber"),
                        CreditCode = reader.ToRSField("CreditCode"),
                        CreditAmountRate = reader.ToRSFieldDecimal("CreditAmountRate"),
                        CreditAllocatedRate = reader.ToRSFieldDecimal("CreditAllocatedRate"),
                        CreditReservedRate = reader.ToRSFieldDecimal("CreditReservedRate"),
                        CreditAvailableRate = reader.ToRSFieldDecimal("CreditAvailableRate"),
                        IsActivated = reader.ToRSFieldBool("IsActivated")
                    };

                    //get exact credit available
                    detail.CreditAvailable = detail.CreditAmountRate - detail.CreditAllocatedRate - detail.CreditReservedRate;

                    giftCodes.Add(detail);
                }
            }
            return giftCodes;
        }

        public void UpdateGiftCode(Guid contactGuid, string giftCodesSerialized)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateCustomerGiftCode, contactGuid.ToString().ToDbQuote(), giftCodesSerialized.ToDbQuote());
        }

        public void ClearGiftCode(Guid contactGuid)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateCustomerGiftCodeToNull, contactGuid.ToString().ToDbQuote());
        }

        public string GetGiftCode(Guid contactGuid)
        {
            string code = String.Empty;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetGiftCodesAppliedToShoppingCart, contactGuid.ToString().ToDbQuote()))
            {
                if(reader.Read()) { code = reader.ToRSField("GiftCode"); }
            }
            return code;
        }

        #endregion 
    
        public void DeleteEcommerceCustomerCartRecord(string customerID, string websiteCode, int cartType, DateTime ageDate)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.Delete_EcommerceCustomerCartRecord, customerID.ToDbQuote(), websiteCode.ToDbQuote(), cartType, ageDate.ToDateTimeStringForDB().ToDbQuote());
        }



        public IEnumerable<ItemReservationCustomModel> GetItemReservation(string contactCode, string customerCode, string websiteCode)
        {
            var itemReservations = new List<ItemReservationCustomModel>();

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_ItemReservation, contactCode.ToDbQuote(), customerCode.ToDbQuote(), websiteCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var reservation = new ItemReservationCustomModel()
                    {
                        ItemCode = reader.ToRSField("ItemCode"),
                        UnitMeasureCode = reader.ToRSField("UnitMeasure"),
                        AllocatedQty = reader.ToRSFieldDecimal("AllocatedQty"),
                        ReservedQty = reader.ToRSFieldDecimal("ReservedQty"),
                        CartRecordID = reader.ToRSFieldInt("ShoppingCartRecID"),
                        DueDate = reader.ToRSFieldDateTime("DueDate")
                    };
                    itemReservations.Add(reservation);
                }
            }
            return itemReservations;
        }

        public void UpdateAllocatedQuantities(IEnumerable<ItemReservationCustomModel> items, string contactCode, string customerCode, string websiteCode)
        {
            var sb = new StringBuilder();
            foreach (var item in items)
            {
                string query = _dataManager.GetQueryConfig(DBQueryConstants.Update_AllocatedQuantity, 
                    item.AllocatedQty, 
                    customerCode.ToDbQuote(), 
                    contactCode.ToDbQuote(), 
                    websiteCode.ToDbQuote(), 
                    item.CartRecordID);
                sb.AppendLine(query);
            }
            
            if(sb.Length > 0) { _dataManager.ExecuteSQL(sb.ToString()); }
        }
    }
}
