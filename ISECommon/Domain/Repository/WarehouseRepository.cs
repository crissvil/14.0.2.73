﻿using System;
using System.Collections.Generic;
using System.Data;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class WarehouseRepository : RepositoryBase, IWarehouseRepository
    {
        public IEnumerable<StorePickupInventoryWarehouseCustomModel> GetInventoryWarehouseForStorePickup(string postalCode, string city, string state, string country, string itemCode, string kitComposition, int kitCompostionLength,
                                                                                                         string unitMeassureCode, int nextRecord, byte recordLimit)
        {
            var models = new List<StorePickupInventoryWarehouseCustomModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Get_EcommerceSystemWarehouseStorePickup, 
                                                postalCode.ToNullStringOrDbQuote(),
                                                city.ToNullStringOrDbQuote(),
                                                state.ToNullStringOrDbQuote(),
                                                country.ToNullStringOrDbQuote(),
                                                itemCode.ToNullStringOrDbQuote(),
                                                kitComposition.ToNullStringOrDbQuote(),
                                                kitCompostionLength,
                                                unitMeassureCode.ToNullStringOrDbQuote(),
                                                nextRecord,
                                                recordLimit))
            {
                while(reader.Read())
                {
                    models.Add(new StorePickupInventoryWarehouseCustomModel()
                    {
                        RowNumber = reader.ToRSFieldLong("RowNum"),
                        WareHouseCode = reader.ToRSField("WarehouseCode"),
                        WareHouseDescription = reader.ToRSField("WarehouseDescription"),
                        Address = new AddressModel()
                        {
                            Address = reader.ToRSField("Address"),
                            PostalCode = reader.ToRSField("PostalCode"),
                            City = reader.ToRSField("City"),
                            State = reader.ToRSField("State"),
                            Country = reader.ToRSField("Country")
                        },
                        Telephone = reader.ToRSField("Telephone"),
                        TelephoneLocalNumber = reader.ToRSField("TelephoneLocalNumber"),
                        TelephoneExtension = reader.ToRSField("TelephoneExtension"),
                        Fax = reader.ToRSField("Fax"),
                        FaxLocalNumber = reader.ToRSField("FaxLocalNumber"),
                        FaxExtension = reader.ToRSField("FaxExtension"),
                        Email = reader.ToRSField("Email"),
                        Website = reader.ToRSField("Website"),
                        Coordinate = new DTO.Coordinate() {
                            Latitude = Convert.ToDecimal(reader.ToRSFieldDouble("Latitude")),
                            Longtitude = Convert.ToDecimal(reader.ToRSFieldDouble("Longitude"))
                        },
                        FreeStock = reader.ToRSFieldDecimal("FreeStock")
                    });
                }
            }

            return models;
        }

        //Helper function to avoid redundancy
        private StoreWorkingHoursModel InternalGetStoreWorkingModel(string day, System.Data.IDataReader reader)
        {
            var model = new StoreWorkingHoursModel()
            {
                Day = day,
                WorkingHour = new WorkingHour()
                {
                    Opening = reader.ToRSFieldDateTime(day + "OpeningTime"),
                    Closing = reader.ToRSFieldDateTime(day + "ClosingTime")
                }
            };

            string fieldName = day + "Closed";
            bool close = true;
            int index = reader.GetOrdinal(fieldName);
            if (reader.IsDBNull(index))
            {
                close = true;
            }
            else
            {
                close = Convert.ToBoolean(reader[fieldName]);
            }

            model.WorkingHour.Close = close;
            return model;
        }

        public IEnumerable<StoreWorkingHoursModel> GetStoreWorkingHours(string warehouseCode)
        { 
            var workingDayHourList = new List<StoreWorkingHoursModel>();

            using(var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadEcommerceSystemWarehouseWorkingHours, warehouseCode.ToDbQuote()))
            {
                if (!reader.Read()) return workingDayHourList;

                string day = "Monday";
                workingDayHourList.Add(InternalGetStoreWorkingModel(day, reader));

                day = "Tuesday";
                workingDayHourList.Add(InternalGetStoreWorkingModel(day, reader));

                day = "Wednesday";
                workingDayHourList.Add(InternalGetStoreWorkingModel(day, reader));

                day = "Thursday";
                workingDayHourList.Add(InternalGetStoreWorkingModel(day, reader));

                day = "Friday";
                workingDayHourList.Add(InternalGetStoreWorkingModel(day, reader));

                day = "Saturday";
                workingDayHourList.Add(InternalGetStoreWorkingModel(day, reader));

                day = "Sunday";
                workingDayHourList.Add(InternalGetStoreWorkingModel(day, reader));
            }

            return workingDayHourList;
        }

        public IEnumerable<StoreHolidayHours> GetStoreHolidayHours(string warehouseCode)
        {
            var models = new List<StoreHolidayHours>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadEcommerceSystemWarehouseWorkingHolidays, warehouseCode.ToDbQuote()))
            {
                while (reader.Read()) {

                    var holiday = new StoreHolidayHours()
                    {
                        Name = reader.ToRSField("Holiday"),
                        Date = reader.ToRSFieldDateTime("HolidayDate"),
                        WorkingDay = new WorkingHour()
                        {
                            Opening = reader.ToRSFieldDateTime("OpeningTime"),
                            Closing = reader.ToRSFieldDateTime("ClosingTime")
                        }
                    };

                    bool close = true;
                    int index = reader.GetOrdinal("Closed");
                    if (reader.IsDBNull(index))
                    {
                        close = true;
                    }
                    else
                    {
                        close = Convert.ToBoolean(reader["Closed"]);
                    }

                    holiday.WorkingDay.Close = close;
                    models.Add(holiday);

                }
            }

            return models;
        }

        public DataRow GetWarehouseRowByCode(string warehouseCode)
        {
            var dataTable = _dataManager.ExecuteSQLByQueryIDReturnTable(DBQueryConstants.ReadInventoryWarehouseByWarehouseCode, warehouseCode.ToDbQuote());
            if(dataTable.Rows.Count > 0)
            {
                return dataTable.Rows[0];
            }

            return null;
        }

        public InventoryWarehouseModel GetWarehouseByCode(string warehouseCode)
        {
            InventoryWarehouseModel model = null;

            using(var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadInventoryWarehouseByWarehouseCode, warehouseCode.ToDbQuote()))
            {
                if(reader.Read())
                {
                    model = new InventoryWarehouseModel()
                    {
                        WareHouseCode = reader.ToRSField("WarehouseCode"),
                        WareHouseDescription = reader.ToRSField("WarehouseDescription"),
                        Address = reader.ToRSField("Address"),
                        City = reader.ToRSField("City"),
                        State = reader.ToRSField("State"),
                        PostalCode = reader.ToRSField("PostalCode"),
                        Country = reader.ToRSField("Country")
                    };
                }
            }

            return model;
        }
    }
}