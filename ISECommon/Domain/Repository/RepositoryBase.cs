﻿using System;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.Exceptions;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public abstract class RepositoryBase : IRepository
    {
        [CLSCompliant(false)]
        protected readonly DataManager _dataManager = null;

        protected RepositoryBase()
        {
            _dataManager = ServiceFactory.GetInstance<DataManager>();
        }

        protected RepositoryBase(DataManager dataManager)
        {
            _dataManager = dataManager;
        }

        public int GetSqlN(string sql)
        {
            int retval = 0;
            using (var reader = _dataManager.ExecuteSQLReturnReader(sql))
            {
                if (reader.Read())
                {
                    retval = reader.ToRSFieldInt("N");
                }
            }
            return retval;
        }

        public string GetSqlS(string sql)
        {
            string retval = String.Empty;
            using (var reader = _dataManager.ExecuteSQLReturnReader(sql))
            {
                if (reader.Read())
                {
                    retval = DB.RSFieldByLocale(reader, "S", System.Threading.Thread.CurrentThread.CurrentUICulture.Name);
                    if (retval.Equals(DBNull.Value))
                    {
                        retval = String.Empty;
                    }
                }
            }
            return retval;
        }

        public int GetSqlNByQueryConfigId(string queryId)
        {
            return GetSqlNByQueryConfigId(queryId, null);
        }

        public int GetSqlNByQueryConfigId(string queryId, params object[] parameters)
        {
            int retval = 0;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(queryId, parameters))
            {
                if (reader.Read())
                {
                    retval = reader.ToRSFieldInt("N");
                }
            }
            return retval;
        }

        public void TestDBConnection()
        {
            _dataManager.TryConnectToDB();
        }

        public string GetQueryConfig(string id)
        {
            return _dataManager.GetQueryConfig(id);
        }

        public string GetQueryConfig(string id, params object[] parameters)
        {
            return _dataManager.GetQueryConfig(id, parameters);
        }
    }
}