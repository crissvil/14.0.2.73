﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    /// <summary>
    /// Restricted tables are:
    /// Customer, CRMContact, EcommerceCustomer
    /// </summary>
    public class CustomerRepository : RepositoryBase, ICustomerRepository
    {
        public Customer Find(Guid contactGuid, string webSiteCode, bool allowCreditHold, bool allowProductFiltering)
        {
            var foundCustomer = Customer.New(contactGuid);
            foundCustomer.IsRegistered = false;

            foundCustomer.AffiliateID = Customer.ro_AffiliateCookieName.ParseCookieValue(true);
            foundCustomer.LastIPAddress = "REMOTE_ADDR".ToServerVariables();

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadFindCustomer, contactGuid.ToString().ToDbQuote(), webSiteCode.ToDbQuote(),
                                                                             Localization.ToDBDateTimeString(DateTime.Now).ToDbQuote()))
            {
                if (!reader.Read()) return foundCustomer;

                foundCustomer.IsRegistered = reader.ToRSFieldBool("IsRegistered");
                foundCustomer.HasCustomerRecord = foundCustomer.IsRegistered || reader.ToRSFieldBool("HasAnonymousRecord");

                //abort the creation if customer is anonymous
                //if (!foundCustomer.HasCustomerRecord)
                //{
                //    reader.Close();
                //    return null;
                //}

                foundCustomer.CustomerCode = reader.ToRSField("CustomerCode");
                foundCustomer.CurrencyCode = reader.ToRSField("CurrencyCode");
                foundCustomer.PrimaryBillingAddressID = reader.ToRSField("DefaultBillToCode");
                foundCustomer.PrimaryShippingAddressID = reader.ToRSField("DefaultShipToCode");
                foundCustomer.DefaultPrice = reader.ToRSField("DefaultPrice");
                foundCustomer.PricingMethod = reader.ToRSField("PricingMethod");
                foundCustomer.PricingLevel = reader.ToRSField("PricingLevel");
                foundCustomer.PricingPercent = reader.ToRSFieldDecimal("PricingPercent");
                foundCustomer.DiscountType = reader.ToRSField("DiscountType");
                foundCustomer.Discount = reader.ToRSFieldDecimal("Discount");
                foundCustomer.DiscountBand = reader.ToRSField("DiscountBand");
                foundCustomer.PaymentTermGroup = reader.ToRSField("PaymentTermGroup");
                foundCustomer.PaymentTermCode = reader.ToRSField("PaymentTermCode");
                foundCustomer.PaymentMethod = reader.ToRSField("PaymentMethod");
                foundCustomer.DefaultBillingCode = reader.ToRSField("DefaultBillingCode");
                foundCustomer.DefaultShippingCode = reader.ToRSField("DefaultShippingCode");
                foundCustomer.LocaleSetting = reader.ToRSField("LocaleSetting");
                foundCustomer.Notes = reader.ToRSField("Notes");
                foundCustomer.LanguageCode = reader.ToRSField("LanguageCode");
                foundCustomer.FirstName = reader.ToRSField("FirstName");
                foundCustomer.Salutation = reader.ToRSField("Salutation");
                foundCustomer.LastName = reader.ToRSField("LastName");
                foundCustomer.EMail = reader.ToRSField("Email");
                foundCustomer.Phone = reader.ToRSField("Phone");
                foundCustomer.WarehouseCode = reader.ToRSField("WarehouseCode");

                foundCustomer.SubscriptionExpiresOn = reader.ToRSFieldDateTime("SubscriptionExpDate");
                foundCustomer.AnonymousCustomerCode = reader.ToRSField("AnonymousCustomerCode");
                foundCustomer.AnonymousShipToCode = reader.ToRSField("AnonymousShipToCode");
                foundCustomer.CouponCode = reader.ToRSField("CouponCode");
                foundCustomer.CurrentSessionID = reader.ToRSFieldInt("CustomerSessionID");
                foundCustomer.LastActivity = reader.ToRSFieldDateTime("LastActivity");
                foundCustomer.Culture = CultureInfo.GetCultureInfo(foundCustomer.LocaleSetting);
                foundCustomer.IsOver13 = reader.ToRSFieldBool("IsOver13");
                foundCustomer.IsUpdatedAnonCustRecord = (reader.ToRSFieldBool("IsUpdated"));
                foundCustomer.ContactCode = reader.ToRSField("ContactCode");
                foundCustomer.ContactFullName = reader.ToRSField("ContactFullName");
                foundCustomer.TaxNumber = reader.ToRSField("TaxNumber");
                foundCustomer.OKToEMail = reader.ToRSFieldBool("IsOkToEmail");
                foundCustomer.IsCreditOnHold = (allowCreditHold) ? false : reader.ToRSFieldBool("IsCreditHold");

                if (allowProductFiltering)
                {
                    if (foundCustomer.IsRegistered)
                    {
                        foundCustomer.ProductFilterID = reader.ToRSFieldGUID("ProductFilterID");
                    }
                    else if (reader.ToRSField("ProductFilterID").IsNullOrEmptyTrimmed())
                    {
                        foundCustomer.ProductFilterID = Guid.Empty.ToString();
                    }
                    else
                    {
                        foundCustomer.ProductFilterID = reader.ToRSFieldGUID("ProductFilterID");
                    }
                }

                bool isBusinessType = DB.RSField(reader, "BusinessType").Equals(Customer.BusinessTypes.WholeSale.ToString(), StringComparison.InvariantCultureIgnoreCase);
                foundCustomer.BusinessType = (isBusinessType) ? Customer.BusinessTypes.WholeSale : Customer.BusinessTypes.Retail;

                if (!foundCustomer.HasCustomerRecord)
                {
                    string localeSettings = Customer.ro_LocaleSettingCookieName.ParseCookieValue(true);

                    //get locale from cookie
                    foundCustomer.LocaleSetting = (localeSettings.IsNullOrEmptyTrimmed()) ? foundCustomer.LocaleSetting : localeSettings;
                    foundCustomer.LanguageCode = AppLogic.GetLanguageCode(foundCustomer.LocaleSetting);
                }
            }

            return foundCustomer;
        }

        public void MakeAnonymousCustomerRecord(out string customerCode, out Guid contactGuid, string webSiteCode)
        {
            customerCode = String.Empty;
            contactGuid = Guid.Empty;

            string currencyCode = String.Empty;
            string newGuid = DB.GetNewGUID();
            string newContactGuid = DB.GetNewGUID();

            //to be modified
            string affiliateID = Customer.RecordAffiliateSessionCookie();

            string localeSetting = String.Empty;

            var anonSettings = GetAnonCustomerSettings(webSiteCode);

            var lstParameter = new List<SqlParameter>();

            // CustomerGuid
            var paramCustomerGuid = new SqlParameter("@CustomerGuid", SqlDbType.UniqueIdentifier);
            paramCustomerGuid.Value = new Guid(newGuid);
            lstParameter.Add(paramCustomerGuid);

            // UserName
            var paramUserName = new SqlParameter("@UserName", SqlDbType.NVarChar, 100);
            paramUserName.Value = string.Format("Anon_{0}", newGuid);
            lstParameter.Add(paramUserName);

            // Password
            var paramPassword = new SqlParameter("@Password", SqlDbType.NVarChar, 100);
            paramPassword.Value = "N/A";
            lstParameter.Add(paramPassword);

            // Referrer
            var paramReferrer = new SqlParameter("@Referrer", SqlDbType.NVarChar, 100);
            paramReferrer.Value = CommonLogic.CookieCanBeDangerousContent("Referrer", true);
            lstParameter.Add(paramReferrer);

            // SalesRep
            var paramSalesRep = new SqlParameter("@SalesRep", SqlDbType.NVarChar, 30);
            paramSalesRep.Value = CommonLogic.IsStringNullOrEmpty(affiliateID) ? "NULL" : affiliateID;
            lstParameter.Add(paramSalesRep);

            // Locale Setting
            var paramLocaleSetting = new SqlParameter("@LocaleSetting", SqlDbType.NVarChar, 30);
            paramLocaleSetting.Value = localeSetting;
            lstParameter.Add(paramLocaleSetting);

            // currency code
            var paramCurrencyCode = new SqlParameter("@CurrencyCode", SqlDbType.NVarChar, 30);
            paramCurrencyCode.Value = currencyCode;
            lstParameter.Add(paramCurrencyCode);

            // last ip address
            var paramLastIPAddress = new SqlParameter("@LastIPAddress", SqlDbType.NVarChar, 20);
            paramLastIPAddress.Value = CommonLogic.ServerVariables("REMOTE_ADDR");
            lstParameter.Add(paramLastIPAddress);

            // ContactGuid
            var paramContactGuid = new SqlParameter("@ContactGuid", SqlDbType.UniqueIdentifier);
            paramContactGuid.Value = new Guid(newContactGuid);
            lstParameter.Add(paramContactGuid);

            _dataManager.ExecuteSP("EcommerceCreateAnonymousCustomer", lstParameter.ToArray());

            MakeAnonCustomerCodeSameAsIDCommand(newContactGuid);

            var latestcontactGuidAndCustomerCode = GetAnonLatestContactGuidAndCustomerCode(newContactGuid);
            if (latestcontactGuidAndCustomerCode != null)
            {
                customerCode = latestcontactGuidAndCustomerCode.CustomerCode;
                contactGuid = latestcontactGuidAndCustomerCode.ContactGuid;
            }
        }

        public AnonCustomerSettings GetAnonCustomerSettings(string webSiteCode)
        {
            AnonCustomerSettings settings = null;
            string preferredLocale = Customer.ro_LocaleSettingCookieName.ParseCookieValue(true);

            using (var reader = _dataManager.ExecuteSQLReturnReader(String.Format("exec EcommerceGetAnonymousCustomerSettings @WebSiteCode={0}", webSiteCode.ToDbQuote())))
            {
                if (reader.Read())
                {
                    settings = new AnonCustomerSettings()
                    {
                        LocaleSettings = (preferredLocale.IsNullOrEmptyTrimmed()) ? reader.ToRSField("LocaleSetting") : preferredLocale,
                        CurrencyCode = reader.ToRSField("CurrencyCode")
                    };
                }
            }

            return settings;
        }

        public void MakeAnonCustomerCodeSameAsIDCommand(string newContactGuid)
        {
            DB.ExecuteSQL(String.Format("UPDATE EcommerceCustomer SET CustomerCode = CustomerID WHERE ContactGUID = {0}", newContactGuid.ToDbQuote()));
        }

        public AnonLatestContactGuidAndCustomerCode GetAnonLatestContactGuidAndCustomerCode(string newGuidString)
        {
            AnonLatestContactGuidAndCustomerCode param = null;
            using (var reader = _dataManager.ExecuteSQLReturnReader(
                        String.Format("SELECT CustomerCode, ContactGUID FROM EcommerceCustomer with (NOLOCK) WHERE ContactGUID = {0}", newGuidString.ToDbQuote())))
            {
                if (reader.Read())
                {
                    param = new AnonLatestContactGuidAndCustomerCode()
                    {
                        ContactGuid = new Guid(reader.ToRSFieldGUID("ContactGUID")),
                        CustomerCode = reader.ToRSField("CustomerCode")
                    };
                }
            }

            return param;
        }

        public string GetAnonEmail(string customerCode)
        {
            string email = String.Empty;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadAnonEmail, customerCode))
            {
                if (reader.Read())
                {
                    email = reader.ToRSField("Email");
                }
            }
            return email;
        }

        //HasOrders
        public bool IsCustomerHasOrders(string customerID)
        {
            return (GetSqlN(String.Format("SELECT COUNT(ordernumber) AS N FROM orders WITH (NOLOCK) WHERE customerid={0}", customerID.ToString())) > 0);
        }

        //HasAtLeastOneAddress
        public bool IsCustomerHasAtLeastOneAddress(string CustomerID)
        {
            return (GetSqlN(String.Format("SELECT COUNT(CustomerCode) AS N FROM Customer WITH (NOLOCK) WHERE CustomerCode={0}", CustomerID.ToDbQuote())) > 0);
        }

        //HasUsedCoupon
        public bool IsCustomerHasUsedCoupon(string couponCode, string customerCode)
        {
            return (GetSqlN(String.Format("SELECT COUNT(SalesOrderCode) AS N FROM CustomerSalesOrder with (NOLOCK) WHERE BillToCode={0} AND LOWER(CouponCode)={1}", customerCode.ToDbQuote(), couponCode.ToLower().ToDbQuote())) != 0);
        }

        //AnyCustomerHasUsedCoupon
        public bool AnyCustomerHasUsedCoupon(string couponCode)
        {
            return (GetSqlN(String.Format("SELECT COUNT(SalesOrderCode) AS N FROM CustomerSalesOrder WITH (NOLOCK) WHERE LOWER(CouponCode)={0}", couponCode.ToLower().ToDbQuote())) != 0);
        }

        //CustomerCanAvailOfThisCoupon
        public bool CustomerCanAvailOfThisCoupon(string customerCode, string couponCode)
        {
            string couponID = String.Empty;
            using (var reader = _dataManager.ExecuteSQLReturnReader(String.Format("SELECT CouponID FROM CustomerSalesCoupon with (NOLOCK) WHERE LOWER(CouponCode)={0}", couponCode.ToLowerInvariant())))
            {
                if (reader.Read())
                {
                    couponID = reader.ToRSField("CouponID");
                }
            }
            return (GetSqlN(String.Format("SELECT COUNT(CustomerCode) AS N FROM CustomerCouponCustomer  with (NOLOCK) WHERE CustomerCode={0} AND CouponID=", customerCode.ToDbQuote(), couponID.ToDbQuote())) != 0);
        }

        public string GetPassword(string contactCode)
        {
            string password = String.Empty;

            try
            {
                string pwd64, salt64, iv64;
                pwd64 = salt64 = iv64 = String.Empty;
                using (var reader = _dataManager.ExecuteSQLReturnReader(GetQueryConfig(DBQueryConstants.ReadPassword, contactCode.ToDbQuote())))
                {
                    if (reader.Read())
                    {
                        pwd64 = DB.RSField(reader, "Password");
                        salt64 = DB.RSField(reader, "PasswordSalt");
                        iv64 = DB.RSField(reader, "PasswordIV");
                    }
                }

                byte[] pwd = Convert.FromBase64String(pwd64);
                byte[] salt = Convert.FromBase64String(salt64);
                byte[] iv = Convert.FromBase64String(iv64);

                password = InterpriseHelper.Decryption(pwd, salt, iv);
            }
            catch
            {
                password = string.Empty;
            }

            return password;
        }

        /// <summary>
        /// In replacement of Customer.OwnsThisAddress(String CustomerCode, String ShipToCode)
        /// </summary>
        public bool OwnsThisAddress(string customerCode, string shipToCode)
        {
            return (GetSqlNByQueryConfigId(DBQueryConstants.ReadOwnsThisAddress, customerCode.ToDbQuote(), shipToCode.ToDbQuote()) > 0);
        }

        public void ClearCouponForRegisteredCustomer(string customerCode)
        {
            _dataManager.ExecuteSQL(
                String.Format("UPDATE Customer SET CouponCode = NULL WHERE CustomerCode={0}", customerCode.ToDbQuote()));
        }

        public void ClearCouponForAnonCustomer(string customerID)
        {
            _dataManager.ExecuteSQL(
                String.Format("UPDATE EcommerceCustomer SET CouponCode = NULL WHERE CustomerID=", customerID.ToDbQuote()));
        }

        public Customer FindByEmailAndPassword(string email, string password, string webSiteCode)
        {
            Customer foundCustomer = null;

            if (email.IsNullOrEmptyTrimmed() || password.IsNullOrEmptyTrimmed()) return foundCustomer;

            var availableLogins = new List<Customer.LoginLogic>();

            string loggedContactCode = String.Empty;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetEcommerceLoginInfo, email.ToDbQuote(), webSiteCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    Customer.LoginLogic login = null;

                    try
                    {
                        var issiLogin = new Customer.InterpriseLoginLogic(
                            new Guid(reader.ToRSFieldGUID("ContactGUID")),
                            reader.ToRSField("Password"),
                            Convert.FromBase64String(reader.ToRSField("PasswordSalt")),
                            Convert.FromBase64String(reader.ToRSField("PasswordIV"))
                        );

                        issiLogin.DefaultContact = reader.ToRSField("DefaultContact");
                        issiLogin.ContactCode = reader.ToRSField("ContactCode");
                        loggedContactCode = issiLogin.ContactCode;

                        login = issiLogin;
                    }
                    catch
                    {
                        // something could have happened.. like salt is null or empty string, in that case it's not a valid Base64 String

                        // this is guaranteed null if an exception has been thrown
                        // but let's make it explicit for readability...
                        login = null;
                    }

                    if (null != login && login.Equals(password))
                    {
                        foundCustomer = Customer.Find(login.ID);
                        break;
                    }

                    try
                    {
                        int? saltKey = reader.ToRSField("PasswordSalt").TryParseInt();
                        if (saltKey.HasValue)
                        {
                            login = new Customer.MLImportedLoginLogic(
                                new Guid(reader.ToRSFieldGUID("ContactGUID")),
                                saltKey.Value,
                                reader.ToRSField("Password")
                            );
                        }
                    }
                    catch
                    {
                        // something could have happened.. like salt is null or empty string or not integer etc., in that case it's an invalid ML saltKey

                        // this is guaranteed null if an exception has been thrown
                        // but let's make it explicit for readability...
                        login = null;
                    }

                    if (null != login && login.Equals(password))
                    {
                        foundCustomer = Customer.Find(login.ID);
                        Customer.Current.ThisCustomerSession["InvalidPassword"] = "false";
                        break;
                    }
                    else
                    {
                        Customer.Current.ThisCustomerSession["InvalidPassword"] = "true";
                    }
                }
            }

            return foundCustomer;
        }

        public bool ValidateContactSubscription(string contactCode, DateTime currentDate, DateTime subExpDate, string webSiteCode)
        {
            bool isEnabled = true;

            string query = "SELECT IsEnabled, SubscriptionExpDate FROM EcommerceCustomerActiveSites with (NOLOCK) WHERE ContactCode = {0} AND WebSiteCode = {1}";
            using (var reader = _dataManager.ExecuteSQLReturnReader(query,contactCode.ToDbQuote(), webSiteCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    isEnabled = reader.ToRSFieldBool("IsEnabled");
                    subExpDate = reader.ToRSFieldDateTime("SubscriptionExpDate");
                }
            }

            //Check first if the contact is allowed access on the website
            if (!isEnabled)
            {
                return false;
            }
            else
            {
                //Contact is allowed on the site
                //Check if subscription date is still valid
                if (currentDate > subExpDate)
                {
                    return false;
                }
            }

            //Validation passed
            return true;
        }

        public void ClearCustomerSession(Guid customerGuid, DateTime currentDate)
        {
            _dataManager.ExecuteSQL(String.Format("exec EcommerceSessionAge @CurrentDate = {0}, @CustomerGuid = {1}",
                    Localization.ToDBDateTimeString(currentDate).ToDbQuote(),
                    (customerGuid == Guid.Empty) ? "NULL" : DB.SQuote(customerGuid.ToString()))
            );
        }

        public void CreateContactSiteLog(string contactCode, string details, DateTime currentDate, string webSiteCode, string userCode)
        {
            _dataManager.ExecuteSQL(String.Format("INSERT INTO EcommerceCustomerActiveSitesLog (LogID, ContactCode, WebSiteCode, LogDateTime, LogDetails, UserCreated, DateCreated, UserModified, DateModified) VALUES({0}, {1}, {2}, {3}, {4}, {5}, {6}, {5}, {6})",
                Guid.NewGuid().ToString().ToDbQuote(),
                contactCode.ToDbQuote(),
                webSiteCode.ToDbQuote(),
                Localization.ToDBDateTimeString(currentDate).ToDbQuote(),
                details.ToDbQuote(),
                userCode.ToDbQuote(),
                Localization.ToDBDateTimeString(currentDate).ToDbQuote()
            ));

        }

        public Guid? GetContactCodeByActiveShoppersCode(string code)
        {
            Guid? contactCode = null;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadEcommerceActiveShoppersView, code.ToDbQuote()))
            {
                if (reader.Read())
                {
                    string guid = reader["ContactGuid"].ToString();
                    contactCode = new Guid(guid);
                }
            }
            return contactCode;
        }

        /// <summary>
        /// In replacement of Customer.IsEmailAlreadyInUse(string email, Guid notByThisCustomer)
        /// </summary>
        public bool IsEmailAlreadyInUse(string email, Guid notByThisCustomer, string webSiteCode)
        {
            return (GetSqlNByQueryConfigId(DBQueryConstants.ReadIsEmailAlreadyInUse,
                                email.ToDbQuote(),
                                (Guid.Empty.Equals(notByThisCustomer) ? String.Empty : String.Format("AND cc.ContactGUID <> '{0}'", notByThisCustomer)),
                                webSiteCode.ToDbQuote()) > 0);
        }

        /// <summary>
        /// In replacement of Customer.IsEmailAlreadyInUse(string email)
        /// </summary>
        public bool IsEmailAlreadyInUse(string email, string webSiteCode)
        {
            return IsEmailAlreadyInUse(email, Guid.Empty, webSiteCode);
        }

        /// <summary>
        /// In replacement of Customer.EmailInUse(string email, string CustomerCode)
        /// </summary>
        public bool EmailInUse(string email, string CustomerCode)
        {
            return (
                    !email.IsNullOrEmptyTrimmed() &&
                    GetSqlNByQueryConfigId(DBQueryConstants.ReadEmailInUse, email.ToDbQuote(), CustomerCode.ToDbQuote()) > 0
                   );
        }

        public string CreateAnonCustomer(string localeSettings, string currencyCode)
        {
            string customerGuid = Guid.NewGuid().ToString();
            string contactGuid = Guid.NewGuid().ToString();
            string userName = String.Format("Anon_{0}", customerGuid);
            string password = "N/A";
            string referrer = CommonLogic.CookieCanBeDangerousContent("Referrer", true);
            string affiliateId = Customer.RecordAffiliateSessionCookie();
            affiliateId = affiliateId.IsNullOrEmptyTrimmed() ? "NULL" : affiliateId.ToDbQuote();
            string ipAddress = "REMOTE_ADDR".ToServerVariables();

            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.CreateCreateAnonCustomer, customerGuid.ToDbQuote(), userName.ToDbQuote(), password.ToDbQuote(), referrer.ToDbQuote(),
                                             affiliateId, localeSettings.ToDbQuote(), currencyCode.ToDbQuote(), ipAddress.ToDbQuote(), contactGuid.ToDbQuote());

            return contactGuid;
        }

        public void UpdateEcommerceCustomerCodeToID(string contactGuid)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateUpdateCustomerCodeToCustomerId, contactGuid.ToDbQuote());
        }

        public string GetUpdatedCustomerIdByContactGuid(string contactGuid)
        {
            string newCustomerID = String.Empty;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadUpdatedCustomerCodeByContactGuid, contactGuid.ToDbQuote()))
            {
                if (reader.Read())
                {
                    newCustomerID = DB.RSField(reader, "CustomerCode");
                }
            }

            return newCustomerID;
        }

        public void UpdateCustomer(CustomerInfo customerInfo)
        {
            var paramArray = new List<SqlParameter>();

            var contactParam = new SqlParameter("@ContactGuid", SqlDbType.UniqueIdentifier);
            contactParam.Value = customerInfo.ContactGuid;
            paramArray.Add(contactParam);

            paramArray.Add(new SqlParameter("@FirstName", SqlDbType.NVarChar, 50)
            {
                Value = customerInfo.FirstName
            });

            paramArray.Add(new SqlParameter("@LastName", SqlDbType.NVarChar, 50)
            {
                Value = customerInfo.LastName
            });

            paramArray.Add(new SqlParameter("@SalutationCode", SqlDbType.NVarChar, 30)
            {
                Value = customerInfo.Salutation
            });

            paramArray.Add(new SqlParameter("@Email", SqlDbType.NVarChar, 50)
            {
                Value = customerInfo.Email
            });

            paramArray.Add(new SqlParameter("@Phone", SqlDbType.NVarChar, 50){
                Value = customerInfo.Phone
            });

            paramArray.Add(new SqlParameter("@Mobile", SqlDbType.NVarChar, 50)
            {
                Value = customerInfo.Mobile
            });

            paramArray.Add(new SqlParameter("@IsOver13", SqlDbType.Bit) {
                Value = customerInfo.IsOver13
            });

            paramArray.Add(new SqlParameter("@EmailRule", SqlDbType.NVarChar, 20){
                Value = customerInfo.EmailRule
            });

            paramArray.Add(new SqlParameter("@IsOkToEmail", SqlDbType.Bit)
            {
                Value = customerInfo.IsOkToEmail
            });

            paramArray.Add(new SqlParameter("@BusinessType", SqlDbType.NVarChar, 30)
            {
                Value = customerInfo.BusinessType.ToString()
            });

            paramArray.Add(new SqlParameter("@TaxNumber", SqlDbType.NVarChar, 50)
            {
                Value = customerInfo.TaxNumber
            });

            paramArray.Add(new SqlParameter("@Password", SqlDbType.NVarChar, 50){
                Value = customerInfo.Password.ToStringOrDBNull()
            });

            paramArray.Add(new SqlParameter("@PasswordSalt", SqlDbType.NVarChar, 50)
            {
                Value = customerInfo.Salt.ToStringOrDBNull()
            });

            paramArray.Add(new SqlParameter("@PasswordIV", SqlDbType.NVarChar, 50)
            {
                Value = customerInfo.Vector.ToStringOrDBNull()
            });

            _dataManager.ExecuteSP(GetQueryConfig(DBQueryConstants.UpdateECommerceUpdateCustomer), paramArray.ToArray());
        }

        public string GetCBNCustomerCode(int cbnNetworkId)
        {
            string tmpS = String.Empty;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetCBNCustomerCode, cbnNetworkId.ToString()))
            {
                if (reader.Read())
                {
                    tmpS = reader.ToRSField("CustomerCode");
                }
            }
            return tmpS;
        }

        public SalesOrderHistoryCollection GetCustomerSalesOrders(int pages, int current, bool showCustomerServiceNotesInReceipts, string websiteCode, Customer thisCustomer)
        {
            if (CommonLogic.GetWebsiteType() == WebsiteType.Company)
            {
                return GetCompanySalesOrders(pages, current, showCustomerServiceNotesInReceipts, websiteCode, thisCustomer);
            }

            var allOrders = new SalesOrderHistoryCollection();
            CultureInfo locale = new CultureInfo(thisCustomer.LocaleSetting);
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadEcommerceCustomerWebOrder,
                                                            thisCustomer.CustomerCode.ToDbQuote(),
                                                            websiteCode.ToDbQuote(),
                                                            pages,
                                                            current,
                                                            CommonLogic.IIF(CommonLogic.GetWebsiteType() == WebsiteType.Company, 1, 0),
                                                            CommonLogic.IIF(thisCustomer.ThisCustomerSession["drpContactList"].Length > 0, thisCustomer.ThisCustomerSession["drpContactList"].ToDbQuote(), thisCustomer.ContactCode.ToDbQuote())))
            {
                if (reader.Read())
                {
                    allOrders.rows = reader.ToRSFieldInt("Rows");
                    allOrders.current = reader.ToRSFieldInt("Current");
                    allOrders.pages = reader.ToRSFieldInt("Pages");
                    allOrders.allPages = reader.ToRSFieldInt("AllPages");
                    allOrders.start = reader.ToRSFieldInt("Start");
                    allOrders.end = reader.ToRSFieldInt("End");
                }


                // now let's read the details if any
                if (allOrders.rows > 0 && reader.NextResult())
                {
                    while (reader.Read())
                    {
                        var orderDetails = new SalesOrderHistoryDTO();
                        orderDetails.row = reader.ToRSFieldInt("Row");
                        orderDetails.salesOrderCode = reader.ToRSField("SalesOrderCode");
                        orderDetails.invoiceCode = reader.ToRSField("InvoiceCode");
                        orderDetails.salesOrderDate = reader.ToRSFieldDateTime("SalesOrderDate").ToString("d", locale);
                        orderDetails.total = reader.ToRSFieldDecimal("TotalRate").ToCustomerCurrency();

                        if (showCustomerServiceNotesInReceipts)
                        {
                            orderDetails.notes = reader.ToRSField("Notes").Length == 0 ? "None" : reader.ToRSField("Notes");
                        }

                        if (reader.ToRSFieldDateTime("ShippingDate") != DateTime.MinValue)
                        {
                            orderDetails.shippingDate = reader.ToRSFieldDateTime("ShippingDate").ToString("d", locale);
                            orderDetails.trackingNumber = reader.ToRSField("TrackingNumber");
                            orderDetails.trackingURL = reader.ToRSField("TrackingLink");
                        }
                        else
                        {
                            orderDetails.shippingStatus = AppLogic.GetString("account.aspx.20");

                        }

                        orderDetails.paymentMethod = reader.ToRSField("PaymentMethod");
                        orderDetails.paymentStatus = reader.ToRSField("TransactionState");
                        orderDetails.electronicDownloadItemCount = reader.ToRSFieldInt("ElectronicDownloadItemCount");
                        orderDetails.location = reader.ToRSField("ContactName");
                        if (orderDetails.electronicDownloadItemCount > 0)
                        {
                            if (reader.ToRSFieldDateTime("DownloadEmailSentDate") != DateTime.MinValue)
                            {
                                orderDetails.shippingStatus = String.Format(AppLogic.GetString("account.aspx.31"),
                                                                            reader.ToRSFieldDateTime("DownloadEmailSentDate").ToString("d", thisCustomer.Culture));
                            }
                            else
                            {
                                orderDetails.shippingStatus = AppLogic.GetString("account.aspx.21");
                            }
                        }
                        allOrders.orders.Add(orderDetails);
                    }
                }
            }
            thisCustomer.ThisCustomerSession.ClearVal("drpContactList");
            return allOrders;
        }

        private SalesOrderHistoryCollection GetCompanySalesOrders(int pages, int current, bool showCustomerServiceNotesInReceipts, string websiteCode, Customer thisCustomer)
        {
            string storeAdminCode = thisCustomer.ThisCustomerSession["MYOBMovexCode"];
            
            var allOrders = new SalesOrderHistoryCollection();
            CultureInfo locale = new CultureInfo(thisCustomer.LocaleSetting);
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadEcommerceCompanyWebOrder,
                                                            thisCustomer.CustomerCode.ToDbQuote(),
                                                            websiteCode.ToDbQuote(),
                                                            storeAdminCode.ToDbQuote(),
                                                            pages,
                                                            current,
                                                            1,
                                                            CommonLogic.IIF(thisCustomer.ThisCustomerSession["drpContactList"].Length > 0, thisCustomer.ThisCustomerSession["drpContactList"].ToDbQuote(), thisCustomer.ContactCode.ToDbQuote())))
            {
                if (reader.Read())
                {
                    allOrders.rows = reader.ToRSFieldInt("Rows");
                    allOrders.current = reader.ToRSFieldInt("Current");
                    allOrders.pages = reader.ToRSFieldInt("Pages");
                    allOrders.allPages = reader.ToRSFieldInt("AllPages");
                    allOrders.start = reader.ToRSFieldInt("Start");
                    allOrders.end = reader.ToRSFieldInt("End");
                }


                // now let's read the details if any
                if (allOrders.rows > 0 && reader.NextResult())
                {
                    while (reader.Read())
                    {
                        var orderDetails = new SalesOrderHistoryDTO();
                        orderDetails.row = reader.ToRSFieldInt("Row");
                        orderDetails.salesOrderCode = reader.ToRSField("SalesOrderCode");
                        orderDetails.invoiceCode = reader.ToRSField("InvoiceCode");
                        orderDetails.salesOrderDate = reader.ToRSFieldDateTime("SalesOrderDate").ToString("d", locale);
                        orderDetails.total = reader.ToRSFieldDecimal("TotalRate").ToCustomerCurrency();

                        if (showCustomerServiceNotesInReceipts)
                        {
                            orderDetails.notes = reader.ToRSField("Notes").Length == 0 ? "None" : reader.ToRSField("Notes");
                        }

                        if (reader.ToRSFieldDateTime("ShippingDate") != DateTime.MinValue)
                        {
                            orderDetails.shippingDate = reader.ToRSFieldDateTime("ShippingDate").ToString("d", locale);
                            orderDetails.trackingNumber = reader.ToRSField("TrackingNumber");
                            orderDetails.trackingURL = reader.ToRSField("TrackingLink");
                        }
                        else
                        {
                            orderDetails.shippingStatus = AppLogic.GetString("account.aspx.20");

                        }

                        orderDetails.paymentMethod = reader.ToRSField("PaymentMethod");
                        orderDetails.paymentStatus = reader.ToRSField("TransactionState");
                        orderDetails.electronicDownloadItemCount = reader.ToRSFieldInt("ElectronicDownloadItemCount");
                        orderDetails.location = reader.ToRSField("ContactName");
                        if (orderDetails.electronicDownloadItemCount > 0)
                        {
                            if (reader.ToRSFieldDateTime("DownloadEmailSentDate") != DateTime.MinValue)
                            {
                                orderDetails.shippingStatus = String.Format(AppLogic.GetString("account.aspx.31"),
                                                                            reader.ToRSFieldDateTime("DownloadEmailSentDate").ToString("d", thisCustomer.Culture));
                            }
                            else
                            {
                                orderDetails.shippingStatus = AppLogic.GetString("account.aspx.21");
                            }
                        }
                        allOrders.orders.Add(orderDetails);
                    }
                }
            }
            thisCustomer.ThisCustomerSession.ClearVal("drpContactList");
            return allOrders;
        }

        public bool IsActiveShopperCodeNotUsed(string customerCode)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetIsActiveShopperCodeUsed, customerCode.ToDbQuote()))
            {
                return !reader.Read();
            }
        }

        public void DeleteInActiveShoppers(DateTime currentTime, int timeLimit)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.DeleteInActiveShoppers, Localization.ToDBDateTimeString(currentTime).ToDbQuote(), timeLimit.ToString());
        }

        public void AddActiveShoppers(ActiveShopper activeShopper)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.CreateActiveShoppers,
                                            activeShopper.ActiveShopperId.ToDbQuote(),
                                            activeShopper.RequestCode.ToDbQuote(),
                                            activeShopper.CustomerCode.ToDbQuote(),
                                            activeShopper.ContactGuid.ToString().ToDbQuote(),
                                            activeShopper.WebsiteCode.ToDbQuote(),
                                            Localization.ToDBDateTimeString(activeShopper.TimeStarted).ToDbQuote(),
                                            Localization.ToDBDateTimeString(activeShopper.DateCreated).ToDbQuote());
        }

        public string GetShopperRequestCodeByCustomerCode(string customerCode)
        {
            string code = String.Empty;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetActiveShopperRequestCodeByCustomerCode, customerCode.ToDbQuote()))
            {
                reader.Read();
                code = reader.ToRSField("RequestCode");
            }

            return code;
        }

        public IEnumerable<SystemWareHouse> GetDealerCustomersAsWareHouse()
        {
            var warehouseFromCustomer = new List<SystemWareHouse>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadDealersFromCustomerAsWareHouse))
            {
                while (reader.Read())
                {
                    warehouseFromCustomer.Add(new SystemWareHouse()
                    {
                        Coordinate = new Coordinate()
                        {
                            Latitude = reader.ToRSFieldDouble("Latitude").ToString().TryParseDecimal().Value,
                            Longtitude = reader.ToRSFieldDouble("Longitude").ToString().TryParseDecimal().Value
                        },
                        Description = reader.ToRSField("Description"),
                        Address = new AddressDTO()
                        {
                            address = reader.ToRSField("address"),
                            state = reader.ToRSField("state"),
                            postalCode = reader.ToRSField("postalcode"),
                            country = reader.ToRSField("country"),
                            city = reader.ToRSField("city"),
                            id = reader.ToRSFieldInt("Counter").ToString(),
                            phone = reader.ToRSField("Telephone")
                        }
                    });    
                }
            }
            return warehouseFromCustomer;
        }

        public string GetCustomerNotes(string customerCode)
        {
            return GetSqlS(GetQueryConfig(DBQueryConstants.GetCustomerNotes, customerCode.ToDbQuote()));
        }

        public void UpdateContactDefaultShippingCode(string addressId, string contactCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateContactDefaultShippingCode, addressId.ToDbQuote(), contactCode.ToDbQuote());
        }

        public void UpdateContactDefaultBillingCode(string addressId, string contactCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateContactDefaultBillingCode, addressId.ToDbQuote(), contactCode.ToDbQuote());
        }

        public void ClearCustomerShippingAddressPlus4Field(string code)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateCustomerShiptoPlus4ToNull, code.ToDbQuote());
        }

        public void ClearCustomerBillingAddressPlus4Field(string code)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateCustomerBillToPlus4ToNull, code.ToDbQuote());
        }

        public RememberMeCustomModel GetRememberMeInfo(Guid contactGuid, string websiteCode)
        {
            RememberMeCustomModel returnModel = null;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(
                                            DBQueryConstants.GetEcommerceGetRememberMeInfo, contactGuid.ToString().ToDbQuote(), websiteCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    returnModel = new RememberMeCustomModel()
                    {
                        Email = reader.ToRSField("Email"),
                        Password = reader.ToRSField("Password"),
                        Salt = reader.ToRSField("PasswordSalt"),
                        Vector = reader.ToRSField("PasswordIV"),
                    };
                }
            }

            return returnModel;
        }

        public Address GetCustomerTransactionAddress(string orderNumber, bool isInvoice, bool isBilling)
        {
            Address returnAddress = null;

            var addressType = isBilling ? AddressTypes.Billing : AddressTypes.Shipping;
            string transactionType = isInvoice ? Interprise.Framework.Base.Shared.Const.CUSTOMER_INVOICE : Interprise.Framework.Base.Shared.Const.CUSTOMER_SALES_ORDER;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(
                                           DBQueryConstants.ReadCustomerTransactionAddress, addressType.ToString().ToDbQuote(), orderNumber.ToDbQuote(), transactionType.ToDbQuote()))
            {
                if (reader.Read())
                {
                    returnAddress = new Address();
                    returnAddress.ContactCode = reader.ToRSField("ContactCode") ;
                    returnAddress.Address1 = isBilling ?  reader.ToRSField("BillToAddress") : reader.ToRSField("ShipToAddress");
                    returnAddress.Country = isBilling ? reader.ToRSField("BillToCountry") : reader.ToRSField("ShipToCountry");
                    returnAddress.PostalCode = isBilling ? reader.ToRSField("BillToPostalCode") : reader.ToRSField("ShipToPostalCode");
                    returnAddress.City = isBilling ? reader.ToRSField("BillToCity") : reader.ToRSField("ShipToCity");
                    returnAddress.State = isBilling ? reader.ToRSField("BillToState") : reader.ToRSField("ShipToState");
                    returnAddress.County = isBilling ? reader.ToRSField("BillToCounty") : reader.ToRSField("ShipToCounty");
                    returnAddress.Name = isBilling ? reader.ToRSField("BillToName") : reader.ToRSField("ShipToName");
                    returnAddress.Phone = isBilling ? reader.ToRSField("BillToPhone") : reader.ToRSField("ShipToPhone");
                    returnAddress.CountryISOCode = reader.ToRSField("ISOCode");
                    returnAddress.EMail = reader.ToRSField("Email1").IsNullOrEmptyTrimmed() ?  reader.ToRSField("Email2") :  reader.ToRSField("Email1");        
                }
            }

            return returnAddress;
        }

        public bool IsVoidedCustomerInvoice(string invoiceCode)
        {
            return (GetSqlNByQueryConfigId(DBQueryConstants.GetCustomerInvoiceVoidStatus, invoiceCode.ToDbQuote()) > 0);
        }

        public bool IsVoidedCustomerSalesOrder(string salesOrderCode)
        {
            return (GetSqlNByQueryConfigId(DBQueryConstants.GetCustomerSalesOrderVoidStatus, salesOrderCode.ToDbQuote()) > 0);
        }

        public void UpdateCustomerRequiredAge(string customerCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.UpdateCustomerRequiredAge, customerCode.ToDbQuote());
        }

        public CustomerTransactionModel GetCustomerTransaction(string documentCode)
        {
            CustomerTransactionModel returnModel = null;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadECommerceCustomerTransaction, documentCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    returnModel = new CustomerTransactionModel()
                    {
                        DocumentDate = reader.ToRSFieldDateTime("DocumentDate"),
                        CustomerName = reader.ToRSField("CustomerName"),
                        Total = reader.ToRSFieldDecimal("Total"),
                        Outstanding = reader.ToRSFieldDecimal("Outstanding"),
                        DueDate = reader.ToRSFieldDateTime("DueDate"),
                        Type = reader.ToRSField("Type"),
                        CustomerCode = reader.ToRSField("CustomerCode"),
                        ContactGUID = reader.ToRSFieldTrueGUID("ContactGUID"),
                        IsPosted = reader.ToRSFieldBool("IsPosted"),
                        IsVoided = reader.ToRSFieldBool("IsVoided"),
                        IsPaid = reader.ToRSFieldBool("IsPaid"),
                    };
                }
            }

            return returnModel;
        }

        public CustomerLoyaltyPointsCustomModel GetCustomerLoyaltyPoints(string customerCode)
        {
            return GetCustomerLoyaltyPoints(customerCode, Decimal.Zero);
        }

        public CustomerLoyaltyPointsCustomModel GetCustomerLoyaltyPoints(string customerCode, decimal redemptionMultiplier)
        {
            CustomerLoyaltyPointsCustomModel customerLoyaltyPoints = null;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadCustomerLoyaltyPoints, customerCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    customerLoyaltyPoints = new CustomerLoyaltyPointsCustomModel()
                    {
                        AccumulatedPoints = reader.ToRSFieldDecimal("AccumulatedPoints"),
                        OutstandingPoints = reader.ToRSFieldDecimal("OutstandingPoints"),
                        RedeemedPoints = reader.ToRSFieldDecimal("RedeemedPoints"),
                        ReservedPoints = reader.ToRSFieldDecimal("ReservedPoints")
                    };
                    customerLoyaltyPoints.RemainingPoints = customerLoyaltyPoints.OutstandingPoints - customerLoyaltyPoints.ReservedPoints;
                    customerLoyaltyPoints.RemainingPointsFormatted = customerLoyaltyPoints.RemainingPoints.ToNumberFormat();
                    customerLoyaltyPoints.MonetizedRemainingPoints = (customerLoyaltyPoints.RemainingPoints * redemptionMultiplier).ToCustomerRoundedMonetary();
                    customerLoyaltyPoints.MonetizedRemainingPointsFormatted = customerLoyaltyPoints.MonetizedRemainingPoints.ToCustomerCurrency();
                }
            }
            return customerLoyaltyPoints;
        }

        public string GetCustomerAdvancePreference(string name)
        {
            string configValue = String.Empty;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetCustomerAdvancePreference, name.ToDbQuote()))
            {
                if (reader.Read())
                {
                    configValue = reader.ToRSField("Value");
                }
            }
            return configValue;
        }

        public string GetTransactionType(string documentCode)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetTransactionType, documentCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    return reader.ToRSField("Type");
                }
            }

            return String.Empty;
        }

        public OpenInvoicesCollection GetCustomerOpenInvoices(int pages, int current, string websiteCode, string customerCode)
        {
            var invoices = new OpenInvoicesCollection();

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadEcommerceCustomerOpenInvoices,
                                                            customerCode.ToDbQuote(),
                                                            websiteCode.ToDbQuote(),
                                                            pages,
                                                            current))
            {
                if (reader.Read())
                {
                    invoices.rows = reader.ToRSFieldInt("Rows");
                    invoices.current = reader.ToRSFieldInt("Current");
                    invoices.pages = reader.ToRSFieldInt("Pages");
                    invoices.allPages = reader.ToRSFieldInt("AllPages");
                    invoices.start = reader.ToRSFieldInt("Start");
                    invoices.end = reader.ToRSFieldInt("End");
                }

                if (invoices.rows > 0 && reader.NextResult())
                {
                    while (reader.Read())
                    {
                        var invoiceDetails = new OpenInvoicesDTO();
                        invoiceDetails.Row = reader.ToRSFieldInt("Row");
                        invoiceDetails.InvoiceCode = reader.ToRSField("InvoiceCode");
                        invoiceDetails.InvoiceDate = reader.ToRSFieldDateTime("InvoiceDate").ToShortDateString();
                        invoiceDetails.InvoiceDueDate = reader.ToRSFieldDateTime("InvoiceDueDate").ToShortDateString();
                        invoiceDetails.DueTotal = reader.ToRSFieldDecimal("DueTotal").ToCustomerCurrency();
                        invoiceDetails.Balance =  reader.ToRSFieldDecimal("Balance").ToCustomerCurrency();
                        invoiceDetails.Payments = reader.ToRSFieldDecimal("Payments").ToCustomerCurrency();
                        invoiceDetails.Status = reader.ToRSField("Status");

                        invoices.invoice.Add(invoiceDetails);
                    }
                }
            }

            return invoices;
        }

        public IEnumerable<CustomerCreditCustomModel> GetCustomerCredits(string customerCode)
        {
            var credits = new List<CustomerCreditCustomModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_CustomerCredit, customerCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var credit = new CustomerCreditCustomModel()
                    {
                        CreditCode = reader.ToRSField("CreditCode"),
                        Type = reader.ToRSField("Type"),
                        CreditAmountRate = reader.ToRSFieldDecimal("CreditAmountRate"),
                        CreditAllocatedRate = reader.ToRSFieldDecimal("CreditAllocatedRate"),
                        CreditReservedRate = reader.ToRSFieldDecimal("CreditReservedRate"),
                        CreditAvailableRate = reader.ToRSFieldDecimal("CreditAvailableRate"),
                    };
                    credit.CreditRemainingBalance = credit.CreditAmountRate - credit.CreditAllocatedRate - credit.CreditReservedRate;
                    credit.CreditRemainingBalanceFormatted = credit.CreditRemainingBalance.ToCustomerCurrency();
                    credits.Add(credit);
                }
            }
            return credits;
        }

        public string GetSalutation(string name)
        {
            string salutation = String.Empty;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Get_Salutation, name.ToDbQuote()))
            {
                if (reader.Read())
                {
                    salutation = reader.ToRSField("Salutation");
                }
            }

            return salutation;
        }

        public string GetSuffix(string name)
        {
            string suffix = String.Empty;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Get_Suffix, name.ToDbQuote()))
            {
                if (reader.Read())
                {
                    suffix = reader.ToRSField("Suffix");
                }
            }

            return suffix;
        }

        #region Gift Codes

        public IEnumerable<GiftCodeCustomModel> GetCustomerGiftCodes(string customerCode)
        {
            var giftCodes = new List<GiftCodeCustomModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadGiftCodesByCustomerCode, customerCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var giftCode = new GiftCodeCustomModel()
                    {
                        Type = reader.ToRSField("Type"),
                        BillToCode = reader.ToRSField("BillToCode"),
                        SerialCode = reader.ToRSField("SerialLotNumber"),
                        CreditCode = reader.ToRSField("CreditCode"),
                        CreditAmountRate = reader.ToRSFieldDecimal("CreditAmountRate"),
                        CreditAllocatedRate = reader.ToRSFieldDecimal("CreditAllocatedRate"),
                        CreditReservedRate = reader.ToRSFieldDecimal("CreditReservedRate"),
                        CreditAvailableRate = reader.ToRSFieldDecimal("CreditAvailableRate"),
                        IsActivated = reader.ToRSFieldBool("IsActivated")
                    };
                    giftCode.CreditAvailable = giftCode.CreditAmountRate - giftCode.CreditAllocatedRate - giftCode.CreditReservedRate; //get exact credit available
                    giftCodes.Add(giftCode);
                }
            }
            return giftCodes;
        }

        #endregion

        public CustomerProductNotificationSubscription GetCustomerProductNotificationSubscription(string contactCode, string websiteCode, string itemCode, string emailAddress)
        {
            var customerProductNotificationSubscription = new CustomerProductNotificationSubscription();

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Get_EcommerceCustomerProductNotificationSubscription, contactCode.ToDbQuote(), 
                                                                                                                                                    websiteCode.ToDbQuote(), 
                                                                                                                                                    itemCode.ToDbQuote(), 
                                                                                                                                                    emailAddress.ToDbQuote()))
            {
                if (reader.Read())
                {
                    customerProductNotificationSubscription.NotifyOnPriceDrop = reader.ToRSFieldBool("NotifyOnPriceDrop");
                    customerProductNotificationSubscription.NotifyOnItemAvail = reader.ToRSFieldBool("NotifyOnItemAvail");
                }
            }

            return customerProductNotificationSubscription;
        }

        public bool IsCustomerEmailNotAvailable(string emailAddress, string customerCode)
        {
            return (GetSqlN("SELECT COUNT(*) AS N FROM Customer C with (NOLOCK) LEFT JOIN CrmContact CC with (NOLOCK) ON C.CustomerCode = CC.EntityCode WHERE CC.UserName={0} AND C.CustomerCode <> {1}".FormatWith(emailAddress.ToDbQuote(), customerCode.ToDbQuote())) > 0);
        }

        public bool IsLeadEmailNotAvailable(string emailAddress)
        {
            return (GetSqlN("SELECT COUNT(*) AS N FROM CRMLead with (NOLOCK) WHERE EMail = {0}".FormatWith(emailAddress.ToDbQuote())) > 0);
        }


        public bool IsIgnoreStockLevels()
        {
            bool ignoreStockLevels = false;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Get_IsIgnoreStockLevels))
            {
                if (reader.Read())
                {
                    ignoreStockLevels = reader.ToRSFieldBool("IsIgnoreStockLevels");
                }
            }

            return ignoreStockLevels;
        }

        public void TryCreateEcommerceCustomerInfo(string email, string websiteCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.GetEcommerceLoginInfo, email.ToDbQuote(), websiteCode.ToDbQuote());
        }


        #region RMA

        public CustomerInvoiceCustomModel GetCustomerInvoice(string contactCode, string invoiceCode, string websiteCode)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_CustomerInvoice, contactCode.ToDbQuote(), invoiceCode.ToDbQuote(), websiteCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    var invoice = new CustomerInvoiceCustomModel()
                    {
                        InvoiceCode = reader.ToRSField("InvoiceCode"),
                        InvoiceDate = reader.ToRSFieldDateTime("InvoiceDate"),
                        SalesOrderCode = reader.ToRSField("SalesOrderCode"),
                        SalesOrderDate = reader.ToRSFieldDateTime("SalesOrderDate"),
                        TotalRate = reader.ToRSFieldDecimal("TotalRate"),
                        CurrencyCode = reader.ToRSField("CurrencyCode")
                    };
                    invoice.TotalRateFormatted = invoice.TotalRate.ToCustomerCurrency();
                    return invoice;
                }
            }
            return null;
        }
        public IEnumerable<CustomerInvoiceCustomModel> GetCustomerInvoices(string contactCode, string websiteCode)
        {
            var invoices = new List<CustomerInvoiceCustomModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_CustomerInvoices, contactCode.ToDbQuote(), websiteCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var invoice = new CustomerInvoiceCustomModel()
                    {
                        InvoiceCode = reader.ToRSField("InvoiceCode"),
                        InvoiceDate = reader.ToRSFieldDateTime("InvoiceDate"),
                        SalesOrderCode = reader.ToRSField("SalesOrderCode"),
                        SalesOrderDate = reader.ToRSFieldDateTime("SalesOrderDate"),
                        TotalRate = reader.ToRSFieldDecimal("TotalRate"),
                        CurrencyCode = reader.ToRSField("CurrencyCode")
                    };
                    invoice.TotalRateFormatted = invoice.TotalRate.ToCustomerCurrency();
                    invoices.Add(invoice);
                }
            }
            return invoices;
        }
        public IEnumerable<CustomerInvoiceItemCustomModel> GetCustomerInvoiceItems(string contactCode, string invoiceCode, string websiteCode)
        {
            var items = new List<CustomerInvoiceItemCustomModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_CustomerInvoiceItems, contactCode.ToDbQuote(), websiteCode.ToDbQuote(), invoiceCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var item = new CustomerInvoiceItemCustomModel()
                    {
                        LineNum = reader.ToRSFieldInt("LineNum"),
                        ItemCode = reader.ToRSField("ItemCode"),
                        ItemDescription = reader.ToRSField("ItemDescription"),
                        QuantityOrdered = reader.ToRSFieldDecimal("QuantityOrdered"),
                        QuantityShipped = reader.ToRSFieldDecimal("QuantityShipped"),
                        QuantityAlReadyRMA = reader.ToRSFieldDecimal("QuantityAlReadyRMA"),
                        UnitMeasureCode = reader.ToRSField("UnitMeasureCode"),
                        UPCCode = reader.ToRSField("UPCCode")
                    };
                    item.QuantityAvailable = item.QuantityShipped - item.QuantityAlReadyRMA;
                    items.Add(item);
                }
            }
            return items;
        }
        public CustomerRMACustomModel GetCustomerRMA(string contactCode, string rmaCode, string websiteCode)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_CustomerRMA, contactCode.ToDbQuote(), websiteCode.ToDbQuote(), rmaCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    var rma = new CustomerRMACustomModel()
                    {
                        RMACode = reader.ToRSField("RMACode"),
                        RMADate = reader.ToRSFieldDateTime("RMADate"),
                        RMAStatus = reader.ToRSField("RMAStatus"),
                        SalesOrderCode = reader.ToRSField("SalesOrderCode"),
                        SalesOrderDate = reader.ToRSFieldDateTime("SalesOrderDate"),
                        TotalRate = reader.ToRSFieldDecimal("TotalRate"),
                        Notes = reader.ToRSField("Notes"),
                        InvoiceCode = reader.ToRSField("InvoiceCode")
                    };
                    rma.TotalRateFormatted = rma.TotalRate.ToCustomerCurrency();
                    return rma;
                }
            }
            return null;
        }
        public IEnumerable<CustomerRMACustomModel> GetCustomerRMAs(string contactCode, string websiteCode)
        {
            var rmas = new List<CustomerRMACustomModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_CustomerRMAs, contactCode.ToDbQuote(), websiteCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var rma = new CustomerRMACustomModel()
                    {
                        RMACode = reader.ToRSField("RMACode"),
                        RMADate = reader.ToRSFieldDateTime("RMADate"),
                        RMAStatus = reader.ToRSField("RMAStatus"),
                        InvoiceCode = reader.ToRSField("InvoiceCode"),
                        SalesOrderCode = reader.ToRSField("SalesOrderCode"),
                        TotalRate = reader.ToRSFieldDecimal("TotalRate"),
                        TotalQuantityReturn = reader.ToRSFieldDecimal("TotalQuantityReturn")
                    };
                    rma.TotalRateFormatted = rma.TotalRate.ToCustomerCurrency();
                    rmas.Add(rma);
                }
            }
            return rmas;
        }
        public IEnumerable<CustomerRMAItemCustomModel> GetCustomerRMAItems(string contactCode, string rmaCode, string websiteCode)
        {
            var items = new List<CustomerRMAItemCustomModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_CustomerRMAItems, contactCode.ToDbQuote(), websiteCode.ToDbQuote(), rmaCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var item = new CustomerRMAItemCustomModel()
                    {
                        ItemDescription = reader.ToRSField("ItemDescription"),
                        QuantityOrdered = reader.ToRSFieldDecimal("QuantityOrdered"),
                        QuantityShipped = reader.ToRSFieldDecimal("QuantityShipped"),
                        UnitMeasureCode = reader.ToRSField("UnitMeasureCode"),
                        UPCCode = reader.ToRSField("UPCCode")
                    };
                    items.Add(item);
                }
            }
            return items;
        }

        #endregion
    }
}