﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class SystemPostalCodeRepository : RepositoryBase, ISystemPostalCodeRepository
    {
        public bool IsAddressAlreadyExist(string postalCode, string stateCode, string city, string countryCode)
        {
            return GetSqlNByQueryConfigId(DBQueryConstants.ReadSystemPostalCodeCount, postalCode.ToDbQuote(), stateCode.ToDbQuote(), city.ToDbQuote(), countryCode.ToDbQuote()) > 0;
        }
    }
}
