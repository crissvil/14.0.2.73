﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class GatewayRepository : RepositoryBase, IGatewayRepository
    {
        public GatewayInformationCustomModel GetEcommerceCreditCardGatewayByWebsite(string websiteCode)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetEcommerceCreditCardGatewayByWebsite, websiteCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    string merchantLogin = reader.ToRSField("MerchantLogin");
                    if (!merchantLogin.IsNullOrEmptyTrimmed())
                    {
                        var gatewayInformationCustomModel = new GatewayInformationCustomModel()
                        {
                            Gateway = reader.ToRSField("CreditCardGateway"),
                            GatewayAssemblyName = reader.ToRSField("CreditCardGatewayAssemblyName")
                        };
                        return gatewayInformationCustomModel;
                    }
                }
            }
            return null;
        }

        public GatewayInformationCustomModel GetEcommerceCreditCardGatewayByPaymentTerm(string paymentTermCode)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetEcommerceCreditCardGatewayByPaymentTerm, paymentTermCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var gatewayInformationCustomModel = new GatewayInformationCustomModel()
                    {
                        Gateway = reader.ToRSField("CreditCardGateway"),
                        GatewayAssemblyName = reader.ToRSField("CreditCardGatewayAssemblyName")
                    };
                    return gatewayInformationCustomModel;
                }
            }
            return null;
        }

        public GatewayInformationCustomModel GetEcommercePaypalGatewayInfo(string websiteCode)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetEcommercePaypalGatewayInfo, websiteCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var paypalGatewayInformationCustomModel = new GatewayInformationCustomModel()
                    {
                        Gateway = reader.ToRSField("Gateway"),
                        GatewayAssemblyName = reader.ToRSField("AssemblyName"),
                    };
                    return paypalGatewayInformationCustomModel;
                }
            }
            return null;
        }


        public string GetCreditCardGatewayDescription(string gateway)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetCreditCardGatewayDescription, gateway))
            {
                if (reader.Read())
                {
                    return reader.ToRSField("description");
                }
            }

            return gateway;
        }
    }
}
