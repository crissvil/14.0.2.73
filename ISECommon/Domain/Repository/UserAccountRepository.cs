﻿using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.Authentication;
using InterpriseSuiteEcommerceCommon.Extensions;
using System;
using InterpriseSuiteEcommerceCommon.Domain.Model;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class UserAccountRepository: RepositoryBase, IUserAccountRepository
    {
        public ISSIUserAccount Find(string userName, string websiteCode)
        {
            ISSIUserAccount account = null;
            using(var reader = _dataManager.ExecuteSQLReturnReader("EXEC ReadeCommerceUserAccounts @UserCode = {0} , @WebSiteCode = {1}"
                                                                    .FormatWith(userName.ToDbQuote(), websiteCode.ToDbQuote())))
            {
                if(reader.Read())
                {
                    bool isAllowWebAccess = DB.RSFieldBool(reader, "IsWebAccess");
                    if (isAllowWebAccess)
                    {
                        account = new ISSIUserAccount(reader.ToRSField("UserCode"),
                            reader.ToRSField("UserPassword"),
                            reader.ToRSField("UserPasswordSalt"),
                            reader.ToRSField("UserPasswordIV"),
                            reader.ToRSFieldInt("BadLoginCount"),
                            reader.ToRSFieldDateTime("LastBadLogin"),
                            reader.ToRSFieldDateTime("LockedUntil")
                        );

                        account.LanguageCode = reader.ToRSField("LanguageCode");
                        account.LocaleSetting = Localization.CheckLocaleSettingForProperCase(reader.ToRSField("LocaleSetting"));
                    }
                }
            }
            return account;    
        }

        public void ClearSecurity(string userCode)
        {
            _dataManager.ExecuteSQL("UPDATE SystemUserAccount SET BadLoginCount = 0, LastBadLogin = NULL, LockedUntil = NULL WHERE UserCode = {0}".FormatWith(userCode.ToDbQuote()));
        }

        public void IncrementBadLogin(string userCode, DateTime dt)
        {
            _dataManager.ExecuteSQL("UPDATE SystemUserAccount SET BadLoginCount = BadLoginCount+1, LastBadLogin = {0} WHERE UserCode = {1}"
                                    .FormatWith(dt.ToDateTimeStringForDB().ToDbQuote(), userCode.ToDbQuote()));
        }

        public IBadLoginField ReloadBadLoginInfo(string userCode)
        {
            var loginField = new BadLoginFieldModel();

            using (var reader = _dataManager.ExecuteSQLReturnReader("SELECT BadLoginCount, LastBadLogin, LockedUntil FROM SystemUserAccount with (NOLOCK) WHERE UserCode = {0}"
                                                                    .FormatWith(userCode.ToDbQuote())))
            {
                reader.Read();

                loginField.BadLoginCount = DB.RSFieldInt(reader, "BadLoginCount");
                loginField.LastBadLoginDate = DB.RSFieldDateTime(reader, "LastBadLogin");
                loginField.LockedInUntilDate = DB.RSFieldDateTime(reader, "LockedUntil");
            }

            return loginField;
        }

        public void ResetBadLoginCount(string userName)
        {
            _dataManager.ExecuteSQL("UPDATE SystemUserAccount SET BadLoginCount = 0 WHERE Usercode = {0}".FormatWith(userName.ToDbQuote()));
        }

        public void LockUserAccount(string userName, string lockUntil)
        {
            _dataManager.ExecuteSQL("UPDATE SystemUserAccount SET LockedUntil = {0} WHERE UserCode = {1}".FormatWith(lockUntil.ToDbQuote(), userName.ToDbQuote()));
        }
    }
}
