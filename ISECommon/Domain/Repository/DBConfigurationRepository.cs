﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using System.Data;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.Model;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class DBConfigurationRepository : RepositoryBase, IDBConfigurationRepository
    {
        public DataSet GetLocales()
        {
            string sql = GetQueryConfig(DBQueryConstants.ReadLocaleSettings);
            string cacheName = "DataSet: " + sql.ToUpperInvariant();

            var ds = _dataManager.ExecuteSQLReturnDataset(sql, "Table", cacheName, System.DateTime.Now.AddHours(1));
            return ds;
        }

        public AppConfigs GetAllAppConfig(string webSiteCode)
        {
            var appConfigs = new AppConfigs();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadAppConfigs, webSiteCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    appConfigs.Add(new AppConfig(
                            DB.RSFieldInt(reader, "AppConfigID"),
                            DB.RSFieldGUID2(reader, "AppConfigGUID"),
                            DB.RSField(reader, "Name"),
                            DB.RSField(reader, "Description"),
                            DB.RSField(reader, "ConfigValue"),
                            DB.RSField(reader, "GroupName"),
                            DB.RSFieldBool(reader, "SuperOnly"),
                            DB.RSFieldDateTime(reader, "CreatedOn")
                        ));
                }
            }
            return appConfigs;
        }

        public StringResources GetAllStringResource(string webSiteCode)
        {
            var stringResources = new StringResources();
            using(var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadAllStringResource, webSiteCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    string locale = reader.ToRSField("LocaleSetting").ToLowerInvariant();
                    string name = reader.ToRSField("Name").ToLowerInvariant();
                    string lookupKey = String.Format("{0}_{1}", locale, name);

                    stringResources.Add(new StringResource(
                        reader.ToRSFieldInt("StringResourceID"),
                        DB.RSFieldGUID2(reader, "StringResourceGUID"),
                        name,
                        locale,
                        reader.ToRSField("ConfigValue"),
                        reader.ToRSFieldDateTime("CreatedOn"),
                        false
                    ));
                }

                if (stringResources.Count == 0)
                {
                    throw new ArgumentException("No String Resource loaded. Please import String Resource using Connected Business Client.");
                }
            }

            return stringResources;
        }

        public int GetAllWebSiteCount(string webSiteCode)
        {
            return GetSqlNByQueryConfigId(DBQueryConstants.ReadAllWebSiteCount , webSiteCode.ToDbQuote());
        }

        public void TryConnectToDB()
        {
            TestDBConnection();
        }

        public void ClearCustomerSession()
        {
            _dataManager.ExecuteSQL(GetQueryConfig(DBQueryConstants.DeleteCustomerSession));
        }

        public SortedDictionary<string,string> GetRedirectURL(string websiteCode)
        {
            var redirectURL = new SortedDictionary<string, string>();
            using(var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadRedirectURL, websiteCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    string oldPath = DB.RSField(reader, "OldPath").ToLowerInvariant();
                    string newPath = DB.RSField(reader, "NewPath").ToLowerInvariant();

                    if (oldPath.StartsWith("/"))
                    {
                        oldPath = oldPath.Remove(0, 1);
                    }

                    if (newPath.StartsWith("/"))
                    {
                        newPath = newPath.Remove(0, 1);
                    }

                    if (!redirectURL.ContainsKey(oldPath))
                    {
                        redirectURL.Add(oldPath, newPath);
                    }
                }
            }
            return redirectURL;
        }

        public InventoryPreference GetInventoryPreference()
        {
            InventoryPreference preference = null;

            using(var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadInventoryPreferenceForCache))
            {
                if (reader.Read())
                {
                    preference = new InventoryPreference()
                    {
                        IsAllowFractional = reader.ToRSFieldBool("IsAllowFractional"),
                        InventoryDecimalPlacesPreference = reader.ToRSFieldInt("QuantityDecimalPlace")
                    };
                }
            }

            return preference;
        }

        public IEnumerable<SystemCountryModel> GetSystemCounties()
        {
            var lst = new List<SystemCountryModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadSystemCountry))
            {
                while (reader.Read())
                {
                    lst.Add(new SystemCountryModel { CountryCode = reader.ToRSField("CountryCode") });
                }
            }
            return lst;
        }

        public void CleanEcommerceCustomerRecord(string websiteCode)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.Delete_EcommerceCustomerRecords, websiteCode.ToDbQuote());
        }

    }
}
