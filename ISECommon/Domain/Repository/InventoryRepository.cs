﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.Model;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;


namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class InventoryRepository : RepositoryBase, IInventoryRepository
    {
        public bool IsAKit(string productId)
        {
            bool tmpS = false;
            using(var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadIsAKit, productId.ToDbQuote()))
            {
                if (reader.Read())
                {
                    tmpS = reader.ToRSFieldBool("IsAKit");
                }
            }
            return tmpS;
        }

        public int GetProductCount()
        {
            return GetSqlN(GetQueryConfig(DBQueryConstants.ReadProductCount));
        }

        public string GetInventoryItemType(string itemCode)
        {
            string itemType = String.Empty;
            using(var reader = _dataManager.ExecuteSQLByQueryIDReturnReader( DBQueryConstants.GetInventoryItemType,  itemCode.ToDbQuote()))
            {
                if(reader.Read())
                {
                    itemType = reader.ToRSField("ItemType");
                }
            }
            return itemType;
        }

        public int GetInventoryItemCounter(string itemCode)
        {
            int itemCounter = 0;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetInventoryItemCounter, itemCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    itemCounter = reader.ToRSFieldInt("Counter");
                }
            }
            return itemCounter;
        }

        public int GetCheckOutOptionCount(string websiteCode, string contactCode, string customerCode, bool isNotRegistered, string languageCode, string currentDate, string productFilterId, ref string xmlString)
        {
            string checkOutOptionsQuery = _dataManager.GetQueryConfig(DBQueryConstants.ReadEcommerceCheckOutOptions, websiteCode.ToDbQuote(),
                                                      contactCode.ToDbQuote(), customerCode.ToDbQuote(), isNotRegistered, languageCode.ToDbQuote(),
                                                      currentDate.ToDbQuote(), productFilterId.ToDbQuote());
            //this will be optimized
            return DB.GetXml(checkOutOptionsQuery, "options", "orderoption", ref xmlString);
        }

        public InventoryProductModel GetInventoryItem(string languageCode, string itemCode)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadInventoryItem, languageCode.ToDbQuote(), itemCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    var item = new InventoryProductModel()
                    {
                        Counter = reader.ToRSFieldInt("Counter"),
                        ItemCode = reader.ToRSField("ItemCode"),
                        ItemName = reader.ToRSField("ItemName"),
                        ItemType = reader.ToRSField("ItemType"),
                        ItemDescription = reader.ToRSField("ItemDescription").ToHtmlEncode(),
                        IsDropShip = reader.ToRSFieldBool("IsDropShip"),
                        IsSpecialOrder = reader.ToRSFieldBool("IsSpecialOrder"),
                        IsCBN = reader.ToRSFieldBool("IsCBN"),
                        CBNItemID = reader.ToRSFieldInt("CBNItemID")
                    };
                    return item;
                }
            }
            return null;
        }

        public IEnumerable<InventoryProductModel> GetInventoryItems(string languageCode)
        {
            var items = new List<InventoryProductModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadInventoryItems, languageCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var item = new InventoryProductModel()
                    {
                        Counter = reader.ToRSFieldInt("Counter"),
                        ItemCode = reader.ToRSField("ItemCode"),
                        ItemName = reader.ToRSField("ItemName"),
                        ItemType = reader.ToRSField("ItemType"),
                        ItemDescription = reader.ToRSField("ItemDescription").ToHtmlEncode()
                    };
                    items.Add(item);
                }
            }
            return items;
        }

        public IEnumerable<InventoryProductModel> GetInventoryItemsWithNoImages(string languageCode)
        {
            var items = new List<InventoryProductModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadInventoryItemsWithNoImages, languageCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var item = new InventoryProductModel()
                    {
                        Counter = reader.ToRSFieldInt("Counter"),
                        ItemCode = reader.ToRSField("ItemCode"),
                        ItemName = reader.ToRSField("ItemName"),
                        ItemType = reader.ToRSField("ItemType"),
                        ItemDescription = reader.ToRSField("ItemDescription").ToHtmlEncode()
                    };
                    items.Add(item);
                }
            }
            return items;
        }

        public IEnumerable<InventoryOverrideImageModel> GetItemImages(string websiteCode, string itemCode)
        {
            var images = new List<InventoryOverrideImageModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadItemImages, websiteCode.ToDbQuote(), itemCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var image = new InventoryOverrideImageModel()
                    {
                        ImageIndex = reader.ToRSFieldInt("ImageIndex"),
                        ItemCode = reader.ToRSField("ItemCode"),
                        FileName = reader.ToRSField("FileName"),
                        IsDefaultIcon = reader.ToRSFieldBool("IsDefaultIcon"),
                        IsDefaultMedium = reader.ToRSFieldBool("IsDefaultMedium")
                    };
                    images.Add(image);
                }
            }
            return images;
        }

        public IEnumerable<InventoryCategoryModel> GetInventoryCategories(string languageCode)
        {
            var categories = new List<InventoryCategoryModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadInventoryCategories, languageCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var category = new InventoryCategoryModel()
                    {
                        ItemCode = reader.ToRSField("ItemCode"),
                        CategoryCode = reader.ToRSField("CategoryCode"),
                        Description = reader.ToRSField("Description")
                    };
                    categories.Add(category);
                }
            }
            return categories;
        }

        public IEnumerable<SystemCategoryModel> GetSystemCategories(string languageCode)
        {
            var categories = new List<SystemCategoryModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadSystemCategories, languageCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var category = new SystemCategoryModel()
                    {
                        CategoryCode = reader.ToRSField("CategoryCode"),
                        Description = reader.ToRSField("Description")
                    };
                    categories.Add(category);
                }
            }
            return categories;
        }

        public decimal GetItemWeight(string itemCode, string unitMeasureCode)
        {
            decimal defaultValue = Decimal.Zero;
            using(var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetItemWeight,itemCode.ToDbQuote(), unitMeasureCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    defaultValue = reader.ToRSFieldDecimal("WeightInPounds");
                }
            }
            return defaultValue;
        }

        public bool UseCustomerPricingForKit(string itemKitCode)
        {
            bool defaultValue = false;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetUseCustomerPricingForKit, itemKitCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    defaultValue = reader.ToRSFieldBool("IsGetItemPrice");
                }
            }
            return defaultValue;
        }

        public decimal GetKitDiscount(string itemKitCode, string contactCode)
        {
            decimal defaultValue = Decimal.Zero;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetKitDiscount, itemKitCode.ToDbQuote(), contactCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    defaultValue = reader.ToRSFieldDecimal("Discount");
                }
            }
            return defaultValue;
        }

        public IEnumerable<SelectedKitComponentModel> GetSelectedKitComponents(string itemKitCode, string currencyCode, string websiteCode, string cartId, bool isAnonymous, string customerCode, string anonymousCustomerCode, string contactCode, bool showDefault)
        { 
            var selectedKitComponents = new List<SelectedKitComponentModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetSelectedKitComponents, itemKitCode.ToDbQuote(), currencyCode.ToDbQuote(), websiteCode.ToDbQuote(),
                                             cartId.ToDbQuote(), isAnonymous, customerCode.ToDbQuote(), anonymousCustomerCode.ToDbQuote(), contactCode.ToDbQuote(), showDefault))
            {
                while (reader.Read())
                {
                    var selectedKitComponent = new SelectedKitComponentModel()
                    {
                       ItemCode = reader.ToRSField("ItemCode"),
                       Quantity = reader.ToRSFieldDecimal("Quantity"),
                       UnitMeasureCode = reader.ToRSField("UnitMeasureCode"),
                       TotalRate = reader.ToRSFieldDecimal("TotalRate")
                    };
                    selectedKitComponents.Add(selectedKitComponent);
                }
            }
            return selectedKitComponents;
        }

        public ValidCartItemCustomModel GetValidItemCodeAndBaseUnitMeasureById(int itemId, bool isAnonymous, string contactCode, string websiteCode, string currentDate, string productFilterID)
        {
            
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetValidItemCodeAndBaseUnitMeasureById, itemId, isAnonymous, contactCode.ToDbQuote(),
                                             websiteCode.ToDbQuote(), currentDate.ToDbQuote(), productFilterID.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var validItemCodeAndBaseUnitMeasure = new ValidCartItemCustomModel()
                    {
                        ItemCode = reader.ToRSField("ItemCode"),
                        UnitMeasureCode = reader.ToRSField("UnitMeasureCode")
                    };
                    return validItemCodeAndBaseUnitMeasure;
                }
            }
            return null;
        }

        public string GetMatrixItemCode(string itemCode)
        {
            string matrixItemCode = String.Empty;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetMatrixItemCode, itemCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    matrixItemCode = reader.ToRSField("ItemCode");
                }
            }
            return matrixItemCode;
        }

        public SystemEntityModel GetVirtualPageSettings(int entityId, string entityName, string languageCode, string websiteCode)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetVirtualPageSettings, entityId, entityName.ToDbQuote(), languageCode.ToDbQuote(), websiteCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var entity = new SystemEntityModel()
                    {
                        EntityCode = reader.ToRSField("EntityCode"),
                        VirtualType = reader.ToRSField("VirtualType"),
                        VirtualPageOption = reader.ToRSField("VirtualPageOption"),
                        VirtualPageValueEntity = reader.ToRSField("VirtualPageValueEntity"),
                        NewEntityId = reader.ToRSFieldInt("NewEntityCode"),
                        VirtualPageValueTopic = reader.ToRSField("VirtualPageValueTopic"),
                        VirtualPageValueExternalPage = reader.ToRSField("VirtualPageValueExternalPage"),
                        OpenInNewTab = reader.ToRSFieldBool("OpenInNewTab")
                    };
                    return entity;
                }
            }
            return null;
        }

        public IEnumerable<MatrixItemWebOptionDescCustomModel> GetMatrixItemWebOptionDescription(string websiteCode, string languageCode, string itemCode)
        {
            var matrixItemWebOptionDescriptions = new List<MatrixItemWebOptionDescCustomModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetMatrixItemWebOptionDescription, websiteCode.ToDbQuote(), languageCode.ToDbQuote(), itemCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    var matrixItemWebOptionDescription = new MatrixItemWebOptionDescCustomModel()
                    {
                        Counter = reader.ToRSFieldInt("Counter"),
                        ItemCode = reader.ToRSField("ItemCode"),
                        Summary = reader.ToRSField("Summary"),
                        WebDescription = reader.ToRSField("WebDescription"),
                        Warranty = reader.ToRSField("Warranty")
                    };
                    matrixItemWebOptionDescriptions.Add(matrixItemWebOptionDescription);
                }
                return matrixItemWebOptionDescriptions;
            }
        }

        public IEnumerable<MatrixItemWebOptionDescCustomModel> GetNonStockMatrixItemWebOptionDescription(string websiteCode, string languageCode, IEnumerable<string> itemCodes)
        {
            var matrixItemWebOptionDescriptions = new List<MatrixItemWebOptionDescCustomModel>();
            string codes = String.Empty;

            if (itemCodes.Count() > 0)
            {
                codes = string.Join(",", itemCodes.Select(c => c.ToDbQuote()));
            }
            else
            {
                codes = "NULL";
            }
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetNonStockMatrixItemWebOptionDescription, websiteCode.ToDbQuote(), languageCode.ToDbQuote(), codes))
            {
                while (reader.Read())
                {
                    var matrixItemWebOptionDescription = new MatrixItemWebOptionDescCustomModel()
                    {
                        Counter = reader.ToRSFieldInt("Counter"),
                        ItemCode = reader.ToRSField("ItemCode"),
                        Summary = reader.ToRSField("Summary"),
                        WebDescription = reader.ToRSField("WebDescription"),
                        Warranty = reader.ToRSField("Warranty")
                    };
                    matrixItemWebOptionDescriptions.Add(matrixItemWebOptionDescription);
                }
                return matrixItemWebOptionDescriptions;
            }
        }
        public ECommerceProductInfoViewCustomModel GetProductInfoViewForShowProduct(string itemCode, string localeSettings, string userCode, string websiteCode, string dateTimeStringFromDB, string productFilterId, string contactCode)
        {
            ECommerceProductInfoViewCustomModel eCommerceProductInfoView = null;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.GetProductInfo, itemCode.ToDbQuote(), localeSettings.ToDbQuote(), userCode.ToDbQuote(), 
                                                                             websiteCode.ToDbQuote(), dateTimeStringFromDB.ToDbQuote(), productFilterId.ToDbQuote(), contactCode.ToDbQuote()))
            {
                if (!reader.Read())
                {
                    reader.Close();
                    return eCommerceProductInfoView;
                }

                eCommerceProductInfoView = new ECommerceProductInfoViewCustomModel
                {
                    Counter = reader.ToRSFieldInt("Counter"),
                    ItemCode = reader.ToRSField("ItemCode"),
                    ItemName = reader.ToRSField("ItemName"),
                    ItemDescription = reader.ToRSField("ItemDescription"),
                    CheckOutOption = reader.ToRSFieldBool("CheckOutOption"),
                    RequiresRegistration = reader.ToRSFieldBool("RequiresRegistration"),
                    SETitle = reader.ToRSField("SETitle"),
                    SEDescription = reader.ToRSField("SEDescription"),
                    SEKeywords = reader.ToRSField("SEKeywords"),
                    SENoScript = reader.ToRSField("SENoScript"),
                    IsAKit = reader.ToRSFieldInt("IsAKit"),
                    IsMatrix = reader.ToRSFieldInt("IsMatrix"),
                    XmlPackage = reader.ToRSField("XmlPackage"),
                    MobileXmlPackage = reader.ToRSField("MobileXmlPackage"),
                    IsCBN = reader.ToRSFieldBool("IsCBN"),
                    ItemType = reader.ToRSField("ItemType"),
                    WebDescription = reader.ToRSField("WebDescription"),
                    Summary = reader.ToRSField("Summary"),
                    Warranty = reader.ToRSField("Warranty"),
                    ExpectedShippingDate = reader.ToRSFieldDateTime("ExpShipingDate"),
                    IsPublished = reader.ToRSFieldBool("Published")
                };
                reader.Close();
            }
            return eCommerceProductInfoView;
        }

        public ItemWebOptionCustomModel GetItemWebOption(string websiteCode, string itemCode, bool IsExpressPrint)
        {
            ItemWebOptionCustomModel itemWebOption = null;

            using(var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadInventoryItemWebOption, 
                                                                            websiteCode.ToDbQuote(), 
                                                                            itemCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    itemWebOption = new ItemWebOptionCustomModel()
                    {
                        ItemCounter = reader.ToRSFieldInt("Counter"),
                        ItemCode = reader.ToRSField("ItemCode"),
                        ZoomOption = reader.ToRSField("ZoomOption"),
                        HidePriceUntilCart = reader.ToRSFieldBool("HidePriceUntilCart"),
                        IsCallToOrder = reader.ToRSFieldBool("IsCallToOrder"),
                        ShowBuyButton = reader.ToRSFieldBool("ShowBuyButton"),
                        RequiresRegistration = reader.ToRSFieldBool("RequiresRegistration"),
                        ShowPurchasedItemsFromSameCategory = reader.ToRSFieldBool("ShowPurchasedItemsFromSameCategory"),
                        ShowViewedItemsFromSameCategory = reader.ToRSFieldBool("ShowViewedItemsFromSameCategory"),
                        MinOrderQuantity = reader.ToRSFieldDecimal("MinOrderQuantity"),
                        IsDontEarnPoints = reader.ToRSFieldBool("DontEarnPoints"),
                        ShowSaleBanner = reader.ToRSFieldBool("IsSale_C"),
                        Dimension = reader.ToRSField("ProductSize_C"),
                        ItemName = reader.ToRSField("ItemName"),
                        IsDropShip = reader.ToRSFieldBool("IsDropShip"),
                        MaterialType = reader.ToRSField("MaterialType_C"),
                        PrintInfo = reader.ToRSField("PrintInfo_C")

                    };

                    string[] quantities = reader.ToRSField("RestrictedQuantity").Split(DomainConstants.COMMA_DELIMITER);
                    decimal quantity = Decimal.Zero;
                    foreach (string qty in quantities)
                    {
                        if (Decimal.TryParse(qty, out quantity)) { itemWebOption.RestrictedQuantities.Add(quantity); }
                    }
                }
            }
            //Replace Restricted values with kit details as required by expressprint. Applicable only for kit items.
            //Checking is included to retain original Restricted values for none kit items.
            if (IsExpressPrint)
            {
                List<string> priceText;
                List<decimal> priceQty;
                priceText = new List<string>();
                priceQty = new List<decimal>();
                using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Get_ExpressKitQtyDropdown, itemCode.ToDbQuote()))
                {
                    while (reader.Read())
                    {
                        priceText.Add(reader.ToRSField("PricingAttribute_C"));
                        priceQty.Add(reader.ToRSFieldDecimal("MaxQuantity"));
                    }
                }
                if (priceQty.Count > 0)
                {
                    itemWebOption.RestrictedQuantities = priceQty;
                    itemWebOption.RestrictedQuantitiesText = priceText;
                }
            }
            return itemWebOption;
        }

        public IEnumerable<ItemWebOptionCustomModel> GetItemWebOptions(string websiteCode, IEnumerable<string> itemCodes, bool IsExpressPrint)
        {
            var itemWebOptions = new List<ItemWebOptionCustomModel>();

            string codes = String.Empty;
            if (itemCodes.Count() > 0) { codes = String.Join(",", itemCodes.Select(c => c.ToDbQuote())); }
            else { codes = "NULL"; }

            using(var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadInventoryItemWebOption, 
                                                                            websiteCode.ToDbQuote(),
                                                                            codes))
            {
                while (reader.Read())
                {
                    var itemWebOption = new ItemWebOptionCustomModel()
                    {
                        ItemCounter = reader.ToRSFieldInt("Counter"),
                        ItemCode = reader.ToRSField("ItemCode"),
                        ZoomOption = reader.ToRSField("ZoomOption"),
                        HidePriceUntilCart = reader.ToRSFieldBool("HidePriceUntilCart"),
                        IsCallToOrder = reader.ToRSFieldBool("IsCallToOrder"),
                        ShowBuyButton = reader.ToRSFieldBool("ShowBuyButton"),
                        RequiresRegistration = reader.ToRSFieldBool("RequiresRegistration"),
                        ShowPurchasedItemsFromSameCategory = reader.ToRSFieldBool("ShowPurchasedItemsFromSameCategory"),
                        ShowViewedItemsFromSameCategory = reader.ToRSFieldBool("ShowViewedItemsFromSameCategory"),
                        MinOrderQuantity = reader.ToRSFieldDecimal("MinOrderQuantity"),
                        IsDontEarnPoints = reader.ToRSFieldBool("DontEarnPoints"),
                        ShowSaleBanner = reader.ToRSFieldBool("IsSale_C"),
                        Dimension = reader.ToRSField("ProductSize_C"),
                        ItemName = reader.ToRSField("ItemName"),
                        IsDropShip = reader.ToRSFieldBool("IsDropShip"),
                        MaterialType = reader.ToRSField("MaterialType_C"),
                        PrintInfo = reader.ToRSField("PrintInfo_C")
                    };

                    string[] quantities = reader.ToRSField("RestrictedQuantity").Split(DomainConstants.COMMA_DELIMITER);
                    decimal quantity = Decimal.Zero;
                    foreach (string qty in quantities)
                    {
                        if (Decimal.TryParse(qty, out quantity)) 
                        { 
                            itemWebOption.RestrictedQuantities.Add(quantity);
                            itemWebOption.RestrictedQuantitiesText.Add(qty); 
                        }
                    }

                    itemWebOptions.Add(itemWebOption);
                }
            }
            
            //Replace Restricted values with kit details as required by expressprint. Applicable only for kit items.
            //Checking is included to retain original Restricted values for none kit items.
            if (IsExpressPrint)
            {
                List<string> priceText;
                List<decimal> priceQty;
                foreach (string item in itemCodes)
                {
                    priceText = new List<string>();
                    priceQty = new List<decimal>();
                    using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Get_ExpressKitQtyDropdown, item.ToDbQuote()))
                    {
                        while (reader.Read())
                        {
                            priceText.Add(reader.ToRSField("PricingAttribute_C"));
                            priceQty.Add(reader.ToRSFieldDecimal("MaxQuantity"));
                        }
                    }
                    foreach (ItemWebOptionCustomModel option in itemWebOptions)
                    {
                        if (option.ItemCode == item && priceQty.Count > 0)
                        {
                            option.RestrictedQuantities = priceQty;
                            option.RestrictedQuantitiesText = priceText;
                        }
                    }
                }
            }
            return itemWebOptions;
        }

        public InventoryPriceInfoModel GetProductPriceInfo(string customerCode, string currencyCode,
            string itemCode, string unitMeasure, decimal unitMeasureQuantity, decimal quantity, decimal matrixGroupQuantity, string languageCode, string websiteCode)
        {
            InventoryPriceInfoModel eCommerceProductPriceInfo = null;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadEcommercePrice,
                customerCode.ToDbQuote(), currencyCode.ToDbQuote(), itemCode.ToDbQuote(), unitMeasure.ToDbQuote(), unitMeasureQuantity, System.DateTime.Today.ToDateTimeStringForDB().ToDbDateQuote(), quantity, matrixGroupQuantity, languageCode.ToDbQuote(), websiteCode.ToDbQuote()
                ))
            {
                if (!reader.Read())
                {
                    reader.Close();
                    return eCommerceProductPriceInfo;
                }

                eCommerceProductPriceInfo = new InventoryPriceInfoModel
                {

                    ItemCode = reader.ToRSField("ItemCode"),
                    UMCode = unitMeasure,
                    IsByTotalQty = reader.ToRSFieldBool("byTotalQty"),
                    SalesPrice = reader.ToRSFieldDecimal("salesPrice"),
                    IsSalePriceInBaseCurrency = reader.ToRSFieldBool("isSalesPriceInBaseCurrency"),
                    RegularPrice = reader.ToRSFieldDecimal("regularPrice"),
                    PromotionalPrice = reader.ToRSFieldDecimal("promotionalPrice"),
                    Pricing = reader.ToRSField("pricing"),
                    Percent = reader.ToRSFieldDecimal("percent"),
                    Discount = reader.ToRSFieldDecimal("discount"),
                    CategoryDiscount = reader.ToRSFieldDecimal("categoryDiscount"),
                    CustomerItemCode = reader.ToRSField("customerItemCode"),
                    CustomerItemDescription = reader.ToRSField("customerItemDescription"),
                    BasePricingCost = reader.ToRSFieldDecimal("basePricingCost"),
                    BaseAverageCost = reader.ToRSFieldDecimal("baseCostingMethodCost"),
                    IsInventorySpecialPriceExpired = reader.ToRSFieldBool("isInventorySpecialPriceExpired"),
                    IsCustomerSpecialPriceExpired = reader.ToRSFieldBool("isCustomerSpecialPriceExpired"),
                    IsDeductPotentialDiscount = reader.ToRSFieldBool("isDeductPotentialDiscount"),
                    PotentialDiscount = reader.ToRSFieldDecimal("potentialDiscount"),
                    ItemType = reader.ToRSField("ItemType")
                };
                reader.Close();
            }


            return eCommerceProductPriceInfo;
        }

        public IEnumerable<UnitMeasureModel> GetItemUnitMeasuresUnitMeasureList(string itemCode, IEnumerable<string> unitMeasureCodes)
        {
            var unitMeasureModelList = new List<UnitMeasureModel>();
            string query = GetQueryConfig(DBQueryConstants.Read_UnitMeasuresByUnitMeasureList, itemCode.ToDbQuote(), String.Join(",", unitMeasureCodes.Select(u => u.ToDbQuote())));
            using (var reader = _dataManager.ExecuteSQLReturnReader(query))
            {
                while(reader.Read())
                {
                    unitMeasureModelList.Add(new UnitMeasureModel() {
                        Code = reader.ToRSField("UnitMeasureCode"),
                        Quantity = reader.ToRSFieldDecimal("UnitMeasureQty"),
                    });
                }
            }
            return unitMeasureModelList;
        }

        public IEnumerable<UnitMeasureModel> GetItemBaseUnitMeasures(string itemCode)
        {
            var unitMeasureModelList = new List<UnitMeasureModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_BaseUnitMeasures, itemCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    unitMeasureModelList.Add(new UnitMeasureModel()
                    {
                        Code = reader.ToRSField("UnitMeasureCode"),
                        Quantity = reader.ToRSFieldDecimal("UnitMeasureQty"),
                        Length = reader.ToRSFieldDecimal("Length"),
                        Width = reader.ToRSFieldDecimal("Width"),
                        Height = reader.ToRSFieldDecimal("Height"),
                        Weight = reader.ToRSFieldDecimal("Weight"),
                    });
                }
            }
            return unitMeasureModelList;
        }

        public UnitMeasureModel GetItemDefaultUnitMeasure(string itemCode)
        {
            var unitMeasureModelList = new UnitMeasureModel();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_DefaultUnitMeasure, itemCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    unitMeasureModelList.Code = reader.ToRSField("UnitMeasureCode");
                    unitMeasureModelList.Quantity = reader.ToRSFieldDecimal("UnitMeasureQty");
                    unitMeasureModelList.Length = reader.ToRSFieldDecimal("Length");
                    unitMeasureModelList.Width = reader.ToRSFieldDecimal("Width");
                    unitMeasureModelList.Height = reader.ToRSFieldDecimal("Height");
                    unitMeasureModelList.Weight = reader.ToRSFieldDecimal("Weight");
                }
            }
            return unitMeasureModelList;
        }
        public InventoryMatrixItemModel GetMatrixItemInfo(string matrixItemCode)
        {
            InventoryMatrixItemModel matrixInfo = null;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadMatrixItemInfo, matrixItemCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    matrixInfo = new InventoryMatrixItemModel
                    {
                        Counter = reader.ToRSFieldInt("Counter"),
                        ItemCode = reader.ToRSField("ItemCode")
                    };
                }
            }
            return matrixInfo;
        }

        public InventoryMatrixItemModel GetMatrixItemInfo(int counter)
        {
            InventoryMatrixItemModel matrixInfo = null;
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadMatrixItemInfoByCounter, counter))
            {
                if (reader.Read())
                {
                    matrixInfo = new InventoryMatrixItemModel
                    {
                        Counter = reader.ToRSFieldInt("Counter"),
                        MatrixItemCode = reader.ToRSField("MatrixItemCode")
                    };
                }
            }
            return matrixInfo;
        }

        public IEnumerable<MatrixAttributeModel> GetMatrixAttributes(string itemCode, string languageCode)
        {
            var matrixAttributes = new List<MatrixAttributeModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadMatrixAttributes, itemCode.ToDbQuote(), languageCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    matrixAttributes.Add(new MatrixAttributeModel()
                    {
                        AttributeCode = reader.ToRSField("AttributeCode"),
                        AttributeDescription = reader.ToRSField("AttributeDescription"),
                        AttributeValueCode = reader.ToRSField("AttributeValueCode"),
                        AttributeValueDescription = reader.ToRSField("AttributeValueDescription")
                    });
                }
            }
            return matrixAttributes;
        }

        public IEnumerable<MatrixAttributeModel> GetNonStockMatrixAttributes(string counter, string itemCode, string websiteCode)
        {
            var matrixAttributes = new List<MatrixAttributeModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.ReadNonStockMatrixAttributes, counter.ToDbQuote(), itemCode.ToDbQuote(), websiteCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    matrixAttributes.Add(new MatrixAttributeModel()
                    {
                        AttributeCode = reader.ToRSField("AttributeCode"),
                        AttributeDescription = reader.ToRSField("AttributeDescription"),
                        AttributeValueCode = reader.ToRSField("AttributeValueCode"),
                        AttributeValueDescription = reader.ToRSField("AttributeValueDescription")
                    });
                }
            }
            return matrixAttributes;
        }

        public IEnumerable<FeaturedItemCustomModel> GetFeaturedItems(string websiteCode, string localeSetting, string contactCode)
        {
            var featuredItems = new List<FeaturedItemCustomModel>();
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_FeaturedItems,
                localeSetting.ToDbQuote(),
                websiteCode.ToDbQuote(),
                Localization.ToDBDateTimeString(DateTime.Now).ToDbQuote(),
                contactCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    featuredItems.Add(new FeaturedItemCustomModel()
                    {
                        Counter = reader.ToRSFieldInt("Counter"),
                        ItemCode = reader.ToRSField("ItemCode"),
                        ItemDescription = reader.ToRSField("ItemDescription"),
                        ItemName = reader.ToRSField("ItemName"),
                        ItemType = reader.ToRSField("ItemType"),
                        Summary = reader.ToRSField("Summary"),
                        ShowBuyButton = reader.ToRSFieldBool("ShowBuyButton"),
                        HidePriceUntilCart = reader.ToRSFieldBool("HidePriceUntilCart")
                    });
                }
            }
            return featuredItems;
        }

        public RatingComputationCustomModel GetRatingComputation(string itemCode, string websiteCode)
        {
            RatingComputationCustomModel model = null;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Get_RatingAveByItemCode, itemCode.ToDbQuote(), websiteCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    model = new RatingComputationCustomModel()
                    {
                        Average = reader.ToRSFieldDecimal("Ave"),
                        Count = reader.ToRSFieldInt("Cnt"),
                        Total = reader.ToRSFieldInt("Total"),
                        Great = reader.ToRSFieldDecimal("5"),
                        Good = reader.ToRSFieldDecimal("4"),
                        OK = reader.ToRSFieldDecimal("3"),
                        BAD = reader.ToRSFieldDecimal("2"),
                        TERRIBLE = reader.ToRSFieldDecimal("1")
                    };
                }
            }

            return model;

        }

        public RatingModel GetRating(string itemCode, string customerCode, string contactCode, string websiteCode)
        { 
            RatingModel model = null;

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_Rating, itemCode.ToDbQuote(), customerCode.ToDbQuote(), contactCode.ToDbQuote(), websiteCode.ToDbQuote()))
            {
                if (reader.Read())
                {
                    model = new RatingModel()
                    {
                        ID = reader.ToRSFieldInt("RatingID"),
                        Comment = reader.ToRSField("Comments"),
                        Rating = reader.ToRSFieldInt("Rating"),
                        HasComment = reader.ToRSFieldBool("HasComment"),
                        IsFilthy = reader.ToRSFieldBool("IsFilthy"),
                        Created = reader.ToRSFieldDateTime("CreatedOn"),
                        ItemDescription = reader.ToRSField("ItemDescription")
                    };
                }
            }

            return model;
        }

        public void SaveRating(RatingModel model)
        {
            _dataManager.ExecuteSQLByQueryID(DBQueryConstants.CreateUpdate_Rating, model.ItemCode.ToDbQuote(), model.CustomerCode.ToDbQuote(), model.ContactCode.ToDbQuote(),
                                                                             model.Rating, model.Comment.ToDbQuote(), model.HasComment.ToBit(), model.IsFilthy.ToBit(),
                                                                             model.WebsiteCode.ToDbQuote(), model.Created.ToDateTimeStringForDB().ToDbQuote());

        }

        public IEnumerable<MatrixItemCustomModel> GetMatrixItemDetails(string searchString, string itemCode, int pageSize, int currentPage, string languageCode, string webSiteCode, string contactCode)
        {
            var matrixItems = new List<MatrixItemCustomModel>();

            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Get_MatrixItems,
                                                                            searchString.ToDbQuote(), itemCode.ToDbQuote(), pageSize, currentPage, 
                                                                            languageCode.ToDbQuote(), webSiteCode.ToDbQuote(), contactCode.ToDbQuote()))
            {
                while (reader.Read())
                {
                    matrixItems.Add(new MatrixItemCustomModel()
                    {
                        Counter = reader.ToRSFieldInt("Counter"),
                        MatrixItemCode = reader.ToRSField("MatrixItemCode"),
                        MatrixItemDescription = reader.ToRSField("MatrixItemDescription"),
                        MatrixItemName = reader.ToRSField("MatrixItemName"),
                        WebDescription = reader.ToRSField("WebDescription"),
                        ZoomOption = reader.ToRSField("ZoomOption"),
                        HidePriceUntilCart = reader.ToRSFieldBool("HidePriceUntilCart"),
                        ItemCount = reader.ToRSFieldInt("ItemCount"),
                        RequiresRegistration = reader.ToRSFieldBool("RequiresRegistration"),
                        ShowBuyButton = reader.ToRSFieldBool("ShowBuyButton"),
                        IsCallToOrder = reader.ToRSFieldBool("IsCallToOrder")
                    });
                }
            }
            return matrixItems;
        }

        public ItemWebOptionDescCustomModel GetItemWebOptionDescription(int itemCounter, string languageCode, string websiteCode)
        {
            using (var reader = _dataManager.ExecuteSQLByQueryIDReturnReader(DBQueryConstants.Read_InventoryItemWebOptionDescription, itemCounter, languageCode.ToDbQuote(), websiteCode.ToDbQuote()))
            {
                if(reader.Read())
                {
                    return new ItemWebOptionDescCustomModel()
                    {
                        Counter = reader.ToRSFieldInt("Counter"),
                        ItemName = reader.ToRSField("ItemName"),
                        ItemDescription = reader.ToRSField("ItemDescription"),
                        WebDescription = reader.ToRSField("WebDescription")
                    };
                }
            }
            return null;
        }
    }
}