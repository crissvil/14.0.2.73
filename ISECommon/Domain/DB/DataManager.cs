﻿using System;
using System.Data;
using System.Data.SqlClient;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.Exceptions;
using System.Data.Common;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class DataManager : IDataManager
    {
        #region Declaration

        Database _database = null;
        bool _dumpSql = false;

        #endregion

        #region Constructors

        public DataManager(string connectionString, bool dubpSql = false)
        {
            try
            {
                _database = new SqlDatabase(connectionString);
                _dumpSql = dubpSql;
            }
            catch (ArgumentException)
            {
                throw new ConnectionStringNotInitializedException();
            }
            catch (SqlException)
            {
                throw new ConnectionStringNotInitializedException();
            }
        }

        #endregion

        #region Methods

        #region Text Command Type

        
        public void ExecuteSQL(string sql)
        {
            _database.ExecuteNonQuery(CommandType.Text, sql);
        }

        public void ExecuteSQL(string sql, params object[] queryParams)
        { 
            _database.ExecuteNonQuery(CommandType.Text, sql.FormatWith(queryParams));
        }

        public void ExecuteSQLByQueryID(string dbQueryId)
        {
            _database.ExecuteNonQuery(CommandType.Text, GetQueryConfig(dbQueryId, null));
        }

        public int ExecuteSQLByQueryID(string dbQueryId, params object[] queryParams)
        {
            return _database.ExecuteNonQuery(CommandType.Text, GetQueryConfig(dbQueryId, queryParams));
        }


        public IDataReader ExecuteSQLReturnReader(string sql)
        {
            return _database.ExecuteReader(CommandType.Text, sql);
        }

        public IDataReader ExecuteSQLReturnReader(string sql, params object[] queryParams)
        {
            return _database.ExecuteReader(CommandType.Text, sql.FormatWith(queryParams));
        }

        public IDataReader ExecuteSQLByQueryIDReturnReader(string dbQueryId, params object[] queryParams)
        {
            return ExecuteSQLReturnReader(GetQueryConfig(dbQueryId, queryParams));
        }

        public IDataReader ExecuteSQLByQueryIDReturnReader(string dbQueryId)
        {
            return ExecuteSQLReturnReader(GetQueryConfig(dbQueryId, null));
        }


        public DataTable ExecuteSQLReturnTable(string sql)
        {
            using (var dataset = _database.ExecuteDataSet(CommandType.Text, sql))
            {
                if (dataset.Tables.Count > 0) { return dataset.Tables[0]; }
            }
            
            return new DataTable();
        }

        public DataTable ExecuteSQLByQueryIDReturnTable(string dbQueryId, params object[] queryParams)
        {
            using (var dataset = _database.ExecuteDataSet(CommandType.Text, GetQueryConfig(dbQueryId, queryParams)))
            {
                if (dataset.Tables.Count > 0) { return dataset.Tables[0]; }
            }

            return new DataTable();
        }

        public DataSet ExecuteSQLReturnDataset(string sql, string dataSetName)
        {
            using (var dataset = _database.ExecuteDataSet(CommandType.Text, sql))
            {
                if (dataset != null && dataset.Tables.Count > 0) { return dataset; }
                else return new DataSet();
            }
        }

        public DataSet ExecuteSQLReturnDataset(string sql, string dataSetName, string cacheName, DateTime duration, bool cachEnable = false)
        {
            var _applicationCachingService = ServiceFactory.GetInstance<IApplicationCachingService>();
            if(cachEnable)
            {
                if (_applicationCachingService == null) { throw new Exception("ApplicationCachingService has not been initialized \n\n Calling Method:ExecuteSQLReturnDataset ()"); }
                if (_applicationCachingService.Exist(cacheName))
                {
                    return _applicationCachingService.GetItem<DataSet>(cacheName);
                }
            }

            using (var dataset = _database.ExecuteDataSet(CommandType.Text, sql))
            {
                if (cachEnable)
                {
                    _applicationCachingService.AddItem(cacheName, dataset, duration);
                }

                if (dataset != null && dataset.Tables.Count > 0) { return dataset; }
                else return new DataSet();
            }
        }

        public DataSet ExecuteSQLReturnDataset(string sql, string dataSetName, string cacheName, bool cachEnable = false)
        {
            var _appConfigService = ServiceFactory.GetInstance<IAppConfigService>();
            double defaultCacheMinutes = 60;
            if (_appConfigService != null) 
            { 
                //throw new Exception("InterpriseAppConfigService has not been initialized \n\n Calling Method:ExecuteSQLReturnDataset()"); 
                defaultCacheMinutes = _appConfigService.CacheDurationMinutes;
            }

            return ExecuteSQLReturnDataset(sql, dataSetName, cacheName, DateTime.Now.AddMinutes(defaultCacheMinutes), cachEnable);
        }


        public T ExecuteSQLScalar<T>(string sql)
        {
            return (T)_database.ExecuteScalar(CommandType.Text, sql);
        }

        public T ExecuteSQLByQueryIDScalar<T>(string dbQueryId)
        {
            return ExecuteSQLByQueryIDScalar<T>(dbQueryId, null);
        }

        public T ExecuteSQLByQueryIDScalar<T>(string dbQueryId, params object[] queryParams)
        {
            return (T)_database.ExecuteScalar(CommandType.Text, GetQueryConfig(dbQueryId, queryParams));
        }


        #endregion

        #region Stored Procedure

        
        public void ExecuteSP(string sql, SqlParameter[] param = null)
        {

            var connection = _database.CreateConnection();
            DbCommand dbCommand = _database.GetStoredProcCommand(sql);
            dbCommand.Connection = connection;
            foreach (var item in param)
	        {
		        var dbParam = dbCommand.CreateParameter();
                dbParam.ParameterName = item.ParameterName;
                dbParam.Value = item.Value;
                dbCommand.Parameters.Add(dbParam);
	        }

            _database.ExecuteNonQuery(dbCommand);

                //.ExecuteNonQuery(sql, param);
        }

        public IDataReader ExecuteSPReturnReader(string sql, SqlParameter[] param = null)
        {
            return _database.ExecuteReader(sql, param);
        }

        public DataTable ExecuteSPReturnTable<T>(string sql, SqlParameter[] param = null)
        {
            using (var dataset = _database.ExecuteDataSet(sql, param))
            {
                if (dataset.Tables.Count > 0) { return dataset.Tables[0]; }
            }

            return new DataTable();
        }

        public T ExecuteSPReturnScalar<T>(string sql, SqlParameter[] param = null)
        {
            return (T)_database.ExecuteScalar(sql, param);
        }


        #endregion

        
        public string GetQueryConfig(string id)
        {
            //Dependent services
            var _dbScriptConfigService = ServiceFactory.GetInstance<IDbScriptConfigService>();
            if (_dbScriptConfigService == null)
            {
                throw new DBQueryServiceNotCreatedException(
                    String.Format("DBQuery Service object has not been Initialized: {0}", "IDbScriptConfigService"));
            }

            string query = _dbScriptConfigService.Get(id);
            if (query.IsNullOrEmptyTrimmed())
            {
                throw new DBQueryIDNotExistException(String.Format("DBQuery ID does not exist: {0}", id));
            }
            return query;
        }

        public string GetQueryConfig(string id, params object[] parameters)
        {
            return GetQueryConfig(id).FormatWith(parameters);
        }

        public void TryConnectToDB()
        {
            try
            {
                using (var connection = _database.CreateConnection())
                {
                    connection.Open();
                    connection.Close();
                }
            }
            catch 
            {
                throw new ConnectionStringNotInitializedException();
            }
        }

        #endregion

        #region Properties
        #endregion

        #region Destructor

        public void Dispose()
        {
            _database = null;
        }

        #endregion

    }
}