﻿using System;
using System.Linq;
using StructureMap.Graph;
using StructureMap.Configuration.DSL;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.Domain
{
    public class SiteTypeScanner : IRegistrationConvention
    {
        public void Process(Type type, Registry registry)
        {
            if (type.IsInterface || type.IsAbstract || !type.IsClass || type.IsNotPublic) return;

            Type searchedType = null;
            string repositoryName = typeof(IRepository).Name;

            //check if type implements IRepository
            var interfaceType = type.GetInterface(repositoryName);
            if (interfaceType != null)
            {
                searchedType = type.GetInterfaces()
                                   .FirstOrDefault(i => !i.Name.Equals(repositoryName, StringComparison.InvariantCulture));

                //If no interface implemented for the plugin except "IRepository" then use the type itself
                if (searchedType == null) { searchedType = type; }

                registry.For(searchedType)
                        .Use(type);
                return;
            }

            string serviceName = typeof(IService).Name;
            interfaceType = type.GetInterface(serviceName);
            if (interfaceType != null) 
            {
                searchedType = type.GetInterfaces()
                                   .FirstOrDefault(i => !i.Name.Equals(serviceName, StringComparison.InvariantCulture));

                //If no interface implemented for the plugin except "IService" then use the type itself
                if (searchedType == null) { searchedType = type; }

                registry.For(searchedType)
                        .Singleton()
                        .Use(type);
            }
        }
    }
}