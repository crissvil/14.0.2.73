// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Collections;
using System.Web;

namespace InterpriseSuiteEcommerceCommon
{
    public class AppConfig
    {
        #region private variables
        private int m_Appconfigid;
        private Guid m_Appconfigguid;
        private string m_Name;
        private string m_Description;
        private string m_Configvalue;
        private string m_Groupname;
        private bool m_Superonly;
        private DateTime m_Createdon;
        #endregion

        #region contructors
        public AppConfig() { }

        public AppConfig(int AppConfigID)
        {

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader rs = DB.GetRSFormat(con, "EcommerceGetAppconfig " + AppConfigID.ToString()))
                {
                    if (rs.Read())
                    {
                        m_Appconfigid = DB.RSFieldInt(rs, "AppConfigID");
                        m_Appconfigguid = DB.RSFieldGUID2(rs, "AppConfigGUID");
                        m_Name = DB.RSField(rs, "Name");
                        m_Description = DB.RSField(rs, "Description");
                        m_Configvalue = DB.RSField(rs, "ConfigValue").Trim();
                        m_Groupname = DB.RSField(rs, "GroupName");
                        m_Superonly = DB.RSFieldBool(rs, "SuperOnly");
                        m_Createdon = DB.RSFieldDateTime(rs, "CreatedOn");
                    }
                }
            }
        }

        public AppConfig(int AppConfigID, Guid Appconfigguid, string Name, string Description, string Configvalue, string Groupname, bool Superonly, DateTime Createdon)
        {
            m_Appconfigid = AppConfigID;
            m_Appconfigguid = Appconfigguid;
            m_Name = Name;
            m_Description = Description;
            m_Configvalue = Configvalue;
            m_Groupname = Groupname;
            m_Superonly = Superonly;
            m_Createdon = Createdon;
        }

        #endregion

        #region static methods

        public static AppConfig Create(int AppConfigID, string Name, string Description, string ConfigValue, string GroupName, bool SuperOnly)
        {
            ConfigValue = ConfigValue.Trim();

            if (Name.Trim().Length == 0)
            {
                return null;
            }

            if (GroupName.Trim().Length == 0)
            {
                GroupName = "Custom";
            }

            if (AppConfigID > 0)
            {
                AppConfig a = new AppConfig(AppConfigID);
                return a;
            }
            return null;

        }
        #endregion

        #region public properties
        public int AppConfigID
        {
            get { return m_Appconfigid; }
        }

        public Guid AppConfigGUID
        {
            get { return m_Appconfigguid; }
        }

        public string Name
        {
            get { return m_Name; }
        }

        public string Description
        {
            get { return m_Description; }
            set
            {
                m_Description = value;
            }
        }

        public string ConfigValue
        {
            get { return m_Configvalue.Trim(); }
            set
            {
                m_Configvalue = value;
            }
        }

        public string GroupName
        {
            get { return m_Groupname; }
            set
            {
                m_Groupname = value;
            }
        }

        public bool SuperOnly
        {
            get { return m_Superonly; }
            set
            {
                m_Superonly = value;
            }
        }

        public DateTime CreatedOn
        {
            get { return m_Createdon; }
        }
        #endregion
    }

    public class AppConfigs : IEnumerable
    {
        public SortedList m_AppConfigs;

        public AppConfigs()
        {
            m_AppConfigs = new SortedList();
        }

        public static AppConfigs GetAll()
        {
            AppConfigs configs = new AppConfigs();


            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "EcommerceGetAppconfig @WebsiteCode = {0}", DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode)))
                {
                    while (reader.Read())
                    {
                        AppConfig config =
                        new AppConfig(
                            DB.RSFieldInt(reader, "AppConfigID"),
                            DB.RSFieldGUID2(reader, "AppConfigGUID"),
                            DB.RSField(reader, "Name"),
                            DB.RSField(reader, "Description"),
                            DB.RSField(reader, "ConfigValue"),
                            DB.RSField(reader, "GroupName"),
                            DB.RSFieldBool(reader, "SuperOnly"),
                            DB.RSFieldDateTime(reader, "CreatedOn")
                        );

                        configs.Add(config);
                    }
                }
            }

            return configs;
        }

        public AppConfig this[string name]
        {
            get
            {
                return (AppConfig)m_AppConfigs[name.ToLowerInvariant()];
            }
        }

        public AppConfig this[int appconfigid]
        {
            get
            {
                SortedList syncdSL = SortedList.Synchronized(m_AppConfigs);

                for (int i = 0; i < syncdSL.Count; i++)
                {
                    if (((AppConfig)syncdSL.GetByIndex(i)).AppConfigID == appconfigid)
                    {
                        return (AppConfig)syncdSL.GetByIndex(i);
                    }
                }
                return null;
            }
        }

        ///// <summary>
        ///// Adds an existing AppConfig object to the collection
        ///// </summary>
        public void Add(AppConfig appconfig)
        {
            if (!m_AppConfigs.ContainsKey(appconfig.Name.ToLowerInvariant()))
                m_AppConfigs.Add(appconfig.Name.ToLowerInvariant(), appconfig);
        }

        ///// <summary>
        ///// Creates a new AppConfig record and adds it to the collection
        ///// </summary>
        public void Add(string Name, string Description, string ConfigValue, string GroupName, bool SuperOnly)
        {
            this.Add(AppConfig.Create(0, Name, Description, ConfigValue.Trim(), GroupName, SuperOnly));
        }

        ///// <summary>
        ///// Deletes the AppConfig record and removes the item from the collection
        ///// </summary>
        public void Remove(string name)
        {
            try
            {
                m_AppConfigs.Remove(name);
            }
            catch { }
        }

        public int Count
        {
            get { return m_AppConfigs.Count; }
        }

        public IEnumerator GetEnumerator()
        {
            return new AppConfigsEnumerator(this);
        }

    }

    public class AppConfigsEnumerator : IEnumerator
    {
        private int position = -1;
        private AppConfigs m_appconfigs;

        public AppConfigsEnumerator(AppConfigs appconfigscol)
        {
            this.m_appconfigs = appconfigscol;
        }

        public bool MoveNext()
        {
            if (position < m_appconfigs.m_AppConfigs.Count - 1)
            {
                position++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            position = -1;
        }

        public object Current
        {
            get
            {
                return m_appconfigs.m_AppConfigs[position];
            }
        }
    }

}



