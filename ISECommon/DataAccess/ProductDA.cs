﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.DataAccess
{
    public class ProductDA
    {
        public static IEnumerable<string> GetProductUnitMeasureAvailability(string customerCode, string itemCode, bool showFromAllWareHouse, bool isAnonymous)
        {
            var unitMeasureCodes = new List<string>();
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "exec eCommerceGetProductUnitMeasureAvailability @CustomerCode = {0}, @ItemCode = {1}, @IncludeAllWarehouses = {2}, @Anon = {3}",
                                                        DB.SQuote(customerCode), 
                                                        DB.SQuote(itemCode), 
                                                        showFromAllWareHouse, 
                                                        isAnonymous))
                {
                    while (reader.Read())
                    {
                        unitMeasureCodes.Add(DB.RSField(reader, "UnitMeasureCode"));
                    }
                }
            }
            return unitMeasureCodes.AsEnumerable();
        }

        public static IEnumerable<Product> GetProductsStockTotal(string languageCode, string filter)
        {
            bool isAllWarehouses = AppLogic.AppConfigBool("ShowInventoryFromAllWarehouses");
            string query = @"SELECT (SELECT SUM(FreeStock) FROM AvailabilityView WHERE ItemCode = IID.ItemCode {1}) AS FreeStock, 
		                                (SELECT SUM(InStock) FROM AvailabilityView WHERE ItemCode = IID.ItemCode {1}) AS InStock, 
		                                (SELECT SUM([Committed]) FROM AvailabilityView WHERE ItemCode = IID.ItemCode {1}) AS [Committed], 
		                                II.ItemCode,
		                                II.ItemName,
		                                IID.ItemDescription, 
                                        II.IsCBN, 
                                        II.CBNItemID, 
                                        II.Status
                            FROM InventoryItem II WITH (NOLOCK)
                            INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON IID.ItemCode = II.ItemCode
                            WHERE IID.LanguageCode = {0}".FormatWith(DB.SQuote(languageCode), 
                                                                    (isAllWarehouses) ? String.Empty: " AND WareHouseCode = (SELECT DefaultWarehouse FROM InventoryPreference) ");
            
            if (filter != String.Empty) { query = "SELECT * FROM ({0}) A WHERE {1}".FormatWith(query, filter); }

            var products = new List<Product>();
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRS(query, con))
                {
                    while (reader.Read())
                    {
                        decimal freeStock = decimal.Zero;
                        bool isCBN = false;
                        int cbnItemId = 0;
                        string itemCode = DB.RSField(reader, "ItemCode");
                        isCBN = DB.RSFieldBool(reader, "IsCBN");
                        cbnItemId = DB.RSFieldInt(reader, "CBNItemID");
                        
                        if (isCBN && !cbnItemId.IsNullOrEmptyTrimmed())
                        {
                            var umInfo = InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);
                            string cbnUMCode = InterpriseHelper.GetCBNUnitMeasureCode(itemCode, umInfo.Code);
                            Interprise.Facade.Base.CBN.CBNTransactionFacade cbnTransactionFacade = new Interprise.Facade.Base.CBN.CBNTransactionFacade();
                            freeStock = cbnTransactionFacade.CheckSupplierAvailableStock(cbnItemId.ToString(), cbnUMCode);
                            freeStock = DB.RSFieldDecimal(reader, "FreeStock") + (freeStock < 0 ? decimal.Zero : freeStock);
                        }
                        else 
                        {
                            freeStock = DB.RSFieldDecimal(reader, "FreeStock");
                        }

                        var item = new Product()
                        {
                            ItemCode = itemCode,
                            ItemName = DB.RSField(reader, "ItemName"),
                            ItemDescription = DB.RSField(reader, "ItemDescription"),
                            StockTotal = new ProductStockTotal()
                            {
                                FreeStock = freeStock,
                                InStock = DB.RSFieldDecimal(reader, "InStock"),
                                Committed = DB.RSFieldDecimal(reader, "Committed")
                            }
                        };
                        products.Add(item);
                    }
                }
            }
            return products.AsEnumerable();
        }

        public static bool UpdateDefaultImageSize(string itemCode, string fileName, string size, string websiteCode)
        { 
            bool retVal = false;
            string query = String.Empty;

            if (size.ToLowerInvariant() == "medium")
            {
                //reset the image defaults
                query = "UPDATE InventoryOverrideImage SET IsDefaultMedium = 0 WHERE ItemCode = {0} AND WebSiteCode = {1}"
                                                        .FormatWith(itemCode.ToDbQuote(), websiteCode.ToDbQuote());
                DB.ExecuteSQL(query);

                query = "UPDATE InventoryOverrideImage SET IsDefaultMedium = 1 WHERE ItemCode = {0} AND [FileName] = {1} AND WebSiteCode = {2}"
                                                        .FormatWith(itemCode.ToDbQuote(), fileName.ToDbQuote(), websiteCode.ToDbQuote());
                DB.ExecuteSQL(query);

                retVal = true;
            }
            else if (size.ToLowerInvariant() == "icon")
            {
                query = "UPDATE InventoryOverrideImage SET IsDefaultIcon = 0 WHERE ItemCode = {0} AND WebSiteCode = {1}"
                                                        .FormatWith(itemCode.ToDbQuote(), websiteCode.ToDbQuote());
                DB.ExecuteSQL(query);

                query = "UPDATE InventoryOverrideImage SET IsDefaultIcon = 1 WHERE ItemCode = {0} AND [FileName] = {1} AND WebSiteCode = {2}"
                                                        .FormatWith(itemCode.ToDbQuote(), fileName.ToDbQuote(), websiteCode.ToDbQuote());
                DB.ExecuteSQL(query);
                retVal = true;
            }

            return retVal;
        }
        
        public static IEnumerable<string> GetProductTypes()
        {
            var itemTypes = new List<string>();
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, @"SELECT ItemType 
                                                          FROM InventoryItemType WITH (NOLOCK) 
                                                          WHERE ItemType IN ('Stock', 'Kit', 'Matrix Group', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly')"))
                {
                    while (reader.Read())
                    {
                        itemTypes.Add(DB.RSField(reader, "ItemType"));
                    }
                }
            }
            return itemTypes.AsEnumerable();
        }

        public static Product GetProductInfo(string itemCode)
        {
            string query = "SELECT IsDropShip, IsSpecialOrder, IsCBN, CBNItemID FROM InventoryItem WITH (NOLOCK) WHERE ItemCode={0}".FormatWith(itemCode.ToDbQuote());
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, query))
                {
                    if (reader.Read())
                    {
                        var product = new Product()
                        {
                            IsDropShip = DB.RSFieldBool(reader, "IsDropShip"),
                            IsSpecialOrder = DB.RSFieldBool(reader, "IsSpecialOrder"),
                            IsCBN = DB.RSFieldBool(reader, "IsCBN"),
                            CBNItemID = DB.RSFieldInt(reader, "CBNItemID")
                        };
                        return product;
                    }
                }
            }
            return null;
        }

        public static IEnumerable<Product> GetProductSubstitutes(string itemCode, Customer thisCustomer)
        {
            var itemSubstitute = new List<Product>();

            // NOTE:
            //  Because of the Cache API uses unique keys cache entries
            //  We SHOULD LEAVE OUT the CurrentDate parameters since DateTime.Now
            //  is NON-Deterministic, it will return a new unique value EVERYTIME it's called
            //  Hence adding it to our cache key would work out against us since we'll be adding
            //  a NEW cache entry EVERYTIME.

            string query = "EXEC GetEcommerceSubstituteItems @ItemCode = {0}, @WebSiteCode = {1}, @ContactCode = {2}, @CurrentDate = {3}, @ProductFilterID = {4}".FormatWith(
                                itemCode.ToDbQuote(),
                                InterpriseHelper.ConfigInstance.WebSiteCode.ToDbQuote(),
                                thisCustomer.ContactCode.ToDbQuote(),
                                DateTime.Now.ToString().ToDbQuote(),
                                thisCustomer.ProductFilterID.ToDbQuote());

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, query))
                {
                    while (reader.Read())
                    {
                        var item = new Product()
                        {
                            Counter = DB.RSFieldInt(reader, "Counter"),
                            ItemName = DB.RSField(reader, "ItemName"),
                            ItemDescription = DB.RSField(reader, "ItemDescription"),
                            ExtendedDescription = DB.RSField(reader, "ExtendedDescription"),
                            WebDescription = DB.RSField(reader, "WebDescription")
                        };
                        itemSubstitute.Add(item);
                    }
                }
            }
            return itemSubstitute.AsEnumerable();
        }

        public static IEnumerable<Product> GetProductAccessories(string itemCode, Customer thisCustomer, string customerCode)
        {
            var itemAccessories = new List<Product>();
            string query = "EXEC EcommerceGetAccessoryItems @CustomerCode = {0}, @WebSiteCode = {1}, @ItemCode = {2}, @LanguageCode = {3}, @CurrentDate = {4}, @ProductFilterID = {5}, @ContactCode = {6}".FormatWith( 
                                customerCode.ToDbQuote(),
                                InterpriseHelper.ConfigInstance.WebSiteCode.ToDbQuote(),
                                itemCode.ToDbQuote(),
                                thisCustomer.LanguageCode.ToDbQuote(),
                                DateTime.Now.ToString().ToDbQuote(),
                                thisCustomer.ProductFilterID.ToDbQuote(),
                                thisCustomer.ContactCode.ToDbQuote());

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, query))
                {
                    while (reader.Read())
                    {
                        var item = new Product()
                        {
                            Counter = DB.RSFieldInt(reader, "Counter"),
                            ItemType = DB.RSField(reader, "ItemType"),
                            ItemCode = DB.RSField(reader, "ItemCode"),
                            ItemDescription = DB.RSField(reader, "ItemDescription"),
                            WebDescription = DB.RSField(reader, "WebDescription"),
                            AccessoryName = DB.RSField(reader, "AccessoryName"),
                            AccessoryCode = DB.RSField(reader, "AccessoryCode"),
                        };
                        itemAccessories.Add(item);
                    }
                }
            }
            return itemAccessories.AsEnumerable();
        }

        public static IEnumerable<string> GetProductCategories(string itemCode)
        {
            var categories = new List<string>();
            string query = "SELECT IC.CategoryCode FROM InventoryItemWebOption IIW with (NOLOCK) LEFT JOIN InventoryCategory IC with (NOLOCK) ON IC.ItemCode = IIW.ItemCode WHERE IIW.ItemCode = {0}".FormatWith(itemCode.ToDbQuote());

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, query))
                {
                    while (reader.Read())
                    {
                        string categoryCode = DB.RSField(reader, "CategoryCode");
                        categories.Add(categoryCode);
                    }
                }
            }
            return categories.AsEnumerable();
        }

        public static IEnumerable<Product> GetAlsoPurchasedProduct(string itemCode, Customer thisCustomer, int displayLimit, string filterByCategories)
        {
            var purchasedProducts = new List<Product>();

            string query = @"EXEC EcommerceGetPurchasedProducts @WebSiteCode={0}, @ItemCode={1}, @CategoryCode={2}, @LanguageCode={3}, @CurrentDate={4}, 
                                                                @ProductsToDisplay = {5}, @ProductFilterID = {6}, @ContactCode = {7}".FormatWith( InterpriseHelper.ConfigInstance.WebSiteCode.ToDbQuote(),
                                                                                                                                     itemCode.ToDbQuote(),
                                                                                                                                     filterByCategories.ToDbQuote(),
                                                                                                                                     thisCustomer.LanguageCode.ToDbQuote(),
                                                                                                                                     DateTime.Now.ToDateTimeStringForDB().ToDbQuote(),
                                                                                                                                     displayLimit,
                                                                                                                                     thisCustomer.ProductFilterID.ToDbQuote(),
                                                                                                                                     thisCustomer.ContactCode.ToDbQuote());

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using(var reader = DB.GetRSFormat(con, query))
                {
                    while (reader.Read())
                    {
                        var product = new Product()
                        {
                            Counter = DB.RSFieldInt(reader, "Counter"),
                            ItemCode = DB.RSField(reader, "ItemCode"),
                            ItemName = DB.RSField(reader, "ItemName"),
                            ItemDescription = DB.RSField(reader, "ItemDescription"),
                            WebDescription = DB.RSField(reader, "WebDescription")
                        };
                        purchasedProducts.Add(product);
                    }
                }
            }



            return purchasedProducts.AsEnumerable();
        }

        public static IEnumerable<Product> GetAlsoViewedProduct(string itemCode, Customer thisCustomer, int displayLimit, string filterByCategories)
        {
            var viewedProducts = new List<Product>();

            string query = @"EXEC EcommerceGetViewedProducts  @WebSiteCode = {0}, @ItemCode = {1}, @CategoryCode = {2}, @LanguageCode = {3}, @CurrentDate = {4}, 
                                                              @ProductsToDisplay = {5}, @ProductFilterID = {6}, @ContactCode = {7}".FormatWith(InterpriseHelper.ConfigInstance.WebSiteCode.ToDbQuote(),
                                                                                                                                     itemCode.ToDbQuote(),
                                                                                                                                     filterByCategories.ToDbQuote(),
                                                                                                                                     thisCustomer.LanguageCode.ToDbQuote(),
                                                                                                                                     DateTime.Now.ToDateTimeStringForDB().ToDbQuote(),
                                                                                                                                     displayLimit,
                                                                                                                                     thisCustomer.ProductFilterID.ToDbQuote(),
                                                                                                                                     thisCustomer.ContactCode.ToDbQuote());

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, query))
                {
                    while (reader.Read())
                    {
                        var product = new Product()
                        {
                            Counter = DB.RSFieldInt(reader, "Counter"),
                            ItemCode = DB.RSField(reader, "ItemCode"),
                            ItemName = DB.RSField(reader, "ItemName"),
                            ItemDescription = DB.RSField(reader, "ItemDescription"),
                            WebDescription = DB.RSField(reader, "WebDescription")
                        };
                        viewedProducts.Add(product);
                    }
                }
            }



            return viewedProducts.AsEnumerable();
        }

        public static string GetDuplicateImageFilename(string filename, string websiteCode)
        {
            string itemCode = String.Empty;

            string query = "SELECT TOP 1 ItemCode FROM InventoryOverrideImage WITH (NOLOCK) WHERE [Filename]={0} AND WebsiteCode = {1}".FormatWith(filename.ToDbQuote(), websiteCode.ToDbQuote());
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, query))
                {
                    if (reader.Read())
                    {
                        itemCode = reader.ToRSField("ItemCode");
                    }
                }
            }
            return itemCode;
        }
    }
}
