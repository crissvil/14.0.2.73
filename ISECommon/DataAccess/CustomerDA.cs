﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Text;
using Interprise.Framework.Customer.DatasetComponent;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.DataAccess
{
    public class CustomerDA
    {
        /// <summary>
        /// Retrieve all payment term group of specific customer's contact
        /// </summary>
        /// <param name="contactCode">string Contact Code</param>
        /// <returns> IEnumerable(PaymentTermDTO)</returns>
        public static IEnumerable<PaymentTermDTO> GetAllForGroup(string contactCode, Address preferredShippingAddress)
        {
            var terms = new PaymentTermDTO.PaymentTermGroup();
            int ctr = 1;
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "exec eCommerceGetPaymentTermGroup @ContactCode = {0}, @IsAnonymous = {1}, @CountryCode = {2}, @BusinessType = {3}",
                                                        contactCode.ToDbQuote(),
                                                        Convert.ToInt32(Customer.Current.IsNotRegistered),
                                                        preferredShippingAddress.Country.ToDbQuote(),
                                                        Customer.Current.DefaultPrice.ToDbQuote()))
                {
                    while (reader.Read())
                    {
                        if (reader.ToRSField("PaymentTermCode") != AppLogic.AppConfig("PaymentTermCodeZeroDollarOrder"))
                        {
                            var term = new PaymentTermDTO(reader.ToRSField("PaymentTermCode"),
                                                    reader.ToRSField("PaymentMethodCode"),
                                                    reader.ToRSField("PaymentTermDescription"),
                                                    reader.ToRSField("PaymentType"),
                                                    true,
                                                    ctr++);

                            term.IsSelected = reader.ToRSFieldBool("IsDefault");
                            terms.AddTerm(term);
                        }
                    }
                }
                con.Close();
            }
            terms.EnsureHasSelected();
            return terms;
        }


        /// <summary>
        /// Retrieve credit card & paypal payment term group of specific customer's contact
        /// </summary>
        /// <param name="contactCode">string Contact Code</param>
        /// <returns> IEnumerable(PaymentTermDTO)</returns>
        public static IEnumerable<PaymentTermDTO> GetOnlinePaymentTerms(string contactCode, Address preferredShippingAddress)
        {
            var terms = new PaymentTermDTO.PaymentTermGroup();
            int ctr = 1;
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "exec eCommerceGetPaymentTermGroup @ContactCode = {0}, @IsAnonymous = {1}, @CountryCode = {2}, @BusinessType = {3}",
                                                        contactCode.ToDbQuote(),
                                                        Convert.ToInt32(Customer.Current.IsNotRegistered),
                                                        preferredShippingAddress.Country.ToDbQuote(),
                                                        Customer.Current.DefaultPrice.ToDbQuote()))
                {

                    var allowedPaymentTerms = AppLogic.AppConfig("OnlinePaymentTermsAllowed");
                    var arrPaymentTerms = allowedPaymentTerms.Split(',').Select(p => p.Trim().ToLowerInvariant());
                    bool hasDefaultOption = false;
                    PaymentTermDTO term = null;
                    while (reader.Read())
                    {
                        string paymentTermCode = reader.ToRSField("PaymentTermCode");
                        bool isDefault = reader.ToRSFieldBool("IsDefault");

                        if (arrPaymentTerms.Any(i => i == paymentTermCode.ToLowerInvariant()))
                        {
                            if (isDefault)
                            {
                                hasDefaultOption = true;
                            }
                            //if (paymentTermCode.ToLowerInvariant() == "invoice")
                            //{
                            //    if (Customer.Current.Type == "Company")
                            //    {
                            //        term = new PaymentTermDTO(paymentTermCode, reader.ToRSField("PaymentMethodCode"), reader.ToRSField("PaymentTermDescription"), true, ctr++);
                            //        term.IsSelected = isDefault;
                            //        terms.AddTerm(term);
                            //    }
                            //}
                            //else
                            //{
                            //    term = new PaymentTermDTO(paymentTermCode, reader.ToRSField("PaymentMethodCode"), reader.ToRSField("PaymentTermDescription"), true, ctr++);
                            //    term.IsSelected = isDefault;
                            //    terms.AddTerm(term);
                            //}
                            term = new PaymentTermDTO(paymentTermCode, reader.ToRSField("PaymentMethodCode"), reader.ToRSField("PaymentTermDescription"), reader.ToRSField("PaymentType"), true, ctr++);
                            term.IsSelected = isDefault;
                            terms.AddTerm(term);
                        }
                    }

                    if (hasDefaultOption == false)
                    {
                        terms.FirstOrDefault(i => i.IsSelected = true);
                    }

                }
                con.Close();
            }

            terms.EnsureHasSelected();
            return terms;
        }

        /// <summary>
        /// Validate if address has changes from the DB vs inputted address
        /// </summary>
        /// <param name="creditCardRow">CreditCardDataset.CustomerCreditCardViewRow object</param>
        /// <param name="billToAddress">Address object</param>
        /// <returns>boolean</returns>
        public static bool HasChangesToAddressInfo(CreditCardDataset.CustomerCreditCardViewRow creditCardRow, Address billToAddress)
        {
            bool hasChanges = false;
            try
            {
                hasChanges = !(creditCardRow.NameOnCard == billToAddress.CardName &&
                        creditCardRow.PostalCode == billToAddress.PostalCode &&
                        creditCardRow.City == billToAddress.City &&
                        creditCardRow.Country == billToAddress.Country &&
                        creditCardRow.State == billToAddress.State &&
                        creditCardRow.Address == billToAddress.Address1 &&
                        creditCardRow.Telephone == billToAddress.Phone);
            }
            catch (InvalidCastException)
            {

                //Added some exception handling for IS Address validation
                //creation of Address in IS might omit filling-up fields that might required in Ecommerce.
                //So, when exception occurs, address has been changed in Ecommerce.

                hasChanges = true;
            }
            catch (StrongTypingException)
            {

                //Added some exception handling for IS Address validation
                //creation of Address in IS might omit filling-up fields that might required in Ecommerce.
                //So, when exception occurs, address has been changed in Ecommerce.

                hasChanges = true;
            }
            catch
            {
                throw;
            }

            return hasChanges;
        }

        /// <summary>
        /// Update customer credit card type
        /// </summary>
        /// <param name="customerCode">string customerCode</param>
        /// <param name="creditCardCode">string creditCardCode</param>
        /// <param name="cardType">string cardType</param>
        public static void UpdateCustomerCreditCardType(string customerCode, string creditCardCode, string cardType)
        {
            DB.ExecuteSQL("UPDATE CustomerCreditCard SET CreditCardType = {0} WHERE CustomerCode = {1} AND CreditCardCode = {2}",
                                    cardType.ToDbQuote(),
                                    customerCode.ToDbQuote(),
                                    creditCardCode.ToDbQuote());
        }

        /// <summary>
        /// Update Customer Business Type
        /// </summary>
        /// <param name="customerCode">string customerCode</param>
        /// <param name="businessType">InterpriseSuiteEcommerceCommon.Customer.BusinessTypes businessType</param>
        public static void UpdateCustomerBusinessType(string customerCode, InterpriseSuiteEcommerceCommon.Customer.BusinessTypes businessType)
        {
            DB.ExecuteSQL("UPDATE Customer SET BusinessType = {0} WHERE CustomerCode = {1}",
                                    businessType.ToString().ToDbQuote(),
                                    customerCode.ToDbQuote());
        }

        /// <summary>
        /// Update Customer Tax Number
        /// </summary>
        /// <param name="customerCode">string customerCode</param>
        /// <param name="taxNumber">string taxNumber</param>
        public static void UpdateCustomerBusinessTaxNumber(string customerCode, string taxNumber)
        {
            DB.ExecuteSQL("UPDATE Customer SET TaxNumber = {0} WHERE CustomerCode = {1}",
                                    taxNumber.ToDbQuote(),
                                    customerCode.ToDbQuote());
        }

        /// <summary>
        /// Clears CustomerShipTo / CustomerCreditCard Plus4 field value (Only applicable for US postal code)
        /// </summary>
        /// <param name="customerCode">string code</param>
        /// <param name="taxNumber">AddressTypes addressType</param>
        [Obsolete("Use this method: ServiceFactory.GetInstance<ICustomerRepostory>().ClearCustomerShippingAddressPlus4Field() OR ServiceFactory.GetInstance<ICustomerRepostory>().ClearCustomerBillingAddressPlus4Field()")]
        public static void ClearCustomerAddressPlus4Field(string code, AddressTypes addressType)
        {
            if (addressType == AddressTypes.Shipping)
            {
                DB.ExecuteSQL("UPDATE CustomerShipTo SET Plus4 = NULL WHERE ShipToCode = {0}", code.ToDbQuote());

            }else{

                DB.ExecuteSQL("UPDATE CustomerCreditCard SET Plus4 = NULL WHERE CreditCardCode = {0}", code.ToDbQuote());
            }

        }

        /// <summary>
        /// Update CustomerCreditCard Plus4 field (Only applicable for US postal code)
        /// </summary>
        /// <param name="customerCode">string code</param>
        /// <param name="taxNumber">int value</param>
        public static void UpdateCustomerCreditCardPlus4Field(string code, int value)
        {
            if (value > 0)
            {
                DB.ExecuteSQL("UPDATE CustomerCreditCard SET Plus4 = {1} WHERE CreditCardCode = {0}", code.ToDbQuote(), value.ToString());
            }

        }

        public static IEnumerable<CustomerInfo> GetCustomers()
        {
            string query = @"SELECT Address, City, State, PostalCode, County, Country, CustomerCode, FirstName, LastName, CustomerName, DateCreated 
                             FROM Customer WITH (NOLOCK)";

            var customers = new List<CustomerInfo>();
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRS(query, con))
                {
                    while (reader.Read())
                    {
                        var address = new List<AddressDTO>();
                        address.Add(new AddressDTO()
                        {
                            address = DB.RSField(reader, "Address"),
                            city = DB.RSField(reader, "City"),
                            state = DB.RSField(reader, "State"),
                            postalCode = DB.RSField(reader, "PostalCode"),
                            county = DB.RSField(reader, "County"),
                            country = DB.RSField(reader, "Country")
                        });

                        var customer = new CustomerInfo()
                        {
                            CustomerCode = DB.RSField(reader, "CustomerCode"),
                            FirstName = DB.RSField(reader, "FirstName"),
                            LastName = DB.RSField(reader, "LastName"),
                            FullName = DB.RSField(reader, "CustomerName"),
                            DateRegistered = DB.RSFieldDateTime(reader, "DateCreated"),
                            Address = address
                        };
                        customers.Add(customer);
                    }
                }
            }
            return customers.AsEnumerable();
        }

        public static IEnumerable<CustomerSalesOrder> GetWebSalesOrders()
        {
            string query = @"SELECT SalesOrderCode, SalesOrderDate, TotalRate, BillToCode, BillToName, CurrencyCode 
                             FROM CustomerSalesOrder WITH (NOLOCK)
                             WHERE WebsiteCode={0} AND SourceCode='Internet' AND Type='Sales Order'".FormatWith(InterpriseHelper.ConfigInstance.WebSiteCode.ToDbQuote());
            
            var orders = new List<CustomerSalesOrder>();
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRS(query, con))
                {
                    while (reader.Read())
                    {
                        var order = new CustomerSalesOrder()
                        {
                            SalesOrderCode = DB.RSField(reader, "SalesOrderCode"),
                            SalesOrderDate = DB.RSFieldDateTime(reader, "SalesOrderDate"),
                            Total = DB.RSFieldDecimal(reader, "TotalRate"),
                            CustomerCode = DB.RSField(reader, "BillToCode"),
                            CustomerName = DB.RSField(reader, "BillToName"),
                            CurrencyCode = DB.RSField(reader, "CurrencyCode")
                        };
                        orders.Add(order);
                    }
                }
            }
            return orders.AsEnumerable();
        }

        public static IEnumerable<CustomerInvoice> GetWebInvoice()
        {
            string query = @"SELECT InvoiceCode, InvoiceDate, Total 
                             FROM CustomerInvoice WITH (NOLOCK)
                             WHERE WebsiteCode={0} AND SourceCode='Internet' AND Type='Invoice'".FormatWith(InterpriseHelper.ConfigInstance.WebSiteCode.ToDbQuote());
            
            var invoices = new List<CustomerInvoice>();
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRS(query, con))
                {
                    while (reader.Read())
                    {
                        var invoice = new CustomerInvoice()
                        {
                            InvoiceCode = DB.RSField(reader, "InvoiceCode"),
                            InvoiceDate= DB.RSFieldDateTime(reader, "InvoiceDate"),
                            Total = DB.RSFieldDecimal(reader, "Total"),
                        };
                        invoices.Add(invoice);
                    }
                }
            }
            return invoices.AsEnumerable();
        }

        public static CustomerInfo GetCustomerInfoByRegistryID(Guid? giftRegistryID)
        {
            CustomerInfo customerInfo = null;

            if (!giftRegistryID.HasValue) return customerInfo;

            string query = @"SELECT cc.ContactCode, EntityCode as CustomerCode FROM crmContact cc
	                            INNER JOIN ecommerceGiftRegistry egr
	                            on cc.ContactGuid = egr.ContactGuid
	                            WHERE egr.RegistryID = {0}".FormatWith(giftRegistryID.Value.ToString().ToDbQuote());

            
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRS(query, con))
                {
                    if(reader.Read())
                    {
                        customerInfo = new CustomerInfo()
                        {
                            ContactCode = DB.RSField(reader, "ContactCode"),
                            CustomerCode = DB.RSField(reader, "CustomerCode")
                        };
                    }
                }
            }
            return customerInfo;
        }

        public static void SavePostalCode(string countryCode, string postalCode, string stateCode, string city)
        {
            using (var postalCodeGateWay = new Interprise.Framework.SystemManager.DatasetGateway.PostalCodeDatasetGateway())
            {
                using (var postalCodeFacade = new Interprise.Facade.SystemManager.PostalCodeFacade(postalCodeGateWay))
                {
                    postalCodeFacade.AssignNewPostal(postalCode, city, stateCode, countryCode);
                    postalCodeFacade.UpdateDataSet(new string[][] { 
                        new string[] {  
                            postalCodeGateWay.SystemPostalCodeView.TableName, 
                            Interprise.Framework.Base.Shared.StoredProcedures.CREATESYSTEMPOSTALCODE, 
                            Interprise.Framework.Base.Shared.StoredProcedures.UPDATESYSTEMPOSTALCODE, 
                            Interprise.Framework.Base.Shared.StoredProcedures.DELETESYSTEMPOSTALCODE
                        }, 
                        }, Interprise.Framework.Base.Shared.Enum.TransactionType.PostalCode, null);

                }
            }
        }


        public static IEnumerable<PaymentTermDTO> GetAvailablePaymentTerms(string contactCode, string country)
        {
            var terms = new PaymentTermDTO.PaymentTermGroup();
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "exec ReadEcommerceCustomerPaymentTermGroup @ContactCode = {0}, @IsAnonymous = 0, @CountryCode = {1}, @BusinessType = {2}",
                                                        DB.SQuote(contactCode),
                                                        DB.SQuote(country),
                                                        DB.SQuote(Customer.Current.DefaultPrice)))
                {

                    int ctr = 1;
                    while (reader.Read())
                    {
                        if (reader.ToRSField("PaymentTermCode") != AppLogic.AppConfig("PaymentTermCodeZeroDollarOrder"))
                        {
                            var term = new PaymentTermDTO(reader.ToRSField("PaymentTermCode"),
                                                    reader.ToRSField("PaymentMethodCode"),
                                                    reader.ToRSField("PaymentTermDescription"),
                                                    reader.ToRSField("PaymentType"),
                                                    true,
                                                    ctr++);

                            term.IsSelected = reader.ToRSFieldBool("IsDefault");
                            terms.AddTerm(term);
                        }
                    }
                }
                con.Close();
            }
            terms.EnsureHasSelected();
            return terms;
        }

        public static void RateComment(string ItemCode, string CustomerID, string ContactID, int HelpfulVal)
        {

            var ThisCustomer = ServiceFactory.GetInstance<IAuthenticationService>()
                                             .GetCurrentLoggedInCustomer();
            ThisCustomer.RequireCustomerRecord();

            string VotingCustomerID = ThisCustomer.CustomerID;
            string VotingContactID = ThisCustomer.ContactCode;

            bool AlreadyVoted = false;

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var rs = DB.GetRSFormat(con, "SELECT * FROM EcommerceRatingCommentHelpfulness with (NOLOCK) WHERE ItemCode=" + ItemCode.ToDbQuote() + 
                                                                " and RatingCustomerCode=" + CustomerID.ToDbQuote() +
                                                                " and VotingCustomerCode=" + VotingCustomerID.ToDbQuote() +
                                                                " and RatingContactCode=" + ContactID.ToDbQuote() +
                                                                " and VotingContactCode=" + VotingContactID.ToDbQuote()))
                {
                    if (rs.Read())
                    {
                        AlreadyVoted = true;
                        // they have already voted on this comment, and are changing their minds perhaps, so adjust totals, and reapply vote:
                        if (rs.ToRSFieldBool("Helpful"))
                        {
                            DB.ExecuteSQL("UPDATE EcommerceRating SET FoundHelpful = FoundHelpful-1 WHERE ItemCode=" +
                                ItemCode.ToDbQuote() + " AND CustomerCode=" + CustomerID.ToDbQuote() +
                                " AND ContactCode=" + ContactID.ToDbQuote());
                        }
                        else
                        {
                            DB.ExecuteSQL("UPDATE EcommerceRating SET FoundNotHelpful = FoundNotHelpful-1 WHERE ItemCode=" +
                                ItemCode.ToDbQuote() + " AND CustomerCode=" + CustomerID.ToDbQuote() +
                                " AND ContactCode=" + ContactID.ToDbQuote());
                        }
                    }
                }
            }

			if(AlreadyVoted)
			{
                DB.ExecuteSQL("DELETE FROM EcommerceRatingCommentHelpfulness WHERE ItemCode=" +
                    ItemCode.ToDbQuote() + " AND RatingCustomerCode=" + CustomerID.ToDbQuote() + " AND VotingCustomerCode=" + VotingCustomerID.ToDbQuote()
                    + " AND RatingContactCode=" + ContactID.ToDbQuote() + " AND VotingContactCode=" + VotingContactID.ToDbQuote());
			}

            DB.ExecuteSQL("INSERT INTO EcommerceRatingCommentHelpfulness(ItemCode,RatingCustomerCode,VotingCustomerCode,Helpful, WebsiteCode,RatingContactCode,VotingContactCode) VALUES(" +
                DB.SQuote(ItemCode) + "," + DB.SQuote(CustomerID) + "," + DB.SQuote(VotingCustomerID) + "," + HelpfulVal.ToString() + "," + DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode) + "," + DB.SQuote(ContactID) + "," + DB.SQuote(VotingContactID) + ")");
            if (HelpfulVal == 1)
			{
                DB.ExecuteSQL("UPDATE EcommerceRating SET FoundHelpful = FoundHelpful+1 WHERE ItemCode=" +
                    DB.SQuote(ItemCode) + " AND CustomerCode=" + DB.SQuote(CustomerID) + " AND ContactCode=" + DB.SQuote(ContactID));
			}
			else
			{
                DB.ExecuteSQL("UPDATE EcommerceRating SET FoundNotHelpful = FoundNotHelpful+1 WHERE ItemCode=" +
                    DB.SQuote(ItemCode) + " and CustomerCode=" + DB.SQuote(CustomerID) + " AND ContactCode=" + DB.SQuote(ContactID));
			}
        }

    }

}