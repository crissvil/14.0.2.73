﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.DataAccess
{
    public class ResourcesDA
    {

        public static bool UpdateTopic(string topicID, string content, string webSiteCode, string languageCode)
        {
            try
            {
                using (var con = new SqlConnection(DB.GetDBConn()))
                {
                    con.Open();

                    DB.ExecuteSQL(string.Format("UPDATE EcommerceTopicLanguage SET TopicContent={0} WHERE TopicId={1} AND WebsiteCode={2} AND LanguageCode ={3}",
                                                    content.ToDbQuote(),
                                                    topicID.ToDbQuote(),
                                                    webSiteCode.ToDbQuote(),
                                                    languageCode.ToDbQuote()), con);
                    con.Close();
                }

                return true;
            }
            catch
            {
                throw;
            }
        }

        public static bool UpdateItemDescription(string itemCode, string content, string contentType, string webSiteCode, string languageCode)
        {
            try
            {
                using (var con = new SqlConnection(DB.GetDBConn()))
                {
                    con.Open();

                    string sql = String.Empty;

                    if (contentType == "item-description")
                    {
                        sql = String.Format("UPDATE InventoryItemDescription SET ItemDescription = {0} WHERE ItemCode = {1} AND LanguageCode = {2}",
                                    content.ToDbQuote(),
                                    itemCode.ToDbQuote(),
                                    languageCode.ToDbQuote());
                    }
                    else
                    {

                        string fieldToUpdate = "WebDescription";

                        if (contentType == "item-summary") fieldToUpdate = "Summary";
                        if (contentType == "item-warranty") fieldToUpdate = "Warranty";


                        sql = String.Format("UPDATE InventoryItemWebOptionDescription SET {4} = {0} WHERE ItemCode = {1} AND LanguageCode = {2} AND WebsiteCode={3}",
                                   content.ToDbQuote(),
                                   itemCode.ToDbQuote(),
                                   languageCode.ToDbQuote(),
                                   webSiteCode.ToDbQuote(), fieldToUpdate);
                    }


                    DB.ExecuteSQL(sql, con);
                    con.Close();
                }

                return true;
            }
            catch
            {
                throw;
            }
        }

    }
}
