﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Data;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.DataAccess
{
    public class StoreLocatorDA
    {
        public static IEnumerable<SystemWareHouse> GetWarehouses()
        {
            var systeWarehouses = new List<SystemWareHouse>();

            using (var zipCodeGateWay = new Interprise.Framework.SystemManager.DatasetGateway.PostalCodeDatasetGateway())
            {
                using (var zipCodeFacade = new Interprise.Facade.SystemManager.PostalCodeFacade(zipCodeGateWay))
                {
                    var strZipCodeCommandSet = new string[][] { new string[] { "SystemPostalCode", "eCommerceReadSystemWarehousePostalCode" } };

                    zipCodeFacade.LoadDataSet(strZipCodeCommandSet, Interprise.Framework.Base.Shared.Enum.ClearType.Specific, Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                    //if we have some warehouses with zip codes, try and get the address to them
                    if (zipCodeGateWay.SystemPostalCode.Rows.Count > 0)
                    {
                        using (var warehouseGateway = new Interprise.Framework.Inventory.DatasetGateway.WarehouseDatasetGateway())
                        {
                            using (var warehouseFacade = new Interprise.Facade.Inventory.WarehouseFacade(warehouseGateway))
                            {
                                string[][] strWarehouseCommandSet = new string[][] { new string[] { "InventoryWarehouse", "ReadInventoryWarehouse" } };
                                warehouseFacade.LoadDataSet(strWarehouseCommandSet, Interprise.Framework.Base.Shared.Enum.ClearType.Specific, Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                                //call the function to add the pins for each warehouse
                                foreach (DataRow row in zipCodeGateWay.SystemPostalCode.Rows)
                                {
                                    var rowWarehouse = warehouseGateway.InventoryWarehouse.Select(String.Format("PostalCode = '{0}' AND Country = '{1}' AND City = '{2}' AND ISNULL(County,'') = '{3}' AND IsActive = 1", row["PostalCode"].ToString(), row["CountryCode"].ToString(), row["City"].ToString(), row["County"].ToString()));
                                    if (rowWarehouse.Length >= 1) //found an exact match - send formatted address info
                                    {
                                        foreach (var rowWh in rowWarehouse)
                                        {
                                            systeWarehouses.Add(new SystemWareHouse()
                                            {
                                                Coordinate = new Coordinate()
                                                {
                                                    Latitude = row["Latitude"].ToString().TryParseDecimal().Value,
                                                    Longtitude = row["Longitude"].ToString().TryParseDecimal().Value
                                                },
                                                Description = rowWh["WarehouseDescription"].ToString(),
                                                Address = new AddressDTO()
                                                {
                                                    address = rowWh["address"].ToString(),
                                                    state = rowWh["state"].ToString(),
                                                    postalCode = rowWh["postalcode"].ToString(),
                                                    country = rowWh["country"].ToString(),
                                                    city = rowWh["city"].ToString(),
                                                    id = rowWh["Counter"].ToString(),
                                                    phone = rowWh["Telephone"].ToString()
                                                }
                                            });
                                        }
                                    }
                                    else
                                    {
                                        systeWarehouses.Add(new SystemWareHouse()
                                        {
                                            Coordinate = new Coordinate()
                                            {
                                                Latitude = row["Latitude"].ToString().TryParseDecimal().Value,
                                                Longtitude = row["Longitude"].ToString().TryParseDecimal().Value
                                            },
                                            Description = row["City"].ToString()
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return systeWarehouses;

        }

        public static IEnumerable<SystemWareHouse> GetDealersAndWarehouses(StoreType storeType)
        {
            IEnumerable<SystemWareHouse> all = null;

            switch (storeType)
            { 
                case StoreType.All:

                    var warehouses = GetWarehouses();
                    var dealders = ServiceFactory.GetInstance<ICustomerRepository>()
                                                 .GetDealerCustomersAsWareHouse();
                    all = warehouses.Union(dealders);

                    break;
                case StoreType.Warehouse:

                    all = GetWarehouses();
                    break;

                case  StoreType.Dealer:
                    all = ServiceFactory.GetInstance<ICustomerRepository>()
                                        .GetDealerCustomersAsWareHouse();
                    break;

                default:
                    break;
            }

            return all;
        }
    }
}
