// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Interprise.Extendable.Base.Facade.Customer.CreditCardGateway;
using Interprise.Facade.Base;
using Interprise.Facade.Customer;
using Interprise.Framework.Base.DatasetComponent;
using Interprise.Framework.Base.DatasetGateway;
using Interprise.Framework.Base.Shared;
using Interprise.Framework.Customer.DatasetGateway;
using InterpriseSuiteEcommerceCommon.DataAccess;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.Web;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;
using OpenFreight.Client;
using OpenFreight.Client.Requests;
using OpenFreight.Client.Responses;
using System.Net;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration
{
    /// <summary>
    /// To properly instantiate this. Use ServiceFactory.GetInstance<IShoppingCartService>().New() method
    /// </summary>
    public class InterpriseShoppingCart : ShoppingCart, IEnumerable, IDisposable
    {
        #region Variable Declaration
        private SalesOrderDatasetGateway _gatewaySalesOrderDataset;
        private SalesOrderFacade _facadeSalesOrder;
        private Customer _customer;
        private string m_countryCode;
        private decimal m_CartFreightRate;
        #endregion

        public decimal CartFreightRate 
        {
            get { return m_CartFreightRate; }
            set { m_CartFreightRate = value; }
        }


        public string CountryCode
        {
            get { return m_countryCode; }
            set { m_countryCode = value; }
        }

        #region Constructor

        private InterpriseShoppingCart() { }

        public InterpriseShoppingCart(Dictionary<string, EntityHelper> entityHelpers,
                                        int skinId,
                                        Customer customer,
                                        CartTypeEnum type,
                                        string originalRecurringOrderNumber,
                                        bool onlyLoadRecurringItemsThatAreDue, bool LoadDatafromDB, string pagename = "", string itemCode = "")
            : base(entityHelpers, skinId, customer, type, originalRecurringOrderNumber, onlyLoadRecurringItemsThatAreDue, LoadDatafromDB, pagename, itemCode)
        {
            if (customer.ShippingAddresses.Count > 0)
            {
                CountryCode = customer.ShippingAddresses[0].Country;
            }
            // end

            _customer = customer;
        }

        #endregion

        #region  For Threading Testing Purposes Only

        private InterpriseShoppingCart(Customer ThisCustomer, CartTypeEnum cartType)
        {
            m_ThisCustomer = ThisCustomer;
            m_CartType = cartType;
            this.LoadFromDB(cartType);
        }

        public static InterpriseShoppingCart Get(Customer forThisCustomer, CartTypeEnum withCartType)
        {
            return Get(forThisCustomer, withCartType, false);
        }

        public static InterpriseShoppingCart Get(Customer forThisCustomer, CartTypeEnum withCartType, bool buildSalesOrderDetails)
        {
            InterpriseShoppingCart cart = new InterpriseShoppingCart(forThisCustomer, withCartType);
            if (buildSalesOrderDetails)
            {
                cart.BuildSalesOrderDetails();
            }
            return cart;
        }

        #endregion

        #region Properties

        public SalesOrderDatasetGateway SalesOrderDataset
        {
            get { return _gatewaySalesOrderDataset; }
        }

        public SalesOrderFacade SalesOrderFacade
        {
            get { return _facadeSalesOrder; }
        }

        public decimal ShoppingCartTotalRate
        {
            get
            {
                if (_gatewaySalesOrderDataset != null && _gatewaySalesOrderDataset.CustomerSalesOrderView.Count > 0)
                { return _gatewaySalesOrderDataset.CustomerSalesOrderView[0].TotalRate; }
                else
                    return Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(
                        Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(ThisCustomer.CurrencyCode)
                        , this.GetCartTaxTotal() + GetCartSubTotal());

            }
        }

        public override bool HasCoupon()
        {
            string couponCode = string.Empty;
            return HasCoupon(ref couponCode);
        }

        public bool HasCouponApplied()
        {
            // NOTE : 
            //  In InterpriseSuiteEcommerce we assume the receipt to hold the coupon info
            return _facadeSalesOrder.Receipt != decimal.Zero;
        }

        #endregion

        #region Enumeration

        public enum OrderStatus
        {
            None,
            MaxMindFraudCheckFailed,
            GatewayAuthorizationFailed,
            Completed
        }

        #endregion

        private OrderStatus _status = OrderStatus.None;

        public OrderStatus Status
        {
            get { return _status; }
        }

        #region Methods

        public InterpriseShoppingCart ForAddress(Address thisAddress)
        {
            var newCart = new InterpriseShoppingCart(this.EntityHelpers, this.SkinID, this.ThisCustomer, this.CartType, string.Empty, false, false);
            foreach (CartItem item in this)
            {
                if (item.m_ShippingAddressID == thisAddress.AddressID)
                {
                    newCart.CartItems.Add(item.Copy());
                }
            }

            newCart.BuildSalesOrderDetails(!IsSalesOrderDetailBuilt);

            return newCart;
        }

        public bool OnlyShippingAddressIsNotCustomerDefault()
        {
            if (this.CartItems.Count == 0 || this.HasMultipleShippingAddresses()) return false;

            return this.FirstItem().m_ShippingAddressID != ThisCustomer.PrimaryShippingAddress.AddressID;
        }

        public List<InterpriseShoppingCart> SplitIntoMultipleOrdersByDifferentShipToAddresses()
        {
            var orders = new List<InterpriseShoppingCart>();
            var nonShipCart = new InterpriseShoppingCart(this.EntityHelpers, this.SkinID, this.ThisCustomer, this.CartType, string.Empty, false, false);
            var itemsWithTheSameShipToAddress = new Dictionary<string, List<CartItem>>();

            foreach (var item in this.CartItems)
            {
                if (item.IsDownload || item.IsService)
                {
                    item.m_ShippingAddressID = ThisCustomer.PrimaryShippingAddress.AddressID;
                    nonShipCart.CartItems.Add(item.Clone<CartItem>());
                }
                else
                {
                    string shipToAddress = item.m_ShippingAddressID;

                    if (!itemsWithTheSameShipToAddress.ContainsKey(shipToAddress))
                    {
                        itemsWithTheSameShipToAddress.Add(shipToAddress, new List<CartItem>());
                    }

                    var items = itemsWithTheSameShipToAddress[shipToAddress];
                    items.Add(item);
                }
            }

            foreach (string uniqueShipToAddress in itemsWithTheSameShipToAddress.Keys)
            {
                var items = itemsWithTheSameShipToAddress[uniqueShipToAddress];
                var newCart = new InterpriseShoppingCart(this.EntityHelpers, this.SkinID, this.ThisCustomer, this.CartType, string.Empty, false, false);

                foreach (CartItem item in items)
                {
                    newCart.CartItems.Add(item.Clone<CartItem>());
                }

                orders.Add(newCart);
            }

            if (nonShipCart.CartItems.Count > 0)
            {
                orders.Add(nonShipCart);
            }

            nonShipCart.MakeShippingNotRequired(false);

            return orders;
        }

        #region BuildSalesOrderDetails

        public void BuildSalesOrderDetails(string couponCode = "")
        {
            BuildSalesOrderDetails(true, true, couponCode);
        }

        public void BuildSalesOrderDetails(bool isForCartDisplay, string couponCode = "")
        {
            BuildSalesOrderDetails(true, true, couponCode, isForCartDisplay);
        }

        public void BuildSalesOrderDetails(bool isForCartDisplay)
        {
            BuildSalesOrderDetails(true, true, String.Empty, isForCartDisplay);
        }

        public bool HasShippableComponents()
        {
            return !this.IsNoShippingRequired();
        }

        #region MakeShippingNotRequired

        public void MakeShippingNotRequired()
        {
            MakeShippingNotRequired(true);
        }

        public void MakeShippingNotRequired(bool updateCart)
        {
            var shippingService = ServiceFactory.GetInstance<IShippingService>();
            shippingService.EnsureNoShippingMethodRequiredIsExisting();
            shippingService.EnsureNoShippingMethodRequiredIsAssociatedWithCurrentCustomer(ThisCustomer.IsRegistered, ThisCustomer.AnonymousShipToCode, ThisCustomer.PrimaryShippingAddress.ShippingMethodGroup);

            string shippingMethod = AppLogic.AppConfig("ShippingMethodCodeZeroDollarOrder");

            CartItems.ForEach(item => item.m_ShippingMethod = shippingMethod);

            if (!updateCart) return;

            SetCartShippingMethod(shippingMethod);
        }

        #endregion

        #region EnsureNoShippingMethodRequiredIsExisting

        private void EnsureNoShippingMethodRequiredIsAssociatedWithCurrentCustomer()
        {
            var shippingService = ServiceFactory.GetInstance<InterpriseSuiteEcommerceCommon.Domain.Infrastructure.IShippingService>();
            shippingService.EnsureNoShippingMethodRequiredIsAssociatedWithCurrentCustomer(ThisCustomer.IsRegistered, ThisCustomer.AnonymousShipToCode, ThisCustomer.PrimaryShippingAddress.ShippingMethodGroup);
        }

        [Obsolete("Use the method : ServiceFactory.GetInstance<IShippingService>().EnsureNoShippingMethodRequiredIsExisting() if fully migrated to CB13.1.3/CB13.2 and above")]
        public static void EnsureNoShippingMethodRequiredIsExisting()
        {
            bool noShippingMethodRequiredShippingMethodExisting = false;

            string noShippingRequiredCode = AppLogic.AppConfig("ShippingMethodCodeZeroDollarOrder");

            if (CommonLogic.IsStringNullOrEmpty(noShippingRequiredCode))
            {
                throw new Exception("ShippingMethodCodeZeroDollarOrder AppConfig is not configured. Please setup a dummy shipping method code to mark orders that doesn't require shipping!!!");
            }


            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "SELECT ShippingMethodCode FROM SystemShippingMethod with (NOLOCK) WHERE ShippingMethodCode = {0}", DB.SQuote(noShippingRequiredCode)))
                {
                    noShippingMethodRequiredShippingMethodExisting = reader.Read();
                }
            }

            if (!noShippingMethodRequiredShippingMethodExisting)
            {
                throw new Exception(string.Format("ShippingMethodCodeZeroDollarOrder AppConfig:\"{0}\" is not existing in the database!!!", noShippingRequiredCode));
            }
        }

        #endregion

        public void MakePaymentTermNotRequired()
        {
            var shippingService = ServiceFactory.GetInstance<InterpriseSuiteEcommerceCommon.Domain.Infrastructure.IShippingService>();
            shippingService.EnsureNoShippingMethodRequiredIsExisting();

            var paymentTermService = ServiceFactory.GetInstance<InterpriseSuiteEcommerceCommon.Domain.Infrastructure.IPaymentTermService>();
            paymentTermService.EnsureNoPaymentTermRequiredIsAssociatedWithCurrentCustomer(ThisCustomer.PaymentTermGroup);

            InterpriseHelper.UpdateCustomerPaymentTerm(ThisCustomer, AppLogic.AppConfig("PaymentTermCodeZeroDollarOrder"));
        }

        [Obsolete("Use the method : ServiceFactory.GetInstance<IPaymentTermService>().EnsureNoPaymentTermRequiredIsAssociatedWithCurrentCustomer() if fully migrated to CB13.1.3/CB13.2 and above")]
        private void EnsureNoPaymentTermRequiredIsAssociatedWithCurrentCustomer()
        {
            string noPaymentPaymentTermCode = AppLogic.AppConfig("PaymentTermCodeZeroDollarOrder");
            bool noPaymentTermRequiredExistingAndIsAssociatedWithCustomerPaymentTermGroup = false;

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT PaymentTermCode FROM SystemPaymentTermGroupDetail with (NOLOCK) WHERE PaymentTermCode = {0} AND PaymentTermGroup = {1}", DB.SQuote(noPaymentPaymentTermCode), DB.SQuote(ThisCustomer.PaymentTermGroup)))
                {
                    noPaymentTermRequiredExistingAndIsAssociatedWithCustomerPaymentTermGroup = reader.Read();
                }
            }

            if (!noPaymentTermRequiredExistingAndIsAssociatedWithCustomerPaymentTermGroup)
            {
                //this is dynamic facade that can contain any dataset
                Interprise.Facade.Base.ListControlFacade listFacade = new Interprise.Facade.Base.ListControlFacade();

                //create current dataset
                Interprise.Framework.Base.DatasetComponent.BaseDataset listDataset = new Interprise.Framework.Base.DatasetComponent.BaseDataset("GatewayPaymentTermGroup");

                //create datatable
                listDataset.Tables.Add(listFacade.CreateTable("SYSTEMPAYMENTTERMGROUPDETAIL"));

                // set dynamic dataset
                listFacade.SetDataset = listDataset;

                //set default column definitions
                foreach (System.Data.DataTable dt in listDataset.Tables)
                {
                    listFacade.InitializeTable(dt);
                }

                if (listDataset.Tables.Contains("SYSTEMPAYMENTTERMGROUPDETAIL"))
                {
                    //add new row to dataset
                    var detailRow = listDataset.Tables[0].NewRow();
                    detailRow.BeginEdit();
                    detailRow[Interprise.Framework.Base.Shared.Const.SYSTEMPAYMENTTERMGROUPDETAILVIEW_PAYMENTTERMGROUP_COLUMN] = ThisCustomer.PaymentTermGroup;
                    detailRow[Interprise.Framework.Base.Shared.Const.SYSTEMPAYMENTTERMGROUPDETAILVIEW_PAYMENTTERMCODE_COLUMN] = noPaymentPaymentTermCode;
                    detailRow.EndEdit();
                    listDataset.Tables[0].Rows.Add(detailRow);

                    //insert data to DB
                    listFacade.UpdateDataSet(new string[][] { new string[] { "SYSTEMPAYMENTTERMGROUPDETAIL" } },
                    Interprise.Framework.Base.Shared.Enum.TransactionType.ShippingMethod, "SYSTEMPAYMENTTERMGROUPDETAIL", false);
                }

            }
        }

        [Obsolete("Use the method : ServiceFactory.GetInstance<IPaymentTermService>().EnsureNoPaymentTermRequiredIsExisting() if fully migrated to CB13.1.3/CB13.2 and above")]
        public static void EnsureNoPaymentTermRequiredIsExisting()
        {
            bool noPaymentTermRequiredPaymentTermExisting = false;
            string noPaymentPaymentTermCode = AppLogic.AppConfig("PaymentTermCodeZeroDollarOrder");

            if (CommonLogic.IsStringNullOrEmpty(noPaymentPaymentTermCode))
            {
                throw new Exception("PaymentTermCodeZeroDollarOrder AppConfig is not configured. Please setup a dummy payment term code to mark orders that doesn't require payment!!!");
            }

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT PaymentTermCode FROM SystemPaymentTerm with (NOLOCK) WHERE PaymentTermCode = {0}", DB.SQuote(noPaymentPaymentTermCode)))
                {
                    noPaymentTermRequiredPaymentTermExisting = reader.Read();
                }
            }

            if (!noPaymentTermRequiredPaymentTermExisting)
            {
                throw new Exception(string.Format("PaymentTermCodeZeroDollarOrder AppConfig:\"{0}\" is not existing in the database!!!", noPaymentPaymentTermCode));
            }
        }



        private void ComputeFreightCharge(bool clear, decimal exchangeRate = 0)
        {
            if (!this.IsSalesOrderDetailBuilt) { this.CartFreightRate = 0; }

            if (clear)
            {
                if (IsSalesOrderDetailBuilt)
                {
                    // zero out the freight so it won't alter the computation of the coupon discount
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShippingMethodCode = String.Empty;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].Freight = Decimal.Zero;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].FreightRate = Decimal.Zero;
                }
            }
            else
            {
                // first check if the shipping method is real-time
                string shippingMethodCode = this.FirstItem().m_ShippingMethod;
                if (shippingMethodCode.IsNullOrEmptyTrimmed()) return;

                if (IsSalesOrderDetailBuilt)
                {
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShippingMethod = shippingMethodCode;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShippingMethodCode = shippingMethodCode;
                }

                string couponCode = String.Empty;
                bool hasCoupon = HasCoupon(ref couponCode);

                //check if no shippable Components, check if free shipping threshold is setup, check if coupon is free shipping

                decimal subTotal = GetCartSubTotalExcludeOversized();
                bool isFreeShipping = (!HasShippableComponents() || (InterpriseHelper.IsFreeShippingThresholdEnabled(subTotal) && InterpriseHelper.ShippingMethodCodeBelongsToFreeShippingMethodList(shippingMethodCode) && !HasOversizedItems()) ||
                                        (hasCoupon && IsCouponHasFreeShipping(couponCode)));

                if (isFreeShipping)
                {
                    if (IsSalesOrderDetailBuilt)
                    {
                        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].Freight = Decimal.Zero;
                        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].FreightRate = Decimal.Zero;
                        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].IsFreightOverwrite = true;
                    }
                }
                else
                {
                    exchangeRate = (exchangeRate == 0) ? Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(ThisCustomer.CurrencyCode) : exchangeRate;
                    var itemWithInStoreShipping = CartItems.FirstOrDefault(c => !c.InStoreWarehouseCode.IsNullOrEmptyTrimmed());
                    if (itemWithInStoreShipping != null)
                    {
                        var warehouseRow = ServiceFactory.GetInstance<IWarehouseRepository>()
                                                         .GetWarehouseRowByCode(itemWithInStoreShipping.InStoreWarehouseCode);
                        if (IsSalesOrderDetailBuilt)
                        {
                            _facadeSalesOrder.IsPickUp = true;
                            _facadeSalesOrder.AssignWarehouse(warehouseRow, _gatewaySalesOrderDataset.CustomerSalesOrderView[0]);
                            _facadeSalesOrder.RecomputeSalesOrderValues(null);
                        }
                    }

                    decimal freight = Decimal.Zero;
                    decimal freightRate = Decimal.Zero;

                    var shippingAddress = Address.Get(ThisCustomer, AddressTypes.Shipping, this.FirstItem().ShippingAddressId, this.FirstItem().GiftRegistryID);
                    var shippingMethod = GetShippingMethods(shippingAddress, shippingMethodCode, this.FirstItem().GiftRegistryID);

                    if (shippingMethod != null && shippingMethod.Count > 0)
                    {
                        for (int i = 0; i < shippingMethod.Count; i++)
                        {
                            if (i == 0 || shippingMethod[i].ForOversizedItem)
                            { freightRate += shippingMethod[i].Freight; }
                        }

                        this.CartFreightRate = freightRate;
                        //This session is only used for Portal website. Need to retain freight value for ajax consumption on portalcheckoutreview.aspx page without reloading the whole freight process.
                        ThisCustomer.ThisCustomerSession["FreightRate"] = freightRate.ToString();
                        if (IsSalesOrderDetailBuilt)
                        {
                            freight = _facadeSalesOrder.ConvertCurrency(exchangeRate, freightRate, true, ThisCustomer.CurrencyCode);
                            //Set Freight to zero for Portal website
                            if (CommonLogic.GetWebsiteType() == WebsiteType.Company)
                            {
                                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].Freight = 0;
                                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].FreightRate = 0;
                                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].IsFreightOverwrite = true;
                                _gatewaySalesOrderDataset.CustomerSalesOrderView[0]["Freight_C"] = freight;
                            }
                            else
                            {
                                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].Freight = freight;
                                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].FreightRate = freightRate;
                                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].IsFreightOverwrite = true;
                            }
                        }
                    }
                    if (IsSalesOrderDetailBuilt) { _facadeSalesOrder.ComputeFreight(); }
                }
            }
        }

        #endregion

        private void AssignKitItem(DataRow itemKitRow, DataRowView lineItemRow, Guid cartId)
        {
            var kitDataset = new ItemKitDatasetGateway();
            var kitFacade = new ItemKitFacade(kitDataset);

            string currencyCode = ThisCustomer.CurrencyCode;
            int parentQty = Convert.ToInt16(lineItemRow["QuantityOrdered"]);
            kitFacade.ExchangeRate = _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ExchangeRate;
            bool isCurrencyIncludedForInventorySelling = InterpriseHelper.IsCurrencyIncludedForInventorySelling(currencyCode);

            // Special case 2 
            //  Currency is added in Inventory Selling Currency late
            //  after the kit has been created, ideally the kit should regenerate kit pricing for this currency
            //  but for the meantime, we should handle this by looking into the home currency
            bool currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem =
            InterpriseHelper.CurrencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYet(currencyCode, itemKitRow["ItemCode"].ToString());

            bool useCustomerPricing = ServiceFactory.GetInstance<IInventoryRepository>().UseCustomerPricingForKit(itemKitRow["ItemCode"].ToString());

            string kitDetailQuery =
                string.Format(
                    "exec GetEcommerceKitCartDetail @ItemKitCode = {0}, @CurrencyCode = {1}, @LanguageCode = {2}, @CustomerCode = {3}, @CartID = {4}, @ContactCode = {5}",
                    itemKitRow["ItemCode"].ToString().ToDbQuote(),
                    (isCurrencyIncludedForInventorySelling && !currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem) ? currencyCode.ToDbQuote() : Currency.GetHomeCurrency().ToDbQuote(),
                    ThisCustomer.LocaleSetting.ToDbQuote(),
                    ThisCustomer.CustomerCode.ToDbQuote(),
                    cartId.ToString().ToDbQuote(),
                    ThisCustomer.ContactCode.ToDbQuote()
                );


            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, kitDetailQuery))
                {
                    while (reader.Read())
                    {
                        string itemCode = DB.RSField(reader, "ItemCode");
                        string unitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                        decimal quantity = Convert.ToDecimal(DB.RSFieldDecimal(reader, "Quantity"));
                        decimal totalRate = DB.RSFieldDecimal(reader, "TotalRate");

                        ItemKitDatasetGateway.KitConfiguratorRow kitConfigRow;
                        kitConfigRow = kitDataset.KitConfigurator.NewKitConfiguratorRow();

                        kitConfigRow.BeginEdit();
                        kitConfigRow._Select = true;
                        kitConfigRow.ItemName = DB.RSField(reader, "ItemName");
                        kitConfigRow.ItemType = DB.RSField(reader, "ItemType");
                        kitConfigRow.GroupType = DB.RSField(reader, "GroupType");
                        kitConfigRow.ItemCode = itemCode;
                        kitConfigRow.ItemDescription = DB.RSField(reader, "ItemDescription");
                        kitConfigRow.DisplayImage = new byte[] { };
                        kitConfigRow.GroupCode = DB.RSField(reader, "GroupCode");
                        kitConfigRow.Quantity = quantity;
                        kitConfigRow.UnitMeasureCode = unitMeasureCode;
                        kitConfigRow.UnitMeasureQty = Convert.ToInt32(DB.RSFieldDecimal(reader, "UnitMeasureQuantity"));
                        kitConfigRow.SalesPrice = DB.RSFieldDecimal(reader, "Total");

                        decimal promotionalPrice = Decimal.Zero;
                        decimal price = Decimal.Zero;
                        price = totalRate;
                        if (useCustomerPricing)
                        {
                            price = InterpriseHelper.GetKitComponentPrice(ThisCustomer.CustomerCode, itemCode, ThisCustomer.CurrencyCode, quantity, parentQty, unitMeasureCode, ref promotionalPrice);
                            if (!promotionalPrice.Equals(Decimal.Zero))
                            {
                                price = promotionalPrice;
                            }
                        }

                        if (isCurrencyIncludedForInventorySelling &&
                            currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem)
                        {
                            decimal convertedRate = kitFacade.ConvertCurrency(kitFacade.ExchangeRate, price, false, currencyCode, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.Total);
                            kitConfigRow.SalesPriceRate = convertedRate;
                        }

                        kitConfigRow.SalesPriceRate = price;
                        kitConfigRow.ExtSalesPrice = kitConfigRow.SalesPriceRate;
                        kitConfigRow.ExtSalesPriceRate = kitConfigRow.SalesPriceRate;

                        kitConfigRow.SalesPriceDifference = Decimal.Zero;
                        kitConfigRow.SalesPriceDifferenceRate = Decimal.Zero;

                        if (!kitConfigRow.IsSalesTaxCodeNull() &&
                            !string.IsNullOrEmpty(kitConfigRow.SalesTaxCode))
                        {
                            using (var con2 = DB.NewSqlConnection())
                            {
                                con.Open();
                                using (var taxReader = DB.GetRSFormat(con2, "SELECT TaxDescription FROM SystemTaxScheme with (NOLOCK) WHERE TaxCode = {0}",
                                                                                    DB.SQuote(kitConfigRow.SalesTaxCode)))
                                {
                                    if (taxReader.Read())
                                    {
                                        kitConfigRow.TaxDescription =
                                            (taxReader["TaxDescription"] != null &&
                                                taxReader["TaxDescription"] != DBNull.Value &&
                                                ((string)taxReader["TaxDescription"]).IsNullOrEmptyTrimmed()
                                                ) ? (string)taxReader["TaxDescription"] : string.Empty;
                                    }
                                }
                            }
                        }

                        kitConfigRow.EndEdit();

                        kitDataset.KitConfigurator.AddKitConfiguratorRow(kitConfigRow);
                    }
                }
            }

            kitFacade.ComputePercentage();
            kitFacade.ComputeKitItemsSalesPrice(kitFacade.ComputeTotal());

            string errMsg = string.Empty;
            _facadeSalesOrder.AssignItemKit(
                itemKitRow,
                kitFacade.KitItems,
                lineItemRow,
                Interprise.Framework.Base.Shared.Const.KIT_DISPLAY_KIT_PRICE,
                Convert.ToInt32((decimal)lineItemRow["QuantityOrdered"] / (decimal)itemKitRow.ItemArray[19]),
                kitFacade.ComputeTotal(),
                Interprise.Framework.Base.Shared.Enum.TransactionType.SalesOrder,
                ref errMsg,
                string.Empty
            );

            // **************************************************
            //  Explicit Disposal and dereferencing goes here...
            // **************************************************
            kitDataset.Dispose();
            kitFacade.Dispose();

            kitDataset = null;
            kitFacade = null;
        }

        public bool HasCoupon(ref string couponCode)
        {
            string customerCode = CommonLogic.IIF(ThisCustomer.IsRegistered, ThisCustomer.CustomerCode, ThisCustomer.CustomerID);
            return InterpriseHelper.GetCustomerCouponIfAny(customerCode, ref couponCode, ThisCustomer.IsRegistered);
        }

        public string RenderHTMLLiteral(IShoppingCartHTMLLiteralRenderer renderer)
        {
            var output = new StringBuilder();
            renderer.Render(this, ref output);
            return output.ToString();
        }

        public bool IsCouponValid(Customer ThisCustomer, string couponCode, ref string errorMessage)
        {
            bool isValid = false;
            var thisCoupon = new CouponStruct();
            string customerCode = CommonLogic.IIF(ThisCustomer.IsRegistered, ThisCustomer.CustomerCode, ThisCustomer.AnonymousCustomerCode);
            // validate the coupon
            string couponQuery =
            string.Format("SELECT * FROM eCommerceValidCouponView with (NOLOCK) WHERE CustomerCode = {0} AND CouponCode = {1} AND ShortString = {2} AND {3} BETWEEN StartingDate AND ExpirationDate",
                DB.SQuote(customerCode),
                DB.SQuote(couponCode),
                DB.SQuote(ThisCustomer.LocaleSetting),
                DB.SQuote(Localization.ToDBDateTimeString(DateTime.Today))
            );

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, couponQuery))
                {
                    if (reader.Read())
                    {
                        // initially we have a valid coupon just because it exists.
                        isValid = true;

                        thisCoupon.m_Code = DB.RSField(reader, "CouponCode");
                        thisCoupon.m_Description = DB.RSField(reader, "Description");

                        switch (DB.RSField(reader, "CouponType").ToLowerInvariant())
                        {
                            case "products":
                                thisCoupon.m_CouponType = CouponTypeEnum.ProductCoupon;
                                break;
                            case "orders":
                                thisCoupon.m_CouponType = CouponTypeEnum.OrderCoupon;
                                break;
                        }

                        switch (DB.RSField(reader, "DiscountType").ToLowerInvariant())
                        {
                            case "percent":
                                thisCoupon.m_DiscountType = CouponDiscountTypeEnum.PercentDiscount;
                                break;
                            case "amount":
                                thisCoupon.m_DiscountType = CouponDiscountTypeEnum.AmountDiscount;
                                break;
                        }

                        thisCoupon.m_DiscountAmount = DB.RSFieldDecimal(reader, "DiscountAmount");
                        thisCoupon.m_DiscountPercent = ((Decimal)DB.RSFieldSingle(reader, "DiscountPercent"));

                        thisCoupon.m_IncludesFreeShipping = DB.RSFieldBool(reader, "DiscountIncludesFreeShipping");
                        thisCoupon.m_IncludesAllCustomer = DB.RSFieldBool(reader, "IncludeAllCustomer");
                        thisCoupon.m_IncludesAllProduct = DB.RSFieldBool(reader, "IncludeAllProduct");

                        switch (DB.RSFieldInt(reader, "CouponUsage"))
                        {
                            case 1:
                                thisCoupon.m_ExpiresOnFirstUseByAnyCustomer = true;
                                thisCoupon.m_ExpiresAfterOneUsageByEachCustomer = false;
                                thisCoupon.m_ExpiresAfterNUses = 0;
                                break;
                            case 2:
                                thisCoupon.m_ExpiresOnFirstUseByAnyCustomer = false;
                                thisCoupon.m_ExpiresAfterOneUsageByEachCustomer = true;
                                thisCoupon.m_ExpiresAfterNUses = 0;
                                break;
                            case 3:
                                thisCoupon.m_ExpiresOnFirstUseByAnyCustomer = false;
                                thisCoupon.m_ExpiresAfterOneUsageByEachCustomer = false;
                                thisCoupon.m_ExpiresAfterNUses = DB.RSFieldInt(reader, "ExpiresAfterNUses");
                                break;
                        }

                        thisCoupon.m_RequiresMinimumOrderAmount = DB.RSFieldDecimal(reader, "RequiresMinimumOrderAmount");
                        thisCoupon.m_ValidForThisEntity = DB.RSField(reader, "ItemCategory");
                        thisCoupon.m_NumUses = DB.RSFieldInt(reader, "TotalUsageCount");
                        thisCoupon.m_CouponComputation = DB.RSField(reader, "CouponComputation");
                        // now let's (really) validate this coupon 
                        errorMessage = ValidateNewCoupon(thisCoupon, ref isValid);
                    }
                    else
                    {
                        // coupon is invalid or expired
                        errorMessage = AppLogic.GetString("shoppingcart.cs.35");
                    }
                }
            }

            return isValid;
        }

        public String ValidateNewCoupon(CouponStruct coupon, ref bool isValid)
        {
            string status = string.Empty;
            var ThisCustomer = Customer.Current;
            string customerCode = CommonLogic.IIF(ThisCustomer.IsRegistered, ThisCustomer.CustomerCode, ThisCustomer.AnonymousCustomerCode);
            bool withVat = ServiceFactory.GetInstance<IAppConfigService>().VATIsEnabled &&
                           ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive;

            isValid = true;
            decimal cartSubTotal = !withVat ? CartItems.Sum(item => (item.Price)) : CartItems.Sum(item => (item.Price - item.TaxRate));

            if (coupon.m_ExpiresOnFirstUseByAnyCustomer && Customer.AnyCustomerHasUsedCoupon(coupon.m_Code))
            {
                status = AppLogic.GetString("shoppingcart.cs.14");
                isValid = false;
            }

            if (coupon.m_ExpiresAfterOneUsageByEachCustomer && Customer.HasUsedCoupon(customerCode, coupon.m_Code))
            {
                status = AppLogic.GetString("shoppingcart.cs.15");
                isValid = false;
            }

            if (coupon.m_ExpiresAfterNUses > 0 && coupon.m_NumUses > coupon.m_ExpiresAfterNUses)
            {
                status = AppLogic.GetString("shoppingcart.cs.16");
                isValid = false;
            }

            if (coupon.m_RequiresMinimumOrderAmount > System.Decimal.Zero && cartSubTotal < coupon.m_RequiresMinimumOrderAmount)
            {
                status = String.Format(AppLogic.GetString("shoppingcart.cs.17"), coupon.m_RequiresMinimumOrderAmount.ToCustomerCurrency());
                isValid = false;
            }

            if (!coupon.m_IncludesAllCustomer && !Customer.CustomerCanAvailOfThisCoupon(customerCode, coupon.m_Code))
            {
                status = AppLogic.GetString("shoppingcart.cs.18");
                isValid = false;
            }

            try
            {
                if (coupon.m_CouponType == CouponTypeEnum.ProductCoupon)
                {
                    int validItem = 0;
                    foreach (CartItem item in CartItems)
                    {
                        if (CouponIsValidForThisProduct(item.ItemCode, coupon.m_Code))
                        {
                            validItem += 1;
                        }
                    }

                    if (validItem == 0)
                    {
                        status = AppLogic.GetString("shoppingcart.cs.19");
                        isValid = false;
                    }
                }

            }
            catch
            {
                status = AppLogic.GetString("shoppingcart.cs.20");
                isValid = false;
            }

            return status;
        }

        Dictionary<String, KeyValuePair<String, Boolean>> cartItemsIncludedInCoupon = new Dictionary<string, KeyValuePair<String, Boolean>>();
        public bool CouponIsValidForThisProduct(String itemCode, String couponCode)
        {
            bool validForThisProduct = false;

            if (cartItemsIncludedInCoupon.Count == 0)
            {
                using (var con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (var rs = DB.GetRSFormat(con, "exec eCommerceCanCartItemsAvailOfCouponDiscount @CustomerCode={0}, @CouponCode={1}", ThisCustomer.CustomerCode.ToDbQuote(), couponCode.ToDbDateQuote()))
                    {
                        while (rs.Read())
                        {
                            itemCode = rs.ToRSField("ItemCode") ;
                            validForThisProduct = rs.ToRSFieldBool("CanAvail");
                            if (!cartItemsIncludedInCoupon.ContainsKey(itemCode))
                            {
                                cartItemsIncludedInCoupon.Add(itemCode, new KeyValuePair<String, Boolean>(couponCode, validForThisProduct));
                            }
                        }
                    }
                }
            }
            
            if (cartItemsIncludedInCoupon.ContainsKey(itemCode)){
                KeyValuePair<String, Boolean> keyValue = cartItemsIncludedInCoupon[itemCode];
                if (keyValue.Key.ToUpperInvariant() == couponCode.ToUpperInvariant())
                {
                    return keyValue.Value;
                }
            }

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var rs = DB.GetRSFormat(con, "exec eCommerceCanItemAvailOfCouponDiscount @ItemCode={0}, @CouponCode={1}", DB.SQuote(itemCode), DB.SQuote(couponCode)))
                {
                    if (rs.Read())
                    {
                        validForThisProduct = DB.RSFieldBool(rs, "CanAvail");
                        cartItemsIncludedInCoupon.Add(itemCode, new KeyValuePair<String, Boolean>(couponCode, validForThisProduct)); 
                    }
                }
            }
            return validForThisProduct;
        }

        public bool CouponIncludesFreeShipping(string couponCode = "")
        {
            bool includesFreeShipping = false;
            string customerCode = CommonLogic.IIF(ThisCustomer.IsRegistered, ThisCustomer.CustomerCode, ThisCustomer.AnonymousCustomerCode);

            if (!couponCode.IsNullOrEmptyTrimmed())
            {
                string errorMessage = string.Empty;
                if (IsCouponValid(ThisCustomer, couponCode, ref errorMessage))
                {
                    string couponQuery =
                    string.Format("SELECT * FROM eCommerceValidCouponView with (NOLOCK) WHERE CustomerCode = {0} AND CouponCode = {1} AND ShortString = {2} AND DiscountIncludesFreeShipping = 1 AND {3} BETWEEN StartingDate AND ExpirationDate",
                    DB.SQuote(customerCode),
                    DB.SQuote(couponCode),
                    DB.SQuote(ThisCustomer.LocaleSetting),
                    DB.SQuote(Localization.ToDBDateTimeString(DateTime.Today))
                    );

                    using (var con = DB.NewSqlConnection())
                    {
                        con.Open();
                        using (var reader = DB.GetRSFormat(con, couponQuery))
                        {
                            includesFreeShipping = reader.Read();
                            this.ShippingIsFree = true;
                        }
                    }
                }
            }

            return includesFreeShipping;
        }

        public void ApplyCoupon(string couponCode)
        {
            //if (ThisCustomer.IsNotRegistered) throw new InvalidOperationException("Coupon not applicable for anonymous customers!");
            bool clearCouponDiscount = !ThisCustomer.ThisCustomerSession[DomainConstants.CLEAR_COUPON_DISCOUNT].IsNullOrEmptyTrimmed();
            bool clearOtherPaymentOptions = !ThisCustomer.ThisCustomerSession[DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS].IsNullOrEmptyTrimmed();
            string selectedPaymentTerm = ThisCustomer.PaymentTermCode.ToUpperInvariant();

            if (clearCouponDiscount || (clearOtherPaymentOptions && (selectedPaymentTerm.Equals("REQUEST QUOTE") || selectedPaymentTerm.Equals("PURCHASE ORDER"))))
            {
                InterpriseHelper.ClearCustomerCoupon(ThisCustomer.CustomerCode, ThisCustomer.IsRegistered);
                ThisCustomer.ThisCustomerSession.ClearVal(DomainConstants.CLEAR_COUPON_DISCOUNT);
                return;
            }

            string errorMessage = string.Empty;
            if (!IsCouponValid(ThisCustomer, couponCode, ref errorMessage))
            {
                ServiceFactory.GetInstance<INavigationService>()
                              .NavigateToShoppingCartRestLinkBackWithErroMessage(errorMessage);
            }

            // NOTE :
            //  Since the EcommerceShoppingCart table only stores entries for inventory items
            //  we'll just use the Customer table as the header and store the coupon info
            string customerCode = CommonLogic.IIF(ThisCustomer.IsRegistered, ThisCustomer.CustomerCode, ThisCustomer.CustomerID);
            string saveCustomerCouponStatement;
            if (ThisCustomer.IsRegistered)
            {
                saveCustomerCouponStatement = string.Format("UPDATE Customer SET CouponCode = {0} WHERE CustomerCode = {1}", DB.SQuote(couponCode), DB.SQuote(customerCode));
            }
            else
            {
                saveCustomerCouponStatement = string.Format("UPDATE EcommerceCustomer SET CouponCode = {0} WHERE CustomerCode = {1}", DB.SQuote(couponCode), DB.SQuote(customerCode));
            }
            DB.ExecuteSQL(saveCustomerCouponStatement);

            if (!this.IsSalesOrderDetailBuilt)
            {
                this.GetCartCouponSetting();
                //if (this.Coupon.m_CouponType == CouponTypeEnum.OrderCoupon) { return; }
                foreach (var cartitem in this.CartItems)
                {
                    if (cartitem.DiscountAmountAlreadyComputed) continue;
                    if (cartitem.Price == decimal.Zero) continue;

                    if (!this.Coupon.m_IncludesAllProduct)
                    {
                        if (!CouponIsValidForThisProduct(cartitem.ItemCode, this.Coupon.m_Code)) { continue; }
                    }
                    cartitem.CouponDiscount = this.GetCartItemCouponDiscount(cartitem);
                    cartitem.DiscountAmountAlreadyComputed = true;
                }
                return;
            }

            if (this.IsSalesOrderDetailBuilt)
            {
                // apply...
                var couponDataset = new DataSet();
                var facadeList = new ListControlFacade();
                var baseCouponDataset = new BaseDataset();

                //deadlock
                int nooftries = 0;
                while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
                {
                    try
                    {
                        if (ThisCustomer.IsRegistered)
                        {
                            facadeList.ReadSearchResultsData("CustomerCouponView",
                                string.Format("CustomerCode = {0} AND CouponCode = {1}",
                                    DB.SQuote(customerCode),
                                    DB.SQuote(couponCode)
                                ),
                                1,
                                false,
                                string.Empty,
                                ref baseCouponDataset,
                                true);
                        }
                        break;
                    }
                    catch (SqlException ex)
                    {
                        if (ex.ErrorCode == 1205)
                        {
                            nooftries += 1;
                        }
                        else
                        {
                            throw;
                        }
                    }
                }

                if (ThisCustomer.IsRegistered)
                {
                    if (baseCouponDataset.Tables["CustomerCouponView"] != null &&
                        baseCouponDataset.Tables["CustomerCouponView"].Rows.Count >= 1)
                    {
                        DataRow couponRow = baseCouponDataset.Tables["CustomerCouponView"].Rows[0];
                        _facadeSalesOrder.AssignCoupon(couponRow, _gatewaySalesOrderDataset.CustomerSalesOrderView[0]);
                    }
                }
                else
                {
                    string getAnonCouponRow = string.Format("exec EcommerceGetAnonCustomerCoupon @CustomerID = {0}, @CouponCode = {1}, @LanguageCode = {2}",
                                              DB.SQuote(customerCode), DB.SQuote(couponCode), DB.SQuote(ThisCustomer.LanguageCode));
                    DataTable dt = DB.GetDS(getAnonCouponRow, false).Tables[0];
                    if (dt.Rows.Count >= 1)
                    {
                        _facadeSalesOrder.AssignCoupon(dt.Rows[0], _gatewaySalesOrderDataset.CustomerSalesOrderView[0]);
                    }
                }


                couponDataset.Dispose();
                baseCouponDataset.Dispose();
                facadeList.Dispose();

                couponDataset = null;
                baseCouponDataset = null;
                facadeList = null;
            }
        }

        public void SetCartShippingMethod(string shippingMethodCode)
        {
            SetCartShippingMethod(shippingMethodCode, string.Empty, Guid.Empty);
        }

        public void SetCartShippingMethod(string shippingMethodCode, string shippingAddress)
        {
            SetCartShippingMethod(shippingMethodCode, shippingAddress, Guid.Empty);
        }

        public void SetCartShippingMethod(string shippingMethodCode, string shippingaddress, Guid rateID)
        {
            DB.ExecuteSQL(
                String.Format("exec SetEcommerceShoppingCartShippingMethod @CustomerCode = {0}, @ShippingMethodCode = {1}, @ShippingAddressID = {2}, @ContactCode = {3}, @RealTimeRateID = {4}",
                ThisCustomer.CustomerCode.ToDbQuote(), shippingMethodCode.ToDbQuote(), shippingaddress.ToDbQuote(), ThisCustomer.ContactCode.ToDbQuote(),
                (rateID != null && rateID != Guid.Empty) ? rateID.ToString().ToDbQuote() : "null")
            );

            foreach (CartItem item in CartItems)
            {
                item.m_ShippingMethod = shippingMethodCode;
            }
        }

        public override void SetItemQuantity(int cartRecordID, decimal Quantity)
        {
            base.SetItemQuantity(cartRecordID, Quantity);
        }

        public bool IsSalesOrderDetailBuilt
        {
            get { return _gatewaySalesOrderDataset != null && _facadeSalesOrder != null; }
        }

        public ShippingMethodDTOCollection GetShippingMethods(Address preferredShippingAddress)
        {
            return GetShippingMethods(preferredShippingAddress, string.Empty);
        }

        public ShippingMethodDTOCollection GetShippingMethods(Address preferredShippingAddress, Guid? giftRegistryId = null)
        {
            return GetShippingMethods(preferredShippingAddress, string.Empty, giftRegistryId);
        }

        public bool IsCartItemsOversizedDownloadsAndService()
        {
            int oversizeItems = CartItems.Where(item => item.IsOverSized && (!item.IsService && !item.IsDownload)).Count();
            int test = CartItems.Where(item => item.IsService || item.IsDownload).Count();

            return ((CartItems.Count() - (oversizeItems + test) == 0));
        }

        public ShippingMethodDTOCollection GetShippingMethods(Address preferredShippingAddress, string shippingMethodCode, Guid? giftRegistryId = null)
        {
            var availableShippingMethods = new ShippingMethodDTOCollection();

            if (IsEmpty()) return availableShippingMethods;

            var shipments = GetShipmentComposition(preferredShippingAddress, false);

            string forCustomer = ThisCustomer.CustomerCode;
            if (ThisCustomer.IsNotRegistered)
            {
                forCustomer = ThisCustomer.AnonymousCustomerCode;
            }

            decimal getExchangeRate = Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(ThisCustomer.CurrencyCode);
            string contactCode = (ThisCustomer.IsNotRegistered) ? String.Empty : ThisCustomer.ContactCode;
            decimal cartSubTotal = GetCartSubTotalExcludeOversized();

            if (shipments.Count > Decimal.Zero)
            {
                //Load the giftregistry owner info if giftregistry id is available.
                if (giftRegistryId.HasValue)
                {
                    var registryCustomerInfo = CustomerDA.GetCustomerInfoByRegistryID(giftRegistryId);
                    forCustomer = registryCustomerInfo.CustomerCode;
                    contactCode = registryCustomerInfo.ContactCode;
                }

                string couponCode = (this.IsSalesOrderDetailBuilt) ? _gatewaySalesOrderDataset.CustomerSalesOrderView[0]
                    [_gatewaySalesOrderDataset.CustomerSalesOrderView.CouponCodeColumn.ColumnName].ToString() : String.Empty;

                if (couponCode.IsNullOrEmptyTrimmed())
                {
                    HasCoupon(ref couponCode);
                }


                bool isCouponFreeShipping = IsCouponHasFreeShipping(couponCode);

                availableShippingMethods = ServiceFactory.GetInstance<IShippingService>()
                                                         .GetCustomerShippingMethods(
                                                            forCustomer,
                                                            isCouponFreeShipping,
                                                            contactCode,
                                                            shippingMethodCode,
                                                            preferredShippingAddress,
                                                            InterpriseHelper.IsFreeShippingThresholdEnabled(cartSubTotal)
                                                         );
            }

            var rateGateway = new Interprise.Framework.Base.DatasetGateway.Shipping.CarrierDatasetGateway();
            var rateFacade = new Interprise.Facade.Base.Shipping.RateFacade(rateGateway);

            string[] warehouseInfo = null;
            string warehouseCode = String.Empty, isoCode = String.Empty, state = String.Empty, postalCode = String.Empty;
            bool isResidential = true;

            contactCode = contactCode.IsNullOrEmptyTrimmed() ? "NULL" : contactCode.ToDbQuote();

            if (ThisCustomer.IsRegistered)
            {
                warehouseInfo = rateFacade.GetRow(new string[] { "WarehouseCode", "ISOCode", "State", "PostalCode", "AddressType" },
                    "InventoryWarehouse IW (NOLOCK) INNER JOIN SystemCountry SC (NOLOCK) ON IW.Country = SC.CountryCode",
                    "IW.WarehouseCode = (SELECT TOP 1 WarehouseCode FROM CustomerShipTo WHERE ShipToCode IN (SELECT TOP 1 DefaultShippingCode FROM CRMContact WHERE EntityCode = "
                    + DB.SQuote(forCustomer) + " AND ContactCode = " + ((ThisCustomer.IsRegistered) ? contactCode : ThisCustomer.ContactCode.ToDbQuote()) + "))", false);
            }
            else
            {
                warehouseInfo = rateFacade.GetRow(new string[] { "WarehouseCode", "ISOCode", "State", "PostalCode", "AddressType" },
                    "InventoryWarehouse IW (NOLOCK) INNER JOIN SystemCountry SC (NOLOCK) ON IW.Country = SC.CountryCode",
                    "IW.WarehouseCode = (SELECT WarehouseCode FROM CustomerShipToClassTemplateDetailView WHERE ClassCode = (SELECT DefaultRetailCustomerShipToClassTemplate FROM SystemCountry" +
                    " WHERE CountryCode = " + preferredShippingAddress.Country.ToDbQuote() + "))", false);
            }

            if (warehouseInfo != null)
            {
                warehouseCode = warehouseInfo[0];
                isoCode = warehouseInfo[1];
                state = warehouseInfo[2];
                postalCode = warehouseInfo[3];
                isResidential = (warehouseInfo[4] == "Residential");
            }

            if (!shippingMethodCode.IsNullOrEmptyTrimmed() && availableShippingMethods.Count > Decimal.Zero)
            {
                if (availableShippingMethods[0].FreightCalculation == 1 || availableShippingMethods[0].FreightCalculation == 2)
                {
                    decimal freight = Decimal.Zero;
                    using (var con = DB.NewSqlConnection())
                    {
                        con.Open();

                        using (var reader = DB.GetRSFormat(con, "SELECT Rate FROM EcommerceRealTimeRate with (NOLOCK) WHERE ContactCode = {0} AND ShippingMethodCode = {1} AND EcommerceRealTimeRateGUID = {2}",
                                                            ThisCustomer.ContactCode.ToDbQuote(), shippingMethodCode.ToDbQuote(), FirstItem().RealTimeRateId.ToString().ToDbQuote()))
                        {
                            if (reader.Read())
                            {
                                freight = reader.ToRSFieldDecimal("Rate");
                            }
                        }
                    }
                    availableShippingMethods[0].Freight = freight;
                }
                else
                {
                    CalculateShippingMethodRates(preferredShippingAddress, availableShippingMethods, shipments, rateGateway, rateFacade, warehouseCode, isoCode, state, postalCode, isResidential, cartSubTotal);
                }
            }
            else
            {
                if (!AppLogic.AppConfigBool("ShippingRatesOnDemand"))
                {
                    // regular packages and prepacked packages
                    CalculateShippingMethodRates(preferredShippingAddress, availableShippingMethods, shipments, rateGateway, rateFacade, warehouseCode, isoCode, state, postalCode, isResidential, cartSubTotal);
                    if (!AppLogic.AppConfigBool("ShowShippingMethodError") && !AppLogic.AppConfig("ShowShippingMethodError").IsNullOrEmptyTrimmed())
                    {
                        var lstShippingMethodWithoutError = availableShippingMethods.Where(item => !item.IsError);
                        var newListOfAvailableShippingMethods = new ShippingMethodDTOCollection();
                        newListOfAvailableShippingMethods.AddRange(lstShippingMethodWithoutError);
                        availableShippingMethods = newListOfAvailableShippingMethods;
                    }
                }
            }

            // oversized packages
            var overSizedShipments = GetShipmentComposition(preferredShippingAddress, true);
            if (overSizedShipments.Count > Decimal.Zero)
            {
                foreach (RTShipping.Packages Shipment in overSizedShipments)
                {
                    foreach (RTShipping.Package p in Shipment)
                    {
                        if (p.OverSizedShippingMethodCode.IsNullOrEmptyTrimmed()) continue;

                        var packageShippingMethod = GetShippingMethod(p.OverSizedShippingMethodCode, warehouseInfo[0]);
                        if (packageShippingMethod == null) continue;

                        // add dummy shipping method
                        if (availableShippingMethods.Count == Decimal.Zero)
                        {
                            using (var con = DB.NewSqlConnection())
                            {
                                con.Open();
                                using (var reader = DB.GetRSFormat(con, String.Format("SELECT ShippingMethod,ShippingMethodDescription FROM CustomerShipTo CST with (NOLOCK) INNER JOIN SystemShippingMethod SSM with (NOLOCK) ON CST.ShippingMethod = SSM.ShippingMethodCode WHERE ShipToCode = {0}", ThisCustomer.PrimaryShippingAddressID.ToDbQuote())))
                                {
                                    if (reader.Read())
                                    {
                                        var shippingMethod = new ShippingMethodDTO(reader.ToRSField("ShippingMethod"), reader.ToRSField("ShippingMethodDescription"));
                                        shippingMethod.IsDefault = true;
                                        shippingMethod.IsDummyShippingMethod = true;
                                        availableShippingMethods.Add(shippingMethod);
                                    }
                                }
                            }
                        }

                        packageShippingMethod.ForOversizedItem = true;
                        packageShippingMethod.OversizedItemName = p.OverSizedItemName;
                        packageShippingMethod.OverSizedItemCode = p.OverSizedItemCode;

                        var packageShippingMethodCollection = new ShippingMethodDTOCollection();
                        packageShippingMethodCollection.Add(packageShippingMethod);

                        var tempShipment = new RTShipping.Packages();
                        tempShipment = ShipmentDestination(tempShipment, preferredShippingAddress);

                        var tempShipments = new RTShipping.Shipments();
                        tempShipment.AddPackage(p);
                        tempShipments.AddPackages(tempShipment);

                        CalculateShippingMethodRates(preferredShippingAddress, packageShippingMethodCollection, tempShipments, rateGateway, rateFacade, warehouseCode, isoCode, state, postalCode, isResidential, getExchangeRate);

                        availableShippingMethods.Add(packageShippingMethod);
                    }
                }
            }

            if (rateFacade != null)
            {
                rateFacade.Dispose();
                rateFacade = null;
            }
            if (rateGateway != null)
            {
                rateGateway.Dispose();
                rateGateway = null;
            }

            return availableShippingMethods;
        }

        public ShippingMethodDTOCollection GetShippingMethodsForShippingCalc(Address preferredShippingAddress, string shippingMethodCode)
        {
            var availableShippingMethods = new ShippingMethodDTOCollection();

            if (IsEmpty()) { return availableShippingMethods; }

            var shipments = GetShipmentComposition(preferredShippingAddress, false);

            string forCustomer = ThisCustomer.CustomerCode;
            if (ThisCustomer.IsNotRegistered)
            {
                forCustomer = ThisCustomer.AnonymousCustomerCode;
            }

            decimal getExchangeRate = _facadeSalesOrder.GetExchangerate(ThisCustomer.CurrencyCode);

            if (shipments.Count > Decimal.Zero)
            {
                using (var con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (var reader = DB.GetRSFormat(con, "exec GetEcommerceShippingMethodsForCalculator @BusinessType = {0}, @CountryCode = {1}, @PostalCode = {2}, @AddressType = {3}",
                                                        ThisCustomer.DefaultPrice.ToDbQuote(), preferredShippingAddress.Country.ToDbQuote(), preferredShippingAddress.PostalCode.ToDbQuote(), preferredShippingAddress.ResidenceType.ToString().ToDbQuote()))
                    {
                        while (reader.Read())
                        {
                            var shippingMethod = new ShippingMethodDTO(reader.ToRSField("ShippingMethodCode"), reader.ToRSField("ShippingMethodDescription"));
                            if (shippingMethod.Code != AppLogic.AppConfig("ShippingMethodCodeZeroDollarOrder") && shippingMethod.Code == "Next Day Dispatch")
                            {
                                shippingMethod.IsDefault = false;
                                shippingMethod.CarrierCode = reader.ToRSField("CarrierCode");
                                shippingMethod.CarrierDescription = reader.ToRSField("CarrierDescription");
                                shippingMethod.PackagingType = reader.ToRSField("PackagingType");
                                shippingMethod.ServiceType = reader.ToRSField("ServiceType");
                                shippingMethod.FreightCalculation = reader.ToRSFieldInt("FreightCalculation");
                                shippingMethod.ChargeType = reader.ToRSFieldInt("ChargeType");
                                shippingMethod.MiscAmount = reader.ToRSFieldDecimal("MiscAmount");
                                shippingMethod.MiscAmount = _facadeSalesOrder.ConvertCurrency(_facadeSalesOrder.GetExchangerate(ThisCustomer.CurrencyCode), shippingMethod.MiscAmount);
                                shippingMethod.Length = reader.ToRSFieldInt("Length");
                                shippingMethod.Width = reader.ToRSFieldInt("Width");
                                shippingMethod.Height = reader.ToRSFieldInt("Height");
                                shippingMethod.WeightThreshold = reader.ToRSFieldDecimal("WeightThreshold");
                                availableShippingMethods.Add(shippingMethod);
                            }
                        }
                    }
                }

            }

            var rateGatewayShippingCalculator = new Interprise.Framework.Base.DatasetGateway.Shipping.CarrierDatasetGateway();
            var rateFacadeShippingCalculator = new Interprise.Facade.Base.Shipping.RateFacade(rateGatewayShippingCalculator);

            string[] warehouseInfo = null;
            string warehouseCode = String.Empty, isoCode = String.Empty, state = String.Empty, postalCode = String.Empty;
            bool isResidential = true;

            warehouseInfo = rateFacadeShippingCalculator.GetRow(new string[] { "WarehouseCode", "ISOCode", "State", "PostalCode", "AddressType" },
                "InventoryWarehouse IW (NOLOCK) INNER JOIN SystemCountry SC (NOLOCK) ON IW.Country = SC.CountryCode",
                "IW.WarehouseCode = (SELECT TOP 1 WarehouseCode FROM CustomerShipTo WHERE ShipToCode IN (SELECT TOP 1 DefaultShippingCode FROM CRMContact WHERE EntityCode = "
                + forCustomer.ToDbQuote() + " AND ContactCode = " + ThisCustomer.ContactCode.ToDbQuote() + "))", false);

            if (warehouseInfo != null)
            {
                warehouseCode = warehouseInfo[0];
                isoCode = warehouseInfo[1];
                state = warehouseInfo[2];
                postalCode = warehouseInfo[3];
                isResidential = (warehouseInfo[4] == "Residential");
            }

            decimal cartSubTotal = GetCartSubTotalExcludeOversized();

            if (!shippingMethodCode.IsNullOrEmptyTrimmed() && availableShippingMethods.Count > Decimal.Zero)
            {
                if (availableShippingMethods[0].FreightCalculation == 1 || availableShippingMethods[0].FreightCalculation == 2)
                {
                    decimal freight = Decimal.Zero;
                    using (var con = DB.NewSqlConnection())
                    {
                        con.Open();

                        using (var reader = DB.GetRSFormat(con, "SELECT Rate FROM EcommerceRealTimeRate with (NOLOCK) WHERE ContactCode = {0} AND ShippingMethodCode = {1} AND EcommerceRealTimeRateGUID = {2}",
                                                            ThisCustomer.ContactCode.ToDbQuote(), shippingMethodCode.ToDbQuote(), FirstItem().RealTimeRateId.ToString().ToDbQuote()))
                        {
                            if (reader.Read())
                            {
                                freight = reader.ToRSFieldDecimal("Rate");
                            }
                        }
                    }
                    availableShippingMethods[0].Freight = freight;
                }
                else
                {
                    CalculateShippingMethodRates(preferredShippingAddress, availableShippingMethods, shipments, rateGatewayShippingCalculator, rateFacadeShippingCalculator, warehouseCode, isoCode, state, postalCode, isResidential, cartSubTotal);
                }
            }
            else
            {
                // regular packages and prepacked packages
                CalculateShippingMethodRates(preferredShippingAddress, availableShippingMethods, shipments, rateGatewayShippingCalculator, rateFacadeShippingCalculator, warehouseCode, isoCode, state, postalCode, isResidential, cartSubTotal);
                if (!AppLogic.AppConfigBool("ShowShippingMethodError") && !AppLogic.AppConfig("ShowShippingMethodError").IsNullOrEmptyTrimmed())
                {
                    var lstShippingMethodWithoutError = availableShippingMethods.Where(item => !item.IsError);
                    var newListOfAvailableShippingMethods = new ShippingMethodDTOCollection();
                    newListOfAvailableShippingMethods.AddRange(lstShippingMethodWithoutError);
                    availableShippingMethods = newListOfAvailableShippingMethods;
                }
            }

            if (rateFacadeShippingCalculator != null)
            {
                rateFacadeShippingCalculator.Dispose();
                rateFacadeShippingCalculator = null;
            }
            if (rateGatewayShippingCalculator != null)
            {
                rateGatewayShippingCalculator.Dispose();
                rateGatewayShippingCalculator = null;
            }

            return availableShippingMethods;

        }

        private ShippingMethodDTO GetShippingMethod(string shippingMethodCode, string warehouseCode)
        {
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, string.Format("SELECT AA.ShippingMethodCode,AA.ShippingMethodDescription,BB.CarrierCode,BB.CarrierDescription,BB.PackagingType,BB.ServiceType,BB.FreightCalculation,BB.ChargeType,BB.MiscAmount, AA.FreightChargeType " +
                                                                            "FROM SystemShippingMethod AA with (NOLOCK) LEFT JOIN ShipmentShippingMethodDefault BB with (NOLOCK) ON AA.ShippingMethodCode = BB.ShippingMethodCode AND BB.WarehouseCode = {1} WHERE AA.ShippingMethodCode = {0}", DB.SQuote(shippingMethodCode), DB.SQuote(warehouseCode))))
                {
                    if (reader.Read())
                    {
                        var shippingMethod = new ShippingMethodDTO(DB.RSField(reader, "ShippingMethodCode"), DB.RSField(reader, "ShippingMethodDescription"));
                        if (shippingMethod.Code != AppLogic.AppConfig("ShippingMethodCodeZeroDollarOrder"))
                        {
                            shippingMethod.IsDefault = false;
                            shippingMethod.CarrierCode = DB.RSField(reader, "CarrierCode");
                            shippingMethod.CarrierDescription = DB.RSField(reader, "CarrierDescription");
                            shippingMethod.PackagingType = DB.RSField(reader, "PackagingType");
                            shippingMethod.ServiceType = DB.RSField(reader, "ServiceType");
                            shippingMethod.FreightCalculation = DB.RSFieldInt(reader, "FreightCalculation");
                            shippingMethod.ChargeType = DB.RSFieldInt(reader, "ChargeType");
                            shippingMethod.MiscAmount = DB.RSFieldDecimal(reader, "MiscAmount");
                            shippingMethod.FreightChargeType = reader.ToRSField("FreightChargeType");
                            shippingMethod.MiscAmount = (this.IsSalesOrderDetailBuilt) ?
                                                        _facadeSalesOrder.ConvertCurrency(_facadeSalesOrder.GetExchangerate(ThisCustomer.CurrencyCode), shippingMethod.MiscAmount) :
                                                        Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(ThisCustomer.CurrencyCode), shippingMethod.MiscAmount);

                        }
                        return shippingMethod;
                    }
                }
            }
            return null;
        }



        private void CalculateShippingMethodRates(Address preferredShippingAddress, ShippingMethodDTOCollection availableShippingMethods, RTShipping.Shipments shipments, Interprise.Framework.Base.DatasetGateway.Shipping.CarrierDatasetGateway rateGateway,
                Interprise.Facade.Base.Shipping.RateFacade rateFacade, string WarehouseCode, string ISOCode, string State, string PostalCode, bool IsResidential, decimal cartSubTotal = 0, decimal exchangeRate = 0)
        {
            var processedShippingMethods = new ShippingMethodDTOCollection();
            foreach (var shippingMethod in availableShippingMethods)
            {
                if (!shippingMethod.ForOversizedItem && InterpriseHelper.IsFreeShippingThresholdEnabled(cartSubTotal) && InterpriseHelper.ShippingMethodCodeBelongsToFreeShippingMethodList(shippingMethod.Code))
                {
                    shippingMethod.Freight = Decimal.Zero;
                    shippingMethod.FreightDisplay = AppLogic.GetString("shoppingcart.aspx.13");
                    shippingMethod.FreightCurrencyCode = String.Empty;
                }
                else
                {
                    decimal freight = Decimal.Zero;

                    if (shippingMethod.FreightCalculation == 1 || shippingMethod.FreightCalculation == 2)
                    {
                        //// check other shippingMethods with the same carrier AND service AND packaging
                        //var similarShippingMethod = GetSimilarShippingMethod(processedShippingMethods, shippingMethod);
                        //if (similarShippingMethod != null)
                        //{
                        //    freight = similarShippingMethod.ReturnedRate;
                        //}
                        //else
                        //{
                            if (shippingMethod.FreightCalculation == 2)
                            {
                                decimal flatCharge = ComputeFreightCharge(shippingMethod.Code, 0, shipments, exchangeRate);
                                freight = GetShippingMethodCarrierRate(preferredShippingAddress, shippingMethod, shipments, rateFacade, rateGateway, WarehouseCode, ISOCode, State, PostalCode, IsResidential, flatCharge);
                            }
                            else
                            {
                                freight = GetShippingMethodCarrierRate(preferredShippingAddress, shippingMethod, shipments, rateFacade, rateGateway, WarehouseCode, ISOCode, State, PostalCode, IsResidential);
                            }

                        //}
                    }
                    else
                    {
                        freight = ComputeFreightCharge(shippingMethod.Code, shippingMethod.MiscAmount, shipments, exchangeRate);
                    }

                    decimal shippingExtraFee = AppLogic.AppConfigNativeDecimal("ShippingHandlingExtraFee");
                    if (shippingExtraFee > Decimal.Zero && !freight.Equals(Decimal.Zero))
                    {
                        freight += shippingExtraFee;
                    }

                    shippingMethod.Freight = freight;
                    shippingMethod.FreightDisplay = freight.ToCustomerCurrency();
                    shippingMethod.FreightCurrencyCode = ThisCustomer.CurrencyCode;
                }
                processedShippingMethods.Add(shippingMethod);
            }
        }

        public void CalculateShippingMethodRatesOnDemand(ShippingMethodDTO shippingMethodInfo, Address preferredShippingAddress, Guid? giftRegistryId = null)
        {
            decimal exchangeRate = _facadeSalesOrder.GetExchangerate(ThisCustomer.CurrencyCode);
            decimal freight = Decimal.Zero;

            string forCustomer = ThisCustomer.CustomerCode;
            if (ThisCustomer.IsNotRegistered)
            {
                forCustomer = ThisCustomer.AnonymousCustomerCode;
            }

            string contactCode = (ThisCustomer.IsNotRegistered) ? "NULL" : ThisCustomer.ContactCode.ToDbQuote();
            var shipments = GetShipmentComposition(preferredShippingAddress, false);

            if (giftRegistryId.HasValue)
            {
                var registryCustomerInfo = CustomerDA.GetCustomerInfoByRegistryID(giftRegistryId);
                forCustomer = registryCustomerInfo.CustomerCode;
                contactCode = registryCustomerInfo.ContactCode.ToDbQuote();
            }

            if ((shippingMethodInfo.FreightCalculation == 1 || shippingMethodInfo.FreightCalculation == 2))
            {
                var rateGateway = new Interprise.Framework.Base.DatasetGateway.Shipping.CarrierDatasetGateway();
                var rateFacade = new Interprise.Facade.Base.Shipping.RateFacade(rateGateway);

                string[] warehouseInfo = null;
                string warehouseCode = String.Empty, isoCode = String.Empty, state = String.Empty, postalCode = String.Empty;
                bool isResidential = true;

                if (ThisCustomer.IsRegistered)
                {
                    warehouseInfo = rateFacade.GetRow(new string[] { "WarehouseCode", "ISOCode", "State", "PostalCode", "AddressType" },
                        "InventoryWarehouse IW (NOLOCK) INNER JOIN SystemCountry SC (NOLOCK) ON IW.Country = SC.CountryCode",
                        "IW.WarehouseCode = (SELECT TOP 1 WarehouseCode FROM CustomerShipTo WHERE ShipToCode IN (SELECT TOP 1 DefaultShippingCode FROM CRMContact WHERE EntityCode = "
                        + DB.SQuote(forCustomer) + " AND ContactCode = " + ((ThisCustomer.IsRegistered) ? contactCode : ThisCustomer.ContactCode.ToDbQuote()) + "))", false);
                }
                else
                {
                    warehouseInfo = rateFacade.GetRow(new string[] { "WarehouseCode", "ISOCode", "State", "PostalCode", "AddressType" },
                        "InventoryWarehouse IW (NOLOCK) INNER JOIN SystemCountry SC (NOLOCK) ON IW.Country = SC.CountryCode",
                        "IW.WarehouseCode = (SELECT WarehouseCode FROM CustomerShipToClassTemplateDetailView WHERE ClassCode = (SELECT DefaultRetailCustomerShipToClassTemplate FROM SystemCountry" +
                        " WHERE CountryCode = " + preferredShippingAddress.Country.ToDbQuote() + "))", false);
                }

                if (warehouseInfo != null)
                {
                    warehouseCode = warehouseInfo[0];
                    isoCode = warehouseInfo[1];
                    state = warehouseInfo[2];
                    postalCode = warehouseInfo[3];
                    isResidential = (warehouseInfo[4] == "Residential");
                }

                if (shippingMethodInfo.FreightCalculation == 2)
                {
                    decimal flatCharge = ComputeFreightCharge(shippingMethodInfo.Code, 0, shipments, exchangeRate);
                    freight = GetShippingMethodCarrierRate(preferredShippingAddress, shippingMethodInfo, shipments, rateFacade, rateGateway, warehouseCode, isoCode, state, postalCode, isResidential, flatCharge);
                }
                else
                {
                    freight = GetShippingMethodCarrierRate(preferredShippingAddress, shippingMethodInfo, shipments, rateFacade, rateGateway, warehouseCode, isoCode, state, postalCode, isResidential);
                }

                if (rateFacade != null)
                {
                    rateFacade.Dispose();
                    rateFacade = null;
                }
                if (rateGateway != null)
                {
                    rateGateway.Dispose();
                    rateGateway = null;
                }
            }
            else
            {
                freight = ComputeFreightCharge(shippingMethodInfo.Code, shippingMethodInfo.MiscAmount, shipments, exchangeRate);
            }

            decimal shippingExtraFee = AppLogic.AppConfigNativeDecimal("ShippingHandlingExtraFee");
            if (shippingExtraFee > Decimal.Zero && !freight.Equals(Decimal.Zero))
            {
                freight += shippingExtraFee;
            }

            shippingMethodInfo.Freight = freight;
            shippingMethodInfo.FreightDisplay = freight.ToCustomerCurrency();
            shippingMethodInfo.FreightCurrencyCode = ThisCustomer.CurrencyCode;
        }

        public CouponStruct GetCartCouponSetting(string couponCode = "")
        {
            if (this.Coupon.m_IsInitialized) { return this.Coupon; }
            var thisCoupon = new CouponStruct();
            if (couponCode.IsNullOrEmptyTrimmed()) { HasCoupon(ref couponCode); }
            if (couponCode.IsNullOrEmptyTrimmed()) { return thisCoupon; }

            string customerCode = CommonLogic.IIF(ThisCustomer.IsRegistered, ThisCustomer.CustomerCode, ThisCustomer.AnonymousCustomerCode);
            // validate the coupon
            string couponQuery =
            string.Format("SELECT * FROM eCommerceValidCouponView with (NOLOCK) WHERE CustomerCode = {0} AND CouponCode = {1} AND ShortString = {2} AND {3} BETWEEN StartingDate AND ExpirationDate",
                DB.SQuote(customerCode),
                DB.SQuote(couponCode),
                DB.SQuote(ThisCustomer.LocaleSetting),
                DB.SQuote(Localization.ToDBDateTimeString(DateTime.Today))
            );


            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, couponQuery))
                {
                    if (reader.Read())
                    {

                        thisCoupon.m_Code = DB.RSField(reader, "CouponCode");
                        thisCoupon.m_id = DB.RSField(reader, "CouponID");
                        thisCoupon.m_Description = DB.RSField(reader, "Description");

                        switch (DB.RSField(reader, "CouponType").ToLowerInvariant())
                        {
                            case "products":
                                thisCoupon.m_CouponType = CouponTypeEnum.ProductCoupon;
                                break;
                            case "orders":
                                thisCoupon.m_CouponType = CouponTypeEnum.OrderCoupon;
                                break;
                        }

                        switch (DB.RSField(reader, "DiscountType").ToLowerInvariant())
                        {
                            case "percent":
                                thisCoupon.m_DiscountType = CouponDiscountTypeEnum.PercentDiscount;
                                break;
                            case "amount":
                                thisCoupon.m_DiscountType = CouponDiscountTypeEnum.AmountDiscount;
                                break;
                        }

                        thisCoupon.m_DiscountAmount = DB.RSFieldDecimal(reader, "DiscountAmount");
                        thisCoupon.m_DiscountPercent = ((Decimal)DB.RSFieldSingle(reader, "DiscountPercent"));

                        thisCoupon.m_IncludesFreeShipping = DB.RSFieldBool(reader, "DiscountIncludesFreeShipping");
                        thisCoupon.m_IncludesAllCustomer = DB.RSFieldBool(reader, "IncludeAllCustomer");
                        thisCoupon.m_IncludesAllProduct = DB.RSFieldBool(reader, "IncludeAllProduct");

                        switch (DB.RSFieldInt(reader, "CouponUsage"))
                        {
                            case 1:
                                thisCoupon.m_ExpiresOnFirstUseByAnyCustomer = true;
                                thisCoupon.m_ExpiresAfterOneUsageByEachCustomer = false;
                                thisCoupon.m_ExpiresAfterNUses = 0;
                                break;
                            case 2:
                                thisCoupon.m_ExpiresOnFirstUseByAnyCustomer = false;
                                thisCoupon.m_ExpiresAfterOneUsageByEachCustomer = true;
                                thisCoupon.m_ExpiresAfterNUses = 0;
                                break;
                            case 3:
                                thisCoupon.m_ExpiresOnFirstUseByAnyCustomer = false;
                                thisCoupon.m_ExpiresAfterOneUsageByEachCustomer = false;
                                thisCoupon.m_ExpiresAfterNUses = DB.RSFieldInt(reader, "ExpiresAfterNUses");
                                break;
                        }

                        thisCoupon.m_RequiresMinimumOrderAmount = DB.RSFieldDecimal(reader, "RequiresMinimumOrderAmount");
                        thisCoupon.m_ValidForThisEntity = DB.RSField(reader, "ItemCategory");
                        thisCoupon.m_NumUses = DB.RSFieldInt(reader, "TotalUsageCount");
                        thisCoupon.m_CouponComputation = DB.RSField(reader, "CouponComputation");
                        thisCoupon.m_IsInitialized = true;
                        this.Coupon = thisCoupon;
                    }

                }
            }

            return thisCoupon;
        }

        public decimal GetCouponDiscountedAmount(decimal amount)
        {
            return GetDiscountedAmount(amount);
        }

        private decimal GetDiscountedAmount(decimal amount)
        {
            string couponDiscountType = String.Empty;
            decimal couponDiscountPercent = Decimal.Zero;
            decimal couponDiscountAmount = Decimal.Zero;

            if (!IsSalesOrderDetailBuilt)
            {
                var couponSetting = this.GetCartCouponSetting();
                couponDiscountType = (couponSetting.m_DiscountType == CouponDiscountTypeEnum.AmountDiscount) ? Interprise.Framework.Customer.Shared.Const.COUPON_DISCOUNTTYPE_AMOUNT : Interprise.Framework.Customer.Shared.Const.COUPON_DISCOUNTTYPE_PERCENT;
                couponDiscountPercent = couponSetting.m_DiscountPercent;
                decimal exchangeRate = Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(m_ThisCustomer.CurrencyCode);
                couponDiscountAmount = Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(exchangeRate, couponSetting.m_DiscountAmount);
            }
            else
            {
                couponDiscountType = SalesOrderDataset.CustomerSalesOrderView[0].CouponDiscountType.ToString();
                couponDiscountPercent = SalesOrderDataset.CustomerSalesOrderView[0].CouponDiscountPercent;
                couponDiscountAmount = SalesOrderDataset.CustomerSalesOrderView[0].CouponDiscountAmount;
            }

            if (couponDiscountType.ToLowerInvariant().Equals("percent"))
            {
                amount = amount - (amount * Decimal.Divide(couponDiscountPercent, 100));
            }
            else
            {
                amount = amount - couponDiscountAmount;
            }

            if (amount < Decimal.Zero) { amount = Decimal.Zero; }
            
            return amount;
        }

        public decimal GetCartItemCouponDiscount(CartItem cartitem)
        {
            // Check if coupon valid for the item
            if (Coupon.m_CouponType == CouponTypeEnum.ProductCoupon)
            {
                if (!CouponIsValidForThisProduct(cartitem.ItemCode, Coupon.m_Code)) { return Decimal.Zero; }
            }
            // check minimum amount
            decimal cartOrderTotal = this.GetCartSubTotal();
            if (cartOrderTotal < Coupon.m_RequiresMinimumOrderAmount) { return Decimal.Zero; }

            decimal couponDiscount = Decimal.Zero;
            decimal couponDiscountRate = Decimal.Zero;
            Interprise.Facade.Customer.BaseSalesOrderFacade couponCalculator = new Interprise.Facade.Customer.BaseSalesOrderFacade();
            // coupon info
            Interprise.Framework.Customer.Shared.Structure.CouponInfo couponInfo = new Interprise.Framework.Customer.Shared.Structure.CouponInfo();
            couponInfo.CouponID = Coupon.m_id;
            couponInfo.CouponComputation = Coupon.m_CouponComputation;
            couponInfo.CouponType = (Coupon.m_CouponType == CouponTypeEnum.ProductCoupon) ? "Products" : "Orders";
            couponInfo.DiscountAmount = Coupon.m_DiscountAmount;
            couponInfo.DiscountType = (Coupon.m_DiscountType == CouponDiscountTypeEnum.AmountDiscount) ? "Amount" : "Percent";
            couponInfo.DiscountPercent = Coupon.m_DiscountPercent;
            couponInfo.RequiresMinimumOrderAmount = Coupon.m_RequiresMinimumOrderAmount;

            decimal itemTax = (cartitem.TaxRate / cartitem.m_Quantity);
            decimal itemSalePrice = (this._customer.VATSettingReconciled == VatDefaultSetting.Inclusive) ? (cartitem.GetProductSalePrice() - itemTax) : cartitem.GetProductSalePrice();
            decimal itemNetPrice = (this._customer.VATSettingReconciled == VatDefaultSetting.Inclusive) ? (((cartitem.Price + cartitem.TaxRate) / cartitem.m_Quantity) - itemTax) : cartitem.UnitPrice;
            decimal totalLineItemPrice = cartitem.Price;
            decimal exchangeRate = couponCalculator.GetExchangerate(m_ThisCustomer.CurrencyCode);

            //compound computation
            if (couponInfo.CouponComputation == "Compound")
            {
                itemSalePrice *= cartitem.m_Quantity;
                itemNetPrice *= cartitem.m_Quantity;
            }

            // check if Amount discount
            if (Coupon.m_DiscountType == CouponDiscountTypeEnum.AmountDiscount)
            {
                // subtract item price of items not included
                if (!Coupon.m_IncludesAllProduct)
                {
                    foreach (CartItem lineItem in this.CartItems)
                    {
                        if (lineItem.ItemCode != cartitem.ItemCode && !CouponIsValidForThisProduct(lineItem.ItemCode, Coupon.m_Code))
                        {
                            cartOrderTotal -= lineItem.Price;
                        }
                    }
                }
                couponDiscount = (totalLineItemPrice / cartOrderTotal) * Coupon.m_DiscountAmount;
                couponDiscountRate = couponCalculator.ConvertCurrency(exchangeRate, couponDiscount);

                couponDiscountRate = couponCalculator.RoundMonetary(couponDiscountRate, m_ThisCustomer.CurrencyCode);
                if (couponDiscountRate >= totalLineItemPrice) { cartitem.TaxRate = decimal.Zero; }
                return couponDiscountRate;
            }
            else
            {
                // compute discount reset
                if (Coupon.m_RequiresMinimumOrderAmount > Decimal.Zero) { couponInfo.RequiresMinimumOrderAmount = itemSalePrice; }

                couponCalculator.ComputeItemCouponDiscount(couponInfo, new string[] { cartitem.ItemCode },
                new decimal[] { itemSalePrice }, new decimal[] { itemNetPrice },
                totalLineItemPrice, ref couponDiscount, ref couponDiscountRate, m_ThisCustomer.CurrencyCode);
                //Convert decimal place to 2 without rounding off to show result same as CB. 
                //couponDiscount = Math.Floor((decimal)couponDiscount * (decimal)Math.Pow(10, 2)) / (decimal)Math.Pow(10, 2);
                couponDiscountRate = exchangeRate * couponDiscount;
                // do not multiply for 
                if (couponInfo.CouponComputation == "Compound" || Coupon.m_CouponType == CouponTypeEnum.OrderCoupon)
                { 
                    return couponDiscountRate; 
                }

                //Round-off decimal 2 to places per CB requirements
                //couponDiscountRate = Math.Round(couponDiscountRate, 2);
                //Compute proper discount based on CB
                couponDiscountRate = itemSalePrice - couponDiscountRate;
                cartitem.DiscountedPrice = itemSalePrice - couponDiscountRate;
                if (cartitem.ItemType == "Kit" && (cartitem.CategoryCode != "White bag and sticker bundles" && cartitem.CategoryCode != "Brown bag and sticker bundles"))
                {
                    cartitem.DiscountedSubTotal = cartitem.DiscountedPrice * (cartitem.m_Quantity / cartitem.UnitMeasureQty);
                }
                else
                {
                    cartitem.DiscountedSubTotal = cartitem.DiscountedPrice * cartitem.m_Quantity;
                }
                // this should return discounted price 
                //couponDiscountRate = couponDiscountRate * cartitem.m_Quantity;
                if (couponDiscountRate >= totalLineItemPrice) { cartitem.TaxRate = decimal.Zero; }
                return couponDiscountRate;
            }

        }

        private Decimal ComputeFreightCharge(string shippingMethodCode, decimal miscAmount, RTShipping.Shipments shipments, decimal exchangeRate = 0)
        {
            decimal returnValue = 0;
            var freightFacade = (SalesOrderFacade == null) ? new BaseSalesOrderFacade() : SalesOrderFacade;

            string currencyCode = ThisCustomer.CurrencyCode;
            exchangeRate = (exchangeRate == 0) ? freightFacade.GetExchangerate(currencyCode) : exchangeRate;

            foreach (RTShipping.Packages Shipment in shipments)
            {
                foreach (RTShipping.Package p in Shipment)
                {
                    decimal tempReturn = 0;
                    decimal baseInsuredValue = freightFacade.ConvertCurrency(exchangeRate, p.InsuredValue, true, currencyCode);

                    freightFacade.ComputeFreightCharge(shippingMethodCode, baseInsuredValue, p.Weight, ref tempReturn, ThisCustomer.CurrencyCode, true);
                    
                    returnValue += CommonLogic.IIF(p.IsShipSeparately, tempReturn * p.Quantity, tempReturn);
                    returnValue = freightFacade.ConvertCurrency(exchangeRate, returnValue);
                    returnValue += CommonLogic.IIF(p.IsShipSeparately, miscAmount * p.Quantity, miscAmount);
                }
            }

            return returnValue;
        }

        private ShippingMethodDTO GetSimilarShippingMethod(ShippingMethodDTOCollection processedShippingMethods, ShippingMethodDTO currentShippingMethod)
        {
            return processedShippingMethods.FirstOrDefault(shippingMethod => shippingMethod.Code != currentShippingMethod.Code && (shippingMethod.FreightCalculation == 1 || shippingMethod.FreightCalculation == 2) && shippingMethod.CarrierCode == currentShippingMethod.CarrierCode && shippingMethod.ServiceType.ToUpper() == currentShippingMethod.ServiceType.ToUpper() &&
                                                    shippingMethod.PackagingType.ToUpper() == currentShippingMethod.PackagingType.ToUpper());
        }

        private RTShipping.Package CreatePackageComposition(int id, decimal weight, decimal insurance, Dimension dimensions)
        {
            var package = new RTShipping.Package();
            package.PackageId = id;
            package.Length = dimensions.Length;
            package.Width = dimensions.Width;
            package.Height = dimensions.Height;
            package.Weight = weight;
            package.InsuredValue = insurance;
            package.Insured = true;
            return package;
        }

        private RTShipping.Shipments GetShipmentComposition(Address shippingAddress, bool isForOverSized)
        {
            // create shipments collection
            var shipments = new RTShipping.Shipments();
            if (IsCartItemsOversizedDownloadsAndService() && !isForOverSized) return shipments;

            int packageID = 1;
            if (!isForOverSized)
            {
                var itemsThatPrepacked = CartItems.FindAll(new FindAllPrePackedItems().Do);
                foreach (CartItem shipSeparatelyItem in itemsThatPrepacked)
                {
                    var shipment = new RTShipping.Packages();
                    // set the destination address of this shipment
                    shipment = ShipmentDestination(shipment, shippingAddress);

                    var shipSeparatelyItemRow = shipSeparatelyItem.AssociatedLineItemRow;
                    decimal itemUnitPrice = (shipSeparatelyItemRow == null) ? shipSeparatelyItem.UnitPrice : shipSeparatelyItemRow.SalesPriceRate;
                    if (!ThisCustomer.CouponCode.IsNullOrEmptyTrimmed())
                    {
                        itemUnitPrice = (shipSeparatelyItemRow == null) ? shipSeparatelyItem.UnitPrice : shipSeparatelyItemRow.NetPriceRate;
                    }

                    var package = CreatePackageComposition(packageID++, shipSeparatelyItem.Weight, itemUnitPrice, Dimension.Parse(shipSeparatelyItem.Dimensions));
                    package.IsPrePacked = shipSeparatelyItem.IsPrePacked;
                    package.Quantity = shipSeparatelyItem.m_Quantity;
                    // add packages to the collection
                    shipment.AddPackage(package);
                    shipments.AddPackages(shipment);
                }

                var itemsThatDoesNotShipSeparately = CartItems.FindAll(new FindAllNonShipSeparatelyItems().Do);
                if (itemsThatDoesNotShipSeparately.Count > 0)
                {
                    decimal overallWeight = Decimal.Zero;
                    decimal overallInsurance = Decimal.Zero;

                    foreach (CartItem nonShipSeparatelyItem in itemsThatDoesNotShipSeparately)
                    {
                        var nonShipSeparatelyItemRow = nonShipSeparatelyItem.AssociatedLineItemRow;
                        decimal weight = nonShipSeparatelyItem.Weight;

                        //Thread probem - nonShipSeparatelyItem.ThisCustomer - calls http context
                        if (nonShipSeparatelyItem.ThisCustomer == null)
                        {
                            nonShipSeparatelyItem.ThisCustomer = ThisCustomer;
                        }

                        overallWeight += (weight * nonShipSeparatelyItem.m_Quantity);

                        if (!ThisCustomer.CouponCode.IsNullOrEmptyTrimmed())
                        {
                            decimal itemPrice = Decimal.Zero;
                            if (nonShipSeparatelyItemRow == null) 
                            {
                                decimal discountAmount = (nonShipSeparatelyItem.DiscountAmountAlreadyComputed) ? nonShipSeparatelyItem.CouponDiscount : this.GetCartItemCouponDiscount(nonShipSeparatelyItem);
                                itemPrice = nonShipSeparatelyItem.Price - discountAmount;
                            }
                            else
                            {
                                itemPrice = nonShipSeparatelyItemRow.ExtPriceRate;
                            }

                            overallInsurance += itemPrice;
                        }
                        else
                        {
                            overallInsurance += ((nonShipSeparatelyItemRow == null) ? nonShipSeparatelyItem.UnitPrice : nonShipSeparatelyItemRow.SalesPriceRate) * nonShipSeparatelyItem.m_Quantity;
                        }
                    }

                    var shipment = new RTShipping.Packages();

                    // set the destination address of this shipment group
                    shipment = ShipmentDestination(shipment, shippingAddress);

                    // Create package object for this item
                    var package = CreatePackageComposition(packageID++, overallWeight, overallInsurance, Dimension.None);

                    shipment.AddPackage(package);
                    shipments.AddPackages(shipment);
                }
            }
            else
            {
                var itemsThatOverSized = CartItems.FindAll(new FindAllOverSizedItems().Do);
                foreach (var shipSeparatelyItem in itemsThatOverSized)
                {
                    var shipment = new RTShipping.Packages();
                    // set the destination address of this shipment
                    shipment = ShipmentDestination(shipment, shippingAddress);

                    var shipSeparatelyItemRow = shipSeparatelyItem.AssociatedLineItemRow;
                    decimal itemUnitPrice = (shipSeparatelyItemRow == null) ? shipSeparatelyItem.UnitPrice : shipSeparatelyItemRow.SalesPriceRate;
                    if (!ThisCustomer.CouponCode.IsNullOrEmptyTrimmed())
                    {
                        itemUnitPrice = (shipSeparatelyItemRow == null) ? shipSeparatelyItem.UnitPrice : shipSeparatelyItemRow.NetPriceRate;
                    }

                    var package = CreatePackageComposition(packageID++, shipSeparatelyItem.Weight, itemUnitPrice, Dimension.Parse(shipSeparatelyItem.Dimensions));
                    package.IsOverSized = shipSeparatelyItem.IsOverSized;
                    package.Quantity = shipSeparatelyItem.m_Quantity;
                    package.OverSizedItemName = shipSeparatelyItem.DisplayName;
                    package.OverSizedItemCode = shipSeparatelyItem.ItemCode;
                    package.UnitMeasureCode = shipSeparatelyItem.UnitMeasureCode;

                    if (package.IsOverSized)
                    {
                        package.OverSizedShippingMethodCode = shipSeparatelyItem.OverSizedShippingMethodCode;
                    }
                    // add packages to the collection
                    shipment.AddPackage(package);
                    shipments.AddPackages(shipment);
                }
            }

            return shipments;
        }

        private decimal GetShippingMethodCarrierRate(Address preferredShippingAddress, ShippingMethodDTO shippingMethod, RTShipping.Shipments shipments, Interprise.Facade.Base.Shipping.RateFacade rateFacade,
                                                     Interprise.Framework.Base.DatasetGateway.Shipping.CarrierDatasetGateway rateGateway, string warehouseCode,
                                                     string srcCountry, string srcState, string srcZIP, bool srcIsResidential, decimal freight)
        {
            decimal carrierRate = Decimal.Zero;
            string response = String.Empty;

            if (shippingMethod.Code == "Next Day Dispatch")
            {
                carrierRate = GetTOLLIPECShippingRate(preferredShippingAddress);
            }
            else
            {
                foreach (RTShipping.Packages Shipment in shipments)
                {
                    foreach (RTShipping.Package p in Shipment)
                    {
                        if (p.IsFreeShipping) { continue; }

                        rateGateway.RateServiceDetail.Clear();

                        if (!p.IsPrePacked && !p.IsOverSized)
                        {
                            try
                            {
                                if (shippingMethod.FreightCalculation == 2)
                                {
                                    shippingMethod.IsError = !rateFacade.GetRealTimeRates(ref response, shippingMethod.CarrierCode, shippingMethod.Code, shippingMethod.FreightCalculation, warehouseCode, srcCountry, srcState, srcZIP, srcIsResidential, Shipment.DestinationCountryCode, Shipment.DestinationStateProvince,
                                                            Shipment.DestinationZipPostalCode, Shipment.DestinationIsResidential, p.Weight.ToString(), Decimal.Zero.ToString(), Convert.ToInt32(shippingMethod.Length), Convert.ToInt32(shippingMethod.Width), Convert.ToInt32(shippingMethod.Height),
                                                            shippingMethod.ServiceType, shippingMethod.PackagingType, Customer.Current.CurrencyCode, p.InsuredValue, shippingMethod.WeightThreshold, freight, shippingMethod.MiscAmount);
                                }
                                else
                                {
                                    shippingMethod.IsError = !rateFacade.GetRealTimeRates(ref response, shippingMethod.CarrierCode, shippingMethod.Code, shippingMethod.FreightCalculation, warehouseCode, srcCountry, srcState, srcZIP, srcIsResidential, Shipment.DestinationCountryCode, Shipment.DestinationStateProvince,
                                                            Shipment.DestinationZipPostalCode, Shipment.DestinationIsResidential, p.Weight.ToString(), Decimal.Zero.ToString(), Convert.ToInt32(shippingMethod.Length), Convert.ToInt32(shippingMethod.Width), Convert.ToInt32(shippingMethod.Height),
                                                            shippingMethod.ServiceType, shippingMethod.PackagingType, Customer.Current.CurrencyCode, p.InsuredValue, shippingMethod.WeightThreshold);
                                }
                            }
                            catch (Exception ex)
                            {
                                shippingMethod.IsError = true;
                                response = ex.Message;
                            }
                        }
                        else
                        {
                            try
                            {
                                if (shippingMethod.FreightCalculation == 2)
                                {
                                    shippingMethod.IsError = !rateFacade.GetRealTimeRates(ref response, shippingMethod.CarrierCode, shippingMethod.Code, shippingMethod.FreightCalculation, warehouseCode, srcCountry, srcState, srcZIP, srcIsResidential, Shipment.DestinationCountryCode, Shipment.DestinationStateProvince,
                                                            Shipment.DestinationZipPostalCode, Shipment.DestinationIsResidential, p.Weight.ToString(), Decimal.Zero.ToString(), Convert.ToInt32(p.Length), Convert.ToInt32(p.Width), Convert.ToInt32(p.Height),
                                                            shippingMethod.ServiceType, shippingMethod.PackagingType, Customer.Current.CurrencyCode, p.InsuredValue, shippingMethod.WeightThreshold, freight, shippingMethod.MiscAmount);
                                }
                                else
                                {
                                    shippingMethod.IsError = !rateFacade.GetRealTimeRates(ref response, shippingMethod.CarrierCode, shippingMethod.Code, shippingMethod.FreightCalculation, warehouseCode, srcCountry, srcState, srcZIP, srcIsResidential, Shipment.DestinationCountryCode, Shipment.DestinationStateProvince,
                                                            Shipment.DestinationZipPostalCode, Shipment.DestinationIsResidential, p.Weight.ToString(), Decimal.Zero.ToString(), Convert.ToInt32(p.Length), Convert.ToInt32(p.Width), Convert.ToInt32(p.Height),
                                                            shippingMethod.ServiceType, shippingMethod.PackagingType, Customer.Current.CurrencyCode, p.InsuredValue, shippingMethod.WeightThreshold);
                                }
                            }
                            catch (Exception ex)
                            {
                                shippingMethod.IsError = true;
                                response = ex.Message;
                            }
                        }

                        if (shippingMethod.IsError)
                        {
                            shippingMethod.Description = String.Format("{0} ({1})", shippingMethod.Description, response);
                            return Decimal.Zero;
                        }

                        decimal totalRate = rateGateway.RateServiceDetail.Rows
                                                                         .OfType<DataRow>()
                                                                         .Sum(r => r[rateGateway.RateServiceDetail.TotalColumn.ColumnName].ToString().TryParseDecimal().Value);
                        carrierRate += (p.IsShipSeparately) ? totalRate * p.Quantity : totalRate;
                    }
                }
            }

            shippingMethod.ReturnedRate = carrierRate;
            if (shippingMethod.FreightCalculation == 1)
            {
                carrierRate += shippingMethod.MiscAmount * shipments.PackageCount;
            }

            return carrierRate;
        }

        private decimal GetShippingMethodCarrierRate(Address preferredShippingAddress, ShippingMethodDTO shippingMethod, RTShipping.Shipments shipments, Interprise.Facade.Base.Shipping.RateFacade rateFacade,
                                                     Interprise.Framework.Base.DatasetGateway.Shipping.CarrierDatasetGateway rateGateway, string warehouseCode,
                                                     string srcCountry, string srcState, string srcZIP, bool srcIsResidential)
        {
            return GetShippingMethodCarrierRate(preferredShippingAddress, shippingMethod, shipments, rateFacade, rateGateway, warehouseCode, srcCountry, srcState, srcZIP, srcIsResidential, 0);
        }

        private decimal GetTOLLIPECShippingRate(Address preferredShippingAddress)
        {
            List<LineItem> items = new List<LineItem>();
            LineItem item;

            APIClient apiClient = new APIClient(new APIClientDetails
            {
                BaseUrl = AppLogic.AppConfig("custom.smartbag.shipping.rate.baseurl"),
                Username = AppLogic.AppConfig("custom.smartbag.shipping.rate.username"),
                Key = AppLogic.AppConfig("custom.smartbag.shipping.rate.key"),
            });

            CartItems.ForEach(cart => 
            {
                if (cart.ItemType != "Service")
                {
                    var defaultUm = ServiceFactory.GetInstance<IInventoryRepository>().GetItemDefaultUnitMeasure(cart.ItemCode);
                    if (defaultUm.Code.ToUpper() == "EACH")
                    {
                        item = new LineItem
                        {
                            Counter = cart.ItemCounter,
                            ItemCode = cart.ItemCode,
                            Quantity = 1,
                            Weight = defaultUm.Weight * Convert.ToInt32(cart.m_Quantity),
                            Length = 0,
                            Width = 0,
                            Height = 0,
                        };
                    }
                    else
                    {
                        if (cart.ItemType.ToLower() == "kit" && (cart.CategoryCode.ToLower() != "white bag and sticker bundles" && cart.CategoryCode.ToLower() != "brown bag and sticker bundles"))
                        {
                            item = new LineItem
                            {
                                Counter = cart.ItemCounter,
                                ItemCode = cart.ItemCode,
                                Quantity = Convert.ToInt32(cart.m_Quantity / cart.UnitMeasureQty),
                                Weight = defaultUm.Weight,
                                Length = defaultUm.Length,
                                Width = defaultUm.Width,
                                Height = defaultUm.Height,
                            };
                        }
                        else
                        {
                            item = new LineItem
                            {
                                Counter = cart.ItemCounter,
                                ItemCode = cart.ItemCode,
                                Quantity = Convert.ToInt32(cart.m_Quantity),
                                Weight = defaultUm.Weight,
                                Length = defaultUm.Length,
                                Width = defaultUm.Width,
                                Height = defaultUm.Height,
                            };
                        }
                    }
                    items.Add(item);
                }
            });
            // call the api to get the rates
            var warehouseRow = ServiceFactory.GetInstance<IWarehouseRepository>()
                                                         .GetWarehouseRowByCode(Customer.Current.WarehouseCode);
            var request = new GetCostEstimateRequest
            {
                EstimateDate = DateTime.Now,

                // Warehouse Details
                SenderTown = warehouseRow[8].ToString(),
                SenderPostcode = Convert.ToInt32(warehouseRow[10]),
                //SenderTown = "Ingleburn",
                //SenderTown = "Ingleburn",
                //SenderPostcode = 2565,

                // Receiver
                ReceiverTown = preferredShippingAddress.City,
                ReceiverPostcode = Convert.ToInt32(preferredShippingAddress.PostalCode),
                //ReceiverTown = "Caringbah",
                //ReceiverPostcode = 2229,


                ServiceCode = null,
                AutobestOnly = true, // if true, will return the best rate only, if false will return multiple rates

                ItemsQuantity = new Dictionary<string, int>(),
                ItemsLength = new Dictionary<string, float>(),
                ItemsWidth = new Dictionary<string, float>(),
                ItemsHeight = new Dictionary<string, float>(),
                ItemsWeight = new Dictionary<string, float>(),
                ItemsCube = new Dictionary<string, float>(),
                ItemsIsdg = new Dictionary<string, bool>()
            };
            int counter = 0;
            string itemCode = string.Empty;
            items.ForEach(shipitem =>
            {
                counter += 1;
                itemCode = shipitem.ItemCode + counter;
                request.ItemsQuantity.Add(itemCode, shipitem.Quantity);
                request.ItemsLength.Add(itemCode, (float)shipitem.Length);
                request.ItemsWidth.Add(itemCode, (float)shipitem.Width);
                request.ItemsHeight.Add(itemCode, (float)shipitem.Height);
                request.ItemsWeight.Add(itemCode, (float)shipitem.Weight);
                request.ItemsIsdg.Add(itemCode, false); // just set to false, since we are not shipping dangerous goods
            });

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            GetCostEstimateResponse response = apiClient.GetCostEstimate(request);
            decimal shippingRate = 0;
            if (response == null)
            {
                ThisCustomer.ThisCustomerSession["CustomShippingError"] = "Cannot connect to shipping carrier";
            }
            else
            {
                if (response.ErrorMessage != null)
                {
                    switch (response.ErrorCode.ToUpper())
                    {
                        case "LOCATION_VALIDATION_FAILED" :
                            ThisCustomer.ThisCustomerSession["CustomShippingError"] = "Invalid Shipping Address. Please change your Shipping Address and try again.";
                            break;
                        default:
                            ThisCustomer.ThisCustomerSession["CustomShippingError"] = response.ErrorMessage;
                            break;
                    }
                }
                else
                {
                    shippingRate = (decimal)response.CostEstimate.First().Value.EstimatedCost;
                }
            }
            decimal extraFee = 0;
            if (decimal.TryParse(AppLogic.AppConfig("custom.shipping.extra.handling.fee"), out extraFee))
            {
                if (extraFee > 0)
                {
                    shippingRate = shippingRate + (shippingRate * (extraFee / 100));
                }
            }
            return shippingRate;
        }

        public RTShipping.Packages ShipmentDestination(RTShipping.Packages thisShipment, Address destinationAddress)
        {
            thisShipment = new RTShipping.Packages();
            thisShipment.DestinationStateProvince = destinationAddress.State;
            thisShipment.DestinationZipPostalCode = destinationAddress.PostalCode;
            if (!destinationAddress.CountryISOCode.IsNullOrEmptyTrimmed())
            {
                thisShipment.DestinationCountryCode = destinationAddress.CountryISOCode;
            }
            else
            {
                thisShipment.DestinationCountryCode = AppLogic.GetCountryTwoLetterISOCode(destinationAddress.Country);
            }
            thisShipment.DestinationIsResidential = CommonLogic.IIF(destinationAddress.ResidenceType == ResidenceTypes.Residential, true, false);
            return thisShipment;
        }

        private class FindAllShippableItems
        {
            public bool ItemIsShippable(CartItem item)
            {
                return !item.IsDownload && !item.IsService;
            }
        }

        private class FindAllItemsWithSupplierThatShipSeparately : FindAllShippableItems
        {
            public bool Do(CartItem item)
            {
                return ItemIsShippable(item) && item.HasSupplier && item.CanShipSeparately && item.IsDropShip;
            }
        }

        private class FindAllItemsWithoutSupplierThatShipSeparately : FindAllShippableItems
        {
            public bool Do(CartItem item)
            {
                return ItemIsShippable(item) && !item.HasSupplier && item.CanShipSeparately;
            }
        }

        private class FindAllItemsBySupplierThatDoNotShipSeparately : FindAllShippableItems
        {
            private string _supplierCode = string.Empty;

            public FindAllItemsBySupplierThatDoNotShipSeparately(string supplierCode)
            {
                _supplierCode = supplierCode;
            }

            public bool Do(CartItem item)
            {
                return ItemIsShippable(item) && _supplierCode == item.SupplierCode && !item.CanShipSeparately && item.IsDropShip;
            }
        }

        private class FindAllItemsWithoutSupplierThatDoNotShipSeparately : FindAllShippableItems
        {
            public bool Do(CartItem item)
            {
                return ItemIsShippable(item) && !item.HasSupplier && !item.CanShipSeparately;
            }
        }

        private class FindAllShipSeparatelyItems
        {
            public bool Do(CartItem item)
            {
                return item.CanShipSeparately;
            }
        }

        private class FindAllPrePackedItems : FindAllShippableItems
        {
            public bool Do(CartItem item)
            {
                return ItemIsShippable(item) && item.IsPrePacked && !item.IsOverSized;
            }
        }

        private class FindAllOverSizedItems : FindAllShippableItems
        {
            public bool Do(CartItem item)
            {
                return ItemIsShippable(item) && item.IsOverSized;
            }
        }

        private class FindAllNonShipSeparatelyItems : FindAllShippableItems
        {
            public bool Do(CartItem item)
            {
                return !item.CanShipSeparately;
            }
        }

        public sealed class Dimension
        {
            public static readonly Dimension None = new Dimension();

            private Dimension()
            {
            }

            public Dimension(decimal length, decimal width, decimal height)
            {
                this.Length = length;
                this.Width = width;
                this.Height = height;
            }

            public static Dimension Parse(string s)
            {
                string dimensions = s.ToLowerInvariant();

                if (dimensions.Length != 0)
                {
                    string[] dd = dimensions.Split('x');

                    decimal length, width, height;
                    length = width = height = decimal.Zero;

                    try
                    {
                        length = Localization.ParseUSDecimal(dd[0].Trim());
                    }
                    catch
                    {
                    }
                    try
                    {
                        width = Localization.ParseUSDecimal(dd[1].Trim());
                    }
                    catch
                    {
                    }
                    try
                    {
                        height = Localization.ParseUSDecimal(dd[2].Trim());
                    }
                    catch
                    {
                    }

                    return new Dimension(length, width, height);
                }

                return Dimension.None;
            }

            public decimal Length;
            public decimal Width;
            public decimal Height;
        }

        public void ClearLineItems()
        {
            ServiceFactory.GetInstance<IShoppingCartService>()
                          .ClearLineItems(this.CartItems.Select(itm => itm.Id.ToString()).ToArray());
        }

        public void ClearGiftItemEmails()
        {
            if (this.HasGiftItems())
            {
                ServiceFactory.GetInstance<IShoppingCartService>().CleanupShoppingCartGiftEmail();
            }
        }

        public void ClearTransaction()
        {
            ClearLineItems();
            ClearGiftItemEmails();

            /*****************************************************
            *  5. Clear the Customer coupon info and notes if any
            * ***************************************************/
            string customerCode = CommonLogic.IIF(ThisCustomer.IsRegistered, ThisCustomer.CustomerCode, ThisCustomer.CustomerID);
            if (AppLogic.AppConfigBool("ClearCouponAfterOrdering"))
            {
                InterpriseHelper.ClearCustomerCoupon(customerCode, ThisCustomer.IsRegistered);
            }

            DB.ExecuteSQL("UPDATE Customer SET Notes = NULL WHERE CustomerCode = {0}", (ThisCustomer.IsRegistered) ? DB.SQuote(ThisCustomer.CustomerCode) : DB.SQuote(ThisCustomer.AnonymousCustomerCode));


            /***********************************************************
            *  6. Clear the Real-Time rates for this customer if any...
            * **********************************************************/
            DB.ExecuteSQL("DELETE EcommerceRealTimeRate WHERE ContactCode = {0}", DB.SQuote(ThisCustomer.ContactCode));
            ThisCustomer.ClearTransactions(true);
            ThisCustomer.ThisCustomerSession.ClearVal("anonymousCompany");
        }

        public decimal GetPackageWeightTotal()
        {
            decimal sum = Decimal.Zero;
            // NOTE : Interprise will not support items with separate shipments..
            foreach (CartItem item in CartItems)
            {
                if (item.ItemType != Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ELECTRONIC_DOWNLOAD &&
                    item.ItemType != Interprise.Framework.Base.Shared.Const.ITEM_TYPE_SERVICE)
                {
                    decimal weight = ServiceFactory.GetInstance<IInventoryRepository>()
                                                   .GetItemWeight(item.ItemCode, item.UnitMeasureCode);
                    sum += (item.m_Quantity * weight);
                }
            }

            if (sum == decimal.Zero)
            {
                sum = 0.5M; // must have something to use!
            }

            return sum;
        }

        public bool MeetsMinimumOrderWeight(decimal weight)
        {
            if (weight <= 0 || IsEmpty())
            {
                return true; // disable checking for empty cart or all system products cart (those are handled separately)
            }
            decimal weightTotal = GetPackageWeightTotal();
            return weightTotal >= weight;
        }

        #region Optimized Codes

        public decimal GetCartSubTotal()
        {
            decimal total = Decimal.Zero; 

            if (this.IsSalesOrderDetailBuilt && _gatewaySalesOrderDataset != null && !_facadeSalesOrder.ApplyCoupon)
            {
                total = _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SubTotalRate; // get it from the header...
            }
            else
            {
                total = this.CartItems.Sum(item => item.Price);
            }

            return total;
        }
        public decimal GetCartTaxTotal()
        {
            decimal total = Decimal.Zero;

            if (this.IsSalesOrderDetailBuilt && _gatewaySalesOrderDataset != null)
            {
                total = _gatewaySalesOrderDataset.CustomerSalesOrderView[0].TaxRate; // get it from the header...
            }
            else
            {
                total = this.CartItems.Sum(item => item.TaxRate);
            }

            return total;
        }
        public decimal GetCartFreightRate()
        {
            //if (CartFreightRate == 0 && !IsSalesOrderDetailBuilt)
            //{
            //    var shippingMethod = GetShippingMethods(ThisCustomer.PrimaryShippingAddress, "", this.FirstItem().GiftRegistryID);
            //    if (shippingMethod != null && shippingMethod.Count > 0)
            //    {
            //        for (int i = 0; i < shippingMethod.Count; i++)
            //        {
            //            if (i == 0 || shippingMethod[i].ForOversizedItem)
            //            { CartFreightRate += shippingMethod[i].Freight; }
            //        }
            //    }
            //}

            //Always get the latest shipping since TollIpec rate depends on item quantity and dimensions. This will make sure the correctrate is always returned.
            decimal shippingRate = 0;
            var shippingMethod = GetShippingMethods(ThisCustomer.PrimaryShippingAddress, "", this.FirstItem().GiftRegistryID);
            if (shippingMethod != null && shippingMethod.Count > 0)
            {
                for (int i = 0; i < shippingMethod.Count; i++)
                {
                    if (i == 0 || shippingMethod[i].ForOversizedItem)
                    { shippingRate += shippingMethod[i].Freight; }
                }
                CartFreightRate = shippingRate;
            }
            return this.CartFreightRate;
        }

        public decimal GetCartFreightRateTax(string currencyCode, decimal freightRate, string freightTaxCode, Address address)
        {
            decimal taxRate = Decimal.Zero;
            var salesOrderFacade = new BaseSalesOrderFacade();
            decimal exchangeRate =  Currency.GetExchangeRate(currencyCode);
            
            int plus4 = 0;
            int.TryParse(address.Plus4, out plus4);

            taxRate = salesOrderFacade.ComputeFreightRateTax(SimpleFacade.Instance.ConvertCurrency(freightRate, exchangeRate), freightRate, freightTaxCode, address.PostalCode, address.City, address.Country, plus4, currencyCode,
                exchangeRate);

            salesOrderFacade.Dispose();

            return taxRate;
           
        }

        public string GetCartShippingMethodSelected()
        {
            var item = this.CartItems.FirstOrDefault();
            if (item != null)
            {
                return item.ShippingMethod;
            }
            return String.Empty;
        }

        #endregion
        

        //added by m.d
        public decimal GetCartSubTotalByGroup(CartItem.CartComparableArgs args, string groupEntityName)
        {
            decimal total = decimal.Zero;
            if (args == CartItem.CartComparableArgs.ITEM_CODE)
            {
                total = CartItems.Where(item => item.ItemCode.ToLower() == groupEntityName.ToLower())
                                 .Sum(item => item.Price);
            }
            else if (args == CartItem.CartComparableArgs.ITEM_DESCRIPTION)
            {
                total = CartItems.Where(item => item.ItemDescription.ToLower() == groupEntityName.ToLower())
                                 .Sum(item => item.Price);
            }
            else if (args == CartItem.CartComparableArgs.ITEM_NAME)
            {
                total = CartItems.Where(item => item.ItemName.ToLower() == groupEntityName.ToLower())
                                    .Sum(item => item.Price);
            }
            else if (args == CartItem.CartComparableArgs.ITEM_SUPPLIER_CODE)
            {
                total = CartItems.Where(item => item.ItemSupplierCode.ToLower() == groupEntityName.ToLower())
                                    .Sum(item => item.Price);
            }
            else if (args == CartItem.CartComparableArgs.ITEM_SUPPLIER_NAME)
            {
                total = CartItems.Where(item => item.ItemSupplierName.ToLower() == groupEntityName.ToLower())
                                 .Sum(item => item.Price);
            }
            else if (args == CartItem.CartComparableArgs.SUPPLIER_CODE)
            {
                total = CartItems.Where(item => item.SupplierCode.ToLower() == groupEntityName.ToLower())
                                 .Sum(item => item.Price);
            }
            return total;
        }

        public decimal GetCartTaxTotalByGroup(CartItem.CartComparableArgs args, string groupEntityName)
        {
            decimal total = decimal.Zero;
            if (args == CartItem.CartComparableArgs.ITEM_CODE)
            {
                total = CartItems.Where(item => item.ItemCode.ToLower() == groupEntityName.ToLower())
                                 .Sum(item => item.Price);
            }
            else if (args == CartItem.CartComparableArgs.ITEM_DESCRIPTION)
            {
                total = CartItems.Where(item => item.ItemDescription.ToLower() == groupEntityName.ToLower())
                                 .Sum(item => item.Price);
            }
            else if (args == CartItem.CartComparableArgs.ITEM_NAME)
            {
                total = CartItems.Where(item => item.ItemName.ToLower() == groupEntityName.ToLower())
                                 .Sum(item => item.Price);
            }
            else if (args == CartItem.CartComparableArgs.ITEM_SUPPLIER_CODE)
            {
                total = CartItems.Where(item => item.ItemSupplierCode.ToLower() == groupEntityName.ToLower())
                                 .Sum(item => item.Price);
            }
            else if (args == CartItem.CartComparableArgs.ITEM_SUPPLIER_NAME)
            {
                total = CartItems.Where(item => item.ItemSupplierName.ToLower() == groupEntityName.ToLower())
                                 .Sum(item => item.Price);
            }
            else if (args == CartItem.CartComparableArgs.SUPPLIER_CODE)
            {
                total = CartItems.Where(item => item.SupplierCode.ToLower() == groupEntityName.ToLower())
                                 .Sum(item => item.Price);
            }
            return total;
        }
        //end of added code



        public bool MeetsMinimumOrderAmount(decimal amount)
        {
            if (this.IsEmpty()) { return true; }

            decimal subTotal = GetCartSubTotal();
            //  NOTE:
            //  Unless the current setting is Vat.Enabled and our customer's vat setting is Vat.Inclusive (since that line item price is already including the tax)
            //  We will always compute the amount + tax
            bool excludeTax = (AppLogic.AppConfigBool("Vat.Enabled") && ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive);
            if (!excludeTax)
            {
                decimal taxTotal = GetCartTaxTotal();
                subTotal += taxTotal;
            }

            return subTotal >= amount;
        }

        public override bool MeetsMinimumOrderQuantity(Decimal MinOrderQuantity)
        {
            if (MinOrderQuantity <= 0 || IsEmpty())
            {
                return true; // disable checking for empty cart or all system products cart (those are handled separately)
            }

            decimal N = this.CartItems.Sum(c => c.m_Quantity);
            return (N >= MinOrderQuantity);
        }

        public static bool IsCouponHasFreeShipping(string couponCode)
        {
            bool isFreeShipping = false;

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var dr = DB.GetRSFormat(con, "SELECT DiscountIncludesFreeShipping FROM CustomerSalesCoupon with (NOLOCK) WHERE CouponCode = {0}", DB.SQuote(couponCode)))
                {
                    if (dr.Read())
                    {
                        isFreeShipping = DB.RSFieldBool(dr, "DiscountIncludesFreeShipping");
                    }
                }
            }

            return isFreeShipping;
        }

        public decimal GetOrderTotal()
        {
            if (!IsSalesOrderDetailBuilt)
            {
                decimal dicountRelatedAmount = decimal.Zero;
                if (this.HasCoupon())
                {
                    decimal computedTax = Decimal.Zero;
                    decimal couponDiscount = decimal.Zero;
                    foreach (CartItem cartItem in this.CartItems)
                    {
                        if (!cartItem.DiscountAmountAlreadyComputed)
                        {
                            cartItem.CouponDiscount = this.GetCartItemCouponDiscount(cartItem);
                            cartItem.DiscountAmountAlreadyComputed = true;
                        }

                        couponDiscount += cartItem.CouponDiscount;
                        if (cartItem.CouponDiscount > Decimal.Zero && cartItem.TaxRate > Decimal.Zero)
                        {
                            // Recompute tax based on discounted price fro GetCartSubTotal()
                            decimal extPrice = cartItem.Price;
                            decimal vat = cartItem.TaxRate;
                            extPrice = (((extPrice / cartItem.m_Quantity) - (couponDiscount / cartItem.m_Quantity)) * cartItem.m_Quantity).ToCustomerRoundedCurrency();
                            computedTax = computedTax + (vat - (extPrice * (vat / Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(cartItem.Price))).ToCustomerRoundedCurrency());
                        }
                    }
                    //dicountRelatedAmount += couponDiscount;
                    //dicountRelatedAmount += computedTax;
                }

                return (this.GetCartSubTotal() + this.GetCartTaxTotal() + this.CartFreightRate) - dicountRelatedAmount; ;
            }

            return _gatewaySalesOrderDataset.CustomerSalesOrderView[0].TotalRate;
        }

        public decimal GetOrderBalance()
        {
            decimal orderTotal = (this.IsSalesOrderDetailBuilt) ? _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BalanceRate : this.GetOrderTotal();
            decimal otherPaymentTotal = this.GetOtherPaymentTotal();
            if (otherPaymentTotal > orderTotal) { return Decimal.Zero; }
            return orderTotal.ToCustomerRoundedCurrency() - otherPaymentTotal.ToCustomerRoundedCurrency();
        }

        public decimal GetOrderTotal(string orderNumber)
        {
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var rs = DB.GetRSFormat(con, "select TotalRate from CustomerSalesOrder with (NOLOCK) where BillToCode=" + DB.SQuote(ThisCustomer.CustomerID) + " and SalesOrderCode =" + DB.SQuote(orderNumber)))
                {
                    if (rs.Read())
                    {
                        return DB.RSFieldDecimal(rs, "TotalRate");
                    }
                }
            }
            return 0;
        }

        #endregion

        #region IEnumerable Members

        public IEnumerator GetEnumerator()
        {
            return CartItems.GetEnumerator();
        }

        #endregion

        public bool ValidateOrderFraud(string salesOrderCode, Address billingAddress, Address shippingAddress)
        {
            bool isOrderFraud = false;
            decimal maxMindRiskScore = Decimal.Zero;
            string fraudDetails = String.Empty;

            try
            {
                string billingEmailDomain = billingAddress.EMail.ToLowerInvariant().Trim();
                if (billingEmailDomain.Length == 0)
                {
                    billingEmailDomain = ThisCustomer.EMail.ToLowerInvariant().Trim();
                }
                int i = billingEmailDomain.IndexOf("@");
                if (i != -1)
                {
                    try
                    {
                        billingEmailDomain = billingEmailDomain.Substring(i + 1);
                    }
                    catch { }
                }

                string emailMD5 = (billingAddress.EMail.Length != 0) ? ServiceFactory.GetInstance<ICryptographyService>().GetMD5Hash(billingAddress.EMail.Trim().ToLowerInvariant()) :
                                                                       ServiceFactory.GetInstance<ICryptographyService>().GetMD5Hash(ThisCustomer.EMail.Trim().ToLowerInvariant());
                string usernameMD5 = ServiceFactory.GetInstance<ICryptographyService>().GetMD5Hash(billingAddress.CardName.Trim().ToLowerInvariant());
                string orderAmount = GetCartSubTotal().ToString();

                var currentRequest = ServiceFactory.GetInstance<IHttpContextService>().CurrentRequest;
                string userAgent = currentRequest.Headers["User-Agent"];
                string acceptLanguage = currentRequest.Headers["Accept-Language"];

                string transactionType = String.Empty;
                string cardBINNumber = String.Empty;

                if (!ThisCustomer.ThisCustomerSession["paypalfrom"].IsNullOrEmptyTrimmed())
                {
                    transactionType = "paypal";
                }
                else if (ThisCustomer.PaymentTermCode == DomainConstants.PAYMENT_METHOD_CREDITCARD)
                {
                    transactionType = "creditcard";
                    string customerCreditCardNumber = ServiceFactory.GetInstance<ICryptographyService>().InterpriseDecryption(ThisCustomer.ThisCustomerSession["CardNumber"],
                                                                                                            ThisCustomer.ThisCustomerSession["CardNumberSalt"],
                                                                                                            ThisCustomer.ThisCustomerSession["CardNumberIV"]);
                    if (!customerCreditCardNumber.IsNullOrEmptyTrimmed())
                    {
                        cardBINNumber = customerCreditCardNumber.Substring(0, 6);
                    }
                }
                else
                {
                    transactionType = "other";
                }

                var minFraud = ServiceFactory.GetInstance<IThirdPartyProcessingService>().ParseMinFraudSoapObject(acceptLanguage, billingAddress.City, billingAddress.State, billingAddress.PostalCode, billingAddress.Country, billingEmailDomain,
                                                                                                                  cardBINNumber, String.Empty, String.Empty, billingAddress.Phone, String.Empty, emailMD5, usernameMD5, String.Empty,
                                                                                                                  shippingAddress.Address1, shippingAddress.City, shippingAddress.State, shippingAddress.PostalCode, shippingAddress.Country,
                                                                                                                  salesOrderCode, "ConnectedBusinessEcommerce", userAgent, orderAmount, String.Empty, String.Empty, String.Empty, transactionType);

                fraudDetails = minFraud.FraudDetails;
                maxMindRiskScore = minFraud.RiskScore;
                // ok, call worked, now let's determine what to do with it: 
                isOrderFraud = maxMindRiskScore >= AppLogic.AppConfigUSDecimal("MaxMind.FailScoreThreshold");
            }
            catch (Exception ex)
            {
                fraudDetails = ex.Message;
                maxMindRiskScore = -1.0M;

                // stop the order when an exception occur
                isOrderFraud = true;
            }

            if (isOrderFraud)
            {
                var transactionFailed = FailedTransaction.New();
                transactionFailed.CustomerCode = ThisCustomer.CustomerCode;
                transactionFailed.SalesOrderCode = salesOrderCode;
                transactionFailed.PaymentGateway = "MaxMind";
                transactionFailed.MaxMindDetails = fraudDetails;
                transactionFailed.TransactionCommand = FailedTransaction.NOT_APPLICABLE;
                transactionFailed.TransactionResult = fraudDetails;
                transactionFailed.IPAddress = ThisCustomer.LastIPAddress;
                transactionFailed.MaxMindFraudScore = maxMindRiskScore;
                transactionFailed.IsFailed = isOrderFraud;
                transactionFailed.Record();
            }

            return isOrderFraud;
        }

        private void AddLineItem(CartItem item)
        {
            using (var facadeList = new ListControlFacade())
            {
                using (var datasetItem = new DataSet())
                {
                    var itemBaseDataset = new BaseDataset();

                    //var umInfo = InterpriseHelper.GetItemUnitMeasure(item.ItemCode, item.UnitMeasureCode);
                    string query = string.Format("ItemCode = {0} AND (WarehouseCode = {1} or WarehouseCode IS NULL) AND UnitMeasureCode = {2}",
                                    DB.SQuote(item.ItemCode),
                                    DB.SQuote(ThisCustomer.WarehouseCode),
                                    DB.SQuote(item.UnitMeasureCode));

                    //deadlock
                    int nooftries = 0;
                    while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
                    {
                        try
                        {
                            facadeList.ReadSearchResultsData("SaleItemView", query, 1, false, string.Empty, ref itemBaseDataset, true);
                            break;
                        }
                        catch (SqlException ex)
                        {
                            if (ex.ErrorCode == 1205)
                            {
                                nooftries += 1;
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }

                    if (itemBaseDataset.Tables["SaleItemView"] != null &&
                        itemBaseDataset.Tables["SaleItemView"].Rows.Count > 0)
                    {
                        var itemRow = itemBaseDataset.Tables["SaleItemView"].Rows[0];
                        var dvSalesOrderItem = new DataView(_gatewaySalesOrderDataset.CustomerSalesOrderDetailView);
                        var lineItemRow = dvSalesOrderItem.AddNew();
                        
                        switch ((string)itemRow["ItemType"])
                        {
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_STOCK:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_NON_STOCK:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_SERVICE:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ELECTRONIC_DOWNLOAD:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ASSEMBLY:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CARD:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CERTIFICATE:
                                string error = string.Empty;
                                if (_facadeSalesOrder.AssignInventoryItem(itemRow, ref lineItemRow, ref error))
                                {
                                    lineItemRow["QuantityOrdered"] = item.m_Quantity;
                                    _facadeSalesOrder.AssignQuantityOrdered(lineItemRow, _facadeSalesOrder.TransactionType);
                                    bool byTotalQty = false;
                                    _facadeSalesOrder.SetSalesPrice(lineItemRow, ref byTotalQty);

                                    var isZeroPrice = AppLogic.IsZeroPrice(lineItemRow["ItemCode"].ToString());
                                    if (isZeroPrice)
                                    {
                                        lineItemRow["SalesPrice"] = decimal.Zero;
                                        lineItemRow["SalesPriceRate"] = decimal.Zero;
                                    }

                                    if (ServiceFactory.GetInstance<IAppConfigService>().UseWebStorePricing)
                                    {
                                        if (!isZeroPrice)
                                        {
                                            lineItemRow["SalesPriceRate"] = Convert.ToDecimal(item.UnitPrice);
                                        }
                                        _facadeSalesOrder.AssignDefaults(new object[][] {
	                                                new object[] {
		                                                _gatewaySalesOrderDataset.CustomerSalesOrderDetailView.SalesPriceColumn.ColumnName,
		                                                Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(_gatewaySalesOrderDataset.CustomerSalesOrderView[0].ExchangeRate, Convert.ToDecimal(item.Price), true, String.Empty, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.CostPrice)
	                                                },
	                                                new object[] {
		                                                _gatewaySalesOrderDataset.CustomerSalesOrderDetailView.PricingColumn.ColumnName,
		                                                InterpriseHelper.ConfigInstance.UserInfo.UserName
	                                                }}, ref lineItemRow);
                                        _facadeSalesOrder.ComputeKitItemsSalesPrice(lineItemRow.Row);
                                    }
                                    _facadeSalesOrder.Compute(lineItemRow.Row, _facadeSalesOrder.TransactionType);
                                }
                                break;

                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT:
                                if (item.CategoryCode != "White bag and sticker bundles" && item.CategoryCode != "Brown bag and sticker bundles")
                                {
                                    lineItemRow["QuantityOrdered"] = item.m_Quantity;
                                }
                                else
                                {
                                    lineItemRow["QuantityOrdered"] = item.m_Quantity * item.UnitMeasureQty;
                                }
                                //lineItemRow["QuantityOrdered"] = item.m_Quantity * item.UnitMeasureQty;
                                AssignKitItem(itemRow, lineItemRow, item.Id);

                                // NOTE:
                                //  call this function so that the quantity per kit would recompute
                                //  therefore have the correct computation for other unit measures like dozen
                                //  which takes into consideration the unit measure quantity
                                _facadeSalesOrder.AssignQuantityOrdered(lineItemRow, _facadeSalesOrder.TransactionType);
                                break;
                        }

                        lineItemRow["WebSiteCode"] = InterpriseHelper.ConfigInstance.WebSiteCode;
                        lineItemRow["VDPCustomisationCode_DEV004817"] = item.VDPCustomisationCode;
                        lineItemRow["DesignComments_C"] = item.DesignComments;
                        lineItemRow["DesignFilename_C"] = item.DesignFilename;
                        lineItemRow["DesignFile_C"] = item.DesignFile;
                        lineItemRow["VDPType_C"] = item.VDPType;
                        
                        // NOTE:
                        //  We only use text option for OrderOption items that has a textbox provided so they can enter their custom notes....
                        //  So we only assume here 2 things if it's not empty:
                        //      1. It was previously an Order option
                        //      2. A note has been specified by the customer
                        string itemTextOption = item.m_TextOption;
                        if (!string.IsNullOrEmpty(itemTextOption))
                        {
                            lineItemRow["ItemDescription"] = itemTextOption;
                        }

                        item.AssociatedLineItemRow = lineItemRow.Row as SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow;
                    }
                    // Explicitly dereference here
                    itemBaseDataset.Dispose();
                    itemBaseDataset = null;
                }
            }
        }

        private void AddLineItem(string itemCode, string UMCode, DataRow whRow, decimal qtyOrdered, string itemDescription, Guid itemGUID)
        {
            using (var facadeList = new ListControlFacade())
            {
                using (var datasetItem = new DataSet())
                {
                    var itemBaseDataset = new BaseDataset();

                    var umInfo = InterpriseHelper.GetItemUnitMeasure(itemCode, UMCode);
                    string query = string.Format("ItemCode = {0} AND (WarehouseCode = {1} or WarehouseCode IS NULL) AND UnitMeasureCode = {2}",
                                    DB.SQuote(itemCode),
                                    DB.SQuote(whRow["WarehouseCode"].ToString()),
                                    DB.SQuote(umInfo.Code));

                    //deadlock
                    int nooftries = 0;
                    while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
                    {
                        try
                        {
                            facadeList.ReadSearchResultsData("SaleItemView", query, 1, false, string.Empty, ref itemBaseDataset, true);
                            break;
                        }
                        catch (SqlException ex)
                        {
                            if (ex.ErrorCode == 1205)
                            {
                                nooftries += 1;
                            }
                            else
                            {
                                throw;
                            }
                        }
                    }

                    if (itemBaseDataset.Tables["SaleItemView"] != null &&
                        itemBaseDataset.Tables["SaleItemView"].Rows.Count > 0)
                    {
                        var itemRow = itemBaseDataset.Tables["SaleItemView"].Rows[0];
                        var dvSalesOrderItem = new DataView(_gatewaySalesOrderDataset.CustomerSalesOrderDetailView);
                        var lineItemRow = dvSalesOrderItem.AddNew();

                        switch ((string)itemRow["ItemType"])
                        {
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_STOCK:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_NON_STOCK:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_SERVICE:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ELECTRONIC_DOWNLOAD:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ASSEMBLY:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CARD:
                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CERTIFICATE:
                                string error = string.Empty;
                                if (_facadeSalesOrder.AssignInventoryItem(itemRow, ref lineItemRow, ref error))
                                {
                                    lineItemRow["QuantityOrdered"] = qtyOrdered;
                                    _facadeSalesOrder.AssignQuantityOrdered(lineItemRow, _facadeSalesOrder.TransactionType);
                                    _facadeSalesOrder.AssignLineItemWarehouse(lineItemRow, whRow);
                                    bool byTotalQty = false;
                                    _facadeSalesOrder.SetSalesPrice(lineItemRow, ref byTotalQty);
                                    _facadeSalesOrder.Compute(lineItemRow.Row, _facadeSalesOrder.TransactionType);
                                }
                                break;

                            case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT:
                                lineItemRow["QuantityOrdered"] = qtyOrdered;
                                AssignKitItem(itemRow, lineItemRow, itemGUID);
                                _facadeSalesOrder.AssignLineItemWarehouse(lineItemRow, whRow);

                                // NOTE:
                                //  call this function so that the quantity per kit would recompute
                                //  therefore have the correct computation for other unit measures like dozen
                                //  which takes into consideration the unit measure quantity
                                _facadeSalesOrder.AssignQuantityOrdered(lineItemRow, _facadeSalesOrder.TransactionType);
                                break;
                        }

                        lineItemRow["WebSiteCode"] = InterpriseHelper.ConfigInstance.WebSiteCode;
                        // NOTE:
                        //  We only use text option for OrderOption items that has a textbox provided so they can enter their custom notes....
                        //  So we only assume here 2 things if it's not empty:
                        //      1. It was previously an Order option
                        //      2. A note has been specified by the customer                        
                        if (!string.IsNullOrEmpty(itemDescription))
                        {
                            lineItemRow["ItemDescription"] = itemDescription;
                        }
                    }
                    // Explicitly dereference here
                    itemBaseDataset.Dispose();
                    itemBaseDataset = null;
                }
            }
        }

        public void BuildSalesOrderDetails(bool computeFreight, bool computeTax, string couponCode = "", bool isForCartDisplay = false)
        {
            if (IsEmpty())
            {
                return;
            }


            int retries = 3;

            for (int ctr = 0; ctr < retries; ctr++)
            {
                try
                {
                    
                    if (isForCartDisplay)
                    {
                        // do build cart using per line item retrival using connector    
                        //follow BuildSalesOrderDetailsCore process
                        BuildCartDetailsCore(computeFreight, couponCode);
                    }
                    else
                    {
                        _gatewaySalesOrderDataset = new SalesOrderDatasetGateway();
                        _facadeSalesOrder = new SalesOrderFacade(_gatewaySalesOrderDataset);
                        BuildSalesOrderDetailsCore(computeFreight, computeTax, couponCode);
                    }

                    break;
                }
                catch
                {
                    if (_gatewaySalesOrderDataset != null) { _gatewaySalesOrderDataset.Dispose(); }
                    if (_facadeSalesOrder != null) { _facadeSalesOrder.Dispose(); }

                    _gatewaySalesOrderDataset = null;
                    _facadeSalesOrder = null;

                    // if this is the last retry, bubble up the error
                    if (ctr + 1 == retries)
                    {
                        throw;
                    }
                }
            }
        }

        private void BuildCartDetailsCore(bool computeFreight, string couponCode = "")
        {
            //For safety double check if couponCode is not supplied.
            if (couponCode.IsNullOrEmptyTrimmed()) { HasCoupon(ref couponCode); }
            // get customer exchangerate
            decimal getExchangeRate = Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(ThisCustomer.CurrencyCode);

            string shipToCode = String.Empty;
            if (ThisCustomer.IsRegistered && AppLogic.AppConfigBool("AllowMultipleShippingAddressPerOrder"))
            {
                shipToCode = (!this.FirstItemShippingAddressID().IsNullOrEmptyTrimmed()) ? this.FirstItemShippingAddressID() : ThisCustomer.PrimaryShippingAddressID;
            }
            else
            {
                shipToCode = (ThisCustomer.IsNotRegistered) ? ThisCustomer.AnonymousShipToCode : ThisCustomer.PrimaryShippingAddressID;
            }

            // Check Shippable components
            if (!this.HasShippableComponents())
            {
                this.MakeShippingNotRequired(false);
            }
            // set coupon
            if (!couponCode.IsNullOrEmptyTrimmed())
            {
                ApplyCoupon(couponCode);
            }
            else
            {
                if (HasCoupon(ref couponCode)) ApplyCoupon(couponCode);
            }

            // compute shipping
            bool doesCouponIncludeFreeShipping = CouponIncludesFreeShipping(couponCode);
            ComputeFreightCharge(false, getExchangeRate);

            // perform new allocation and reservation procedure
            ProcessAllocationAndReservation();

        }

        private void BuildSalesOrderDetailsCore(bool computeFreight, bool computeTax, string couponCode = "")
        {

            //For safety double check if couponCode is not supplied.
            if (couponCode.IsNullOrEmptyTrimmed()) { HasCoupon(ref couponCode); }

            // use Interprise.Framework.Base.Shared.Enum.TransactionType.SalesOrder to show the reservation value.
            _facadeSalesOrder.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.SalesOrder;
            decimal getExchangeRate = _facadeSalesOrder.GetExchangerate(ThisCustomer.CurrencyCode);

            string shipToCode = String.Empty;
            if (ThisCustomer.IsRegistered && AppLogic.AppConfigBool("AllowMultipleShippingAddressPerOrder"))
            {
                shipToCode = (!this.FirstItemShippingAddressID().IsNullOrEmptyTrimmed()) ? this.FirstItemShippingAddressID() : ThisCustomer.PrimaryShippingAddressID;
            }
            else
            {
                shipToCode = (ThisCustomer.IsNotRegistered) ? ThisCustomer.AnonymousShipToCode : ThisCustomer.PrimaryShippingAddressID;
            }

            var shipToDataset = new DataSet();
            var facadeListControl = new ListControlFacade();
            var shipToBaseDataset = new BaseDataset();

            //deadlock
            int nooftries = 0;
            while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
            {
                try
                {
                    shipToDataset = facadeListControl.ReadSearchResultsData("CustomerShipToView",
                                                                            string.Format("ShipToCode = {0}", DB.SQuote(shipToCode)),
                                                                            1,
                                                                            false,
                                                                            string.Empty,
                                                                            ref shipToBaseDataset, true);
                    break;
                }
                catch (SqlException ex)
                {
                    if (ex.ErrorCode == 1205)
                    {
                        nooftries += 1;
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            if (shipToBaseDataset.Tables["CustomerShipToView"].Rows.Count == 0) return;

            var rowShipTo = shipToBaseDataset.Tables["CustomerShipToView"].Rows[0];
            string contactCode = String.Empty;
            string contactFullName = String.Empty;

            if (ThisCustomer.IsRegistered)
            {
                // retrieve the contact info
                contactCode = ThisCustomer.ContactCode;
                contactFullName = ThisCustomer.ContactFullName;
            }


            /***************************************************************
            *  1. Make Sales Order
            * *************************************************************/
            string msg = String.Empty;

            if (_facadeSalesOrder.CreateSalesOrder(_facadeSalesOrder.TransactionType,
                                            rowShipTo,
                                            contactCode,
                                            contactFullName,
                                            null,
                                            ref msg,
                                            String.Empty,
                                            String.Empty, null, null, true, true, true))
            {
                // apply the notes...
                if (ThisCustomer.IsRegistered)
                {
                    if (!ThisCustomer.ThisCustomerSession["paypalfrom"].IsNullOrEmptyTrimmed())
                    {
                        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].InternalNotes = "Payment Type: Paypal " + "\r\n" + ThisCustomer.ThisCustomerSession["notesFromPayPal"];
                    }
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].PublicNotes = ThisCustomer.Notes;
                }
                else
                {
                    if (!ThisCustomer.ThisCustomerSession["paypalfrom"].IsNullOrEmptyTrimmed())
                    {
                        if (!ThisCustomer.ThisCustomerSession["notesFromPayPal"].IsNullOrEmptyTrimmed())
                        {
                            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].InternalNotes = "Payment Type: Paypal " + "\r\n" + ThisCustomer.ThisCustomerSession["notesFromPayPal"];
                        }
                        else
                        {
                            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].InternalNotes = ThisCustomer.ThisCustomerSession["anonymousCustomerNote"];
                        }
                    }
                    else
                    {
                        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].InternalNotes = ThisCustomer.ThisCustomerSession["anonymousCustomerNote"];
                    }

                    if (!ThisCustomer.ThisCustomerSession["anonymousCompany"].IsNullOrEmptyTrimmed())
                    {
                        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].InternalNotes += "\r\n" + "Anonymous Company Name: " + ThisCustomer.ThisCustomerSession["anonymousCompany"];
                    }
                }

                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].WebSiteCode = InterpriseHelper.ConfigInstance.WebSiteCode;

                if (ThisCustomer.IsRegistered)
                {
                    //override default billing address from primary billing address
                    var registeredCustomerBillingAddress = ThisCustomer.PrimaryBillingAddress;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToName = registeredCustomerBillingAddress.CardName;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToAddress = registeredCustomerBillingAddress.Address1;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToCity = registeredCustomerBillingAddress.City;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToState = registeredCustomerBillingAddress.State;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToPostalCode = registeredCustomerBillingAddress.PostalCode;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToCounty = registeredCustomerBillingAddress.County;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToCountry = registeredCustomerBillingAddress.Country;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToPhone = registeredCustomerBillingAddress.Phone;
                }
                else
                {
                    //Anonymous Customer Shipping Address
                    var anonymousShippingAddress = ThisCustomer.PrimaryShippingAddress;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToName = ThisCustomer.ThisCustomerSession["anonymousCompany"];
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToName = ThisCustomer.ThisCustomerSession["anonymousCompany"];
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToAddress = anonymousShippingAddress.Address1;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToCity = anonymousShippingAddress.City;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToState = anonymousShippingAddress.State;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToPostalCode = anonymousShippingAddress.PostalCode;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToCounty = anonymousShippingAddress.County;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToCountry = anonymousShippingAddress.Country;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ShipToEmail = anonymousShippingAddress.EMail;
                }

                this.CartItems.ForEach(item => AddLineItem(item));

                // Compute Shipping
                if (!this.HasShippableComponents())
                {
                    this.MakeShippingNotRequired(false);
                }

                bool doesCouponIncludeFreeShipping = CouponIncludesFreeShipping(couponCode);
                ComputeFreightCharge(!computeFreight || doesCouponIncludeFreeShipping, getExchangeRate);

                if (!computeTax)
                {
                    foreach (var lineItemRow in _gatewaySalesOrderDataset.CustomerSalesOrderDetailView)
                    {
                        lineItemRow.Tax = string.Empty;
                        lineItemRow.TaxCode = string.Empty;
                        lineItemRow.SalesTaxAmount = decimal.Zero;
                        lineItemRow.SalesTaxAmountRate = decimal.Zero;
                    }
                    // zero out the tax so it won't alter the computation of the coupon discount
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].FreightTaxCode = string.Empty;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].FreightTaxScheme = string.Empty;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].OtherTaxCode = string.Empty;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].OtherTaxScheme = string.Empty;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].Tax = decimal.Zero;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].TaxRate = decimal.Zero;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].OtherTax = decimal.Zero;
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].OtherTaxRate = decimal.Zero;
                }

                if (!computeFreight && !computeTax)
                {
                    _facadeSalesOrder.ComputeTotals(
                        _gatewaySalesOrderDataset.CustomerSalesOrderView[0],
                        _gatewaySalesOrderDataset.CustomerSalesOrderDetailView,
                        true,
                        false
                    );
                }

                // if instantiating this object was caused by a postback on entering coupon code
                // we must re-apply the coupon discount...

                if (!couponCode.IsNullOrEmptyTrimmed())
                {
                    ApplyCoupon(couponCode);
                }
                else
                {
                    if (HasCoupon(ref couponCode)) ApplyCoupon(couponCode);
                }

                // recompute just to be sure, esp in real time shipping..
                // the freight my be zeroed out once we set a non-existent shipping method
                ComputeFreightCharge(!computeFreight || doesCouponIncludeFreeShipping, getExchangeRate);

                // perform new allocation and reservation procedure
                ProcessAllocationAndReservation();
            }
        }

        private void ComputeShipping()
        {
            // Compute Shipping
            if (!this.HasShippableComponents())
            {
                this.MakeShippingNotRequired(false);
            }
        }

        private void ApplyCoupon()
        {
            /***************************************************************
            *  1.b. Apply the coupon, if any...
            * *************************************************************/
            // NOTE : 
            //  Once we reach here, we assume that the coupon( if any ) is valid

            bool hasCoupon = false;
            string couponCode = string.Empty;
            string customerCode = CommonLogic.IIF(ThisCustomer.IsRegistered, ThisCustomer.CustomerCode, ThisCustomer.CustomerID);
            // retrieve the coupon from the customer table
            string couponQuery = string.Format("SELECT CouponCode FROM Customer with (NOLOCK) WHERE CustomerCode = {0}", DB.SQuote(customerCode));

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, couponQuery))
                {
                    if (reader.Read())
                    {
                        hasCoupon = reader["CouponCode"] != null &&
                                    reader["CouponCode"] != DBNull.Value &&
                                    !string.IsNullOrEmpty((string)reader["CouponCode"]);

                        if (hasCoupon)
                            couponCode = (string)reader["CouponCode"];
                    }
                }
            }

            if (hasCoupon)
            {
                // apply...
                var couponDataset = new DataSet();
                var facadeList = new ListControlFacade();
                var baseCouponDataset = new BaseDataset();

                //deadlock
                int nooftries = 0;
                while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
                {
                    try
                    {
                        facadeList.ReadSearchResultsData("CustomerCouponView", string.Format("CouponCode = {0}", DB.SQuote(couponCode)), 1, false, string.Empty, ref baseCouponDataset, true);
                        break;
                    }
                    catch (SqlException ex)
                    {
                        if (ex.ErrorCode == 1205)
                        {
                            nooftries += 1;
                        }
                        else
                        {
                            throw;
                        }
                    }
                }

                if (baseCouponDataset.Tables["CustomerCouponView"] != null &&
                    baseCouponDataset.Tables["CustomerCouponView"].Rows.Count >= 1)
                {
                    var couponRow = baseCouponDataset.Tables["CustomerCouponView"].Rows[0];
                    _facadeSalesOrder.AssignCoupon(couponRow, _gatewaySalesOrderDataset.CustomerSalesOrderView[0]);
                }
            }
        }

        private void TagOrder(string salesOrderCode)
        {
            string itemCode = FirstItem().ItemCode;
            // mark this order
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode = salesOrderCode;

            if (ThisCustomer.PaymentTermCode.ToUpper() == "PURCHASE ORDER")
            {
                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].POCode = CommonLogic.IIF(ThisCustomer.ThisCustomerSession.Session("PONumber") != "", ThisCustomer.ThisCustomerSession.Session("PONumber") + "/Web Order", "Web Order");
                ThisCustomer.ThisCustomerSession.ClearVal("PONumber");
            }
            else
            {
                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].POCode = "Web Order";
            }
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SourceCode = "Internet";
            _gatewaySalesOrderDataset.CustomerSalesOrderView[0]["CostCenter_DEV004817"] = AppLogic.SetCostCenter(itemCode, true);

            if (ThisCustomer.IsRegistered)
            {
                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ContactCode = ThisCustomer.ContactCode;
            }
        }

        private void AssignAffiliate()
        {
            /***************************************************************
           *  1.c. Assign the affiliate(if any)
           * *************************************************************/
            // Note :
            //  If this is the first affiliate/sales rep used by this customer
            //  We will assign this affiliate to always be used by this customer
            //  So that every order will be tagged for this affiliate.
            //  Otherwise if the customer already has a affiliate set up
            //  and the current affiliate is not the default(Which is the very first affiliate assigned to this customer)
            //  We will tag this affiliate only for this order
            bool isAffiliateDefinedAndValid = false;
            bool affiliateIsCustomerDefault = false;
            bool customerHasDefaultAffiliate = false;

            // NOTE :
            //  Affiliate ID from customer is always retrieved from the cookie
            string affiliateId = ThisCustomer.AffiliateID;

            //  let's just validate if it has an affiliate regardless if the current cached affiliate
            //  is the customer's default or not since if it's a new, tagging for this affiliate
            //  will only occur on this transaction and succeeding transactions will always associate
            //  to the customer default.
            isAffiliateDefinedAndValid = !string.IsNullOrEmpty(affiliateId) && InterpriseSuiteEcommerceCommon.InterpriseIntegration.Affiliate.IsValid(affiliateId);

            customerHasDefaultAffiliate = _gatewaySalesOrderDataset.CustomerSalesRepCommissionView.Count > 0;

            if (customerHasDefaultAffiliate)
            {
                if (!isAffiliateDefinedAndValid)
                {
                    affiliateIsCustomerDefault = true;
                }
                else
                {
                    affiliateIsCustomerDefault = (affiliateId == _gatewaySalesOrderDataset.CustomerSalesRepCommissionView[0].SalesRepGroupCode);
                }
            }
            else if (isAffiliateDefinedAndValid)
            {
                affiliateIsCustomerDefault = false;
            }

            if (isAffiliateDefinedAndValid && !affiliateIsCustomerDefault)
            {
                if (_gatewaySalesOrderDataset.CustomerSalesRepCommissionView.Count > 0)
                {
                    _gatewaySalesOrderDataset.CustomerSalesRepCommissionView.Clear();
                    _facadeSalesOrder.ClearCommission();
                }

                // retrieve the datarow from the database
                using (var facadeSalesRepList = new ListControlFacade())
                {
                    var baseSalesRepDataset = new BaseDataset();
                    var salesRepDataset = new DataSet();

                    facadeSalesRepList.ReadSearchResultsData("CustomerSalesRepView",
                        string.Format("ApplyTo = 'Sales' AND SalesRepGroupCode = {0}", DB.SQuote(affiliateId)),
                        1,
                        false,
                        string.Empty,
                        ref baseSalesRepDataset,
                        true);

                    if (baseSalesRepDataset.Tables["CustomerSalesRepView"] != null &&
                        baseSalesRepDataset.Tables["CustomerSalesRepView"].Rows.Count >= 1)
                    {
                        var salesRepRow = baseSalesRepDataset.Tables["CustomerSalesRepView"].Rows[0];
                        var salesRepCommisionRow = (new DataView(_gatewaySalesOrderDataset.CustomerSalesRepCommissionView)).AddNew();
                        _facadeSalesOrder.AssignSalesRepCommission(salesRepRow, salesRepCommisionRow);

                        // assign the contact...
                        var affliateInfo = InterpriseIntegration.Affiliate.FindById(affiliateId);
                        salesRepCommisionRow.BeginEdit();
                        salesRepCommisionRow["ContactCode"] = affliateInfo.ContactCode;
                        salesRepCommisionRow["ContactFullName"] = affliateInfo.FullName;
                        salesRepCommisionRow.EndEdit();
                    }
                    // Explicitly dereference
                    baseSalesRepDataset.Dispose();
                    salesRepDataset.Dispose();

                    baseSalesRepDataset = null;
                    salesRepDataset = null;
                }
            }

            if (isAffiliateDefinedAndValid && !affiliateIsCustomerDefault)
            {
                //  Now, if the customer doesn't have an affiliate set up
                //  make this first one the default
                DB.ExecuteSQL("UPDATE Customer SET SalesRepGroupCode = {0} WHERE CustomerCode = {1}",
                    DB.SQuote(affiliateId),
                    DB.SQuote(ThisCustomer.CustomerCode));
            }
        }

        private void UpdateBillingAndShippingInformation(Address billingAddress, Address shippingAddress, Address giftRegistryAddress = null)
        {
            //***************************************************************
            //  Update the Bill To and Ship To Information
            //  Incase the customer has multiple billto's and shipto's
            //***************************************************************
            var salesOrderRow = _gatewaySalesOrderDataset.CustomerSalesOrderView[0];
            // set the payment term based on the customer...
            salesOrderRow.PaymentTermCode = ThisCustomer.PaymentTermCode;
            // billing information               
            salesOrderRow.BillToAddress = billingAddress.Address1;
            salesOrderRow.BillToCity = billingAddress.City;
            salesOrderRow.BillToCounty = billingAddress.County;
            salesOrderRow.BillToState = billingAddress.State;
            salesOrderRow.BillToPostalCode = billingAddress.PostalCode;
            salesOrderRow.BillToCountry = billingAddress.Country;
            salesOrderRow.BillToPhone = billingAddress.Phone;

            if (!billingAddress.Plus4.IsNullOrEmptyTrimmed())
            {
                salesOrderRow.BillToPlus4 = Convert.ToInt32(billingAddress.Plus4);
            }

            if (giftRegistryAddress != null)
            {
                salesOrderRow.ShipToName = giftRegistryAddress.Name;
                salesOrderRow.ShipToAddress = giftRegistryAddress.Address1;
                salesOrderRow.ShipToCity = giftRegistryAddress.City;
                salesOrderRow.ShipToCounty = giftRegistryAddress.County;
                salesOrderRow.ShipToState = giftRegistryAddress.State;
                salesOrderRow.ShipToPostalCode = giftRegistryAddress.PostalCode;
                salesOrderRow.ShipToCountry = giftRegistryAddress.Country;
                salesOrderRow.ShipToPhone = giftRegistryAddress.Phone;
            }
            else
            {
                // shipping information
                salesOrderRow.ShipToName = shippingAddress.Name;
                salesOrderRow.ShipToAddress = shippingAddress.Address1;
                salesOrderRow.ShipToCity = shippingAddress.City;
                salesOrderRow.ShipToCounty = shippingAddress.County;
                salesOrderRow.ShipToState = shippingAddress.State;
                salesOrderRow.ShipToPostalCode = shippingAddress.PostalCode;
                salesOrderRow.ShipToCountry = shippingAddress.Country;
                salesOrderRow.ShipToPhone = shippingAddress.Phone;
            }
            if (CommonLogic.GetWebsiteType() == WebsiteType.Smartbag)
            {
                if (ThisCustomer.IsRegistered)
                {
                    salesOrderRow.BillToName = billingAddress.ThisCustomer.CompanyName;
                    salesOrderRow.ShipToName = ThisCustomer.CompanyName;
                }
                else
                {
                    salesOrderRow.BillToName = ThisCustomer.ThisCustomerSession["anonymousCompany"];
                    salesOrderRow.ShipToName = ThisCustomer.ThisCustomerSession["anonymousCompany"];
                }
            }
            else
            {
                salesOrderRow.BillToName = billingAddress.Name;
            }

            if (!shippingAddress.Plus4.IsNullOrEmptyTrimmed())
            {
                salesOrderRow.ShipToPlus4 = Convert.ToInt32(shippingAddress.Plus4);
            }

            if (ThisCustomer.IsNotRegistered)
            {
                if (ThisCustomer.ThisCustomerSession["paypalfrom"].IsNullOrEmptyTrimmed())
                {
                    salesOrderRow.ShippingMethodGroup = shippingAddress.ShippingMethodGroup;
                    salesOrderRow.PaymentTermGroup = shippingAddress.PaymentTermGroup;
                }
                else
                {
                    salesOrderRow.ShippingMethodGroup = ThisCustomer.PrimaryShippingAddress.ShippingMethodGroup;
                    salesOrderRow.PaymentTermGroup = ThisCustomer.PrimaryShippingAddress.PaymentTermGroup;
                }
            }
        }

        private void SaveSalesOrder()
        {
            //Retry saving if concurrency error was encountered otherwise, simply throw the exception.
            int nooftries = 0;
            while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
            {
                try
                {
                    string[] relatedTables = _facadeSalesOrder.get_RelatedTables(_facadeSalesOrder.TransactionType);
                    string[][] commands = _facadeSalesOrder.CreateParameterSet(relatedTables);

                    //Set this property to skip stock deallocation. This should fix the deadlock issue.
                    var soDataset = (SalesOrderDatasetGateway)_facadeSalesOrder.CurrentDataset;
                    foreach (DataRow drow in soDataset.CustomerSalesOrderDetailView.Rows)
                    {
                        drow.BeginEdit();

                        if (_facadeSalesOrder.TransactionType == Interprise.Framework.Base.Shared.Enum.TransactionType.Quote)
                        {
                            drow[soDataset.CustomerSalesOrderDetailView.SourceDocumentTypeColumn.ColumnName] = Interprise.Framework.Base.Shared.Enum.TransactionType.Quote.ToString();
                        }

                        if (System.Convert.ToDecimal(Interprise.Framework.Base.Shared.Common.IsNull(drow["QuantityReserved"], 0)) > 0)
                        {
                            drow[soDataset.CustomerSalesOrderDetailView.DueDateColumn.ColumnName] = _facadeSalesOrder.LatestShippingDate;
                        }
                        else
                        {
                            drow[soDataset.CustomerSalesOrderDetailView.DueDateColumn.ColumnName] = DateTime.Now;
                        }

                        //Set all price to zero if Cost Center is APG
                        if (ThisCustomer.CostCenter == "Smartbag-PREMIUM APG" || ThisCustomer.CostCenter == "Smartbag-PREMIUM DION LEE" || ThisCustomer.CostCenter == "Smartbag-PREMIUM CUE")
                        {
                            drow[soDataset.CustomerSalesOrderDetailView.SalesPriceColumn.ColumnName] = 0;
                            drow[soDataset.CustomerSalesOrderDetailView.SalesPriceRateColumn.ColumnName] = 0;
                            drow[soDataset.CustomerSalesOrderDetailView.NetPriceColumn.ColumnName] = 0;
                            drow[soDataset.CustomerSalesOrderDetailView.NetPriceRateColumn.ColumnName] = 0;
                            drow[soDataset.CustomerSalesOrderDetailView.ExtPriceColumn.ColumnName] = 0;
                            drow[soDataset.CustomerSalesOrderDetailView.ExtPriceRateColumn.ColumnName] = 0;
                            drow[soDataset.CustomerSalesOrderDetailView.SalesTaxAmountColumn.ColumnName] = 0;
                            drow[soDataset.CustomerSalesOrderDetailView.SalesTaxAmountRateColumn.ColumnName] = 0;
                        }
                        drow.EndEdit();
                    }

                    //Set all price to zero if Cost Center is APG
                    if (ThisCustomer.CostCenter == "Smartbag-PREMIUM APG" || ThisCustomer.CostCenter == "Smartbag-PREMIUM DION LEE" || ThisCustomer.CostCenter == "Smartbag-PREMIUM CUE")
                    {
                        soDataset.CustomerSalesOrderView.BeginInit();
                        soDataset.CustomerSalesOrderView[0].SubTotal = 0;
                        soDataset.CustomerSalesOrderView[0].SubTotalRate = 0;
                        soDataset.CustomerSalesOrderView[0].Freight = 0;
                        soDataset.CustomerSalesOrderView[0].FreightRate = 0;
                        soDataset.CustomerSalesOrderView[0]["Freight_C"] = 0;
                        soDataset.CustomerSalesOrderView[0].FreightTax = 0;
                        soDataset.CustomerSalesOrderView[0].FreightTaxRate = 0;
                        soDataset.CustomerSalesOrderView[0].Tax = 0;
                        soDataset.CustomerSalesOrderView[0].TaxRate = 0;
                        soDataset.CustomerSalesOrderView[0].Total = 0;
                        soDataset.CustomerSalesOrderView[0].TotalRate = 0;
                        soDataset.CustomerSalesOrderView[0].Balance = 0;
                        soDataset.CustomerSalesOrderView[0].BalanceRate = 0;
                        soDataset.CustomerSalesOrderView[0].TaxGroup1 = 0;
                        soDataset.CustomerSalesOrderView[0].TaxGroup1Rate = 0;
                        soDataset.CustomerSalesOrderView[0].TaxGroup2 = 0;
                        soDataset.CustomerSalesOrderView[0].TaxGroup2Rate = 0;
                        soDataset.CustomerSalesOrderView[0].TaxGroup3 = 0;
                        soDataset.CustomerSalesOrderView[0].TaxGroup3Rate = 0;
                        soDataset.CustomerSalesOrderView[0].AmountPaid = 0;
                        soDataset.CustomerSalesOrderView[0].AmountPaidRate = 0;
                        soDataset.CustomerSalesOrderView.EndInit();
                    }

                    //Check if transaction is Quote Then Clear reservation
                    if (_facadeSalesOrder.TransactionType == Interprise.Framework.Base.Shared.Enum.TransactionType.Quote)
                    {
                        //Change Header Type to Quote
                        foreach (DataRow drow in soDataset.CustomerSalesOrderView.Rows)
                        {
                            drow.BeginEdit();
                            drow[soDataset.CustomerSalesOrderView.TypeColumn.ColumnName] = Interprise.Framework.Base.Shared.Enum.TransactionType.Quote.ToString();
                            drow.EndEdit();
                        }

                        _facadeSalesOrder.IsSkipStockDeallocation = false;
                        _facadeSalesOrder.ClearTransactionReservation(true);
                    }
                    else
                    {
                        _facadeSalesOrder.IsSkipStockDeallocation = true;
                    }

                    _facadeSalesOrder.UpdateDataSet(commands, _facadeSalesOrder.TransactionType, string.Empty, false);
                    SetTaxZero(soDataset.CustomerSalesOrderView[0].SalesOrderCode);
                    break;
                }
                catch (DBConcurrencyException)
                {
                    nooftries += 1;
                    SaveSalesOrderAndAllocate(_facadeSalesOrder.CurrentDataset.Tables["CustomerSalesOrderView"].Rows[0]["SalesOrderCode"].ToString());
                }
                catch (Exception)
                {
                    //???
                    throw;
                }
            }
        }

        //Created for CUE to set tax to zero since CB API always overwrite the zero value to it needs to manually be set
        private void SetTaxZero(string SalesOrderCode)
        {
            if (ThisCustomer.CostCenter == "Smartbag-PREMIUM APG" || ThisCustomer.CostCenter == "Smartbag-PREMIUM DION LEE" || ThisCustomer.CostCenter == "Smartbag-PREMIUM CUE")
            {
                DB.ExecuteSQL("UPDATE CustomerSalesOrder SET Tax = {0}, TaxRate = {0}, Total = {0}, TotalRate = {0}, Balance = {0}, BalanceRate = {0} WHERE SalesOrderCode = {1}", "0", DB.SQuote(SalesOrderCode));
            }
        }
        private void SaveSalesOrderAndAllocate(string salesOrderCode)
        {
            //Only save after allocation/reservation has completed
            if (ProcessAllocationAndReservation()) { SaveSalesOrder(); }
        }

        private bool ProcessAllocationAndReservation()
        {
            try
            {
                if (!this.IsSalesOrderDetailBuilt)
                {return true;}

                //get so dataset instance
                var soDataset = (SalesOrderDatasetGateway)_facadeSalesOrder.CurrentDataset;

                // Clear Reservation transaction - if sales order code equal to temporary document code
                _facadeSalesOrder.ClearTransactionReservation(soDataset.CustomerSalesOrderView[0].SalesOrderCode.ToString() == Interprise.Framework.Base.Shared.Const.TEMPORARY_DOCUMENTCODE);

                //Call for INITIAL allocation of items on the order
                _facadeSalesOrder.AllocateStock();

                //Save allocated qty to ecommerce shopping cart
                StoreAllocatedQuantities(soDataset);

                //if ShowInventoryFromAllWarehouses == true  
                if (System.Convert.ToBoolean(AppLogic.AppConfig("ShowInventoryFromAllWarehouses")))
                {
                    if (soDataset.CustomerSalesOrderView[0].SalesOrderCode.ToString() == Interprise.Framework.Base.Shared.Const.TEMPORARY_DOCUMENTCODE)
                    {
                        ProcessAllocationAndReservationAllWH();
                    }
                    //else { ReInitializeReservation(soDataset, false); }
                    ReInitializeReservation(soDataset, false);
                }
                else
                {
                    //Modify each due date in line item 
                    foreach (DataRow drow in soDataset.CustomerSalesOrderDetailView.Rows)
                    {
                        drow.BeginEdit();
                        drow[soDataset.CustomerSalesOrderDetailView.DueDateColumn.ColumnName] = Interprise.Framework.Base.Shared.Const.MAX_SMALLDATETIME;
                        drow.EndEdit();
                    }

                    //Deletes currently stored reservation information to make way for an updated one
                    //Also modifies the TransactionCode of the reservation table to match that of the current sales order code.
                    ReInitializeReservation(soDataset);

                    foreach (SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow drow in soDataset.CustomerSalesOrderDetailView.Rows)
                    {
                        //begin qty reservation
                        _facadeSalesOrder.LoadItemReservation(drow);
                        _facadeSalesOrder.ReserveStock(drow);
                        //store reservation data
                        if (System.Convert.ToDecimal(Interprise.Framework.Base.Shared.Common.IsNull(drow["QuantityReserved"], 0)) > 0)
                        {
                            if (drow.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                            {
                                StoreReservedKitQuantity(drow);
                            }
                            else
                            {
                                StoreReservedQuantity(drow.ItemCode);
                            }
                        }
                    }
                }


                //Modify shipping date
                soDataset.CustomerSalesOrderView[0].BeginEdit();
                if (HasAllocatedQuantity())
                {
                    //Cart has allocated quantities, return shipping date to the current date...
                    soDataset.CustomerSalesOrderView[0][soDataset.CustomerSalesOrderView.ShippingDateColumn.ColumnName] = DateTime.Now;
                }
                else
                {
                    //Remodify ship date to retrieve the latest possible shipping date based on the reservation dates...
                    soDataset.CustomerSalesOrderView[0][soDataset.CustomerSalesOrderView.ShippingDateColumn.ColumnName] = _facadeSalesOrder.LatestShippingDate;
                }
                soDataset.CustomerSalesOrderView[0].EndEdit();

                //object disposal/dereference
                soDataset.Dispose();
                soDataset = null;

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private void ProcessAllocationAndReservationAllWH()
        {
            var soDataset = (SalesOrderDatasetGateway)_facadeSalesOrder.CurrentDataset;

            //retrieve available warehouses
            DataTable dtWarehouse = GetWarehouse();

            //START ALLOCATION CODE ------------------------------------------------------

            //note original cart item count
            int rowCount = this.CartItems.Count;
            decimal qtyToReserve = 0;
            Object[][] reserveInfo = new Object[this.CartItems.Count][];

            //NOTE: we use "for" instead of "foreach" to avoid infinite looping since we are adding new rows to the detail table...
            //check rows if there are line items that have unallocated/unreserved quantities
            for (int rowIndex = 0; rowIndex <= rowCount - 1; rowIndex++)
            {
                var drow = (SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow)soDataset.CustomerSalesOrderDetailView.Rows[rowIndex];

                //Get cart item GUID
                var itemGuid = new Guid();
                var cartItem = this.CartItems.FirstOrDefault(item => item.ItemCode == drow.ItemCode);
                if (cartItem != null) { itemGuid = cartItem.Id; }

                decimal qtyOrigOrd = drow.QuantityOrdered; //original qty order for the line item
                decimal qtyAlloc = drow.QuantityAllocated; //qty allocated (so far) for the line item

                //filter the list of warehouses available (excluding the default) from the active warehouses
                string[] whFiltered = GetWarehouseFiltered(dtWarehouse, drow.WarehouseCode);

                //allocate
                if (qtyOrigOrd > qtyAlloc)
                {
                    //loop warehouses
                    for (int whCtr = 0; whCtr <= whFiltered.Length - 1; whCtr++)
                    {
                        DataRow[] whRow = dtWarehouse.Select(string.Format("WarehouseCode = '{0}'", whFiltered[whCtr].ToString()));

                        //Add as a new order line item with new warehouse
                        AddLineItem(drow.ItemCode, drow.UnitMeasureCode, whRow[0], qtyOrigOrd - drow.QuantityAllocated, drow.ItemDescription, itemGuid);
                        var newRow = (SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow)soDataset.CustomerSalesOrderDetailView.Rows[soDataset.CustomerSalesOrderDetailView.Rows.Count - 1];

                        //reallocate for new qty with diff wh
                        _facadeSalesOrder.AllocateStock(newRow);

                        //reassign row allocated for assessment...
                        //always assign the last row as this is the location of the newly added row (Count - 1)
                        newRow = (SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow)soDataset.CustomerSalesOrderDetailView.Rows[soDataset.CustomerSalesOrderDetailView.Rows.Count - 1];

                        //Initialize row...
                        CleanItemLine(newRow);

                        //if qtyallocated = 0, delete row
                        if (newRow.QuantityAllocated == 0)
                        {
                            soDataset.CustomerSalesOrderDetailView.Rows.Remove(newRow);
                        }
                        else
                        {
                            //increment total qty allocated
                            qtyAlloc += newRow.QuantityAllocated;
                        }

                        if (qtyOrigOrd == qtyAlloc) { break; } //if fully allocated, simply exit the loop.
                    }
                }

                //Save allocated qty to ecommerce shopping cart
                StoreAllocatedQuantities(soDataset, drow.ItemCode, drow.UnitMeasureCode, qtyAlloc);

                //Update the root item's quantity order to reflect the missing quantity unallocated...
                // Do not use Extension method ".ToDBQuote()" when using compute method of a DataTable object causes ivalid operation exception.
                decimal totalAlloc = (decimal)soDataset.CustomerSalesOrderDetailView.Compute("SUM(QuantityAllocated)", String.Format("ItemCode = '{0}' AND UnitMeasureCode = '{1}'", drow.ItemCode, drow.UnitMeasureCode));
                foreach (SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow drow2 in soDataset.CustomerSalesOrderDetailView.Rows)
                {
                    if (drow2.ItemCode != drow.ItemCode) continue;

                    if (drow2.LineNum == drow.LineNum)
                    {
                        //root item
                        drow.BeginEdit();
                        //the quantity assigned to the root item should also include the un-allocated qty for its split items
                        drow.QuantityOrdered = qtyOrigOrd - (totalAlloc - drow.QuantityAllocated);
                        drow.EndEdit();
                    }
                    else
                    {
                        //split items
                        drow2.BeginEdit();
                        drow2.QuantityOrdered = drow2.QuantityAllocated;
                        drow2.EndEdit();
                    }

                    //Update Kit Detail
                    if (drow2.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                    {
                        UpdateKitDetailView(drow2, soDataset);
                    }
                }

                //track total qty to reserve
                qtyToReserve = qtyOrigOrd - totalAlloc;

                reserveInfo[rowIndex] = new Object[] { drow.ItemCode, qtyToReserve, (decimal)0, qtyOrigOrd };
            }
            //END ALLOCATION CODE ------------------------------------------------------


            //START RESERVATION CODE ------------------------------------------------------

            //Start reserving already included line item rows
            //--------------------------------------------------------
            decimal currReservedQty = 0;

            //Deletes currently stored reservation information to make way for an updated one
            //Also modifies the TransactionCode of the reservation table to match that of the current sales order code.
            ReInitializeReservation(soDataset);

            string currentItemCode = "";

            //Begin reservation for all the line items.
            foreach (SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow drow in soDataset.CustomerSalesOrderDetailView.Rows)
            {
                //After the allocation has completed, we need to reserve items                
                decimal balance = 0;

                //get reserveInfo
                for (int resIdx = 0; resIdx <= reserveInfo.Length - 1; resIdx++)
                {
                    if (reserveInfo[resIdx][0].ToString() != drow.ItemCode) continue;

                    if (currentItemCode != drow.ItemCode)
                    {
                        currReservedQty = 0;
                    }

                    balance = (decimal)reserveInfo[resIdx][1] - currReservedQty;
                    break;

                }

                //if there are no longer any qty left for reservation, simply move on to the next item.
                if (balance == 0) { continue; }

                drow.QuantityOrdered = drow.QuantityAllocated + System.Convert.ToDecimal(Interprise.Framework.Base.Shared.Common.IsNull(drow["QuantityReserved"], 0));
                drow.QuantityOrdered += balance;
                drow.DueDate = Interprise.Framework.Base.Shared.Const.MAX_SMALLDATETIME;

                //begin qty reservation
                _facadeSalesOrder.LoadItemReservation(drow);

                //begin qty reservation
                _facadeSalesOrder.ReserveStock(drow);
                //store reservation data
                if (System.Convert.ToDecimal(Interprise.Framework.Base.Shared.Common.IsNull(drow["QuantityReserved"], 0)) > 0)
                {
                    //track reserved qty
                    currReservedQty += drow.QuantityReserved;

                    // update current detail record
                    currentItemCode = drow.ItemCode;

                    //update reserveInfo
                    for (int resIdx = 0; resIdx <= reserveInfo.Length - 1; resIdx++)
                    {
                        if (reserveInfo[resIdx][0].ToString() != drow.ItemCode) continue;

                        reserveInfo[resIdx][2] = (decimal)currReservedQty;
                        break;
                    }

                    if (drow.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                    {
                        StoreReservedKitQuantity(drow);
                    }
                    else
                    {
                        StoreReservedQuantity(drow.ItemCode, true);
                    }
                }

                drow.QuantityOrdered = drow.QuantityAllocated + System.Convert.ToDecimal(Interprise.Framework.Base.Shared.Common.IsNull(drow["QuantityReserved"], 0));
                //Re-initialize row...
                CleanItemLine(drow);

                //Update Kit Detail
                if (drow.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                {
                    UpdateKitDetailView(drow, soDataset);
                }

            }
            //--------------------------------------------------------


            //Search other POs from warehouses not in SO
            //--------------------------------------------------------
            decimal currResQty = 0;
            decimal totalResQty = 0;
            for (int resIdx = 0; resIdx <= reserveInfo.Length - 1; resIdx++)
            {
                totalResQty = (decimal)reserveInfo[resIdx][1];
                currResQty = (decimal)reserveInfo[resIdx][2];

                //check for item remaining qty unallocated/unreserved
                if (totalResQty > currResQty)
                {
                    SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow drow = null;
                    string str = string.Format("ItemCode = '{0}'", reserveInfo[resIdx][0]);
                    DataRow[] itemRows = soDataset.CustomerSalesOrderDetailView.Select(str, "LineNum");

                    //NOTE: We only need 1 instance of the item since we're going to filter out the warehouses thru Select looping all warehouses read                   
                    drow = (SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow)itemRows[0];

                    //Get cart item GUID
                    var itemGuid = new Guid();
                    var cartItem = this.CartItems.FirstOrDefault(item => item.ItemCode == drow.ItemCode);
                    if (cartItem != null) { itemGuid = cartItem.Id; }

                    //check for other warehouse locations if there are any POs available for reservation
                    foreach (DataRow drWarehouse in dtWarehouse.Rows)
                    {
                        //get order detail row instance for splits
                        string strExp = string.Format("ItemCode = '{0}' and WarehouseCode = '{1}'", drow.ItemCode, drWarehouse["WarehouseCode"].ToString());
                        DataRow[] lineItem = soDataset.CustomerSalesOrderDetailView.Select(strExp);

                        //If result.length == 0, it means that the row does not exist yet and is exactly what need
                        if (lineItem.Length == 0)
                        {
                            //Add a new line item with a new warehouse code and quantity
                            AddLineItem(drow.ItemCode, drow.UnitMeasureCode, drWarehouse, totalResQty - currResQty, drow.ItemDescription, itemGuid);

                            //get the instance of the newly added reservation row
                            var newRow = (SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow)soDataset.CustomerSalesOrderDetailView.Rows[soDataset.CustomerSalesOrderDetailView.Rows.Count - 1];

                            //Initialize row...
                            CleanItemLine(newRow);

                            //begin qty reservation
                            _facadeSalesOrder.LoadItemReservation(newRow);

                            //begin qty reservation
                            _facadeSalesOrder.ReserveStock(newRow);

                            if (System.Convert.ToDecimal(Interprise.Framework.Base.Shared.Common.IsNull(newRow["QuantityReserved"], 0)) > 0)
                            {
                                //modify qty ordered to be the same as the reserved then recompute
                                newRow.BeginEdit();
                                newRow.QuantityOrdered = newRow.QuantityReserved;
                                newRow.EndEdit();

                                CleanItemLine(newRow);

                                //Update Kit Detail
                                if (newRow.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                                {
                                    UpdateKitDetailView(newRow, soDataset);
                                }

                                //update current reserved qty
                                currResQty += newRow.QuantityReserved;

                                //update reserveInfo
                                reserveInfo[resIdx][2] = currResQty;

                                if (newRow.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                                {
                                    StoreReservedKitQuantity(newRow);
                                }
                                else
                                {
                                    StoreReservedQuantity(newRow.ItemCode, true);
                                }

                                //if qty fully reserved, exit. otherwise, search for possible POs from other warehouses
                                if (currResQty == totalResQty) { break; }
                            }
                            else
                            {
                                //remove row if its not reserved
                                if (System.Convert.ToDecimal(Interprise.Framework.Base.Shared.Common.IsNull(newRow["QuantityReserved"], 0)) == 0) { soDataset.CustomerSalesOrderDetailView.Rows.Remove(newRow); }
                            }
                        }
                    }
                }
            }
            //--------------------------------------------------------


            //Reservation clean-up
            //--------------------------------------------------------
            //check if after looking through all warehouses there are still qty not reserved
            for (int rowIndex = 0; rowIndex <= rowCount - 1; rowIndex++)
            {
                var drow = (SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow)soDataset.CustomerSalesOrderDetailView.Rows[rowIndex];

                //get remaining reserve and reserved qty
                decimal finalNeededReserved = 0;
                decimal finalReserved = 0;
                for (int resIdx = 0; resIdx <= reserveInfo.Length - 1; resIdx++)
                {
                    if (reserveInfo[resIdx][0].ToString() != drow.ItemCode) continue;
                    finalNeededReserved = (decimal)reserveInfo[resIdx][1];
                    finalReserved = (decimal)reserveInfo[resIdx][2];
                    break;
                }

                if (finalReserved < finalNeededReserved)
                {
                    decimal remResQty = finalNeededReserved - finalReserved;
                    drow.BeginEdit();
                    //since we modified the root item's quantity ordered for the process of allocation and reservation
                    //we need to restore the qty removed but are unallocated or unreserved...
                    drow.QuantityOrdered += remResQty;
                    drow.EndEdit();

                    //Update Kit Detail
                    if (drow.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                    {
                        UpdateKitDetailView(drow, soDataset);
                    }
                }
            }
            //--------------------------------------------------------

            //END RESERVATION CODE ------------------------------------------------------

            //Full Allocation and Reservation Clean-up
            //--------------------------------------------------------
            for (int rowIndex = soDataset.CustomerSalesOrderDetailView.Rows.Count - 1; rowIndex >= 0; rowIndex--)
            {
                var drow = (SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow)soDataset.CustomerSalesOrderDetailView.Rows[rowIndex];
                if (drow.QuantityOrdered == 0 && drow.QuantityAllocated == 0 && System.Convert.ToDecimal(Interprise.Framework.Base.Shared.Common.IsNull(drow["QuantityReserved"], 0)) == 0)
                {
                    DataRow[] splitRow = soDataset.CustomerSalesOrderDetailView.Select(string.Format("ItemCode = '{0}' and LineNum <> {1}", drow.ItemCode, drow.LineNum));

                    //Check if there are split rows for the root item. If none, do not delete the root item
                    if (splitRow.Length > 0)
                    {
                        //Since we will be deleting the root item, an exception on redering the page will be encountered.
                        //To fix this, we will replace the
                        var cartItem = this.CartItems.FirstOrDefault(item => item.ItemCode == drow.ItemCode);
                        var lineNum = drow["LineNum"];
                        if (cartItem != null)
                        {
                            cartItem.AssociatedLineItemRow = splitRow[0] as SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow;
                        }

                        DeleteRelatedRows(drow, soDataset);
                        soDataset.CustomerSalesOrderDetailView.Rows.Remove(drow);

                        //Update LineNum value from the deleted original row. Requirement of HN for Pick & Pack and Delivery item to be always at the bottom.
                        //Reservation re-allocate CustomerSalesOrderDetail item when main warehouse stock is zero
                        splitRow[0]["LineNum"] = lineNum;
                        splitRow[0]["SortOrder"] = lineNum;
                    }
                }
            }
            //--------------------------------------------------------

            //Recompute whole order
            //---------------------------------------------------------------------------
            foreach (SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow drowItems in soDataset.CustomerSalesOrderDetailView.Rows)
            {
                _facadeSalesOrder.Compute(drowItems, Interprise.Framework.Base.Shared.Enum.TransactionType.SalesOrder);
            }

            _facadeSalesOrder.ComputeTotals(false, false, true);
            //---------------------------------------------------------------------------
        }

        private void UpdateKitDetailView(SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow drow, SalesOrderDatasetGateway soDataset)
        {
            var kitDetailRows = soDataset.CustomerItemKitDetailView.Select(string.Format("ItemKitCode = '{0}' and LineNum = {1}", drow.ItemCode, drow.LineNum));
            for (int kitCtr = 0; kitCtr <= kitDetailRows.Length - 1; kitCtr++)
            {
                var kitDetailRow = kitDetailRows[kitCtr] as SalesOrderDatasetGateway.CustomerItemKitDetailViewRow;
                kitDetailRow.QuantityOrdered = drow.QuantityOrdered * drow.UnitMeasureQty * kitDetailRow.QuantityPerKit * kitDetailRow.UnitMeasureQty;
            }
        }

        private void CleanItemLine(SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow drow)
        {
            drow.BeginEdit();
            drow.OldWarehouseCode = string.Empty;
            drow.OriginalQuantityOrdered = 0;
            drow.OriginalQuantityAllocated = 0;
            drow.OriginalQuantityReservedInBase = 0;
            drow.OriginalQuantityOrdered = 0;
            drow.DueDate = Interprise.Framework.Base.Shared.Const.MAX_SMALLDATETIME;
            drow.EndEdit();
        }

        private void DeleteRelatedRows(SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow drow, SalesOrderDatasetGateway soDataset)
        {
            //Delete CustomerSalesOrderWorkflow            
            for (int wfCtr = soDataset.CustomerSalesOrderWorkflowView.Rows.Count - 1; wfCtr >= 0; wfCtr--)
            {
                bool wfUsed = soDataset.CustomerSalesOrderDetailView.Rows
                                .OfType<SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow>()
                                .Any(row => row.WarehouseCode == soDataset.CustomerSalesOrderWorkflowView.Rows[wfCtr]["WarehouseCode"].ToString());
                if (wfUsed) return;

                //delete unused warehouse workflow
                if (!wfUsed) { soDataset.CustomerSalesOrderWorkflowView.Rows[wfCtr].Delete(); }
            }

            //Delete TransactionItemTaxDetail
            DataRow[] taxDetails = soDataset.TransactionItemTaxDetailView.Select(string.Format("ItemCode = '{0}' and LineNum = {1}", drow.ItemCode, drow.LineNum));
            for (int taxCtr = taxDetails.Length - 1; taxCtr >= 0; taxCtr--)
            {
                taxDetails[taxCtr].Delete();
            }

            //Delete CustomerItemKitDetail
            DataRow[] kitDetails = soDataset.CustomerItemKitDetailView.Select(string.Format("ItemKitCode = '{0}' and LineNum = {1}", drow.ItemCode, drow.LineNum));
            for (int kitCtr = kitDetails.Length - 1; kitCtr >= 0; kitCtr--)
            {
                kitDetails[kitCtr].Delete();
            }

        }

        private bool HasAllocatedQuantity()
        {
            var soDataset = (SalesOrderDatasetGateway)_facadeSalesOrder.CurrentDataset;

            bool result = soDataset.CustomerSalesOrderDetailView.Rows
                                .OfType<SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow>()
                                .Any(row => row.QuantityAllocated > 0);

            soDataset.Dispose();
            soDataset = null;

            return result;
        }

        private void ReInitializeReservation(SalesOrderDatasetGateway soDataset, bool isClearReservation = true)
        {
            var sqlStr = new StringBuilder();

            //delete existing reservation data
            if (isClearReservation)
            {
                sqlStr.AppendFormat("DELETE [EcommerceShoppingCartReservation] WHERE ContactCode = {0}; ", DB.SQuote(ThisCustomer.ContactCode));
                DB.ExecuteSQL(sqlStr.ToString());
            }
            
        }

        private void StoreAllocatedQuantities(Interprise.Framework.Customer.DatasetGateway.SalesOrderDatasetGateway soDataset, string itemCode = "", string unitMeasureCode = "", decimal qtyAlloc = 0)
        {
            if (itemCode == string.Empty)
            {
                var rows = soDataset.CustomerSalesOrderDetailView.Rows;

                var items = rows.OfType<DataRow>().Select(s => new CustomerSalesOrderDetailViewDTO()
                {
                    QuantityAllocated = Convert.ToDecimal(s["QuantityAllocated"]),
                    ItemCode = s["ItemCode"].ToString(),
                    UnitMeasureCode = s["UnitMeasureCode"].ToString()
                });
                ServiceFactory.GetInstance<IShoppingCartService>().UpdateAllocatedQty(items);

                foreach (SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow drow in rows)
                {
                    //update cart item with the allocated qty
                    foreach (var cart in this.CartItems)
                    {
                        if (cart.ItemCode == drow.ItemCode && cart.UnitMeasureCode == drow.UnitMeasureCode)
                        {
                            cart.m_AllocatedQty = drow.QuantityAllocated;
                        }
                    }
                }
            }
            else
            {
                ServiceFactory.GetInstance<IShoppingCartService>().UpdateAllocatedQty(qtyAlloc, itemCode, unitMeasureCode);

                //update cart item with the allocated qty
                foreach (CartItem cart in this.CartItems)
                {
                    if (cart.ItemCode == itemCode && cart.UnitMeasureCode == unitMeasureCode)
                    {
                        cart.m_AllocatedQty = qtyAlloc;
                    }
                }
            }
        }

        private void StoreReservedQuantity(DataRow[] reservedTrans, string itemCode, bool isClearReservation = false)
        {
            if (reservedTrans == null) { return; }
            if (reservedTrans.Length == 0) { return; }

            var sqlStr = new StringBuilder();

            if (reservedTrans.Length > 0)
            {
                if (isClearReservation)
                {
                    sqlStr.AppendFormat("DELETE [EcommerceShoppingCartReservation] WHERE ContactCode = {0} AND ItemCode = {1}; ", DB.SQuote(ThisCustomer.ContactCode), DB.SQuote(itemCode));
                }
                foreach (DataRow drowReserved in reservedTrans)
                {
                    //insert reservation data for the item
                    sqlStr.AppendFormat("INSERT INTO [EcommerceShoppingCartReservation] (ContactCode, ItemCode, ReservedQty, DueDate) VALUES ({0}, {1}, {2}, {3}); ",
                        DB.SQuote(ThisCustomer.ContactCode),
                        DB.SQuote(itemCode),
                        DB.SQuote(System.Convert.ToDecimal(drowReserved["ReserveQty"]).ToString()),
                        DB.SQuote(System.Convert.ToDateTime(drowReserved["DueDate"]).ToString(System.Globalization.DateTimeFormatInfo.InvariantInfo.ShortDatePattern)));
                }
            }

            //execute sqlStr
            DB.ExecuteSQL(sqlStr.ToString());
            //object disposal/dereference

        }

        private void StoreReservedQuantity(string itemCode, bool isClearReservation = false)
        {

            //retrieve the reservation facade's reservation dataset
            var reserveDataset = (Interprise.Framework.Base.DatasetGateway.ReservationDatasetGateway)_facadeSalesOrder.ReservationFacade.CurrentDataset;
            //filter the dataset of which rows are reserved
            DataRow[] reservedTrans = reserveDataset.SupplierPurchaseOrderReservationView.Select(string.Format("{0} > 0 and {1} = '{2}'", reserveDataset.SupplierPurchaseOrderReservationView.ReserveQtyColumn.ColumnName, reserveDataset.SupplierPurchaseOrderReservationView.ItemCodeColumn.ColumnName, itemCode));
            StoreReservedQuantity(reservedTrans, itemCode, isClearReservation);
            // disposal
            reserveDataset.Dispose();
            reserveDataset = null;
        }

        private void StoreReservedKitQuantity(SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow itemRow)
        {
            //retrieve the reservation facade's reservation dataset
            Interprise.Framework.Base.DatasetGateway.ReservationDatasetGateway reserveDataset = (Interprise.Framework.Base.DatasetGateway.ReservationDatasetGateway)_facadeSalesOrder.ReservationFacade.CurrentDataset;
            //filter the dataset of which rows are reserved
            DataRow[] reservedTrans = reserveDataset.CustomerKitTransactionReservationView.Select(string.Format("{0} > 0 and {1} = '{2}'", reserveDataset.CustomerKitTransactionReservationView.ReserveQtyColumn.ColumnName, reserveDataset.CustomerKitTransactionReservationView.KitCodeColumn.ColumnName, itemRow.ItemCode), reserveDataset.CustomerKitTransactionReservationView.DueDateColumn.ColumnName);

            var sqlStr = new StringBuilder();
            if (reservedTrans.Length > 0)
            {
                //foreach (DataRow drowReserved in reservedTrans)
                //{
                //insert reservation data for the item
                sqlStr.AppendFormat("INSERT INTO [EcommerceShoppingCartReservation] (ContactCode, ItemCode, ReservedQty, DueDate) VALUES ({0}, {1}, {2}, {3}); ",
                    DB.SQuote(ThisCustomer.ContactCode),
                    DB.SQuote(itemRow.ItemCode),
                    DB.SQuote(itemRow.QuantityReserved.ToString()),
                    DB.SQuote(System.Convert.ToDateTime(reservedTrans[reservedTrans.Length - 1]["DueDate"]).ToShortDateString()));
                //}
            }

            //execute sqlStr
            DB.ExecuteSQL(sqlStr.ToString());
            //object disposal/dereference
            reserveDataset.Dispose();
            reserveDataset = null;
        }

        private static DataSet dsWarehouse = null;
        private static DataTable GetWarehouse()
        {
            if (dsWarehouse == null)
            {
                //if (CommonLogic.GetWebsiteType() == WebsiteType.Company)
                //{
                //    dsWarehouse = Interprise.Facade.Base.SimpleFacade.Instance.LoadDataSet(CommandType.Text,
                //        "SELECT * FROM InventoryWarehouse with (NOLOCK) WHERE IsActive = 1 AND WarehouseCode IN ('HN Warehouse', 'INGLEBURN-STOCK')",
                //        new string[] { "InventoryWarehouse" }, null);
                //}
                //else
                //{
                //    dsWarehouse = Interprise.Facade.Base.SimpleFacade.Instance.LoadDataSet(CommandType.Text,
                //        "SELECT * FROM InventoryWarehouse with (NOLOCK) WHERE IsActive = 1 ",
                //        new string[] { "InventoryWarehouse" }, null);
                //}
                dsWarehouse = Interprise.Facade.Base.SimpleFacade.Instance.LoadDataSet(CommandType.Text,
                        "SELECT * FROM InventoryWarehouse with (NOLOCK) WHERE IsActive = 1 ",
                        new string[] { "InventoryWarehouse" }, null);
            }
            return dsWarehouse.Tables["InventoryWarehouse"];
        }

        private static string[] GetWarehouseFiltered(DataTable dtWarehouse, string whfilter)
        {
            string[] filteredWH = new string[dtWarehouse.Rows.Count - 1];
            string filterStr = string.Format("WarehouseCode <> '{0}'", whfilter);
            DataRow[] whRows = dtWarehouse.Select(filterStr);

            for (int whCtr = 0; whCtr <= whRows.Length - 1; whCtr++)
            {
                filteredWH[whCtr] = whRows[whCtr]["WarehouseCode"].ToString();
            }

            return filteredWH;
        }

        private static string GetPreferredGateway()
        {
            return ServiceFactory.GetInstance<IPaymentTermService>().GetPrefferedGatewayInfo()[0];
        }

        [Obsolete("Use the method : ServiceFactory.GetInstance<IPaymentService>().GetPrefferedGatewayInfo() if fully migrated to CB13.1.3/CB13.2 and above")]
        private static string[] GetPrefferedGatewayInfo()
        {
            string[] info = new string[] { string.Empty, string.Empty, string.Empty, string.Empty, string.Empty };

            bool hasGatewayDefinedInWebsite = false;

            if (System.Web.HttpContext.Current == null)
            {
                hasGatewayDefinedInWebsite = false;
            }

            // First try the WebSite if it has one defined            
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "EcommerceGetCreditCardGatewayByWebsite @WebsiteCode = {0}", DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode)))
                {
                    if (reader.Read())
                    {
                        string merchantLogin = DB.RSField(reader, "MerchantLogin");
                        if (!merchantLogin.IsNullOrEmptyTrimmed())
                        {
                            hasGatewayDefinedInWebsite = true;
                            info[0] = DB.RSField(reader, "CreditCardGateway");
                            info[1] = DB.RSFieldBool(reader, "IsCustom").ToString();
                            info[2] = String.Empty;
                            info[3] = DB.RSField(reader, "CreditCardGatewayAssemblyName");
                            info[4] = merchantLogin;
                        }
                    }
                }
            }

            // If we don't have one defined let's try the Customer's Payment Term instead
            if (!hasGatewayDefinedInWebsite)
            {
                using (var con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (var reader = DB.GetRSFormat(con, "eCommerceGetCreditCardGatewayByPaymentTerm @PaymentTermCode = {0}", DB.SQuote(Customer.Current.PaymentTermCode)))
                    {
                        if (reader.Read())
                        {
                            hasGatewayDefinedInWebsite = true;
                            info[0] = DB.RSField(reader, "CreditCardGateway");
                            info[1] = DB.RSFieldBool(reader, "IsCustom").ToString();
                            info[2] = String.Empty;
                            info[3] = DB.RSField(reader, "CreditCardGatewayAssemblyName");
                            info[4] = DB.RSField(reader, "MerchantLogin");
                        }
                    }
                }
            }
            return info;
        }

        [Obsolete("Use the method GetGateway in CreditCardAuthorizationFacade in getting Credit Card Gateway interface. You may use ServiceFactory.GetInstance<IPaymentTermService>().GetGatewayInterface in getting gateway information to pass on GetGateway method.")]
        private static ICreditCardGatewayInterface GetGatewayProcessorCore()
        {
            string[] info = new string[5];
            var request = ServiceFactory.GetInstance<IHttpContextService>().CurrentRequest;
            if (request.QueryString["PayPal"] == bool.TrueString && request.QueryString["token"] != null)
            {
                info = ServiceFactory.GetInstance<IPaymentTermService>().GetPaypalGatewayInfo();
            }
            else
            {
                info = ServiceFactory.GetInstance<IPaymentTermService>().GetPrefferedGatewayInfo();
            }

            ICreditCardGatewayInterface gatewayProcessor = null;
            try
            {
                var gatewayInterface = Interprise.Facade.Base.SimpleFacade.Instance.DeserializeFormSectionPlugin(info, new object[] { }) as Interprise.Extendable.Base.Presentation.Customer.CreditCardGateway.ICreditCardGatewayInterface;
                if (null != gatewayInterface)
                {
                    gatewayProcessor = gatewayInterface.CreditCardGatewayFacade;
                }
            }
            catch (Exception)
            {
                throw new Exception("Unable to instantiate Default Credit Card Gateway");
            }

            return gatewayProcessor;
        }

        private string CreatePayment(Address billingAddress, GatewayResponse withGatewayAuthResponse, bool is3DsecondAuth, string receiptCode, string salesOrderCode,
            ref ReceiptDatasetGateway paramGatewayReceiptDataset, ref ReceiptFacade paramFacadeReceipt, ref  bool isApproved)
        {
            string status = AppLogic.ro_OK;
            string sagepayPaymentTerm = ServiceFactory.GetInstance<IAppConfigService>().SagePayPaymentTerm;

            /***************************************************************
           *  2. Create Receipt
           * *************************************************************/

            // get customer payment term
            var term = PaymentTermDTO.Find(ThisCustomer.PaymentTermCode);

            // generate document code
            if (term.PaymentTermCode != AppLogic.AppConfig("PaymentTermCodeZeroDollarOrder") &&
                false == "REQUEST QUOTE".Equals(term.PaymentTermCode.ToUpperInvariant()) &&
                false == "PURCHASE ORDER".Equals(term.PaymentTermCode.ToUpperInvariant())
               )
            {
                #region create receipt facade

                var gatewayReceiptDataset = new ReceiptDatasetGateway();
                var facadeReceipt = new ReceiptFacade(gatewayReceiptDataset);

                var facadeCustomerReceiptList = new ListControlFacade();
                var datasetCustomerReceipt = new DataSet();
                var customerReceiptBaseDataset = new BaseDataset();

                #endregion

                // retrieve the customer information

                #region get customer info and row
                string customerInfoQuery = string.Format("CustomerCode = {0}",
                                            DB.SQuote(CommonLogic.IIF(ThisCustomer.IsNotRegistered, ThisCustomer.AnonymousCustomerCode, ThisCustomer.CustomerCode)));

                //deadlock
                int nooftries = 0;
                while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
                {
                    try
                    {
                        // this will load data for both customer and prospect.
                        datasetCustomerReceipt = facadeCustomerReceiptList.ReadSearchResultsData("CustomerView",
                                                    customerInfoQuery,
                                                    1,
                                                    false,
                                                    string.Empty,
                                                    ref customerReceiptBaseDataset,
                                                    true);
                        break;
                    }
                    catch (SqlException ex)
                    {
                        if (ex.ErrorCode == 1205)
                        {
                            nooftries += 1;
                        }
                        else
                        {
                            throw;
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                }


                //get customer detail row
                DataRow rowCustomer = customerReceiptBaseDataset.Tables["CustomerView"].Rows[0];

                #endregion

                #region get cardExtraCode

                string cardExtraCode = String.Empty;
                if (term.PaymentMethod == DomainConstants.PAYMENT_METHOD_CREDITCARD)
                {
                    cardExtraCode = AppLogic.GetCardExtraCodeFromSession(ThisCustomer);
                }

                #endregion

                // assign transaction type
                facadeReceipt.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt;

                //create receipt row
                if (is3DsecondAuth)
                {
                    //reload data

                    #region relaod data

                    DataSet ds = Interprise.Facade.Base.SimpleFacade.Instance.LoadDataSet(CommandType.StoredProcedure,
                                                                                          "EcommerceGetCustomerPayment",
                                                                                           new string[] { Interprise.Framework.Customer.Shared.Const.CUSTOMERTRANSACTIONRECEIPTVIEW_TABLE, 
                                                                                                          Const.CUSTOMERPAYMENT_TABLE, Const.CUSTOMERCARDPAYMENT_TABLE, Const.PAYMENTMETHODVIEW_TABLE, 
                                                                                                          "DefaultAccount", "HasReserved", Interprise.Framework.Customer.Shared.Const.CUSTOMERTRADINGINFOVIEW_TABLE },
                                                                                           new string[][] { new string[] {"@DocumentCode", salesOrderCode},new string[] {"@CardPaymentCode", receiptCode},
                                                                                                            new string[] {"@CustomerCode", ThisCustomer.CustomerCode}},
                                                                                           Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                    if (ds.Tables.Contains(Interprise.Framework.Customer.Shared.Const.CUSTOMERTRANSACTIONRECEIPTVIEW_TABLE))
                    {
                        gatewayReceiptDataset.Merge(ds.Tables[Interprise.Framework.Customer.Shared.Const.CUSTOMERTRANSACTIONRECEIPTVIEW_TABLE]);
                    }
                    _facadeSalesOrder.IsTransactionReceiptLoaded = true;
                    if (ds.Tables.Contains("DefaultAccount") && ds.Tables["DefaultAccount"].Rows.Count > 0 & ds.Tables.Contains("HasReserved"))
                    {
                        facadeReceipt.SetSettings(Convert.ToString(ds.Tables["DefaultAccount"].Rows[0][0]), ds.Tables["HasReserved"].Rows.Count > 0);
                    }

                    gatewayReceiptDataset.PaymentMethodView.Clear();
                    gatewayReceiptDataset.CustomerPayment.Clear();
                    gatewayReceiptDataset.CustomerCardPayment.Clear();

                    facadeReceipt.RemovePaymentTableRelation();

                    if (ds.Tables.Contains(Interprise.Framework.Base.Shared.Const.CUSTOMERPAYMENT_TABLE))
                    {
                        gatewayReceiptDataset.Merge(ds.Tables[Interprise.Framework.Base.Shared.Const.CUSTOMERPAYMENT_TABLE]);
                    }
                    if (ds.Tables.Contains(Interprise.Framework.Base.Shared.Const.CUSTOMERCARDPAYMENT_TABLE))
                    {
                        gatewayReceiptDataset.Merge(ds.Tables[Interprise.Framework.Base.Shared.Const.CUSTOMERCARDPAYMENT_TABLE]);
                    }
                    if (ds.Tables.Contains(Const.PAYMENTMETHODVIEW_TABLE))
                    {
                        gatewayReceiptDataset.Merge(ds.Tables[Const.PAYMENTMETHODVIEW_TABLE]);
                    }

                    if (!facadeReceipt.DisableRelationInitialization)
                    {
                        facadeReceipt.InitializePaymentTableRelation();
                    }

                    facadeReceipt.RestoreCV();

                    #endregion
                }
                else
                {
                    #region AddReceipt

                    facadeReceipt.AddReceipt(new DataRow[] { rowCustomer },
                     Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt,
                     true,
                     _facadeSalesOrder.GetCalculatedTotalDue(),
                     _gatewaySalesOrderDataset.CustomerSalesOrderView[0].PaymentTermCode,
                     _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode,
                     _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ContactCode);

                    #endregion
                }

                // assign documentcode to card payment
                if (term.PaymentMethod == DomainConstants.PAYMENT_METHOD_CREDITCARD || term.PaymentTermCode == sagepayPaymentTerm)
                {
                    gatewayReceiptDataset.CustomerCardPayment[0].CardPaymentCode = receiptCode;
                    //gatewayReceiptDataset.CustomerCardPayment[0]["CostCenter_DEV004817"] = "Smartbag-RETAIL";
                }

                // get payment row
                var rowPayMethod = gatewayReceiptDataset.PaymentMethodView[0];

                // create payment header row
                if (ThisCustomer.IsNotRegistered)
                {
                    #region Anon Checkout
                    // NOTE:
                    //  This should just set the default values
                    facadeReceipt.AssignPaymentMethodDefaults(
                        rowPayMethod,
                        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].PaymentTermCode
                    );

                    // we then will manually assign the credit card details...
                    rowPayMethod.BeginEdit();

                    // NOTE: 
                    //  settting IsCardOnFile to false MUST come first
                    //  Order is important here since internally it modifies other fields
                    //  Setting this column's value at the last would result in a different behaviour
                    rowPayMethod.IsCardOnFile = false;

                    // get payment type detail for IsDeposited
                    string isDeposited = Boolean.TrueString;
                    isDeposited = facadeReceipt.GetField("IsDeposited",
                                                          Interprise.Framework.Base.Shared.Const.SYSTEMPAYMENTTYPE_TABLE,
                                                          String.Format("{0}='{1}'", Interprise.Framework.Base.Shared.Const.SYSTEMPAYMENTTYPE_PAYMENTTYPECODE_COLUMN,
                                                          Convert.ToString(Interprise.Framework.Base.Shared.Common.IsNull(gatewayReceiptDataset.PaymentMethodView[0][gatewayReceiptDataset.PaymentMethodView.PaymentTypeCodeColumn.ColumnName], String.Empty))));

                    if (isDeposited == null || String.Compare(isDeposited, Boolean.FalseString, true) == 0)
                    {
                        rowPayMethod.Account = Interprise.Framework.Base.Shared.Const.PAYMENT_ACCOUNT_UNDEPOSITED;
                    }
                    else
                    {
                        rowPayMethod.Account = Interprise.Framework.Base.Shared.Const.PAYMENT_ACCOUNT;
                    }

                    #region assign credit card payment info

                    if (term.PaymentMethod == DomainConstants.PAYMENT_METHOD_CREDITCARD || term.PaymentTermCode == sagepayPaymentTerm)
                    {

                        // Reset credit card value for anonymous
                        if (facadeReceipt.IsInterpriseGatewayv2 || facadeReceipt.IsInterpriseGateway)
                        {
                            rowPayMethod.InterpriseGatewayRefNo = 0;
                            rowPayMethod.CreditCardOnFile = String.Empty;
                        }

                        rowPayMethod.CreditCardCode = String.Empty;
                        rowPayMethod.CreditCardSalt = billingAddress.CardNumberSalt;
                        rowPayMethod.CreditCardEmail = billingAddress.EMail;
                        rowPayMethod.CreditCardIV = billingAddress.CardNumberIV;
                        rowPayMethod.CreditCardName = billingAddress.CardName;

                        // NOTE:
                        // IS Always assume that the credit card being passed here is encrypted
                        // thus the need for the next line below
                        rowPayMethod.CreditCard = facadeReceipt.EncryptCardNumber(billingAddress.CardNumber, rowPayMethod);

                        rowPayMethod.CreditCardAddress = billingAddress.Address1;
                        rowPayMethod.CreditCardCity = billingAddress.City;
                        rowPayMethod.CreditCardState = billingAddress.State;
                        rowPayMethod.CreditCardPostalCode = billingAddress.PostalCode;

                        if (!billingAddress.Plus4.IsNullOrEmptyTrimmed())
                        {
                            rowPayMethod.CreditCardPlus4 = Convert.ToInt32(billingAddress.Plus4);
                        }

                        rowPayMethod.CreditCardCountry = billingAddress.Country;
                        if (ThisCustomer.IsNotRegistered && ThisCustomer.ThisCustomerSession["paypalfrom"] != null && (ThisCustomer.ThisCustomerSession["paypalfrom"] == "shoppingcart" || ThisCustomer.ThisCustomerSession["paypalfrom"] == "checkoutpayment" || ThisCustomer.ThisCustomerSession["paypalfrom"] == "onepagecheckout"))
                        {
                            DateTime now = DateTime.Now;
                            rowPayMethod.CreditCardExpMon = now.ToString("MMM");
                            rowPayMethod.CreditCardExpYear = now.AddYears(1).Year.ToString();
                        }
                        else
                        {
                            rowPayMethod.CreditCardExpMon = InterpriseHelper.ToInterpriseExpMonth(billingAddress.CardExpirationMonth);
                            rowPayMethod.CreditCardExpYear = billingAddress.CardExpirationYear;
                        }

                        if (AppLogic.AppConfigBool("ShowCardStartDateFields"))
                        {
                            rowPayMethod.CreditCardStartMon = InterpriseHelper.ToInterpriseExpMonth(billingAddress.CardStartMonth);
                            rowPayMethod.CreditCardStartYear = billingAddress.CardStartYear;

                            // same reason for credit card applies here
                            rowPayMethod.CreditCardIssueNumber = facadeReceipt.EncryptCardIssueNumber(billingAddress.CardIssueNumber, rowPayMethod);
                        }
                    }

                    #endregion

                    rowPayMethod.CreditCardTelephone = billingAddress.Phone;
                    rowPayMethod.CreditCardTelephoneExtension = string.Empty;
                    rowPayMethod.CreditCardType = billingAddress.CardType;
                    rowPayMethod.CreditCardTypeDescription = billingAddress.CardType;
                    rowPayMethod.EndEdit();

                    #endregion
                }
                else
                {
                    #region Registered Customer

                    // Code inside this function should retrieve the card number
                    // information of the customer if it has a default one...
                    if (!is3DsecondAuth)
                    {
                        facadeReceipt.AssignPaymentMethodDefaults(
                            rowPayMethod,
                            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].PaymentTermCode
                        );

                    }

                    #endregion
                }

                // NOTE :   this should recompute for AmountPaid and AmountPaidRate
                //          both CustomerPayment and PaymentMethod table

                // assign amount to pay
                rowPayMethod.AmountPaidRate = _facadeSalesOrder.GetCalculatedTotalDue();
                rowPayMethod.CustomerIPAddress = ThisCustomer.LastIPAddress;

                // Save Payment
                string[] receiptRelatedTables = facadeReceipt.get_RelatedTables(Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt);
                string[][] receiptCommands = facadeReceipt.CreateParameterSet(receiptRelatedTables);

                isApproved = facadeReceipt.UpdateDataSet(receiptCommands,
                                            Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt,
                                            string.Empty,
                                            false);
                DB.ExecuteSQL("UPDATE CustomerCardPayment SET CostCenter_DEV004817 = 'Smartbag-RETAIL' WHERE CardPaymentCode = {0}", DB.SQuote(receiptCode));

                if (term.PaymentMethod == DomainConstants.PAYMENT_METHOD_CREDITCARD || term.PaymentTermCode == sagepayPaymentTerm)
                {
                    isApproved = false;

                    #region capture credit card transaction info

                    #region capture credit card number

                    //Capture the credit card number from the payment page and encrypt it so that the gateway can capture from that credit card
                    rowPayMethod.CreditCardSalt = ThisCustomer.ThisCustomerSession["CardNumberSalt"];
                    rowPayMethod.CreditCardIV = ThisCustomer.ThisCustomerSession["CardNumberIV"];
                    rowPayMethod.CreditCard = ThisCustomer.ThisCustomerSession["CardNumber"];


                    if (!(Convert.ToString(Interprise.Framework.Base.Shared.Common.IsNull(rowPayMethod[gatewayReceiptDataset.PaymentMethodView.CreditCardColumn.ColumnName]))).IsNullOrEmptyTrimmed())
                    {
                        if (facadeReceipt.IsInterpriseGateway || facadeReceipt.IsInterpriseGatewayv2 || ThisCustomer.IsNotRegistered)
                        {
                            rowPayMethod.CreditCardOnFile = Interprise.Framework.Base.Shared.Common.MaskCardNumber(facadeReceipt.DecryptCardNumber(Convert.ToString(Interprise.Framework.Base.Shared.Common.IsNull(rowPayMethod[gatewayReceiptDataset.PaymentMethodView.CreditCardColumn.ColumnName])), rowPayMethod));
                        }
                    }


                    if (ThisCustomer.IsNotRegistered && ThisCustomer.ThisCustomerSession["paypalfrom"] != null && (ThisCustomer.ThisCustomerSession["paypalfrom"] == "shoppingcart" || ThisCustomer.ThisCustomerSession["paypalfrom"] == "checkoutpayment" || ThisCustomer.ThisCustomerSession["paypalfrom"] == "onepagecheckout"))
                    {
                        DateTime now = DateTime.Now;
                        rowPayMethod.CreditCardExpMon = now.ToString("MMM");
                        rowPayMethod.CreditCardExpYear = now.AddYears(1).Year.ToString();
                    }
                    else
                    {
                        rowPayMethod.CreditCardExpMon = InterpriseHelper.ToInterpriseExpMonth(billingAddress.CardExpirationMonth);
                        rowPayMethod.CreditCardExpYear = billingAddress.CardExpirationYear;
                    }
                    rowPayMethod.CreditCardCV = billingAddress.CardIssueNumber;

                    // this will assign the Card On Name to Credit Card Gateway.
                    if (ThisCustomer.IsNotRegistered && term.PaymentTermCode == sagepayPaymentTerm)
                    {
                        rowPayMethod.CreditCardName = billingAddress.Name;
                    }
                    else
                    {
                        rowPayMethod.CreditCardName = billingAddress.CardName;
                    }


                    rowPayMethod.CreditCardCV = cardExtraCode;
                    rowPayMethod.CreditCardTransactionType = "Auth";

                    rowPayMethod.CreditCardCountry = billingAddress.Country;
                    rowPayMethod.CreditCardAddress = billingAddress.Address1;
                    rowPayMethod.CreditCardCity = billingAddress.City;
                    rowPayMethod.CreditCardState = billingAddress.State;
                    rowPayMethod.CreditCardPostalCode = billingAddress.PostalCode;
                    rowPayMethod.CreditCardTelephone = billingAddress.Phone;
                    rowPayMethod.CreditCardEmail = billingAddress.EMail;

                    if (!billingAddress.Plus4.IsNullOrEmptyTrimmed())
                    {
                        rowPayMethod.CreditCardPlus4 = Convert.ToInt32(billingAddress.Plus4);
                    }

                    #endregion

                    //Credit Card Tokenization...............
                    #region Tokenization
                    if (AppLogic.IsUsingInterpriseGatewayv2() && ThisCustomer.IsRegistered)
                    {
                        //Check if billing has existing reference no.
                        #region GetReferenceNo
                        using (var con = DB.NewSqlConnection())
                        {
                            con.Open();
                            using (var reader = DB.GetRSFormat(con, "SELECT InterpriseGatewayRefNo FROM CustomerCreditCard with (NOLOCK) WHERE CreditCardCode={0}", DB.SQuote(billingAddress.AddressID)))
                            {
                                if (reader.Read())
                                {
                                    rowPayMethod.InterpriseGatewayRefNo = DB.RSFieldInt(reader, "InterpriseGatewayRefNo");
                                }
                            }
                        }
                        #endregion

                        rowPayMethod.IsCardOnFile = AppLogic.IsSaveCardInfoChecked(ThisCustomer); //if true, refno will be generated upon successful authorzation

                        if (rowPayMethod.InterpriseGatewayRefNo > 0)
                        {
                            rowPayMethod.IsCardOnFile = false; //set to false since refno already exists

                            if (!AppLogic.AppConfigBool("AllowCreditCardInfoSaving"))
                            {
                                rowPayMethod.InterpriseGatewayRefNo = 0;
                                DB.ExecuteSQL("UPDATE CustomerCreditCard SET InterpriseGatewayRefNo=NULL, Vault=NULL WHERE CreditCardCode={0}", DB.SQuote(billingAddress.AddressID));
                            }
                        }

                        #region override paymethod billing info

                        rowPayMethod.CreditCardName = billingAddress.CardName;
                        rowPayMethod.CreditCardType = billingAddress.CardType;
                        rowPayMethod.CreditCardStartMon = (billingAddress.CardStartMonth != string.Empty) ? InterpriseHelper.ToInterpriseExpMonth(billingAddress.CardStartMonth) : string.Empty;
                        rowPayMethod.CreditCardStartYear = billingAddress.CardStartYear;
                        rowPayMethod.CreditCardExpMon = InterpriseHelper.ToInterpriseExpMonth(billingAddress.CardExpirationMonth);
                        rowPayMethod.CreditCardExpYear = billingAddress.CardExpirationYear;

                        rowPayMethod.CreditCardCountry = billingAddress.Country;
                        rowPayMethod.CreditCardAddress = billingAddress.Address1;
                        rowPayMethod.CreditCardCity = billingAddress.City;
                        rowPayMethod.CreditCardState = billingAddress.State;
                        rowPayMethod.CreditCardPostalCode = billingAddress.PostalCode;
                        rowPayMethod.CreditCardTelephone = billingAddress.Phone;
                        rowPayMethod.CreditCardEmail = billingAddress.EMail;
                        rowPayMethod.CreditCardCode = billingAddress.AddressID;

                        #endregion
                    }
                    #endregion
                    //.....

                    #region Auth Credit Card Auth/Response Save

                    string gatewayMessage = string.Empty;
                    bool approved = false;
                    string gatewayResponseCode = String.Empty;
                    bool is3DSecureFirstAuthFail = false;
                    bool try3DSecure = false;

                    if (withGatewayAuthResponse != null)
                    {
                        #region PayPal Transaction

                        isApproved = true;
                        AttachGatewayAuthorization(gatewayReceiptDataset, rowPayMethod, withGatewayAuthResponse, receiptCode);

                        // Set the next IS gateways that will process the capture part
                        // Since now we're just doing AUTH
                        if (!ThisCustomer.ThisCustomerSession["paypalfrom"].IsNullOrEmptyTrimmed())
                        {
                            rowPayMethod.CreditCardGatewayAssemblyName = "Interprise.Presentation.Customer.PaymentGateway.PayPal";
                            rowPayMethod.CreditCardGatewayType = "Interprise.Presentation.Customer.PaymentGateway.PayPal.PayPalGatewayControl";
                        }

                        if (term.PaymentTermCode == sagepayPaymentTerm)
                        {
                            rowPayMethod.CreditCardGatewayAssemblyName = "Interprise.Presentation.Customer.PaymentGateway.Protx";
                            rowPayMethod.CreditCardGatewayType = "Interprise.Presentation.Customer.PaymentGateway.Protx.ProtxGatewayControl";
                            rowPayMethod.WebsiteCode = InterpriseHelper.ConfigInstance.WebSiteCode;
                            rowPayMethod.IsPrioritizePaymentType = true;
                        }

                        facadeReceipt.UpdateDataSet(new string[][] { 
                                new string[] { gatewayReceiptDataset.CustomerCardPayment.TableName, 
                                    "CreateCustomerCardPayment", 
                                    "UpdateCustomerCardPayment", 
                                    "DeleteCustomerCardPayment" }, 
                                new string[] { gatewayReceiptDataset.CustomerPayment.TableName, 
                                    "CreateCustomerPayment", 
                                    "UpdateCustomerPayment", 
                                    "DeleteCustomerPayment" }, 
                                new string[] { gatewayReceiptDataset.PaymentMethodView.TableName, 
                                    "CreatePaymentMethod", 
                                    "UpdatePaymentMethod", 
                                    "DeletePaymentMethod" }, 
                                new string[] { gatewayReceiptDataset.CustomerCCAuthResponse.TableName, 
                                    "CreateCustomerCCAuthResponse", 
                                    "UpdateCustomerCCAuthResponse", 
                                    "DeleteCustomerCCAuthResponse" } },
                                facadeReceipt.TransactionType,
                                "Processed credit card payment.",
                                false);

                        status = AppLogic.ro_OK;
                        #endregion
                    }
                    else
                    {
                        #region Payment Gateways Transaction

                        string prefferredGateway = GetPreferredGateway();

                        string merchantLogin = DB.GetSqlS(String.Format("SELECT MerchantLogin AS S FROM EcommerceSite WHERE WebsiteCode = {0}", InterpriseHelper.ConfigInstance.WebSiteCode.ToDbQuote()));
                        if (!merchantLogin.IsNullOrEmptyTrimmed())
                        {
                            rowPayMethod.WebsiteCode = InterpriseHelper.ConfigInstance.WebSiteCode;
                        }

                        using (var facadeCard = new CreditCardAuthorizationFacade())
                        {
                            var gatewayInfo = ServiceFactory.GetInstance<IPaymentTermService>().GetGatewayInterface();

                            var cardGateway = facadeCard.GetGateway(gatewayInfo[0], gatewayInfo[1]);
                            if (null == cardGateway) throw new Exception("Could not load payment Gateway!!!");

                            rowPayMethod.CreditCardGatewayAssemblyName = cardGateway.CreditCardGatewayAssemblyName;
                            rowPayMethod.CreditCardGatewayType = cardGateway.CreditCardGatewayType;

                            facadeCard.TransactionType = facadeReceipt.TransactionType;

                            if (prefferredGateway == AppLogic.ro_PROTX)
                            {
                                facadeCard.IsEcommerce = true;
                            }

                            var obj = new object[][] { };
                            gatewayMessage = facadeCard.AuthorizeCreditCard(facadeReceipt, false, ref approved, ref obj, false);

                            isApproved = approved;
                            if (merchantLogin.IsNullOrEmptyTrimmed())
                            {
                                rowPayMethod.WebsiteCode = InterpriseHelper.ConfigInstance.WebSiteCode;
                            }

                            gatewayResponseCode = facadeReceipt.CurrentDataset.Tables["CustomerCCAuthResponse"].Rows[0]["GatewayResponseCode"].ToString();
                            if (prefferredGateway == AppLogic.ro_PROTX && try3DSecure && gatewayResponseCode != "3DAUTH" && !is3DsecondAuth)
                            {
                                is3DSecureFirstAuthFail = true;
                            }

                            if (prefferredGateway == AppLogic.ro_PROTX && facadeCard.Is3DSecure && !is3DsecondAuth)
                            {
                                ThisCustomer.ThisCustomerSession["3DSecure.MD"] = facadeCard.Secure3DMD;
                                ThisCustomer.ThisCustomerSession["3DSecure.ACSUrl"] = facadeCard.Secure3DACSURL;
                                ThisCustomer.ThisCustomerSession["3DSecure.PAReq"] = facadeCard.Secure3DPAReq;
                                ThisCustomer.ThisCustomerSession["3DSecure.CustomerID"] = ThisCustomer.ContactGUID.ToString();
                                ThisCustomer.ThisCustomerSession["3DSecure.OrderNumber"] = salesOrderCode;
                                ThisCustomer.ThisCustomerSession["3DSecure.XID"] = receiptCode;
                                ThisCustomer.ThisCustomerSession["3DSecure.AuthorizationResult"] = facadeCard.Secure3DAuthorizationResult;
                            }

                            //Credit Card Tokenization........
                            if (cardGateway.PnRefNo > 0 && AppLogic.IsUsingInterpriseGatewayv2() && gatewayMessage == string.Empty && ThisCustomer.IsRegistered)
                            {
                                //update reference no and vault manually
                                #region Update ReferenceNo
                                if (AppLogic.IsSaveCardInfoChecked(ThisCustomer))
                                {
                                    DB.ExecuteSQL("UPDATE CustomerCreditCard SET InterpriseGatewayRefNo={0}, Vault='NMI' WHERE CreditCardCode={1}",
                                        DB.SQuote(cardGateway.PnRefNo.ToString()),
                                        DB.SQuote(billingAddress.AddressID));
                                }
                                #endregion

                                ThisCustomer.ThisCustomerSession["SaveCreditCardChecked"] = string.Empty;
                            }
                            //.....

                            cardGateway.Dispose();

                        #endregion
                        }

                        #region Save Auth Response
                        if (!approved)
                        {
                            if (prefferredGateway == AppLogic.ro_PROTX && !is3DsecondAuth && try3DSecure && !is3DSecureFirstAuthFail)
                            {
                                status = AppLogic.ro_3DSecure;
                            }
                            else
                            {
                                ThisCustomer.LastGatewayErrorMessage = facadeReceipt.GetLastCreditCardProcessMessage();
                                if (prefferredGateway == AppLogic.ro_PROTX && is3DsecondAuth)
                                {
                                    switch (gatewayResponseCode.ToUpperInvariant())
                                    {
                                        case "ERROR":
                                            status = "The transaction encountered an error. Please try again";
                                            break;
                                        case "INVALID":
                                            status = "The card was not accepted. Please try again";
                                            break;
                                        case "NOTAUTHED":
                                            status = "Your card was not authorized for that amount. Please try again";
                                            break;
                                        case "REJECTED":
                                            status = "Your card was not not accepted. Please try again";
                                            break;
                                        default:
                                            status = facadeReceipt.CurrentDataset.Tables["CustomerCCAuthResponse"].Rows[0]["ResponseMsg"].ToString();
                                            break;
                                    }
                                }
                                else
                                {
                                    status = AppLogic.ro_INTERPRISE_GATEWAY_AUTHORIZATION_FAILED;
                                }

                                string gateway = ServiceFactory.GetInstance<IPaymentTermRepository>()
                                                               .GetGateWayDescriptionByGateway(prefferredGateway);

                                if (gateway.IsNullOrEmptyTrimmed()) gateway = "Gateway";

                                LogTransactionFailed(ThisCustomer.CustomerCode,
                                                    (is3DsecondAuth) ? _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode : String.Empty,
                                                    gateway,
                                                    facadeReceipt.GetLastCreditCardProcessMessage() + gatewayMessage);

                            }
                        }

                    }

                        #endregion

                    #endregion

                    #endregion
                }


                // **************************************************
                //  Explicit Disposal and dereferencing goes here...
                // **************************************************

                facadeCustomerReceiptList.Dispose();
                datasetCustomerReceipt.Dispose();
                customerReceiptBaseDataset.Dispose();

                facadeCustomerReceiptList = null;
                datasetCustomerReceipt = null;
                customerReceiptBaseDataset = null;

                paramGatewayReceiptDataset = gatewayReceiptDataset;
                paramFacadeReceipt = facadeReceipt;

            }

            return status;
        }

        private bool AssociatePayment(string status, bool convertToInvoiceAndCapture, ReceiptFacade facadeReceipt, ReceiptDatasetGateway gatewayReceiptDataset, string receiptCode)
        {

            /*****************************************************
            * 3. Associate the receipt with the Sales Order
            *****************************************************/
            bool isVoided = false;
            var gatewayTransactionReceiptDataset = _facadeSalesOrder.TransactionReceiptListFacade.CurrentDataset as TransactionReceiptDatasetGateway;
            var facadeTransactionReceipt = _facadeSalesOrder.TransactionReceiptListFacade as TransactionReceiptFacade;
            int datasourceRowIndex = 0;
            facadeTransactionReceipt.AssignTransactionReceipt(_gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode, facadeReceipt, ref datasourceRowIndex);
            // Manually merge the Customer Transaction Receipt table..
            _gatewaySalesOrderDataset.Merge(gatewayTransactionReceiptDataset.CustomerTransactionReceiptView);
            SaveSalesOrder();
            facadeTransactionReceipt.ReserveAndAllocateReceipt(_gatewaySalesOrderDataset, null, Interprise.Framework.Base.Shared.Const.CUSTOMER_SALES_ORDER);
            isVoided = false;

            if (status != AppLogic.ro_3DSecure && status != AppLogic.ro_OK)
            {
                facadeTransactionReceipt.Void(_facadeSalesOrder.TransactionType.ToString(), _gatewaySalesOrderDataset);
                _facadeSalesOrder.VoidOrder();

                SaveSalesOrder();
                isVoided = true;
            }

            if (convertToInvoiceAndCapture && !isVoided)
            {
                ConvertToInvoiceAndCapture(gatewayReceiptDataset, facadeReceipt, receiptCode);
                isVoided = false;
            }

            // **************************************************
            //  Explicit Disposal and dereferencing goes here...
            // **************************************************

            gatewayReceiptDataset.Dispose();
            gatewayTransactionReceiptDataset.Dispose();
            facadeTransactionReceipt.Dispose();
            facadeReceipt.Dispose();

            gatewayReceiptDataset = null;
            gatewayTransactionReceiptDataset = null;
            facadeTransactionReceipt = null;
            facadeReceipt = null;

            return isVoided;

        }

        private string ProcessPayment(bool convertToInvoiceAndCapture,
            Address billingAddress,
            GatewayResponse withGatewayAuthResponse,
            string receiptCode,
            string salesOrderCode,
            bool is3DsecondAuth,
            decimal amountDeduction)
        {
            string status = AppLogic.ro_OK;

            /***************************************************************
            *  2. Process the Receipt
            * *************************************************************/

            // NOTE : 
            //  Process only the receipt if the Payment Method is Credit Card
            //  If the Payment Method is Cash or Cheque, we only create a Sales Order
            var term = PaymentTermDTO.Find(ThisCustomer.PaymentTermCode);

            if (term.PaymentTermCode != AppLogic.AppConfig("PaymentTermCodeZeroDollarOrder") &&
                false == "REQUEST QUOTE".Equals(term.PaymentTermCode.ToUpperInvariant()) &&
                false == "PURCHASE ORDER".Equals(term.PaymentTermCode.ToUpperInvariant()) &&
                term.PaymentMethod != DomainConstants.PAYMENT_METHOD_CREDITCARD)
            {
                status = ProcessOtherPaymentType(convertToInvoiceAndCapture, billingAddress, receiptCode, salesOrderCode, amountDeduction);
                return status;
            }


            // Credit Card Payment
            if (term.PaymentTermCode != AppLogic.AppConfig("PaymentTermCodeZeroDollarOrder") &&
                false == "REQUEST QUOTE".Equals(term.PaymentTermCode.ToUpperInvariant()) &&
                false == "PURCHASE ORDER".Equals(term.PaymentTermCode.ToUpperInvariant()) &&
                term.PaymentMethod == DomainConstants.PAYMENT_METHOD_CREDITCARD)
            {
                var gatewayReceiptDataset = new ReceiptDatasetGateway();
                var facadeReceipt = new ReceiptFacade(gatewayReceiptDataset);

                var facadeCustomerReceiptList = new ListControlFacade();
                var datasetCustomerReceipt = new DataSet();
                var customerReceiptBaseDataset = new BaseDataset();

                // retrieve the customer information
                string customerInfoQuery = string.Format("CustomerCode = {0}",
                                            DB.SQuote(CommonLogic.IIF(ThisCustomer.IsNotRegistered, ThisCustomer.AnonymousCustomerCode, ThisCustomer.CustomerCode)));

                //deadlock
                int nooftries = 0;
                while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
                {
                    try
                    {
                        datasetCustomerReceipt = facadeCustomerReceiptList.ReadSearchResultsData("CustomerActiveCustomersView",
                                                    customerInfoQuery,
                                                    1,
                                                    false,
                                                    string.Empty,
                                                    ref customerReceiptBaseDataset,
                                                    true);
                        break;
                    }
                    catch (SqlException ex)
                    {
                        if (ex.ErrorCode == 1205)
                        {
                            nooftries += 1;
                        }
                        else
                        {
                            throw;
                        }
                    }
                }

                string cardExtraCode = AppLogic.GetCardExtraCodeFromSession(ThisCustomer);

                DataRow rowCustomer = customerReceiptBaseDataset.Tables["CustomerActiveCustomersView"].Rows[0];

                facadeReceipt.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt;

                if (is3DsecondAuth)
                {
                    DataSet ds = Interprise.Facade.Base.SimpleFacade.Instance.LoadDataSet(CommandType.StoredProcedure,
                                                                                          "EcommerceGetCustomerPayment",
                                                                                           new string[] { Interprise.Framework.Customer.Shared.Const.CUSTOMERTRANSACTIONRECEIPTVIEW_TABLE, 
                                                                                                          Const.CUSTOMERPAYMENT_TABLE, Const.CUSTOMERCARDPAYMENT_TABLE, Const.PAYMENTMETHODVIEW_TABLE, 
                                                                                                          "DefaultAccount", "HasReserved", Interprise.Framework.Customer.Shared.Const.CUSTOMERTRADINGINFOVIEW_TABLE },
                                                                                           new string[][] { new string[] {"@DocumentCode", salesOrderCode},
                                                                                                            new string[] {"@CustomerCode", ThisCustomer.CustomerCode}},
                                                                                           Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                    if (ds.Tables.Contains(Interprise.Framework.Customer.Shared.Const.CUSTOMERTRANSACTIONRECEIPTVIEW_TABLE))
                    {
                        gatewayReceiptDataset.Merge(ds.Tables[Interprise.Framework.Customer.Shared.Const.CUSTOMERTRANSACTIONRECEIPTVIEW_TABLE]);
                    }
                    _facadeSalesOrder.IsTransactionReceiptLoaded = true;
                    if (ds.Tables.Contains("DefaultAccount") && ds.Tables["DefaultAccount"].Rows.Count > 0 & ds.Tables.Contains("HasReserved"))
                    {
                        facadeReceipt.SetSettings(Convert.ToString(ds.Tables["DefaultAccount"].Rows[0][0]), ds.Tables["HasReserved"].Rows.Count > 0);
                    }

                    gatewayReceiptDataset.PaymentMethodView.Clear();
                    gatewayReceiptDataset.CustomerPayment.Clear();
                    gatewayReceiptDataset.CustomerCardPayment.Clear();

                    facadeReceipt.RemovePaymentTableRelation();

                    if (ds.Tables.Contains(Interprise.Framework.Base.Shared.Const.CUSTOMERPAYMENT_TABLE))
                    {
                        gatewayReceiptDataset.Merge(ds.Tables[Interprise.Framework.Base.Shared.Const.CUSTOMERPAYMENT_TABLE]);
                    }
                    if (ds.Tables.Contains(Interprise.Framework.Base.Shared.Const.CUSTOMERCARDPAYMENT_TABLE))
                    {
                        gatewayReceiptDataset.Merge(ds.Tables[Interprise.Framework.Base.Shared.Const.CUSTOMERCARDPAYMENT_TABLE]);
                    }
                    if (ds.Tables.Contains(Const.PAYMENTMETHODVIEW_TABLE))
                    {
                        gatewayReceiptDataset.Merge(ds.Tables[Const.PAYMENTMETHODVIEW_TABLE]);
                    }

                    if (!facadeReceipt.DisableRelationInitialization)
                    {
                        facadeReceipt.InitializePaymentTableRelation();
                    }

                    facadeReceipt.RestoreCV();
                }
                else
                {
                    facadeReceipt.AddReceipt(new DataRow[] { rowCustomer },
                    Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt,
                    true,
                    _facadeSalesOrder.GetCalculatedTotalDue(),
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].PaymentTermCode,
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode,
                    _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ContactCode);
                }

                gatewayReceiptDataset.CustomerCardPayment[0].CardPaymentCode = receiptCode;

                var rowPayMethod = gatewayReceiptDataset.PaymentMethodView[0];

                if (ThisCustomer.IsNotRegistered)
                {
                    // NOTE:
                    //  This should just set the default values
                    facadeReceipt.AssignPaymentMethodDefaults(
                        rowPayMethod,
                        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].PaymentTermCode
                    );

                    // we then will manually assign the credit card details...
                    rowPayMethod.BeginEdit();

                    // NOTE: 
                    //  settting IsCardOnFile to false MUST come first
                    //  Order is important here since internally it modifies other fields
                    //  Setting this column's value at the last would result in a different behaviour
                    rowPayMethod.IsCardOnFile = false;

                    // get payment type detail for IsDeposited
                    string isDeposited = Boolean.TrueString;
                    isDeposited = facadeReceipt.GetField("IsDeposited",
                                                          Interprise.Framework.Base.Shared.Const.SYSTEMPAYMENTTYPE_TABLE,
                                                          String.Format("{0}='{1}'", Interprise.Framework.Base.Shared.Const.SYSTEMPAYMENTTYPE_PAYMENTTYPECODE_COLUMN,
                                                          Convert.ToString(Interprise.Framework.Base.Shared.Common.IsNull(gatewayReceiptDataset.PaymentMethodView[0][gatewayReceiptDataset.PaymentMethodView.PaymentTypeCodeColumn.ColumnName], String.Empty))));

                    if (isDeposited == null || String.Compare(isDeposited, Boolean.FalseString, true) == 0)
                    {
                        rowPayMethod.Account = Interprise.Framework.Base.Shared.Const.PAYMENT_ACCOUNT_UNDEPOSITED;
                    }
                    else
                    {
                        rowPayMethod.Account = Interprise.Framework.Base.Shared.Const.PAYMENT_ACCOUNT;
                    }

                    rowPayMethod.CreditCardCode = String.Empty;
                    rowPayMethod.CreditCardSalt = billingAddress.CardNumberSalt;
                    rowPayMethod.CreditCardEmail = billingAddress.EMail;
                    rowPayMethod.CreditCardIV = billingAddress.CardNumberIV;
                    rowPayMethod.CreditCardName = billingAddress.CardName;

                    // NOTE:
                    // IS Always assume that the credit card being passed here is encrypted
                    // thus the need for the next line below
                    rowPayMethod.CreditCard = facadeReceipt.EncryptCardNumber(billingAddress.CardNumber, rowPayMethod);

                    rowPayMethod.CreditCardAddress = billingAddress.Address1;
                    rowPayMethod.CreditCardCity = billingAddress.City;
                    rowPayMethod.CreditCardState = billingAddress.State;
                    rowPayMethod.CreditCardPostalCode = billingAddress.PostalCode;

                    if (!billingAddress.Plus4.IsNullOrEmptyTrimmed())
                    {
                        rowPayMethod.CreditCardPlus4 = Convert.ToInt32(billingAddress.Plus4);
                    }

                    rowPayMethod.CreditCardCountry = billingAddress.Country;
                    if (ThisCustomer.IsNotRegistered && ThisCustomer.ThisCustomerSession["paypalfrom"] != null && (ThisCustomer.ThisCustomerSession["paypalfrom"] == "shoppingcart" || ThisCustomer.ThisCustomerSession["paypalfrom"] == "checkoutpayment" || ThisCustomer.ThisCustomerSession["paypalfrom"] == "onepagecheckout"))
                    {
                        DateTime now = DateTime.Now;
                        rowPayMethod.CreditCardExpMon = now.ToString("MMM");
                        rowPayMethod.CreditCardExpYear = now.AddYears(1).Year.ToString();
                    }
                    else
                    {
                        rowPayMethod.CreditCardExpMon = InterpriseHelper.ToInterpriseExpMonth(billingAddress.CardExpirationMonth);
                        rowPayMethod.CreditCardExpYear = billingAddress.CardExpirationYear;
                    }

                    if (AppLogic.AppConfigBool("ShowCardStartDateFields"))
                    {
                        rowPayMethod.CreditCardStartMon = InterpriseHelper.ToInterpriseExpMonth(billingAddress.CardStartMonth);
                        rowPayMethod.CreditCardStartYear = billingAddress.CardStartYear;

                        // same reason for credit card applies here
                        rowPayMethod.CreditCardIssueNumber = facadeReceipt.EncryptCardIssueNumber(billingAddress.CardIssueNumber, rowPayMethod);
                    }

                    rowPayMethod.CreditCardTelephone = billingAddress.Phone;
                    rowPayMethod.CreditCardTelephoneExtension = string.Empty;
                    rowPayMethod.CreditCardType = billingAddress.CardType;
                    rowPayMethod.CreditCardTypeDescription = billingAddress.CardType;
                    rowPayMethod.EndEdit();
                }
                else
                {
                    // Registered Customer..........

                    // Code inside this function should retrieve the card number
                    // information of the customer if it has a default one...
                    if (!is3DsecondAuth)
                    {
                        facadeReceipt.AssignPaymentMethodDefaults(
                            rowPayMethod,
                            _gatewaySalesOrderDataset.CustomerSalesOrderView[0].PaymentTermCode
                        );
                    }
                }


                // NOTE :   this should recompute for AmountPaid and AmountPaidRate
                //          both CustomerPayment and PaymentMethod table
                rowPayMethod.AmountPaidRate = _facadeSalesOrder.GetCalculatedTotalDue();
                rowPayMethod.CustomerIPAddress = ThisCustomer.LastIPAddress;

                if (amountDeduction > Decimal.Zero)
                {
                    rowPayMethod.AmountPaidRate = _facadeSalesOrder.GetCalculatedTotalDue() - amountDeduction;
                }


                string[] receiptRelatedTables = facadeReceipt.get_RelatedTables(Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt);
                string[][] receiptCommands = facadeReceipt.CreateParameterSet(receiptRelatedTables);

                facadeReceipt.UpdateDataSet(receiptCommands,
                                            Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt,
                                            string.Empty,
                                            false);

                //Capture the credit card number from the payment page and encrypt it so that the gateway can capture from that credit card
                rowPayMethod.CreditCardSalt = ThisCustomer.ThisCustomerSession["CardNumberSalt"];
                rowPayMethod.CreditCardIV = ThisCustomer.ThisCustomerSession["CardNumberIV"];
                rowPayMethod.CreditCard = ThisCustomer.ThisCustomerSession["CardNumber"];
                if (ThisCustomer.IsNotRegistered && ThisCustomer.ThisCustomerSession["paypalfrom"] != null && (ThisCustomer.ThisCustomerSession["paypalfrom"] == "shoppingcart" || ThisCustomer.ThisCustomerSession["paypalfrom"] == "checkoutpayment" || ThisCustomer.ThisCustomerSession["paypalfrom"] == "onepagecheckout"))
                {
                    DateTime now = DateTime.Now;
                    rowPayMethod.CreditCardExpMon = now.ToString("MMM");
                    rowPayMethod.CreditCardExpYear = now.AddYears(1).Year.ToString();
                }
                else
                {
                    rowPayMethod.CreditCardExpMon = InterpriseHelper.ToInterpriseExpMonth(billingAddress.CardExpirationMonth);
                    rowPayMethod.CreditCardExpYear = billingAddress.CardExpirationYear;
                }
                rowPayMethod.CreditCardCV = billingAddress.CardIssueNumber;

                // this will assign the Card On Name to Credit Card Gateway.
                rowPayMethod.CreditCardName = billingAddress.CardName;


                rowPayMethod.CreditCardCV = cardExtraCode;
                rowPayMethod.CreditCardTransactionType = "Auth";

                rowPayMethod.CreditCardCountry = billingAddress.Country;
                rowPayMethod.CreditCardAddress = billingAddress.Address1;
                rowPayMethod.CreditCardCity = billingAddress.City;
                rowPayMethod.CreditCardState = billingAddress.State;
                rowPayMethod.CreditCardPostalCode = billingAddress.PostalCode;
                rowPayMethod.CreditCardTelephone = billingAddress.Phone;
                rowPayMethod.CreditCardEmail = billingAddress.EMail;

                if (!billingAddress.Plus4.IsNullOrEmptyTrimmed())
                {
                    rowPayMethod.CreditCardPlus4 = Convert.ToInt32(billingAddress.Plus4);
                }

                //if (AppLogic.AppConfigBool("ShowCardStartDateFields"))
                //{
                //    string cardIssueNumber = ThisCustomer.ThisCustomerSession["CardIssueNumber"].ToString();
                //    string cardIssueNumberSalt = ThisCustomer.ThisCustomerSession["CardIssueNumberSalt"].ToString();
                //    string cardIsseuNumberIV = ThisCustomer.ThisCustomerSession["CardIssueNumberIV"].ToString();
                //    Interprise.Licensing.Base.Services.CryptoServiceProvider cryptoService = new Interprise.Licensing.Base.Services.CryptoServiceProvider();
                //    cardIssueNumber = cryptoService.Decrypt(Convert.FromBase64String(cardIssueNumber), Convert.FromBase64String(cardIssueNumberSalt), Convert.FromBase64String(cardIsseuNumberIV));
                //    rowPayMethod.CreditCardIssueNumber = cardIssueNumber;
                //}

                //Credit Card Tokenization...............
                if (AppLogic.IsUsingInterpriseGatewayv2() && ThisCustomer.IsRegistered)
                {
                    //Check if billing has existing reference no.
                    #region GetReferenceNo
                    using (var con = DB.NewSqlConnection())
                    {
                        con.Open();
                        using (var reader = DB.GetRSFormat(con, "SELECT InterpriseGatewayRefNo FROM CustomerCreditCard with (NOLOCK) WHERE CreditCardCode={0}", DB.SQuote(billingAddress.AddressID)))
                        {
                            if (reader.Read())
                            {
                                rowPayMethod.InterpriseGatewayRefNo = DB.RSFieldInt(reader, "InterpriseGatewayRefNo");
                            }
                        }
                    }
                    #endregion

                    rowPayMethod.IsCardOnFile = AppLogic.IsSaveCardInfoChecked(ThisCustomer); //if true, refno will be generated upon successful authorzation

                    if (rowPayMethod.InterpriseGatewayRefNo > 0)
                    {
                        rowPayMethod.IsCardOnFile = false; //set to false since refno already exists

                        if (!AppLogic.AppConfigBool("AllowCreditCardInfoSaving"))
                        {
                            rowPayMethod.InterpriseGatewayRefNo = 0;
                            DB.ExecuteSQL("UPDATE CustomerCreditCard SET InterpriseGatewayRefNo=NULL, Vault=NULL WHERE CreditCardCode={0}", DB.SQuote(billingAddress.AddressID));
                        }
                    }

                    //override paymethod billing info
                    rowPayMethod.CreditCardName = billingAddress.CardName;
                    rowPayMethod.CreditCardType = billingAddress.CardType;
                    rowPayMethod.CreditCardStartMon = (billingAddress.CardStartMonth != string.Empty) ? InterpriseHelper.ToInterpriseExpMonth(billingAddress.CardStartMonth) : string.Empty;
                    rowPayMethod.CreditCardStartYear = billingAddress.CardStartYear;
                    rowPayMethod.CreditCardExpMon = InterpriseHelper.ToInterpriseExpMonth(billingAddress.CardExpirationMonth);
                    rowPayMethod.CreditCardExpYear = billingAddress.CardExpirationYear;

                    rowPayMethod.CreditCardCountry = billingAddress.Country;
                    rowPayMethod.CreditCardAddress = billingAddress.Address1;
                    rowPayMethod.CreditCardCity = billingAddress.City;
                    rowPayMethod.CreditCardState = billingAddress.State;
                    rowPayMethod.CreditCardPostalCode = billingAddress.PostalCode;
                    rowPayMethod.CreditCardTelephone = billingAddress.Phone;
                    rowPayMethod.CreditCardEmail = billingAddress.EMail;
                    rowPayMethod.CreditCardCode = billingAddress.AddressID;
                }
                //.....

                string gatewayMessage = string.Empty;
                bool approved = false;
                string gatewayResponseCode = String.Empty;
                bool is3DSecureFirstAuthFail = false;
                bool try3DSecure = false;

                if (withGatewayAuthResponse != null)
                {
                    approved = true;
                    AttachGatewayAuthorization(gatewayReceiptDataset, rowPayMethod, withGatewayAuthResponse, receiptCode);

                    // Set the next IS gateways that will process the capture part
                    // Since now we're just doing AUTH
                    if (!ThisCustomer.ThisCustomerSession["paypalfrom"].IsNullOrEmptyTrimmed())
                    {
                        rowPayMethod.CreditCardGatewayAssemblyName = "Interprise.Presentation.Customer.PaymentGateway.PayPal";
                        rowPayMethod.CreditCardGatewayType = "Interprise.Presentation.Customer.PaymentGateway.PayPal.PayPalGatewayControl";
                    }

                    facadeReceipt.UpdateDataSet(new string[][] { 
                            new string[] { gatewayReceiptDataset.CustomerCardPayment.TableName, 
                                "CreateCustomerCardPayment", 
                                "UpdateCustomerCardPayment", 
                                "DeleteCustomerCardPayment" }, 
                            new string[] { gatewayReceiptDataset.CustomerPayment.TableName, 
                                "CreateCustomerPayment", 
                                "UpdateCustomerPayment", 
                                "DeleteCustomerPayment" }, 
                            new string[] { gatewayReceiptDataset.PaymentMethodView.TableName, 
                                "CreatePaymentMethod", 
                                "UpdatePaymentMethod", 
                                "DeletePaymentMethod" }, 
                            new string[] { gatewayReceiptDataset.CustomerCCAuthResponse.TableName, 
                                "CreateCustomerCCAuthResponse", 
                                "UpdateCustomerCCAuthResponse", 
                                "DeleteCustomerCCAuthResponse" } },
                            facadeReceipt.TransactionType,
                            "Processed credit card payment.",
                            false);

                    status = AppLogic.ro_OK;
                }
                else
                {
                    string prefferredGateway = GetPreferredGateway();

                    string merchantLogin = DB.GetSqlS(String.Format("SELECT MerchantLogin AS S FROM EcommerceSite WHERE WebsiteCode = {0}", InterpriseHelper.ConfigInstance.WebSiteCode.ToDbQuote()));
                    if (!merchantLogin.IsNullOrEmptyTrimmed())
                    {
                        rowPayMethod.WebsiteCode = InterpriseHelper.ConfigInstance.WebSiteCode;
                    }

                    using (var facadeCard = new CreditCardAuthorizationFacade())
                    {
                        var gatewayInfo = ServiceFactory.GetInstance<IPaymentTermService>().GetGatewayInterface();

                        var cardGateway = facadeCard.GetGateway(gatewayInfo[0], gatewayInfo[1]);
                        if (null == cardGateway) throw new Exception("Could not load payment Gateway!!!");

                        rowPayMethod.CreditCardGatewayAssemblyName = cardGateway.CreditCardGatewayAssemblyName;
                        rowPayMethod.CreditCardGatewayType = cardGateway.CreditCardGatewayType;

                        facadeCard.TransactionType = facadeReceipt.TransactionType;

                        if (prefferredGateway == AppLogic.ro_PROTX)
                        {
                            facadeCard.IsEcommerce = true;
                            try3DSecure = CommonLogic.StringInCommaDelimitedStringList(billingAddress.CardType, AppLogic.AppConfig("3DSECURE.CreditCardTypes"));
                            if (try3DSecure)
                            {
                                facadeCard.Is3DSecure = try3DSecure;
                                if (is3DsecondAuth)
                                {
                                    facadeCard.Secure3DMD = ThisCustomer.ThisCustomerSession["3DSecure.MD"];
                                    facadeCard.Secure3DPAReq = ThisCustomer.ThisCustomerSession["3Dsecure.PaRes"];
                                }
                            }
                        }

                        gatewayMessage = facadeCard.AuthorizeCreditCard(facadeReceipt, false, ref approved);
                        if (merchantLogin.IsNullOrEmptyTrimmed())
                        {
                            rowPayMethod.WebsiteCode = InterpriseHelper.ConfigInstance.WebSiteCode;
                        }

                        gatewayResponseCode = facadeReceipt.CurrentDataset.Tables["CustomerCCAuthResponse"].Rows[0]["GatewayResponseCode"].ToString();
                        if (prefferredGateway == AppLogic.ro_PROTX && try3DSecure && gatewayResponseCode != "3DAUTH" && !is3DsecondAuth)
                        {
                            is3DSecureFirstAuthFail = true;
                        }

                        if (prefferredGateway == AppLogic.ro_PROTX && facadeCard.Is3DSecure && !is3DsecondAuth)
                        {
                            if (facadeCard.Secure3DMD != null && facadeCard.Secure3DACSURL != null && facadeCard.Secure3DPAReq != null)
                            {
                                ThisCustomer.ThisCustomerSession["3DSecure.MD"] = facadeCard.Secure3DMD;
                                ThisCustomer.ThisCustomerSession["3DSecure.ACSUrl"] = facadeCard.Secure3DACSURL;
                                ThisCustomer.ThisCustomerSession["3DSecure.PAReq"] = facadeCard.Secure3DPAReq;
                                ThisCustomer.ThisCustomerSession["3DSecure.CustomerID"] = ThisCustomer.ContactGUID.ToString();
                                ThisCustomer.ThisCustomerSession["3DSecure.OrderNumber"] = salesOrderCode;
                                ThisCustomer.ThisCustomerSession["3DSecure.XID"] = receiptCode;
                            }
                        }

                        //Credit Card Tokenization........
                        if (cardGateway.PnRefNo > 0 && AppLogic.IsUsingInterpriseGatewayv2() && gatewayMessage.IsNullOrEmptyTrimmed() && ThisCustomer.IsRegistered)
                        {
                            //update reference no and vault manually
                            #region Update ReferenceNo
                            if (AppLogic.IsSaveCardInfoChecked(ThisCustomer))
                            {
                                DB.ExecuteSQL("UPDATE CustomerCreditCard SET InterpriseGatewayRefNo={0}, Vault='NMI' WHERE CreditCardCode={1}",
                                    DB.SQuote(cardGateway.PnRefNo.ToString()),
                                    DB.SQuote(billingAddress.AddressID));
                            }
                            #endregion

                            ThisCustomer.ThisCustomerSession["SaveCreditCardChecked"] = string.Empty;
                        }
                        //.....

                        cardGateway.Dispose();
                    }

                    if (!approved)
                    {
                        if (prefferredGateway == AppLogic.ro_PROTX && !is3DsecondAuth && try3DSecure && !is3DSecureFirstAuthFail)
                        {
                            status = AppLogic.ro_3DSecure;
                        }
                        else
                        {
                            string message = facadeReceipt.CurrentDataset.Tables["CustomerCCAuthResponse"].Rows[0]["ResponseMsg"].ToString();
                            ThisCustomer.LastGatewayErrorMessage = (message.IsNullOrEmptyTrimmed() && !gatewayMessage.IsNullOrEmptyTrimmed()) ?
                                                                    gatewayMessage :
                                                                    facadeReceipt.GetLastCreditCardProcessMessage();

                            if (prefferredGateway == AppLogic.ro_PROTX && is3DsecondAuth)
                            {
                                switch (gatewayResponseCode.ToUpperInvariant())
                                {
                                    case "ERROR":
                                        status = "The transaction encountered an error. Please try again";
                                        break;
                                    case "INVALID":
                                        status = "The card was not accepted. Please try again";
                                        break;
                                    case "NOTAUTHED":
                                        status = "Your card was not authorized for that amount. Please try again";
                                        break;
                                    case "REJECTED":
                                        status = "Your card was not not accepted. Please try again";
                                        break;
                                    default:
                                        status = message;
                                        break;
                                }
                            }
                            else
                            {
                                status = AppLogic.ro_INTERPRISE_GATEWAY_AUTHORIZATION_FAILED;
                            }

                            string gateway = ServiceFactory.GetInstance<IPaymentTermRepository>()
                                                           .GetGateWayDescriptionByGateway(prefferredGateway);

                            if (gateway.IsNullOrEmptyTrimmed()) gateway = "Gateway";

                            LogTransactionFailed(ThisCustomer.CustomerCode,
                                               _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode,
                                               gateway,
                                               facadeReceipt.GetLastCreditCardProcessMessage() + gatewayMessage);

                        }
                    }
                }


                /*****************************************************
                 * 3. Associate the receipt with the Sales Order
                *****************************************************/
                var gatewayTransactionReceiptDataset = _facadeSalesOrder.TransactionReceiptListFacade.CurrentDataset as TransactionReceiptDatasetGateway;
                var facadeTransactionReceipt = _facadeSalesOrder.TransactionReceiptListFacade as TransactionReceiptFacade;
                int datasourceRowIndex = 0;
                facadeTransactionReceipt.AssignTransactionReceipt(_gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode, facadeReceipt, ref datasourceRowIndex);
                // Manually merge the Customer Transaction Receipt table..
                _gatewaySalesOrderDataset.Merge(gatewayTransactionReceiptDataset.CustomerTransactionReceiptView);
                SaveSalesOrder();
                facadeTransactionReceipt.ReserveAndAllocateReceipt(_gatewaySalesOrderDataset, null, Interprise.Framework.Base.Shared.Const.CUSTOMER_SALES_ORDER);

                if (!approved)
                {
                    if (status != AppLogic.ro_3DSecure)
                    {
                        facadeTransactionReceipt.Void(_facadeSalesOrder.TransactionType.ToString(), _gatewaySalesOrderDataset);
                        _facadeSalesOrder.VoidOrder();

                        SaveSalesOrder();
                    }
                }

                if (approved && convertToInvoiceAndCapture)
                {
                    ConvertToInvoiceAndCapture(gatewayReceiptDataset, facadeReceipt, receiptCode);
                }



                // **************************************************
                //  Explicit Disposal and dereferencing goes here...
                // **************************************************

                facadeCustomerReceiptList.Dispose();
                datasetCustomerReceipt.Dispose();
                customerReceiptBaseDataset.Dispose();

                gatewayReceiptDataset.Dispose();
                gatewayTransactionReceiptDataset.Dispose();
                facadeTransactionReceipt.Dispose();
                facadeReceipt.Dispose();

                facadeCustomerReceiptList = null;
                datasetCustomerReceipt = null;
                customerReceiptBaseDataset = null;

                gatewayReceiptDataset = null;
                gatewayTransactionReceiptDataset = null;
                facadeTransactionReceipt = null;
                facadeReceipt = null;
            }

            return status;
        }

        private void LogTransactionFailed(string customerCode, string salesOrderCode, string gatewayName, string message)
        {
            var transactionFailed = FailedTransaction.New();
            transactionFailed.CustomerCode = customerCode;
            transactionFailed.SalesOrderCode = salesOrderCode;
            transactionFailed.PaymentGateway = gatewayName;
            transactionFailed.PaymentMethod = DomainConstants.PAYMENT_METHOD_CREDITCARD;
            transactionFailed.TransactionCommand = FailedTransaction.NOT_APPLICABLE;
            transactionFailed.TransactionResult = message;
            transactionFailed.IPAddress = ThisCustomer.LastIPAddress;
            transactionFailed.IsFailed = true;
            transactionFailed.Record();
        }


        private string ProcessOtherPaymentType(bool convertToInvoiceAndCapture,
              Address billingAddress,
              string receiptCode,
              string salesOrderCode,
              decimal amountDeduction)
        {
            string status = AppLogic.ro_OK;

            /***************************************************************
            *  2. Process the Receipt
            * *************************************************************/

            // NOTE : 
            //  Process only the receipt if the Payment Method is Credit Card
            //  If the Payment Method is Cash or Cheque, we only create a Sales Order
            var term = PaymentTermDTO.Find(ThisCustomer.PaymentTermCode);

            if (term.PaymentTermCode != AppLogic.AppConfig("PaymentTermCodeZeroDollarOrder") &&
                false == "REQUEST QUOTE".Equals(term.PaymentTermCode.ToUpperInvariant()) &&
                false == "PURCHASE ORDER".Equals(term.PaymentTermCode.ToUpperInvariant()) &&
                term.PaymentMethod != DomainConstants.PAYMENT_METHOD_CREDITCARD)
            {
                var gatewayReceiptDataset = new ReceiptDatasetGateway();
                var facadeReceipt = new ReceiptFacade(gatewayReceiptDataset);

                var facadeCustomerReceiptList = new ListControlFacade();
                var datasetCustomerReceipt = new DataSet();
                var customerReceiptBaseDataset = new BaseDataset();

                // retrieve the customer information
                string customerInfoQuery = string.Format("CustomerCode = {0}",
                                            DB.SQuote(CommonLogic.IIF(ThisCustomer.IsNotRegistered, ThisCustomer.AnonymousCustomerCode, ThisCustomer.CustomerCode)));

                //deadlock
                int nooftries = 0;
                while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
                {
                    try
                    {
                        datasetCustomerReceipt = facadeCustomerReceiptList.ReadSearchResultsData("CustomerActiveCustomersView",
                                                    customerInfoQuery,
                                                    1,
                                                    false,
                                                    string.Empty,
                                                    ref customerReceiptBaseDataset,
                                                    true);
                        break;
                    }
                    catch (SqlException ex)
                    {
                        if (ex.ErrorCode == 1205)
                        {
                            nooftries += 1;
                        }
                        else
                        {
                            throw;
                        }
                    }
                }


                DataRow rowCustomer = customerReceiptBaseDataset.Tables["CustomerActiveCustomersView"].Rows[0];

                facadeReceipt.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt;

                facadeReceipt.AddReceipt(new DataRow[] { rowCustomer },
                Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt,
                true,
                _facadeSalesOrder.GetCalculatedTotalDue(),
                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].PaymentTermCode,
                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode,
                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ContactCode);

                var rowPayMethod = gatewayReceiptDataset.PaymentMethodView[0];
                rowPayMethod.CustomerIPAddress = ThisCustomer.LastIPAddress;

                if (ThisCustomer.IsNotRegistered)
                {
                    // NOTE:
                    //  This should just set the default values
                    facadeReceipt.AssignPaymentMethodDefaults(
                        rowPayMethod,
                        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].PaymentTermCode
                    );

                    // we then will manually assign the credit card details...
                    rowPayMethod.BeginEdit();

                    // NOTE: 
                    //  settting IsCardOnFile to false MUST come first
                    //  Order is important here since internally it modifies other fields
                    //  Setting this column's value at the last would result in a different behaviour
                    rowPayMethod.IsCardOnFile = false;

                    // get payment type detail for IsDeposited
                    string isDeposited = Boolean.TrueString;
                    isDeposited = facadeReceipt.GetField("IsDeposited",
                                                          Interprise.Framework.Base.Shared.Const.SYSTEMPAYMENTTYPE_TABLE,
                                                          String.Format("{0}='{1}'", Interprise.Framework.Base.Shared.Const.SYSTEMPAYMENTTYPE_PAYMENTTYPECODE_COLUMN,
                                                          Convert.ToString(Interprise.Framework.Base.Shared.Common.IsNull(gatewayReceiptDataset.PaymentMethodView[0][gatewayReceiptDataset.PaymentMethodView.PaymentTypeCodeColumn.ColumnName], String.Empty))));

                    if (isDeposited == null || String.Compare(isDeposited, Boolean.FalseString, true) == 0)
                    {
                        rowPayMethod.Account = Interprise.Framework.Base.Shared.Const.PAYMENT_ACCOUNT_UNDEPOSITED;
                    }
                    else
                    {
                        rowPayMethod.Account = Interprise.Framework.Base.Shared.Const.PAYMENT_ACCOUNT;
                    }

                    rowPayMethod.CreditCardTelephone = billingAddress.Phone;
                    rowPayMethod.CreditCardTelephoneExtension = string.Empty;
                    rowPayMethod.CreditCardType = billingAddress.CardType;
                    rowPayMethod.CreditCardTypeDescription = billingAddress.CardType;
                    rowPayMethod.EndEdit();
                }
                else
                {
                    // Registered Customer..........
                    facadeReceipt.AssignPaymentMethodDefaults(
                        rowPayMethod,
                        _gatewaySalesOrderDataset.CustomerSalesOrderView[0].PaymentTermCode
                    );
                }


                // NOTE :   this should recompute for AmountPaid and AmountPaidRate
                //          both CustomerPayment and PaymentMethod table
                rowPayMethod.AmountPaidRate = _facadeSalesOrder.GetCalculatedTotalDue();

                if (amountDeduction > Decimal.Zero)
                {
                    rowPayMethod.AmountPaidRate = _facadeSalesOrder.GetCalculatedTotalDue() - amountDeduction;
                }


                string[] receiptRelatedTables = facadeReceipt.get_RelatedTables(Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt);
                string[][] receiptCommands = facadeReceipt.CreateParameterSet(receiptRelatedTables);

                facadeReceipt.UpdateDataSet(receiptCommands,
                                            Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt,
                                            string.Empty,
                                            false);
                /*****************************************************
                 * 3. Associate the receipt with the Sales Order
                *****************************************************/
                var gatewayTransactionReceiptDataset = _facadeSalesOrder.TransactionReceiptListFacade.CurrentDataset as TransactionReceiptDatasetGateway;
                var facadeTransactionReceipt = _facadeSalesOrder.TransactionReceiptListFacade as TransactionReceiptFacade;
                int datasourceRowIndex = 0;
                facadeTransactionReceipt.AssignTransactionReceipt(_gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode, facadeReceipt, ref datasourceRowIndex);
                // Manually merge the Customer Transaction Receipt table..
                _gatewaySalesOrderDataset.Merge(gatewayTransactionReceiptDataset.CustomerTransactionReceiptView);
                SaveSalesOrder();

                //get receivablecode
                if (gatewayReceiptDataset.CustomerPayment.Rows.Count > 0)
                {
                    receiptCode = gatewayReceiptDataset.CustomerPayment[0].ReceivableCode;
                }

                facadeTransactionReceipt.ReserveAndAllocateReceipt(_gatewaySalesOrderDataset, null, Interprise.Framework.Base.Shared.Const.CUSTOMER_SALES_ORDER);

                if (convertToInvoiceAndCapture)
                {
                    ConvertToInvoice(gatewayReceiptDataset, facadeReceipt, receiptCode);
                }


                // **************************************************
                //  Explicit Disposal and dereferencing goes here...
                // **************************************************

                gatewayTransactionReceiptDataset.Dispose();
                facadeTransactionReceipt.Dispose();
                gatewayTransactionReceiptDataset = null;
                facadeTransactionReceipt = null;

            }

            return status;
        }

        private void AttachGatewayAuthorization(ReceiptDatasetGateway gatewayReceiptDataset,
            ReceiptDatasetGateway.PaymentMethodViewRow rowPaymentMethod,
            GatewayResponse withGatewayAuthResponse,
            string receiptCode)
        {
            // MAP Interprise Needed Fields here..
            ReceiptDatasetGateway.CustomerCCAuthResponseRow rowResponse = gatewayReceiptDataset.CustomerCCAuthResponse.NewCustomerCCAuthResponseRow();
            rowResponse.BeginEdit();
            rowResponse.ResponseCode = SimpleFacade.Instance.GenerateDocumentCode(Interprise.Framework.Base.Shared.Enum.TransactionType.CCResponse.ToString());
            rowResponse.CustomerCode = _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToCode;
            rowResponse.DocumentCode = gatewayReceiptDataset.CustomerCardPayment[0].CardPaymentCode;
            rowResponse.TransactionType = "Auth";
            //rowResponse.CreditCard = rowPaymentMethod.CreditCardOnFile;
            rowResponse.Amount = rowPaymentMethod.AmountPaidRate;
            rowResponse.GatewayResponseCode = withGatewayAuthResponse.Status;
            rowResponse.ResponseMsg = withGatewayAuthResponse.Details;
            rowResponse.Message = withGatewayAuthResponse.Details;
            rowResponse.Result = 0;
            rowResponse.AuthCode = withGatewayAuthResponse.AuthorizationCode;
            rowResponse.Reference = withGatewayAuthResponse.AuthorizationTransID;
            rowResponse.AVSResult = withGatewayAuthResponse.AVSResult;
            rowResponse.CVResult = withGatewayAuthResponse.CV2Result;
            rowResponse.TransactionCommand = withGatewayAuthResponse.TransactionCommandOut;
            rowResponse.AuthorizationResult = withGatewayAuthResponse.TransactionResponse;
            rowResponse.EndEdit();

            gatewayReceiptDataset.CustomerCCAuthResponse.AddCustomerCCAuthResponseRow(rowResponse);

            rowPaymentMethod.BeginEdit();
            rowPaymentMethod.CreditCardReference = withGatewayAuthResponse.AuthorizationTransID;
            rowPaymentMethod.CreditCardResponseCode = rowResponse.ResponseCode;
            rowPaymentMethod.CreditCardAuthorizationCode = withGatewayAuthResponse.AuthorizationCode;
            // NOTE:
            //  We only do auth only for now
            rowPaymentMethod.CreditCardIsAuthorized = true;
            rowPaymentMethod.CreditCardIsVoided = false;
            rowPaymentMethod.CreditCardIsCredited = false;
            rowPaymentMethod.EndEdit();
        }

        private void AssignDownloadableItemCustomerAssociations()
        {
            this.CartItems.Where(item => item.ItemType == Const.ITEM_TYPE_ELECTRONIC_DOWNLOAD)
                          .ForEach(item =>
                          {
                              var download = DownloadableItem.FindByItemCode(item.ItemCode);
                              if (null != download)
                              {
                                  // allow this customer to download this item..
                                  download.AddCustomer(ThisCustomer);
                              }
                          });
        }

        private void AssignOptionalNotesAsDescriptionToCartItems()
        {
            for (int i = 0; i < m_CartItems.Count; i++)
            {
                if (m_CartItems[i].m_Notes.IsNullOrEmptyTrimmed()) { continue; }

                DataRow row = m_CartItems[i].AssociatedLineItemRow;
                if (row != null)
                {
                    row.BeginEdit();
                    row[SalesOrderDataset.CustomerSalesOrderDetailView.ItemDescriptionColumn.ColumnName] = m_CartItems[i].m_Notes;
                    row.EndEdit();
                }
            }

            if (SalesOrderDataset.CustomerSalesOrderDetailView.GetChanges(DataRowState.Modified) != null)
            {
                string[][] commands = new string[][] { new string[]{ SalesOrderDataset.CustomerSalesOrderDetailView.TableName,
                Interprise.Framework.Base.Shared.StoredProcedures.CREATECUSTOMERSALESORDERDETAIL,
                Interprise.Framework.Base.Shared.StoredProcedures.UPDATECUSTOMERSALESORDERDETAIL,
                Interprise.Framework.Base.Shared.StoredProcedures.DELETECUSTOMERSALESORDERDETAIL}};
                _facadeSalesOrder.UpdateDataSet(commands, _facadeSalesOrder.TransactionType, string.Empty, false);
            }
        }

        public string PlaceOrder(Gateway usingThisGateway,
            Address billingAddress,
            Address shippingAddress,
            ref string salesOrderCode,
            ref string receiptCode,
            bool clearTransaction,
            bool clearLineItems,
            bool is3DsecondAuth)
        {
            return PlaceOrder(usingThisGateway,
                billingAddress,
                shippingAddress,
                ref salesOrderCode,
                ref receiptCode,
                clearTransaction,
                clearLineItems,
                null,
                true,
                is3DsecondAuth);
        }

        public string PlaceOrder(Gateway usingThisGateway,
            Address billingAddress,
            Address shippingAddress,
            ref string salesOrderCode,
            ref string receiptCode,
            bool clearTransaction,
            bool clearLineItems,
            GatewayResponse response,
            bool applyPayment,
            bool is3DsecondAuth)
        {
            string status = AppLogic.ro_OK;

            //Change Transaction Type to Quote if "REQUEST QUOTE"
            if ((ThisCustomer.PaymentTermCode.ToUpperInvariant() == "REQUEST QUOTE"))
            {
                _facadeSalesOrder.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.Quote;
            }

            // NOTE:
            // A possible instance of salesOrderCode not being empty
            // is if it's coming from a 3DSecure callback
            if (CommonLogic.IsStringNullOrEmpty(salesOrderCode))
            {
                salesOrderCode = _facadeSalesOrder.GenerateDocumentCode(_facadeSalesOrder.TransactionType.ToString());
            }

            // NOTE:
            // A possible instance of receiptCode not being empty
            // is if it's coming from a 3DSecure callback            
            var term = PaymentTermDTO.Find(ThisCustomer.PaymentTermCode);
            if (CommonLogic.IsStringNullOrEmpty(receiptCode))
            {
                if (term.PaymentMethod == DomainConstants.PAYMENT_METHOD_CREDITCARD)
                {
                    receiptCode = SimpleFacade.Instance.GenerateDocumentCode(Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerCardPayment.ToString());
                }

            }

            if (AppLogic.AppConfigBool("MaxMind.Enabled") && term.PaymentMethod == DomainConstants.PAYMENT_METHOD_CREDITCARD)
            {
                if (ValidateOrderFraud(salesOrderCode, billingAddress, shippingAddress))
                {
                    return "MAXMIND FRAUD CHECK FAILED";
                }
            }

            decimal getExchangeRate = _facadeSalesOrder.GetExchangerate(ThisCustomer.CurrencyCode);
            //recompute the freight charge
            ComputeFreightCharge(false, getExchangeRate);

            if (usingThisGateway != null && term.PaymentMethod == DomainConstants.PAYMENT_METHOD_CREDITCARD)
            {
                decimal total = GetOrderTotal();
                string cv2 = AppLogic.GetCardExtraCodeFromSession(ThisCustomer);

                response = usingThisGateway.ProcessCard(salesOrderCode,
                            ThisCustomer,
                            total,
                            receiptCode,
                            "AUTH",
                            billingAddress,
                            cv2,
                            shippingAddress,
                            string.Empty,
                            string.Empty,
                            string.Empty);

                cv2 = "1111111";

                status = response.Status;

                if (status == AppLogic.ro_3DSecure)
                {
                    return response.Status;
                }
                else if (status != AppLogic.ro_OK)
                {
                    // record failed transaction
                    var transactionFailed = FailedTransaction.New();
                    transactionFailed.CustomerCode = ThisCustomer.CustomerCode;
                    if (is3DsecondAuth)
                    {
                        transactionFailed.SalesOrderCode = salesOrderCode;
                    }
                    transactionFailed.PaymentGateway = response.Gateway;
                    transactionFailed.PaymentMethod = DomainConstants.PAYMENT_METHOD_CREDITCARD;
                    transactionFailed.TransactionCommand = response.TransactionCommandOut;
                    transactionFailed.TransactionResult = response.TransactionResponse;
                    transactionFailed.IPAddress = ThisCustomer.LastIPAddress;
                    transactionFailed.IsFailed = true;

                    transactionFailed.Record();

                    if (AppLogic.AppConfigBool("ShowGatewayError"))
                    {
                        return AppLogic.ro_INTERPRISE_GATEWAY_AUTHORIZATION_FAILED;
                    }
                    else
                    {
                        return response.Status;
                    }
                }
            }

            TagOrder(salesOrderCode);
            AssignAffiliate();

            var registryAddress = Address.Get(ThisCustomer, AddressTypes.Shipping, FirstItem().m_ShippingAddressID, FirstItem().GiftRegistryID);
            if (!FirstItem().GiftRegistryID.HasValue) { registryAddress = null; }

            UpdateBillingAndShippingInformation(billingAddress, shippingAddress, registryAddress);

            // create payment to check if cc or payment info is authorized
            ReceiptDatasetGateway gatewayReceiptDataset = null;
            ReceiptFacade facadeReceipt = null;
            bool isApproved = false;

            //other payment types
            if (term.PaymentMethod != DomainConstants.PAYMENT_METHOD_CREDITCARD)
            {
                status = AppLogic.ro_OK; isApproved = true;
            }
            else
            {
                status = CreatePayment(billingAddress, response, is3DsecondAuth, receiptCode, salesOrderCode, ref gatewayReceiptDataset, ref facadeReceipt, ref isApproved);
            }

            if (status == AppLogic.ro_3DSecure)
            {
                return status;
            }

            if (status == AppLogic.ro_OK && isApproved)
            {
                SaveSalesOrderAndAllocate(salesOrderCode);

                if (this.HasGiftItems()) { TagCustomerGiftEmail(salesOrderCode); }

                if (_facadeSalesOrder.TransactionType != Interprise.Framework.Base.Shared.Enum.TransactionType.Quote)
                {
                    // Promote to Customer if still prospect..
                    InterpriseHelper.SetCustomerProspect(false, ThisCustomer.CustomerCode);
                }

                decimal cartTotalDeduction = Decimal.Zero;
                decimal cartTotal = _facadeSalesOrder.GetCalculatedTotalDue();

                ApplyOtherPayments(salesOrderCode, cartTotal, ref cartTotalDeduction);
                cartTotal -= cartTotalDeduction;

                //other payment types
                if (term.PaymentMethod != DomainConstants.PAYMENT_METHOD_CREDITCARD && AppLogic.AppConfigBool("CreateReceiptForAllPaymentType"))
                {
                    bool convertToInvoice = "AUTH CAPTURE".Equals(AppLogic.AppConfig("TransactionMode"), StringComparison.InvariantCultureIgnoreCase);
                    // this include conversion to invoice
                    status = ProcessOtherPaymentType(convertToInvoice, billingAddress, receiptCode, salesOrderCode, cartTotalDeduction);
                }

            }

            if (applyPayment && isApproved && term.PaymentMethod == DomainConstants.PAYMENT_METHOD_CREDITCARD)
            {
                bool convertToInvoiceAndCapture = "AUTH CAPTURE".Equals(AppLogic.AppConfig("TransactionMode"), StringComparison.InvariantCultureIgnoreCase);
                convertToInvoiceAndCapture = AssociatePayment(status, convertToInvoiceAndCapture, facadeReceipt, gatewayReceiptDataset, receiptCode);

                //dispose objects
                gatewayReceiptDataset.Dispose();
                facadeReceipt.Dispose();
                gatewayReceiptDataset = null;
                facadeReceipt = null;

                // voided invoice
                if (convertToInvoiceAndCapture) { return status; }
            }

            if (status == AppLogic.ro_3DSecure)
            {
                return status;
            }

            if (status == AppLogic.ro_OK)
            {
                AssignDownloadableItemCustomerAssociations();
                AssignOptionalNotesAsDescriptionToCartItems();
            }

            if (status != AppLogic.ro_OK)
            {
                return status;
            }

            if (status != AppLogic.ro_INTERPRISE_GATEWAY_AUTHORIZATION_FAILED)
            {
                if (clearTransaction)
                {
                    this.ClearTransaction();
                }

                if (clearLineItems)
                {
                    this.ClearLineItems();
                }
            }

            if (!ThisCustomer.ThisCustomerSession[DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS].IsNullOrEmptyTrimmed()) { ThisCustomer.ThisCustomerSession.ClearVal(DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS); }

            return status;
        }



        private void TagCustomerGiftEmail(string salesOrderCode)
        {
            var shoppingCartService = ServiceFactory.GetInstance<IShoppingCartService>();
            var giftEmails = shoppingCartService.GetShoppingCartGiftEmails().Where(i => !i.EmailRecipient.IsNullOrEmptyTrimmed());
            var groupedGiftEmails = giftEmails.GroupBy(i => new { i.ItemCode, i.LineNum, i.EmailRecipient })
                                              .Select(i => new ShoppingCartGiftEmailCustomModel()
                                              {
                                                  ItemCode = i.Key.ItemCode,
                                                  LineNum = i.Key.LineNum,
                                                  EmailRecipient = i.Key.EmailRecipient,
                                                  QuantityOrdered = i.Count()
                                              });
            if (groupedGiftEmails.Count() > 0)
            {
                shoppingCartService.CreateCustomerCartGiftEmail(salesOrderCode, groupedGiftEmails.ToList());
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (_gatewaySalesOrderDataset != null)
            {
                _gatewaySalesOrderDataset.Dispose();
            }

            if (_facadeSalesOrder != null)
            {
                _facadeSalesOrder.Dispose();
            }
        }

        #endregion

        private void ConvertToInvoiceAndCapture(ReceiptDatasetGateway gatewayReceiptDataset, ReceiptFacade facadeReceipt, string receiptCode)
        {
            _gatewaySalesOrderDataset.CustomerSalesOrderWorkflowView[0].Stage = Interprise.Framework.Customer.Shared.Const.WORKFLOW_STAGE_READY_TO_INVOICE;

            DataTable warehouseTable = _facadeSalesOrder.InitializeConvertion();
            _facadeSalesOrder.SelectAllItemsToInvoice(warehouseTable, true);

            InvoiceDatasetGateway tmpGatewayInvoiceDataset = null;
            string invoiceConversionErrorMessage = string.Empty;
            if (_facadeSalesOrder.ConvertSalesOrderToInvoice(
                    Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice,
                    ref tmpGatewayInvoiceDataset,
                    true,
                    false,
                    ref invoiceConversionErrorMessage))
            {
                if (null != tmpGatewayInvoiceDataset)
                {
                    using (var gatewayInvoiceDataset = new InvoiceDatasetGateway())
                    {
                        using (var facadeInvoice = new InvoiceFacade(gatewayInvoiceDataset))
                        {
                            facadeInvoice.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice;

                            string convertedInvoiceCode = tmpGatewayInvoiceDataset.CustomerInvoiceView[0].InvoiceCode;

                            string[][] paramSet = new string[][] { new string[] { "@InvoiceCode", convertedInvoiceCode } };

                            //unified load of data to avoid PK related error during update.
                            facadeInvoice.LoadDataSet(Interprise.Framework.Base.Shared.StoredProcedures.READCUSTOMERINVOICEUNIFIED,
                            new string[] {tmpGatewayInvoiceDataset.CustomerInvoiceView.TableName, tmpGatewayInvoiceDataset.CustomerInvoiceDetailView.TableName, gatewayInvoiceDataset.TransactionTaxDetailView.TableName, 
                                      tmpGatewayInvoiceDataset.TransactionItemTaxDetailView.TableName, tmpGatewayInvoiceDataset.CustomerCreditAllocationView.TableName, tmpGatewayInvoiceDataset.CustomerCreditView.TableName,
                                      tmpGatewayInvoiceDataset.CustomerItemKitDetailView.TableName, tmpGatewayInvoiceDataset.CustomerSalesRepCommissionView.TableName, tmpGatewayInvoiceDataset.TransactionTaxSummary.TableName,
                                      tmpGatewayInvoiceDataset.CustomerInvoiceSerialLotNumbers.TableName, tmpGatewayInvoiceDataset.CustomerInvoiceKitSerialLotNumbers.TableName},
                            paramSet,
                            Interprise.Framework.Base.Shared.Enum.ClearType.None,
                            Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                            // let's reload the receipt table
                            string[][] reloadReceiptCommandset = new string[][]{
                                                                    new string[]{
                                                                        Const.CUSTOMERCARDPAYMENT_TABLE,
                                                                        StoredProcedures.READCUSTOMERCARDPAYMENT, 
                                                                        "@CardPaymentCode",
                                                                        receiptCode},
                                                                    new string[]{
                                                                        Const.PAYMENTMETHODVIEW_TABLE,
                                                                        StoredProcedures.READPAYMENTMETHOD,
                                                                        "@ReceivableCode",
                                                                        receiptCode}};

                            facadeReceipt.LoadDataSet(reloadReceiptCommandset, Interprise.Framework.Base.Shared.Enum.ClearType.Specific, Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                            // generate GC serial if auto generate
                            var invoiceDetailGiftCodeRows = tmpGatewayInvoiceDataset.CustomerInvoiceDetailView.Select(String.Format("{0} IN ('{1}', '{2}')", Interprise.Framework.Base.Shared.Const.INVENTORYITEM_ITEMTYPE_COLUMN,
                                                                                                                                                             Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CARD,
                                                                                                                                                             Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CERTIFICATE));
                            bool hasAutoGenerateSerialNumberGiftCodes = false;
                            bool hasManualGenerateSerialNumberGiftCodes = false;
                            if (invoiceDetailGiftCodeRows != null && invoiceDetailGiftCodeRows.Length > 0)
                            {
                                foreach (DataRow invoiceDetailRow in invoiceDetailGiftCodeRows)
                                {
                                    if (!hasAutoGenerateSerialNumberGiftCodes)
                                    {
                                        if (facadeInvoice.AutoGenerate(invoiceDetailRow[Interprise.Framework.Base.Shared.Const.INVENTORYITEM_ITEMCODE_COLUMN].ToString()))
                                        {
                                            hasAutoGenerateSerialNumberGiftCodes = true;
                                            continue;
                                        }
                                    }

                                    if (!hasManualGenerateSerialNumberGiftCodes)
                                    {
                                        if (!facadeInvoice.AutoGenerate(invoiceDetailRow[Interprise.Framework.Base.Shared.Const.INVENTORYITEM_ITEMCODE_COLUMN].ToString()))
                                        {
                                            hasManualGenerateSerialNumberGiftCodes = true;
                                            continue;
                                        }
                                    }
                                }
                            }
							
							if (hasAutoGenerateSerialNumberGiftCodes)
							{
								facadeInvoice.GenerateGiftCardSerialNumbers(null);
							}
							
							

                            // this time capture the order....
                            gatewayReceiptDataset.PaymentMethodView[0].CreditCardTransactionType = "Capture";

                            var gatewayTransactionReceiptDatasetInvoice = facadeInvoice.TransactionReceiptListFacade.CurrentDataset as TransactionReceiptDatasetGateway;
                            var facadeTransactionReceiptInvoice = facadeInvoice.TransactionReceiptListFacade as TransactionReceiptFacade;

                            string gatewayMessage = string.Empty;
                            bool approved = false;

                            // we should re-load the card gateway processor
                            // just to make sure that we have it properly initialized clean

                            using (var facadeCardCapture = new CreditCardAuthorizationFacade())
                            {
                                facadeCardCapture.TransactionType = facadeReceipt.TransactionType;
                                facadeCardCapture.TransactionReceiptFacade = facadeTransactionReceiptInvoice;
                                facadeCardCapture.CurrentCardPayment = receiptCode;
                                facadeCardCapture.CurrentParentTransactionCode = convertedInvoiceCode;

                                gatewayMessage = facadeCardCapture.AuthorizeCreditCard(facadeReceipt, false, ref approved);

                                if (approved)
                                {
                                    string errorCode = string.Empty;
                                    string[] invoiceRelatedTables = facadeInvoice.get_RelatedTables(Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice);
                                    string[][] invoiceCommands = facadeInvoice.CreateParameterSet(invoiceRelatedTables);

                                    facadeInvoice.CurrentDataset.Merge(gatewayTransactionReceiptDatasetInvoice);
                                    facadeInvoice.EmailDownloadableItem(ref errorCode);
                                    facadeInvoice.ComputeTotals(false, false, false);
                                    //save current balance to DB before posting transaction.
                                    facadeInvoice.UpdateDataSet();

                                    //don't post invoice if line items contain gc that doesn't auto generate serial
                                    if (hasManualGenerateSerialNumberGiftCodes)
                                    {
                                        if (facadeCardCapture.IsCurrentCardPaymentHasNewReceipt)
                                        {
                                            facadeReceipt.RefreshPaymentTable(facadeCardCapture.NewReceiptDataTable, facadeCardCapture.NewPaymentMethodDataTable);
                                            facadeTransactionReceiptInvoice.ReserveAndAllocateReceipt(gatewayInvoiceDataset, facadeReceipt, "Invoice");
                                            facadeInvoice.UpdateDataSet(invoiceCommands, Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice, string.Empty, false);
                                        }
                                    }                                    
                                    else
                                    {
                                        facadeInvoice.Post(String.Empty, String.Empty);
                                    }

                                    if (gatewayInvoiceDataset.CustomerInvoiceView[0].IsPosted)
                                    {
                                        if (facadeInvoice.IsCreateBackOrder())
                                        {
                                            string boCodes = string.Empty;
                                            bool backOrderCreated = facadeInvoice.CreateBackOrder(ref boCodes);
                                        }

                                        if (facadeCardCapture.IsCurrentCardPaymentHasNewReceipt)
                                        {
                                            facadeReceipt.RefreshPaymentTable(facadeCardCapture.NewReceiptDataTable, facadeCardCapture.NewPaymentMethodDataTable);
                                            facadeTransactionReceiptInvoice.ReserveAndAllocateReceipt(gatewayInvoiceDataset, facadeReceipt, "Invoice");
                                            facadeInvoice.UpdateDataSet(invoiceCommands, Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice, string.Empty, false);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ConvertToInvoice(ReceiptDatasetGateway gatewayReceiptDataset, ReceiptFacade facadeReceipt, string receiptCode)
        {
            _gatewaySalesOrderDataset.CustomerSalesOrderWorkflowView[0].Stage = Interprise.Framework.Customer.Shared.Const.WORKFLOW_STAGE_READY_TO_INVOICE;

            DataTable warehouseTable = _facadeSalesOrder.InitializeConvertion();
            _facadeSalesOrder.SelectAllItemsToInvoice(warehouseTable, true);

            InvoiceDatasetGateway tmpGatewayInvoiceDataset = null;
            string invoiceConversionErrorMessage = string.Empty;
            if (_facadeSalesOrder.ConvertSalesOrderToInvoice(
                    Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice,
                    ref tmpGatewayInvoiceDataset,
                    true,
                    false,
                    ref invoiceConversionErrorMessage))
            {
                if (null != tmpGatewayInvoiceDataset)
                {
                    using (var gatewayInvoiceDataset = new InvoiceDatasetGateway())
                    {
                        using (var facadeInvoice = new InvoiceFacade(gatewayInvoiceDataset))
                        {
                            facadeInvoice.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice;

                            string convertedInvoiceCode = tmpGatewayInvoiceDataset.CustomerInvoiceView[0].InvoiceCode;

                            string[][] paramSet = new string[][] { new string[] { "@InvoiceCode", convertedInvoiceCode } };

                            //unified load of data to avoid PK related error during update.
                            facadeInvoice.LoadDataSet(Interprise.Framework.Base.Shared.StoredProcedures.READCUSTOMERINVOICEUNIFIED,
                            new string[] {tmpGatewayInvoiceDataset.CustomerInvoiceView.TableName, tmpGatewayInvoiceDataset.CustomerInvoiceDetailView.TableName, gatewayInvoiceDataset.TransactionTaxDetailView.TableName, 
                                      tmpGatewayInvoiceDataset.TransactionItemTaxDetailView.TableName, tmpGatewayInvoiceDataset.CustomerCreditAllocationView.TableName, tmpGatewayInvoiceDataset.CustomerCreditView.TableName,
                                      tmpGatewayInvoiceDataset.CustomerItemKitDetailView.TableName, tmpGatewayInvoiceDataset.CustomerSalesRepCommissionView.TableName, tmpGatewayInvoiceDataset.TransactionTaxSummary.TableName,
                                      tmpGatewayInvoiceDataset.CustomerInvoiceSerialLotNumbers.TableName, tmpGatewayInvoiceDataset.CustomerInvoiceKitSerialLotNumbers.TableName},
                            paramSet,
                            Interprise.Framework.Base.Shared.Enum.ClearType.None,
                            Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                            // let's reload the receipt table
                            string[][] reloadReceiptCommandset = new string[][]{
                                                                    new string[]{
                                                                        Const.CUSTOMERCARDPAYMENT_TABLE,
                                                                        StoredProcedures.READCUSTOMERCARDPAYMENT, 
                                                                        "@CardPaymentCode",
                                                                        receiptCode},
                                                                    new string[]{
                                                                        Const.PAYMENTMETHODVIEW_TABLE,
                                                                        StoredProcedures.READPAYMENTMETHOD,
                                                                        "@ReceivableCode",
                                                                        receiptCode}};

                            facadeReceipt.LoadDataSet(reloadReceiptCommandset, Interprise.Framework.Base.Shared.Enum.ClearType.Specific, Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                            // generate GC serial if auto generate
                            var invoiceDetailGiftCodeRows = tmpGatewayInvoiceDataset.CustomerInvoiceDetailView.Select(String.Format("{0} IN ('{1}', '{2}')", Interprise.Framework.Base.Shared.Const.INVENTORYITEM_ITEMTYPE_COLUMN,
                                                                                                                                                             Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CARD,
                                                                                                                                                             Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CERTIFICATE));
                            bool hasAutoGenerateSerialNumberGiftCodes = false;
                            bool hasManualGenerateSerialNumberGiftCodes = false;
                            if (invoiceDetailGiftCodeRows != null && invoiceDetailGiftCodeRows.Length > 0)
                            {
                                foreach (DataRow invoiceDetailRow in invoiceDetailGiftCodeRows)
                                {
                                    if (!hasAutoGenerateSerialNumberGiftCodes)
                                    {
                                        if (facadeInvoice.AutoGenerate(invoiceDetailRow[Interprise.Framework.Base.Shared.Const.INVENTORYITEM_ITEMCODE_COLUMN].ToString()))
                                        {
                                            hasAutoGenerateSerialNumberGiftCodes = true;
                                            continue;
                                        }
                                    }

                                    if (!hasManualGenerateSerialNumberGiftCodes)
                                    {
                                        if (!facadeInvoice.AutoGenerate(invoiceDetailRow[Interprise.Framework.Base.Shared.Const.INVENTORYITEM_ITEMCODE_COLUMN].ToString()))
                                        {
                                            hasManualGenerateSerialNumberGiftCodes = true;
                                            continue;
                                        }
                                    }
                                }
                            }

                            if (hasAutoGenerateSerialNumberGiftCodes)
                            {
                                facadeInvoice.GenerateGiftCardSerialNumbers(null);
                            }

                            string errorCode = string.Empty;
                            string[] invoiceRelatedTables = facadeInvoice.get_RelatedTables(Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice);
                            string[][] invoiceCommands = facadeInvoice.CreateParameterSet(invoiceRelatedTables);

                            facadeInvoice.EmailDownloadableItem(ref errorCode);
                            facadeInvoice.ComputeTotals(false, false, false);
                            //save current balance to DB before posting transaction.
                            facadeInvoice.UpdateDataSet();

                            if (!hasManualGenerateSerialNumberGiftCodes)
                            {
                                facadeInvoice.Post(string.Empty, string.Empty);
                            }

                            if (gatewayInvoiceDataset.CustomerInvoiceView[0].IsPosted)
                            {
                                if (facadeInvoice.IsCreateBackOrder())
                                {
                                    string boCodes = string.Empty;
                                    bool backOrderCreated = facadeInvoice.CreateBackOrder(ref boCodes);
                                }

                            }

                        }
                    }
                }
            }
        }

        #region Gift Registry

        public bool HasRegistryItemsAndOneOrMoreItemsHasZeroInNeed()
        {
            return this.CartItems.AsQueryable()
                                .Where(ItemHasRegistryID())
                                .Any(item => item.RegistryItemQuantity == 0);
        }

        public bool HasRegistryItemsAndOneOrMoreItemsExceedsToTheInNeedQuantity()
        {
            return this.CartItems
                        .AsQueryable()
                        .Where(ItemHasRegistryID())
                        .Where(IsNotExistInRegistry())
                        .Any(item => item.HasRegistryItemQuantityConflict());
        }

        public bool HasRegistryItemButParentRegistryIsRemoved()
        {
            return CartItems.AsQueryable()
                            .Where(ItemHasRegistryID())
                            .Where(IsNotExistInRegistry())
                            .Any(IsNotParentRegistryExist());
        }

        public bool HasRegistryItemsRemovedFromRegistry()
        {
            return CartItems.AsQueryable()
                            .Where(ItemHasRegistryID())
                            .Where(IsNotExistInRegistry())
                            .Any(IsNotExistInRegistry());
        }

        public int RemoveRegistryItemsHasDeletedRegistry()
        {
            int totalItemToRemove = 0;
            if (!HasRegistryItems()) return totalItemToRemove;

            var itemsWithRemovedParentRegistry = CartItems
                                         .AsQueryable()
                                         .Where(ItemHasRegistryID())
                                         .Where(IsNotParentRegistryExist())
                                         .Select(item => string.Format("'{0}'", item.RegistryItemCode.ToString()))
                                         .ToArray()
                                         .AsParallel();

            totalItemToRemove = itemsWithRemovedParentRegistry.Count();
            if (totalItemToRemove > 0)
            {
                GiftRegistryDA.RemoveRegistryItems(string.Join(",", itemsWithRemovedParentRegistry));
            }
            return totalItemToRemove;
        }

        public int RemoveRegistryItemsHasBeenDeletedInRegistry()
        {
            int totalItemToRemove = 0;
            if (!HasRegistryItems()) return totalItemToRemove;

            var removedItems = CartItems.AsQueryable()
                                        .Where(ItemHasRegistryID())
                                        .Where(IsNotExistInRegistry())
                                        .Select(item => string.Format("'{0}'", item.RegistryItemCode.ToString()))
                                        .ToArray()
                                        .AsParallel();

            totalItemToRemove = removedItems.Count();
            if (totalItemToRemove > 0)
            {
                GiftRegistryDA.RemoveRegistryItems(string.Join(",", removedItems));
            }
            return totalItemToRemove;

        }

        public void RemoveRegistryItems()
        {
            var removedItems = CartItems
                                .AsQueryable()
                                .Where(ItemHasRegistryID())
                                .Select(item => string.Format("'{0}'", item.RegistryItemCode.ToString()))
                                .ToArray()
                                .AsParallel();
            GiftRegistryDA.RemoveRegistryItems(string.Join(",", removedItems));

        }

        public static Expression<Func<CartItem, bool>> IsNotParentRegistryExist()
        {
            Expression<Func<CartItem, bool>> exp = (item => !GiftRegistryDA.IsRegistryExist(item.GiftRegistryID.Value));
            return exp;
        }

        public static Expression<Func<CartItem, bool>> IsNotExistInRegistry()
        {
            Expression<Func<CartItem, bool>> exp = (item => !GiftRegistryDA.IsRegistryItemExists(item.RegistryItemCode.Value));
            return exp;
        }

        public static Expression<Func<CartItem, bool>> ItemHasRegistryID()
        {
            Expression<Func<CartItem, bool>> exp = (item => item.GiftRegistryID.HasValue);
            return exp;
        }

        #endregion


        public decimal GetCartSubTotalExcludeOversized()
        {
            decimal subTotal = Decimal.Zero;

            if (this.IsSalesOrderDetailBuilt && _gatewaySalesOrderDataset != null)
            {
                subTotal = _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SubTotalRate;
            }
            else if (!this.IsSalesOrderDetailBuilt)
            {
                subTotal = this.GetCartSubTotal();
            }

            if (!this.IsSalesOrderDetailBuilt)
            {
                var oversizedCartItemsSubTotal = CartItems.Where(item => item.IsOverSized)
                                                 .Sum(item =>
                                                 {
                                                     if (!ThisCustomer.CouponCode.IsNullOrEmptyTrimmed())
                                                     {
                                                         if (!item.DiscountAmountAlreadyComputed)
                                                         {
                                                             item.CouponDiscount = this.GetCartItemCouponDiscount(item);
                                                             item.DiscountAmountAlreadyComputed = true;
                                                         }

                                                         return item.Price - item.CouponDiscount;
                                                     }
                                                     else
                                                     {
                                                         return item.Price;
                                                     }
                                                 });


                return subTotal - oversizedCartItemsSubTotal;

            }

            var oversizedSubTotal = CartItems.Where(item => item.IsOverSized)
                                             .Sum(item =>
                                             {
                                                 var lineItemRow = item.AssociatedLineItemRow;

                                                 //original price X quantity
                                                 decimal originalAmount = lineItemRow.SalesPriceRate * lineItemRow.QuantityOrdered;

                                                 //discounted price ( if has coupon )
                                                 decimal discountedPrice = lineItemRow.ExtPriceRate;

                                                 if (!ThisCustomer.CouponCode.IsNullOrEmptyTrimmed())
                                                 {
                                                     return discountedPrice;
                                                 }
                                                 else
                                                 {
                                                     return originalAmount;
                                                 }
                                             });


            return subTotal - oversizedSubTotal;
        }

        [Obsolete("Use this method: ServiceFactory<IShoppingCartService>().SetRealTimeRateRecord()")]
        public void SetRealTimeRateRecord(string shippingMethodCode, string freight, string realTimeRateGUID, bool isFromMultipleShipping)
        {
            ServiceFactory.GetInstance<IShippingService>()
                          .SetRealTimeRateRecord(shippingMethodCode, freight, realTimeRateGUID, isFromMultipleShipping);
        }

        public bool HasMultipleShippingMethod()
        {
            var cartItems = CartItems.Select(c => new { ShippingMethod = c.ShippingMethod });

            bool hasMultipleShippingMethod = cartItems.Where(c => !c.ShippingMethod.IsNullOrEmptyTrimmed())
                                                      .Distinct().Count() > 1;

            return HasPickupItem() || hasMultipleShippingMethod;
        }

        public bool HasPickupItem()
        {
            return CartItems.Any(c => !c.InStoreWarehouseCode.IsNullOrEmptyTrimmed());
        }

        public bool HasCartItemWithDecimalQuantity()
        {
            return CartItems.Any(i => (i.m_AllocatedQty % 1) != 0);
        }

        #region Credit Memos, Loyalty Points, Gift Codes

        private bool _clearAlreadyAppliedOtherPayment = true;
        public bool ClearAlreadyAppliedOtherPayment 
        {
            get { return _clearAlreadyAppliedOtherPayment; } 
            set { _clearAlreadyAppliedOtherPayment = value; }
        }

        private void ApplyOtherPayments(string salesOrderCode, decimal cartTotal, ref decimal amountApplied)
        {
            var customer = ServiceFactory.GetInstance<IAuthenticationService>()
                                            .GetCurrentLoggedInCustomer();
            bool isClearOtherPayment = (!customer.ThisCustomerSession[DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS].IsNullOrEmptyTrimmed()) ?
                    Convert.ToBoolean(customer.ThisCustomerSession[DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS]) : false;
            string paymentTerm = customer.PaymentTermCode;

            if (isClearOtherPayment && (paymentTerm.EqualsIgnoreCase(PaymentTerm.REQUEST_QUOTE) || (paymentTerm.EqualsIgnoreCase(PaymentTerm.PURCHASE_ORDER))))
            {
                this.ClearGiftCodes();
                this.ClearLoyaltyPoints();
                this.ClearCreditMemos();
                return;
            }

            if (this.HasAppliedGiftCode())
            {
                decimal giftCodesAmountApplied = Decimal.Zero;
                ApplyGiftCodes(salesOrderCode, cartTotal, ref giftCodesAmountApplied);
                amountApplied += giftCodesAmountApplied;

                if (this.ClearAlreadyAppliedOtherPayment) { this.ClearGiftCodes(); }
            }
            if (this.HasAppliedLoyaltyPoints())
            {
                decimal loyaltyPointsAmountApplied = Decimal.Zero;
                ApplyLoyaltyPoints(salesOrderCode, cartTotal, ref loyaltyPointsAmountApplied);
                amountApplied += loyaltyPointsAmountApplied;

                if (this.ClearAlreadyAppliedOtherPayment) { this.ClearLoyaltyPoints(); }
            }
            if (this.HasAppliedCreditMemo())
            {
                decimal creditsAmountApplied = Decimal.Zero;
                ApplyCredits(salesOrderCode, cartTotal, ref creditsAmountApplied);
                amountApplied += creditsAmountApplied;

                if (this.ClearAlreadyAppliedOtherPayment) { this.ClearCreditMemos(); }
            }
        }

        private void ApplyGiftCodes(string salesOrderCode, decimal cartTotal, ref decimal amountApplied)
        {
            try
            {
                var appconfigService = ServiceFactory.GetInstance<IAppConfigService>();

                if (cartTotal == Decimal.Zero) { return; }

                //we need to manually assign documentcode to avoid error during gift card/certificate apply method
                _facadeSalesOrder.TransactionReceiptListFacade.DocumentCode = salesOrderCode;

                var gatewayReceiptDataset = new ReceiptDatasetGateway();
                var facadeReceipt = new ReceiptFacade(gatewayReceiptDataset);
                var gatewayTransactionReceiptDataSet = _facadeSalesOrder.TransactionReceiptListFacade.CurrentDataset as TransactionReceiptDatasetGateway;
                var facadeTransactionReceipt = _facadeSalesOrder.TransactionReceiptListFacade as TransactionReceiptFacade;

                var appliedGiftCodes = ServiceFactory.GetInstance<IShoppingCartService>()
                                                     .GetAppliedGiftCodes(false)
                                                     .ToList();

                decimal balance = Decimal.Zero;
                int index = 0;

                foreach (var giftCode in appliedGiftCodes)
                {
                    //check if we need to further apply giftcode
                    if (cartTotal <= Decimal.Zero) { break; }

                    decimal amount = (giftCode.AmountApplied > cartTotal) ? cartTotal : giftCode.AmountApplied;
                    balance = amount;

                    var transactionType = (giftCode.Type.EqualsIgnoreCase(Const.ITEM_TYPE_GIFT_CERTIFICATE)) ? Interprise.Framework.Base.Shared.Enum.TransactionType.GiftCertificate : Interprise.Framework.Base.Shared.Enum.TransactionType.GiftCard;

                    //apply gift code
                    bool applied = _facadeSalesOrder.TransactionReceiptListFacade.ApplyCredit(transactionType,
                        giftCode.CreditCode,
                        amount,
                        cartTotal,
                        facadeReceipt,
                        ref index,
                        ref balance);

                    if (applied && _facadeSalesOrder.TransactionReceiptListFacade.CurrentDataset.HasChanges())
                    {
                        _gatewaySalesOrderDataset.Merge(gatewayTransactionReceiptDataSet.CustomerTransactionReceiptView);

                        string[] relatedTables = _facadeSalesOrder.get_RelatedTables(_facadeSalesOrder.TransactionType);
                        string[][] commands = _facadeSalesOrder.CreateParameterSet(relatedTables);
                        _facadeSalesOrder.UpdateDataSet(commands, _facadeSalesOrder.TransactionType, String.Empty);

                        facadeTransactionReceipt.UpdateCreditTransaction();

                        gatewayTransactionReceiptDataSet.CustomerTransactionReceiptView.AcceptChanges();

                        amountApplied += amount;
                        cartTotal -= amount;
                        giftCode.AmountApplied -= amount;
                    }
                }

                if (!this.ClearAlreadyAppliedOtherPayment)
                {
                    var updatedGiftCodes = appliedGiftCodes.Select(x => new GiftCodeAmountAppliedModel() { SerialCode = x.SerialCode, AmountApplied = x.AmountApplied });
                    string serialized = ServiceFactory.GetInstance<ICryptographyService>().SerializeToJson<List<GiftCodeAmountAppliedModel>>(updatedGiftCodes.ToList());
                    ServiceFactory.GetInstance<IShoppingCartService>().ApplyGiftCodes(serialized);
                }

                // save salesorder if total due is zero (no payment required) or 
                // if appconfig CreateReceiptForAllPaymentType is set to false
                if (cartTotal == Decimal.Zero || !appconfigService.CreateReceiptForAllPaymentType) { SaveSalesOrder(); }
            }
            catch
            {
                amountApplied = Decimal.Zero;
            }
        }
        private void ApplyLoyaltyPoints(string salesOrderCode, decimal cartTotal, ref decimal amountApplied)
        {
            try
            {
                var appconfigService = ServiceFactory.GetInstance<IAppConfigService>();
                var customerService = ServiceFactory.GetInstance<ICustomerService>();

                if (cartTotal == Decimal.Zero) { return; }

                //we need to manually assign documentcode to avoid error during gift card/certificate apply method
                _facadeSalesOrder.TransactionReceiptListFacade.DocumentCode = salesOrderCode;

                var gatewayReceiptDataset = new ReceiptDatasetGateway();
                var facadeReceipt = new ReceiptFacade(gatewayReceiptDataset);
                var loyaltyPointsDataset = new LoyaltyPointsDatasetGateway();

                decimal cartTotalDue = cartTotal;
                decimal creditsAvailable = this.LoyaltyPointsAmountApplied;
                decimal creditsToBeApplied = (creditsAvailable > cartTotalDue) ? cartTotalDue : creditsAvailable;
                int rowIndex = 0;

                string[][] paramset = new string[][] 
                { 
                    new string[] { loyaltyPointsDataset.CustomerLoyaltyPoints.TableName, "ReadCustomerLoyaltyPoints", "@CustomerCode", ThisCustomer.CustomerCode }
                };
                var applyPaymentFacade = new ApplyPaymentFacade(Interprise.Framework.Customer.Shared.Enum.ApplyPayment.LoyaltyPoints, loyaltyPointsDataset, ThisCustomer.CustomerCode);
                applyPaymentFacade.LoadDataSet(paramset, Interprise.Framework.Base.Shared.Enum.ClearType.None, Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);
                _facadeSalesOrder.TransactionReceiptListFacade.DocumentCode = salesOrderCode;
                _facadeSalesOrder.TransactionReceiptListFacade.AppliedPoints = Convert.ToInt32(Math.Ceiling(creditsToBeApplied / customerService.GetRedemptionMultiplier()));
                _facadeSalesOrder.TransactionReceiptListFacade.ApplyCredit(Interprise.Framework.Base.Shared.Enum.TransactionType.LoyaltyPoints,
                    Interprise.Framework.Base.Shared.Const.TEMPORARY_DOCUMENTCODE,
                    creditsToBeApplied,
                    cartTotalDue,
                    facadeReceipt,
                    ref rowIndex,
                    loyaltyPointsDataset);

                var gatewayTransactionReceiptDataSet = _facadeSalesOrder.TransactionReceiptListFacade.CurrentDataset as TransactionReceiptDatasetGateway;
                var facadeTransactionReceipt = _facadeSalesOrder.TransactionReceiptListFacade as TransactionReceiptFacade;

                if (_facadeSalesOrder.TransactionReceiptListFacade.CurrentDataset.HasChanges())
                {
                    _gatewaySalesOrderDataset.Merge(gatewayTransactionReceiptDataSet.CustomerTransactionReceiptView);

                    string[] relatedTables = _facadeSalesOrder.get_RelatedTables(_facadeSalesOrder.TransactionType);
                    string[][] commands = _facadeSalesOrder.CreateParameterSet(relatedTables);
                    _facadeSalesOrder.UpdateDataSet(commands, _facadeSalesOrder.TransactionType, String.Empty);

                    facadeTransactionReceipt.UpdateCreditTransaction();

                    gatewayTransactionReceiptDataSet.CustomerTransactionReceiptView.AcceptChanges();

                    amountApplied += creditsToBeApplied;
                    cartTotalDue -= creditsToBeApplied;

                    if (!this.ClearAlreadyAppliedOtherPayment)
                    {
                        decimal amountLeft = (creditsAvailable - creditsToBeApplied);
                        decimal multiplier = ServiceFactory.GetInstance<ICustomerService>().GetRedemptionMultiplier();
                        decimal pointsLeft = amountLeft / multiplier;
                        ServiceFactory.GetInstance<IShoppingCartService>().ApplyLoyaltyPoints(pointsLeft.ToString());
                    }
                }

                // save salesorder if total due is zero (no payment required) or 
                // if appconfig CreateReceiptForAllPaymentType is set to false
                if (cartTotalDue == Decimal.Zero || !appconfigService.CreateReceiptForAllPaymentType) { SaveSalesOrder(); }
            }
            catch
            {
                amountApplied = Decimal.Zero;
            }
        }
        private void ApplyCredits(string salesOrderCode, decimal cartTotal, ref decimal amountApplied)
        {
            try
            {
                var appconfigService = ServiceFactory.GetInstance<IAppConfigService>();

                if (cartTotal == Decimal.Zero) { return; }

                //we need to manually assign documentcode to avoid error during gift card/certificate apply method
                _facadeSalesOrder.TransactionReceiptListFacade.DocumentCode = salesOrderCode;

                var gatewayReceiptDataset = new ReceiptDatasetGateway();
                var facadeReceipt = new ReceiptFacade(gatewayReceiptDataset);

                var availableCreditMemos = ServiceFactory.GetInstance<ICustomerService>().GetCustomerCreditMemosWithRemainingBalance().ToList();
                var creditMemos = ServiceFactory.GetInstance<IShoppingCartService>().GetAppliedCreditMemos()
                                                .Where(x => availableCreditMemos.Exists(y => y.CreditCode == x.CreditCode))
                                                .ToList();
                
                decimal cartTotalDue = cartTotal;

                var gatewayTransactionReceiptDataSet = _facadeSalesOrder.TransactionReceiptListFacade.CurrentDataset as TransactionReceiptDatasetGateway;
                var facadeTransactionReceipt = _facadeSalesOrder.TransactionReceiptListFacade as TransactionReceiptFacade;

                int rowIndex = 0;

                foreach (var creditMemo in creditMemos)
                {
                    //check if we need to further apply creditmemo
                    if (cartTotalDue <= Decimal.Zero) { break; }

                    decimal creditsAvailable = creditMemo.CreditAppliedInShoppingCart;
                    decimal creditsToBeApplied = Decimal.Zero;

                    //check if creditmemo available credits are enough to pay cart's total due
                    if (creditsAvailable > cartTotalDue) { creditsToBeApplied = cartTotalDue; }
                    else { creditsToBeApplied = creditsAvailable; }


                    //apply credits
                    bool applied = _facadeSalesOrder.TransactionReceiptListFacade.ApplyCredit(Interprise.Framework.Base.Shared.Enum.TransactionType.CreditMemo,
                            new string[] { creditMemo.CreditCode },
                            creditsToBeApplied,
                            cartTotalDue,
                            facadeReceipt,
                            ref rowIndex);

                    if (applied && _facadeSalesOrder.TransactionReceiptListFacade.CurrentDataset.HasChanges())
                    {
                        _gatewaySalesOrderDataset.Merge(gatewayTransactionReceiptDataSet.CustomerTransactionReceiptView);

                        string[] relatedTables = _facadeSalesOrder.get_RelatedTables(_facadeSalesOrder.TransactionType);
                        string[][] commands = _facadeSalesOrder.CreateParameterSet(relatedTables);
                        _facadeSalesOrder.UpdateDataSet(commands, _facadeSalesOrder.TransactionType, String.Empty);

                        facadeTransactionReceipt.UpdateCreditTransaction();

                        gatewayTransactionReceiptDataSet.CustomerTransactionReceiptView.AcceptChanges();

                        amountApplied += creditsToBeApplied;
                        cartTotalDue -= creditsToBeApplied;
                        creditMemo.CreditAppliedInShoppingCart -= creditsToBeApplied;
                    }
                }

                if (!this.ClearAlreadyAppliedOtherPayment)
                {
                    var updateCreditMemos = creditMemos.Select(x => new CustomerCreditAppliedModel() { CreditCode = x.CreditCode, CreditAppliedInShoppingCart = x.CreditAppliedInShoppingCart  });
                    string serialized = ServiceFactory.GetInstance<ICryptographyService>().SerializeToJson<List<CustomerCreditAppliedModel>>(updateCreditMemos.ToList());
                    ServiceFactory.GetInstance<IShoppingCartService>().ApplyCreditMemos(serialized);
                }

                // save salesorder if total due is zero (no payment required) or 
                // if appconfig CreateReceiptForAllPaymentType is set to false
                if (cartTotalDue == Decimal.Zero || !appconfigService.CreateReceiptForAllPaymentType) { SaveSalesOrder(); }
            }
            catch
            {
                amountApplied = Decimal.Zero;
            }
        }

        #endregion

        public bool HasOverSizedItemWithPickupShippingMethod()
        {
            return m_CartItems.Any(c =>
            {
                bool retVal = false;
                if (c.IsOverSized)
                {
                    var shippingMethod = ServiceFactory.GetInstance<IShippingService>()
                                                       .GetOverSizedItemShippingMethod(c.ItemCode, c.UnitMeasureCode);
                    if (shippingMethod != null)
                    {
                        retVal = (shippingMethod.FreightChargeType.ToUpperInvariant() == DomainConstants.PICKUP_FREIGHT_CHARGE_TYPE);
                    }
                }
                return retVal;
            });
        }

        public IEnumerable<int> GetOverSizedItemWithShippingMethodNotBelongToCustomerShippingGroup()
        {
            //Get all none oversized items
            var shippingMethods = ServiceFactory.GetInstance<IShippingService>()
                                                .GetCustomerShippingMethods(
                                                    ThisCustomer.GetValidCustomerCodeForShoppingCartRecord(),
                                                    false,
                                                    ThisCustomer.ContactCode,
                                                    String.Empty,
                                                    ThisCustomer.PrimaryShippingAddress,
                                                    false
                                                );
            
            var oversizedItems = CartItems.Where(c => c.IsOverSized);

            return oversizedItems.Where(os => !shippingMethods.Any(sm => sm.Code == os.OverSizedShippingMethodCode))
                                 .Select(itm => itm.m_ShoppingCartRecordID)
                                 .AsParallel();
        }

        public IEnumerable<int> GetGiftRegistryItemsWithPickupShippingMethod()
        {
            return CartItems.Where(c =>
            {
                if (!c.IsOverSized && !c.OverSizedShippingMethodCode.IsNullOrEmptyTrimmed()) return false;
                var shippingMethodModel = ServiceFactory.GetInstance<IShippingService>()
                                                        .GetOverSizedItemShippingMethod(c.ItemCode, c.UnitMeasureCode);

                return (shippingMethodModel != null &&
                            shippingMethodModel.FreightChargeType.ToUpperInvariant() == DomainConstants.PICKUP_FREIGHT_CHARGE_TYPE);

            }).Where(c => c.GiftRegistryID.HasValue)
              .Select(c => c.m_ShoppingCartRecordID)
              .AsParallel();
        }

        #region RMA

        public bool CreateNewRMA(string invoiceCode, IEnumerable<CustomerInvoiceItemCustomModel> items, string notes, ref string errorMsg, ref bool isQuantityTrimmed)
        {
            bool isCreated = false;
            try
            {
                // try to create rma from invoice 
                if (CreateRMAFromInvoice(invoiceCode, ref errorMsg))
                {
                    // set rma item quantity
                    bool trimmed = false;
                    items.ForEach(x => SetRMAItemQuantity(x.LineNum, x.QuantityToReturn, ref trimmed));
                    isQuantityTrimmed = trimmed;

                    // set rma notes
                    SetRMANotes(notes);

                    // save rma
                    SaveRMA();

                    // return success
                    isCreated = true;
                }

            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }
            return isCreated;
        }
        private bool CreateRMAFromInvoice(string invoiceCode, ref string errorMsg)
        {
            bool isCreated = false;
            try
            {
                _gatewaySalesOrderDataset = new SalesOrderDatasetGateway();
                _facadeSalesOrder = new SalesOrderFacade(_gatewaySalesOrderDataset);
                _facadeSalesOrder.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.RMA;

                var dt = new DataTable();
                dt.Columns.Add(DomainConstants.COLUMN_INVOICE_CODE);

                var dr = dt.NewRow();
                dr[DomainConstants.COLUMN_INVOICE_CODE] = invoiceCode;
                dt.Rows.Add(dr);

                if(_facadeSalesOrder.CreateRMAFromInvoice(dt.Rows[0]))
                {
                    if(!_facadeSalesOrder.ConvertInvoiceToRMAMessage.IsNullOrEmptyTrimmed())
                    {
                        errorMsg = "{0} : {1}".FormatWith(Interprise.Facade.Base.SimpleFacade.Instance.GetMessage("ERR0057"), _facadeSalesOrder.ConvertInvoiceToRMAMessage);
                        isCreated = false;
                    }
                    isCreated = true;
                }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }
            return isCreated;
        }
        private void SetRMAItemQuantity(int lineNum, decimal quantity, ref bool isTrimmed)
        {
            if(_gatewaySalesOrderDataset != null && _gatewaySalesOrderDataset.CustomerSalesOrderDetailView != null)
            {
                if(_gatewaySalesOrderDataset.CustomerSalesOrderDetailView.Sum(x => x.QuantityShipped - x.QuantityAlReadyRMA) == 0)
                {
                    throw new Exception(DomainConstants.RMA_NOITEM_AVAILABLE_ERROR);
                }

                var lineItemRow = _gatewaySalesOrderDataset.CustomerSalesOrderDetailView.FirstOrDefault(x => x.LineNum == lineNum);
                if(lineItemRow != null)
                {
                    decimal qtyAllowed = lineItemRow.QuantityShipped - lineItemRow.QuantityAlReadyRMA;
                    if (quantity > qtyAllowed) 
                    {
                        quantity = qtyAllowed;
                        isTrimmed = true;
                    }
                    lineItemRow.QuantityOrdered = quantity;
                }
            }
        }
        private void SetRMANotes(string notes)
        {
            if(_gatewaySalesOrderDataset != null && _gatewaySalesOrderDataset.CustomerSalesOrderView[0] != null)
            {
                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].InternalNotes = notes;
            }
        }
        public void VoidRMA(string rmaCode)
        {
            _gatewaySalesOrderDataset = new SalesOrderDatasetGateway();
            _facadeSalesOrder = new SalesOrderFacade(_gatewaySalesOrderDataset);
            _facadeSalesOrder.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.RMA;
            _facadeSalesOrder.LoadDataSet(rmaCode);

            var salesOrderRow = _gatewaySalesOrderDataset.CustomerSalesOrderView[0];
            if (salesOrderRow != null && !salesOrderRow.OrderStatus.EqualsIgnoreCase("close"))
            {
                _facadeSalesOrder.VoidOrder();
                SaveRMA();
            }
        }
        private void SaveRMA()
        {
            //Retry saving if concurrency error was encountered otherwise, simply throw the exception.
            int nooftries = 0;
            while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
            {
                try
                {
                    string[] relatedTables = _facadeSalesOrder.get_RelatedTables(_facadeSalesOrder.TransactionType);
                    string[][] commands = _facadeSalesOrder.CreateParameterSet(relatedTables);
                    _facadeSalesOrder.UpdateDataSet(commands, _facadeSalesOrder.TransactionType, string.Empty, false);
                    break;
                }
                catch (DBConcurrencyException)
                {
                    nooftries += 1;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion
    }

    public class LineItem
    {
        public int Counter { get; set; }
        public string ItemCode { get; set; }
        public decimal Length { get; set; }

        public decimal Width { get; set; }

        public decimal Height { get; set; }

        public decimal Weight { get; set; }

        public int Quantity { get; set; }

    }
}
