// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration
{
    public class WishListPageLiteralRenderer : IShoppingCartHTMLLiteralRenderer
    {
        #region IShoppingCartHTMLLiteralRenderer Members

        private IStringResourceService _stringResourceService = null;
        private void InitializeDomainServices()
        {
            _stringResourceService = ServiceFactory.GetInstance<IStringResourceService>();
        }

        public void Render(InterpriseShoppingCart cart, ref StringBuilder output)
        {
            InitializeDomainServices(); 

            if (cart.IsEmpty())
            {
                output.Append("<br><br>");
                if (cart.CartType == CartTypeEnum.ShoppingCart)
                {
                    Topic t1 = new Topic("EmptyCartText", cart.ThisCustomer.LocaleSetting, cart.SkinID, null);
                    output.Append(t1.Contents);
                }
                if (cart.CartType == CartTypeEnum.WishCart)
                {
                    Topic t1 = new Topic("EmptyWishListText", cart.ThisCustomer.LocaleSetting, cart.SkinID, null);
                    output.Append(t1.Contents);
                }

                if (cart.CartType == CartTypeEnum.GiftRegistryCart)
                {
                    Topic t1 = new Topic("EmptyGiftRegistryText", cart.ThisCustomer.LocaleSetting, cart.SkinID, null);
                    output.Append(t1.Contents);
                }
                output.Append("<br><br><br>");
            }
            else
            {
                bool showPicsInCart = AppLogic.AppConfigBool("ShowPicsInCart");
                bool showLinkBack = AppLogic.AppConfigBool("LinkToProductPageInCart");
                bool hideUnitMeasure = AppLogic.AppConfigBool("HideUnitMeasure");

                output.Append("<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\"  >");
                output.Append("<tr>");
                if (showPicsInCart)
                {
                    output.Append("<td align=\"center\" valign=\"middle\"><b>");
                    output.Append(AppLogic.GetString("shoppingcart.cs.1"));
                    output.Append("</b></td>");
                    output.Append("<td align=\"center\" valign=\"middle\">&nbsp;</td>");
                }
                else
                {
                    output.Append("<td align=\"left\" valign=\"middle\">");
                    output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.cs.1"));
                    output.Append("</td>");
                }

                if (!hideUnitMeasure)
                {
                    // Unit Measure Column Header
                    output.Append("<td align=\"center\" valign=\"middle\">");
                    output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.cs.37"));
                    output.Append("</td>");
                }

                // Quantity Column Header...
                output.Append("<td align=\"center\" valign=\"middle\">");
                output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.cs.2"));
                output.Append("</td>");

                // Sub Total Column Header...
                output.Append("<td align=\"right\" valign=\"middle\">");
                output.Append("</td>");
                output.Append("</tr>");

                foreach (CartItem cartItem in cart.CartItems)
                {
                    // get the associated cart item for this line item...
                    // Divider
                    output.AppendFormat("<tr><td id='tdWishListHeaderDivider' colspan=\"{0}\"></td></tr>", CommonLogic.IIF(showPicsInCart, 5, 4));
                    output.Append("<tr>");

                    if (showPicsInCart)
                    {
                        // PICTURE COL:
                        output.Append("<td align=\"center\" valign=\"top\">");
                        if (showLinkBack)
                        {
                            output.Append("<a href=\"" + SE.MakeProductLink(cartItem.ItemCounter.ToString(), cartItem.DisplayName) + "\">");
                        }

                        var img = ProductImage.Locate("product", cartItem.ItemCode, "icon");
                        if (null != img)
                        {
                            output.Append("<img src=\"" + img.src + "\" border=\"0\">");
                        }

                        if (showLinkBack)
                        {
                            output.Append("</a>");
                        }
                        output.Append("</td>");
                    }

                    /*********************************
                     * Line Item
                     * *******************************/
                    output.Append("<td align=\"left\" valign=\"top\">");

                    switch (cartItem.ItemType)
                    {
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP:
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM:
                            if (showLinkBack)
                            {
                                using (var con = DB.NewSqlConnection())
                                {
                                    con.Open();
                                    using (var reader = DB.GetRSFormat(con, "SELECT ItemCode FROM InventoryMatrixItem with (NOLOCK) WHERE MatrixItemCode = {0}", DB.SQuote(cartItem.ItemCode)))
                                    {
                                        if (reader.Read())
                                        {
                                            output.AppendFormat(
                                                "<a href=\"{0}\"><b>{1}</b></a><br>",
                                                InterpriseSuiteEcommerceCommon.InterpriseHelper.MakeItemLink(DB.RSField(reader, "ItemCode")),
                                                Security.HtmlEncode(cartItem.DisplayName)
                                            );
                                        }
                                    }
                                }
                            }
                            else
                            {
                                output.AppendFormat("<b>{0}</b><br>", cartItem.ItemCode);
                            }
                            // display the details
                            using (var con = DB.NewSqlConnection())
                            {
                                con.Open();
                                using (var reader = DB.GetRSFormat(con, String.Format("exec eCommerceGetMatrixItemAttributes @ItemCode = NULL, @MatrixItemCode = {0}, @WebsiteCode = {1}, @CurrentDate = {2}, @LanguageCode = {3}, @ContactCode = {4}", DB.SQuote(cartItem.ItemCode), DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode), DB.SQuote(DateTime.Now.ToDateTimeStringForDB()), DB.SQuote(Customer.Current.LanguageCode), DB.SQuote(Customer.Current.ContactCode))))
                                {
                                    if (reader.Read())
                                    {
                                        for (int ctr = 1; ctr <= 6; ctr++)
                                        {
                                            string attribute = DB.RSField(reader, string.Format("Attribute{0}", ctr));
                                            string attributeValue = DB.RSField(reader, string.Format("Attribute{0}ValueDescription", ctr));

                                            if (!string.IsNullOrEmpty(attribute) && !string.IsNullOrEmpty(attributeValue))
                                            {
                                                output.AppendFormat(
                                                    "&nbsp;&nbsp;{0}:{1}<br />",
                                                    Security.HtmlEncode(attribute),
                                                    Security.HtmlEncode(attributeValue)
                                                );
                                            }
                                        }
                                    }
                                }
                            }
                            break;

                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT:
                            output.Append("<div style=\"margin-left: 10px;\">");
                            if (showLinkBack)
                            {
                                output.AppendFormat(
                                    "<a href=\"{0}\"><b>{1}</b></a><br>",
                                    SE.MakeProductLink(cartItem.ItemCounter.ToString(), cartItem.DisplayName),
                                    Security.HtmlEncode(cartItem.DisplayName)
                                );
                            }
                            else
                            {
                                output.AppendFormat("<b>{0}</b><br>", cartItem.ItemCode);
                            }
                            bool isCurrencyIncludedForInventorySelling = InterpriseHelper.IsCurrencyIncludedForInventorySelling(cart.ThisCustomer.CurrencyCode);

                            // Special case 2 
                            //  Currency is added in Inventory Selling Currency late
                            //  after the kit has been created, ideally the kit should regenerate kit pricing for this currency
                            //  but for the meantime, we should handle this by looking into the home currency
                            bool currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem =
                            InterpriseHelper.CurrencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYet(cart.ThisCustomer.CurrencyCode, cartItem.ItemCode);

                            string kitDetailQuery =
                                string.Format(
                                    "exec GetEcommerceKitCartDetail @ItemKitCode = {0}, @CurrencyCode = {1}, @LanguageCode = {2}, @CustomerCode = {3}, @CartID = {4}, @ContactCode = {5}",
                                    cartItem.ItemCode.ToDbQuote(),
                                    (isCurrencyIncludedForInventorySelling && !currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem) ? cart.ThisCustomer.CurrencyCode.ToDbQuote() : Currency.GetHomeCurrency().ToDbQuote(), //cart.ThisCustomer.CurrencySetting, 
                                    cart.ThisCustomer.LocaleSetting.ToDbQuote(),
                                    cart.ThisCustomer.CustomerCode.ToDbQuote(),
                                    cartItem.Id.ToString().ToDbQuote(),
                                    cart.ThisCustomer.ContactCode.ToDbQuote()
                                );

                            // read the records from the EcommerceKitCart table...
                            using (var con = DB.NewSqlConnection())
                            {
                                con.Open();
                                using (var reader = DB.GetRSFormat(con, kitDetailQuery))
                                {
                                    while (reader.Read())
                                    {
                                        string itemName = DB.RSField(reader, "ItemDescription");
                                        decimal quantity = Convert.ToDecimal(DB.RSFieldDecimal(reader, "Quantity"));

                                        output.AppendFormat("&nbsp;&nbsp;-&nbsp;({0}) {1}", quantity, Security.HtmlEncode(itemName));
                                        output.Append("<br />");
                                    }
                                }
                            }

                            output.Append("<br />");
                            output.Append("</div>");
                            break;

                        default:
                            if (showLinkBack)
                            {
                                output.AppendFormat(
                                    "<a href=\"{0}\"><b>{1}</b></a><br>",
                                    SE.MakeProductLink(cartItem.ItemCounter.ToString(), cartItem.DisplayName), Security.HtmlEncode(cartItem.DisplayName)
                                );
                            }
                            else
                            {
                                output.AppendFormat("<b>{0}</b><br>", Security.HtmlEncode(cartItem.DisplayName));
                            }
                            break;
                    }
                    /*************** multiple address **********************/
                    if (cart.HasMultipleShippingAddresses() && !cartItem.IsDownload && !cartItem.IsService)
                    {
                        output.Append(AppLogic.GetString("shoppingcart.cs.24"));
                        output.Append(" ");

                        Address adr = new Address();
                        bool includename = true;

                        if (cartItem.m_ShippingAddressID == cart.ThisCustomer.PrimaryShippingAddressID)
                        {
                            adr.LoadByCustomer(cart.ThisCustomer, AddressTypes.Shipping, cartItem.m_ShippingAddressID);
                            output.Append(AppLogic.GetString("account.aspx.10"));
                        }
                        else if (cartItem.m_ShippingAddressID == cart.ThisCustomer.PrimaryBillingAddressID)
                        {
                            adr.LoadByCustomer(cart.ThisCustomer, AddressTypes.Billing, cartItem.m_ShippingAddressID);
                            output.Append(AppLogic.GetString("account.aspx.8"));
                        }
                        else
                        {
                            output.Append(adr.Name);
                            includename = false;
                        }
                        output.Append("<div style=\"margin-left: 10px;\">");
                        output.Append(adr.DisplayString(false, false, includename, "<br>"));
                        output.Append("</div>");

                        output.Append("<div>");
                        output.Append(AppLogic.GetString("order.cs.23"));
                        output.Append(cartItem.m_ShippingMethod);
                        output.Append("</div>");
                    }
                    /*******************************************************/
                    output.Append("</td>");

                    if (!hideUnitMeasure)
                    {
                        // Unit Measure Column ********************************
                        output.Append("<td align=\"center\" valign=\"top\">");

                        // show the unit measure
                        var availableUnitMeasures = new List<KeyValuePair<string, string>>();
                        using (var con = DB.NewSqlConnection())
                        {
                            con.Open();
                            using (var reader = DB.GetRSFormat(con, "exec eCommerceGetProductUnitMeasureAvailability @CustomerCode = {0}, @ItemCode = {1}, @IncludeAllWarehouses = {2}, @Anon = {3}", DB.SQuote(cart.ThisCustomer.CustomerCode), DB.SQuote(cartItem.ItemCode), AppLogic.AppConfigBool("ShowInventoryFromAllWarehouses"), cart.ThisCustomer.IsNotRegistered))
                            {
                                while (reader.Read())
                                {
                                    availableUnitMeasures.Add(new KeyValuePair<string, string>(DB.RSField(reader, "UnitMeasureCode"), DB.RSField(reader, "UnitMeasureDescription")));
                                }
                            }
                        }

                        // NOTE :
                        //  We should be guaranteed to have at least 1 unit measure
                        //  and if it's 1 it should be the default...
                        //  so we can safely assume that the first index is the Default unit measure
                        if (availableUnitMeasures.Count > 1)
                        {
                            // render as combobox
                            output.AppendFormat("<select name=\"UnitMeasureCode_{0}\" id=\"UnitMeasureCode_{0}\" size=\"1\">", cartItem.m_ShoppingCartRecordID);
                            output.AppendLine();

                            foreach (var unitMeasureValue in availableUnitMeasures)
                            {
                                bool isCurrentUnitMeasureTheOneSelectedInProductPage = cartItem.UnitMeasureCode.Equals(unitMeasureValue.Key, StringComparison.InvariantCultureIgnoreCase);

                                output.AppendFormat("<option value=\"{0}\" {2}>{1}</option>",
                                    Security.HtmlEncode(unitMeasureValue.Key),
                                    Security.HtmlEncode(unitMeasureValue.Value),
                                    CommonLogic.IIF((isCurrentUnitMeasureTheOneSelectedInProductPage), "selected=\"selected\"", string.Empty));
                            }

                            output.Append("</select>");
                            output.AppendLine();
                        }
                        else
                        {
                            // register the unit measure control
                            output.AppendFormat("<input type=\"hidden\" id=\"UnitMeasureCode_{0}\" name=\"UnitMeasureCode_{0}\" value=\"{1}\" />", cartItem.m_ShoppingCartRecordID, Security.HtmlEncode(cartItem.UnitMeasureCode));
                            output.AppendLine();
                            // render as span
                            output.AppendFormat("<span >{0}</span>", Security.HtmlEncode(availableUnitMeasures[0].Value));
                            output.AppendLine();
                        }

                        output.Append("</td>");
                        // End Unit Measure Column...
                    }

                    // Quantity and Delete
                    output.Append("<td align=\"center\" valign=\"top\">");

                    if (cartItem.RestrictedQuantities.Count > 0)
                    {
                        output.AppendFormat("<select name=\"Quantity_{0}\" id=\"Quantity_{0}\" size=\"1\" >&nbsp;", cartItem.m_ShoppingCartRecordID);
                        if (AppLogic.AppConfigBool("ShowCartDeleteItemButton"))
                        {
                            output.Append("<option value=\"0\">DELETE</option>");
                        }

                        foreach (int restrictedQuantity in cartItem.RestrictedQuantities)
                        {
                            output.AppendFormat(
                                "<option value=\"{0}\" {1}>{0}</option>",
                                restrictedQuantity,
                                CommonLogic.IIF((restrictedQuantity == cartItem.m_Quantity), "selected=\"selected\"", string.Empty)
                            );
                        }
                        output.Append("</select>");
                    }
                    else
                    {
                        output.AppendFormat("<input type=\"text\" name=\"Quantity_{0}\" id=\"Quantity_{0}\" size=\"4\" value=\"{1}\" maxlength=\"14\">&nbsp;", cartItem.m_ShoppingCartRecordID, Localization.ParseLocaleDecimal(cartItem.m_Quantity, cart.ThisCustomer.LocaleSetting));

                        string deleteCaption = AppLogic.GetString("shoppingcart.cs.31", true);
                        deleteCaption = string.IsNullOrEmpty(deleteCaption) ? "Delete" : deleteCaption;
                        if (AppLogic.AppConfigBool("ShowCartDeleteItemButton"))
                        {

                            if (Customer.Current.IsInEditingMode())
                            {
                                output.AppendFormat("<span class='content' data-contentKey='shoppingcart.cs.31' data-contentValue=\"{0}\" data-contentType='string resource'>{0}</span>", deleteCaption);
                            }
                            else
                            {
                                output.AppendFormat("<input type=\"submit\" name=\"bt_Delete\" class=\"site-button\" value=\"{0}\" onclick=\"this.form.Quantity_{1}.value='0';\">", deleteCaption, cartItem.m_ShoppingCartRecordID);
                            }
                        }
                    }
                    output.AppendFormat("<input type=\"hidden\" name=\"MinOrderQuantity_{0}\" id=\"MinOrderQuantity_{0}\" value=\"{1}\" >\n", cartItem.m_ShoppingCartRecordID, cartItem.m_MinimumQuantity);

                    output.Append("</td>");

                    // Move to shopping cart button
                    output.Append("<td align=\"right\" valign=\"top\">");
                    if (AppLogic.ItemHasVisibleBuyButton(cartItem.ItemCode))
                    {
                        string qtyElement = String.Format("Quantity_{0}", cartItem.m_ShoppingCartRecordID);

                        if (Customer.Current.IsInEditingMode())
                        {
                            output.AppendFormat("<span class='content' data-contentKey='shoppingcart.cs.3' data-contentValue=\"{0}\" data-contentType='string resource'>{0}</span>&nbsp;&nbsp;", _stringResourceService.GetString("shoppingcart.cs.3", true));
                            output.AppendFormat("<span class='content' data-contentKey='AppConfig.CartPrompt' data-contentValue=\"{0}\" data-contentType='string resource'>{0}</span>", _stringResourceService.GetString("AppConfig.CartPrompt", true));
                        }
                        else
                        {

                            string quantityValiadtionMessage = (AppLogic.IsAllowFractional) ? String.Format(_stringResourceService.GetString("common.cs.26"), AppLogic.InventoryDecimalPlacesPreference.ToString()) : String.Empty;
                            output.AppendFormat("<input type=\"button\" class=\"move-to-shopping-cart site-button content\" value='{0}' data-elementId='{1}' data-cartId='{2}' data-messageQuantityInvalid='{3}' data-messageQuantityEmpty='{4}' data-quantityRegEx='{5}'>",
                                                    String.Format(_stringResourceService.GetString("shoppingcart.cs.3", true), _stringResourceService.GetString("AppConfig.CartPrompt", true)),
                                                    qtyElement ,
                                                    cartItem.m_ShoppingCartRecordID.ToString(),
                                                    String.Format("{0}\n{1}", _stringResourceService.GetString("common.cs.22", true), quantityValiadtionMessage),
                                                    _stringResourceService.GetString("common.cs.24", true),
                                                    AppLogic.GetQuantityRegularExpression(cartItem.ItemType, false)
                                                  );
                        }
                    }
                    else
                    {
                        output.Append("&nbsp;");
                    }
                    output.Append("</td>");
                }

                output.Append("</tr>\n");
                output.Append("</table>\n");
            }
        }
        #endregion
    }
}









