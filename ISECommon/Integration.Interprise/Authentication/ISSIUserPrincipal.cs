// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Principal;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Authentication
{
    public class ISSIUserPrincipal : IPrincipal
    {
        #region Variable Declaration

        private IIdentity _identity = null;
        private ISSIUserAccount _userAccount = null;

        #endregion

        public ISSIUserPrincipal(IIdentity identity)
        {
            _identity = identity;
        }

        #region IPrincipal Members

        public IIdentity Identity
        {
            get { return _identity; }
        }

        public bool IsInRole(string role)
        {
            return true;
        }

        #region UserAccount
        
        /// <summary>
        /// Gets the associated User account for this principal
        /// </summary>
        public ISSIUserAccount UserAccount
        {
            get 
            {
                if (_userAccount == null)
                {
                    _userAccount = ISSIUserAccount.Find(_identity.Name);
                }

                return _userAccount;
            }
        }

        #endregion

        #endregion
    }
}
