// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Authentication
{
    internal sealed class Qualifier
    {
        private List<string> _patterns = new List<string>();

        public Qualifier() {}

        public List<string> Patterns
        {
            get { return _patterns; }
        }

        public bool Qualifies(string resource)
        {
            foreach (string pattern in _patterns)
            {
                Regex exp = new Regex(pattern, RegexOptions.Compiled | RegexOptions.IgnorePatternWhitespace | RegexOptions.IgnoreCase);
                if (exp.IsMatch(resource))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
