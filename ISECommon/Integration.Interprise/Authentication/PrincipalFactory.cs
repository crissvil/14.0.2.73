// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Principal;
using System.Reflection;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Authentication
{
    internal static class PrincipalFactory
    {
        public static IPrincipal CreatePrincipal(Type principalType, IIdentity identity)
        {
            return Activator.CreateInstance(principalType, new object[] {identity}) as IPrincipal;
        }
    }
}
