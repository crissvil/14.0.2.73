// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class ConjunctedLogicalExpression : LogicalExpression
    {
        private ILogicalExpression _conjunctee = null;
        private IConjunction _conjunction = null;

        public ConjunctedLogicalExpression(ILogicalExpression conjunctee, IConjunction conjunction)
            : base(conjunctee.Left, conjunctee.Right)
        {
            _conjunctee = conjunctee;
            _conjunction = conjunction;
        }

        public override string ToSqlSelectString()
        {
            return _conjunctee.ToSqlSelectString() + _conjunction.ToSqlSelectString();
        }
    }
}
