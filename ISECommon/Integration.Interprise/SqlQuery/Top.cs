// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class Top : SelectBase, ITop
    {
        private int _count;

        public Top(int count)
        {
            _count = count;
        }


        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }

        public override string ToSqlSelectString()
        {
            return " TOP " + _count.ToString() + " ";
        }
    }
}
