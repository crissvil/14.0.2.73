// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class LogicalExpression : SelectBase, ILogicalExpression
    {
        private ISelect _left;
        private ISelect _right;

        protected LogicalExpression(ISelect left, ISelect right)
        {
            _left = left;
            _right = right;
        }

        public ISelect Left
        {
            get { return _left; }
        }

        public ISelect Right
        {
            get { return _right; }
        }

        public ILogicalExpression Conjunct(IConjunction conjunction)
        {
            return new ConjunctedLogicalExpression(this, conjunction);
        }

        public override string ToSqlSelectString()
        {
            throw new NotImplementedException();
        }
    }
}
