// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class ColumnCollection : NameObjectCollectionBase 
    {
        public IColumn this[int index]
        {
            get { return BaseGet(index) as IColumn; }
            set { BaseSet(index, value); }
        }

        public IColumn this[string columnName]
        {
            get { return BaseGet(columnName) as IColumn; }
            set { BaseSet(columnName, value); }
        }

        public void Add(IColumn column)
        {
            BaseAdd(column.ColumnName, column);
        }

        public override IEnumerator GetEnumerator()
        {
            return base.BaseGetAllValues().GetEnumerator();
        }
    }
}
