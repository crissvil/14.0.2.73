// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class OrConjunction : SelectBase, IConjunction
    {
        ILogicalExpression _expression;

        public OrConjunction(ILogicalExpression targetExpression)
        {
            _expression = targetExpression;
        }

        public override string ToSqlSelectString()
        {
            return " OR " + _expression.ToSqlSelectString();
        }
    }
}
