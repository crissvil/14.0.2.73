// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public interface ITable : ISelect
    {
        string TableName { get;}
        string Alias { get;set;}
        bool WithNoLock { get;set;}
        ColumnCollection Columns { get;}
        IOrderBy OrderBy { get;set;}
        ITop Top { get;set;}
        List<ITable> JoinTables { get;}
        ILogicalExpression Qualifier { get; set;}
    }
}
