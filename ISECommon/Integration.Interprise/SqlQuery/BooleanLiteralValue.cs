// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class BooleanLiteralValue : SelectBase, ILiteralValue
    {
        private bool _value;

        public BooleanLiteralValue(bool value)
        {
            _value = value;
        }

        public override string ToSqlSelectString()
        {
            return _value ? "1" : "0";
        }
    }
}
