// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public enum OrderByDirection
    {
        Ascending,
        Descending
    }

    public interface IOrderBy : ISelect
    {
        IColumn Column { get;}
        OrderByDirection Direction { get;}
    }
}
