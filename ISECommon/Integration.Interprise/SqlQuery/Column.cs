// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class Column : SelectBase, IColumn
    {
        private ITable _table;
        private string _columnName;
        private Type _type;
        private string _friendlyName;

        public Column(ITable table, string columnName, Type type)
        {
            _table = table;
            _columnName = columnName;
            _type = type;
        }

        public Column(ITable table, string columnName, Type type, string friendlyName)
        {
            _table = table;
            _columnName = columnName;
            _type = type;
            _friendlyName = friendlyName;
        }

        public override string ToSqlSelectString()
        {
            return string.Format("[{0}].[{1}]", CommonLogic.IIF(CommonLogic.IsStringNullOrEmpty(_table.Alias) == false, _table.Alias , _table.TableName), _columnName);
        }

        public ITable Table
        {
            get { return _table; }
        }

        public string ColumnName
        {
            get { return _columnName; }
            set { _columnName = value; }
        }

        public Type DataType
        {
            get { return _type; }
            set { _type = value; }
        }

        public string FriendlyName
        {
            get { return _friendlyName; }
            set { _friendlyName = value; }
        }
    }
}
