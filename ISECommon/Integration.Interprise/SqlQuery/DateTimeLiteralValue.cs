// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class DateTimeLiteralValue : SelectBase, ILiteralValue
    {
        private DateTime _value;

        public DateTimeLiteralValue(DateTime value)
        {
            _value = value;
        }

        public override string ToSqlSelectString()
        {
            return string.Format("'{0}'", _value);
        }
    }
}
