// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class WhereClause : SelectBase, IQualifier
    {
        private List<ILogicalExpression> _expressions = new List<ILogicalExpression>();

        public List<ILogicalExpression> Expressions
        {
            get { return _expressions; }
        }

        public void AddExpression(ILogicalExpression expression)
        {
            _expressions.Add(expression);
        }

        public override string ToSqlSelectString()
        {
            string sql = string.Empty;
            // check if we have the first item
            if (_expressions.Count > 0)
            {
                sql += " WHERE ";
                sql += _expressions[0].ToSqlSelectString();

                for (int ctr = 1; ctr < _expressions.Count; ctr++)
                {
                    ILogicalExpression expression = _expressions[0];
                    sql = expression.ToSqlSelectString();
                }
            }

            return sql;
        }
    }
}
