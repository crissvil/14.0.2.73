// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class RestrictedLogicalExpression : LogicalExpression
    {
        private ILogicalExpression _expression;
        public RestrictedLogicalExpression(ILogicalExpression expression)
            : base(expression.Left, expression.Right)
        {
            _expression = expression;
        }

        public override string ToSqlSelectString()
        {
            return " (" + _expression.ToSqlSelectString() + ") ";
        }
    }
}
