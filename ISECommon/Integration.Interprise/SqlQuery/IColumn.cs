// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public interface IColumn : ISelect
    {
        ITable Table { get;}
        string ColumnName { get;set;}
        Type DataType { get;set;}
        string FriendlyName {get;set;}
    }
}
