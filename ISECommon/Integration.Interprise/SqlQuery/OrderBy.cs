// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class OrderBy : SelectBase, IOrderBy
    {
        private IColumn _column;
        private OrderByDirection _direction;

        public OrderBy(IColumn column, OrderByDirection direction)
        {
            _column = column;
            _direction = direction;
        }

        public override string ToSqlSelectString()
        {
            string sql = " ORDER BY " + _column.ToSqlSelectString();
            switch (_direction)
            {
                case OrderByDirection.Ascending:
                    sql += " ASC ";
                    break;
                case OrderByDirection.Descending:
                    sql += " DESC ";
                    break;
            }

            return sql;
        }

        public IColumn Column
        {
            get { return _column; }
        }

        public OrderByDirection Direction
        {
            get { return _direction; }
        }
    }
}
