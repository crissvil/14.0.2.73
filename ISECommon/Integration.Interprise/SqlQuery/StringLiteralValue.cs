// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using InterpriseSuiteEcommerceCommon;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class StringLiteralValue : SelectBase, ILiteralValue
    {
        private string _value;

        public StringLiteralValue(string value)
        {
            _value = value;
        }

        public override string ToSqlSelectString()
        {
            return string.Format("{0}", DB.SQuote(_value));
        }
    }
}
