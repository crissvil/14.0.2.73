// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class EqualLogicalExpression : LogicalExpression
    {
        public EqualLogicalExpression(ISelect left, ISelect right) : base(left, right) { }

        public override string ToSqlSelectString()
        {
            return Left.ToSqlSelectString() + " = " + Right.ToSqlSelectString();
        }
    }
}
