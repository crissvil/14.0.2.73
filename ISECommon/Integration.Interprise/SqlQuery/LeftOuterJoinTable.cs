// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class LeftOuterJoinTable : TableSelect
    {
        private ILogicalExpression _qualifier;

        public LeftOuterJoinTable(string tableName) : base(tableName) { }

        public override ILogicalExpression Qualifier
        {
            get { return _qualifier; }
            set { _qualifier = value; }
        }

        public override string ToSqlSelectString()
        {
            string sql = string.Empty;
            sql += string.Format(" LEFT OUTER JOIN {0} {1} {2}",
                        base.TableName,
                        CommonLogic.IIF(!CommonLogic.IsStringNullOrEmpty(base.Alias), base.Alias, string.Empty),
                        CommonLogic.IIF(this.WithNoLock, " WITH (NOLOCK) ", string.Empty));

            if (null == _qualifier)
                throw new ArgumentException("Qualifier not specified on join clause!!!");

            // inner join members
            sql += " ON ";
            sql += _qualifier.ToSqlSelectString();

            return sql;
        }
    }
}
