// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery
{
    public class IntegerLiteralValue : SelectBase, ILiteralValue
    {
        private int _value;

        public IntegerLiteralValue(int value)
        {
            _value = value;
        }

        public override string ToSqlSelectString()
        {
            return _value.ToString();
        }
    }

}
