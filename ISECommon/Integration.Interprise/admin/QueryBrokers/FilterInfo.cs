// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery;
using System.Web;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.QueryBrokers
{
    public class FilterInfo
    {
        private int _topCount;
        private string _orderByColumn;
        private OrderByDirection _orderByDirection;
        private DateFilterOption _dateFilterOption;

        private DateTime _fromDate;
        private DateTime _toDate;

        private FilterInfo()
        {
        }

        public int TopCount
        {
            get { return _topCount; }
            set { _topCount = value; }
        }

        public string OrderByColumn
        {
            get { return _orderByColumn; }
            set { _orderByColumn = value; }
        }

        public OrderByDirection OrderByDirection
        {
            get { return _orderByDirection; }
            set { _orderByDirection = value; }
        }

        public DateFilterOption DateFilterOption
        {
            get { return _dateFilterOption; }
            set { _dateFilterOption = value; }
        }

        public DateTime FromDate
        {
            get { return _fromDate; }
            set { _fromDate = value; }
        }

        public DateTime ToDate
        {
            get { return _toDate; }
            set { _toDate = value; }
        }

        public static FilterInfo ExtractFrom(NameValueCollection form)
        {
            FilterInfo info = new FilterInfo();

            try
            {
                foreach (string name in form.AllKeys)
                {
                    string value = form[name];

                    if (name.EndsWith("txtTopCount"))
                    {
                        int topCount = 0;
                        if (int.TryParse(value, out topCount))
                        {
                            info.TopCount = topCount;
                        }
                    }
                    else if (name.EndsWith("cboOrderBy") && !string.IsNullOrEmpty(value))
                    {
                        string orderByColumn = value;
                        info.OrderByColumn = orderByColumn;
                    }
                    else if (name.EndsWith("cboOrderByDirection") && !string.IsNullOrEmpty(value))
                    {
                        string orderByDirectionValue = value;
                        OrderByDirection direction = (OrderByDirection)Enum.Parse(typeof(OrderByDirection), orderByDirectionValue);
                        info.OrderByDirection = direction;
                    }
                    else if (name.EndsWith("txtDateFrom"))
                    {
                        DateTime dateFrom = DateTime.MinValue;
                        if (DateTime.TryParse(value, out dateFrom))
                        {
                            info.FromDate = dateFrom;
                        }
                    }
                    else if (name.EndsWith("txtDateTo"))
                    {
                        DateTime dateTo = DateTime.MaxValue;
                        if (DateTime.TryParse(value, out dateTo))
                        {
                            info.ToDate = dateTo;
                        }
                    }
                    info.DateFilterOption = DateFilterOption.DateRange;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return info;
        }
    }
}
