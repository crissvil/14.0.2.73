// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.QueryBrokers
{
    public class ActiveCustomerQueryBroker : QueryBroker
    {
        private TableSelect _tableSelect = null;

        private void EnsureTableSelect()
        {
            if (null == _tableSelect)
            {
                _tableSelect = new TableSelect("CustomerActiveCustomersView");

                _tableSelect.WithColumn("CustomerCode");
                _tableSelect.WithColumn("CustomerName");
                _tableSelect.WithColumn("CustomerAddress");
                _tableSelect.WithColumn("CustomerCity");
                _tableSelect.WithColumn("CustomerState");
                _tableSelect.WithColumn("CustomerPostalCode");
                _tableSelect.WithColumn("CustomerCountry");
                _tableSelect.WithColumn("Balance");

                _tableSelect.Alias = "c";

                _tableSelect.Top = new Top(AppLogic.AppConfigUSInt("AdminInitialListCount"));
            }
        }

        public override TableSelect TableSelect
        {
            get 
            {
                EnsureTableSelect();
                return _tableSelect;
            }
        }
    }
}
