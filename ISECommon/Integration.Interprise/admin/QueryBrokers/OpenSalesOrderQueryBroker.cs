// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.QueryBrokers
{
    public class OpenSalesOrderQueryBroker : QueryBroker
    {
        private TableSelect _tableSelect = null;

        private void EnsureTableSelect()
        {
            if (null == _tableSelect)
            {
                _tableSelect = new TableSelect("CustomerSalesOrderSearchListView");

                IColumn colSalesOrderCode = _tableSelect.AddColumn("SalesOrderCode", typeof(string));
                _tableSelect.WithColumn("CustomerCode");
                _tableSelect.WithColumn("CustomerName");
                _tableSelect.WithColumn("Total");
                _tableSelect.WithColumn("Currency");
                _tableSelect.WithColumn("IsProcessed");
                _tableSelect.WithColumn("IsVoided");
                _tableSelect.WithColumn("Date", typeof(DateTime));

                _tableSelect.Alias = "sov";

                _tableSelect.Top = new Top(AppLogic.AppConfigUSInt("AdminInitialListCount"));
                
                _tableSelect.OrderBy = new OrderBy(colSalesOrderCode, OrderByDirection.Descending);

                //(Type = 'Sales Order' or Type = 'Back Order')
                IColumn colType = _tableSelect.AddColumn("Type", typeof(string));
                ILogicalExpression matchOnlySalesOrder = new EqualLogicalExpression(colType, new StringLiteralValue("Sales Order"));

                IColumn colIsProcessed = _tableSelect.Columns["IsProcessed"];
                IColumn colIsVoided = _tableSelect.Columns["IsVoided"];

                ILogicalExpression matchOnlyNotIsProcessed = new EqualLogicalExpression(colIsProcessed, new BooleanLiteralValue(false));
                ILogicalExpression matchOnlyNotIsVoided = new EqualLogicalExpression(colIsVoided, new BooleanLiteralValue(false));

                ILogicalExpression matchOnlyProcessedAndVoided = matchOnlyNotIsVoided.Conjunct(new AndConjunction(matchOnlyNotIsProcessed));

                ILogicalExpression matchOnlyTypeIsSalesorderAndProcessedAndVoided = matchOnlyProcessedAndVoided.Conjunct(new AndConjunction(matchOnlySalesOrder));

                _tableSelect.Qualifier = matchOnlyTypeIsSalesorderAndProcessedAndVoided;
            }
        }

        public override TableSelect TableSelect
        {
            get
            {
                EnsureTableSelect();
                return _tableSelect;
            }
        }

        public override void ApplyFilter(FilterInfo filter)
        {
            EnsureTableSelect();
            base.ApplyFilter(filter);

            IDateFilterStrategy dateFilterStrategy = DateFilterStrategyFactory.GetDateFilterStrategy(filter.DateFilterOption);
            if (null != dateFilterStrategy)
            {
                dateFilterStrategy.ApplyFilter(_tableSelect, filter, "Date");
            }
        }
    }
}
