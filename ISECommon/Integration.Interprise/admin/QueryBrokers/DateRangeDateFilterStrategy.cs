// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery;
using System.Web;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.QueryBrokers
{
    internal class DateRangeDateFilterStrategy : DateFilterStrategy
    {
        public override void ApplyFilter(TableSelect table, FilterInfo info, string dateColumn)
        {
            // verify
            if (DateFilterOption.DateRange != info.DateFilterOption)
            {
                throw new NotSupportedException("Only supports Date Range Filter Option");
            }

            if (DateTime.MinValue == info.FromDate)
            {
                throw new ArgumentException("Invalid argument for date from");
            }

            if (DateTime.MaxValue == info.ToDate)
            {
                throw new ArgumentException("Invalid argument for date to");
            }

            ILogicalExpression filter =
            new BetweenLogicalExpression(table.Columns[dateColumn], new DateTimeLiteralValue(info.FromDate), new DateTimeLiteralValue(info.ToDate));           

            if (null != table.Qualifier)
            {
                table.Qualifier = filter.Conjunct(new AndConjunction(table.Qualifier));
            }
            else
            {
                table.Qualifier = filter;
            }
        }
    }
}
