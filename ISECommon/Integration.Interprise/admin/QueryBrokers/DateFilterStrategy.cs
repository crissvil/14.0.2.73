// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery;
using System.Web;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.QueryBrokers
{
    internal abstract class DateFilterStrategy : IDateFilterStrategy
    {
        public abstract void ApplyFilter(TableSelect table, FilterInfo info, string dateColumn);
    }
}
