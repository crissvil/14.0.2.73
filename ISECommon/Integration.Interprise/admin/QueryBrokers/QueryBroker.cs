// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.QueryBrokers
{
    public abstract class QueryBroker : IQueryBroker
    {
        protected QueryBroker() {}

        public abstract TableSelect TableSelect { get;}
        public virtual void ApplyFilter(FilterInfo filter)
        {
            TableSelect table = this.TableSelect;
            table.Top = new Top(filter.TopCount);
            IColumn orderByColumn = table.Columns[filter.OrderByColumn];
            if (null != orderByColumn)
            {
                table.OrderBy = new OrderBy(orderByColumn, filter.OrderByDirection);
            }
        }
    }
}
