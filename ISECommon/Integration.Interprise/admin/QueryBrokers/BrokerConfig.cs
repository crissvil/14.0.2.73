// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;
using System.IO;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.QueryBrokers
{
    internal class BrokerConfig
    {
        private string _key;
        private Type _broker;

        internal BrokerConfig() { }

        internal string Key
        {
            get { return _key; }
            set { _key = value; }
        }

        internal Type Broker
        {
            get { return _broker; }
            set { _broker = value; }
        }
    }
}
