// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Xml;
using System.IO;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.SqlQuery;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.QueryBrokers
{
    public class FilterConfig
    {
        private bool _showDate;
        private string _dateColumn;

        public bool ShowDate
        {
            get { return _showDate; }
            set { _showDate = value; }
        }

        public string DateColumn
        {
            get { return _dateColumn; }
            set { _dateColumn = value; }
        }
    }
}
