// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.QueryBrokers
{
    internal static class DateFilterStrategyFactory
    {
        internal static IDateFilterStrategy GetDateFilterStrategy(DateFilterOption option)
        {
            if (DateFilterOption.None != option)
            {
                switch (option)
                {
                    case DateFilterOption.AllDates:
                        break;

                    case DateFilterOption.NumberOfDays:
                        break;

                    case DateFilterOption.Yesterday:
                        break;

                    case DateFilterOption.ThisWeek:
                        break;

                    case DateFilterOption.ThisMonth:
                        break;

                    case DateFilterOption.SpecificDate:
                        break;

                    case DateFilterOption.DateRange:
                        return new DateRangeDateFilterStrategy();
                }
            }

            return null;
        }
    }
}
