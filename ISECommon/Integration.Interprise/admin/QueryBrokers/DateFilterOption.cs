// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.QueryBrokers
{
    public enum DateFilterOption
    {
        None = 0,
        AllDates = 1,
        NumberOfDays = 2,
        Yesterday = 4,
        ThisWeek = 8,
        ThisMonth = 16,
        SpecificDate = 32,
        DateRange = 64
    }
}
