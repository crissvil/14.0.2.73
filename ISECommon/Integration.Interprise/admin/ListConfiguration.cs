// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Web;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.Web.Caching;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin
{
    /// <summary>
    /// Summary description for ListConfiguration
    /// </summary>
    public class ListConfiguration
    {
        #region Variable Declaration

        private const string LIST_CACHE_KEY = "ListConfig";
        private Dictionary<string, string> _controls = new Dictionary<string, string>();

        #endregion

        #region Constructor
        /// <summary>
        /// ListConfiguration
        /// </summary>
        private ListConfiguration() { }
        #endregion

        #region Properties

        #region ListControls
        /// <summary>
        /// Gets the collection of list controls
        /// </summary>
        public Dictionary<string, string> ListControls
        {
            get { return _controls; }
        }
        #endregion

        #region Instance
        /// <summary>
        /// Gets the configuration cached instance
        /// </summary>
        public static ListConfiguration Instance
        {
            get
            {
                if (HttpRuntime.Cache[LIST_CACHE_KEY] == null) LoadConfig();
                return HttpRuntime.Cache[LIST_CACHE_KEY] as ListConfiguration;
            }
        }
        #endregion

        #endregion

        #region Methods

        #region LoadConfig
        /// <summary>
        /// Loads the configuration and caches the instance
        /// </summary>
        private static void LoadConfig()
        {
            string configFile = HttpContext.Current.Server.MapPath("~/admin/list.config");
            if (!File.Exists(configFile)) throw new InvalidOperationException("Config file not found!!!");

            XmlDocument configDoc = new XmlDocument();
            configDoc.Load(configFile);

            ListConfiguration config = new ListConfiguration();

            XmlNodeList controlNodes = configDoc.SelectNodes("/list/controls/add");
            foreach (XmlNode controlNode in controlNodes)
            {
                string key = controlNode.Attributes["key"].Value;
                if (config.ListControls.ContainsKey(key)) throw new ArgumentException("key must be unique!", key);

                config.ListControls.Add(key, controlNode.Attributes["control"].Value);
            }

            HttpRuntime.Cache.Add(
                LIST_CACHE_KEY,
                config,
                new CacheDependency(configFile),
                Cache.NoAbsoluteExpiration,
                Cache.NoSlidingExpiration,
                CacheItemPriority.High,
                null
            );
        }
        #endregion

        #endregion

    }
}
