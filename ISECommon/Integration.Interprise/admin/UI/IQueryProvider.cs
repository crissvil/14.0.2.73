// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.UI
{
    /// <summary>
    /// Summary description for IQueryProvider
    /// </summary>
    public interface IQueryProvider
    {
        void Query(int top);
        void QueryStartsWith(int top, string startsWith);
    }
}