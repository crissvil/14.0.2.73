// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Data;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.UI
{
    /// <summary>
    /// Summary description for QueryProvider
    /// </summary>
    public interface IQueryControl
    {
        void Query(int top);
        void QueryStartsWith(int top, string startsWith);
    }
}
