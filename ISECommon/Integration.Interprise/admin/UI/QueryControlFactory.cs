// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.IO;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Admin.UI
{
    /// <summary>
    /// Summary description for QueryControlFactory
    /// </summary>
    public class QueryControlFactory
    {
        #region Methods

        #region GetControl
        /// <summary>
        /// Gets the associated control based on the key argument
        /// </summary>
        /// <param name="host">The page that will contain the control</param>
        /// <param name="key">The key of the control in the configuration file</param>
        /// <returns></returns>
        public static IQueryControl GetControl(Page host, string key)
        {
            if (host == null) throw new ArgumentException("Parameter host cannot be null");
            if (string.IsNullOrEmpty(key)) throw new ArgumentException("Parameter key cannot be null");

            string controlFileName = ListConfiguration.Instance.ListControls[key];
            if (string.IsNullOrEmpty(controlFileName)) return null;

            IQueryControl control = host.LoadControl(controlFileName) as IQueryControl;
            return control;
        }
        #endregion

        #endregion
    }
}
