// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System.Text;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration
{
    public class ShoppingCartPageSummaryLiteralRenderer : IShoppingCartHTMLLiteralRenderer
    {
        #region IShoppingCartHTMLLiteralRenderer Members

        public void Render(InterpriseShoppingCart cart, ref StringBuilder output)
        {
            if(!cart.IsEmpty())
            {
                output.Append("<table width=\"100%\">");
                output.Append("<tr>");
                output.AppendFormat("<td align=\"right\" width=\"100%\">{0}&#160;</td>", AppLogic.GetString("shoppingcart.cs.27"));
                output.Append("<td align=\"right\" valign=\"middle\">");

                decimal subTotal = cart.GetCartSubTotal();
                string subTotalFormatted = subTotal.ToCustomerCurrency();
                output.AppendFormat("<nobr>{0}</nobr>", subTotalFormatted);

                output.Append("</td>");
                output.Append("</tr>");

                output.Append("<tr>");                
                
                output.AppendFormat("<td align=\"right\" width=\"100%\">{0}&#160;</td>", AppLogic.GetString("shoppingcart.aspx.10"));
                output.Append("<td align=\"right\" valign=\"middle\">");
                output.AppendFormat("<nobr>{0}</nobr>", AppLogic.GetString("shoppingcart.aspx.12"));
                output.Append("</td>");
                output.Append("</tr>");

                string taxCaption = AppLogic.GetString("shoppingcart.aspx.11");
                if (!AppLogic.AppConfigBool("VAT.Enabled"))
                {
                    // Tax
                    output.Append("<tr>");
                    output.AppendFormat("<td align=\"right\" width=\"100%\">{0}&#160;</td>", taxCaption);
                    output.Append("<td align=\"right\" valign=\"middle\">");
                    output.AppendFormat("<nobr>{0}</nobr>", AppLogic.GetString("shoppingcart.aspx.12"));
                    output.Append("</td>");
                    output.Append("</tr>");
                }

                string couponCode = string.Empty;
                string couponErrorMessage = string.Empty;
                if (cart.HasCoupon(ref couponCode) && cart.IsCouponValid(cart.ThisCustomer, couponCode, ref couponErrorMessage))
                {
                    decimal couponDiscount = InterpriseHelper.GetCouponDiscountApplied(cart);
                    output.Append("<tr>");
                    output.AppendFormat("<td align=\"right\" width=\"100%\">{0}&#160;</td>", AppLogic.GetString("shoppingcart.cs.38"));
                    output.Append("<td align=\"right\" valign=\"middle\">");
                    output.AppendFormat("<nobr>{0}</nobr>", couponDiscount.ToCustomerCurrency());
                    output.Append("</td>");
                    output.Append("</tr>");
                }
                
                output.Append("</table>");
            }            
        }

        #endregion
    }
}
