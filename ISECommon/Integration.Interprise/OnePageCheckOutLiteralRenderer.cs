// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Data;
using System.Text;
using System.Web;
using Interprise.Framework.Customer.DatasetGateway;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using System.Data.SqlClient;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration
{
    public class OnePageCheckOutLiteralRenderer : IShoppingCartHTMLLiteralRenderer
    {
        public void Render(InterpriseShoppingCart cart, ref StringBuilder output)
        {
            bool showPicsInCart = AppLogic.AppConfigBool("ShowPicsInCart");
            bool showLinkBack = AppLogic.AppConfigBool("LinkToProductPageInCart");
            bool hideUnitMeasure = AppLogic.AppConfigBool("HideUnitMeasure");
            bool showShipDateInCart = AppLogic.AppConfigBool("ShowShipDateInCart");
            bool showStockHints = AppLogic.AppConfigBool("ShowStockHints");
            string msgforavailability = AppLogic.GetString("shoppingcart.cs.47");
            
            output.Append("<div align=\"left\">");
            output.Append("<br>");

            string couponCode = string.Empty;
            bool hasCoupon = cart.HasCoupon(ref couponCode);
            if (hasCoupon)
            {
                output.Append("<br/>");
                output.Append("<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\" style=\"border-style: solid; border-width: 0px; border-color: #" + AppLogic.AppConfig("HeaderBGColor") + "\">\n");
                output.Append("<tr><td align=\"left\" valign=\"top\">\n");
                output.Append("<img src=\"" + AppLogic.LocateImageURL("skins/Skin_" + cart.SkinID.ToString() + "/images/ShoppingCartCoupon.gif") + "\" border=\"0\"><br/>");
                output.Append("<table width=\"100%\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\" style=\"" + AppLogic.AppConfig("BoxFrameStyle") + "\">\n");
                output.Append("<tr><td align=\"left\" valign=\"top\">\n");

                output.Append(AppLogic.GetString("order.cs.12"));
                output.Append("&nbsp;");
                output.Append(HttpContext.Current.Server.HtmlEncode(couponCode));

                output.Append("</td></tr>\n");
                output.Append("</table>\n");
                output.Append("</td></tr>\n");
                output.Append("</table>\n");
            }

            output.Append("<br>");
            output.Append("<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\" style=\"border-style: solid; border-width: 0px; border-color: #" + AppLogic.AppConfig("HeaderBGColor") + "\">\n");
            output.Append("<tr><td align=\"left\" valign=\"bottom\">\n");
            output.Append("<img src=\"" + AppLogic.LocateImageURL("skins/Skin_" + cart.SkinID.ToString() + "/images/orderinfo.gif") + "\" align=\"absbottom\" border=\"0\"> " + AppLogic.GetString("checkoutcard.aspx.1") + " <a href=\"ShoppingCart.aspx\">" + AppLogic.GetString("checkoutcard.aspx.2") + "</a>.<br>");
            output.Append("<table width=\"100%\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\" style=\"" + AppLogic.AppConfig("BoxFrameStyle") + "\">\n");
            output.Append("<tr><td align=\"left\" valign=\"top\">\n");

            // Line Items
            output.Append("<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\">");
            output.Append("<tr>");
            if (showPicsInCart)
            {
                output.Append("<td align=\"center\" valign=\"middle\"><b>");
                output.Append(AppLogic.GetString("shoppingcart.cs.1"));
                output.Append("</b></td>");
                output.Append("<td align=\"center\" valign=\"middle\">&nbsp;</td>");
            }
            else
            {
                output.Append("<td align=\"left\" valign=\"middle\">");
                output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.cs.1"));
                output.Append("</td>");
            }

            if (showStockHints)
            {
                if (showShipDateInCart)
                {
                    // Shipping Dates Header...
                    output.Append("<td align=\"center\" valign=\"middle\">");
                    output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.aspx.17"));
                    output.Append("</td>");
                }
            }

            if (!hideUnitMeasure)
            {
                // Unit Measure Column Header
                output.Append("<td align=\"center\" valign=\"middle\">");
                output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.cs.37"));
                output.Append("</td>");
            }

            // Quantity Column Header
            output.Append("<td align=\"center\" valign=\"middle\">");
            output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.cs.2"));
            output.Append("</td>");

            // Price Sub-Total Column Header..
            output.Append("<td align=\"right\" valign=\"middle\">");
            output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.cs.27"));
            output.Append("</td>");

            output.Append("</tr>");

            foreach (CartItem item in cart.CartItems)
            {
                // get the associated cart item for this line item...
                SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow lineItemRow = item.AssociatedLineItemRow;
                // Divider
                output.Append("<tr>");
                output.AppendFormat("<td colspan=\"{0}\">", CommonLogic.IIF(showPicsInCart, 6, CommonLogic.IIF(showShipDateInCart, 5, 4)));
                output.Append("<hr style=\"height: 1px; width:100%; color: #DDDDDD;\" />");
                output.Append("</td>");
                output.Append("</tr>");
                output.Append("<tr>");

                if (showPicsInCart)
                {
                    // PICTURE COL:
                    output.Append("<td align=\"center\" valign=\"top\">");
                    string picUrl = InterpriseSuiteEcommerceCommon.InterpriseHelper.LookUpImageByItemCode(lineItemRow.ItemCode, "icon", cart.ThisCustomer);
                    if (showLinkBack)
                    {
                        output.Append("<a href=\"" + InterpriseSuiteEcommerceCommon.InterpriseHelper.MakeItemLink(lineItemRow.ItemCode) + "\">");
                    }

                    ProductImage img = ProductImage.Locate("product", item.ItemCounter, "icon");
                    if (null != img)
                    {
                        output.Append("<img src=\"" + img.src + "\" border=\"0\">");
                    }

                    if (showLinkBack)
                    {
                        output.Append("</a>");
                    }

                    output.Append("</td>");
                }

                /*********************************
                 * Line Item
                 * *******************************/
                output.Append("<td align=\"left\" valign=\"top\">");

                switch ((string)lineItemRow["ItemType"])
                {
                    case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP:
                    case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM:
                        if (showLinkBack)
                        {
                            using (SqlConnection con = DB.NewSqlConnection())
                            {
                                con.Open();
                                using (IDataReader reader = DB.GetRSFormat(con, "SELECT ItemCode FROM InventoryMatrixItem with (NOLOCK) WHERE MatrixItemCode = {0}", DB.SQuote(lineItemRow.ItemCode)))
                                {
                                    if (reader.Read())
                                    {
                                        output.AppendFormat(
                                            "<a href=\"{0}\"><b>{1}</b></a><br>",
                                            InterpriseSuiteEcommerceCommon.InterpriseHelper.MakeItemLink(DB.RSField(reader, "ItemCode")),
                                            Security.HtmlEncode(CommonLogic.IIF(!string.IsNullOrEmpty(lineItemRow.ItemDescription), lineItemRow.ItemDescription, lineItemRow.ItemName))
                                        );
                                    }
                                }
                            }
                        }
                        else
                        {
                            output.AppendFormat("<b>{0}</b><br>", lineItemRow.ItemCode);
                        }
                        // display the details

                        using (SqlConnection con = DB.NewSqlConnection())
                        {
                            con.Open();
                            using (IDataReader reader = DB.GetRSFormat(con, String.Format("exec eCommerceGetMatrixItemAttributes @ItemCode = NULL, @MatrixItemCode = {0}, @WebsiteCode = {1}, @CurrentDate = {2}, @LanguageCode = {3}, @ContactCode = {4}", DB.SQuote(lineItemRow.ItemCode), DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode), DB.SQuote(Localization.DateTimeStringForDB(DateTime.Now)), DB.SQuote(Customer.Current.LanguageCode), DB.SQuote(Customer.Current.ContactCode))))
                            {
                                if (reader.Read())
                                {
                                    for (int ctr = 1; ctr <= 6; ctr++)
                                    {
                                        string attribute = DB.RSField(reader, string.Format("Attribute{0}", ctr));
                                        string attributeValue = DB.RSField(reader, string.Format("Attribute{0}ValueDescription", ctr));

                                        if (!string.IsNullOrEmpty(attribute) && !string.IsNullOrEmpty(attributeValue))
                                        {
                                            output.AppendFormat(
                                                "&nbsp;&nbsp;{0}:{1}<br />",
                                                Security.HtmlEncode(attribute),
                                                Security.HtmlEncode(attributeValue)
                                            );
                                        }
                                    }
                                }
                            }
                        }
                        if (item.Status == "P" && item.IsOutOfStock)
                        {
                            output.AppendFormat("<img src=\"skins/Skin_{0}/images/outofstock.gif\">", cart.SkinID);
                        }
                        break;

                    case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT:
                        output.Append("<div style=\"margin-left: 10px;\">");
                        if (showLinkBack)
                        {
                            output.AppendFormat(
                                "<a href=\"{0}\"><b>{1}</b></a><br>",
                                InterpriseSuiteEcommerceCommon.InterpriseHelper.MakeItemLink((string)lineItemRow["ItemCode"]),
                                Security.HtmlEncode(CommonLogic.IIF(!string.IsNullOrEmpty(lineItemRow.ItemDescription), lineItemRow.ItemDescription, lineItemRow.ItemName))
                            );
                        }
                        else
                        {
                            output.AppendFormat("<b>{0}</b><br>", lineItemRow.ItemCode);
                        }

                        output.AppendFormat(
                            "&nbsp;&nbsp;<a href=\"{0}\"><img src=\"skins/Skin_{1}/images/edit.gif\" align=\"absmiddle\" border=\"0\" alt=\"{2}\"></a>&nbsp;",
                            InterpriseSuiteEcommerceCommon.InterpriseHelper.MakeItemLink((string)lineItemRow["ItemCode"]) + string.Format("?kcid={0}", item.Id.ToString()),
                            cart.SkinID,AppLogic.GetString("shoppingcart.cs.4")
                        );
                        output.Append("<br />");
                        // render the child items
                        DataRelation kitDetailRelation =
                            cart.SalesOrderDataset.GetRelation(
                                cart.SalesOrderDataset.CustomerSalesOrderDetailView,
                                cart.SalesOrderDataset.CustomerItemKitDetailView
                            );

                        foreach (SalesOrderDatasetGateway.CustomerItemKitDetailViewRow kitItemDetail in
                                lineItemRow.GetChildRows(kitDetailRelation))
                        {
                            output.AppendFormat("&nbsp;&nbsp;-&nbsp;({0}) {1}", Localization.ParseLocaleDecimal(kitItemDetail.QuantityPerKit, cart.ThisCustomer.LocaleSetting), Security.HtmlEncode(kitItemDetail.ItemDescription));
                            output.Append("<br />");
                        }

                        output.Append("<br />");
                        output.Append("</div>");

                        if (cart.HasNoStockPhasedOutItem)
                        {
                            output.AppendFormat("<img src=\"skins/Skin_{0}/images/outofstock.gif\">", cart.SkinID);
                        }

                        break;

                    default:
                        if (showLinkBack)
                        {
                            output.AppendFormat(
                                "<a href=\"{0}\"><b>{1}</b></a><br>",
                                InterpriseSuiteEcommerceCommon.InterpriseHelper.MakeItemLink((string)lineItemRow["ItemCode"]),
                                Security.HtmlEncode(CommonLogic.IIF(!string.IsNullOrEmpty(lineItemRow.ItemDescription), lineItemRow.ItemDescription, lineItemRow.ItemName))
                            );
                        }
                        else
                        {
                            output.AppendFormat("<b>{0}</b><br>",
                                Security.HtmlEncode(CommonLogic.IIF(!string.IsNullOrEmpty(lineItemRow.ItemDescription), lineItemRow.ItemDescription, lineItemRow.ItemName)));
                        }

                        if (item.Status == "P" && item.IsOutOfStock)
                        {
                            output.AppendFormat("<img src=\"skins/Skin_{0}/images/outofstock.gif\">", cart.SkinID);
                        }
                        break;
                }
                /*************** multiple address **********************/
                if (cart.HasMultipleShippingAddresses() && !item.IsDownload)
                {
                    output.Append(AppLogic.GetString("shoppingcart.cs.24"));
                    output.Append(" ");

                    Address adr = new Address();
                    bool includename = true;
                    adr.LoadByCustomer(cart.ThisCustomer, AddressTypes.Shipping, item.m_ShippingAddressID);
                    if (item.m_ShippingAddressID == cart.ThisCustomer.PrimaryShippingAddressID)
                    {
                        output.Append(AppLogic.GetString("account.aspx.10"));
                    }
                    else if (item.m_ShippingAddressID == cart.ThisCustomer.PrimaryBillingAddressID)
                    {
                        output.Append(AppLogic.GetString("account.aspx.8"));
                    }
                    else
                    {
                        output.Append(adr.Name);
                        includename = false;
                    }
                    output.Append("<div style=\"margin-left: 10px;\">");
                    output.Append(adr.DisplayString(false, false, includename, "<br>"));
                    output.Append("</div>");

                    output.Append("<div>");
                    output.Append(AppLogic.GetString("order.cs.23"));
                    output.Append(item.m_ShippingMethod);
                    output.Append("</div>");
                }
                /*******************************************************/
                output.Append("</td>");

                if (showStockHints)
                {
                    if (showShipDateInCart)
                    {
                        //Exclude the ff. item types for stock reservation
                        if ((string)lineItemRow["ItemType"] == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_NON_STOCK || (string)lineItemRow["ItemType"] == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_SERVICE ||
                            (string)lineItemRow["ItemType"] == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ELECTRONIC_DOWNLOAD || (string)lineItemRow["ItemType"] == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                        {
                            output.Append("<td align=\"left\" valign=\"top\"></td>");
                        }
                        else
                        {
                            CartItem.ReserveItemCollection reserveCol = cart.GetReservation(item.ItemCode);

                            if (item.Status == "P" && item.IsOutOfStock)
                            {
                                output.AppendFormat("<td align=\"center\" valign=\"top\"><i>{0}</i>", " ");
                            }
                            else
                            {
                                //Allocation and Reservation data
                                if (item.m_AllocatedQty > 0 && reserveCol.Count == 0)
                                {
                                    output.AppendFormat("<td align=\"center\" valign=\"top\"><i>{0} can be shipped immediately</i>", Localization.ParseLocaleDecimal(item.m_AllocatedQty, cart.ThisCustomer.LocaleSetting));
                                }
                                else
                                {

                                    output.AppendFormat("<td align=\"center\" valign=\"top\"><i>{0} {1}</i>", Localization.ParseLocaleDecimal(item.m_AllocatedQty, cart.ThisCustomer.LocaleSetting), msgforavailability);
                                }
                            }

                            for (int resCtr = 0; resCtr <= reserveCol.Count - 1; resCtr++)
                            {
                                ReserveItem reserved = reserveCol[resCtr];
                                if (item.ItemCode == reserved.ItemCode)
                                {
                                    if (resCtr == 0 && item.m_AllocatedQty == 0)
                                    {
                                        output.AppendFormat("<td align=\"center\" valign=\"top\"><i>{0} can be shipped on {1}</i>", Localization.ParseLocaleDecimal(reserved.QtyReserved, cart.ThisCustomer.LocaleSetting), reserved.ShipDate.ToShortDateString());
                                    }
                                    else
                                    {
                                        output.Append("<br/><i>" + Localization.ParseLocaleDecimal(reserved.QtyReserved, cart.ThisCustomer.LocaleSetting) + " can be shipped on " + reserved.ShipDate.ToShortDateString() + "</i>");
                                    }
                                }
                            }

                            //no allocation and reservation found!
                            if (item.m_AllocatedQty == 0 && reserveCol.Count == 0)
                            {
                                output.Append("<td align=\"left\" valign=\"top\"></td>");
                            }
                        }        
                    }
                }
                output.Append("</td>");

                if (!hideUnitMeasure)
                {
                    // Unit Measure Column ********************************
                    output.Append("<td align=\"center\" valign=\"top\">");

                    using (SqlConnection con = DB.NewSqlConnection())
                    {
                        con.Open();
                        using (IDataReader reader = DB.GetRSFormat(con, "SELECT UnitMeasureDescription FROM SystemUnitMeasure with (NOLOCK) WHERE UnitMeasureCode = {0}", DB.SQuote(lineItemRow.UnitMeasureCode)))
                        {
                            if (reader.Read())
                            {
                                output.Append(Security.HtmlEncode(DB.RSField(reader, "UnitMeasureDescription")));
                            }
                            else
                            {
                                output.Append(Security.HtmlEncode(lineItemRow.UnitMeasureCode));
                            }
                        }
                    }
                    output.Append("</td>");
                }

                // Quantity 
                output.AppendFormat("<td align=\"center\" valign=\"top\">{0}</td>", Localization.ParseLocaleDecimal(item.m_Quantity, cart.ThisCustomer.LocaleSetting));

                // Extended Price
                decimal extPriceRate = (decimal)item.AssociatedLineItemRow.Table.Compute("SUM(ExtPriceRate)", string.Format("ItemCode = '{0}' AND UnitMeasureCode = '{1}'", item.ItemCode, item.UnitMeasureCode));
                if (AppLogic.AppConfigBool("VAT.Enabled"))
                {
                    output.AppendFormat("<td align=\"right\" valign=\"top\">\n");

                    //decimal extPrice = lineItemRow.ExtPriceRate;
                    decimal vat = decimal.Zero;
                    object vatComputed = cart.SalesOrderDataset.TransactionItemTaxDetailView.Compute("SUM(TaxAmountRate)", string.Format("ItemCode = '{0}'", item.ItemCode));
                    if (vatComputed is decimal)
                    {
                        vat = Convert.ToDecimal(vatComputed);
                    }
                    string vatSettingText = string.Empty;

                    if (cart.ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                    {
                        extPriceRate += vat;
                        vatSettingText = AppLogic.GetString("showproduct.aspx.38");
                    }
                    else
                    {
                        vatSettingText = AppLogic.GetString("showproduct.aspx.37");
                    }

                    output.AppendFormat("{0} <span class=\"VATLabel\">{1}</span>\n", extPriceRate.ToCustomerCurrency(), vatSettingText);
                    output.AppendFormat("<br />\n");
                    output.AppendFormat("<span class=\"VATAmount\">{0} {1}</span>\n", AppLogic.GetString("showproduct.aspx.41"), vat.ToCustomerCurrency());

                    output.AppendFormat("</td>\n");
                }
                else
                {
                    output.AppendFormat("<td align=\"right\" valign=\"top\">{0}</td>", extPriceRate.ToCustomerCurrency());
                }

               
            }

            // Line Items..............

            output.Append("<br>");
            output.Append("</tr>\n");
            output.Append("</table>\n");
            output.Append("</td></tr>\n");
            output.Append("</table>\n");

            output.Append("</td></tr>\n");
            output.Append("</table></div>\n");

            StringWriter sw = new StringWriter(output);
            HtmlTextWriter writer = new HtmlTextWriter(sw);
            RenderSummary(cart, writer);

            writer.Flush();
        }

        private void RenderSummary(InterpriseShoppingCart cart, HtmlTextWriter writer)
        {
            // The values
            decimal subTotal = cart.SalesOrderDataset.CustomerSalesOrderView[0].SubTotalRate;
            decimal freight = cart.SalesOrderDataset.CustomerSalesOrderView[0].FreightRate;
            decimal tax = cart.SalesOrderDataset.CustomerSalesOrderView[0].TaxRate;
            decimal discount = cart.SalesOrderDataset.CustomerSalesOrderView[0].CouponDiscountRate;
            decimal dueTotal = cart.SalesOrderDataset.CustomerSalesOrderView[0].TotalRate;
            decimal balance = cart.SalesOrderDataset.CustomerSalesOrderView[0].BalanceRate;

            if (AppLogic.AppConfigBool("VAT.Enabled"))
            {
                if (cart.ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                {
                    subTotal += tax;
                }
            }

            string currencyCode = cart.ThisCustomer.CurrencyCode;

            // Captions
            string subTotalCaption = AppLogic.GetString("shoppingcart.cs.27");
            string freightCaption = AppLogic.GetString("shoppingcart.aspx.10");
            string taxCaption = AppLogic.GetString("shoppingcart.aspx.11");
            if (AppLogic.AppConfigBool("VAT.Enabled"))
            {
                taxCaption = AppLogic.GetString("shoppingcart.aspx.15");
            }
            string dueTotalCaption = AppLogic.GetString("shoppingcart.cs.1008");
            string discountCaption = AppLogic.GetString("shoppingcart.cs.38");
            string totalCaption = AppLogic.GetString("shoppingcart.cs.11");

            Panel divSummary = new Panel();
            divSummary.Attributes.Add("align", "right");

            Table tblSummary = new Table();
            tblSummary.Width = Unit.Percentage(100);

            // SubTotal Row
            TableRow rowSubTotal = new TableRow();

            TableCell cellSubTotalCaption = new TableCell();
            cellSubTotalCaption.Attributes.Add("align", "right");
            cellSubTotalCaption.Width = Unit.Percentage(80);
            cellSubTotalCaption.Controls.Add(new LiteralControl(string.Format("{0}&#160;", subTotalCaption)));
            rowSubTotal.Cells.Add(cellSubTotalCaption);

            TableCell cellSubTotalValue = new TableCell();
            cellSubTotalValue.Attributes.Add("align", "right");
            cellSubTotalValue.Attributes.Add("valign", "middle");
            cellSubTotalValue.Width = Unit.Percentage(20);
            cellSubTotalValue.Controls.Add(new LiteralControl(string.Format("<nobr><span id='subtotal_elem'>{0}</span></nobr>", subTotal.ToCustomerCurrency())));
            rowSubTotal.Cells.Add(cellSubTotalValue);

            tblSummary.Rows.Add(rowSubTotal);

            // Freight Row
            TableRow rowFreight = new TableRow();

            TableCell cellFreightCaption = new TableCell();
            cellFreightCaption.Attributes.Add("align", "right");
            cellFreightCaption.Width = Unit.Percentage(80);
            cellFreightCaption.Controls.Add(new LiteralControl(string.Format("{0}&#160;", freightCaption)));
            rowFreight.Cells.Add(cellFreightCaption);

            TableCell cellFreightValue = new TableCell();
            cellFreightValue.Attributes.Add("align", "right");
            cellFreightValue.Attributes.Add("valign", "middle");
            cellFreightValue.Width = Unit.Percentage(20);
            
            if (cart.HasShippableComponents())
            {
                if (cart.CouponIncludesFreeShipping(cart.ThisCustomer.CouponCode) || (InterpriseHelper.IsFreeShippingThresholdEnabled(cart.GetCartSubTotalExcludeOversized()) && InterpriseHelper.ShippingMethodCodeBelongsToFreeShippingMethodList(cart.SalesOrderDataset.CustomerSalesOrderView[0].ShippingMethodCode)))
                {
                    cellFreightValue.Controls.Add(new LiteralControl(AppLogic.GetString("shoppingcart.aspx.13")));
                }
                else
                {
                    cellFreightValue.Controls.Add(new LiteralControl(string.Format("<nobr><span id='fr_elem'>{0}</span></nobr>", freight.ToCustomerCurrency())));
                }
            }

            rowFreight.Cells.Add(cellFreightValue);

            tblSummary.Rows.Add(rowFreight);

            // Tax Row
            TableRow rowTax = new TableRow();

            TableCell cellTaxCaption = new TableCell();
            cellTaxCaption.Attributes.Add("align", "right");
            cellTaxCaption.Width = Unit.Percentage(80);
            cellTaxCaption.Controls.Add(new LiteralControl(string.Format("{0}&#160;", taxCaption)));
            rowTax.Cells.Add(cellTaxCaption);

            TableCell cellTaxValue = new TableCell();
            cellTaxValue.Attributes.Add("align", "right");
            cellTaxValue.Attributes.Add("valign", "middle");
            cellTaxValue.Width = Unit.Percentage(20);
            cellTaxValue.Controls.Add(new LiteralControl(string.Format("<nobr><span id='tax_elem'>{0}</span></nobr> {1}", tax.ToCustomerCurrency(), CommonLogic.IIF(cart.HasShippableComponents(), AppLogic.GetString("checkout1.aspx.8"), String.Empty))));
            rowTax.Cells.Add(cellTaxValue);

            tblSummary.Rows.Add(rowTax);

            bool hasCoupon =
            !cart.SalesOrderDataset.CustomerSalesOrderView[0].IsCouponIDNull() &&
            !string.IsNullOrEmpty(cart.SalesOrderDataset.CustomerSalesOrderView[0].CouponID);

            if (hasCoupon)
            {
                // Divider Row
                TableRow rowDueTotalDivider = new TableRow();

                // filler table cell
                rowDueTotalDivider.Cells.Add(new TableCell());

                TableCell cellDueTotalDivider = new TableCell();
                cellDueTotalDivider.Controls.Add(new LiteralControl("<hr />"));
                rowDueTotalDivider.Controls.Add(cellDueTotalDivider);

                tblSummary.Rows.Add(rowDueTotalDivider);

                // Due Total Row
                TableRow rowDueTotal = new TableRow();

                TableCell cellDueTotalCaption = new TableCell();
                cellDueTotalCaption.Attributes.Add("align", "right");
                cellDueTotalCaption.Width = Unit.Percentage(80);
                rowDueTotal.Cells.Add(cellDueTotalCaption);

                TableCell cellDueTotalValue = new TableCell();
                cellDueTotalValue.Attributes.Add("align", "right");
                cellDueTotalValue.Attributes.Add("valign", "middle");
                cellDueTotalValue.Width = Unit.Percentage(20);
                cellDueTotalValue.Controls.Add(new LiteralControl(string.Format("<nobr>{0}</nobr>", dueTotal.ToCustomerCurrency())));
                rowDueTotal.Cells.Add(cellDueTotalValue);

                tblSummary.Rows.Add(rowDueTotal);


                // Coupon Row
                TableRow rowCoupon = new TableRow();

                TableCell cellCouponCaption = new TableCell();
                cellCouponCaption.Attributes.Add("align", "right");
                cellCouponCaption.Width = Unit.Percentage(80);
                cellCouponCaption.Controls.Add(new LiteralControl(string.Format("{0}&#160;", discountCaption)));
                rowCoupon.Cells.Add(cellCouponCaption);

                TableCell cellCouponValue = new TableCell();
                cellCouponValue.Attributes.Add("align", "right");
                cellCouponValue.Attributes.Add("valign", "middle");
                cellCouponCaption.Width = Unit.Percentage(20);
                cellCouponValue.Controls.Add(new LiteralControl(string.Format("<nobr>{0}</nobr>", discount.ToCustomerCurrency())));
                rowCoupon.Cells.Add(cellCouponValue);

                tblSummary.Rows.Add(rowCoupon);
            }

            // Divider Row
            TableRow rowTotalDivider = new TableRow();

            // filler table cell
            rowTotalDivider.Cells.Add(new TableCell());

            TableCell cellTotalDivider = new TableCell();
            cellTotalDivider.Controls.Add(new LiteralControl("<hr />"));
            rowTotalDivider.Cells.Add(cellTotalDivider);

            tblSummary.Rows.Add(rowTotalDivider);

            // Total/Balance Row
            TableRow rowTotal = new TableRow();

            TableCell cellTotalCaption = new TableCell();
            cellTotalCaption.Attributes.Add("align", "right");
            cellTotalCaption.Width = Unit.Percentage(80);
            cellTotalCaption.Controls.Add(new LiteralControl(string.Format("{0}&#160;", totalCaption)));
            rowTotal.Cells.Add(cellTotalCaption);

            TableCell cellTotalValue = new TableCell();
            cellTotalValue.Attributes.Add("align", "right");
            cellTotalValue.Attributes.Add("valign", "middle");
            cellTotalValue.Width = Unit.Percentage(20);
            cellTotalValue.Controls.Add(new LiteralControl(string.Format("<nobr><span id='total_elem'>{0}</span></nobr>", balance.ToCustomerCurrency())));
            rowTotal.Cells.Add(cellTotalValue);

            tblSummary.Rows.Add(rowTotal);

            divSummary.Controls.Add(tblSummary);

            divSummary.RenderControl(writer);
        }
    }
}



