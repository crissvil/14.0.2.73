// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Interprise.Business.Customer;
using Interprise.Business.Base.Supplier;
using Interprise.Business.Base.CRM;
using Interprise.Framework.Base.Shared;
using Interprise.Licensing.Base.Services;
using Interprise.Facade.Customer;
using Interprise.Facade.Base.Supplier;
using Interprise.Facade.Base.CRM;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration
{
    public class Affiliate
    {
        #region Variable Declaration

        private string _affiliateId;
        private string _contactCode;
        private string _firstName;
        private string _lastName;
        #endregion

        #region Constructor

        private Affiliate() {}
        
        #endregion

        #region Properties

        public string AffiliateID
        {
            get { return _affiliateId; }
        }

        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }

        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }

        public string ContactCode
        {
            get { return _contactCode; }
            set { _contactCode = value; }
        }

        public string FullName
        {
            get { return _firstName + " " + _lastName; }
        }
        
        #endregion        

        #region Methods

        public static bool IsValid(string id)
        {
            bool validAffiliate = false;

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, string.Format("SELECT SalesRepGroupCode FROM CustomerSalesRep with (NOLOCK) WHERE SalesRepGroupCode = {0}", DB.SQuote(id))))
                {
                    validAffiliate = reader.Read() &&
                                       !CommonLogic.IsStringNullOrEmpty(DB.RSField(reader, "SalesRepGroupCode"));
                }
            }

            return validAffiliate;
        }

        #region Find

        public static Affiliate FindById(string id)
        {
            return Find(id, string.Empty);
        }

       
        private static Affiliate Find(string id, string email)
        {

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, string.Format("exec eCommerceGetAffiliate @AffiliateID = {0}, @Email = {1}", CommonLogic.IIF(CommonLogic.IsStringNullOrEmpty(id),"null",DB.SQuote(id)), CommonLogic.IIF(CommonLogic.IsStringNullOrEmpty(email),"null",DB.SQuote(email)))))
                {
                    if (reader.Read())
                    {
                        Affiliate affiliate = new Affiliate();
                        affiliate._affiliateId = DB.RSField(reader, "SalesRepGroupCode");
                        affiliate._firstName = DB.RSField(reader, "ContactFirstName");
                        affiliate._lastName = DB.RSField(reader, "ContactLastName");
                        affiliate._contactCode = DB.RSField(reader, "ContactCode");
                        return affiliate;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        #endregion
        #endregion
    }
}



