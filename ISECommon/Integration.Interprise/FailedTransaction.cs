// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration
{
    public class FailedTransaction
    {
        #region Variable Declaration

        private string _customerCode = string.Empty;
        private string _salesOrderCode = string.Empty;
        private string _paymentGateway = string.Empty;
        private string _paymentMethod = string.Empty;
        private string _transactionCommand = string.Empty;
        private string _transactionResult = string.Empty;
        private string _ipAddress = string.Empty;
        private string _maxMindDetails = string.Empty;
        private decimal _maxMindFraudScore = -1.0M;
        private bool _isFailed = true;

        public const string NOT_APPLICABLE = "N/A";

        #endregion

        public FailedTransaction() { }

        public static FailedTransaction New()
        {
            return new FailedTransaction();
        }

        public string CustomerCode
        {
            get { return _customerCode; }
            set { _customerCode = value; }
        }

        public string SalesOrderCode
        {
            get { return _salesOrderCode; }
            set { _salesOrderCode = value; }
        }

        public string PaymentGateway
        {
            get { return _paymentGateway; }
            set { _paymentGateway = value; }
        }

        public string PaymentMethod
        {
            get { return _paymentMethod; }
            set { _paymentMethod = value; }
        }

        public string TransactionCommand
        {
            get { return _transactionCommand; }
            set { _transactionCommand = value; }
        }

        public string TransactionResult
        {
            get { return _transactionResult; }
            set { _transactionResult = value; }
        }

        public string IPAddress
        {
            get { return _ipAddress; }
            set { _ipAddress = value; }
        }

        public string MaxMindDetails
        {
            get { return _maxMindDetails; }
            set { _maxMindDetails = value; }
        }

        public decimal MaxMindFraudScore
        {
            get { return _maxMindFraudScore; }
            set { _maxMindFraudScore = value; }
        }

        public bool IsFailed
        {
            get { return _isFailed; }
            set { _isFailed = value; }
        }

        public void Record()
        {
            using (SqlConnection con = new SqlConnection(DB.GetDBConn()))
            {
                using (SqlCommand cmdSaveFailedTransaction = new SqlCommand("EcommerceSaveFailedTransaction", con))
                {
                    cmdSaveFailedTransaction.CommandType = CommandType.StoredProcedure;

                    cmdSaveFailedTransaction.Parameters.Add("@CustomerCode", SqlDbType.NVarChar, 30).Value = this.CustomerCode;
                    cmdSaveFailedTransaction.Parameters.Add("@SalesOrderCode", SqlDbType.NVarChar, 30).Value = this.SalesOrderCode;
                    cmdSaveFailedTransaction.Parameters.Add("@PaymentGateway", SqlDbType.NVarChar, 50).Value = this.PaymentGateway;
                    cmdSaveFailedTransaction.Parameters.Add("@PaymentMethod", SqlDbType.NVarChar, 50).Value = this.PaymentMethod;
                    cmdSaveFailedTransaction.Parameters.Add("@TransactionCommand", SqlDbType.NText).Value = this.TransactionCommand;
                    cmdSaveFailedTransaction.Parameters.Add("@TransactionResult", SqlDbType.NText).Value = this.TransactionResult;
                    cmdSaveFailedTransaction.Parameters.Add("@MaxMindDetails", SqlDbType.NText).Value = this.MaxMindDetails;
                    cmdSaveFailedTransaction.Parameters.Add("@MaxMindFraudScore", SqlDbType.Decimal).Value = this.MaxMindFraudScore;
                    cmdSaveFailedTransaction.Parameters.Add("@IPAddress", SqlDbType.NVarChar, 25).Value = this.IPAddress;
                    cmdSaveFailedTransaction.Parameters.Add("@IsFailed", SqlDbType.Bit).Value = this.IsFailed;

                    con.Open();
                    cmdSaveFailedTransaction.ExecuteNonQuery();
                }
            }
        }
    }
}



