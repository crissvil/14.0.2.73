// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Data;
using System.Runtime.InteropServices;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Web
{
    public class CachedMimeLookUp
    {
        #region Variable Declaration
        private static CachedMimeLookUp _instance = null;

        private Dictionary<string, string> _extMimeMap = new Dictionary<string, string>();
        #endregion

        private CachedMimeLookUp() 
        {
             MimeHelper.FillExtensionMimeMaps(_extMimeMap);
        }

        #region Properties

        #region Instance
        /// <summary>
        /// 
        /// </summary>
        public static CachedMimeLookUp Instance
        {
            get
            {
                if (null == _instance)
                {
                    _instance = new CachedMimeLookUp();
                }

                return _instance;
            }
        }
        #endregion

        #endregion


        #region Methods

        #region GetMimeType
        /// <summary>
        /// 
        /// </summary>
        /// <param name="extension"></param>
        /// <returns></returns>
        public string GetMimeType(string extension)
        {
            string mimeType = MimeHelper.UNKNOWN_MIME_TYPE;

            if (_extMimeMap.ContainsKey(extension))
            {
                mimeType = _extMimeMap[extension];
            }

            return mimeType;
        }
        #endregion

        #endregion

    }
}
