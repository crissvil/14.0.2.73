// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Web;
using System.Data;
using System.Runtime.InteropServices;
using System.Data.SqlClient;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Web
{
    public static class MimeHelper
    {
        public const string UNKNOWN_MIME_TYPE = "application/octet-stream";

        public static Dictionary<string, string> GetExtensionMimeTypeMaps()
        {
            Dictionary<string, string> maps = new Dictionary<string, string>();
            FillExtensionMimeMaps(maps);

            return maps;
        }

        internal static void FillExtensionMimeMaps(Dictionary<string, string> maps)
        {
            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "SELECT Extension, MimeType FROM EcommerceMimeType with (NOLOCK)"))
                {
                    while (reader.Read())
                    {
                        maps.Add(DB.RSField(reader, "Extension"), DB.RSField(reader, "MimeType"));
                    }
                }
            }
        }

        public static string GetMimeTypeByExtension(string extension)
        {
            // default mime type
            string mimeType = UNKNOWN_MIME_TYPE;

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "SELECT MimeType FROM EcommerceMimeType with (NOLOCK) WHERE Extension = {0}", DB.SQuote(extension)))
                {
                    if (reader.Read())
                    {
                        mimeType = DB.RSField(reader, "MimeType");
                    }
                }
            }

            return mimeType;
        }
    }
}



