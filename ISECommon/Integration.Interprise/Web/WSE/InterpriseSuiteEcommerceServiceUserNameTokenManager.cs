// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using Microsoft.Web.Services3;
using Microsoft.Web.Services3.Design;
using Microsoft.Web.Services3.Security;
using Microsoft.Web.Services3.Security.Tokens;
using InterpriseSuiteEcommerceCommon;
using System.Reflection;
using System.Data.SqlClient;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Web.WSE
{
    /// <summary>
    /// Summary description for InterpriseSuiteEcommerceUserNameTokenManager
    /// </summary>
    public class InterpriseSuiteEcommerceServiceUserNameTokenManager : UsernameTokenManager
    {
        public InterpriseSuiteEcommerceServiceUserNameTokenManager()
        {
            
        }

        public InterpriseSuiteEcommerceServiceUserNameTokenManager(XmlNodeList configData)
            : base(configData)
        {
            
        }

        protected override string AuthenticateToken(UsernameToken token)
        {
            if (string.Empty == token.Username)
            {
                return string.Empty;
            }

            string authentication = string.Empty;
            bool hasUserRecord = false;


            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "SELECT UserPassword FROM SystemUserAccount with (NOLOCK) WHERE UserCode = {0}", DB.SQuote(token.Username)))
                {
                    hasUserRecord = reader.Read();

                    if (hasUserRecord)
                    {
                        authentication = DB.RSField(reader, "UserPassword");
                    }
                }
            }

            return authentication;
        }
    }
}
