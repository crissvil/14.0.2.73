// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Web
{
    public class RenderHeaderEventArgs : EventArgs
    {
        private List<string> _includes = new List<string>();

        public RenderHeaderEventArgs()
        {
        }

        public List<string> Includes
        {
            get { return _includes; }
        }
    }
}
