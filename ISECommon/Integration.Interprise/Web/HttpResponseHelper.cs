// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration.Web
{
    public static class HttpResponseHelper
    {
        public static void RespondWithFileNotFound(HttpResponse response)
        {
            response.Write("<html><body><div align=\"center\"><b>File not found!</b></div></body></html>");
            response.Flush();
            response.End();
        }

        public static void RespondWithForbidden(HttpResponse response)
        {
            response.Write("<html><body><div align=\"center\"><b>You have no permission to view this content!</b></div></body></html>");
            response.Flush();
            response.End();
        }
    }
}
