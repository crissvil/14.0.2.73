// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.DataAccess;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration
{
    public class ShoppingCartPageLiteralRenderer : IShoppingCartHTMLLiteralRenderer
    {
        #region IShoppingCartHTMLLiteralRenderer Members

        public void Render(InterpriseShoppingCart cart, ref StringBuilder output)
        {
            if (cart.IsEmpty())
            {
                output.Append("<br><br>");
                if (cart.CartType == CartTypeEnum.ShoppingCart)
                {
                    var t1 = new Topic("EmptyCartText", cart.ThisCustomer.LocaleSetting, cart.SkinID, null);
                    output.Append(t1.Contents);
                }
                if (cart.CartType == CartTypeEnum.WishCart)
                {
                    var t1 = new Topic("EmptyWishListText", cart.ThisCustomer.LocaleSetting, cart.SkinID, null);
                    output.Append(t1.Contents);
                }
                output.Append("<br><br><br>");
            }
            else
            {
                bool showPicsInCart = AppLogic.AppConfigBool("ShowPicsInCart");
                bool showLinkBack = AppLogic.AppConfigBool("LinkToProductPageInCart");
                bool hideUnitMeasure = AppLogic.AppConfigBool("HideUnitMeasure");
                bool showShipDateInCart = AppLogic.AppConfigBool("ShowShipDateInCart");
                bool showStockHints = AppLogic.AppConfigBool("ShowStockHints");
                string msgforavailability = AppLogic.GetString("shoppingcart.cs.47");

                output.Append("<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\"  >");
                output.Append("<tr>");
                if (showPicsInCart)
                {
                    output.Append("<td align=\"center\" valign=\"middle\"><b>");
                    output.Append(AppLogic.GetString("shoppingcart.cs.1"));
                    output.Append("</b></td>");
                    output.Append("<td align=\"center\" valign=\"middle\">&nbsp;</td>");
                }
                else
                {
                    output.Append("<td align=\"left\" valign=\"middle\">");
                    output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.cs.1"));
                    output.Append("</td>");
                }

                if (showStockHints)
                {
                    if (showShipDateInCart)
                    {
                        // Shipping Dates Header...
                        output.Append("<td align=\"center\" valign=\"middle\">");
                        output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.aspx.17"));
                        output.Append("</td>");
                    }
                }

                if (!hideUnitMeasure)
                {
                    // Unit Measure Column Header
                    output.Append("<td align=\"center\" valign=\"middle\">");
                    output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.cs.37"));
                    output.Append("</td>");
                }

                // Quantity Column Header...
                output.Append("<td align=\"center\" valign=\"middle\">");
                output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.cs.2"));
                output.Append("</td>");

                // Sub Total Column Header...
                output.Append("<td align=\"right\" valign=\"middle\">");
                output.AppendFormat("<b>{0}</b>", AppLogic.GetString("shoppingcart.cs.27"));
                output.Append("</td>");
                output.Append("</tr>");

                string languageCode = AppLogic.GetLanguageCode(cart.ThisCustomer.LocaleSetting);
                var lstImageAttributes = AppLogic.GetSEImageAttributeByItemCodes(cart.CartItems.Select(c => DB.SQuote(c.ItemCode)).ToArray(), "ICON", languageCode);

                foreach(var cartItem in cart.CartItems)
                {
                    string itemCode = cartItem.ItemCode;
                    string seName = cartItem.DisplayName;
                    int productid = cartItem.ItemCounter;

                    // get the associated cart item for this line item...                    
                    output.Append("<tr>");
                    output.AppendFormat("<td colspan=\"{0}\">", CommonLogic.IIF(showPicsInCart, 6, CommonLogic.IIF(showShipDateInCart, 5, 4)));
                    output.Append("<hr style=\"height: 1px; width:100%; color: #DDDDDD;\" />");
                    output.Append("</td>");
                    output.Append("</tr>");
                    output.Append("<tr>");

                    if (showPicsInCart)
                    {
                        // PICTURE COL:
                        output.Append("<td align=\"center\" valign=\"top\">");                        
                        if (showLinkBack)
                        {
                            output.AppendFormat("<a href=\"{0}\" >", SE.MakeProductLink(productid.ToString(), seName));
                        }

                        var img = ProductImage.Locate("product", itemCode, "icon");
                        if (null != img)
                        {
                            var att = lstImageAttributes.FirstOrDefault(i => i.ItemCode == itemCode);
                            string title = string.Empty;
                            string alt = string.Empty;
                            if (att != null)
                            {
                                title = att.Title;
                                alt = att.Alt;
                            }
                            output.AppendFormat("<img src=\"{0}\" border=\"0\" title=\"{1}\" alt=\"{2}\" >", img.src, title, alt);
                        }
                    
                        if (showLinkBack)
                        {
                            output.Append("</a>");
                        }
                        output.Append("</td>");
                    }

                    /*********************************
                     * Line Item
                     * *******************************/
                    output.Append("<td align=\"left\" valign=\"top\">");

                    
                    switch(cartItem.ItemType)
                    {  
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP:
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM:
                            if(showLinkBack)
                            {

                                using (var con = DB.NewSqlConnection())
                                {
                                    con.Open();
                                    using (var reader = DB.GetRSFormat(con, String.Format("SELECT ItemCode FROM InventoryMatrixItem with (NOLOCK) WHERE MatrixItemCode = {0}", DB.SQuote(cartItem.ItemCode))))
                                    {
                                        if (reader.Read())
                                        {
                                            output.AppendFormat(
                                                "<a href=\"{0}\"><b>{1}</b></a><br>",
                                                InterpriseSuiteEcommerceCommon.InterpriseHelper.MakeItemLink(DB.RSField(reader, "ItemCode")),
                                                Security.HtmlEncode(cartItem.DisplayName)
                                            );
                                        }
                                    }
                                }
                            }
                            else
                            {
                                output.AppendFormat("<b>{0}</b><br>", itemCode);
                            }
                            // display the details

                            using (var con = DB.NewSqlConnection())
                            {
                                con.Open();
                                using (var reader = DB.GetRSFormat(con, String.Format("exec eCommerceGetMatrixItemAttributes @ItemCode = NULL, @MatrixItemCode = {0}, @WebsiteCode = {1}, @CurrentDate = {2}, @LanguageCode = {3}, @ContactCode = {4}", 
                                                                                DB.SQuote(itemCode), DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode),
                                                                                DateTime.Now.ToDateTimeStringForDB().ToDbQuote(), DB.SQuote(Customer.Current.LanguageCode), DB.SQuote(Customer.Current.ContactCode))))
                                {
                                    if (reader.Read())
                                    {
                                        for (int ctr = 1; ctr <= 6; ctr++)
                                        {
                                            string attribute = DB.RSField(reader, string.Format("Attribute{0}", ctr));
                                            string attributeValue = DB.RSField(reader, string.Format("Attribute{0}ValueDescription", ctr));

                                            if (string.IsNullOrEmpty(attribute) && string.IsNullOrEmpty(attributeValue)) continue;

                                            output.AppendFormat("&nbsp;&nbsp;{0}:{1}<br />", attribute.ToHtmlEncode(), attributeValue.ToHtmlEncode());
                                        }
                                    }
                                }
                            }

                            RenderRegistryChecking(cartItem, ref output, cart.SkinID, cart.ThisCustomer.LocaleSetting);

                            output.Append("<br />");

                            if (cartItem.Status == "P" && cartItem.IsOutOfStock)
                            {
                                output.AppendFormat("<img src=\"skins/Skin_{0}/images/outofstock.gif\">", cart.SkinID);
                            }
                            break;

                            
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT:
                            output.Append("<div style=\"margin-left: 10px;\">");
                            if(showLinkBack)
                            {
                                output.AppendFormat("<a href=\"{0}\"><b>{1}</b></a><br>", SE.MakeProductLink(productid.ToString(), seName), Security.HtmlEncode(seName));
                            }
                            else
                            {
                                output.AppendFormat("<b>{0}</b><br>",cartItem.ItemCode);
                            }

                            //remove the edit button for registry item
                            if (!cartItem.GiftRegistryID.HasValue)
                            {
                                output.AppendFormat(
                                "&nbsp;&nbsp;<a href=\"{0}\"><img src=\"skins/Skin_{1}/images/edit.gif\" align=\"absmiddle\" border=\"0\" alt=\"{2}\"></a>&nbsp;",
                                SE.MakeProductLink(productid.ToString(), itemCode) + string.Format("?kcid={0}", cartItem.Id.ToString()),
                                cart.SkinID, AppLogic.GetString("shoppingcart.cs.4"));
                            }

                            output.Append("<br />");

                            bool isCurrencyIncludedForInventorySelling = InterpriseHelper.IsCurrencyIncludedForInventorySelling(cart.ThisCustomer.CurrencyCode);

                            // Special case 2 
                            //  Currency is added in Inventory Selling Currency late
                            //  after the kit has been created, ideally the kit should regenerate kit pricing for this currency
                            //  but for the meantime, we should handle this by looking into the home currency
                            bool currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem =
                            InterpriseHelper.CurrencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYet(cart.ThisCustomer.CurrencyCode, itemCode);

                            string kitDetailQuery =
                                string.Format(
                                    "exec GetEcommerceKitCartDetail @ItemKitCode = {0}, @CurrencyCode = {1}, @LanguageCode = {2}, @CustomerCode = {3}, @CartID = {4}, @ContactCode = {5}", 
                                    cartItem.ItemCode.ToDbQuote(),
                                    (isCurrencyIncludedForInventorySelling && !currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem ) ? cart.ThisCustomer.CurrencyCode.ToDbQuote() : Currency.GetHomeCurrency().ToDbQuote(), //cart.ThisCustomer.CurrencyCode, 
                                    cart.ThisCustomer.LocaleSetting.ToDbQuote(),
                                    cart.ThisCustomer.CustomerCode.ToDbQuote(),
                                    cartItem.Id.ToString().ToDbQuote(),
                                    cart.ThisCustomer.ContactCode.ToDbQuote()
                                );

                            // read the records from the EcommerceKitCart table...

                            using (var con = DB.NewSqlConnection())
                            {
                                
                                con.Open();
                                using (var reader = DB.GetRSFormat(con, kitDetailQuery))
                                {
                                    while (reader.Read())
                                    {
                                        string productDisplayName = DB.RSField(reader, "ItemDescription");
                                        decimal quantity = Convert.ToDecimal(DB.RSFieldDecimal(reader, "Quantity"));
                                        
                                        output.AppendFormat("&nbsp;&nbsp;-&nbsp;({0}) {1}", Localization.ParseLocaleDecimal(quantity, cart.ThisCustomer.LocaleSetting), productDisplayName.ToHtmlEncode());
                                        output.Append("<br />");
                                    }
                                }
                            }

                            RenderRegistryChecking(cartItem, ref output, cart.SkinID, cart.ThisCustomer.LocaleSetting);

                            output.Append("<br />");

                            output.Append("</div>");

                            if (cart.HasNoStockPhasedOutItem)
                            {
                                output.AppendFormat("<img src=\"skins/Skin_{0}/images/outofstock.gif\">", cart.SkinID);
                            }
                            break;
                        default:
                            if(showLinkBack)
                            {
                                if (!cartItem.IsCheckoutOption)
                                {
                                    output.AppendFormat("<a href=\"{0}\"><b>{1}</b></a><br>", SE.MakeProductLink(productid.ToString(), seName), seName.ToHtmlEncode());
                                }
                                else
                                {
                                    //checkout options should only display a popup and not go to a product page
                                    output.AppendFormat(
                                        "<a onclick=\"popuporderoptionwh('Order Option {0}',{1},650,550,'yes');\" href=\"javascript:void(0);\"><b>{0}</b></a><br>", 
                                            seName.ToHtmlEncode(), productid.ToString()
                                    );
                                }
                            }
                            else
                            {
                                output.AppendFormat("<b>{0}</b><br>", Security.HtmlEncode(seName));
                            }

                            RenderRegistryChecking(cartItem, ref output, cart.SkinID, cart.ThisCustomer.LocaleSetting);
                            
                            output.Append("<br />");

                            if (cartItem.Status == "P" && cartItem.IsOutOfStock)
                            {
                                output.AppendFormat("<img src=\"skins/Skin_{0}/images/outofstock.gif\">", cart.SkinID);
                            }
                            break;
                    }
                    /*************** multiple address **********************/
                    if (cart.HasMultipleShippingAddresses() && !cartItem.IsDownload)
                    {
                        output.Append(AppLogic.GetString("shoppingcart.cs.24"));
                        output.Append(" ");

                        var adr = new Address();
                        bool includename = true;

                        if (cartItem.m_ShippingAddressID == cart.ThisCustomer.PrimaryShippingAddressID && !cartItem.GiftRegistryID.HasValue)
                        {
                            adr.LoadByCustomer(cart.ThisCustomer, AddressTypes.Shipping, cartItem.m_ShippingAddressID, cartItem.GiftRegistryID);
                            output.Append(AppLogic.GetString("account.aspx.10"));
                        }
                        else if (cartItem.m_ShippingAddressID == cart.ThisCustomer.PrimaryBillingAddressID)
                        {
                            adr.LoadByCustomer(cart.ThisCustomer, AddressTypes.Billing, cartItem.m_ShippingAddressID, cartItem.GiftRegistryID);
                            output.Append(AppLogic.GetString("account.aspx.8"));
                        }
                        else
                        {
                            adr.LoadByCustomer(cart.ThisCustomer, AddressTypes.Shipping, cartItem.m_ShippingAddressID, cartItem.GiftRegistryID);
                            output.Append(adr.Name);
                            includename = false;
                        }
                        output.Append("<div style=\"margin-left: 10px;\">");
                        output.Append(adr.DisplayString(false, false, includename, "<br>"));
                        output.Append("</div>");

                        output.Append("<div>");
                        output.Append(AppLogic.GetString("order.cs.23"));
                        output.Append(cartItem.m_ShippingMethod);
                        output.Append("</div>");
                    }
                    /*******************************************************/
                    output.Append("</td>");

                    if (showStockHints)
                    {
                        if (showShipDateInCart)
                        {
                            //Exclude the ff. item types for stock reservation
                            if (cartItem.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_NON_STOCK || cartItem.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_SERVICE ||
                                cartItem.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ELECTRONIC_DOWNLOAD || cartItem.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                            {
                                output.Append("<td align=\"left\" valign=\"top\"></td>");
                            }
                            else
                            {
                                //Allocation and Reservation data
                                var reserveCol = cart.GetReservation(cartItem.ItemCode);

                                if (cartItem.Status == "P" && cartItem.IsOutOfStock)
                                {
                                    output.AppendFormat("<td align=\"center\" valign=\"top\"><i>{0}</i>", " ");
                                }
                                else
                                {
                                    if (cartItem.m_AllocatedQty > 0 && reserveCol.Count == 0)
                                    {
                                        output.AppendFormat("<td align=\"center\" valign=\"top\"><i>{0} can be shipped immediately</i>", Localization.ParseLocaleDecimal(cartItem.m_AllocatedQty, cart.ThisCustomer.LocaleSetting));
                                    }
                                    else if (cartItem.m_AllocatedQty > 0 && reserveCol.Count > 0)
                                    {
                                        output.AppendFormat("<td align=\"center\" valign=\"top\"><i>{0} can be shipped immediately</i>", Localization.ParseLocaleDecimal(cartItem.m_AllocatedQty, cart.ThisCustomer.LocaleSetting));
                                        //output.AppendFormat("<td align=\"center\" valign=\"top\"><i>{0} {1}</i>", Localization.ParseLocaleDecimal(cartItem.m_AllocatedQty, cart.ThisCustomer.LocaleSetting), msgforavailability);'
                                    }
                                    else
                                    {
                                        output.AppendFormat("<td align=\"center\" valign=\"top\"><i>{0}</i>", msgforavailability);
                                    }
                                }

                                for (int resCtr = 0; resCtr <= reserveCol.Count - 1; resCtr++)
                                {
                                    var reserved = reserveCol[resCtr];
                                    if (cartItem.ItemCode != reserved.ItemCode) { continue; }

                                    if (resCtr == 0 && cartItem.m_AllocatedQty == 0)
                                    {
                                        output.AppendFormat("<td align=\"center\" valign=\"top\"><i>{0} can be shipped on {1}</i>", Localization.ParseLocaleDecimal(reserved.QtyReserved, cart.ThisCustomer.LocaleSetting), reserved.ShipDate.ToShortDateString());
                                    }
                                    else
                                    {
                                        output.Append("<br/><i>" + Localization.ParseLocaleDecimal(reserved.QtyReserved, cart.ThisCustomer.LocaleSetting) + " can be shipped on " + reserved.ShipDate.ToShortDateString() + "</i>");
                                    }
                                }

                                //no allocation and reservation found!
                                if (cartItem.m_AllocatedQty == 0 && reserveCol.Count == 0)
                                {
                                    output.Append("<td align=\"left\" valign=\"top\"></td>");
                                }
                            }
                        }
                    }
                    output.Append("</td>");

                    if (!hideUnitMeasure)
                    {
                        // Unit Measure Column ********************************
                        output.Append("<td align=\"center\" valign=\"top\">");

                        // show the unit measure
                        var availableUnitMeasures = new List<KeyValuePair<string, string>>();
                        using (var con = DB.NewSqlConnection())
                        {
                            con.Open();
                            using (var reader = DB.GetRSFormat(con, "exec eCommerceGetProductUnitMeasureAvailability @CustomerCode = {0}, @ItemCode = {1}, @IncludeAllWarehouses = {2}, @Anon = {3}", DB.SQuote(cart.ThisCustomer.CustomerCode), DB.SQuote(itemCode), AppLogic.AppConfigBool("ShowInventoryFromAllWarehouses"), cart.ThisCustomer.IsNotRegistered))
                            {
                                while (reader.Read())
                                {
                                    availableUnitMeasures.Add(new KeyValuePair<string, string>(DB.RSField(reader, "UnitMeasureCode"), DB.RSField(reader, "UnitMeasureDescription")));
                                }
                            }
                        }

                        // NOTE :
                        //  We should be guaranteed to have at least 1 unit measure
                        //  and if it's 1 it should be the default...
                        //  so we can safely assume that the first index is the Default unit measure

                        if (availableUnitMeasures.Count > 1)
                        {
                            // render as combobox
                            output.AppendFormat("<select name=\"UnitMeasureCode_{0}\" id=\"UnitMeasureCode_{0}\" size=\"1\">", cartItem.m_ShoppingCartRecordID);
                            output.AppendLine();

                            foreach (var unitMeasureValue in availableUnitMeasures)
                            {
                                bool isCurrentUnitMeasureTheOneSelectedInProductPage = cartItem.UnitMeasureCode.Equals(unitMeasureValue.Key, StringComparison.InvariantCultureIgnoreCase);

                                output.AppendFormat("<option value=\"{0}\" {2}>{1}</option>",
                                    Security.HtmlEncode(unitMeasureValue.Key),
                                    Security.HtmlEncode(unitMeasureValue.Value),
                                    CommonLogic.IIF((isCurrentUnitMeasureTheOneSelectedInProductPage), "selected=\"selected\"", string.Empty));
                            }

                            output.Append("</select>");
                            output.AppendLine();
                        }
                        else
                        {
                            // register the unit measure control
                            output.AppendFormat("<input type=\"hidden\" id=\"UnitMeasureCode_{0}\" name=\"UnitMeasureCode_{0}\" value=\"{1}\" />", cartItem.m_ShoppingCartRecordID, Security.HtmlEncode(cartItem.UnitMeasureCode));
                            output.AppendLine();
                            // render as span
                            output.AppendFormat("<span >{0}</span>", Security.HtmlEncode(availableUnitMeasures[0].Value));
                            output.AppendLine();
                        }

                        output.Append("</td>");
                        // End Unit Measure Column...
                    }

                    // Quantity and Delete
                    output.Append("<td align=\"center\" valign=\"top\" class=\"cart-quantity-expander\">");

                    if (cartItem.RestrictedQuantities.Count > 0)
                    {
                        output.AppendFormat("<select name=\"Quantity_{0}\" id=\"Quantity_{0}\" size=\"1\" >&nbsp;", cartItem.m_ShoppingCartRecordID);
                        if (AppLogic.AppConfigBool("ShowCartDeleteItemButton"))
                        {
                            output.Append("<option value=\"0\">DELETE</option>");
                        }

                        foreach (decimal restrictedQuantity in cartItem.RestrictedQuantities)
                        {
                            output.AppendFormat(
                                "<option value=\"{0}\" {1}>{0}</option>",
                                restrictedQuantity,
                                (restrictedQuantity == cartItem.m_Quantity) ? "selected=\"selected\"" : string.Empty
                            );
                        }
                        output.Append("</select>");
                    }
                    else
                    {
                        output.AppendFormat("<input type=\"text\" name=\"Quantity_{0}\" id=\"Quantity_{0}\" size=\"4\" value=\"{1}\" maxlength=\"14\">&nbsp;", cartItem.m_ShoppingCartRecordID, Localization.ParseLocaleDecimal(cartItem.m_Quantity, cart.ThisCustomer.LocaleSetting));

                        string deleteCaption = AppLogic.GetString("shoppingcart.cs.31", true);
                        deleteCaption = string.IsNullOrEmpty(deleteCaption) ? "Delete" : deleteCaption;
                        if (AppLogic.AppConfigBool("ShowCartDeleteItemButton"))
                        {
                            output.AppendFormat("<input type=\"submit\" name=\"bt_Delete\" class=\"site-button cart-delete-custom\" value=\"{0}\" onclick=\"this.form.Quantity_{1}.value='0';\">", deleteCaption, cartItem.m_ShoppingCartRecordID);
                        }
                    }
                    output.AppendFormat("<input type=\"hidden\" name=\"MinOrderQuantity_{0}\" id=\"MinOrderQuantity_{0}\" value=\"{1}\" >\n", cartItem.m_ShoppingCartRecordID, cartItem.m_MinimumQuantity);

                    output.Append("</td>");

                    // Sub-Total(Price) Column
                    string priceFormatted = cartItem.Price.ToCustomerCurrency();

                    string vatDisplayIfIncluded = string.Empty;
                    if (AppLogic.AppConfigBool("VAT.Enabled"))
                    {
                        if (cart.ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                        {
                            vatDisplayIfIncluded = string.Format("<span class=\"VATLabel\">{0}</span>", AppLogic.GetString("showproduct.aspx.38"));
                        }
                        else
                        {
                            vatDisplayIfIncluded = string.Format("<span class=\"VATLabel\">{0}</span>", AppLogic.GetString("showproduct.aspx.37"));
                        }

                        vatDisplayIfIncluded += "<br />";
                        vatDisplayIfIncluded += String.Format("<span class=\"VATAmount\">{0} {1}</span>", AppLogic.GetString("showproduct.aspx.41"), cartItem.TaxRate.ToCustomerCurrency());
                    }

                    output.AppendFormat("<td align=\"right\" valign=\"top\">{0} {1}</td>", priceFormatted, vatDisplayIfIncluded);
                }
               
                output.Append("</tr>\n");
                output.Append("</table>\n");
            }
        }

        public void RenderRegistryChecking(CartItem cartItem, ref StringBuilder output, int skinId, string locale)
        {
            if (!cartItem.RegistryItemCode.HasValue && !cartItem.GiftRegistryID.HasValue) return;

            decimal? regItemQty = cartItem.RegistryItemQuantity;
            
            output.Append("<br />");
            output.Append("<span class='registry-notification'>" + AppLogic.GetString("giftregistry.aspx.10") + "</span>");
            output.Append("<br />");
            output.Append("<span class='registry-notification'>" + AppLogic.GetString("giftregistry.aspx.12") + ": " + Localization.FormatDecimal2Places(regItemQty.Value) + "</span>");

            if (regItemQty.Value == 0)
            {
                output.Append("<span class='errorLg'>" + AppLogic.GetString("editgiftregistry.error.16") + "</span>");
            }

            if (cartItem.HasRegistryItemQuantityConflict())
            {
                output.Append("<br />");
                output.Append("<span class='errorLg'>" + AppLogic.GetString("editgiftregistry.error.17") + "</span>");
            }

            output.Append("<br />");

        }

        #endregion
    }
}









