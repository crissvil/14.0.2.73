﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Xml.Linq;
using System.Linq;
using System.Collections.Generic;
using Interprise.Framework.Customer.DatasetGateway;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Tool;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration
{
    public class MobileCheckOutShippingPageLiteralRenderer : IShoppingCartHTMLLiteralRenderer
    {
        public void Render(InterpriseShoppingCart cart, ref StringBuilder output)
        {
            bool showPicsInCart = AppLogic.AppConfigBool("ShowPicsInCart");
            bool showLinkBack = AppLogic.AppConfigBool("LinkToProductPageInCart");
            bool hideUnitMeasure = AppLogic.AppConfigBool("HideUnitMeasure");
            bool showShipDateInCart = AppLogic.AppConfigBool("ShowShipDateInCart");
            bool showStockHints = AppLogic.AppConfigBool("ShowStockHints");
            bool vatEnabled = AppLogic.AppConfigBool("VAT.Enabled");
            bool vatInclusive = (cart.ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive);
            string currencyCode = cart.ThisCustomer.CurrencyCode;

            var root = new XElement(DomainConstants.XML_ROOT_NAME);
            root.Add(new XElement("VAT_INCLUSIVE", vatInclusive.ToStringLower()));
            root.Add(new XElement("VATENABLED", vatEnabled.ToStringLower()));
            root.Add(new XElement("SKINID", cart.SkinID));
            root.Add(new XElement("CLICK_HERE_SUB_TEXT", AppLogic.GetString("checkoutcard.aspx.1")));
            root.Add(new XElement("CLICK_HERE_TEXT", AppLogic.GetString("checkoutcard.aspx.2")));

            string couponCode = string.Empty;
            bool isCouponTypeOrders = false;
            decimal computedSubtotal = Decimal.Zero;
            decimal appliedCouponDiscount = Decimal.Zero;
            bool hasCoupon = cart.HasCoupon(ref couponCode);
            if (hasCoupon)
            {
                isCouponTypeOrders = cart.SalesOrderDataset.CustomerSalesOrderView[0]
                                   [cart.SalesOrderDataset.CustomerSalesOrderView.CouponTypeColumn.ColumnName].ToString().Equals("Orders");
                root.Add(new XElement("HASCOUPON", hasCoupon.ToStringLower()));
                root.Add(new XElement("COUPON_VALUE", couponCode.ToHtmlEncode()));
                root.Add(new XElement("IS_COUPON_TYPE_ORDERS", isCouponTypeOrders));
            }

            root.Add(new XElement("CHECKOUTSHIPPING_HEADER_TEXT", AppLogic.GetString("mobile.checkoutshipping.aspx.13")));
            foreach (CartItem cartItem in cart.CartItems)
            {
                var lineItemRow = cartItem.AssociatedLineItemRow;

                var xmlcartItem = new XElement("CART_ITEMS");
                root.Add(xmlcartItem);

                string producthref = InterpriseSuiteEcommerceCommon.InterpriseHelper.MakeItemLink((string)lineItemRow["ItemCode"]);
                string productName = !string.IsNullOrEmpty(lineItemRow["ItemDescription"].ToString()) ? lineItemRow["ItemDescription"].ToString().ToHtmlEncode() : lineItemRow["ItemName"].ToString().ToHtmlEncode();

                xmlcartItem.Add(new XElement("SHOWPICSINCART", showPicsInCart));
                xmlcartItem.Add(new XElement("SHOWSHIPDATEINCART", showShipDateInCart));
                xmlcartItem.Add(new XElement("SHOW_LINKBACK", showLinkBack));
                xmlcartItem.Add(new XElement("CART_ITEMCODE", lineItemRow["ItemCode"].ToString()));
                xmlcartItem.Add(new XElement("CART_ITEM_ID", lineItemRow["counter"].ToString()));
                xmlcartItem.Add(new XElement("PRODUCTLINKNAME", productName));

                if (showPicsInCart)
                {
                    var img = ProductImage.Locate("product", cartItem.ItemCounter, "icon");
                    if (null != img)
                    {
                        string seTitle = string.Empty;
                        string seAltText = string.Empty;
                        string itemCode = itemCode = InterpriseHelper.GetInventoryItemCode(cartItem.ItemCounter);
                        AppLogic.GetSEImageAttributes(itemCode, "ICON", AppLogic.GetLanguageCode(cart.ThisCustomer.LocaleSetting), ref seTitle, ref seAltText);

                        xmlcartItem.Add(new XElement("PRODUCTIMAGEPATH", img.src));
                        xmlcartItem.Add(new XElement("PRODUCTIMAGETITLE", seTitle));
                        xmlcartItem.Add(new XElement("PRODUCTIMAGEALT", seAltText));
                    }
                }

                xmlcartItem.Add(new XElement("INPUTQUANTITYVALUE", Localization.ParseLocaleDecimal(cartItem.m_Quantity, cart.ThisCustomer.LocaleSetting)));

                switch ((string)lineItemRow["ItemType"])
                {
                    case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP:

                    #region "ITEM_TYPE_MATRIX_ITEM"
                    case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM:

                        xmlcartItem.Add(new XElement("ITEM_TYPE", "ITEM_TYPE_MATRIX_ITEM"));
                        if (showLinkBack)
                        {
                            var matrixInfo = ServiceFactory.GetInstance<IProductService>()
                                                          .GetMatrixItemInfo(cartItem.ItemCode);
                            if (matrixInfo != null)
                            {
                                string productLinkHref = String.Empty;
                                productLinkHref = InterpriseHelper.MakeItemLink(matrixInfo.ItemCode);
                                productLinkHref = CommonLogic.QueryStringSetParam(productLinkHref, DomainConstants.QUERY_STRING_KEY_MATRIX_ID, matrixInfo.Counter.ToString());
                                xmlcartItem.Add(new XElement("PRODUCT_HREF", productLinkHref));
                            }
                        }

                        var matrixAttributes = AppLogic.GetMatrixItemAttributes(lineItemRow.ItemCode, Customer.Current.LanguageCode);
                        var matrixDetails = new StringBuilder();

                        matrixAttributes.ForEach(itemAttributes =>
                        {
                            matrixDetails.AppendFormat("&nbsp;&nbsp;{0}:{1}<br />", Security.HtmlEncode(itemAttributes.AttributeValue), Security.HtmlEncode(itemAttributes.AttributeValueDescription));
                        });

                        xmlcartItem.Add(new XElement("MATRIXDETAILS", matrixDetails.ToString()));

                        break;
                    #endregion

                    #region "ITEM_TYPE_KIT"
                    case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT:

                        xmlcartItem.Add(new XElement("ITEM_TYPE", "ITEM_TYPE_KIT"));
                        xmlcartItem.Add(new XElement("KIT_EDIT_IMAGE_ALT", cart.SkinID, AppLogic.GetString("shoppingcart.cs.4")));

                        string href = InterpriseSuiteEcommerceCommon.InterpriseHelper.MakeItemLink(cartItem.ItemCode) + "?kcid={0}".FormatWith(cartItem.Id.ToString());
                        xmlcartItem.Add(new XElement("KIT_EDIT_HREF", href));

                        xmlcartItem.Add(new XElement("PRODUCT_HREF", producthref));
                        xmlcartItem.Add(new XElement("KIT_HIDEDETAILSTEXT", AppLogic.GetString("mobile.wishlist.aspx.3")));

                        var kitDetailRelation = cart.SalesOrderDataset.GetRelation(
                                                        cart.SalesOrderDataset.CustomerSalesOrderDetailView,
                                                        cart.SalesOrderDataset.CustomerItemKitDetailView
                        );

                        var kitItems = new XElement("KIT_ITEMS");
                        var lineItemRows = lineItemRow.GetChildRows(kitDetailRelation);

                        lineItemRows.ForEach(kitItemDetail =>
                        {
                            var kitDetail = ((SalesOrderDatasetGateway.CustomerItemKitDetailViewRow)kitItemDetail);
                            var kitItem = new XElement("KITITEM");
                            kitItem.Add(new XElement("QTY", Localization.ParseLocaleDecimal(kitDetail.QuantityPerKit, cart.ThisCustomer.LocaleSetting)));
                            kitItem.Add(new XElement("DESCRIPTION", Security.HtmlEncode(kitDetail.ItemDescription)));
                            kitItems.Add(kitItem);
                        });
                       
                        xmlcartItem.Add(kitItems);
                        break;

                    #endregion

                    #region  "STOCK"
                    default:
                        xmlcartItem.Add(new XElement("ITEM_TYPE", "ITEM_TYPE_STOCK"));
                        //xml already processed above.
                        xmlcartItem.Add(new XElement("PRODUCT_HREF", producthref));
                        break;
                    #endregion
                }

                xmlcartItem.Add(new XElement("SHOWSTOCKHINTS", showStockHints));
                if (showStockHints)
                {
                    if (showShipDateInCart)
                    {
                        string allocatedQty = Localization.ParseLocaleDecimal(cartItem.m_AllocatedQty, cart.ThisCustomer.LocaleSetting);
                        xmlcartItem.Add(new XElement("ALLOCATEDQTY", allocatedQty));

                        var reserveCol = cart.GetReservation(cartItem.ItemCode);
                        xmlcartItem.Add(new XElement("RESERVATION_COUNT", reserveCol.Count));
                        var reservationItems = reserveCol
                                                .Select(r => new XElement("RESERVATIONITEM",
                                                        new XElement("RESERVE_ITEMCODE", r.ItemCode),
                                                        new XElement("RESERVE_SHIPDATE", r.ShipDate.ToShortDateString()),
                                                        new XElement("RESERVE_QTY", Localization.ParseLocaleDecimal(r.QtyReserved, cart.ThisCustomer.LocaleSetting))));
                        xmlcartItem.Add(reservationItems);
                    }
                }

                #region "Unit Measure"

                xmlcartItem.Add(new XElement("NOT_HIDE_UNIT_MEASURE", !hideUnitMeasure));
                if (!hideUnitMeasure)
                {
                    xmlcartItem.Add(new XElement("UNITMEASUREDESCRIPTION", AppLogic.GetUnitMeassureDescription(cartItem.UnitMeasureCode).ToHtmlEncode()));
                }

                #endregion

                #region Subtotal

                //original price no discount and should be tax exclusive
                decimal originalPrice = cartItem.Price.ToCustomerRoundedCurrency();

                if (vatEnabled && vatInclusive)
                {
                    originalPrice = (cartItem.Price - cartItem.TaxRate).ToCustomerRoundedCurrency();
                }

                decimal extPrice = lineItemRow.ExtPriceRate;
                if (isCouponTypeOrders)
                {
                    extPrice = originalPrice;
                }

                decimal vat = Decimal.Zero;
                var itemTaxDetail = cart.SalesOrderDataset
                                        .TransactionItemTaxDetailView
                                        .FirstOrDefault(itm => itm.ItemCode == cartItem.ItemCode && itm.LineNum == lineItemRow.LineNum);
                if (itemTaxDetail != null) { vat = itemTaxDetail.TaxAmount.ToCustomerRoundedCurrency(); }

                //add this if there are coupon
                if (!couponCode.IsNullOrEmptyTrimmed())
                {
                    //original amount (non discounted ) - discounted extended price
                    decimal discountAmount = originalPrice - lineItemRow.ExtPriceRate;
                    string discountType = cart.SalesOrderDataset.CustomerSalesOrderView[0][cart.SalesOrderDataset.CustomerSalesOrderView.CouponDiscountTypeColumn.ColumnName].ToString();
                    if (discountType == Interprise.Framework.Customer.Shared.Const.COUPON_DISCOUNTTYPE_AMOUNT)
                    {
                        discountAmount = lineItemRow.CouponDiscountRate;
                    }
                    appliedCouponDiscount += discountAmount;
                    xmlcartItem.Add(new XElement("COUPON_DISCOUNT_TYPE", discountType));
                    xmlcartItem.Add(new XElement("DISCOUNT_COUPON_RATE_VALUE", discountAmount.ToCustomerCurrency()));
                    xmlcartItem.Add(new XElement("DISCOUNT_COUPON_PERCENTAGE", string.Format("{0}%", Localization.ParseLocaleDecimal(lineItemRow.CouponDiscountRate, cart.ThisCustomer.LocaleSetting))));
                }

                xmlcartItem.Add(new XElement("VAT_INCLUSIVE", vatInclusive.ToStringLower()));
                xmlcartItem.Add(new XElement("VATENABLED", vatEnabled.ToStringLower()));

                if (vatEnabled && vatInclusive)
                {
                    extPrice += vat;
                }

                //computedSubtotal = sum of the item extended prices 
                 computedSubtotal += extPrice;

                xmlcartItem.Add(new XElement("VATAMOUNT_VALUE", cart.ThisCustomer.FormatBasedOnMyCurrency(vat)));
                xmlcartItem.Add(new XElement("PRICEFORMATTED", cart.ThisCustomer.FormatBasedOnMyCurrency(extPrice)));

                #endregion
            }

            decimal subTotal = cart.SalesOrderDataset.CustomerSalesOrderView[0].SubTotalRate;
            decimal tax = cart.SalesOrderDataset.CustomerSalesOrderView[0].TaxRate;
            decimal dueTotal = cart.SalesOrderDataset.CustomerSalesOrderView[0].TotalRate;
            decimal balance = cart.SalesOrderDataset.CustomerSalesOrderView[0].BalanceRate;
            decimal freightTax = cart.SalesOrderDataset.CustomerSalesOrderView[0].FreightTaxRate;

            tax -= freightTax;
            if (isCouponTypeOrders)
            {
                //if the condition below is satified, computedSubtotal will be used to reflect the applied coupon discount
                if (subTotal.Equals(Decimal.Zero) && appliedCouponDiscount > subTotal)
                {
                    appliedCouponDiscount = computedSubtotal;
                }
                subTotal += appliedCouponDiscount;

                //total coupon discount applied to the web order
                root.Add(new XElement("APPLIED_COUPON_DISCOUNT", appliedCouponDiscount.ToCustomerCurrency()));
            }
            
            decimal total = subTotal + tax;

            root.Add(new XElement("TAXCALCULATED_TEXT_2", tax.ToCustomerCurrency()));

            if (vatInclusive)
            {
                subTotal += tax;
            }

            if (isCouponTypeOrders)
            {
                total -= appliedCouponDiscount;
            }

            
            root.Add(new XElement("SUBTOTAL_VALUE", subTotal.ToCustomerCurrency()));
            root.Add(new XElement("TOTAL_VALUE", total.ToCustomerCurrency()));

            string html = new XmlPackage2("page.checkoutshipping_template.xml.config", root).TransformString();
            output.Append(html);
        }
    }
}
