﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Tool;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration
{
    public class DefaultShoppingCartPageLiteralRenderer : IShoppingCartHTMLLiteralRenderer
    {
        #region IShoppingCartHTMLLiteralRenderer Members

        public RenderType PageRenderType { get; set; }
        public string CustomXmlPackage { get; set; }
        public string coupon { get; set; }

        public decimal GiftCardAllocated { get; set; }

        public decimal GiftCertificateAllocated { get; set; }

        public decimal LoyaltyPointsCreditAllocated { get; set; }

        public decimal CreditMemosCreditAllocated { get; set; }

        public DefaultShoppingCartPageLiteralRenderer(RenderType pageRenderType, string customXmlPackage, decimal giftCardAllocated, decimal giftCertificateAllocated, decimal loyaltyPointsCreditAllocated, decimal creditMemosCreditAllocated, string couponcode = "")
        {
            this.PageRenderType = pageRenderType;
            this.CustomXmlPackage = customXmlPackage;
            this.coupon = couponcode;
            this.GiftCardAllocated = giftCardAllocated;
            this.GiftCertificateAllocated = giftCertificateAllocated;
            this.LoyaltyPointsCreditAllocated = loyaltyPointsCreditAllocated;
            this.CreditMemosCreditAllocated = creditMemosCreditAllocated;
        }

        public DefaultShoppingCartPageLiteralRenderer(RenderType pageRenderType, string customXmlPackage, string couponcode = "")
        {
            this.PageRenderType = pageRenderType;
            this.CustomXmlPackage = customXmlPackage;
            this.coupon = couponcode;
        }

        public DefaultShoppingCartPageLiteralRenderer(RenderType pageRenderType, string couponcode = "")
        {
            PageRenderType = pageRenderType;
            coupon = couponcode;
        }

        public DefaultShoppingCartPageLiteralRenderer()
        {
            PageRenderType = RenderType.ShoppingCart;
        }

        public void RenderShoppingCart(InterpriseShoppingCart cart, ref StringBuilder output)
        {
            var currencyService = ServiceFactory.GetInstance<ICurrencyService>();
            var appConfigService = ServiceFactory.GetInstance<IAppConfigService>();
            var shoppingCartService = ServiceFactory.GetInstance<IShoppingCartService>();

            bool vatenabled = AppLogic.AppConfigBool("VAT.Enabled");
            bool vatInclusive = (cart.ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive);
            bool isFreeShipping = false;
            decimal exchangeRate = Interprise.Facade.Base.SimpleFacade.Instance.GetExchangerate(cart.ThisCustomer.CurrencyCode);

            decimal subTotalChecker = cart.GetCartSubTotalExcludeOversized();
            isFreeShipping = (cart.CouponIncludesFreeShipping(coupon)) ? true :
                             (InterpriseHelper.IsFreeShippingThresholdEnabled(subTotalChecker) &&
                              InterpriseHelper.ShippingMethodCodeBelongsToFreeShippingMethodList(cart.GetCartShippingMethodSelected()) &&
                                (!cart.HasOversizedItems() || !cart.HasShippableComponents()));

            var root = new XElement(DomainConstants.XML_ROOT_NAME);
            root.Add(new XElement("RENDERTYPE", PageRenderType.ToString().ToUpper()));
            root.Add(new XElement("ISREGISTERED", Customer.Current.IsRegistered));
            root.Add(new XElement("VATINCLUSIVE", vatInclusive.ToStringLower()));
            root.Add(new XElement("VATENABLED", vatenabled.ToStringLower()));
            root.Add(new XElement("ISFREESHIPPING", isFreeShipping.ToStringLower()));
            root.Add(new XElement("ALLOWFRACTIONAL", AppLogic.IsAllowFractional.ToStringLower()));
            root.Add(new XElement("COSTCENTER", cart.ThisCustomer.CostCenter));

            if (cart.IsEmpty())
            {
                string cartEmptyText = string.Empty;
                if (cart.CartType == CartTypeEnum.ShoppingCart)
                {
                    var t1 = new Topic("EmptyCartText", cart.ThisCustomer.LocaleSetting, cart.SkinID, null);
                    output.Append(t1.Contents);
                }

                if (cart.CartType == CartTypeEnum.WishCart)
                {
                    var t1 = new Topic("EmptyWishListText", cart.ThisCustomer.LocaleSetting, cart.SkinID, null);
                    output.Append(t1.Contents);
                }

                root.Add(new XElement("EMPTY_CART_TEXT", cartEmptyText));
            }
            else
            {
                bool showPicsInCart = AppLogic.AppConfigBool("ShowPicsInCart");
                bool showLinkBack = AppLogic.AppConfigBool("LinkToProductPageInCart");
                bool hideUnitMeasure = AppLogic.AppConfigBool("HideUnitMeasure");
                bool showShipDateInCart = AppLogic.AppConfigBool("ShowShipDateInCart");
                bool showStockHints = AppLogic.AppConfigBool("ShowStockHints");
                bool isCouponTypeOrders = false;
                decimal computedSubtotal = Decimal.Zero;
                decimal appliedCouponDiscount = Decimal.Zero;
                var couponSetting = cart.GetCartCouponSetting(coupon);
                IEnumerable<ProductImage> productImagesForCart = null;

                if (!coupon.IsNullOrEmptyTrimmed())
                { isCouponTypeOrders = (couponSetting.m_CouponType == CouponTypeEnum.OrderCoupon); }

                root.Add(new XElement("SKINID", cart.SkinID));
                root.Add(new XElement("HASCOUPON", (!coupon.IsNullOrEmptyTrimmed()).ToStringLower()));
                root.Add(new XElement("IS_COUPON_TYPE_ORDERS", isCouponTypeOrders));
                root.Add(new XElement("SHOWSTOCKHINTS", showStockHints.ToStringLower()));
                root.Add(new XElement("SHOWSHIPDATEINCART", showShipDateInCart.ToStringLower()));

                if (showPicsInCart) {
                    var codes = new List<string>();
                    var ids = new List<string>();
                    codes = cart.CartItems.Select(c=> c.ItemCode).ToList();
                    ids = cart.CartItems.Select(c => c.ItemCounter.ToString()).ToList();
                    productImagesForCart = new List<ProductImage>();
                    productImagesForCart = ProductImage.LocateBulkDefaultImages("product", "icon", cart.ThisCustomer.LanguageCode, true, codes, ids).ToArray();
                }

                // get item reservations...
                var itemReservations = new List<ItemReservationCustomModel>();
                if (appConfigService.ShowStockHints && appConfigService.ShowShipDateInCart)
                {
                    shoppingCartService.UpdateAllocatedQuantities(cart.CartItems);
                    itemReservations = shoppingCartService.GetItemReservations().ToList();
                }

                foreach (CartItem cartItem in cart.CartItems)
                {
                    var xmlcartItem = new XElement("CART_ITEMS");
                    
                    var reserveCol = cart.GetReservation(itemReservations, cartItem);
                    xmlcartItem.Add(new XElement("RESERVECOL", reserveCol.Count()));

                    xmlcartItem.Add(new XElement("CART_ITEM_ID", cartItem.m_ShoppingCartRecordID));
                    xmlcartItem.Add(new XElement("ITEMTYPE", cartItem.ItemType));
                    xmlcartItem.Add(new XElement("ISOUTOFSTOCK", cartItem.IsOutOfStock.ToStringLower()));
                    xmlcartItem.Add(new XElement("CARTSTATUS", cartItem.Status));
                    xmlcartItem.Add(new XElement("POSTATUS", cartItem.POStatus));
                    xmlcartItem.Add(new XElement("ALLOCQTY", cartItem.m_AllocatedQty));
                    xmlcartItem.Add(new XElement("ITEMCODE", cartItem.ItemCode));
                    xmlcartItem.Add(new XElement("ITEMNAME", cartItem.ItemName));
                    xmlcartItem.Add(new XElement("COUNTER", cartItem.ItemCounter));

                    string productLinkHref = InterpriseHelper.MakeItemLink(cartItem.ItemCode);
                    string displayName = cartItem.DisplayName.ToHtmlEncode();
                    string notes = cartItem.m_Notes.ToHtmlEncode();
                    xmlcartItem.Add(new XElement("PRODUCTLINKNAME", displayName));
                    xmlcartItem.Add(new XElement("LINKBACK", showLinkBack.ToStringLower()));
                    xmlcartItem.Add(new XElement("ISCHECKOUTOPTION", cartItem.IsCheckoutOption.ToStringLower()));
                    xmlcartItem.Add(new XElement("PRODUCTNOTES", notes));
                    xmlcartItem.Add(new XElement("ISVDP", cartItem.IsVDP));
                    xmlcartItem.Add(new XElement("VDPCUSTOMIZATIONCODE", cartItem.VDPCustomisationCode));
                    xmlcartItem.Add(new XElement("VDPDOCUMENTCODE", cartItem.VDPDocumentCode));
                    xmlcartItem.Add(new XElement("VDPTYPE", cartItem.VDPType));
                    xmlcartItem.Add(new XElement("RECID", cartItem.Id));
                    xmlcartItem.Add(new XElement("ARTCOSTITEM", AppLogic.GetArtCostItemCode()));
                    xmlcartItem.Add(new XElement("ISFEATURED", cartItem.IsFeatured));
                    xmlcartItem.Add(new XElement("CATEGORYCODE", cartItem.CategoryCode.ToLower()));
                    bool isStorePickup = !cartItem.InStoreWarehouseCode.IsNullOrEmptyTrimmed();
                    if (isStorePickup)
                    {
                        xmlcartItem.Add(new XElement("IS_STOREPICKUP", isStorePickup.ToStringLower()));
                        xmlcartItem.Add(new XElement("ITEM_SHIPPINGMETHOD", cartItem.ShippingMethod));

                        var warehouseModel = ServiceFactory.GetInstance<IWarehouseService>()
                                                           .GetWarehouseByCodeCachedPerRequest(cartItem.InStoreWarehouseCode);

                        if (warehouseModel != null)
                        {
                            string address = "{1}, {2} {3} \n {4}".FormatWith(warehouseModel.Address,
                                                                                    warehouseModel.City, warehouseModel.State,
                                                                                    warehouseModel.PostalCode, warehouseModel.Country);

                            xmlcartItem.Add(new XElement("WAREHOUSE_NAME", warehouseModel.WareHouseDescription));
                            xmlcartItem.Add(new XElement("WAREHOUSE_ADDRESS", address));
                        }
                    }

                    //Add this item to the root.
                    root.Add(xmlcartItem);

                    switch (cartItem.ItemType)
                    {
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP:

                        #region "ITEM_TYPE_MATRIX_ITEM"
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM:

                            var matrixInfo = ServiceFactory.GetInstance<IProductService>()
                                                           .GetMatrixItemInfo(cartItem.ItemCode);
                            if (matrixInfo != null)
                            {
                                productLinkHref = InterpriseHelper.MakeItemLink(matrixInfo.ItemCode);
                                productLinkHref = CommonLogic.QueryStringSetParam(productLinkHref, DomainConstants.QUERY_STRING_KEY_MATRIX_ID, matrixInfo.Counter.ToString());
                            }

                            if (showLinkBack && !cartItem.IsCheckoutOption)
                            {
                                xmlcartItem.Add(new XElement("ISCHECKOUTOPTION", false));
                                xmlcartItem.Add(new XElement("PRODUCTLINKHREF", productLinkHref));
                            }

                            var matrixDetails = new StringBuilder();
                            var matrixAttributes = AppLogic.GetMatrixItemAttributes(cartItem.ItemCode, Customer.Current.LanguageCode);

                            //Iterate through matrix attributes and append it to the details
                            matrixAttributes.ForEach(item =>
                            {
                                matrixDetails.AppendFormat("&nbsp;&nbsp;{0}:{1}<br />", item.AttributeValue.ToHtmlEncode(), item.AttributeValueDescription.ToHtmlEncode());
                            });

                            xmlcartItem.Add(new XElement("MatrixDetails", matrixDetails.ToString()));

                            bool hasRegistryItem = (cartItem.RegistryItemCode.HasValue && cartItem.GiftRegistryID.HasValue);
                            xmlcartItem.Add(new XElement("ISREGISTRYITEM", hasRegistryItem.ToStringLower()));
                            if (hasRegistryItem)
                            {
                                xmlcartItem.Add(new XElement("REGISTRYITEMQUANTITY", Localization.ParseLocaleDecimal(cartItem.RegistryItemQuantity.Value, cart.ThisCustomer.LocaleSetting)));
                                xmlcartItem.Add(new XElement("ISREGISTRYITEMHASCONFLICT", cartItem.HasRegistryItemQuantityConflict().ToStringLower()));
                            }

                            break;

                        #endregion

                        #region "ITEM_TYPE_KIT"
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT:

                            if (showLinkBack && !cartItem.IsCheckoutOption)
                            {
                                xmlcartItem.Add(new XElement("ISCHECKOUTOPTION", false));
                                xmlcartItem.Add(new XElement("PRODUCTLINKHREF", productLinkHref));
                            }

                            var kitItems = new XElement("KIT_ITEMS");
                            string href = InterpriseSuiteEcommerceCommon.InterpriseHelper.MakeItemLink(cartItem.ItemCode) + "?kcid={0}".FormatWith(cartItem.Id.ToString());
                            kitItems.Add(new XElement("KIT_EDIT_HREF", href));

                            bool isCurrencyIncludedForInventorySelling = InterpriseHelper.IsCurrencyIncludedForInventorySelling(cart.ThisCustomer.CurrencyCode);
                            // Special case 2 
                            //  Currency is added in Inventory Selling Currency late
                            //  after the kit has been created, ideally the kit should regenerate kit pricing for this currency
                            //  but for the meantime, we should handle this by looking into the home currency
                            bool currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem =
                            InterpriseHelper.CurrencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYet(cart.ThisCustomer.CurrencyCode, cartItem.ItemCode);

                            // read the records from the EcommerceKitCart table...
                            var lstKitDetails = AppLogic.GetKitDetail(cartItem.ItemCode,
                                                                      CommonLogic.IIF((isCurrencyIncludedForInventorySelling && !currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem), cart.ThisCustomer.CurrencyCode, Currency.GetHomeCurrency()),
                                                                      cart.ThisCustomer.LocaleSetting,
                                                                      cart.ThisCustomer.CustomerCode,
                                                                      cartItem.Id,
                                                                      cart.ThisCustomer.ContactCode);

                            lstKitDetails.ForEach(item =>
                            {
                                kitItems.Add(new XElement("KITITEM", "({0}) {1}".FormatWith(
                                    Localization.ParseLocaleDecimal(item.Quantity, cart.ThisCustomer.LocaleSetting).TryParseInt().Value, item.Name).ToHtmlEncode()));
                            });

                            xmlcartItem.Add(kitItems);

                            hasRegistryItem = (cartItem.RegistryItemCode.HasValue && cartItem.GiftRegistryID.HasValue);
                            xmlcartItem.Add(new XElement("ISREGISTRYITEM", hasRegistryItem.ToStringLower()));
                            if (hasRegistryItem)
                            {
                                xmlcartItem.Add(new XElement("REGISTRYITEMQUANTITY", Localization.ParseLocaleDecimal(cartItem.RegistryItemQuantity.Value, cart.ThisCustomer.LocaleSetting)));
                                xmlcartItem.Add(new XElement("ISREGISTRYITEMHASCONFLICT", cartItem.HasRegistryItemQuantityConflict().ToStringLower()));
                            }

                            break;
                        #endregion

                        #region "STOCK"
                        default:

                            if (showLinkBack)
                            {
                                xmlcartItem.Add(new XElement("PRODUCTLINKHREF", productLinkHref));

                                hasRegistryItem = (cartItem.RegistryItemCode.HasValue && cartItem.GiftRegistryID.HasValue);
                                xmlcartItem.Add(new XElement("ISREGISTRYITEM", hasRegistryItem.ToStringLower()));
                                if (hasRegistryItem)
                                {
                                    xmlcartItem.Add(new XElement("REGISTRYITEMQUANTITY", Localization.ParseLocaleDecimal(cartItem.RegistryItemQuantity.Value, cart.ThisCustomer.LocaleSetting)));
                                    xmlcartItem.Add(new XElement("ISREGISTRYITEMHASCONFLICT", cartItem.HasRegistryItemQuantityConflict().ToStringLower()));
                                }
                            }

                            break;
                        #endregion
                    }

                    #region "Product Picture"

                    if (showPicsInCart)
                    {
                        xmlcartItem.Add(new XElement("SHOWPICSINCART", true));
                        var img = productImagesForCart.Single(imgItem => imgItem.Code == cartItem.ItemCode);

                        if (null != img)
                        {
                            xmlcartItem.Add(new XElement("PRODUCTIMAGEPATH", img.src));
                            xmlcartItem.Add(new XElement("PRODUCTIMAGETITLE", img.Title));
                            xmlcartItem.Add(new XElement("PRODUCTIMAGEALT", img.Alt));
                        }
                    }

                    #endregion

                    #region "Multiple Address"

                    xmlcartItem.Add(new XElement("HAS_MULTIPLE_ADDRESSES", cart.HasMultipleShippingAddresses().ToStringLower()));

                    /*************** multiple address **********************/
                    if (cart.HasMultipleShippingAddresses() && !cartItem.IsDownload)
                    {
                        xmlcartItem.Add(new XElement("ITEMISDOWNLOAD", false));
                        var adr = new Address();
                        bool includename = true;

                        string shippingName = string.Empty;
                        if (cartItem.m_ShippingAddressID == cart.ThisCustomer.PrimaryShippingAddressID && !cartItem.GiftRegistryID.HasValue)
                        {
                            adr.LoadByCustomer(cart.ThisCustomer, AddressTypes.Shipping, cartItem.m_ShippingAddressID, cartItem.GiftRegistryID);
                            shippingName = AppLogic.GetString("account.aspx.10");
                        }
                        else if (cartItem.m_ShippingAddressID == cart.ThisCustomer.PrimaryBillingAddressID)
                        {
                            adr.LoadByCustomer(cart.ThisCustomer, AddressTypes.Billing, cartItem.m_ShippingAddressID, cartItem.GiftRegistryID);
                            shippingName = AppLogic.GetString("account.aspx.8");
                        }
                        else
                        {
                            //specify the name if the shipping address is not primary
                            adr.LoadByCustomer(cart.ThisCustomer, AddressTypes.Shipping, cartItem.m_ShippingAddressID, cartItem.GiftRegistryID);
                            shippingName = adr.Name;
                            includename = false;
                        }

                        xmlcartItem.Add(new XElement("SHIP_ITEM_TO_VALUE", shippingName));
                        xmlcartItem.Add(new XElement("SHIP_ITEM_DETAIL", adr.DisplayString(false, false, includename, "")));
                        xmlcartItem.Add(new XElement("SHIPING_METHOD_VALUE", cartItem.m_ShippingMethod));
                    }

                    #endregion

                    #region "Stock Hints"

                    if (showStockHints && showShipDateInCart)
                    {
                        string localizedQty = Localization.ParseLocaleDecimal(cartItem.m_AllocatedQty, cart.ThisCustomer.LocaleSetting);
                        bool isCBNItem = (cartItem.IsCBN && !cartItem.CBNItemID.IsNullOrEmptyTrimmed()) ? true : false;
                        decimal freeStock = cartItem.FreeStock;
                        bool isDropShip = cartItem.IsDropShip;

                        xmlcartItem.Add(new XElement("ALLOCATEDQTY", localizedQty));
                        xmlcartItem.Add(new XElement("ISCBNITEM", isCBNItem));
                        xmlcartItem.Add(new XElement("ISDROPSHIP", isDropShip));

                        decimal ordered =  cartItem.m_Quantity;
                        decimal totalReserved =cartItem.m_AllocatedQty;
                        decimal notAvailableQty = decimal.Zero;

                        if (!cartItem.IsCBN)
                        {

                            decimal qtyReserved = (reserveCol == null) ? 0 : reserveCol.Sum(rc => rc.QtyReserved);

                            notAvailableQty = cartItem.m_Quantity - (cartItem.m_AllocatedQty + qtyReserved);
                                
                            xmlcartItem.Add(new XElement("NOTAVAILABLEQTYWITHRESERVATION", Localization.ParseLocaleDecimal(notAvailableQty, cart.ThisCustomer.LocaleSetting)));
                        }
                        else
                        {
                            decimal computedQty = decimal.Zero;
                            computedQty = ordered - freeStock;

                            if (computedQty <= 0)
                            {
                                computedQty = ordered;
                            }
                            else
                            {
                                notAvailableQty = computedQty;
                                computedQty = ordered - notAvailableQty;
                            }
                            xmlcartItem.Add(new XElement("AVAILABLEQTY", Localization.ParseLocaleDecimal(computedQty, cart.ThisCustomer.LocaleSetting)));
                            xmlcartItem.Add(new XElement("NOTAVAILABLEQTY", Localization.ParseLocaleDecimal(notAvailableQty, cart.ThisCustomer.LocaleSetting)));
                        }

                        xmlcartItem.Add(new XElement("RESERVATION_COUNT", reserveCol.Count));
                        var reservationItems = reserveCol
                                .Select(r => new XElement("RESERVATIONITEM",
                                        new XElement("RESERVE_ITEMCODE", r.ItemCode),
                                        new XElement("RESERVE_SHIPDATE", r.ShipDate.ToShortDateString()),
                                        new XElement("RESERVE_QTY", Localization.ParseLocaleDecimal(r.QtyReserved, cart.ThisCustomer.LocaleSetting))));
                        xmlcartItem.Add(reservationItems);
                    }

                    #endregion

                    #region "Unit Measure"

                    if (!hideUnitMeasure)
                    {
                        xmlcartItem.Add(new XElement("NOT_HIDE_UNIT_MEASURE", true));
                        var availableUnitMeasures = ProductPricePerUnitMeasure.GetAll(cartItem.ItemCode, cart.ThisCustomer, false, false, true);

                        if (availableUnitMeasures.Count > 1)
                        {
                            xmlcartItem.Add(new XElement("AVAILABLEUNITMESSURE_GREATER_ONE", true));
                            xmlcartItem.Add(new XElement("MULTIPLE_UNITMEASURECODENAME", "UnitMeasureCode_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                            xmlcartItem.Add(new XElement("MULTIPLE_UNITMEASURECODEID", "UnitMeasureCode_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));

                            foreach (var unitMeasureValue in availableUnitMeasures)
                            {
                                var unitmeasures = new XElement("UNITMEASSURE_ITEM");
                                xmlcartItem.Add(unitmeasures);
                                bool isCurrentUnitMeasureTheOneSelectedInProductPage = cartItem.UnitMeasureCode.Equals(unitMeasureValue.code, StringComparison.InvariantCultureIgnoreCase);
                                unitmeasures.Add(new XElement("VALUE", unitMeasureValue.code.ToHtmlEncode()));
                                unitmeasures.Add(new XElement("TEXT", unitMeasureValue.description));
                                unitmeasures.Add(new XElement("SELECTED", isCurrentUnitMeasureTheOneSelectedInProductPage));
                                
                                unitmeasures.Add(new XElement("UNITMEASUREQUANTITY", unitMeasureValue.unitMeasureQuantity));
                                unitmeasures.Add(new XElement("MINORDERQUANTITYID", "MinOrderQuantity_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                                
                                if (isCurrentUnitMeasureTheOneSelectedInProductPage)
                                {
                                    xmlcartItem.Add(new XElement("UNITMEASURECODESPANDISPLAY", unitMeasureValue.code.ToHtmlEncode()));
                                }
                            }
                        }
                        else
                        {
                            xmlcartItem.Add(new XElement("AVAILABLEUNITMESSURE_GREATER_ONE", false));
                            xmlcartItem.Add(new XElement("UNITMEASURECODEID", "UnitMeasureCode_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                            xmlcartItem.Add(new XElement("UNITMEASURECODENAME", "UnitMeasureCode_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                            xmlcartItem.Add(new XElement("UNITMEASURECODEVALUE", cartItem.UnitMeasureCode.ToHtmlEncode()));
                            xmlcartItem.Add(new XElement("UNITMEASURECODESPANDISPLAY", availableUnitMeasures[0].code.ToHtmlEncode()));
                        }
                    }

                    #endregion

                    #region "Input Quantities and Delete"
                    if ((cart.ThisCustomer.CostCenter != "Smartbag-PREMIUM APG" && cart.ThisCustomer.CostCenter != "Smartbag-PREMIUM CUE" && cart.ThisCustomer.CostCenter != "Smartbag-PREMIUM DION LEE") || cart.ThisCustomer.JobRole != "Admin")
                    {
                        xmlcartItem.Add(new XElement("ISRESTRICTEDQUANTITIES", (cartItem.RestrictedQuantities.Count > 0)));
                        if (cartItem.RestrictedQuantities.Count > 0)
                        {
                            xmlcartItem.Add(new XElement("QUANTITYLISTID", cartItem.m_ShoppingCartRecordID));
                            xmlcartItem.Add(cartItem.RestrictedQuantities
                                        .Select(r => new XElement("RESTRICTEDQUANTITIES",
                                                new XElement("QTY", r),
                                                new XElement("SELECTED", r == cartItem.m_Quantity))));
                        }
                        else
                        {
                            xmlcartItem.Add(new XElement("INPUTQUANTITYID", "Quantity_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                        }
                    }
                    else
                    {
                        xmlcartItem.Add(new XElement("ISRESTRICTEDQUANTITIES", false));
                        xmlcartItem.Add(new XElement("INPUTQUANTITYID", "Quantity_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                    }
                    xmlcartItem.Add(new XElement("INPUTQUANTITYVALUE", Localization.ParseLocaleDecimal(cartItem.m_Quantity, cart.ThisCustomer.LocaleSetting)));
                    xmlcartItem.Add(new XElement("INPUTQUANTITYVALUE2", Localization.ParseLocaleDecimal((CommonLogic.IIF(cartItem.m_Quantity / cartItem.UnitMeasureQty < 1, cartItem.m_Quantity, cartItem.m_Quantity / cartItem.UnitMeasureQty)), cart.ThisCustomer.LocaleSetting)));
                    xmlcartItem.Add(new XElement("UOMQUANTITY", Localization.ParseLocaleDecimal(cartItem.UnitMeasureQty, cart.ThisCustomer.LocaleSetting)));

                    if (AppLogic.AppConfigBool("ShowCartDeleteItemButton"))
                    {
                        xmlcartItem.Add(new XElement("SHOWCARTDELETEITEMBUTTON", true));
                    }

                    decimal minimumOrderQty = cartItem.m_MinimumQuantity / cartItem.UnitMeasureQty;
                    if (!AppLogic.IsAllowFractional) {minimumOrderQty = Math.Ceiling(minimumOrderQty);}

                    xmlcartItem.Add(new XElement("MINORDERQUANTITYID", "MinOrderQuantity_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                    xmlcartItem.Add(new XElement("MINORDERQUANTITYNAME", "MinOrderQuantity_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                    xmlcartItem.Add(new XElement("MINORDERQUANTITYVALUE", minimumOrderQty.ToCustomerRoundedCurrency()));

                    #endregion

                    #region "Subtotal"

                    //original price (no discount and should be tax exclusive)
                    decimal originalPrice = cartItem.Price.ToCustomerRoundedCurrency();
                    if (vatenabled && vatInclusive)
                    {
                        originalPrice = (cartItem.Price - cartItem.TaxRate).ToCustomerRoundedCurrency();
                    }
                    // with tax
                    decimal extPrice = cartItem.Price;
                    //added logic to display the non discounted price
                    //if (isCouponTypeOrders)
                    //{
                    //    extPrice = originalPrice;
                    //}

                    decimal vat = cartItem.TaxRate;
                    
                    //added: reason -> to display sales price (no deductions) on order summary
                    xmlcartItem.Add(new XElement("SALES_PRICE", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency( cartItem.UnitPrice,cart.ThisCustomer.CurrencyCode).ToCustomerCurrency()));
                    xmlcartItem.Add(new XElement("SALES_EXTPRICE", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(extPrice, cart.ThisCustomer.CurrencyCode).ToCustomerCurrency()));//cartItem.Price.ToCustomerCurrency()));
                    //add this if there are coupon
                    decimal coupondiscount = Decimal.Zero;
                    if (!coupon.IsNullOrEmptyTrimmed())
                    {
                        //original amount (non discounted) - discounted extended price
                        coupondiscount = (cartItem.DiscountAmountAlreadyComputed) ? cartItem.CouponDiscount : cart.GetCartItemCouponDiscount(cartItem);
                        //decimal discountAmount = (coupondiscount == Decimal.Zero) ? coupondiscount : originalPrice - coupondiscount;
                        
                        string discountType = couponSetting.m_DiscountType.ToString();
                        decimal percentageDiscount = decimal.Zero;
                        discountType = (couponSetting.m_DiscountType == CouponDiscountTypeEnum.AmountDiscount) ? Interprise.Framework.Customer.Shared.Const.COUPON_DISCOUNTTYPE_AMOUNT : Interprise.Framework.Customer.Shared.Const.COUPON_DISCOUNTTYPE_PERCENT;

                        if (discountType == Interprise.Framework.Customer.Shared.Const.COUPON_DISCOUNTTYPE_PERCENT)
                        {
                            percentageDiscount = (coupondiscount == Decimal.Zero) ? coupondiscount : couponSetting.m_DiscountPercent;
                        }

                        if (isCouponTypeOrders){
                            //this will support/fix rounding issues from CB to CBE
                            appliedCouponDiscount += coupondiscount.ToCustomerRoundedCurrency();
                        }

                        else {
                            //Do not round-off decimal
                            //coupondiscount = coupondiscount.ToCustomerRoundedCurrency();
                            appliedCouponDiscount += coupondiscount;
                            //Differentiate express bag computation from white bag and sticker bundles
                            if (cartItem.ItemType == "Kit" && (cartItem.CategoryCode != "White bag and sticker bundles" && cartItem.CategoryCode != "Brown bag and sticker bundles"))
                            {
                                extPrice = (coupondiscount * (cartItem.m_Quantity / cartItem.UnitMeasureQty)).ToCustomerRoundedCurrency();
                            }
                            else
                            {
                                extPrice = (coupondiscount * cartItem.m_Quantity).ToCustomerRoundedCurrency();
                            }
                            //extPrice = (((extPrice / cartItem.m_Quantity) - (coupondiscount / cartItem.m_Quantity)) * cartItem.m_Quantity).ToCustomerRoundedCurrency();

                            if (extPrice <= decimal.Zero) { extPrice = decimal.Zero; }

                            xmlcartItem.Add(new XElement("COUPON_DISCOUNT_TYPE", discountType));
                            xmlcartItem.Add(new XElement("DISCOUNT_COUPON_RATE_VALUE", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(coupondiscount, cart.ThisCustomer.CurrencyCode).ToCustomerCurrency()));
                            xmlcartItem.Add(new XElement("DISCOUNT_COUPON_PERCENTAGE", string.Format("{0}%", Localization.ParseLocaleDecimal(percentageDiscount, cart.ThisCustomer.LocaleSetting))));
                        }
                    }

                    // subtotal for order
                    computedSubtotal += extPrice;

                    // set flags for computaion
                    cartItem.PriceAndTaxAlreadyComputed = true;
                    cartItem.UnitPriceAlreadyComputed = true;
                    cartItem.DiscountAmountAlreadyComputed = true;

                    // recalculate tax rate for item based on discounted price
                    if (coupondiscount > Decimal.Zero && vat > Decimal.Zero)
                    {
                        // for computation in summary
                        //cartItem.TaxRate = (extPrice * (vat / cartItem.Price.ToCustomerRoundedCurrency())).ToCustomerRoundedCurrency();
                        cartItem.TaxRate = (extPrice * (vat / Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(cartItem.Price)));
                        // for display
                        vat = cartItem.TaxRate;
                    }
                                   
                    xmlcartItem.Add(new XElement("PRICEFORMATTED", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(extPrice).ToCustomerCurrency()));
                    
                    if (vatenabled){xmlcartItem.Add(new XElement("TAX_RATE_VALUE", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(vat).ToCustomerCurrency()));}
                    
                    #endregion
                }

                //TOTAL COMPUTATION ------------------------------------------------------------------------------------//

                decimal subTotal = computedSubtotal;

                if (isCouponTypeOrders)
                {
                    if (couponSetting.m_DiscountType == CouponDiscountTypeEnum.AmountDiscount && appliedCouponDiscount > decimal.Zero) { appliedCouponDiscount = couponSetting.m_DiscountAmount; }

                    //if the condition below is satified, computedSubtotal will be used to reflect the applied coupon discount 
                    if (appliedCouponDiscount > subTotal)
                    {
                        appliedCouponDiscount = computedSubtotal;
                        subTotal = decimal.Zero;
                    }
                    else
                    {
                        subTotal -= appliedCouponDiscount;
                    }

                    subTotal += appliedCouponDiscount;

                    //total coupon discount applied to the web order
                    root.Add(new XElement("APPLIED_COUPON_DISCOUNT", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(appliedCouponDiscount).ToCustomerCurrency()));
                }


                //use API to get freight
                //Set Freight to zero for Portal website
                decimal freightRate = PageRenderType != RenderType.Company ? cart.CartFreightRate : Decimal.Zero;
                
                // get cart shipping address default
                var shippingAddress = cart.ThisCustomer.PrimaryShippingAddress;
                string freightTaxCode = cart.ThisCustomer.FreightTaxCode;
                
                // for multiship
                if (cart.ThisCustomer.IsRegistered && AppLogic.AppConfigBool("AllowMultipleShippingAddressPerOrder") && shippingAddress.AddressID != cart.FirstItemShippingAddressID())
                    {
                    var shipAddresses = cart.ThisCustomer.ShippingAddresses.Where(a => a.AddressID == cart.FirstItemShippingAddressID());
                    if (shipAddresses != null) { 
                        shippingAddress = shipAddresses.First();
                        freightTaxCode = Interprise.Facade.Base.SimpleFacade.Instance.GetField(
                        Interprise.Framework.Base.Shared.Const.CUSTOMERSHIPTOVIEW_FREIGHTTAX_COLUMN, Interprise.Framework.Base.Shared.Const.CUSTOMERSHIPTO_TABLE,
                        String.Format(Interprise.Framework.Base.Shared.Const.FORMAT_FILTER_0, Interprise.Framework.Base.Shared.Const.CUSTOMERSHIPTO_SHIPTOCODE_COLUMN, cart.FirstItemShippingAddressID()));

                        if (freightTaxCode.IsNullOrEmptyTrimmed()) { freightTaxCode = cart.ThisCustomer.FreightTaxCode; }
                    }
                    else{ 
                        shippingAddress = cart.ThisCustomer.PrimaryShippingAddress; 
                    }
                }
                
                // compute freight tax
                //Set Freight to zero for Portal website
                decimal freightTax = PageRenderType != RenderType.Company ? cart.GetCartFreightRateTax(cart.ThisCustomer.CurrencyCode, freightRate, freightTaxCode, shippingAddress) : Decimal.Zero;
                
                decimal tax = Decimal.Zero;
                decimal total = Decimal.Zero;

                //if ((PageRenderType != RenderType.ShoppingCart && Customer.Current.IsNotRegistered) || Customer.Current.IsRegistered)
                //{
                    //add tax if Inclusive
                    tax =(vatenabled && vatInclusive)?Decimal.Zero:Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(cart.GetCartTaxTotal());
                    
                    // add shipping fee
                     //total += Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(freightTax);
                //}
                            
                // add tax for final total
                total = subTotal + tax + freightRate + freightTax;

                var _stringResourceService = ServiceFactory.GetInstance<IStringResourceService>();

                string subTotalFormatted = Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(subTotal).ToCustomerCurrency();
                decimal freight = (vatenabled && vatInclusive) ? cart.GetCartFreightRate() + freightTax :  cart.GetCartFreightRate();
                //Set Freight to zero for Portal website
                if (CommonLogic.GetWebsiteType() == WebsiteType.Company)
                {
                    freight = 0;
                }

                root.Add(new XElement("SHOW_TAX_BREAK_DOWN",  (AppLogic.AppConfigBool("ShowTaxBreakDown") && (freightTax + tax) > 0).ToStringLower()));

                //if (PageRenderType == RenderType.Payment || PageRenderType == RenderType.Review || PageRenderType == RenderType.Company)
                //{
                    root.Add(new XElement("IS_EMPTY_SHIPPING_METHOD", cart.GetCartShippingMethodSelected().IsNullOrEmptyTrimmed().ToStringLower()));
                    root.Add(new XElement("FREIGHT", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(freight, cart.ThisCustomer.CurrencyCode).ToCustomerCurrency()));
                    root.Add(new XElement("FREIGHTTAX", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(freightTax, cart.ThisCustomer.CurrencyCode).ToCustomerCurrency()));
                //}

                if (isCouponTypeOrders)
                {
                    total -= appliedCouponDiscount;
                }

                root.Add(new XElement("SUBTOTAL_VALUE", subTotalFormatted));

                #region "Gift Card / Gift Certificate"
                if (appConfigService.GiftCodeEnabled)
                {
                    decimal gCardCredit = cart.GiftCardsTotalAmountApplied - this.GiftCardAllocated;
                    decimal gCertCredit = cart.GiftCertsTotalAmountApplied - this.GiftCertificateAllocated;
                    decimal allocate = Decimal.Zero;

                    if (!cart.ThisCustomer.ThisCustomerSession[DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS].IsNullOrEmptyTrimmed())
                    {
                        string selectedPaymentTerm = cart.ThisCustomer.PaymentTermCode.ToUpperInvariant();
                        if (Convert.ToBoolean(cart.ThisCustomer.ThisCustomerSession[DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS]) &&
                           (selectedPaymentTerm.Equals("REQUEST QUOTE") || selectedPaymentTerm.Equals("PURCHASE ORDER")))
                        {
                            cart.ClearGiftCodes();
                        }
                    }

                    if (gCardCredit > Decimal.Zero && total > Decimal.Zero && !cart.HasAppliedInvalidGiftCard())
                    {
                        allocate = (gCardCredit > total) ? total : gCardCredit;
                        total -= allocate;
                        cart.GiftCardsTotalCreditAllocated = allocate;
                        root.Add(new XElement("APPLIED_CREDIT_GIFT_CARD", currencyService.FormatCurrency(allocate)));
                    }

                    if (gCertCredit > Decimal.Zero && total > Decimal.Zero && !cart.HasAppliedInvalidGiftCert())
                    {
                        allocate = (gCertCredit > total) ? total : gCertCredit;
                        total -= allocate;
                        cart.GiftCertsTotalCreditAllocated = allocate;
                        root.Add(new XElement("APPLIED_CREDIT_GIFT_CERTIFICATE", currencyService.FormatCurrency(allocate)));
                    }
                }
                #endregion

                #region "LoyaltyPoints
                if (appConfigService.LoyaltyPointsEnabled)
                {
                    decimal loyaltyPointsCredit = cart.LoyaltyPointsAmountApplied - this.LoyaltyPointsCreditAllocated;
                    decimal allocate = Decimal.Zero;

                    if (!cart.ThisCustomer.ThisCustomerSession[DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS].IsNullOrEmptyTrimmed())
                    {
                        string selectedPaymentTerm = cart.ThisCustomer.PaymentTermCode.ToUpperInvariant();
                        if (Convert.ToBoolean(cart.ThisCustomer.ThisCustomerSession[DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS]) &&
                           (selectedPaymentTerm.Equals("REQUEST QUOTE") || selectedPaymentTerm.Equals("PURCHASE ORDER")))
                        {
                            cart.ClearLoyaltyPoints();
                        }
                    }

                    if (loyaltyPointsCredit > Decimal.Zero && total > Decimal.Zero && !cart.HasAppliedInvalidLoyaltyPoints())
                    {
                        allocate = (loyaltyPointsCredit > total) ? total : loyaltyPointsCredit;
                        total -= allocate;
                        cart.LoyaltyPointsCreditAllocated = allocate;
                        root.Add(new XElement("APPLIED_LOYALTYPOINTS_AMOUNT", currencyService.FormatCurrency(allocate)));
                    }
                }
                #endregion

                #region "Credit Memo"
                if (appConfigService.CreditRedemptionIsEnabled)
                {
                    decimal creditMemosCredit = cart.CreditMemosTotalAmountApplied - this.CreditMemosCreditAllocated;
                    decimal allocate = Decimal.Zero;

                    if (creditMemosCredit > Decimal.Zero && total > Decimal.Zero && !cart.HasAppliedInvalidCreditMemo())
                    {
                        allocate = (creditMemosCredit > total) ? total : creditMemosCredit;
                        total -= allocate;
                        cart.CreditMemosCreditAllocated = allocate;
                        root.Add(new XElement("APPLIED_CREDITMEMOS_AMOUNT", currencyService.FormatCurrency(allocate)));
                    }
                }
                #endregion

                //root.Add(new XElement("TAX_RATE_VALUE", _stringResourceService.GetString("shoppingcart.aspx.12")));

                root.Add(new XElement("LINEITEMTAX", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(tax, cart.ThisCustomer.CurrencyCode).ToCustomerCurrency()));

                root.Add(new XElement("TAX_RATE_VALUE", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency((tax + freightTax), cart.ThisCustomer.CurrencyCode).ToCustomerCurrency()));
                root.Add(new XElement("TOTAL", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(total).ToCustomerCurrency()));

                string html = string.Empty;

                if (this.CustomXmlPackage.IsNullOrEmptyTrimmed())
                {
                    html = new XmlPackage2("page.shoppingcart_items_template.xml.config", root).TransformString();
                }
                else
                {
                    html = new XmlPackage2(this.CustomXmlPackage, root).TransformString();
                }

                output.Append(html);
            }
        }

        public void Render(InterpriseShoppingCart cart, ref StringBuilder output)
        {
            if (!cart.IsSalesOrderDetailBuilt) { RenderShoppingCart(cart, ref output); return; }

            var currencyService = ServiceFactory.GetInstance<ICurrencyService>();
            var appConfigService = ServiceFactory.GetInstance<IAppConfigService>();

            bool vatenabled = AppLogic.AppConfigBool("VAT.Enabled");
            bool vatInclusive = (cart.ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive);
            bool isFreeShipping = false;

            if (cart.SalesOrderDataset != null)
            {
                decimal subTotalChecker = cart.GetCartSubTotalExcludeOversized();
                isFreeShipping = (cart.CouponIncludesFreeShipping(coupon) ||
                    (InterpriseHelper.ShippingMethodCodeBelongsToFreeShippingMethodList(cart.SalesOrderDataset.CustomerSalesOrderView[0].ShippingMethodCode) &&
                    InterpriseHelper.IsFreeShippingThresholdEnabled(subTotalChecker) && !cart.HasOversizedItems()) || !cart.HasShippableComponents());
            }

            var root = new XElement(DomainConstants.XML_ROOT_NAME);
            root.Add(new XElement("RENDERTYPE", PageRenderType.ToString().ToUpper()));
            root.Add(new XElement("ISREGISTERED", Customer.Current.IsRegistered));
            root.Add(new XElement("VATINCLUSIVE", vatInclusive.ToStringLower()));
            root.Add(new XElement("VATENABLED", vatenabled.ToStringLower()));
            root.Add(new XElement("ISFREESHIPPING", isFreeShipping.ToStringLower()));

            if (cart.IsEmpty())
            {
                string cartEmptyText = string.Empty;
                if (cart.CartType == CartTypeEnum.ShoppingCart)
                {
                    var t1 = new Topic("EmptyCartText", cart.ThisCustomer.LocaleSetting, cart.SkinID, null);
                    output.Append(t1.Contents);
                }

                if (cart.CartType == CartTypeEnum.WishCart)
                {
                    var t1 = new Topic("EmptyWishListText", cart.ThisCustomer.LocaleSetting, cart.SkinID, null);
                    output.Append(t1.Contents);
                }

                root.Add(new XElement("EMPTY_CART_TEXT", cartEmptyText));
            }
            else
            {
                bool showPicsInCart = AppLogic.AppConfigBool("ShowPicsInCart");
                bool showLinkBack = AppLogic.AppConfigBool("LinkToProductPageInCart");
                bool hideUnitMeasure = AppLogic.AppConfigBool("HideUnitMeasure");
                bool showShipDateInCart = AppLogic.AppConfigBool("ShowShipDateInCart");
                bool showStockHints = AppLogic.AppConfigBool("ShowStockHints");
                bool isCouponTypeOrders = false;
                decimal computedSubtotal = Decimal.Zero;
                decimal appliedCouponDiscount = Decimal.Zero;

                if (!coupon.IsNullOrEmptyTrimmed())
                {
                    isCouponTypeOrders = cart.SalesOrderDataset.CustomerSalesOrderView[0]
                                        [cart.SalesOrderDataset.CustomerSalesOrderView.CouponTypeColumn.ColumnName].ToString().Equals("Orders");
                }

                //root.Add(new XElement("SHOPPINGCART_HEADER_TEXT", AppLogic.GetString("mobile.shoppingcart.cs.1", cart.SkinID, cart.ThisCustomer.LocaleSetting)));
                root.Add(new XElement("SKINID", cart.SkinID));
                root.Add(new XElement("HASCOUPON", (!coupon.IsNullOrEmptyTrimmed()).ToStringLower()));
                root.Add(new XElement("IS_COUPON_TYPE_ORDERS", isCouponTypeOrders));
                root.Add(new XElement("SHOWSTOCKHINTS", showStockHints.ToStringLower()));
                root.Add(new XElement("SHOWSHIPDATEINCART", showShipDateInCart.ToStringLower()));

                foreach (CartItem cartItem in cart.CartItems)
                {
                    var xmlcartItem = new XElement("CART_ITEMS");
                    xmlcartItem.Add(new XElement("CART_ITEM_ID", cartItem.m_ShoppingCartRecordID));
                    xmlcartItem.Add(new XElement("ITEMTYPE", cartItem.ItemType));
                    xmlcartItem.Add(new XElement("ISOUTOFSTOCK", cartItem.IsOutOfStock.ToStringLower()));
                    xmlcartItem.Add(new XElement("CARTSTATUS", cartItem.Status));
                    xmlcartItem.Add(new XElement("POSTATUS", cartItem.POStatus));
                    xmlcartItem.Add(new XElement("ALLOCQTY", cartItem.m_AllocatedQty));
                    xmlcartItem.Add(new XElement("ITEMCODE", cartItem.ItemCode));

                    var reserveCol = cart.GetReservation(cartItem.ItemCode);
                    xmlcartItem.Add(new XElement("RESERVECOL", reserveCol.Count()));

                    string productLinkHref = InterpriseHelper.MakeItemLink(cartItem.ItemCode);
                    string displayName = cartItem.DisplayName.ToHtmlEncode();
                    string notes = cartItem.m_Notes.ToHtmlEncode();
                    xmlcartItem.Add(new XElement("PRODUCTLINKNAME", displayName));
                    xmlcartItem.Add(new XElement("LINKBACK", showLinkBack.ToStringLower()));
                    xmlcartItem.Add(new XElement("ISCHECKOUTOPTION", cartItem.IsCheckoutOption.ToStringLower()));
                    xmlcartItem.Add(new XElement("PRODUCTNOTES", notes));

                    bool isStorePickup = !cartItem.InStoreWarehouseCode.IsNullOrEmptyTrimmed();
                    if (isStorePickup)
                    {
                        xmlcartItem.Add(new XElement("IS_STOREPICKUP", isStorePickup.ToStringLower()));
                        xmlcartItem.Add(new XElement("ITEM_SHIPPINGMETHOD", cartItem.ShippingMethod));

                        var warehouseModel = ServiceFactory.GetInstance<IWarehouseService>()
                                                           .GetWarehouseByCodeCachedPerRequest(cartItem.InStoreWarehouseCode);

                        if (warehouseModel != null)
                        {
                            string address = "{1}, {2} {3} \n {4}".FormatWith(warehouseModel.Address,
                                                                                    warehouseModel.City, warehouseModel.State,
                                                                                    warehouseModel.PostalCode, warehouseModel.Country);

                            xmlcartItem.Add(new XElement("WAREHOUSE_NAME", warehouseModel.WareHouseDescription));
                            xmlcartItem.Add(new XElement("WAREHOUSE_ADDRESS", address));
                        }
                    }

                    //Add this item to the root.
                    root.Add(xmlcartItem);

                    switch (cartItem.ItemType)
                    {
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP:

                        #region "ITEM_TYPE_MATRIX_ITEM"
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM:

                            var matrixInfo = ServiceFactory.GetInstance<IProductService>()
                                                           .GetMatrixItemInfo(cartItem.ItemCode);
                            if (matrixInfo != null)
                            {
                                productLinkHref = InterpriseHelper.MakeItemLink(matrixInfo.ItemCode);
                                productLinkHref = CommonLogic.QueryStringSetParam(productLinkHref, DomainConstants.QUERY_STRING_KEY_MATRIX_ID, matrixInfo.Counter.ToString());
                            }

                            if (showLinkBack && !cartItem.IsCheckoutOption)
                            {
                                xmlcartItem.Add(new XElement("ISCHECKOUTOPTION", false));
                                xmlcartItem.Add(new XElement("PRODUCTLINKHREF", productLinkHref));
                            }

                            var matrixDetails = new StringBuilder();
                            var matrixAttributes = AppLogic.GetMatrixItemAttributes(cartItem.ItemCode, Customer.Current.LanguageCode);

                            //Iterate through matrix attributes and append it to the details
                            matrixAttributes.ForEach(item =>
                            {
                                matrixDetails.AppendFormat("&nbsp;&nbsp;{0}:{1}<br />", item.AttributeValue.ToHtmlEncode(), item.AttributeValueDescription.ToHtmlEncode());
                            });

                            xmlcartItem.Add(new XElement("MatrixDetails", matrixDetails.ToString()));

                            bool hasRegistryItem = (cartItem.RegistryItemCode.HasValue && cartItem.GiftRegistryID.HasValue);
                            xmlcartItem.Add(new XElement("ISREGISTRYITEM", hasRegistryItem.ToStringLower()));
                            if (hasRegistryItem)
                            {
                                xmlcartItem.Add(new XElement("REGISTRYITEMQUANTITY", Localization.ParseLocaleDecimal(cartItem.RegistryItemQuantity.Value, cart.ThisCustomer.LocaleSetting)));
                                xmlcartItem.Add(new XElement("ISREGISTRYITEMHASCONFLICT", cartItem.HasRegistryItemQuantityConflict().ToStringLower()));
                            }

                            break;

                        #endregion

                        #region "ITEM_TYPE_KIT"
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT:

                            if (showLinkBack && !cartItem.IsCheckoutOption)
                            {
                                xmlcartItem.Add(new XElement("ISCHECKOUTOPTION", false));
                                xmlcartItem.Add(new XElement("PRODUCTLINKHREF", productLinkHref));
                            }

                            var kitItems = new XElement("KIT_ITEMS");
                            string href = InterpriseSuiteEcommerceCommon.InterpriseHelper.MakeItemLink(cartItem.ItemCode) + "?kcid={0}".FormatWith(cartItem.Id.ToString());
                            kitItems.Add(new XElement("KIT_EDIT_HREF", href));

                            bool isCurrencyIncludedForInventorySelling = InterpriseHelper.IsCurrencyIncludedForInventorySelling(cart.ThisCustomer.CurrencyCode);
                            // Special case 2 
                            //  Currency is added in Inventory Selling Currency late
                            //  after the kit has been created, ideally the kit should regenerate kit pricing for this currency
                            //  but for the meantime, we should handle this by looking into the home currency
                            bool currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem =
                            InterpriseHelper.CurrencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYet(cart.ThisCustomer.CurrencyCode, cartItem.ItemCode);

                            // read the records from the EcommerceKitCart table...
                            var lstKitDetails = AppLogic.GetKitDetail(cartItem.ItemCode,
                                                                      CommonLogic.IIF((isCurrencyIncludedForInventorySelling && !currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem), cart.ThisCustomer.CurrencyCode, Currency.GetHomeCurrency()),
                                                                      cart.ThisCustomer.LocaleSetting,
                                                                      cart.ThisCustomer.CustomerCode,
                                                                      cartItem.Id,
                                                                      cart.ThisCustomer.ContactCode);

                            lstKitDetails.ForEach(item =>
                            {
                                kitItems.Add(new XElement("KITITEM", "({0}) {1}".FormatWith(
                                    Localization.ParseLocaleDecimal(item.Quantity, cart.ThisCustomer.LocaleSetting).TryParseInt().Value, item.Name).ToHtmlEncode()));
                            });

                            xmlcartItem.Add(kitItems);

                            hasRegistryItem = (cartItem.RegistryItemCode.HasValue && cartItem.GiftRegistryID.HasValue);
                            xmlcartItem.Add(new XElement("ISREGISTRYITEM", hasRegistryItem.ToStringLower()));
                            if (hasRegistryItem)
                            {
                                xmlcartItem.Add(new XElement("REGISTRYITEMQUANTITY", Localization.ParseLocaleDecimal(cartItem.RegistryItemQuantity.Value, cart.ThisCustomer.LocaleSetting)));
                                xmlcartItem.Add(new XElement("ISREGISTRYITEMHASCONFLICT", cartItem.HasRegistryItemQuantityConflict().ToStringLower()));
                            }

                            break;
                        #endregion

                        #region "STOCK"
                        default:

                            if (showLinkBack)
                            {
                                xmlcartItem.Add(new XElement("PRODUCTLINKHREF", productLinkHref));

                                hasRegistryItem = (cartItem.RegistryItemCode.HasValue && cartItem.GiftRegistryID.HasValue);
                                xmlcartItem.Add(new XElement("ISREGISTRYITEM", hasRegistryItem.ToStringLower()));
                                if (hasRegistryItem)
                                {
                                    xmlcartItem.Add(new XElement("REGISTRYITEMQUANTITY", Localization.ParseLocaleDecimal(cartItem.RegistryItemQuantity.Value, cart.ThisCustomer.LocaleSetting)));
                                    xmlcartItem.Add(new XElement("ISREGISTRYITEMHASCONFLICT", cartItem.HasRegistryItemQuantityConflict().ToStringLower()));
                                }
                            }

                            break;
                        #endregion
                    }

                    #region "Product Picture"

                    if (showPicsInCart)
                    {
                        xmlcartItem.Add(new XElement("SHOWPICSINCART", true));
                        var img = ProductImage.Locate("product", cartItem.ItemCounter, "icon");
                        if (null != img)
                        {
                            string seTitle = "";
                            string seAltText = "";
                            string itemCode = itemCode = InterpriseHelper.GetInventoryItemCode(cartItem.ItemCounter);
                            AppLogic.GetSEImageAttributes(itemCode, "ICON", AppLogic.GetLanguageCode(cart.ThisCustomer.LocaleSetting), ref seTitle, ref seAltText);

                            xmlcartItem.Add(new XElement("PRODUCTIMAGEPATH", img.src));
                            xmlcartItem.Add(new XElement("PRODUCTIMAGETITLE", seTitle));
                            xmlcartItem.Add(new XElement("PRODUCTIMAGEALT", seAltText));
                        }
                    }

                    #endregion

                    #region "Multiple Address"

                    xmlcartItem.Add(new XElement("HAS_MULTIPLE_ADDRESSES", cart.HasMultipleShippingAddresses().ToStringLower()));

                    /*************** multiple address **********************/
                    if (cart.HasMultipleShippingAddresses() && !cartItem.IsDownload)
                    {
                        xmlcartItem.Add(new XElement("ITEMISDOWNLOAD", false));
                        var adr = new Address();
                        bool includename = true;

                        string shippingName = string.Empty;
                        if (cartItem.m_ShippingAddressID == cart.ThisCustomer.PrimaryShippingAddressID && !cartItem.GiftRegistryID.HasValue)
                        {
                            adr.LoadByCustomer(cart.ThisCustomer, AddressTypes.Shipping, cartItem.m_ShippingAddressID, cartItem.GiftRegistryID);
                            shippingName = AppLogic.GetString("account.aspx.10");
                        }
                        else if (cartItem.m_ShippingAddressID == cart.ThisCustomer.PrimaryBillingAddressID)
                        {
                            adr.LoadByCustomer(cart.ThisCustomer, AddressTypes.Billing, cartItem.m_ShippingAddressID, cartItem.GiftRegistryID);
                            shippingName = AppLogic.GetString("account.aspx.8");
                        }
                        else
                        {
                            //specify the name if the shipping address is not primary
                            adr.LoadByCustomer(cart.ThisCustomer, AddressTypes.Shipping, cartItem.m_ShippingAddressID, cartItem.GiftRegistryID);
                            shippingName = adr.Name;
                            includename = false;
                        }

                        xmlcartItem.Add(new XElement("SHIP_ITEM_TO_VALUE", shippingName));
                        xmlcartItem.Add(new XElement("SHIP_ITEM_DETAIL", adr.DisplayString(false, false, includename, "")));
                        xmlcartItem.Add(new XElement("SHIPING_METHOD_VALUE", cartItem.m_ShippingMethod));
                    }

                    #endregion

                    #region "Stock Hints"

                    if (showStockHints && showShipDateInCart)
                    {
                        string localizedQty = Localization.ParseLocaleDecimal(cartItem.m_AllocatedQty, cart.ThisCustomer.LocaleSetting);
                        bool isCBNItem = (cartItem.IsCBN && !cartItem.CBNItemID.IsNullOrEmptyTrimmed()) ? true : false;
                        decimal freeStock = cartItem.FreeStock;
                        bool isDropShip = cartItem.IsDropShip;

                        xmlcartItem.Add(new XElement("ALLOCATEDQTY", localizedQty));
                        xmlcartItem.Add(new XElement("ISCBNITEM", isCBNItem));
                        xmlcartItem.Add(new XElement("ISDROPSHIP", isDropShip));

                        decimal ordered = cartItem.AssociatedLineItemRow.QuantityOrdered;
                        decimal totalReserved = cartItem.AssociatedLineItemRow.QuantityReserved;
                        decimal notAvailableQty = decimal.Zero;

                        if (!cartItem.IsCBN)
                        {
                            notAvailableQty = cartItem.AssociatedLineItemRow.QuantityOrdered - (cartItem.m_AllocatedQty + cartItem.AssociatedLineItemRow.QuantityReserved);
                            xmlcartItem.Add(new XElement("NOTAVAILABLEQTYWITHRESERVATION", Localization.ParseLocaleDecimal(notAvailableQty, cart.ThisCustomer.LocaleSetting)));
                        }
                        else
                        {
                            decimal computedQty = decimal.Zero;
                            computedQty = ordered - freeStock;

                            if (computedQty <= 0)
                            {
                                computedQty = ordered;
                            }
                            else
                            {
                                notAvailableQty = computedQty;
                                computedQty = ordered - notAvailableQty;
                            }
                            xmlcartItem.Add(new XElement("AVAILABLEQTY", Localization.ParseLocaleDecimal(computedQty, cart.ThisCustomer.LocaleSetting)));
                            xmlcartItem.Add(new XElement("NOTAVAILABLEQTY", Localization.ParseLocaleDecimal(notAvailableQty, cart.ThisCustomer.LocaleSetting)));
                        }

                        xmlcartItem.Add(new XElement("RESERVATION_COUNT", reserveCol.Count));
                        var reservationItems = reserveCol
                                .Select(r => new XElement("RESERVATIONITEM",
                                        new XElement("RESERVE_ITEMCODE", r.ItemCode),
                                        new XElement("RESERVE_SHIPDATE", r.ShipDate.ToShortDateString()),
                                        new XElement("RESERVE_QTY", Localization.ParseLocaleDecimal(r.QtyReserved, cart.ThisCustomer.LocaleSetting))));
                        xmlcartItem.Add(reservationItems);
                    }

                    #endregion

                    #region "Unit Measure"

                    if (!hideUnitMeasure)
                    {
                        xmlcartItem.Add(new XElement("NOT_HIDE_UNIT_MEASURE", true));
                        //var availableUnitMeasures = AppLogic.GetAvailableUnitMesure(cart.ThisCustomer.CustomerCode, cartItem.ItemCode, AppLogic.AppConfigBool("ShowInventoryFromAllWarehouses"), cart.ThisCustomer.IsNotRegistered);
                        var availableUnitMeasures = ProductPricePerUnitMeasure.GetAll(cartItem.ItemCode, cart.ThisCustomer, false,false,true);

                        if (availableUnitMeasures.Count > 1)
                        {
                            xmlcartItem.Add(new XElement("AVAILABLEUNITMESSURE_GREATER_ONE", true));
                            xmlcartItem.Add(new XElement("MULTIPLE_UNITMEASURECODENAME", "UnitMeasureCode_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                            xmlcartItem.Add(new XElement("MULTIPLE_UNITMEASURECODEID", "UnitMeasureCode_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));

                            foreach (var unitMeasureValue in availableUnitMeasures)
                            {
                                var unitmeasures = new XElement("UNITMEASSURE_ITEM");
                                xmlcartItem.Add(unitmeasures);
                                bool isCurrentUnitMeasureTheOneSelectedInProductPage = cartItem.UnitMeasureCode.Equals(unitMeasureValue.code, StringComparison.InvariantCultureIgnoreCase);
                                unitmeasures.Add(new XElement("VALUE", unitMeasureValue.code.ToHtmlEncode()));
                                unitmeasures.Add(new XElement("TEXT", unitMeasureValue.description));
                                unitmeasures.Add(new XElement("UNITMEASUREQUANTITY", unitMeasureValue.unitMeasureQuantity));
                                unitmeasures.Add(new XElement("SELECTED", isCurrentUnitMeasureTheOneSelectedInProductPage));
                                if (isCurrentUnitMeasureTheOneSelectedInProductPage)
                                {
                                    xmlcartItem.Add(new XElement("UNITMEASURECODESPANDISPLAY", unitMeasureValue.code.ToHtmlEncode()));
                                }
                            }
                        }
                        else
                        {
                            xmlcartItem.Add(new XElement("AVAILABLEUNITMESSURE_GREATER_ONE", false));
                            xmlcartItem.Add(new XElement("UNITMEASURECODEID", "UnitMeasureCode_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                            xmlcartItem.Add(new XElement("UNITMEASURECODENAME", "UnitMeasureCode_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                            xmlcartItem.Add(new XElement("UNITMEASURECODEVALUE", cartItem.UnitMeasureCode.ToHtmlEncode()));
                            xmlcartItem.Add(new XElement("UNITMEASURECODESPANDISPLAY", availableUnitMeasures[0].code.ToHtmlEncode()));
                        }
                    }

                    #endregion

                    #region "Input Quantities and Delete"
                    if ((cart.ThisCustomer.CostCenter != "Smartbag-PREMIUM APG" && cart.ThisCustomer.CostCenter != "Smartbag-PREMIUM CUE" && cart.ThisCustomer.CostCenter != "Smartbag-PREMIUM DION LEE") || cart.ThisCustomer.JobRole != "Admin")
                    {
                        xmlcartItem.Add(new XElement("ISRESTRICTEDQUANTITIES", (cartItem.RestrictedQuantities.Count > 0)));
                        if (cartItem.RestrictedQuantities.Count > 0)
                        {
                            xmlcartItem.Add(new XElement("QUANTITYLISTID", cartItem.m_ShoppingCartRecordID));
                            xmlcartItem.Add(cartItem.RestrictedQuantities
                                        .Select(r => new XElement("RESTRICTEDQUANTITIES",
                                                new XElement("QTY", r),
                                                new XElement("SELECTED", r == cartItem.m_Quantity))));
                        }
                        else
                        {
                            xmlcartItem.Add(new XElement("INPUTQUANTITYID", "Quantity_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                        }
                    }
                    else
                    {
                        xmlcartItem.Add(new XElement("ISRESTRICTEDQUANTITIES", false));
                        xmlcartItem.Add(new XElement("INPUTQUANTITYID", "Quantity_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                    }
                    xmlcartItem.Add(new XElement("INPUTQUANTITYVALUE", Localization.ParseLocaleDecimal(cartItem.m_Quantity, cart.ThisCustomer.LocaleSetting)));

                    if (AppLogic.AppConfigBool("ShowCartDeleteItemButton"))
                    {
                        xmlcartItem.Add(new XElement("SHOWCARTDELETEITEMBUTTON", true));
                    }

                    decimal minimumOrderQty = cartItem.m_MinimumQuantity / cartItem.UnitMeasureQty;

                    if (!AppLogic.IsAllowFractional)
                    {
                        minimumOrderQty = Math.Ceiling(minimumOrderQty);
                    }

                    xmlcartItem.Add(new XElement("MINORDERQUANTITYID", "MinOrderQuantity_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                    xmlcartItem.Add(new XElement("MINORDERQUANTITYNAME", "MinOrderQuantity_{0}".FormatWith(cartItem.m_ShoppingCartRecordID)));
                    xmlcartItem.Add(new XElement("MINORDERQUANTITYVALUE", minimumOrderQty));

                    #endregion

                    #region "Subtotal"

                    var lineItemRow = cartItem.AssociatedLineItemRow;

                    //original price (no discount and should be tax exclusive)
                    decimal originalPrice = cartItem.Price.ToCustomerRoundedCurrency();
                    if (vatenabled && vatInclusive)
                    {
                        originalPrice = (cartItem.Price - cartItem.TaxRate).ToCustomerRoundedCurrency();
                    }

                    decimal extPrice = lineItemRow.ExtPriceRate;
                    //added logic to display the non discounted price
                    if (isCouponTypeOrders)
                    {
                        extPrice = originalPrice;
                    }

                    decimal vat = Decimal.Zero;
                    var itemTaxDetail = cart.SalesOrderDataset
                                            .TransactionItemTaxDetailView
                                            .FirstOrDefault(i => i.ItemCode == cartItem.ItemCode && i.LineNum == lineItemRow.LineNum);
                    if (itemTaxDetail != null) { vat = itemTaxDetail.TaxAmount.ToCustomerRoundedCurrency(); }

                    //added: reason -> to display sales price (no deductions) on order summary
                    xmlcartItem.Add(new XElement("SALES_PRICE", (this.PageRenderType == RenderType.ShoppingCart) ? cartItem.AssociatedLineItemRow.NetPriceRate.ToCustomerCurrency() : cartItem.AssociatedLineItemRow.SalesPriceRate.ToCustomerCurrency()));

                    //add this if there are coupon
                    if (!coupon.IsNullOrEmptyTrimmed())
                    {
                        //original amount (non discounted) - discounted extended price
                        decimal discountAmount = originalPrice - lineItemRow.ExtPriceRate;
                        string discountType = cart.SalesOrderDataset.CustomerSalesOrderView[0][cart.SalesOrderDataset.CustomerSalesOrderView.CouponDiscountTypeColumn.ColumnName].ToString();
                        if (discountType == Interprise.Framework.Customer.Shared.Const.COUPON_DISCOUNTTYPE_AMOUNT)
                        {
                            discountAmount = lineItemRow.CouponDiscountRate;
                        }
                        appliedCouponDiscount += discountAmount;
                        xmlcartItem.Add(new XElement("COUPON_DISCOUNT_TYPE", discountType));
                        xmlcartItem.Add(new XElement("DISCOUNT_COUPON_RATE_VALUE", discountAmount.ToCustomerCurrency()));
                        xmlcartItem.Add(new XElement("DISCOUNT_COUPON_PERCENTAGE", string.Format("{0}%", Localization.ParseLocaleDecimal(lineItemRow.CouponDiscountRate, cart.ThisCustomer.LocaleSetting))));
                    }

                    if (vatenabled && vatInclusive)
                    {
                        extPrice += vat;
                    }

                    //computedSubtotal = sum of the item extended prices 
                    computedSubtotal += extPrice;

                    xmlcartItem.Add(new XElement("PRICEFORMATTED", extPrice.ToCustomerCurrency()));

                    if (vatenabled)
                    {
                        xmlcartItem.Add(new XElement("TAX_RATE_VALUE", vat.ToCustomerCurrency()));
                    }

                    #endregion
                }

                //TOTAL COMPUTATION ------------------------------------------------------------------------------------//

                decimal subTotal = cart.SalesOrderDataset.CustomerSalesOrderView[0].SubTotalRate;

                if (isCouponTypeOrders)
                {
                    //if the condition below is satified, computedSubtotal will be used to reflect the applied coupon discount 
                    if (subTotal.Equals(Decimal.Zero) && appliedCouponDiscount > subTotal)
                    {
                        appliedCouponDiscount = computedSubtotal;
                    }
                    subTotal += appliedCouponDiscount;

                    //total coupon discount applied to the web order
                    root.Add(new XElement("APPLIED_COUPON_DISCOUNT", appliedCouponDiscount.ToCustomerCurrency()));
                }

                decimal tax = Decimal.Zero;
                if ((PageRenderType != RenderType.ShoppingCart && Customer.Current.IsNotRegistered) || Customer.Current.IsRegistered)
                {
                    tax = cart.SalesOrderDataset.CustomerSalesOrderView[0].TaxRate;
                }

                decimal freightTax = cart.SalesOrderDataset.CustomerSalesOrderView[0].FreightTaxRate;
                //Set Freight to zero for Portal website
                if (CommonLogic.GetWebsiteType() == WebsiteType.Company)
                {
                    freightTax = 0;
                }
                //breaking down the taxes to display the freight tax
                tax -= freightTax;

                decimal total = subTotal + tax;

                if (vatenabled && vatInclusive)
                {
                    subTotal += tax;
                }

                string subTotalFormatted = subTotal.ToCustomerCurrency();
                //if (PageRenderType == RenderType.Payment || PageRenderType == RenderType.Review || PageRenderType == RenderType.Company)
                //{
                    decimal freight = cart.SalesOrderDataset.CustomerSalesOrderView[0].FreightRate;

                    if (isFreeShipping)
                    {
                        freight = 0;
                        freightTax = 0;
                    }

                    if (vatenabled && vatInclusive)
                    {
                        freight += freightTax;
                        total += freight;
                    }
                    else
                    {
                        total += freight;
                        total += freightTax;
                    }

                    root.Add(new XElement("FREIGHT", freight.ToCustomerCurrency()));
                    root.Add(new XElement("FREIGHTTAX", freightTax.ToCustomerCurrency()));
                //}

                if (isCouponTypeOrders)
                {
                    total -= appliedCouponDiscount;
                }

                root.Add(new XElement("SUBTOTAL_VALUE", subTotalFormatted));

                #region "Gift Card / Gift Certificate"
                if (appConfigService.GiftCodeEnabled)
                {
                    decimal gCardCredit = cart.GiftCardsTotalAmountApplied - this.GiftCardAllocated;
                    decimal gCertCredit = cart.GiftCertsTotalAmountApplied - this.GiftCertificateAllocated;
                    decimal allocate = Decimal.Zero;

                    if (!cart.ThisCustomer.ThisCustomerSession[DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS].IsNullOrEmptyTrimmed())
                    {
                        string selectedPaymentTerm = cart.ThisCustomer.PaymentTermCode.ToUpperInvariant();
                        if (Convert.ToBoolean(cart.ThisCustomer.ThisCustomerSession[DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS]) &&
                           (selectedPaymentTerm.Equals("REQUEST QUOTE") || selectedPaymentTerm.Equals("PURCHASE ORDER")))
                        {
                            cart.ClearGiftCodes();
                        }
                    }

                    if (gCardCredit > Decimal.Zero && total > Decimal.Zero && !cart.HasAppliedInvalidGiftCard())
                    {
                        allocate = (gCardCredit > total) ? total : gCardCredit;
                        total -= allocate;
                        cart.GiftCardsTotalCreditAllocated = allocate;
                        root.Add(new XElement("APPLIED_CREDIT_GIFT_CARD", currencyService.FormatCurrency(allocate)));
                    }

                    if (gCertCredit > Decimal.Zero && total > Decimal.Zero && !cart.HasAppliedInvalidGiftCert())
                    {
                        allocate = (gCertCredit > total) ? total : gCertCredit;
                        total -= allocate;
                        cart.GiftCertsTotalCreditAllocated = allocate;
                        root.Add(new XElement("APPLIED_CREDIT_GIFT_CERTIFICATE", currencyService.FormatCurrency(allocate)));
                    }
                }
                #endregion

                #region "LoyaltyPoints
                if (appConfigService.LoyaltyPointsEnabled)
                {
                    decimal loyaltyPointsCredit = cart.LoyaltyPointsAmountApplied - this.LoyaltyPointsCreditAllocated;
                    decimal allocate = Decimal.Zero;

                    if (!cart.ThisCustomer.ThisCustomerSession[DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS].IsNullOrEmptyTrimmed())
                    {
                        string selectedPaymentTerm = cart.ThisCustomer.PaymentTermCode.ToUpperInvariant();
                        if (Convert.ToBoolean(cart.ThisCustomer.ThisCustomerSession[DomainConstants.CLEAR_OTHER_PAYMENT_OPTIONS]) &&
                           (selectedPaymentTerm.Equals("REQUEST QUOTE") || selectedPaymentTerm.Equals("PURCHASE ORDER")))
                        {
                            cart.ClearLoyaltyPoints();
                        }
                    }

                    if (loyaltyPointsCredit > Decimal.Zero && total > Decimal.Zero && !cart.HasAppliedInvalidLoyaltyPoints())
                    {
                        allocate = (loyaltyPointsCredit > total) ? total : loyaltyPointsCredit;
                        total -= allocate;
                        cart.LoyaltyPointsCreditAllocated = allocate;
                        root.Add(new XElement("APPLIED_LOYALTYPOINTS_AMOUNT", currencyService.FormatCurrency(allocate)));
                    }
                }
                #endregion

                #region "Credit Memo"
                if (appConfigService.CreditRedemptionIsEnabled)
                {
                    decimal creditMemosCredit = cart.CreditMemosTotalAmountApplied - this.CreditMemosCreditAllocated;
                    decimal allocate = Decimal.Zero;

                    if (creditMemosCredit > Decimal.Zero && total > Decimal.Zero && !cart.HasAppliedInvalidCreditMemo())
                    {
                        allocate = (creditMemosCredit > total) ? total : creditMemosCredit;
                        total -= allocate;
                        cart.LoyaltyPointsCreditAllocated = allocate;
                        root.Add(new XElement("APPLIED_CREDITMEMOS_AMOUNT", currencyService.FormatCurrency(allocate)));
                    }
                }
                #endregion

                root.Add(new XElement("TAX_RATE_VALUE", tax.ToCustomerCurrency()));
                root.Add(new XElement("TOTAL", total.ToCustomerCurrency()));

                string html = string.Empty;

                if (this.CustomXmlPackage.IsNullOrEmptyTrimmed())
                {
                    html = new XmlPackage2("page.shoppingcart_items_template.xml.config", root).TransformString();
                }
                else
                {
                    html = new XmlPackage2(this.CustomXmlPackage, root).TransformString();
                }

                output.Append(html);
            }
        }

        #endregion



    }
}
