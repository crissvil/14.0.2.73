﻿using System;
using System.Data;
using System.Data.SqlClient;
using Interprise.Extendable.Base.Facade.Customer.CreditCardGateway;
using Interprise.Facade.Base;
using Interprise.Facade.Customer;
using Interprise.Framework.Base.DatasetComponent;
using Interprise.Framework.Base.Shared;
using Interprise.Framework.Customer.DatasetGateway;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using Interprise.Framework.Base;

namespace InterpriseSuiteEcommerceCommon.InterpriseIntegration
{
    public class SimplePayment 
    {
        SalesOrderDatasetGateway _gatewaySalesOrderDataset;
        SalesOrderFacade _facadeSalesOrder;
        Customer _customer;

        InvoiceDatasetGateway _gatewayInvoiceDataset;
        InvoiceFacade _facadeInvoice;

        DateTime _receiptDate;
        decimal _receiptAmount;

        public SimplePayment() { }

        #region Properties

        private bool IsSalesOrderDetailBuilt
        {
            get { return _gatewaySalesOrderDataset != null && _facadeSalesOrder != null; }
        }

        private bool IsInvoiceDetailBuilt
        {
            get { return _gatewayInvoiceDataset != null && _facadeInvoice != null; }
        }

        public DateTime ReceiptDate
        {
            get { return _receiptDate; }
            set { _receiptDate = value; }
        }

        public decimal ReceiptAmount
        {
            get { return _receiptAmount; }
            set { _receiptAmount = value; }
        }

        public InvoiceDatasetGateway InvoiceDateSet
        {
            get { return _gatewayInvoiceDataset; }
        }

        public InvoiceFacade InvoiceFacade
        {
            get { return _facadeInvoice; }
        }

        public SalesOrderFacade SaleOrderFacade
        {
            get { return _facadeSalesOrder; }
        }

        #endregion

        #region Load & Save Invoice

        public void LoadInvoice(string invoiceCode)
        {

            int retries = 3;

            for (int ctr = 0; ctr < retries; ctr++)
            {
                try
                {
                    string[][] paramset = new string[][] {  
                    new string[] {"@InvoiceCode", invoiceCode}, 
                    new string[] {"@TransactionType", Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice.ToString()},   
                    };

                    _gatewayInvoiceDataset = new InvoiceDatasetGateway();
                    _facadeInvoice = new InvoiceFacade(_gatewayInvoiceDataset);

                    // For POS created transaction
                    _gatewayInvoiceDataset.CustomerInvoiceView.SaveCounterIDColumn.AllowDBNull = true;
                    _facadeInvoice.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice;
                    string[][] paramSet = new string[][] { new string[] { "@InvoiceCode", invoiceCode } };

                    //unified load of data to avoid PK related error during update.
                    _facadeInvoice.LoadDataSet(Interprise.Framework.Base.Shared.StoredProcedures.READCUSTOMERINVOICEUNIFIED,
                    new string[] {_gatewayInvoiceDataset.CustomerInvoiceView.TableName, _gatewayInvoiceDataset.CustomerInvoiceDetailView.TableName, _gatewayInvoiceDataset.TransactionTaxDetailView.TableName, 
                                      _gatewayInvoiceDataset.TransactionItemTaxDetailView.TableName, _gatewayInvoiceDataset.CustomerCreditAllocationView.TableName, _gatewayInvoiceDataset.CustomerCreditView.TableName,
                                      _gatewayInvoiceDataset.CustomerItemKitDetailView.TableName, _gatewayInvoiceDataset.CustomerSalesRepCommissionView.TableName, _gatewayInvoiceDataset.TransactionTaxSummary.TableName,
                                      _gatewayInvoiceDataset.CustomerInvoiceSerialLotNumbers.TableName, _gatewayInvoiceDataset.CustomerInvoiceKitSerialLotNumbers.TableName},
                    paramSet,
                    Interprise.Framework.Base.Shared.Enum.ClearType.None,
                    Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                    _facadeInvoice.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice;

                    break;
                }
                catch
                {
                    _gatewayInvoiceDataset.Dispose();
                    _facadeInvoice.Dispose();

                    _gatewayInvoiceDataset = null;
                    _facadeInvoice = null;

                    // if this is the last retry, bubble up the error
                    if (ctr + 1 == retries)
                    {
                        throw;
                    }
                }
            }
        }

        public void SaveInvoice()
        {
            //Retry saving if concurrency error was encountered otherwise, simply throw the exception.

            int nooftries = 0;
            while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
            {
                try
                {
                    string[] relatedTables = _facadeInvoice.get_RelatedTables(_facadeInvoice.TransactionType);
                    string[][] commands = _facadeInvoice.CreateParameterSet(relatedTables);
                    _facadeInvoice.UpdateDataSet(commands, _facadeInvoice.TransactionType, String.Empty, false);

                    break;
                }
                catch (DBConcurrencyException)
                {
                    nooftries += 1;
                    SaveInvoice();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        public InvoiceFacade GetInvoiceFacade()
        {
            return _facadeInvoice;
        }


        #endregion

        #region Invoice

        public string PlacePayment(Gateway usingThisGateway, Address billingAddress, Address shippingAddress, ref string documentCode,
            ref string receiptCode, GatewayResponse response, bool applyPayment, bool is3DsecondAuth, decimal amountToPay, bool isInvoice){
            
            _customer = Customer.Current;

            string status = AppLogic.ro_OK;

            documentCode = documentCode.IsNullOrEmptyTrimmed() ? _facadeSalesOrder.GenerateDocumentCode(_facadeInvoice.TransactionType.ToString()) : documentCode;
            receiptCode = receiptCode.IsNullOrEmptyTrimmed() ? SimpleFacade.Instance.GenerateDocumentCode(Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerCardPayment.ToString()) : receiptCode;

            if (AppLogic.AppConfigBool("MaxMind.Enabled"))
            {

                if (ValidateOrderFraud(documentCode, billingAddress, shippingAddress, 0))
                {
                    return "MAXMIND FRAUD CHECK FAILED";
                }
            }

            decimal getExchangeRate = isInvoice ? _facadeInvoice.GetExchangerate(_customer.CurrencyCode) : _facadeSalesOrder.GetExchangerate(_customer.CurrencyCode);

            if (usingThisGateway != null)
            {
                decimal total = GetOrderTotal(true);
                string cv2 = AppLogic.GetCardExtraCodeFromSession(_customer);

                response = usingThisGateway.ProcessCard(documentCode,
                            _customer,
                            total,
                            receiptCode,
                            "AUTH/CAPTURE",
                            billingAddress,
                            cv2,
                            shippingAddress,
                            string.Empty,
                            string.Empty,
                            string.Empty);

                cv2 = "1111111";

                status = response.Status;

                if (status == AppLogic.ro_3DSecure)
                {
                    return response.Status;
                }
                else if (status != AppLogic.ro_OK)
                {
                    // record failed transaction
                    var transactionFailed = FailedTransaction.New();
                    transactionFailed.CustomerCode = _customer.CustomerCode;
                    transactionFailed.SalesOrderCode = documentCode;
                    transactionFailed.PaymentGateway = response.Gateway;
                    transactionFailed.PaymentMethod = DomainConstants.PAYMENT_METHOD_CREDITCARD;
                    transactionFailed.TransactionCommand = response.TransactionCommandOut;
                    transactionFailed.TransactionResult = response.TransactionResponse;
                    transactionFailed.IPAddress = _customer.LastIPAddress;
                    transactionFailed.IsFailed = true;

                    transactionFailed.Record();

                    if (AppLogic.AppConfigBool("ShowGatewayError"))
                    {
                        return AppLogic.ro_INTERPRISE_GATEWAY_AUTHORIZATION_FAILED;
                    }
                    else
                    {
                        return response.Status;
                    }
                }
            }

            if (isInvoice)
            {
                _gatewayInvoiceDataset.CustomerInvoiceView[0].PaymentTermCode = _customer.PaymentTermCode;
                _facadeInvoice.UpdateDataSet();
            }
            else
            {
                _gatewaySalesOrderDataset.CustomerSalesOrderView[0].PaymentTermCode = _customer.PaymentTermCode;
                _facadeSalesOrder.UpdateDataSet();
            }

            if(applyPayment){

                status = ProcessInvoicePayment(billingAddress, response, receiptCode, documentCode, is3DsecondAuth, amountToPay, isInvoice, _customer.LastIPAddress);
            }

            return status;

        }

        public string ProcessInvoicePayment(Address billingAddress, GatewayResponse withGatewayAuthResponse, string receiptCode, string documentCode, bool is3DsecondAuth, decimal amountToPay, bool isInvoice, string ipAddress)
        {
            string status = AppLogic.ro_OK;

            #region Process Receipt

            var term = PaymentTermDTO.Find(_customer.PaymentTermCode);
            var paymentTermCode = term.PaymentTermCode;

            bool isNotRequestAndQoute = (false == "REQUEST QUOTE".Equals(paymentTermCode) && false == "PURCHASE ORDER".Equals(paymentTermCode));
            bool isCreditCard = (paymentTermCode == DomainConstants.PAYMENT_METHOD_CREDITCARD);

            if (paymentTermCode != AppLogic.AppConfig("PaymentTermCodeZeroDollarOrder") && isNotRequestAndQoute && isCreditCard)
            {
                var gatewayReceiptDataset = new ReceiptDatasetGateway();
                var facadeReceipt = new ReceiptFacade(gatewayReceiptDataset);

                var facadeCustomerReceiptList = new ListControlFacade();
                var datasetCustomerReceipt = new DataSet();
                var customerReceiptBaseDataset = new BaseDataset();

                datasetCustomerReceipt = GetCustomerReceipt(documentCode, ref customerReceiptBaseDataset, isInvoice);

                string cardExtraCode = AppLogic.GetCardExtraCodeFromSession(_customer);

                var rowCustomer = customerReceiptBaseDataset.Tables["CustomerActiveCustomersView"].Rows[0];
                facadeReceipt.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt;


                if (is3DsecondAuth)
                {
                    var ds = Interprise.Facade.Base.SimpleFacade.Instance.LoadDataSet(CommandType.StoredProcedure,
                                                                                                       "EcommerceGetCustomerPayment",
                                                                                                        new string[] { Interprise.Framework.Customer.Shared.Const.CUSTOMERTRANSACTIONRECEIPTVIEW_TABLE, 
                                                                                                          Const.CUSTOMERPAYMENT_TABLE, Const.CUSTOMERCARDPAYMENT_TABLE, Const.PAYMENTMETHODVIEW_TABLE, 
                                                                                                          "DefaultAccount", "HasReserved", Interprise.Framework.Customer.Shared.Const.CUSTOMERTRADINGINFOVIEW_TABLE },
                                                                                                        new string[][] { new string[] {"@DocumentCode", documentCode},
                                                                                                            new string[] {"@CustomerCode", _customer.CustomerCode}},
                                                                                                        Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                    if (ds.Tables.Contains(Interprise.Framework.Customer.Shared.Const.CUSTOMERTRANSACTIONRECEIPTVIEW_TABLE))
                    {
                        gatewayReceiptDataset.Merge(ds.Tables[Interprise.Framework.Customer.Shared.Const.CUSTOMERTRANSACTIONRECEIPTVIEW_TABLE]);
                    }

                    if (isInvoice)
                    {
                        _facadeInvoice.IsTransactionReceiptLoaded = true;
                    }
                    else
                    {
                        _facadeSalesOrder.IsTransactionReceiptLoaded = true;
                    }

                    if (ds.Tables.Contains("DefaultAccount") && ds.Tables["DefaultAccount"].Rows.Count > 0 & ds.Tables.Contains("HasReserved"))
                    {
                        facadeReceipt.SetSettings(Convert.ToString(ds.Tables["DefaultAccount"].Rows[0][0]), ds.Tables["HasReserved"].Rows.Count > 0);
                    }

                    gatewayReceiptDataset.PaymentMethodView.Clear();
                    gatewayReceiptDataset.CustomerPayment.Clear();
                    gatewayReceiptDataset.CustomerCardPayment.Clear();

                    facadeReceipt.RemovePaymentTableRelation();

                    if (ds.Tables.Contains(Interprise.Framework.Base.Shared.Const.CUSTOMERPAYMENT_TABLE))
                    {
                        gatewayReceiptDataset.Merge(ds.Tables[Interprise.Framework.Base.Shared.Const.CUSTOMERPAYMENT_TABLE]);
                    }
                    if (ds.Tables.Contains(Interprise.Framework.Base.Shared.Const.CUSTOMERCARDPAYMENT_TABLE))
                    {
                        gatewayReceiptDataset.Merge(ds.Tables[Interprise.Framework.Base.Shared.Const.CUSTOMERCARDPAYMENT_TABLE]);
                    }
                    if (ds.Tables.Contains(Const.PAYMENTMETHODVIEW_TABLE))
                    {
                        gatewayReceiptDataset.Merge(ds.Tables[Const.PAYMENTMETHODVIEW_TABLE]);
                    }

                    if (!facadeReceipt.DisableRelationInitialization)
                    {
                        facadeReceipt.InitializePaymentTableRelation();
                    }

                    facadeReceipt.RestoreCV();

                }
                else
                {
                    paymentTermCode = isInvoice ? _gatewayInvoiceDataset.CustomerInvoiceView[0].PaymentTermCode : _gatewaySalesOrderDataset.CustomerSalesOrderView[0].PaymentTermCode;
                    documentCode = isInvoice ? _gatewayInvoiceDataset.CustomerInvoiceView[0].InvoiceCode : _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode;
                    string contactCode = isInvoice ? _gatewayInvoiceDataset.CustomerInvoiceView[0].ContactCode : _gatewaySalesOrderDataset.CustomerSalesOrderView[0].ContactCode;

                    facadeReceipt.AddReceipt(new DataRow[] { rowCustomer }, Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt, true, amountToPay, paymentTermCode, documentCode, contactCode);

                }

                facadeReceipt.AssignCreditCardTransactionType(gatewayReceiptDataset.PaymentMethodView[0], Interprise.Framework.Base.Shared.Enum.CreditCardTransaction.AuthorizeOnly);

                gatewayReceiptDataset.CustomerCardPayment[0].CardPaymentCode = receiptCode;

                var rowPayMethod = gatewayReceiptDataset.PaymentMethodView[0];

                if (_customer.IsNotRegistered)
                {
                    // Set default value
                    facadeReceipt.AssignPaymentMethodDefaults(rowPayMethod, paymentTermCode);

                    #region Manually Assign Credit Details
                    rowPayMethod.BeginEdit();
                    rowPayMethod.IsCardOnFile = false;

                    string isDeposited = Boolean.TrueString;
                    isDeposited = facadeReceipt.GetField("IsDeposited",
                                                         Interprise.Framework.Base.Shared.Const.SYSTEMPAYMENTTYPE_TABLE,
                                                         String.Format("{0}='{1}'", Interprise.Framework.Base.Shared.Const.SYSTEMPAYMENTTYPE_PAYMENTTYPECODE_COLUMN,
                                                         Convert.ToString(Interprise.Framework.Base.Shared.Common.IsNull(gatewayReceiptDataset.PaymentMethodView[0][gatewayReceiptDataset.PaymentMethodView.PaymentTypeCodeColumn.ColumnName], String.Empty))));


                    string unDeposited = Interprise.Framework.Base.Shared.Const.PAYMENT_ACCOUNT_UNDEPOSITED;
                    string paymentAccount = Interprise.Framework.Base.Shared.Const.PAYMENT_ACCOUNT;

                    rowPayMethod.Account = (isDeposited == null || String.Compare(isDeposited, Boolean.FalseString, true) == 0) ? unDeposited : paymentAccount;

                    // Reset credit card value for anonymous
                    if (facadeReceipt.IsInterpriseGatewayv2 || facadeReceipt.IsInterpriseGateway)
                    {
                        rowPayMethod.InterpriseGatewayRefNo = 0;
                        rowPayMethod.CreditCardOnFile = String.Empty;
                    }
                    
                    rowPayMethod.CreditCardCode = String.Empty;
                    rowPayMethod.CreditCardSalt = billingAddress.CardNumberSalt;
                    rowPayMethod.CreditCardEmail = billingAddress.EMail;
                    rowPayMethod.CreditCardIV = billingAddress.CardNumberIV;
                    rowPayMethod.CreditCardName = billingAddress.CardName;

                    // NOTE:
                    // IS Always assume that the credit card being passed here is encrypted
                    // thus the need for the next line below
                    rowPayMethod.CreditCard = facadeReceipt.EncryptCardNumber(billingAddress.CardNumber, rowPayMethod);

                    rowPayMethod.CreditCardAddress = billingAddress.Address1;
                    rowPayMethod.CreditCardCity = billingAddress.City;
                    rowPayMethod.CreditCardState = billingAddress.State;
                    rowPayMethod.CreditCardPostalCode = billingAddress.PostalCode;
                    rowPayMethod.CreditCardCountry = billingAddress.Country;

                    rowPayMethod.CreditCardExpMon = InterpriseHelper.ToInterpriseExpMonth(billingAddress.CardExpirationMonth);
                    rowPayMethod.CreditCardExpYear = billingAddress.CardExpirationYear;

                    if (AppLogic.AppConfigBool("ShowCardStartDateFields"))
                    {
                        rowPayMethod.CreditCardStartMon = InterpriseHelper.ToInterpriseExpMonth(billingAddress.CardStartMonth);
                        rowPayMethod.CreditCardStartYear = billingAddress.CardStartYear;

                        // same reason for credit card applies here
                        rowPayMethod.CreditCardIssueNumber = facadeReceipt.EncryptCardIssueNumber(billingAddress.CardIssueNumber, rowPayMethod);
                    }

                    rowPayMethod.CreditCardTelephone = billingAddress.Phone;
                    rowPayMethod.CreditCardTelephoneExtension = String.Empty;
                    rowPayMethod.CreditCardType = billingAddress.CardType;
                    rowPayMethod.CreditCardTypeDescription = billingAddress.CardType;

                    rowPayMethod.EndEdit();

                    #endregion

                }

                rowPayMethod.AmountPaidRate = amountToPay;

                string[] receiptRelatedTables = facadeReceipt.get_RelatedTables(Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt);
                string[][] receiptCommands = facadeReceipt.CreateParameterSet(receiptRelatedTables);

                facadeReceipt.UpdateDataSet(receiptCommands,
                                            Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt,
                                            String.Empty,
                                            false);

                if (!String.IsNullOrEmpty(Convert.ToString(Interprise.Framework.Base.Shared.Common.IsNull(rowPayMethod[gatewayReceiptDataset.PaymentMethodView.CreditCardColumn.ColumnName]))))
                {
                    if (facadeReceipt.IsInterpriseGateway || facadeReceipt.IsInterpriseGatewayv2)
                    {
                        rowPayMethod.CreditCardOnFile = Interprise.Framework.Base.Shared.Common.MaskCardNumber(facadeReceipt.DecryptCardNumber(Convert.ToString(Interprise.Framework.Base.Shared.Common.IsNull(rowPayMethod[gatewayReceiptDataset.PaymentMethodView.CreditCardColumn.ColumnName])), rowPayMethod));
                    }
                }

                rowPayMethod.CreditCardSalt = _customer.ThisCustomerSession["CardNumberSalt"];
                rowPayMethod.CreditCardIV = _customer.ThisCustomerSession["CardNumberIV"];
                rowPayMethod.CreditCard = _customer.ThisCustomerSession["CardNumber"];

                rowPayMethod.CreditCardCV = billingAddress.CardIssueNumber;

                // this will assign the Card On Name to Credit Card Gateway.
                rowPayMethod.CreditCardName = billingAddress.CardName;


                rowPayMethod.CreditCardCV = cardExtraCode;
                rowPayMethod.CreditCardTransactionType = "Auth";
                rowPayMethod.CustomerIPAddress = ipAddress;

                string gatewayMessage = String.Empty;
                bool approved = false;

                if (withGatewayAuthResponse != null)
                {

                    approved = true;
                    AttachGatewayAuthorization(gatewayReceiptDataset, rowPayMethod, withGatewayAuthResponse, receiptCode, isInvoice);
                  
                    // Set the next IS gateways that will process the capture part
                    // Since now we're just doing AUTH
                    if (!_customer.ThisCustomerSession["paypalfrom"].IsNullOrEmptyTrimmed())
                    {
                        rowPayMethod.CreditCardGatewayAssemblyName = "Interprise.Presentation.Customer.PaymentGateway.PayPal";
                        rowPayMethod.CreditCardGatewayType = "Interprise.Presentation.Customer.PaymentGateway.PayPal.PayPalGatewayControl";
                    }

                    facadeReceipt.UpdateDataSet(new string[][] { 
                            new string[] { gatewayReceiptDataset.CustomerCardPayment.TableName, 
                                "CreateCustomerCardPayment", 
                                "UpdateCustomerCardPayment", 
                                "DeleteCustomerCardPayment" }, 
                            new string[] { gatewayReceiptDataset.CustomerPayment.TableName, 
                                "CreateCustomerPayment", 
                                "UpdateCustomerPayment", 
                                "DeleteCustomerPayment" }, 
                            new string[] { gatewayReceiptDataset.PaymentMethodView.TableName, 
                                "CreatePaymentMethod", 
                                "UpdatePaymentMethod", 
                                "DeletePaymentMethod" }, 
                            new string[] { gatewayReceiptDataset.CustomerCCAuthResponse.TableName, 
                                "CreateCustomerCCAuthResponse", 
                                "UpdateCustomerCCAuthResponse", 
                                "DeleteCustomerCCAuthResponse" } },
                            facadeReceipt.TransactionType,
                            "Processed credit card payment.",
                            false);

                    status = AppLogic.ro_OK;
                }
                else
                {
                    string prefferredGateway = GetPreferredGateway();
                    
                    rowPayMethod.WebsiteCode = InterpriseHelper.ConfigInstance.WebSiteCode;

                    using (var facadeCard = new CreditCardAuthorizationFacade())
                    {
                       var gatewayInfo = ServiceFactory.GetInstance<IPaymentTermService>().GetGatewayInterface();

                        var cardGateway = facadeCard.GetGateway(gatewayInfo[0], gatewayInfo[1]);
                        if (null == cardGateway) throw new Exception("Could not load payment Gateway!!!");

                        rowPayMethod.CreditCardGatewayAssemblyName = cardGateway.CreditCardGatewayAssemblyName;
                        rowPayMethod.CreditCardGatewayType = cardGateway.CreditCardGatewayType;

                        facadeCard.TransactionType = facadeReceipt.TransactionType;

                        if (prefferredGateway == AppLogic.ro_PROTX)
                        {
                            facadeCard.IsEcommerce = true;
                            facadeCard.Is3DSecure = CommonLogic.StringInCommaDelimitedStringList(billingAddress.CardType, AppLogic.AppConfig("3DSECURE.CreditCardTypes"));
                            if (facadeCard.Is3DSecure && is3DsecondAuth)
                            {
                                facadeCard.Secure3DMD = _customer.ThisCustomerSession["3DSecure.MD"];
                                facadeCard.Secure3DPAReq = _customer.ThisCustomerSession["3Dsecure.PaRes"];
                            }
                        }

                        gatewayMessage = facadeCard.AuthorizeCreditCard(facadeReceipt, false, ref approved);

                        if (prefferredGateway == AppLogic.ro_PROTX && facadeCard.Is3DSecure && !is3DsecondAuth)
                        {
                            _customer.ThisCustomerSession["3DSecure.MD"] = facadeCard.Secure3DMD;
                            _customer.ThisCustomerSession["3DSecure.ACSUrl"] = facadeCard.Secure3DACSURL;
                            _customer.ThisCustomerSession["3DSecure.PAReq"] = facadeCard.Secure3DPAReq;
                            _customer.ThisCustomerSession["3DSecure.CustomerID"] = _customer.ContactGUID.ToString();
                            _customer.ThisCustomerSession["3DSecure.OrderNumber"] = documentCode;
                            _customer.ThisCustomerSession["3DSecure.XID"] = receiptCode;
                        }

                        cardGateway.Dispose();

                    }

                    if (!approved)
                    {
                        status = logFailedTransaction(gatewayMessage, billingAddress.CardType, prefferredGateway, is3DsecondAuth,
                                    facadeReceipt.GetLastCreditCardProcessMessage(), facadeReceipt.CurrentDataset.Tables["CustomerCCAuthResponse"].Rows[0]["GatewayResponseCode"].ToString(),
                                    facadeReceipt.CurrentDataset.Tables["CustomerCCAuthResponse"].Rows[0]["ResponseMsg"].ToString(), isInvoice);
                    }


                }

                var gatewayTransactionReceiptDataset = isInvoice ? _facadeInvoice.TransactionReceiptListFacade.CurrentDataset as TransactionReceiptDatasetGateway : _facadeSalesOrder.TransactionReceiptListFacade.CurrentDataset as TransactionReceiptDatasetGateway;
                var facadeTransactionReceipt = isInvoice ? _facadeInvoice.TransactionReceiptListFacade as TransactionReceiptFacade : _facadeSalesOrder.TransactionReceiptListFacade as TransactionReceiptFacade;
                int datasourceRowIndex = 0;

                #region Reserve and Allocate Receipt
                // Asssociate receipt with invoice or sales order

                documentCode = isInvoice ? _gatewayInvoiceDataset.CustomerInvoiceView[0].InvoiceCode : _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode;
                facadeTransactionReceipt.AssignTransactionReceipt(documentCode, facadeReceipt, ref datasourceRowIndex);

                // Manually merge the Customer Transaction Receipt table..

                if (isInvoice)
                {
                    _gatewayInvoiceDataset.Merge(gatewayTransactionReceiptDataset.CustomerTransactionReceiptView);
                }
                else
                {
                    _gatewaySalesOrderDataset.Merge(gatewayTransactionReceiptDataset.CustomerTransactionReceiptView);
                }

                DataRow[] dataRows = gatewayTransactionReceiptDataset.CustomerTransactionReceiptView.Select(
                String.Format("{0}='{1}' AND {2}='{3}'", gatewayTransactionReceiptDataset.CustomerTransactionReceiptView.DocumentCodeColumn.ColumnName,
                documentCode, gatewayTransactionReceiptDataset.CustomerTransactionReceiptView.ReceivableCodeColumn.ColumnName, receiptCode));

                if (dataRows.Length > 0)
                {
                    ReceiptDate = Convert.ToDateTime(dataRows[0][gatewayTransactionReceiptDataset.CustomerTransactionReceiptView.DatePaidColumn.ColumnName]);
                    ReceiptAmount = Convert.ToDecimal(dataRows[0][gatewayTransactionReceiptDataset.CustomerTransactionReceiptView.TotalAmountColumn.ColumnName]);
                }

                if (isInvoice)
                {
                    SaveInvoice();
                }
                else
                {
                    SaveSalesOrder();
                }

                // Do Not Allocate if posted
                if (isInvoice)
                {

                    if (!Convert.ToBoolean(_gatewayInvoiceDataset.CustomerInvoiceView[0].IsPosted))
                    {
                        facadeTransactionReceipt.ReserveAndAllocateReceipt(_gatewayInvoiceDataset, null, Interprise.Framework.Base.Shared.Const.CUSTOMER_INVOICE);
                    }

                }
                else
                {
                    // Manually merge the Customer Transaction Receipt table..
                    _gatewaySalesOrderDataset.Merge(gatewayTransactionReceiptDataset.CustomerTransactionReceiptView);
                   
                    SaveSalesOrder();
                    facadeTransactionReceipt.ReserveAndAllocateReceipt(_gatewaySalesOrderDataset, null, Interprise.Framework.Base.Shared.Const.CUSTOMER_SALES_ORDER);
                }


                #endregion

                #region If Not Approve VOID Receipt

                if (!approved)
                {
                    if (status != AppLogic.ro_3DSecure)
                    {
                        string transactionType = isInvoice ? _facadeInvoice.TransactionType.ToString() : _facadeSalesOrder.TransactionType.ToString();

                        facadeTransactionReceipt.Void(transactionType, _gatewayInvoiceDataset);

                        if (isInvoice)
                        {
                            SaveInvoice();
                        }
                        else
                        {
                            SaveSalesOrder();
                        }
                    }
                }

                #endregion

                #region Capture Payment

                if (approved && isInvoice)
                {
                    CapturePayment(gatewayReceiptDataset, facadeReceipt, receiptCode);
                }

                #endregion

                #region Dispose Objects

                facadeCustomerReceiptList.Dispose();
                datasetCustomerReceipt.Dispose();
                customerReceiptBaseDataset.Dispose();

                gatewayReceiptDataset.Dispose();
                gatewayTransactionReceiptDataset.Dispose();
                facadeTransactionReceipt.Dispose();
                facadeReceipt.Dispose();

                facadeCustomerReceiptList = null;
                datasetCustomerReceipt = null;
                customerReceiptBaseDataset = null;

                gatewayReceiptDataset = null;
                gatewayTransactionReceiptDataset = null;
                facadeTransactionReceipt = null;
                facadeReceipt = null;

                _facadeInvoice = null;
                _facadeSalesOrder = null;

                #endregion

            }

            #endregion

            return status;
        }
        #endregion

        #region Load & Save Sales Order

        public void LoadSalesOrder(string documentCode)
        {
            int retries = 3;

            for (int ctr = 0; ctr < retries; ctr++)
            {
                try
                {
                    _gatewaySalesOrderDataset = new SalesOrderDatasetGateway();
                    _facadeSalesOrder = new SalesOrderFacade(_gatewaySalesOrderDataset);

                    // For POS created transaction
                    _gatewaySalesOrderDataset.CustomerSalesOrderView.SaveCounterIDColumn.AllowDBNull = true;

                    _facadeSalesOrder.LoadDataSet(documentCode);
                    _facadeSalesOrder.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.SalesOrder;

                    break;
                }
                catch
                {
                    _gatewaySalesOrderDataset.Dispose();
                    _facadeSalesOrder.Dispose();

                    _gatewaySalesOrderDataset = null;
                    _facadeSalesOrder = null;

                    // if this is the last retry, bubble up the error
                    if (ctr + 1 == retries)
                    {
                        throw;
                    }
                }
            }
        }

        public SalesOrderFacade GetSalesOrderFacade()
        {
            return _facadeSalesOrder;
        }

        public void SaveSalesOrder()
        {
            //Retry saving if concurrency error was encountered otherwise, simply throw the exception.

            int nooftries = 0;
            while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
            {
                try
                {
                    string[] relatedTables = _facadeSalesOrder.get_RelatedTables(_facadeSalesOrder.TransactionType);
                    string[][] commands = _facadeSalesOrder.CreateParameterSet(relatedTables);
                    _facadeSalesOrder.UpdateDataSet(commands, _facadeSalesOrder.TransactionType, String.Empty, false);

                    break;
                }
                catch (DBConcurrencyException)
                {
                    nooftries += 1;
                    SaveSalesOrder();
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion


        public bool ValidateOrderFraud(string salesOrderCode, Address billingAddress, Address shippingAddress, decimal orderSubTotal)
        {
            bool isOrderFraud = false;
            decimal maxMindRiskScore = Decimal.Zero;
            string fraudDetails = String.Empty;

            try
            {
                string billingEmailDomain = billingAddress.EMail.ToLowerInvariant().Trim();
                if (billingEmailDomain.Length == 0)
                {
                    billingEmailDomain = _customer.EMail.ToLowerInvariant().Trim();
                }
                int i = billingEmailDomain.IndexOf("@");
                if (i != -1)
                {
                    try
                    {
                        billingEmailDomain = billingEmailDomain.Substring(i + 1);
                    }
                    catch { }
                }

                string emailMD5 = (billingAddress.EMail.Length != 0) ? ServiceFactory.GetInstance<ICryptographyService>().GetMD5Hash(billingAddress.EMail.Trim().ToLowerInvariant()) :
                                                                       ServiceFactory.GetInstance<ICryptographyService>().GetMD5Hash(_customer.EMail.Trim().ToLowerInvariant());
                string usernameMD5 = ServiceFactory.GetInstance<ICryptographyService>().GetMD5Hash(billingAddress.CardName.Trim().ToLowerInvariant());
                string orderAmount = orderSubTotal.ToString();

                var currentRequest = ServiceFactory.GetInstance<IHttpContextService>().CurrentRequest;
                string userAgent = currentRequest.Headers["User-Agent"];
                string acceptLanguage = currentRequest.Headers["Accept-Language"];

                string transactionType = String.Empty;
                string cardBINNumber = String.Empty;

                if (!_customer.ThisCustomerSession["paypalfrom"].IsNullOrEmptyTrimmed())
                {
                    transactionType = "paypal";
                }
                else if (_customer.PaymentTermCode == DomainConstants.PAYMENT_METHOD_CREDITCARD)
                {
                    transactionType = "creditcard";
                    string customerCreditCardNumber = ServiceFactory.GetInstance<ICryptographyService>().InterpriseDecryption(_customer.ThisCustomerSession["CardNumber"],
                                                                                                            _customer.ThisCustomerSession["CardNumberSalt"],
                                                                                                            _customer.ThisCustomerSession["CardNumberIV"]);
                    if (!customerCreditCardNumber.IsNullOrEmptyTrimmed())
                    {
                        cardBINNumber = customerCreditCardNumber.Substring(0, 6);
                    }
                }
                else
                {
                    transactionType = "other";
                }

                var minFraud = ServiceFactory.GetInstance<IThirdPartyProcessingService>().ParseMinFraudSoapObject(acceptLanguage, billingAddress.City, billingAddress.State, billingAddress.PostalCode, billingAddress.Country, billingEmailDomain,
                                                                                                                  cardBINNumber, String.Empty, String.Empty, billingAddress.Phone, String.Empty, emailMD5, usernameMD5, String.Empty,
                                                                                                                  shippingAddress.Address1, shippingAddress.City, shippingAddress.State, shippingAddress.PostalCode, shippingAddress.Country,
                                                                                                                  salesOrderCode, "ConnectedBusinessEcommerce", userAgent, orderAmount, String.Empty, String.Empty, String.Empty, transactionType);

                fraudDetails = minFraud.FraudDetails;
                maxMindRiskScore = minFraud.RiskScore;
                // ok, call worked, now let's determine what to do with it: 
                isOrderFraud = maxMindRiskScore >= AppLogic.AppConfigUSDecimal("MaxMind.FailScoreThreshold");
            }
            catch (Exception ex)
            {
                fraudDetails = ex.Message;
                maxMindRiskScore = -1.0M;

                // stop the order when an exception occur
                isOrderFraud = true;
            }

            if (isOrderFraud)
            {

                var transactionFailed = FailedTransaction.New();
                transactionFailed.CustomerCode = _customer.CustomerCode;
                transactionFailed.SalesOrderCode = salesOrderCode;
                transactionFailed.PaymentGateway = "MaxMind";
                transactionFailed.MaxMindDetails = fraudDetails;
                transactionFailed.TransactionCommand = FailedTransaction.NOT_APPLICABLE;
                transactionFailed.TransactionResult = fraudDetails;
                transactionFailed.IPAddress = _customer.LastIPAddress;
                transactionFailed.MaxMindFraudScore = maxMindRiskScore;
                transactionFailed.IsFailed = isOrderFraud;
                transactionFailed.Record();
            }

            return isOrderFraud;
        }


        private string logFailedTransaction(string gatewayMessage, string cardType, string prefferredGateway, bool is3DsecondAuth, string lastGatewayErrorMessage, string responseCode, string responseMessage, bool isInvoice)
        {
            string status = String.Empty;

            if (prefferredGateway == AppLogic.ro_PROTX && !is3DsecondAuth && (CommonLogic.StringInCommaDelimitedStringList(cardType, AppLogic.AppConfig("3DSECURE.CreditCardTypes"))))
            {
                return AppLogic.ro_3DSecure;
            }

            _customer.LastGatewayErrorMessage = lastGatewayErrorMessage;
            status = AppLogic.ro_INTERPRISE_GATEWAY_AUTHORIZATION_FAILED;
            if (prefferredGateway == AppLogic.ro_PROTX && is3DsecondAuth)
            {
                string gatewayResponseCode = responseCode;
                switch (gatewayResponseCode.ToUpperInvariant())
                {
                    case "ERROR":
                        status = "The transaction encountered an error. Please try again";
                        break;
                    case "INVALID":
                        status = "The card was not accepted. Please try again";
                        break;
                    case "NOTAUTHED":
                        status = "Your card was not authorized for that amount. Please try again";
                        break;
                    case "REJECTED":
                        status = "Your card was not not accepted. Please try again";
                        break;
                    default:
                        status = responseMessage;
                        break;
                }
            }

            string gateway = ServiceFactory.GetInstance<IGatewayRepository>().GetCreditCardGatewayDescription("Gateway");
             
            var transactionFailed = FailedTransaction.New();
            transactionFailed.CustomerCode = _customer.CustomerCode;
            transactionFailed.SalesOrderCode = isInvoice ? _gatewayInvoiceDataset.CustomerInvoiceView[0].InvoiceCode : _gatewaySalesOrderDataset.CustomerSalesOrderView[0].SalesOrderCode;
            transactionFailed.PaymentGateway = gateway;
            transactionFailed.PaymentMethod = DomainConstants.PAYMENT_METHOD_CREDITCARD;
            transactionFailed.TransactionCommand = FailedTransaction.NOT_APPLICABLE;
            transactionFailed.TransactionResult = String.Format("{0} {1}", lastGatewayErrorMessage, gatewayMessage);
            transactionFailed.IPAddress = _customer.LastIPAddress;
            transactionFailed.IsFailed = true;

            transactionFailed.Record();

            return status;
        }

        private static string GetPreferredGateway()
        {
            return ServiceFactory.GetInstance<IPaymentTermService>().GetPrefferedGatewayInfo()[0];
        }

        [Obsolete("Use the method GetGateway in CreditCardAuthorizationFacade in getting Credit Card Gateway interface. You may use ServiceFactory.GetInstance<IPaymentTermService>().GetGatewayInterface in getting gateway information to pass on GetGateway method.")]
        private static ICreditCardGatewayInterface GetGatewayProcessorCore()
        {
            string[] info = new string[5];
            var request = ServiceFactory.GetInstance<IHttpContextService>().CurrentRequest;
            if (request.QueryString["PayPal"] == bool.TrueString && request.QueryString["token"] != null)
            {
                info = ServiceFactory.GetInstance<IPaymentTermService>().GetPaypalGatewayInfo();
            }
            else
            {
                info = ServiceFactory.GetInstance<IPaymentTermService>().GetPrefferedGatewayInfo();
            }

            ICreditCardGatewayInterface gatewayProcessor = null;
            try
            {
                var gatewayInterface = Interprise.Facade.Base.SimpleFacade.Instance.DeserializeFormSectionPlugin(info, new object[] { }) as Interprise.Extendable.Base.Presentation.Customer.CreditCardGateway.ICreditCardGatewayInterface;
                if (null != gatewayInterface)
                {
                    gatewayProcessor = gatewayInterface.CreditCardGatewayFacade;
                }
            }
            catch (Exception)
            {
                throw new Exception("Unable to instantiate Default Credit Card Gateway");
            }

            return gatewayProcessor;
        }

        private DataSet GetCustomerReceipt(string invoiceCode, ref BaseDataset dataset, bool isInvoice)
        {
            var facadeReceiptList = new ListControlFacade();
            var datasetReceipt = new DataSet();

            //deadlock

            int nooftries = 0;
            while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
            {
                try
                {
                    string tableName = isInvoice ? Const.CUSTOMERINVOICE_TABLE : Interprise.Framework.Customer.Shared.Const.CUSTOMERSALESORDER_TABLE;
                    string columnName = isInvoice ? Interprise.Framework.Customer.Shared.Const.CUSTOMERINVOICE_INVOICECODE_COLUMN : Interprise.Framework.Customer.Shared.Const.CUSTOMERSALESORDER_SALESORDERCODE_COLUMN;

                    string customerInfoQuery = String.Format("CustomerCode = (SELECT BillToCode FROM {0} WHERE {1} = {2})", tableName, columnName, invoiceCode.ToDbQuote());

                    datasetReceipt = facadeReceiptList.ReadSearchResultsData("CustomerActiveCustomersView",
                                                customerInfoQuery,
                                                1,
                                                false,
                                                string.Empty,
                                                ref dataset,
                                                true);
                    break;
                }
                catch (SqlException ex)
                {
                    if (ex.ErrorCode == 1205)
                    {
                        nooftries += 1;
                    }
                    else
                    {
                        throw;
                    }
                }

            }

            facadeReceiptList.Dispose();

            return datasetReceipt;

        }

        private void AttachGatewayAuthorization(ReceiptDatasetGateway gatewayReceiptDataset,
          ReceiptDatasetGateway.PaymentMethodViewRow rowPaymentMethod,
          GatewayResponse withGatewayAuthResponse,
          string receiptCode, bool isInvoice)
        {
            // MAP Interprise Needed Fields here..
            ReceiptDatasetGateway.CustomerCCAuthResponseRow rowResponse = gatewayReceiptDataset.CustomerCCAuthResponse.NewCustomerCCAuthResponseRow();
            rowResponse.BeginEdit();
            rowResponse.ResponseCode = SimpleFacade.Instance.GenerateDocumentCode(Interprise.Framework.Base.Shared.Enum.TransactionType.CCResponse.ToString());
            rowResponse.CustomerCode = isInvoice ? _gatewayInvoiceDataset.CustomerInvoiceView[0].BillToCode : _gatewaySalesOrderDataset.CustomerSalesOrderView[0].BillToCode;
            rowResponse.DocumentCode = gatewayReceiptDataset.CustomerCardPayment[0].CardPaymentCode;
            rowResponse.TransactionType = "Auth";
            //rowResponse.CreditCard = rowPaymentMethod.CreditCardOnFile;
            rowResponse.Amount = rowPaymentMethod.AmountPaidRate;
            rowResponse.GatewayResponseCode = withGatewayAuthResponse.Status;
            rowResponse.ResponseMsg = withGatewayAuthResponse.Details;
            rowResponse.Message = withGatewayAuthResponse.Details;
            rowResponse.Result = 0;
            rowResponse.AuthCode = withGatewayAuthResponse.AuthorizationCode;
            rowResponse.Reference = withGatewayAuthResponse.AuthorizationTransID;
            rowResponse.AVSResult = withGatewayAuthResponse.AVSResult;
            rowResponse.CVResult = withGatewayAuthResponse.CV2Result;
            rowResponse.TransactionCommand = withGatewayAuthResponse.TransactionCommandOut;
            rowResponse.AuthorizationResult = withGatewayAuthResponse.TransactionResponse;
            rowResponse.EndEdit();

            gatewayReceiptDataset.CustomerCCAuthResponse.AddCustomerCCAuthResponseRow(rowResponse);

            rowPaymentMethod.BeginEdit();
            rowPaymentMethod.CreditCardReference = withGatewayAuthResponse.AuthorizationTransID;
            rowPaymentMethod.CreditCardResponseCode = rowResponse.ResponseCode;
            rowPaymentMethod.CreditCardAuthorizationCode = withGatewayAuthResponse.AuthorizationCode;
            // NOTE:
            //  We only do auth only for now
            rowPaymentMethod.CreditCardIsAuthorized = true;
            rowPaymentMethod.CreditCardIsVoided = false;
            rowPaymentMethod.CreditCardIsCredited = false;
            rowPaymentMethod.EndEdit();
        }

        private decimal GetOrderTotal(bool isInvoice)
        {
            bool isDetailBuilt = isInvoice ? IsInvoiceDetailBuilt : IsSalesOrderDetailBuilt;
            string transaction = isInvoice ? "Invoice" : "Sales Order";

            if (!isDetailBuilt)
            {
                throw new InvalidOperationException(String.Format("{0} Detail not yet built!", transaction));
            }

            return isInvoice ? _gatewayInvoiceDataset.CustomerInvoiceView[0].TotalRate : _gatewaySalesOrderDataset.CustomerSalesOrderView[0].TotalRate;
        }

        private void CapturePayment(ReceiptDatasetGateway gatewayReceiptDataset, ReceiptFacade facadeReceipt, string receiptCode)
        {

            InvoiceDatasetGateway tmpGatewayInvoiceDataset = _gatewayInvoiceDataset;

            if (null != tmpGatewayInvoiceDataset)
            {
                using (var gatewayInvoiceDataset = new InvoiceDatasetGateway())
                {
                    using (var facadeInvoice = new InvoiceFacade(gatewayInvoiceDataset))
                    {
                        facadeInvoice.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice;

                        string invoiceCode = tmpGatewayInvoiceDataset.CustomerInvoiceView[0].InvoiceCode;

                        string[][] paramSet = new string[][] { new string[] { "@InvoiceCode", invoiceCode } };

                        //unified load of data to avoid PK related error during update.
                        facadeInvoice.LoadDataSet(Interprise.Framework.Base.Shared.StoredProcedures.READCUSTOMERINVOICEUNIFIED,
                        new string[] {tmpGatewayInvoiceDataset.CustomerInvoiceView.TableName, tmpGatewayInvoiceDataset.CustomerInvoiceDetailView.TableName, gatewayInvoiceDataset.TransactionTaxDetailView.TableName, 
                                      tmpGatewayInvoiceDataset.TransactionItemTaxDetailView.TableName, tmpGatewayInvoiceDataset.CustomerCreditAllocationView.TableName, tmpGatewayInvoiceDataset.CustomerCreditView.TableName,
                                      tmpGatewayInvoiceDataset.CustomerItemKitDetailView.TableName, tmpGatewayInvoiceDataset.CustomerSalesRepCommissionView.TableName, tmpGatewayInvoiceDataset.TransactionTaxSummary.TableName,
                                      tmpGatewayInvoiceDataset.CustomerInvoiceSerialLotNumbers.TableName, tmpGatewayInvoiceDataset.CustomerInvoiceKitSerialLotNumbers.TableName},
                        paramSet,
                        Interprise.Framework.Base.Shared.Enum.ClearType.None,
                        Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                        // let's reload the receipt table
                        string[][] reloadReceiptCommandset = new string[][]{
                                                                    new string[]{
                                                                        Const.CUSTOMERCARDPAYMENT_TABLE,
                                                                        StoredProcedures.READCUSTOMERCARDPAYMENT, 
                                                                        "@CardPaymentCode",
                                                                        receiptCode},
                                                                    new string[]{
                                                                        Const.PAYMENTMETHODVIEW_TABLE,
                                                                        StoredProcedures.READPAYMENTMETHOD,
                                                                        "@ReceivableCode",
                                                                        receiptCode}};

                        facadeReceipt.LoadDataSet(reloadReceiptCommandset, Interprise.Framework.Base.Shared.Enum.ClearType.Specific, Interprise.Framework.Base.Shared.Enum.ConnectionStringType.Online);

                        // this time capture the order....
                        gatewayReceiptDataset.PaymentMethodView[0].CreditCardTransactionType = "Capture";

                        var gatewayTransactionReceiptDatasetInvoice = facadeInvoice.TransactionReceiptListFacade.CurrentDataset as TransactionReceiptDatasetGateway;
                        var facadeTransactionReceiptInvoice = facadeInvoice.TransactionReceiptListFacade as TransactionReceiptFacade;

                        string gatewayMessage = string.Empty;
                        bool approved = false;

                        // we should re-load the card gateway processor
                        // just to make sure that we have it properly initialized clean

                        using (var facadeCardCapture = new CreditCardAuthorizationFacade())
                        {
                            facadeCardCapture.TransactionType = facadeReceipt.TransactionType;
                            facadeCardCapture.TransactionReceiptFacade = facadeTransactionReceiptInvoice;
                            facadeCardCapture.CurrentCardPayment = receiptCode;
                            facadeCardCapture.CurrentParentTransactionCode = invoiceCode;

                            gatewayMessage = facadeCardCapture.AuthorizeCreditCard(facadeReceipt, false, ref approved);

                            if (approved)
                            {
                                gatewayInvoiceDataset.Merge(gatewayTransactionReceiptDatasetInvoice.CustomerTransactionReceiptView);

                                string errorCode = string.Empty;
                                string[] invoiceRelatedTables = facadeInvoice.get_RelatedTables(Interprise.Framework.Base.Shared.Enum.TransactionType.Invoice);
                                string[][] invoiceCommands = facadeInvoice.CreateParameterSet(invoiceRelatedTables);

                                facadeInvoice.EmailDownloadableItem(ref errorCode);
                                facadeInvoice.ComputeTotals(false, false, false);

                                //save current balance to DB before posting transaction.
                                facadeInvoice.UpdateDataSet();

                                if (facadeCardCapture.IsCurrentCardPaymentHasNewReceipt && gatewayInvoiceDataset.CustomerInvoiceView[0].IsPosted)
                                {

                                    facadeReceipt.RefreshPaymentTable(facadeCardCapture.NewReceiptDataTable, facadeCardCapture.NewPaymentMethodDataTable);
                                    facadeReceipt.Post(Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceipt);

                                    using (Interprise.Framework.Customer.DatasetGateway.ReceiptDatasetGateway receiptAllocation = new ReceiptDatasetGateway())
                                    {
                                        using (Interprise.Facade.Customer.ReceiptFacade facadeReceiptAllocation = new ReceiptFacade(receiptAllocation))
                                        {
                                            facadeReceiptAllocation.TransactionType = Interprise.Framework.Base.Shared.Enum.TransactionType.CustomerReceiptAllocation;
                                            facadeReceiptAllocation.AddAllocationHeader(facadeInvoice.CustomerInfo.CustomerRow);

                                            decimal exchangeRate = facadeInvoice.GetExchangerate(_customer.CurrencyCode);
                                            decimal allocatedRate = gatewayReceiptDataset.CustomerPayment[0].AmountPaidRate;

                                            facadeReceiptAllocation.CreateCustomerAllocationCreditFromReceipt(gatewayReceiptDataset, gatewayReceiptDataset.CustomerPayment[0], gatewayReceiptDataset.CustomerPayment[0].CurrencyCode);
                                            facadeReceiptAllocation.CreateCustomerAllocationDebitViewFromInvoice(gatewayInvoiceDataset.CustomerInvoiceView[0]);

                                            //allocate the invoice.
                                            facadeReceiptAllocation.SetColumnValue(receiptAllocation.CustomerAllocationDebitView[0], receiptAllocation.CustomerAllocationDebitView.AllocatedRateColumn.ColumnName, allocatedRate);
                                            facadeReceiptAllocation.SetColumnValue(receiptAllocation.CustomerAllocationDebitView[0], receiptAllocation.CustomerAllocationDebitView.AllocatedColumn.ColumnName, facadeInvoice.ConvertCurrency(exchangeRate, allocatedRate, true, _customer.CurrencyCode));
                                            facadeReceiptAllocation.SetColumnValue(receiptAllocation.CustomerAllocationDebitView[0], receiptAllocation.CustomerAllocationDebitView.SelectedColumn.ColumnName, true);

                                            //allocate the receipt 
                                            facadeReceiptAllocation.SetColumnValue(receiptAllocation.CustomerAllocationCreditView[0], receiptAllocation.CustomerAllocationCreditView.AllocatedRateColumn.ColumnName, allocatedRate);
                                            facadeReceiptAllocation.SetColumnValue(receiptAllocation.CustomerAllocationCreditView[0], receiptAllocation.CustomerAllocationCreditView.AllocatedColumn.ColumnName, facadeInvoice.ConvertCurrency(exchangeRate, allocatedRate, true, _customer.CurrencyCode));
                                            facadeReceiptAllocation.SetColumnValue(receiptAllocation.CustomerAllocationCreditView[0], receiptAllocation.CustomerAllocationCreditView.SelectedColumn.ColumnName, true);

                                            facadeReceiptAllocation.CreateReceiptAllocation();

                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
