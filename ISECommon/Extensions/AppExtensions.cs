﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.Collections;
using System.Data;
using System.Xml;
using System.Security;
using System.Web.Hosting;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.Extensions
{
    public static class AppExtensions
    {
        //Format sql quote string
        public static string FormatSqlQuote(this string s)
        {
            if (s == null) return "''";
                
            int len = s.Length + 25;
            var tmpS = new StringBuilder(len); // hopefully only one alloc

            tmpS.Append("N'");
            tmpS.Append(s.Replace("'", "''"));
            tmpS.Append("'");
            return tmpS.ToString();
        }   

        //shortcut of String.Format
        public static string FormatWith(this string s, params object[] args)
        {
            if (args == null) return s;
            return String.Format(CultureInfo.InvariantCulture, s, args);
        }

        public static bool? TryParseBool (this string str)
        {
            bool? ret = null;
            if (str == string.Empty) return ret;

            //if value is 1 or 0
            int? intValue = str.TryParseInt();
            if(intValue.HasValue && intValue.Value < 2)
            {
                return Convert.ToBoolean(intValue.Value);
            }

            //if value true or false
            if (str.ToLowerInvariant() == "true") return true;

            if (str.ToLowerInvariant() == "false") return false;

            return ret;
        }

        public static bool TryParseBool(this int? intValue)
        {
            bool ret = false;
            if (intValue == null) return ret;
            if (intValue.Value < 2)
            {
                ret = Convert.ToBoolean(intValue.Value);
            }
            return ret;
        }

        public static int? TryParseInt(this object obj)
        {
            return (obj == DBNull.Value || obj == null) ? null : obj.ToString().TryParseInt();
        }

        public static DateTime? TryParseDateTime(this string str)
        {
            DateTime dt = DateTime.MinValue;
            DateTime.TryParse(str, out dt);
            if (dt == DateTime.MinValue) return null;
            return dt;
        }

        public static DateTime? TryParseDateTime(this object obj)
        {
            return (obj == DBNull.Value || obj == null) ? null : obj.ToString().TryParseDateTime();
        }

        public static int ToBit(this bool value)
        {
            return (value) ? 1 : 0;
        }

        public static string ToBitString(this bool value)
        {
            return value.ToBit().ToString();
        }

        public static decimal ToDecimal(this string str)
        {
            decimal value;
            if (Decimal.TryParse(str, out value)) { return value; }
            return Decimal.Zero;
        }

        //Compare 2 string ignore case
        public static bool EqualsIgnoreCase(this string s, string matchWith)
        {
            return s.Equals(matchWith, StringComparison.OrdinalIgnoreCase);
        }

        //parse string if int32 else return 0
        public static bool IsInt32(this string s)
        {
            int x;
            return int.TryParse(s, out x);
        }

        public static TEnum TryParseEnum<TEnum>(this string enumname)
        {
            return (TEnum)Enum.Parse(typeof(TEnum), enumname, true);
        }

        //parse string if int32 else return null
        public static int? TryParseInt(this string str)
        {
            int xValue;
            if (int.TryParse(str, out xValue))
            {
                return xValue;
            }
            return null;
        }

        //check if value is in between the start and end Int value
        public static bool Between(this int n, int start, int end)
        {
            return n >= start && n <= end;
        }

        //check if value is in between the start and end Date value
        public static bool Between(this DateTime n, DateTime start, DateTime end)
        {
            return n >= start && n <= end;
        }

        //check if value is in the collection
        public static bool In(this int n, params int[] sequence)
        {
            return sequence.Any(x => x == n);
        }

        //pick a specified items in the collection.
        public static IEnumerable<T> Pick<T>(this IEnumerable<T> collection, Func<T, bool> when)
        {
            var items = new List<T>();
            foreach (var item in collection)
            {
                if (when(item))
                {
                    items.Add(item);
                }
            }

            return items;
        }

        /// <summary>
        /// shuffles the items in the collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="random"></param>
        /// <returns></returns>
        public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source, Random random)
        {
            var buffer = source.ToList();

            for (int i = 0; i < buffer.Count; i++)
            {
                int j = random.Next(i, buffer.Count);
                yield return buffer[j];

                buffer[j] = buffer[i];
            }
        }

        //parse string if decimal else return 0
        public static decimal? TryParseDecimal(this string str)
        {
            decimal xValue;
            if (decimal.TryParse(str, out xValue))
            {
                return xValue;
            }
            return null;
        }

        //parse string if float else return 0
        public static float? TryParseFloat(this string str)
        {
            float xValue;
            if (float.TryParse(str, out xValue))
            {
                return xValue;
            }
            return null;
        }

        //parse string if float else return 0
        public static long? TryParseLong(this string str)
        {
            long xValue;
            if (long.TryParse(str, out xValue))
            {
                return xValue;
            }
            return null;
        }

        public static DateTime? TryParseDataTimeLocalized(this string strDateTime, CultureInfo cultureInfo) 
        {
            var dateFormat = cultureInfo.DateTimeFormat.Clone() as DateTimeFormatInfo;
            DateTime? dte = null;
            DateTime xdte = DateTime.MinValue;
            if (DateTime.TryParseExact(strDateTime, dateFormat.FullDateTimePattern, cultureInfo, DateTimeStyles.None, out xdte))
            {
                dte = xdte;
            }
            return dte;
        }

        public static Guid? TryParseGuid(this string str)
        {
            Guid guid;
            if(Guid.TryParse(str, out guid))
            {
                return guid;
            }
            return null;
        }

        //Direct string parsing
        public static bool IsNullOrEmptyTrimmed(this string str)
        {
            if (str != null) str = str.Trim();
            return string.IsNullOrEmpty(str);
        }

        //Direct string parsing
        public static bool IsNullOrEmpty(this string str)
        {
            return string.IsNullOrEmpty(str);
        }

        //Direct object parsing
        public static bool IsNullOrEmptyTrimmed(this object str)
        {
            string temp = string.Empty;
            if (str != null) temp = str.ToString().Trim();
            return string.IsNullOrEmpty(temp);
        }
        
        public static void ForEach<T> (this IEnumerable<T> collection, Action<T> cAction)
        {
            foreach (var item in collection)
            {
                cAction(item);
            }
        }

        public static bool IsValidGuid(this string guidStringToTest)
        {
            try
            {
                Guid g = new Guid(guidStringToTest);
                return true;
            }
            catch { }

            return false;
        }

        public static string ToMungeName(this string str, string regex = "[^a-z0-9-_ ]", string otherFormat = "[\\.\\@\\$\\%\\^\\*\\]\\&\\!\\+\\=\\#\\[\\~\\`\\(\\)]")
        {
            string strToFormat = Regex.Replace(str.Trim().ToLowerInvariant(), otherFormat, "");
            strToFormat = Regex.Replace(strToFormat.Trim().ToLowerInvariant(), regex, "-").Replace(" ", "-");
            while (strToFormat.IndexOf("--") != -1) { strToFormat = strToFormat.Replace("--", "-"); }
            while (strToFormat.IndexOf("__") != -1) { strToFormat = strToFormat.Replace("__", "_"); }
            return HttpUtility.UrlEncode(strToFormat);
        }

        public static string ToHtmlDecode(this string str)
        {
            return HttpUtility.HtmlDecode(str);
        }

        public static string ToHtmlEncode(this string str)
        {
            return HttpUtility.HtmlEncode(str);
        }

        public static string ToUrlDecode(this string str)
        {
            return HttpUtility.UrlDecode(str);
        }

        public static string ToUrlEncode(this string str)
        {
            return HttpUtility.UrlEncode(str);
        }

        public static string ToDriverLink(this string str) 
        {
            return "t-{0}.aspx".FormatWith(str).ToLowerInvariant();
        }

        public static string ToQueryString(this string str)
        {
            string tmpS = String.Empty;
            if (!(HttpContext.Current.Request.QueryString[str].IsNullOrEmptyTrimmed()))
            {
                tmpS = HttpContext.Current.Request.QueryString[str];
            }
            return tmpS;
        }

        public static string ToQueryStringDecode(this string str ){
             return str.ToQueryString().ToUrlDecode();
        }

        public static string ToQueryStringEncode(this string str)
        {
            return str.ToQueryString().ToUrlEncode();
        }

        public static int? TryParseIntLocalization(this string str, CultureInfo cultureInfo)
        { 
            int usi = 0;
            Int32.TryParse(str, NumberStyles.Integer, cultureInfo, out usi); // use default locale setting
            if (usi == 0) return null;
            return usi;
        }

        public static decimal? TryParseDecimalLocalization(this string str, CultureInfo cultureInfo)
        {
            decimal usi = 0;
            Decimal.TryParse(str, NumberStyles.Integer|NumberStyles.AllowDecimalPoint, cultureInfo, out usi); // use default locale setting
            if (usi == 0) return null;
            return usi;
        }

        public static decimal? TryParseDecimalUsLocalization(this string str)
        {
            return str.TryParseDecimalLocalization(new CultureInfo("en-US"));
        }

        public static int? TryParseIntUsLocalization(this string str)
        {
            return str.TryParseIntLocalization(new CultureInfo("en-US"));
        }

        public static string ToSubString(this string str, int value)
        {
            if (str.Length <= value) return str;
            else return str.Substring(0, value - 1);
        }

        public static IEnumerable<KeyValuePair<string, string>> ToPairs(this NameValueCollection collection)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }
            return collection.Cast<string>().Select(key => new KeyValuePair<string, string>(key, collection[key]));
        }

        public static IEnumerable<KeyValuePair<DictionaryEntry, object>> HashToPairs(this Hashtable collection, bool useParallel = false)
        {
            if (collection == null)
            {
                throw new ArgumentNullException("collection");
            }

            if (useParallel)
            {
                return collection.Cast<DictionaryEntry>().Select(key => new KeyValuePair<DictionaryEntry, object>(key, collection[key])).AsParallel();
            }
            else
            {
                return collection.Cast<DictionaryEntry>().Select(key => new KeyValuePair<DictionaryEntry, object>(key, collection[key]));
            }
        }

        public static string ToPathFromFileOrPath(this string fileOrpath, bool includeBackslash = false)
        {
            string path = fileOrpath;
            if (File.Exists(fileOrpath))
            {
                var fileInfo = new FileInfo(fileOrpath);
                path = fileInfo.Directory.FullName;
            }
            // Handle special cases like resource(axd) and Rewritted URLs(aspx) which is not a valid file.
            else if (fileOrpath.EndsWith(".aspx"))
            {
                //Get the directory leaving the invalid file
                path = fileOrpath.Substring(0, fileOrpath.LastIndexOf(@"\"));
            }

            if (!includeBackslash) return path;
            
            if (!path.EndsWith(@"\"))
            {
                path += @"\";
            }

            return path;
        }

        public static string ToStringLower(this bool condition) 
        {
            return condition.ToString().ToLowerInvariant();    
        }

        public static T FindByParse<T>(this RepeaterItem item, string controlId) where T :class 
        {
            return item.FindControl(controlId) as T;
        }

        public static T DataItemAs<T>(this RepeaterItem repeaterItem) where T : class
        {
            return repeaterItem.DataItem as T;
        }

        public static string ToJSON(this IEnumerable array)
        {
            try
            {
                return InterpriseSuiteEcommerceCommon.Tool.JSONHelper.Serialize(array);
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public static string ParseCookieValue(this string key, bool decode)
        {
            string tmp = String.Empty;

            if (HttpContext.Current == null) return tmp;

            if (HttpContext.Current.Request.Cookies[key] == null)
            {
                return tmp;
            }
            try
            {
                tmp = HttpContext.Current.Request.Cookies[key].Value.ToString();
                if (decode)
                {
                    tmp = HttpContext.Current.Server.UrlDecode(tmp);
                }
                return tmp;
            }
            catch
            {
                return tmp;
            }
        }

        public static HttpCookie ParseCookieRequest(this string key)
        {
            if (HttpContext.Current == null) return null;
            try
            {
                return HttpContext.Current.Request.Cookies[key];
            }
            catch
            {
                return null;
            }
        }

        public static HttpCookie ParseCookieResponse(this string key)
        {
            if (HttpContext.Current == null) return null;

            try
            {
                return HttpContext.Current.Response.Cookies[key];
            }
            catch
            {
                return null;
            }
        }

        public static string ToServerVariables(this string key)
        {
            string tmpValue = String.Empty;

            if (HttpContext.Current == null) return tmpValue;

            if (HttpContext.Current.Request.ServerVariables[key] != null)
            {
                try
                {
                    tmpValue = HttpContext.Current.Request.ServerVariables[key].ToString();
                }
                catch
                {
                    tmpValue = String.Empty;
                }
            }
            return tmpValue;
        }

        public static string ToMapPath(this string path)
        {
            if (path.StartsWith("\\\\"))
            {
                return path;
            }

            string result = path;

            //Try it as a virtual path. Try to map it based on the Request.MapPath to handle Medium trust level and "~/" paths automatically 
            try
            {
                result = HttpContext.Current.Server.MapPath(path);
            }
            catch
            {
                //Didn't like something about the virtual path.
                //May be a drive path. See if it will expand to a valid path
                try
                {
                    //Try a GetFullPath. If the path is not virtual or has other malformed problems
                    //Return it as is
                    result = Path.GetFullPath(path);
                }
                catch (NotSupportedException) // Contains a colon, probably already a full path.
                {
                    return path;
                }
                catch (SecurityException exc)//Path is somewhere you're not allowed to access or is otherwise damaged
                {
                    throw new SecurityException("If you are running in Medium Trust you may have virtual directories defined that are not accessible at this trust level,\n " + exc.Message);
                }
            }
            return result;
        }

        public static Hashtable SplitToHash(this string configValue)
        {
            char[] trimChars = { ' ', ';' };
            var m_mapEntries = new Hashtable();

            string imgAppConfigParam = configValue.TrimEnd(trimChars);

            // Split the input string
            var arEntries = imgAppConfigParam.Split(';');
            for (int i = 0; i < arEntries.Length; ++i)
            {
                // Split this entry in to key and value
                var arPieces = arEntries[i].Split(':');
                try
                {
                    if (arPieces.Length > 1)
                    {
                        // Add this key/value pair (trimmed and lowercase) to our map
                        m_mapEntries[arPieces[0].Trim().ToLower()] = arPieces[1].Trim().ToLower();
                    }
                }
                catch { }
            }

            return m_mapEntries;
        }

        public static bool IsFileExists(this string pathName)
        {
            return File.Exists(pathName.ToMapPath());
        }

        public static string ReplaceSpecialCharacters(this string str)
        {
            string pattern = "[\\~#%&*{}/:<>?|\"-,]";
            string replacement = " ";

            var regEx = new System.Text.RegularExpressions.Regex(pattern);
            string sanitizedString = System.Text.RegularExpressions.Regex.Replace(regEx.Replace(str, replacement), @"\s+", " ");
            return sanitizedString;
        }
    }

    public static class EnumExtensions
    {
        public static IEnumerable<T> GetAllItems<T>()
        {
            return from object item in Enum.GetValues(typeof(T)) select (T)item;
        }
    }

    public static class DomainSecurityExtension
    { 
        public static string ToJavaScriptEscape(this string value)
        {
            var sb = new StringBuilder(value);
            sb.Replace("\b", string.Empty);
            sb.Replace("\f", string.Empty);
            sb.Replace("\n", string.Empty);
            sb.Replace("\r", string.Empty);
            sb.Replace("\t", string.Empty);
            sb.Replace("'", @"\\'");
            sb.Replace("\"", "\\\"");

            return sb.ToString();
        }

        /// <summary>
        /// Calls HttpContext.Current.Request.Cookies and convert it to List
        /// </summary>
        public static IEnumerable<HttpCookie> ToEnumberableCookie(this string[] keys)
        {
            if (HttpContext.Current == null) return new List<HttpCookie>();
            return keys.Select(k => HttpContext.Current.Request.Cookies[k]).ToList();
        }
    }

    public static class DBExtensions
    {
        public static string ToDbQuote(this string value)
        {
            if (value == null) { return "''"; }
            int len = value.Length + 25;
            var tmpS = new StringBuilder(len); // hopefully only one alloc
            tmpS.Append("N'");
            tmpS.Append(value.Replace("'", "''"));
            tmpS.Append("'");
            return tmpS.ToString();
        }

        public static string ToDbDateQuote(this string s)
        {
            int len = s.Length + 25;
            var tmpS = new StringBuilder(len); // hopefully only one alloc
            tmpS.Append("'");
            tmpS.Append(s.Replace("'", "''"));
            tmpS.Append("'");
            return tmpS.ToString();
        }

        public static string ToRSField(this IDataReader reader, string fieldName)
        {
            int index = reader.GetOrdinal(fieldName);
            if (reader.IsDBNull(index) || reader.GetString(index) == "\0")
            {
                return String.Empty;
            }
            return reader.GetString(index);
        }

        public static int ToRSFieldInt(this IDataReader reader, string fieldName)
        {
            int index = reader.GetOrdinal(fieldName);
            if (reader.IsDBNull(index) || reader[fieldName].ToString().IsNullOrEmptyTrimmed())
            {
                return 0;
            }
            return reader.GetInt32(index);
        }

        public static byte[] ToRSFieldImage(this IDataReader reader, string fieldName)
        {
            int index = reader.GetOrdinal(fieldName);
            if (reader.IsDBNull(index) || reader[fieldName].ToString().IsNullOrEmptyTrimmed())
            {
                return null;
            }
            return (byte[])reader[index];
        }

        public static DateTime ToRSFieldDateTime(this IDataReader reader, string fieldName)
        {
            int index = reader.GetOrdinal(fieldName);
            if (reader.IsDBNull(index))
            {
                return System.DateTime.MinValue;
            }

            var sqlServerCulture = new CultureInfo("DBSQLServerLocaleSetting".ToConfigAppSettings());
            return Convert.ToDateTime(reader[index], sqlServerCulture);
        }

        public static Decimal ToRSFieldDecimal(this IDataReader reader, string fieldName)
        {
            int index = reader.GetOrdinal(fieldName);
            if (reader.IsDBNull(index))
            {
                return System.Decimal.Zero;
            }
            return reader.GetDecimal(index);
        }

        public static bool ToRSFieldBool(this IDataReader reader, string fieldName)
        {
            int index = reader.GetOrdinal(fieldName);
            if (reader.IsDBNull(index))
            {
                return false;
            }

            string s = reader[fieldName].ToString();

            return "TRUE".Equals(s, StringComparison.InvariantCultureIgnoreCase) ||
                    "YES".Equals(s, StringComparison.InvariantCultureIgnoreCase) ||
                    "1".Equals(s, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string ToRSFieldGUID(this IDataReader rs, string fieldName)
        {
            int index = rs.GetOrdinal(fieldName);
            if (rs.IsDBNull(index))
            {
                return String.Empty;
            }
            return rs.GetGuid(index).ToString();
        }

        public static Guid ToRSFieldTrueGUID(this IDataReader rs, string fieldName)
        {
            int index = rs.GetOrdinal(fieldName);
            if (rs.IsDBNull(index))
            {
                return Guid.Empty;
            }

            return rs.GetGuid(index);
        }

        public static object ToStringOrDBNull(this string value)
        {
            return value.IsNullOrEmptyTrimmed() ? DBNull.Value : (object)value;
        }

        public static long ToRSFieldLong(this IDataReader rs, string fieldName)
        {
            int idx = rs.GetOrdinal(fieldName);
            if (rs.IsDBNull(idx))
            {
                return 0;
            }
            return rs.GetInt64(idx);
        }

        public static Single ToRSFieldSingle(this IDataReader rs, string fieldName)
        {
            int idx = rs.GetOrdinal(fieldName);
            if (rs.IsDBNull(idx))
            {
                return 0.0F;
            }
            return (Single)rs.GetDouble(idx); // SQL server seems to fail the GetFloat calls, so we have to do this
        }

        public static Double ToRSFieldDouble(this IDataReader rs, string fieldName)
        {
            int idx = rs.GetOrdinal(fieldName);
            if (rs.IsDBNull(idx))
            {
                return 0.0F;
            }
            return rs.GetDouble(idx);
        }

        public static string ToNullStringOrDbQuote(this string param)
        {
            if (param.IsNullOrEmptyTrimmed())
            {
                param = DomainConstants.DB_NULL_CONST;
            }
            else 
            {
               param = param.ToDbQuote();
            }
            return param;
        }

    }

    public static class LocalizationExtension
    {
        // ----------------------------------------------------------------------------------------------
        // the following routines must (should) work in ALL locales, no matter what SQL Server setting is
        // ----------------------------------------------------------------------------------------------
        public static string ToDateTimeStringForDB(this DateTime dt)
        {
            return dt.ToString("s");
        }

        /// <summary>
        /// This will return the formatted currency rounded value with currency sysmbol in string
        /// Ex: 
        /// Input: 100.12232
        /// Output: $ 100.12
        /// </summary>
        public static string ToCustomerCurrency(this decimal value)
        {
            return ServiceFactory.GetInstance<ICurrencyService>()
                                 .FormatCurrency(value).Replace(" ", "");
        }

        /// <summary>
        /// This will return the currency rounded value in decimal
        /// Ex: 
        /// Input: 100.12232
        /// Output: 100.12
        /// </summary>
        public static decimal ToCustomerRoundedCurrency(this decimal value)
        {
            return ServiceFactory.GetInstance<ICurrencyService>()
                                 .RoundCurrencyByCustomer(value);
        }

        /// <summary>
        /// This will return the rounded monetary value in decimal
        /// Ex: 
        /// Input: 100.12232
        /// Output: 100.12
        /// </summary>
        public static decimal ToCustomerRoundedMonetary(this decimal value)
        {
            return ServiceFactory.GetInstance<ICurrencyService>()
                                 .RoundMonetaryByCustomer(value);
        }

        /// <summary>
        /// This will return the currency with formated value in string
        /// </summary>
        public static string ToCustomerRawCurrency(this decimal value)
        {
            return ServiceFactory.GetInstance<ICurrencyService>()
                                 .RawCurrencyByCustomer(value);
        }

        /// <summary>
        /// This will return formatted number with digit grouping
        /// Ex:
        /// Input: 100122.32
        /// Output: "100,122.32"
        /// </summary>
        /// <returns></returns>
        public static string ToNumberFormat(this decimal value)
        {
            return ServiceFactory.GetInstance<ILocalizationService>()
                                 .FormatDecimalToNumber(value, 0);
        }
        
        public static string ToSqlDecimalFormat(this decimal value)
        {
            System.Globalization.CultureInfo sqlCultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture(Localization.GetSqlServerLocale());
            return value.ToString(sqlCultureInfo);
        }

    }

    public static class DomainAppSettings
    {
        public static string ToConfigAppSettings(this string key)
        {
            string tempValue = string.Empty;
            if (System.Web.Configuration.WebConfigurationManager.AppSettings[key] != null)
            {
                try
                {
                    tempValue = System.Web.Configuration.WebConfigurationManager.AppSettings[key];
                }
                catch
                {
                    tempValue = string.Empty;
                }
            }
            return tempValue;
        }
    }

    public static class XmlExtension
    {
        public static string ToXmlEncode(this string value)
        {
            if (value == null) { return null; }

            value = Regex.Replace(value, @"[^\u0009\u000A\u000D\u0020-\uD7FF\uE000-\uFFFD]", "", RegexOptions.Compiled);
            return value.ToXmlEncodeAsIs();
        }

        public static string ToXmlEncodeAsIs(this string value)
        {
            if (value == null) { return null; }

            var sw = new StringWriter();
            var xwr = new XmlTextWriter(sw);
            xwr.WriteString(value);
            string sTmp = sw.ToString();
            xwr.Close();
            sw.Close();
            return sTmp;
        }
    }

}

namespace InterpriseSuiteEcommerceCommon.UI.Extensions
{
    public static class UIExtension
    {
        public static int ToSelectedIndexByText(this System.Web.UI.WebControls.DropDownList ddr, string text)
        {
            return ddr.Items.IndexOf(ddr.Items.FindByText(text));
        }
    }
}

