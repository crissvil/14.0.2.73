﻿
namespace InterpriseSuiteEcommerceCommon
{
    public class DomainConstants
    {
        public const string HTTP_USER_AGENT = "HTTP_USER_AGENT";
        public const string MENU_ITEM = "MENU_ITEM";
        public const string SHOWSTRING_RESOURCE_KEY = "ShowStringResourceKeys";

        public const string TEXT_NAME = "TEXT";
        public const string URL_NAME = "URL";
        public const string OPEN_NEW_TAB = "OPEN_NEW_TAB";

        public const string LOOKUP_HELPER_CATEGORIES = "CATEGORY";
        public const string LOOKUP_HELPER_DEPARTMENT = "DEPARTMENT";
        public const string LOOKUP_HELPER_MANUFACTURERS = "MANUFACTURER";
        public const string LOOKUP_HELPER_ATTRIBUTE = "ATTRIBUTE";
        public const string LOOKUP_HELPER_COMPANY = "COMPANY";
        public const string XML_ROOT_NAME = "FIELD";
        public const string XML_SECTION_TYPE = "SECTION_TYPE";

        public const string TOP_MENU_CACHE_NAME = "TOP_MENU_CACHE_NAME";

        public const string EntityProduct = "product";

        public const string EmailRegExValidator = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";
        public const string GCCallbackLoadCheck = "GCCallbackLoadCheck";

        public const string GcQueryParamId = "tid";

        public const string DeviceTypeName = "DeviceType";
        public const string DeviceTypeValue = "mobile";
        public const string MobileFolderName = "MobileFolderName";
        public const string DeviceSupport = "DeviceSupport";

        public const string QUERY_STRING = "QUERY_STRING";
        public const string QUERY_STRING_KEY_PAGENUM = "pagenum";
        public const string QUERY_STRING_KEY_SORT = "sort";
        public const string QUERY_STRING_KEY_ENTITY_ID = "entityid";
        public const string QUERY_STRING_KEY_ENTITY_NAME = "entityname";
        public const string QUERY_STRING_KEY_RESET_FILTERS = "resetfilters";
        public const string QUERY_STRING_KEY_ENTITY_TYPE = "entitytype";
        public const string QUERY_STRING_QUESTIONMARK = "?";
        public const string QUERY_STRING_AMPERSAND = "&";
        public const string QUERY_STRING_EQUAL_SIGN = "=";
        public const string QUERY_STRING_KEY_MATRIX_ID = "matrixid";
        public const string QUERY_STRING_KEY_UOM = "uom";
        public const string QUERY_STRING_KEY_VATSETTING = "vatsetting";
        public const string QUERY_STRING_KEY_RETURNURL = "returnurl";

        public const string XML_HELPER_PACKAGE_NAME = "XmlHelperPackage";

        public const string TEMPLATE_SWITCHING_PREFIX = "template";
        public const string DEFAULT_TEMPLATE_NAME = "template";
        public const string DEFAULT_TEMPLATE_EXTENSION = "ascx";

        public const string TOPIC_NAME = "topic";
        public const string DRIVER_PAGE_NAME = "driver";

        public const string GIFTREGISTRYPARAMCHAR = "g";

        public const char KITCOMPOSITION_DELIMITER = '+';
        public const char COMMA_DELIMITER = ',';
        public const char PIPELINE_DELIMITER = '|';

        public const string GIFTREGISTRY_CUSTOM_URL_REGEX_VALIDATION_VALUE = "^[a-zA-Z0-9-_]*$";

        public const long DEFAULT_REGISTRY_PHOTOSIZE = 500;

        public const double DEFAULT_REGISTRY_PAGESIZE = 50;

        public const string NOTIFICATION_QRY_STRING_PARAM = "NOTIFY";

        public const string DEFAULT_NO_PIC_FILENAME = "nopicture.gif";
        public const string SERVICE_TOKEN = "TOKEN";

        public const string SERVICE_TOKEN_FAILED = "Authorization Failed. Invalid Service Token.";

        public const string CMS_ENABLE_EDITMODE = "CMS_ENABLE_EDITMODE";

        public const string CMS_TEMPLATE = "CMS_TEMPLATE";
        public const string CMS_TOPIC_TEMPLATE = "CMS_TOPIC_TEMPLATE";
        public const string MOBILE_FULLMODE_QUERYTSTRING = "fullmode";
        public const string MOBILE_FULLMODE_SWITCHER = "MOBILE_FULLMODE_SWITCHER";
        public const string LIVECHAT = "LIVECHAT";

        public const string COUNTRY_US = "United States of America";

        public const string SAGEPAY_RESPONSE_STATUS_OK = "OK";
        public const string SAGEPAY_RESPONSE_STATUS_INVALID = "INVALID";
        public const string SAGEPAY_RESPONSE_STATUS_MALFORMED = "MALFORMED";
        public const string SAGEPAY_RESPONSE_STATUS_NOTAUTHED = "NOTAUTHED";
        public const string SAGEPAY_RESPONSE_STATUS_REJECTED = "REJECTED";
        public const string SAGEPAY_RESPONSE_STATUS_AUTHENTICATED = "AUTHENTICATED";
        public const string SAGEPAY_RESPONSE_STATUS_REGISTERED = "REGISTERED";
        public const string SAGEPAY_RESPONSE_STATUS_ERROR = "ERROR";
        public const string SAGEPAY_RESPONSE_STATUS_ABORT = "ABORT";
        public const string SAGEPAY_RESPONSE_STATUS_THREEDAUTH = "THREEDAUTH";
        public const string SAGEPAY_RESPONSE_STATUS_PENDING = "PENDING";

        public const string RMA_NOITEM_AVAILABLE_ERROR = "NOITEMAVAILABLE";

        public static string[] GetImageSupportedExtensions()
        {
            return new string[] { "jpg", "gif", "png" };
        }

        public const int ORDER_NOTE_MAX_LENGTH = 500;

        public const string REMEMBERME_COOKIE_NAME = "ISERememberMeCookie";

        public const string PAYMENT_METHOD_CREDITCARD = "Credit Card";

        public const string AdminKey = "AdminKey";
        public const string ADMIN_KEY_DEFAULT_VALUE = ".ADMINAUTH";

        public const string AdminTimeout = "AdminTimeout";
        public const double ADMIN_TIMEOUT_DEFAULT_VALUE = 30;

        public const string AdminLoginUrl = "AdminLoginUrl";
        public const string ADMIN_LOGIN_URL_DEFAULT_VALUE = "signin.aspx";

        public const string AdminDefault = "AdminDefault";
        public const string ADMIN_DEFAULT_URL_VALUE = "default.aspx";

        public const string MOBILE_IMAGE_CACHE_NAME = "MOBILE_IMAGE_CACHE_NAME";
        public const string MINICART_IMAGE_CACHE_NAME = "MINICART_IMAGE_CACHE_NAME";

        public const string PAYMENT_METHOD_CHECK = "Check/Cheque";
        public const string PAYMENT_METHOD_CASH = "Cash/Other";
        public const string PAYMENT_METHOD_PAYPALX = "PayPal";
        public const string PAYMENT_METHOD_WEBCHECKOUT = "Web Checkout";

        public const string CUSTOMER_SESSION_SHIPPING_ADDRESS_EDITED_KEY = "CUSTOMER_SESSION_SHIPPING_ADDRESS_EDITED_KEY";

        public const string STORE_HTTP_LOCATION = "STORE_HPPT_LOCATION";
        public const string SYSTEM_COUNTRY_CACHE_KEY = "SYSTE_COUNTRY_CACHE_KEY";
        public const string SYSTEM_COUNTRY_CACHE__JSON_KEY = "SYSTE_COUNTRY_CACHE_JSON_KEY";

        public const string DB_NULL_CONST = "NULL";
        public const byte INSTORE_SEARCH_LIMIT = 6;

        public const string LOYALTY_POINTS = "LoyaltyPoints";
        public const string GIFTCODES_ADDITIONAL = "GiftCodesAdditional";

        public const int DEFAULT_RATING_MAXIMUM = 5;
        public const string PICKUP_FREIGHT_CHARGE_TYPE = "PICK UP";

        public const string REDEMPTION_MULTIPLIER = "Redemption Multiplier";
        public const string PURCHASE_MULTIPLIER = "Purchase Multiplier";

        public const string FEATURED_ITEMS_CACHE_NAME = "FEATURED_ITEMS_CACHE_NAME";

        public const string CLEAR_OTHER_PAYMENT_OPTIONS = "ClearOtherPaymentOptions";
        public const string CLEAR_COUPON_DISCOUNT = "ClearCouponDiscount";
        public const string HAS_NOSTOCK_PHASEDOUT_CART_ITEM = "HasNoStockPhasedOutItem";

        public const string COLUMN_INVOICE_CODE = "InvoiceCode";
    }

    public enum CachingOption
    {
        CacheOnHTTPContext,
        CacheOnHTTPRuntime
    }

    public enum ImageType
    {
        Item = 0,
        Category = 1,
        Manufacturer = 2,
        Department = 4,
        CompanyLogo = 5,
        Attribute = 8
    }

    public enum XMLSectionType
    {
        DISPLAY_PRICE,
        DISPLAY_NOTIFYPRICEDROP,
        DISPLAY_PRICINGLEVEL,
        DISPLAY_ADDTOCARTFORM,
        DISPLAY_STOCKHINT,
        DISPLAY_NOTIFYONITEMAVAIL,
        //Obsolete Code

        //DISPLAY_EMAILPRODUCTTOFRIEND,
        DISPLAY_EXPECTEDSHIPDATE,
        DISPLAY_PRODUCTIMAGE,
        DISPLAY_SUBSTITUTEPRODUCT,
        DISPLAY_ACCESSORIES,
        REGISTER_STOCK_PRODUCT_JSCRIPT,
        DISPLAY_MATRIX_ATTRIBUTES,
        DISPLAY_NAV_LINKS,
        DISPLAY_KIT_DETAILS,
        DISPLAY_PRODUCTLINK,
        DISPLAY_CARTCONTROL,
        DISPLAY_REVIEWS,
        DISPLAY_REVIEWCONTROL,
        DISPLAY_NAV_LINK,
        DISPLAY_CMS_TEMPLATE,
        DISPLAY_CMS_TEMPLATE_FOR_TOPIC,
        DISPLAY_MOBILE_FULLMODE_SWITCHING_LINK,
        DISPLAY_SHAREBOX,
        DISPLAY_COMMENTBOX,
        DISPLAY_FEEDBOX,
        DISPLAY_LIVECHAT,
        FEATURED_ITEMS,
        DISPLAY_NUMCARTITEMS,
        DISPLAY_NUMCARTITEMS_BADGE,
        DISPLAY_NUMCARTITEMS_BADGE_1,
        DISPLAY_NUMCARTITEMS_BADGE_2
    }

    public enum XMLSearchSectionType
    {
        DISPLAY_PRODUCTENTITYLIST
    }

    public enum XMLEntitySectionType
    {
        HEADER_CONTROL,
        SORTING_CONTROL,
        PAGING_CONTROL,
        COMPANY_PAGING_CONTROL,
        FILTER_CONTROL,
        DISPLAY_EXTERNALPAGE,
        KIT_PRICE,
        LOYALTY_POINTS
    }

    public enum XMLDefaultPageSectionType
    {
        GETSPECIALSBOXEXPANDEDRANDOM,
        GET_SPECIALS_BOX_EXPANDED,
        GET_NEWS_BOX_EXPANDED
    }

    public enum XMLProductSectionType
    {
        NAVIGATION_CONTROL,
        SHARE_CONTROL,
        PRICE_CONTROL,
        STOCKHINT_CONTROL,
        IMAGE_CONTROL,
        EXPECTED_SHIPDATE_CONTROL,
        PRICEDROP_NOTIFICATION_CONTROL,
        PRICELEVEL_CONTROL,
        AVAILABILITY_NOTIFICATION_CONTROL,
        SUBSTITUTE_ITEMS_CONTROL,
        ACCESSORIES_ITEMS_CONTROL,
        COMMENT_CONTROL,
        ADDTOCART_FORM_CONTROL,
        ALSOPURCHASED_CONTROL,
        ALSOVIEWED_CONTROL,
        RATING_CONTROL,
        STOREPICKUP_SHOPPING_OPTION,
        MATRIX_CONTROL,
        NON_STOCK_MATRIX_CONTROL,
        LOYALTY_POINTS,
        SKU_CONTROL,
        REVIEW_CONTROL
    }

    public enum GiftRegistryItemType
    { 
        vItem = 0,
        vOption = 1
    }

    public enum EditorDisplayMode
    {
        Simple,
        Advanced
    }

    public enum ImageSyncType
    {
        Mobile,
        Minicart
    }

    public enum RenderType
    {
        ShoppingCart,
        Shipping,
        Payment,
        Review,
        Company
    }

    public enum WidgetType
    {
        Blank,
        Visitors,
        SalesOverview,
        RecentOrders,
        StockAlert,
        NewCustomers,
        Sales,
        StoreSettings
    }

    public enum DateRangeType
    {
        Date,
        Week,
        Month,
        Year
    }

    public enum ImageSize
    {
        Large,
        Medium,
        Icon,
        MiniCart,
        Mobile
    }

    public enum RatingGraphCompositeType
    {
        Empty,
        Half,
        Full
    }

    public enum NotificationStatus
    {
        Success,
        Error,
        Warning
    }

    public enum WebsiteType
    {
        Company,
        Smartbag,
    }

    public static class GatewayErrorCodes
    {
        public const string CARD_CHECK_FAILED_DIGIT = "Digit check failed";
        public const string CARD_CHECK_FAILED_FORMAT = "Invalid CardNumber format";
        public const string CARD_CHECK_FAILED_TYPE = "Could not determine the Credit Card type";
        public const string CARD_CHECK_FAILED_FORMAT2 = "Format cannot be determined";
        public const string CARD_DATE_CHECK_FAILED_INVALID = "Card date invalid";
        public const string CARD_DATE_CHECK_FAILED_EXPIRED = "Card expired";
    }

    public static class StandardNumericFormat
    {
        public const string CURRENCY = "C";
        public const string DECIMAL = "D";
        public const string EXPONENTIAL = "E";
        public const string FIXEDPOINT = "F";
        public const string GENERAL = "G";
        public const string NUMBER = "N";
        public const string PERCENT = "P";
        public const string ROUNDTRIP = "R";
        public const string HEXADECIMAL = "X";
    }
    public static class CustomerCreditType
    {
        public const string CREDIT_MEMO = "Credit Memo";
        public const string GIFT_CARD = "Gift Card";
        public const string GIFT_CERTIFICATE = "Gift Certificate";
    }

    public static class PaymentTerm
    {
        public const string REQUEST_QUOTE = "REQUEST QUOTE";
        public const string PURCHASE_ORDER = "PURCHASE ORDER";
    }

    public enum EmailContent
    {
        Smartbag,
        Company,
        Quote,
        PurchaseOrder
    }
}
