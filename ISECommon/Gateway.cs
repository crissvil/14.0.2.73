// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon
{
    public abstract class Gateway
    {
        #region Variable Declaration

        private string _name = string.Empty;

        #endregion

        #region Constructor
        
        protected Gateway(string name)
        {
            _name = name;
        }

        #endregion

        #region Properties

        #region Name
        
        public string Name
        {
            get { return _name; }
        }

        #endregion

        #endregion

        #region Methods

        public abstract GatewayResponse ProcessCard(string salesOrderCode,
            Customer thisCustomer,
            decimal orderTotal,
            string vendorTransactionCode,
            String TransactionMode,
            Address UseBillingAddress,
            String CardExtraCode,
            Address UseShippingAddress,
            String CAVV,
            String ECI,
            String XID);

        #endregion
        
    }
}
