// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Web;
using System.Globalization;
using System.Drawing;
using System.Drawing.Imaging;

namespace InterpriseSuiteEcommerceCommon.Handlers
{
    public class Watermark : IHttpHandler 
    {
        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {

            context.Response.CacheControl = "private";
            context.Response.Expires = 0;
            context.Response.AddHeader("pragma", "no-cache");

            int SkinID = CommonLogic.QueryStringUSInt("SkinID");
            if (SkinID == 0)
            {
                SkinID = CommonLogic.QueryStringUSInt("SkinID");
            }
            if (SkinID == 0)
            {
                SkinID = 1;
            }

            String LocaleSetting = CommonLogic.QueryStringCanBeDangerousContent("LocaleSetting");

            if (LocaleSetting.IndexOf("<script>", StringComparison.InvariantCultureIgnoreCase) != -1)
            {
                throw new ArgumentException("SECURITY EXCEPTION");
            }
            if (LocaleSetting.Length == 0)
            {
                LocaleSetting = Localization.WebConfigLocale;
            }
            LocaleSetting = Localization.CheckLocaleSettingForProperCase(LocaleSetting);
            String ImgSize = CommonLogic.QueryStringCanBeDangerousContent("size");
            if (ImgSize.Length == 0)
            {
                ImgSize = "icon";
            }

            String EOName = "Product";

            String ImgUrl = String.Empty;

            // NOTE: value should be the counter of the item in the InventoryItem table
            string counter = CommonLogic.QueryStringUSInt("counter").ToString();
            if (CommonLogic.QueryStringCanBeDangerousContent("src").Length == 0)
            {
                ImgUrl = AppLogic.LookupImage(EOName, counter, ImgSize, SkinID, LocaleSetting);
            }
            else
            {
                ImgUrl = HttpUtility.UrlDecode(CommonLogic.QueryStringCanBeDangerousContent("src"));
                if (CommonLogic.QueryStringCanBeDangerousContent("e") == "1")
                {
                    ImgUrl = Security.UnmungeString(ImgUrl);
                }
            }

            String filename = System.IO.Path.GetFileName(ImgUrl);
            System.Drawing.Image imgPhoto = CommonLogic.LoadImage(ImgUrl);
            String CopyrightText = AppLogic.AppConfig("Watermark.CopyrightText");
            String CopyrightImage = AppLogic.AppConfig("Watermark.CopyrightImage." + ImgSize);

            if (AppLogic.AppConfigBool("Watermark.Enabled") && ImgUrl.IndexOf("nopicture") == -1 && (CopyrightText.Length != 0 || CopyrightImage.Length != 0))
            {
                try
                {
                    imgPhoto = CommonLogic.AddWatermark(imgPhoto, CopyrightText, CopyrightImage);
                }
                catch { }
            }

            if (ImgUrl.EndsWith(".jpeg", StringComparison.InvariantCultureIgnoreCase))
            {
                context.Response.ContentType = "image/jpeg";
                EncoderParameters encoderParameters = new EncoderParameters();
                encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, 100L);
                imgPhoto.Save(context.Response.OutputStream, ImageCodecInfo.GetImageEncoders()[1], encoderParameters);
            }
            if (ImgUrl.EndsWith(".jpg", StringComparison.InvariantCultureIgnoreCase))
            {
                context.Response.ContentType = "image/jpeg";
                EncoderParameters encoderParameters = new EncoderParameters();
                encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, 100L);
                imgPhoto.Save(context.Response.OutputStream, ImageCodecInfo.GetImageEncoders()[1], encoderParameters);
            }
            if (ImgUrl.EndsWith(".gif", StringComparison.InvariantCultureIgnoreCase))
            {
                context.Response.ContentType = "image/gif";
                imgPhoto.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
            }
            if (ImgUrl.EndsWith(".png", StringComparison.InvariantCultureIgnoreCase))
            {
                context.Response.ContentType = "image/png";
                imgPhoto.Save(context.Response.OutputStream, System.Drawing.Imaging.ImageFormat.Gif);
            }

            context.Response.AddHeader("Content-Disposition", "inline; filename=\"" + filename + "\"");
            imgPhoto.Dispose();

        }
        
    }
}
