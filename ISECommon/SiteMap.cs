// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Web;
using System.Configuration;
using System.Web.SessionState;
using System.Web.Caching;
using System.Web.Util;
using System.Data;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;
using System.Globalization;

namespace InterpriseSuiteEcommerceCommon
{
    /// <summary>
    /// Summary description for SiteMap1.
    /// </summary>
    public class SiteMap1
    {
        private String m_Contents;

        public String Contents
        {
            get
            {
                return m_Contents;
            }
        }

        public SiteMap1(System.Collections.Generic.Dictionary<string, EntityHelper> EntityHelpers, int SkinID, Customer ThisCustomer)
        {
            bool FromCache = false;
            String CacheName = String.Format("SiteMap1_{0}_{1}", SkinID.ToString(), ThisCustomer.LocaleSetting);
            if (AppLogic.CachingOn)
            {
                m_Contents = (String)HttpContext.Current.Cache.Get(CacheName);
                if (m_Contents != null)
                {
                    FromCache = true;
                }
            }

            if (!FromCache)
            {
                DataSet ds;
                StringBuilder tmpS = new StringBuilder(50000);

                if (AppLogic.AppConfigBool("SiteMap.ShowCategories"))
                {
                    // Categories:
                    String s = AppLogic.LookupHelper("Category").GetEntityULList(string.Empty, ThisCustomer.LocaleSetting, ThisCustomer.AffiliateID, string.Empty, AppLogic.AppConfigBool("SiteMap.ShowProducts") && AppLogic.NumProductsInDB < 250, true, true, "sitemapul", true, string.Empty, String.Empty);
                    if (s.Length != 0)
                    {
                        //tmpS.Append("<b>");
                        //tmpS.Append(AppLogic.GetString("AppConfig.CategoryPromptPlural").ToUpperInvariant());
                        //tmpS.Append("</b>");
                        tmpS.Append(s);
                    }
                }

                if (AppLogic.AppConfigBool("SiteMap.ShowDepartments"))
                {
                    String s = AppLogic.LookupHelper("Department").GetEntityULList(String.Empty, ThisCustomer.LocaleSetting, ThisCustomer.AffiliateID, string.Empty, AppLogic.AppConfigBool("SiteMap.ShowProducts") && AppLogic.NumProductsInDB < 250, true, true, "sitemapul", true, string.Empty, String.Empty);
                    if (s.Length != 0)
                    {
                        tmpS.Append("<b>");
                        tmpS.Append(AppLogic.GetString("AppConfig.DepartmentPromptPlural").ToUpperInvariant());
                        tmpS.Append("</b>");
                        tmpS.Append(s);
                    }
                }

                if (AppLogic.AppConfigBool("SiteMap.ShowManufacturers"))
                {
                    // Manufacturers:
                    String s = AppLogic.LookupHelper("Manufacturer").GetEntityULList(string.Empty, ThisCustomer.LocaleSetting, ThisCustomer.AffiliateID, string.Empty, AppLogic.AppConfigBool("SiteMap.ShowProducts") && AppLogic.NumProductsInDB < 250, false, true, "sitemapul", true, string.Empty, String.Empty);
                    if (s.Length != 0)
                    {
                        tmpS.Append("<b>");
                        tmpS.Append(AppLogic.GetString("AppConfig.ManufacturerPromptPlural").ToUpperInvariant());
                        tmpS.Append("</b>");
                        tmpS.Append(s);
                    }
                }

                if (AppLogic.AppConfigBool("SiteMap.ShowTopics"))
                {
                    // Topics:
                    tmpS.Append("<b>");
                    tmpS.Append(AppLogic.GetString("sitemap.aspx.2").ToUpperInvariant());
                    tmpS.Append("</b>");
                    tmpS.Append("<ul class=\"sitemapul\">\n");

                    ds = DB.GetDS("SELECT * FROM EcommerceWebTopicView with (NOLOCK) WHERE ShowInSiteMap = 1 AND (SkinID IS NULL OR SkinID  = 0 OR SkinID=" + SkinID.ToString() +
                                  " ) AND LocaleSetting=" + DB.SQuote(ThisCustomer.LocaleSetting) + " AND WebSiteCode=" + DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode),
                                  AppLogic.CachingOn, System.DateTime.Now.AddMinutes(AppLogic.CacheDurationMinutes()));
                    
                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        tmpS.Append("<li>");
                        tmpS.Append("<a href=\"" + SE.MakeDriverLink(Convert.ToString(DB.RowFieldByLocale(row, "Name", ThisCustomer.LocaleSetting)).Replace(" ", "-")) + "\">");
                        tmpS.Append(Security.HtmlEncode(DB.RowFieldByLocale(row, "Title", ThisCustomer.LocaleSetting)));
                        tmpS.Append("</a>");
                        tmpS.Append("</li>\n");
                    }
                    ds.Dispose();

                    // File Topics:
                    // create an array to hold the list of files
                    ArrayList fArray = new ArrayList();

                    // get information about our initial directory
                    String SFP = CommonLogic.SafeMapPath("skins/skin_" + SkinID.ToString() + "/template.htm").Replace("template.htm", "");

                    DirectoryInfo dirInfo = new DirectoryInfo(SFP);

                    // retrieve array of files & subdirectories
                    FileSystemInfo[] myDir = dirInfo.GetFileSystemInfos();

                    for (int i = 0; i < myDir.Length; i++)
                    {
                        // check the file attributes

                        // if a subdirectory, add it to the sArray    
                        // otherwise, add it to the fArray
                        if (((Convert.ToUInt32(myDir[i].Attributes) & Convert.ToUInt32(FileAttributes.Directory)) > 0))
                        {
                        }
                        else
                        {
                            bool skipit = false;
                            if (!myDir[i].FullName.EndsWith("htm", StringComparison.InvariantCultureIgnoreCase) || 
                                (myDir[i].FullName.IndexOf("TEMPLATE", StringComparison.InvariantCultureIgnoreCase) != -1) || 
                                (myDir[i].FullName.IndexOf("AFFILIATE_", StringComparison.InvariantCultureIgnoreCase) != -1) || 
                                (myDir[i].FullName.IndexOf(AppLogic.ro_PMMicropay, StringComparison.InvariantCultureIgnoreCase) != -1))
                            {
                                skipit = true;
                            }
                            if (!skipit)
                            {
                                fArray.Add(Path.GetFileName(myDir[i].FullName));
                            }
                        }
                    }

                    if (fArray.Count != 0)
                    {
                        // sort the files alphabetically
                        fArray.Sort(0, fArray.Count, null);
                        for (int i = 0; i < fArray.Count; i++)
                        {
                            tmpS.Append("<li>");
                            tmpS.Append("<a href=\"" + SE.MakeDriverLink(fArray[i].ToString().Replace(".htm", "")) + "\">");
                            tmpS.Append(Security.HtmlEncode(CommonLogic.Capitalize(fArray[i].ToString().Replace(".htm", ""))));
                            tmpS.Append("</a>");
                            tmpS.Append("</li>\n");
                        }
                    }
                    tmpS.Append("</ul>\n");
                }
                m_Contents = tmpS.ToString();
                if (AppLogic.CachingOn)
                {
                    HttpContext.Current.Cache.Insert(CacheName, m_Contents, null, System.DateTime.Now.AddMinutes(AppLogic.CacheDurationMinutes()), TimeSpan.Zero);
                }
            }

        }
    }
}



