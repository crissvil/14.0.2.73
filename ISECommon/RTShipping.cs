// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using System.Globalization;

namespace InterpriseSuiteEcommerceCommon
{
    public partial class RTShipping
    {
        //Collections for holding information about cart items and addresses which we can then use for real time carriers
        public class Shipments : CollectionBase // Data class which holds information about different Packages (a group of items shipping from the same origin)
        {
            private bool s_HasDistributorItems;
            private bool s_HasFreeItems;
            public Shipments()
            {
                s_HasDistributorItems = false;
                s_HasFreeItems = false;
            }

            public bool HasDistributorItems
            {
                get { return this.s_HasDistributorItems; }
                set { this.s_HasDistributorItems = value; }
            }

            public bool HasFreeItems
            {
                get { return this.s_HasFreeItems; }
                set { this.s_HasFreeItems = value; }
            }

            public void AddPackages(Packages packages)
            {
                this.List.Add(packages);
            }

            public Packages this[int index]
            {
                get
                {
                    return (Packages)this.List[index];
                }
            }

            public int PackageCount
            {
                get
                {
                    int counter = 0;
                    foreach (Packages packages in (Shipments)this.List)
                    {
                        foreach (Package p in packages)
                        {
                            counter += (int)CommonLogic.IIF(p.IsShipSeparately, p.Quantity, 1);
                        }
                    }
                    return counter;
                }
            }

        }

        public class Packages : CollectionBase	// Data class which holds the multiples packages information
        {
            private string m_DestinationStateProvince;
            private string m_DestinationZipPostalCode;
            private string m_DestinationCountryCode;
            private bool m_DestinationIsResidential;

            public Packages()
            {
                m_DestinationStateProvince = string.Empty;
                m_DestinationZipPostalCode = string.Empty;
                m_DestinationCountryCode = string.Empty;
                m_DestinationIsResidential = true;
            }

            public bool DestinationIsResidential
            {
                get { return m_DestinationIsResidential; }
                set { m_DestinationIsResidential = value; }
            }

            public string DestinationZipPostalCode
            {
                get { return this.m_DestinationZipPostalCode; }
                set { this.m_DestinationZipPostalCode = value; }
            }

            public string DestinationStateProvince	// Shipment destination State or Province
            {
                get
                {
                    if (m_DestinationStateProvince == "-" || m_DestinationStateProvince == "--" || m_DestinationStateProvince == "ZZ")
                    {
                        return String.Empty;
                    }
                    else
                    {
                        return m_DestinationStateProvince;
                    }
                }
                set { m_DestinationStateProvince = value; }
            }

            public string DestinationCountryCode
            {
                get { return this.m_DestinationCountryCode; }
                set { this.m_DestinationCountryCode = value; }
            }

            public void AddPackage(Package package)
            {
                this.List.Add(package);
            }


            public Package this[int index]
            {
                get
                {
                    return (Package)this.List[index];
                }
            }
        }

        public class Package	// Data class which holds information about a single package
        {
            private Decimal m_Weight;
            private Decimal m_Height;
            private Decimal m_Length;
            private Decimal m_Width;
            private bool m_Insured;
            private Decimal m_InsuredValue;
            private int m_PackageId;
            private Boolean m_IsFreeShipping;
            private Decimal m_Quantity;
            private Boolean m_IsPrePacked;
            private Boolean m_IsOverSized;
            private String m_OverSizedShippingMethodCode;
            private String m_OverSizedItemName;

            public Decimal Quantity
            {
                get { return m_Quantity; }
                set { m_Quantity = value; }
            }

            public bool IsShipSeparately
            {
                get { return (m_IsPrePacked || m_IsOverSized); }
                //set { m_IsShipSeparately = value; }
            }

            public Boolean IsPrePacked
            {
                get { return m_IsPrePacked; }
                set { m_IsPrePacked = value; }
            }

            public Boolean IsOverSized
            {
                get { return m_IsOverSized; }
                set { m_IsOverSized = value; }
            }

            public Boolean IsFreeShipping
            {
                get { return m_IsFreeShipping; }
                set { m_IsFreeShipping = value; }
            }

            public Decimal InsuredValue
            {
                get { return m_InsuredValue; }
                set { m_InsuredValue = value; }
            }

            public int PackageId
            {
                get { return m_PackageId; }
                set { m_PackageId = value; }
            }

            public bool Insured
            {
                get { return m_Insured; }
                set { m_Insured = value; }
            }

            public Decimal Width
            {
                get { return m_Width; }
                set { m_Width = value; }
            }

            public Decimal Weight
            {
                get { return m_Weight; }
                set { m_Weight = value; }
            }

            public Decimal Height
            {
                get { return m_Height; }
                set { m_Height = value; }
            }

            public Decimal Length
            {
                get { return m_Length; }
                set { m_Length = value; }
            }

            public String OverSizedShippingMethodCode
            {
                get { return m_OverSizedShippingMethodCode; }
                set { m_OverSizedShippingMethodCode = value; }
            }

            public String OverSizedItemName
            {
                get { return m_OverSizedItemName; }
                set { m_OverSizedItemName = value; }
            }

            private String _overSizeItemCode;
            private String _unitMeasureCode;

            public String OverSizedItemCode
            {
                get { return _overSizeItemCode; }
                set { _overSizeItemCode = value; }
            }

            public String UnitMeasureCode
            {
                get { return _unitMeasureCode; }
                set { _unitMeasureCode = value; }
            }

            public Package()
            {
            }
        }
    }

}
