// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Web;
using System.Web.Security;
using System.Data;
using System.Text;
using System.Xml;
using System.Collections;
using System.Globalization;

namespace InterpriseSuiteEcommerceCommon
{
    /// <summary>
    /// </summary>
    public static class Currency
    {

        public static string m_LastRatesResponseXml = string.Empty;
        public static string m_LastRatesTransformedXml = string.Empty;

        //Modified for threading purposes
        private static XmlDocument RatesDoc
        {
            get
            {
                XmlDocument d = null;
                if (HttpContext.Current != null)
                {
                    d = (XmlDocument)HttpContext.Current.Cache.Get("CurrencyDoc");
                    if (d == null)
                    {
                        d = DB.GetSqlXmlDoc("SELECT * FROM SystemCurrency with (NOLOCK) ORDER BY CurrencyCode ASC,CurrencyDescription FOR XML AUTO", null);

                        int cacheMinutes = AppLogic.AppConfigUSInt("Localization.CurrencyCacheMinutes");
                        if (cacheMinutes == 0)
                        {
                            cacheMinutes = 60;
                        }
                        HttpContext.Current.Cache.Insert("CurrencyDoc", d, null, System.DateTime.Now.AddMinutes(cacheMinutes), TimeSpan.Zero);
                    }
                }
                else 
                {
                    d = DB.GetSqlXmlDoc("SELECT * FROM SystemCurrency with (NOLOCK) ORDER BY CurrencyCode ASC,CurrencyDescription FOR XML AUTO", null);
                }
                return d;
            }
        }

        public static void GetLiveRates()
        {
            string PN = AppLogic.AppConfig("Localization.CurrencyFeedXmlPackage");
            if (PN.Length != 0)
            {
                try
                {
                    using (XmlPackage2 p = new XmlPackage2(PN))
                    {
                        m_LastRatesResponseXml = p.XmlDataDocument.InnerXml;
                        m_LastRatesTransformedXml = p.TransformString();
                        if (m_LastRatesTransformedXml.Length != 0)
                        {
                            // update master db table:
                            XmlDocument d = new XmlDocument();
                            d.LoadXml(m_LastRatesTransformedXml);
                            foreach (XmlNode n in d.SelectNodes("//currency"))
                            {
                                string CurrencyCode = XmlCommon.XmlAttribute(n, "code");
                                string rate = XmlCommon.XmlAttribute(n, "rate");
                                DB.ExecuteSQL("update Currency set ExchangeRate=" + rate + ", WasLiveRate=1, LastUpdated=getdate() where CurrencyCode=" + DB.SQuote(CurrencyCode));
                            }
                        }
                    }
                    
                    FlushCache(); // flush anyway for safety
                }
                catch(Exception ex)
                {
                    try
                    {
                        AppLogic.SendMail(AppLogic.AppConfig("StoreName") + " Currency.GetLiveRates Failure", "Occurred at: " + System.DateTime.Now.ToString() + CommonLogic.GetExceptionDetail(ex,"<br>"), false, AppLogic.AppConfig("MailMe_FromAddress"), AppLogic.AppConfig("MailMe_FromName"), AppLogic.AppConfig("MailMe_ToAddress"), AppLogic.AppConfig("MailMe_ToName"), String.Empty, AppLogic.AppConfig("MailMe_Server"));
                    }
                    catch { }
                }
            }
        }

        public static void FlushCache()
        {
            try
            {
                HttpContext.Current.Cache.Remove("CurrencyDoc");
            }
            catch { }
        }

        public static bool isValidCurrencyCode(string CurrencyCode)
        {
            string tmpS = string.Empty;
            XmlNode n = RatesDoc.SelectSingleNode("//Currency[@CurrencyCode='" + CurrencyCode + "']");
            return (n != null);

        }

        public static string GetCurrencyCode(string Name)
        {
            if (Name.Length == 0)
            {
                throw new ArgumentException("Invalid Currency Name (empty string)");
            }
            String tmpS = string.Empty;
            XmlNode n = RatesDoc.SelectSingleNode("//Currency[@Name=" + Name + "]");
            if (n != null)
            {
                tmpS = XmlCommon.XmlAttribute(n, "CurrencyCode");
            }
            return tmpS;
        }

        public static string GetCurrencyCode(int CurrencyID)
        {
            string tmpS = string.Empty;
            XmlNode n = RatesDoc.SelectSingleNode("//Currency[@CurrencyID=" + CurrencyID.ToString() + "]");
            if (n != null)
            {
                tmpS = XmlCommon.XmlAttribute(n, "CurrencyCode");
            }
            return tmpS;
        }

        public static int GetCurrencyID(string CurrencyCode)
        {
            if (CurrencyCode.Length == 0)
            {
                throw new ArgumentException("Invalid CurrencyCode (empty string)");
            }
            int tmpS = 0;
            XmlNode n = RatesDoc.SelectSingleNode("//Currency[@CurrencyCode=" + CommonLogic.SQuote(CurrencyCode) + "]");
            if (n != null)
            {
                tmpS = XmlCommon.XmlAttributeNativeInt(n, "CurrencyID");
            }
            return tmpS;
        }

        public static decimal GetExchangeRate(string CurrencyCode)
        {
            if (CurrencyCode.Length == 0)
            {
                throw new ArgumentException("Invalid CurrencyCode (empty string)");
            }
            Decimal tmpS = System.Decimal.Zero;
            XmlNode n = RatesDoc.SelectSingleNode("//SystemCurrency[@CurrencyCode=" + CommonLogic.SQuote(CurrencyCode) + "]");
            if (n != null)
            {
                tmpS = XmlCommon.XmlAttributeUSDecimal(n, "ExchangeRate");
            }
            return tmpS;
        }

        public static string GetFeedReferenceCurrencyCode()
        {
            return AppLogic.AppConfig("Localization.CurrencyFeedBaseRateCurrencyCode");
        }

        public static void SetReferenceCurrencyCode(string CurrencyCode, bool UpdateRates)
        {
            if (CurrencyCode.Length == 0)
            {
                throw new ArgumentException("Invalid CurrencyCode (empty string)");
            }
            AppLogic.SetAppConfig(0,"Localization.CurrencyFeedBaseRateCurrencyCode", CurrencyCode);
            if (UpdateRates)
            {
                GetLiveRates();
            }
        }

        public static string GetFeedUrl()
        {
            return AppLogic.AppConfig("Localization.CurrencyFeedUrl");
        }

        public static void SetFeedUrl(string NewUrl, bool UpdateRates)
        {
            AppLogic.SetAppConfig(0,"Localization.CurrencyFeedUrl", NewUrl);
            if (NewUrl.Length != 0 && UpdateRates)
            {
                GetLiveRates();
            }
        }

        public static string GetName(int CurrencyID)
        {
            string tmpS = string.Empty;
            XmlNode n = RatesDoc.SelectSingleNode("//Currency[@CurrencyID=" + CurrencyID.ToString() + "]");
            if (n != null)
            {
                tmpS = XmlCommon.XmlAttribute(n, "Name");
            }
            return tmpS;
        }

        public static string GetName(string CurrencyCode)
        {
            if (CurrencyCode.Length == 0)
            {
                throw new ArgumentException("Invalid CurrencyCode (empty string)");
            }
            String tmpS = string.Empty;
            XmlNode n = RatesDoc.SelectSingleNode("//Currency[@CurrencyCode=" + CommonLogic.SQuote(CurrencyCode) + "]");
            if (n != null)
            {
                tmpS = XmlCommon.XmlAttribute(n, "Name");
            }
            return tmpS;
        }

        public static string GetDisplayLocaleFormat(string CurrencyCode)
        {
            if (CurrencyCode.Length == 0)
            {
                throw new ArgumentException("Invalid CurrencyCode (empty string)");
            }
            string tmpS = string.Empty;
            XmlNode n = RatesDoc.SelectSingleNode("//Currency[@CurrencyCode=" + CommonLogic.SQuote(CurrencyCode) + "]");
            if (n != null)
            {
                tmpS = XmlCommon.XmlAttribute(n, "DisplayLocaleFormat");
            }
            return tmpS;
        }

        public static string GetDisplaySpec(string CurrencyCode)
        {
            if (CurrencyCode.Length == 0)
            {
                throw new ArgumentException("Invalid CurrencyCode (empty string)");
            }
            string tmpS = string.Empty;
            XmlNode n = RatesDoc.SelectSingleNode("//Currency[@CurrencyCode=" + CommonLogic.SQuote(CurrencyCode) + "]");
            if (n != null)
            {
                tmpS = XmlCommon.XmlAttribute(n, "DisplaySpec");
            }
            return tmpS;
        }

        public static Decimal Convert(Decimal SourceValue, string SourceCurrencyCode, string TargetCurrencyCode)
        {
            Decimal result = SourceValue;
            if (result != System.Decimal.Zero && SourceCurrencyCode != TargetCurrencyCode)
            {
                result = ConvertToBaseCurrency(result, SourceCurrencyCode);
                result = ConvertFromBaseCurrency(result, TargetCurrencyCode);
            }
            return result;
        }

        public static Decimal ConvertToBaseCurrency(Decimal SourceValue, string SourceCurrencyCode)
        {
            Decimal result = SourceValue;
            if (result != System.Decimal.Zero && SourceCurrencyCode != GetFeedReferenceCurrencyCode())
            {
                Decimal ExchangeRate = GetExchangeRate(SourceCurrencyCode);
                if (ExchangeRate == System.Decimal.Zero)
                {
                    throw new ArgumentException("Exchange Rate Not Found for Currency=" + SourceCurrencyCode);
                }
                result = result / ExchangeRate;
            }
            return result;
        }

        public static Decimal ConvertFromBaseCurrency(Decimal SourceValue, string TargetCurrencyCode)
        {
            Decimal result = SourceValue;
            if (result != System.Decimal.Zero && TargetCurrencyCode != GetFeedReferenceCurrencyCode())
            {
                Decimal ExchangeRate = GetExchangeRate(TargetCurrencyCode);
                if (ExchangeRate == System.Decimal.Zero)
                {
                    throw new ArgumentException("Exchange Rate Not Found for Currency=" + TargetCurrencyCode);
                }
                result = result * ExchangeRate;
            }
            return result;
        }

        public static string GetDefaultCurrency()
        {
            return Localization.GetPrimaryCurrency();
        }

        public static string ValidateCurrencySetting(string Currency)
        {
            string tmp = Localization.GetPrimaryCurrency();
            if (isValidCurrencyCode(Currency))
            {
                tmp = Currency;
            }
            return Localization.CheckCurrencySettingForProperCase(tmp);
        }
        // this DOES NOT apply any exchange rates!!
        // it is FORMATTING ONLY!
        public static string ToString(decimal amt, string TargetCurrencyCode)
        {
            return Localization.CurrencyStringForDisplayWithoutExchangeRate(amt, TargetCurrencyCode);
        }


        public static int NumPublishedCurrencies()
        {
            return RatesDoc.SelectNodes("//Currency[@Published = 1]").Count;
        }

        public static String GetSelectList(String SelectName, string OnChangeHandler, string CssClass, string SelectedCurrencyCode)
        {
            var tmpS = new StringBuilder(4096);
            tmpS.Append("<select size=\"1\" id=\"" + SelectName + "\" name=\"" + SelectName + "\"");
            if (OnChangeHandler.Length != 0)
            {
                tmpS.Append(" onChange=\"" + OnChangeHandler + "\"");
            }
            if (CssClass.Length != 0)
            {
                tmpS.Append(" class=\"" + CssClass + "\"");
            }
            tmpS.Append(">");
            foreach (XmlNode n in RatesDoc.SelectNodes("//Currency[@Published = 1]"))
            {
                string cc = XmlCommon.XmlAttribute(n, "CurrencyCode");
                tmpS.Append("<option value=\"" + cc + "\" " + CommonLogic.IIF(SelectedCurrencyCode == cc, " selected ", "") + ">" + cc + " (" + XmlCommon.XmlAttribute(n, "Name") + ")</option>");
            }
            tmpS.Append("</select>");
            return tmpS.ToString();
        }

        public static ArrayList getCurrencyList()
        {
            var list = new ArrayList();

            foreach (XmlNode n in RatesDoc.SelectNodes("//Currency[@Published = 1]"))
            {
                int cID = System.Convert.ToInt32(XmlCommon.XmlAttribute(n, "CurrencyID"));
                string cc = XmlCommon.XmlAttribute(n, "CurrencyCode");
                string cn = XmlCommon.XmlAttribute(n, "Name");
                var item = new ListItemClass();
                item.Item = cc + " (" + cn + ")";
                item.Value = cID;
                list.Add(item);
            }

            return list;
        }

        // Interprise Integration
        public static string GetSymbol(string currencyCode)
        {
            string xpath = string.Format("//SystemCurrency[@CurrencyCode='{0}']", currencyCode);
            XmlNode currencyNode = RatesDoc.SelectSingleNode(xpath);
            if (currencyNode != null)
            {
                return (currencyNode.Attributes["Symbol"] != null) ? currencyNode.Attributes["Symbol"].Value : string.Empty;
            }

            return string.Empty;
        }

        // Interprise Integration
        public static NumberFormatInfo GetCurrencyFormat(string currencyCode)
        {
            var currencyFormat = NumberFormatInfo.CurrentInfo.Clone() as NumberFormatInfo;
            string xpath = string.Format("//SystemCurrency[@CurrencyCode='{0}']", currencyCode );
            XmlNode currencyNode = RatesDoc.SelectSingleNode(xpath);
            if (currencyNode != null) {
                XmlAttributeCollection currencyAttributes = currencyNode.Attributes;
                currencyFormat.CurrencySymbol = currencyAttributes["Symbol"].Value;
                currencyFormat.CurrencyPositivePattern = int.Parse(currencyAttributes["CurrencyPositivePattern"].Value);
                currencyFormat.CurrencyNegativePattern = int.Parse(currencyAttributes["CurrencyNegativePattern"].Value);
                currencyFormat.CurrencyDecimalSeparator = currencyAttributes["CurrencyDecimalSeparator"].Value;
                currencyFormat.CurrencyDecimalDigits = int.Parse(currencyAttributes["CurrencyDecimalDigits"].Value);
                currencyFormat.CurrencyGroupSeparator = currencyAttributes["CurrencyGroupSeparator"].Value;
                currencyFormat.CurrencyGroupSizes = new int[] { int.Parse(currencyAttributes["CurrencyGroupSizes"].Value) };
            }            

            return currencyFormat;
        }

        public static string GetHomeCurrency()
        {
            string homeCurrency = string.Empty;
            string xpath = string.Format("//SystemCurrency[@IsHomeCurrency=1]");
            XmlNode homeCurrencyNode = RatesDoc.SelectSingleNode(xpath);
            if (homeCurrencyNode != null)
            {
                homeCurrency = homeCurrencyNode.Attributes["CurrencyCode"].Value;
            }
            else
            {
                throw new InvalidOperationException("Home Currency not found!!!");
            }

            return homeCurrency;
        }

    }
}
