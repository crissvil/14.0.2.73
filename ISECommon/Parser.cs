// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------

using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using InterpriseSuiteEcommerceCommon.Tool;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon;

namespace InterpriseSuiteEcommerceCommon
{
    public delegate void RenderHeaderDelegate(object sender, TextWriter writer);

    /// <summary>
    /// Summary description for Parser.
    /// </summary>
    public class Parser
    {
        // the ise string parser, for skin files and XmlPackages
        // replaces found tokenm_S.

        private Customer m_ThisCustomer = null;
        private int m_SkinID = 1;
        private System.Collections.Generic.Dictionary<string, EntityHelper> m_EntityHelpers;

        //SMARTBAG Optimization
        //Skip instantiating other entities not used by SGE to improve performance
        //static private readonly String[] readonly_SupportedEntities = { "Category", "Department", "Manufacturer" };
        static private readonly String[] readonly_SupportedEntities = { "Category" };

        private string RegExString = "\\(\\!(\\w+)(?:\\s(?:(\\w*)=(?:'|\")(.*?)(?:\"|'))?)*\\!\\)";
        private MatchEvaluator m_CmdMatchEval;
        private int m_CacheMinutes = 0;

        // static tokes are the same from one page request to the next, regardless of who's logged in, viewing the page, 
        // and what the active locale is, etc
        // these are put in application cache, if caching is enabled
        private Hashtable m_StaticTokens = null;

        // dynamic tokens vary on EACH page request, due to live customer state, etc.
        // these are not cached, but built up the first time a parser object is created on the page request.
        private Hashtable m_DynamicTokens = null;

        public event RenderHeaderDelegate RenderHeader;

        protected virtual void OnRenderHeader(TextWriter writer)
        {
            if (RenderHeader != null)
            {
                RenderHeader(this, writer);
            }
        }

        public Parser(int SkinID, Customer cust)
            : this(null, SkinID, cust)
        { }

        public Parser(System.Collections.Generic.Dictionary<string, EntityHelper> EntityHelpers, int SkinID, Customer cust)
        {
            m_ThisCustomer = cust;
            if (m_ThisCustomer == null)
            {
                m_ThisCustomer = Customer.Current;
            }
            m_SkinID = SkinID;
            m_EntityHelpers = EntityHelpers;
            m_CmdMatchEval = new MatchEvaluator(CommandMatchEvaluator);
            m_CacheMinutes = AppLogic.CacheDurationMinutes();
        }

        public String ReplacePageStaticTokens(String s)
        {
            if (m_StaticTokens == null)
            {
                BuildPageStaticTokens();
            }
            var en = m_StaticTokens.GetEnumerator();
            var result = new StringBuilder(s, s.Length * 4);
            while (en.MoveNext())
            {
                result.Replace(en.Key.ToString(), en.Value.ToString());
            }
            // PROCESS REGEX TYPE TOKENS HERE, e.g. AppConfig, Param, Topic, XmlPackage, Localize, etc...
            return Regex.Replace(result.ToString(), RegExString, m_CmdMatchEval, RegexOptions.Compiled);
        }

        public String ReplacePageDynamicTokens(String s)
        {
            if (m_DynamicTokens == null)
            {
                BuildPageDynamicTokens();
            }

            var result = new StringBuilder(s);
            var en = m_DynamicTokens.GetEnumerator();

            while (en.MoveNext())
            {
                result.Replace(en.Key.ToString(), en.Value.ToString());
            }
            // PROCESS REGEX TYPE TOKENS HERE, e.g. AppConfig, Param, Topic, XmlPackage, Localize, etc...
            return Regex.Replace(result.ToString(), RegExString, m_CmdMatchEval, RegexOptions.Compiled);
        }

        // these are the same for ALL page requests since app start!
        public void BuildPageStaticTokens()
        {
            string m_CacheName = string.Empty;
            string localeSetting = ThisCustomer.LocaleSetting; 
            string currencyCode = ThisCustomer.CurrencyCode;

            if (AppLogic.CachingOn)
            {
                m_CacheName = String.Format("StaticTokens_{0}_{1}_{2}_{3}_{4}", SkinID.ToString(), localeSetting, currencyCode, ThisCustomer.AffiliateID, ThisCustomer.VATSettingReconciled);
                m_StaticTokens = (Hashtable)HttpContext.Current.Cache.Get(m_CacheName);
            }
            if (m_StaticTokens == null)
            {
                m_StaticTokens = new Hashtable();
                m_StaticTokens.Add("(!STORE_VERSION!)", CommonLogic.GetVersion());
                m_StaticTokens.Add("(!COPYRIGHTYEARS!)", AppLogic.AppConfig("StartingCopyrightYear") + "-" + DateTime.Now.Year.ToString());
                m_StaticTokens.Add("(!SKINID!)", SkinID.ToString());
                m_StaticTokens.Add("(!RIGHTCOL!)", "The RIGHTCOL token is no longer supported. You should put the right column you want directly into your skin templtae.ascx design where you want it");
                m_StaticTokens.Add("(!SITENAME!)", AppLogic.AppConfig("StoreName"));
                m_StaticTokens.Add("(!SITE_NAME!)", AppLogic.AppConfig("StoreName"));
                m_StaticTokens.Add("(!STORELOCALE!)", localeSetting);
                m_StaticTokens.Add("(!LOCALESETTING!)", localeSetting);
                m_StaticTokens.Add("(!CUSTOMERLOCALE!)", localeSetting);
                m_StaticTokens.Add("(!SEARCH_BOX!)", AppLogic.GetSearchBox(SkinID, ThisCustomer.LocaleSetting));

                string helperBoxNoFrame = AppLogic.GetHelpBox(SkinID, false, localeSetting, null);
                m_StaticTokens.Add("(!HELPBOX!)", AppLogic.GetHelperBoxWithFrame(helperBoxNoFrame, SkinID));
                m_StaticTokens.Add("(!HELPBOX_CONTENTS!)", helperBoxNoFrame);

                m_StaticTokens.Add("(!NEWS_SUMMARY!)", AppLogic.GetNewsSummary(3));
                m_StaticTokens.Add("(!CATEGORY_PROMPT!)", CommonLogic.IIF(AppLogic.GetString("AppConfig.CategoryPromptPlural").Length != 0, AppLogic.GetString("AppConfig.CategoryPromptPlural"), AppLogic.GetString("skinbase.cs.3")));
                m_StaticTokens.Add("(!CATEGORY_PROMPT_SINGULAR!)", CommonLogic.IIF(AppLogic.GetString("AppConfig.CategoryPromptSingular").Length != 0, AppLogic.GetString("AppConfig.CategoryPromptSingular"), 
                    AppLogic.GetString("skinbase.cs.3")));
                m_StaticTokens.Add("(!CATEGORY_PROMPT_PLURAL!)", CommonLogic.IIF(AppLogic.GetString("AppConfig.CategoryPromptPlural").Length != 0, AppLogic.GetString("AppConfig.CategoryPromptPlural"), 
                    AppLogic.GetString("skinbase.cs.2")).ToUpperInvariant());
                m_StaticTokens.Add("(!SECTION_PROMPT!)", CommonLogic.IIF(AppLogic.GetString("AppConfig.DepartmentPromptPlural").Length != 0, AppLogic.GetString("AppConfig.DepartmentPromptPlural"), AppLogic.GetString("skinbase.cs.2")));
                m_StaticTokens.Add("(!SECTION_PROMPT_SINGULAR!)", CommonLogic.IIF(AppLogic.GetString("AppConfig.DepartmentPromptSingular").Length != 0, AppLogic.GetString("AppConfig.DepartmentPromptSingular"), 
                    AppLogic.GetString("skinbase.cs.2")));
                m_StaticTokens.Add("(!SECTION_PROMPT_PLURAL!)", CommonLogic.IIF(AppLogic.GetString("AppConfig.DepartmentPromptPlural").Length != 0, AppLogic.GetString("AppConfig.DepartmentPromptPlural"), 
                    AppLogic.GetString("skinbase.cs.1")).ToUpperInvariant());
                m_StaticTokens.Add("(!MANUFACTURER_PROMPT!)", CommonLogic.IIF(AppLogic.GetString("AppConfig.ManufacturerPromptPlural").Length != 0, AppLogic.GetString("AppConfig.ManufacturerPromptPlural"), AppLogic.GetString("skinbase.cs.3")));
                m_StaticTokens.Add("(!MANUFACTURER_PROMPT_SINGULAR!)", CommonLogic.IIF(AppLogic.GetString("AppConfig.ManufacturerPromptSingular").Length != 0, AppLogic.GetString("AppConfig.ManufacturerPromptSingular"), AppLogic.GetString("skinbase.cs.3")));
                m_StaticTokens.Add("(!MANUFACTURER_PROMPT_PLURAL!)", CommonLogic.IIF(AppLogic.GetString("AppConfig.ManufacturerPromptPlural").Length != 0, 
                    AppLogic.GetString("AppConfig.ManufacturerPromptPlural"), AppLogic.GetString("skinbase.cs.2")).ToUpperInvariant());
                m_StaticTokens.Add("(!SOCIALMEDIA_FEEDBOX!)", AppLogic.GetSocialMediaFeedBox(SkinID, localeSetting));

                m_StaticTokens.Add("(!GA_ACCOUNT!)", AppLogic.AppConfig("GoogleAnalytics.TrackingCode"));
                m_StaticTokens.Add("(!GA_PAGE_TRACKING!)", AppLogic.AppConfig("GoogleAnalytics.PageTracking"));
                m_StaticTokens.Add("(!GA_ECOMMERCE_TRACKING!)", AppLogic.AppConfig("GoogleAnalytics.ConversionTracking"));

                m_StaticTokens.Add("(!ADDRESS_VERIFICATION_DIALOG_LISTING!)", AppLogic.RenderAddressVerificationDialogHTML(true));
                m_StaticTokens.Add("(!ADDRESS_VERIFICATION_DIALOG_OPTIONS!)", AppLogic.RenderAddressVerificationDialogHTML(false));
                
                m_StaticTokens.Add("(!BUY_SAFE_SEAL!)", AppLogic.GetBuySafeSealScript());
                m_StaticTokens.Add("(!BUY_SAFE_KICKER!)", AppLogic.GetBuySafeKicker());

                foreach (String EntityName in AppLogic.ro_SupportedEntities)
                {
                    //SMARTBAG Optimization
                    //Load only entities supported on specific website type
                    String ENU = EntityName.ToUpperInvariant();
                    StringBuilder tmpSx = new StringBuilder(4096);
                    EntityHelper Helper = null;
                    if (CommonLogic.GetWebsiteType() == WebsiteType.Company && EntityName == "Company")
                    {
                        Helper = AppLogic.LookupHelper(EntityName);
                        m_StaticTokens.Add("(!" + ENU + "_BROWSE_BOX!)", Helper.GetEntityBrowseBox(SkinID, localeSetting));
                    }
                    else if (CommonLogic.GetWebsiteType() == WebsiteType.Smartbag && EntityName != "Company")
                    {
                        Helper = AppLogic.LookupHelper(EntityName);
                        m_StaticTokens.Add("(!" + ENU + "_BROWSE_BOX!)", Helper.GetEntityBrowseBox(SkinID, localeSetting));
                    }
                }
            }

            if (AppLogic.CachingOn)
            {
                HttpContext.Current.Cache.Insert(m_CacheName, m_StaticTokens, null, System.DateTime.Now.AddMinutes(m_CacheMinutes), TimeSpan.Zero);
            }
        }

        // these can change on EVERY page request!!
        public void BuildPageDynamicTokens()
        {
            if (m_DynamicTokens == null)
            {
                // page/customer specific items (that may change every page):
                m_DynamicTokens = new Hashtable();

                string script = string.Empty;
                if (RenderHeader != null)
                {
                    var writer = new StringWriter();
                    OnRenderHeader(writer);
                    script = writer.ToString();
                }

                m_DynamicTokens.Add("(!JAVASCRIPT_INCLUDES!)", script);

                if (AppLogic.NumLocaleSettingsInstalled() < 2)
                {
                    m_DynamicTokens.Add("(!COUNTRYDIVVISIBILITY!)", "hidden");
                    m_DynamicTokens.Add("(!COUNTRYDIVDISPLAY!)", "none");
                    m_DynamicTokens.Add("(!COUNTRYSELECTLIST!)", String.Empty);
                }
                else
                {
                    m_DynamicTokens.Add("(!COUNTRYDIVVISIBILITY!)", "visible");
                    m_DynamicTokens.Add("(!COUNTRYDIVDISPLAY!)", "inline");
                    m_DynamicTokens.Add("(!COUNTRYSELECTLIST!)", AppLogic.GetCountrySelectList(ThisCustomer.LocaleSetting));
                }

                if (Currency.NumPublishedCurrencies() < 2)
                {
                    m_DynamicTokens.Add("(!CURRENCYDIVVISIBILITY!)", "hidden");
                    m_DynamicTokens.Add("(!CURRENCYDIVDISPLAY!)", "none");
                    m_DynamicTokens.Add("(!CURRENCYSELECTLIST!)", String.Empty);
                }
                else
                {
                    m_DynamicTokens.Add("(!CURRENCYDIVVISIBILITY!)", "visible");
                    m_DynamicTokens.Add("(!CURRENCYDIVDISPLAY!)", "inline");
                    m_DynamicTokens.Add("(!CURRENCYSELECTLIST!)", AppLogic.GetCurrencySelectList(ThisCustomer));
                }

                //SMARTBAG Optimization
                //foreach (String EntityName in readonly_SupportedEntities)
                //{
                //    string ENU = EntityName.ToUpperInvariant();
                //    var Helper = AppLogic.LookupHelper(m_EntityHelpers, EntityName);

                //    m_DynamicTokens.Add("(!ADVANCED_" + ENU + "_BROWSE_BOX!)", Helper.GetAdvancedEntityBrowseBox(ThisCustomer, SkinID));
                //}

                if (AppLogic.VATIsEnabled() && AppLogic.AppConfigBool("VAT.AllowCustomerToChooseSetting"))
                {
                    m_DynamicTokens.Add("(!VATDIVVISIBILITY!)", "visible");
                    m_DynamicTokens.Add("(!VATDIVDISPLAY!)", "inline");
                    m_DynamicTokens.Add("(!VATSELECTLIST!)", AppLogic.GetVATSelectList(ThisCustomer));
                }
                else
                {
                    m_DynamicTokens.Add("(!VATDIVVISIBILITY!)", "hidden");
                    m_DynamicTokens.Add("(!VATDIVDISPLAY!)", "none");
                    m_DynamicTokens.Add("(!VATSELECTLIST!)", String.Empty);
                }

                if (AppLogic.AppConfigBool("Polls.Enabled"))
                {
                    string PollString = string.Empty;
                    int CategoryID = CommonLogic.QueryStringUSInt("categoryid");
                    int DepartmentID = CommonLogic.QueryStringUSInt("departmentid");

                    // polls are assigned to specific categories or sections via Admin > Misc > More > Manage Polls
                    // they are not displayed on non-category or non-section pages. These include product, topic, home, and checkout pages.
                    if (CategoryID != 0 || DepartmentID != 0)
                    {
                        if (Poll.HasAnyPollNotVotedByThisCustomerYet(ThisCustomer))
                        {
                            PollString = AppLogic.GetPollBox(ThisCustomer, string.Empty, false);
                        }
                    }
                    m_DynamicTokens.Add("(!POLL!)", PollString);
                }
                else
                {
                    m_DynamicTokens.Add("(!POLL!)", string.Empty);
                }

                if (!ThisCustomer.IsRegistered)
                {
                    m_DynamicTokens.Add("(!SUBSCRIPTION_EXPIRATION!)", AppLogic.ro_NotApplicable);
                }
                else
                {
                    if (ThisCustomer.SubscriptionExpiresOn == System.DateTime.MinValue)
                    {
                        m_DynamicTokens.Add("(!SUBSCRIPTION_EXPIRATION!)", "Expired");
                    }
                    else
                    {
                        m_DynamicTokens.Add("(!SUBSCRIPTION_EXPIRATION!)", Localization.ToNativeShortDateString(ThisCustomer.SubscriptionExpiresOn));
                    }
                }

                m_DynamicTokens.Add("(!PAGEURL!)", HttpContext.Current.Server.UrlEncode(CommonLogic.GetThisPageName(false) + "?" + CommonLogic.ServerVariables("QUERY_STRING")));

                string randomNum = CommonLogic.GetRandomNumber(1, 7).ToString();
                m_DynamicTokens.Add("(!RANDOM!)", randomNum);
                m_DynamicTokens.Add("(!HDRID!)", randomNum);
                m_DynamicTokens.Add("(!INVOCATION!)", HttpContext.Current.Server.HtmlEncode(CommonLogic.PageInvocation()));
                m_DynamicTokens.Add("(!REFERRER!)", HttpContext.Current.Server.HtmlEncode(CommonLogic.PageReferrer()));

                var tmp = new StringBuilder(4096);
                tmp.Append("<!--\n");
                tmp.Append("PAGE INVOCATION: " + CommonLogic.PageInvocation().ToHtmlEncode() + "\n");
                tmp.Append("PAGE REFERRER: " + CommonLogic.PageReferrer().ToHtmlEncode() + "\n");
                tmp.Append("STORE LOCALE: " + Localization.CompanyLocale() + "\n");
                tmp.Append("STORE CURRENCY: " + Localization.GetPrimaryCurrency() + "\n");
                tmp.Append("CUSTOMER ID: " + ThisCustomer.CustomerID.ToString() + "\n");
                tmp.Append("AFFILIATE ID: " + ThisCustomer.AffiliateID.ToString() + "\n");
                tmp.Append("CUSTOMER LOCALE: " + ThisCustomer.LocaleSetting + "\n");
                tmp.Append("CURRENCY SETTING: " + ThisCustomer.CurrencyCode + "\n");
                tmp.Append("STORE VERSION: " + CommonLogic.GetVersion() + "\n");
                tmp.Append("CACHE MENUS: " + AppLogic.AppConfigBool("CacheMenus").ToString() + "\n");
                tmp.Append("-->\n");
                m_DynamicTokens.Add("(!PAGEINFO!)", tmp.ToString());

                bool IsRegistered = CommonLogic.IIF(ThisCustomer != null, ThisCustomer.IsRegistered, false);
                string tmpS = string.Empty;

                if (IsRegistered)
                {
                    tmpS = AppLogic.GetString("skinbase.cs.1") + " <a class=\"username\" href=\"account.aspx\">" + ThisCustomer.FullName.ToHtmlEncode() + "</a>";
                    m_DynamicTokens.Add("(!USER_NAME!)", tmpS);
                    m_DynamicTokens.Add("(!USERNAME!)", tmpS);
                }
                else
                {
                    m_DynamicTokens.Add("(!USER_NAME!)", string.Empty);
                    m_DynamicTokens.Add("(!USERNAME!)", string.Empty);
                
                }

                m_DynamicTokens.Add("(!USER_WELCOMETEXT!)", CommonLogic.IIF(!IsRegistered, string.Empty, AppLogic.GetString("mobile.skinbase.cs.1")));
                m_DynamicTokens.Add("(!USERFULLNAME!)", CommonLogic.IIF(!IsRegistered, string.Empty, ThisCustomer.FullName));

                m_DynamicTokens.Add("(!USER_MENU_NAME!)", CommonLogic.IIF(!IsRegistered, "my account", ThisCustomer.FullName));
                m_DynamicTokens.Add("(!USER_MENU!)", AppLogic.GetUserMenu(ThisCustomer.IsRegistered, SkinID, ThisCustomer.LocaleSetting));
                m_DynamicTokens.Add("(!NUM_CART_ITEMS!)", AppLogic.RenderCartNumItems(0));
                m_DynamicTokens.Add("(!NUM_CART_ITEMS-1!)", AppLogic.RenderCartNumItems(1));
                m_DynamicTokens.Add("(!NUM_CART_ITEMS-2!)", AppLogic.RenderCartNumItems(2));
                tmpS = AppLogic.GetString("AppConfig.CartPrompt");
                m_DynamicTokens.Add("(!CARTPROMPT!)", tmpS);

                //Add token if requesting from mobile 
                if (CurrentContext.IsRequestingFromMobileMode(Customer.Current))
                {
                    tmpS = AppLogic.GetString("AppConfig.MobileCartPrompt");
                    m_DynamicTokens.Add("(!MOBILECARTPROMT!)", tmpS);
                }

                
                bool showWishList = AppLogic.AppConfigBool("ShowWishListButton");
                if (showWishList)
                {
                    tmpS = Localization.ParseLocaleDecimal(ShoppingCart.NumItems(ThisCustomer.CustomerID, CartTypeEnum.WishCart, ThisCustomer.ContactCode), ThisCustomer.LocaleSetting);
                    m_DynamicTokens.Add("(!NUM_WISH_ITEMS!)", tmpS);
                }

                tmpS = CommonLogic.IIF(!IsRegistered, AppLogic.GetString("skinbase.cs.4"), AppLogic.GetString("skinbase.cs.5"));
                m_DynamicTokens.Add("(!SIGNINOUT_TEXT!)", tmpS);
                m_DynamicTokens.Add("(!SIGNINOUT_LINK!)", CommonLogic.IIF(!IsRegistered, "signin.aspx", "signout.aspx"));

                string PN = CommonLogic.GetThisPageName(false);
                if (AppLogic.AppConfigBool("MiniCart.Enabled"))
                {
                    if (PN.StartsWith("shoppingcart", StringComparison.InvariantCultureIgnoreCase) || PN.StartsWith("checkout", StringComparison.InvariantCultureIgnoreCase) || PN.StartsWith("cardinal", StringComparison.InvariantCultureIgnoreCase) || PN.StartsWith("addtocart") || PN.IndexOf("_process", StringComparison.InvariantCultureIgnoreCase) != -1)
                    {
                        m_DynamicTokens.Add("(!MINICART!)", string.Empty); // don't show on these pages
                    }
                    else
                    {
                        m_DynamicTokens.Add("(!MINICART!)", ShoppingCart.DisplayMiniCart(m_EntityHelpers, ThisCustomer, SkinID, true));
                    }
                    if (PN.StartsWith("shoppingcart", StringComparison.InvariantCultureIgnoreCase) || PN.StartsWith("checkout", StringComparison.InvariantCultureIgnoreCase) || PN.StartsWith("cardinal", StringComparison.InvariantCultureIgnoreCase) || PN.StartsWith("addtocart", StringComparison.InvariantCultureIgnoreCase) || PN.IndexOf("_process", StringComparison.InvariantCultureIgnoreCase) != -1)
                    {
                        m_DynamicTokens.Add("(!MINICART_PLAIN!)", string.Empty); // don't show on these pages
                    }
                    else
                    {
                        m_DynamicTokens.Add("(!MINICART_PLAIN!)", ShoppingCart.DisplayMiniCart(m_EntityHelpers, ThisCustomer, SkinID, false));
                    }
                }
                else
                {
                    m_DynamicTokens.Add("(!MINICART!)", string.Empty);
                    m_DynamicTokens.Add("(!MINICART_PLAIN!)", string.Empty);
                }

                m_DynamicTokens.Add("(!CUSTOMERID!)", ThisCustomer.CustomerID.ToString());

                m_DynamicTokens.Remove(String.Format("(!{0}!)", DomainConstants.MOBILE_FULLMODE_SWITCHER));
                string link = String.Empty;
                if (CurrentContext.IsRequestingFromMobileBrowser())
                {
                    link = AppLogic.RenderMobileSwitcherLink(!ThisCustomer.FullModeInMobile);
                }
                m_DynamicTokens.Add(String.Format("(!{0}!)", DomainConstants.MOBILE_FULLMODE_SWITCHER), link);
                m_DynamicTokens.Add("(!{0}!)".FormatWith(DomainConstants.LIVECHAT), AppLogic.RenderLiveChat());

                //Customization
                if (ThisCustomer.Type == "Company")
                {
                    m_DynamicTokens.Add("(!PORTAL_COMPANY!)", AppLogic.GetPortalCompanyName(ThisCustomer.ContactCode, ThisCustomer.CompanyName));
                    m_DynamicTokens.Add("(!PORTAL_SHIPTONAME!)", AppLogic.GetPortalShipToName(CommonLogic.IIF(ThisCustomer.ThisCustomerSession["ContactCode"].Length > 0, ThisCustomer.ThisCustomerSession["ContactCode"], ThisCustomer.ContactCode))); 
                }
                m_DynamicTokens.Add("(!COMPANY_LOGO!)", AppLogic.GetCompanyLogo(ThisCustomer.CustomerCode));
                //AppLogic.CurrentEntity currentEntity = AppLogic.GetCurrentEntity(m_EntityHelpers);
                //m_DynamicTokens.Add("(!BROWSE_ATTRIBUTES_TEXT!)", CommonLogic.IIF(currentEntity.IsNull || (currentEntity.EntityType.ToUpperInvariant() != "CATEGORY" && currentEntity.EntityType.ToUpperInvariant() != "DEPARTMENT" && currentEntity.EntityType.ToUpperInvariant() != "PRODUCT"), string.Empty, "Browse Attributes"));                
            }
        }

        public string ReplaceRegExTokens(string s)
        {
            // PROCESS REGEX TYPE TOKENS HERE:
            // e.g. AppConfig, Param, Topic, XmlPackage, Localize, etc...
            return Regex.Replace(s, RegExString, m_CmdMatchEval, RegexOptions.Compiled);
        }

        public string ReplaceTokens(string s)
        {
            if (s.IndexOf("(!") == -1)
            {
                return s; // no tokens!
            }

            s = ReplacePageStaticTokens(s);
            s = ReplacePageDynamicTokens(s);
            s = ReplaceRegExTokens(s);

            return s;
        }

        /// <summary>
        /// Evaluates (!!) tokens and replaces them with correct command output
        /// </summary>
        protected string CommandMatchEvaluator(Match match)
        {
            string cmd = match.Groups[1].Value; // The command string

            var parameters = new Hashtable();

            for (int i = 0; i < match.Groups[2].Captures.Count; i++)
            {
                string attr = match.Groups[2].Captures[i].Value;
                string val = match.Groups[3].Captures[i].Value;

                if (attr == null)
                {
                    attr = string.Empty;
                }

                if (val == null)
                {
                    val = string.Empty;
                }

                parameters.Add(attr.ToLowerInvariant(), val);
            }
            return DispatchCommand(cmd, parameters);
        }

        /// <summary>
        /// Takes command string and parameters and returns the result string of the command.
        /// </summary>
        protected string DispatchCommand(string command, Hashtable parameters)
        {
            string result = "(!" + command + "!)";
            command = command.ToLowerInvariant().Replace("username", "user_name");
            var ExtObj = new XSLTExtensions(m_ThisCustomer, m_SkinID);

            switch (command)
            {
                case "remoteurl":
                    {
                        string URL = CommonLogic.HashtableParam(parameters, "url");
                        if (URL.Length != 0)
                        {
                            result = ExtObj.RemoteUrl(URL);
                        }
                        break;
                    }
                case "pagingcontrol":
                    {
                        string BaseURL = CommonLogic.HashtableParam(parameters, "baseurl"); // optional, will use existing QUERY_STRING if not provided
                        int PageNum = CommonLogic.HashtableParamUSInt(parameters, "pagenum"); // optional, can get from QUERY_STRING if not provided
                        int NumPages = CommonLogic.HashtableParamUSInt(parameters, "numpages"); // required
                        result = ExtObj.PagingControl(BaseURL, PageNum.ToString(), NumPages.ToString());
                        break;
                    }
                case "skinid":
                    {
                        result = SkinID.ToString();
                        break;
                    }
                case "customerid":
                    {
                        if (ThisCustomer != null)
                        {
                            result = ThisCustomer.CustomerID.ToString();
                        }
                        else
                        {
                            result = string.Empty;
                        }
                        break;
                    }
                case "user_name":
                    {
                        result = ExtObj.User_Name();
                        break;
                    }
                case "user_menu_name":
                    {
                        result = ExtObj.User_Menu_Name();
                        break;
                    }
                case "store_version":
                    {
                        result = AppLogic.AppConfig("StoreVersion");
                        break;
                    }
                case "manufacturerlink":
                    {
                        int ManufacturerID = CommonLogic.HashtableParamUSInt(parameters, "manufacturerid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        result = ExtObj.ManufacturerLink(ManufacturerID.ToString(), SEName, IncludeATag.ToString());
                        break;
                    }
                case "categorylink":
                    {
                        int CategoryID = CommonLogic.HashtableParamUSInt(parameters, "categoryid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        result = ExtObj.CategoryLink(CategoryID.ToString(), SEName, IncludeATag.ToString());
                        break;
                    }
                case "sectionlink":
                    {
                        int SectionID = CommonLogic.HashtableParamUSInt(parameters, "sectionid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        result = ExtObj.SectionLink(SectionID.ToString(), SEName, IncludeATag.ToString());
                        break;
                    }
                case "sectionmenucontrol":
                    {
                        // (!SectionMenu PackageName="xxx"!)
                        string packageName = CommonLogic.HashtableParam(parameters, "packagename");
                        result = ExtObj.SectionMenuControl(packageName);
                        break;
                    }
                case "sectionbreadcrumbcontrol":
                    {
                        // (!SectionBreadcrumb PackageName="xxx"!)
                        string packageName = CommonLogic.HashtableParam(parameters, "packagename");
                        result = ExtObj.SectionBreadcrumbControl(packageName);
                        break;
                    }
                case "librarylink":
                    {
                        int LibraryID = CommonLogic.HashtableParamUSInt(parameters, "libraryid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        result = ExtObj.LibraryLink(LibraryID.ToString(), SEName, IncludeATag.ToString());
                        break;
                    }
                case "productlink":
                    {
                        int ProductID = CommonLogic.HashtableParamUSInt(parameters, "productid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        result = ExtObj.ProductLink(ProductID.ToString(), SEName, IncludeATag.ToString());
                        break;
                    }
                case "relatedproducts":
                    {
                        string ProductID = CommonLogic.HashtableParam(parameters, "productid");
                        result = ExtObj.GetSubstituteProducts(ProductID);
                        break;
                    }
                case "documentlink":
                    {
                        // (!DocumentLink DocumentID="N" SEName="xxx" IncludeATag="true/false"!)
                        int DocumentID = CommonLogic.HashtableParamUSInt(parameters, "documentid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        result = ExtObj.DocumentLink(DocumentID.ToString(), SEName, IncludeATag.ToString());
                        break;
                    }
                case "productandcategorylink":
                    {
                        // (!ProductAndCategoryLink ProductID="N" CategoryID="M" SEName="xxx" IncludeATag="true/false"!)
                        int ProductID = CommonLogic.HashtableParamUSInt(parameters, "productid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        int CategoryID = CommonLogic.HashtableParamUSInt(parameters, "categoryid");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        result = ExtObj.ProductandCategoryLink(ProductID.ToString(), SEName, CategoryID.ToString(), IncludeATag.ToString());
                        break;
                    }
                case "productandsectionlink":
                    {
                        // (!ProductAndSectionLink ProductID="N" SectionID="M" SEName="xxx" IncludeATag="true/false"!)
                        int ProductID = CommonLogic.HashtableParamUSInt(parameters, "productid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        int SectionID = CommonLogic.HashtableParamUSInt(parameters, "sectionid");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        result = ExtObj.ProductandSectionLink(ProductID.ToString(), SEName, SectionID.ToString(), IncludeATag.ToString());
                        break;
                    }
                case "productandmanufacturerlink":
                    {
                        // (!ProductAndManufacturerLink ProductID="N" ManufacturerID="M" SEName="xxx" IncludeATag="true/false"!)
                        int ProductID = CommonLogic.HashtableParamUSInt(parameters, "productid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        int ManufacturerID = CommonLogic.HashtableParamUSInt(parameters, "manufacturerid");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        result = ExtObj.ProductandManufacturerLink(ProductID.ToString(), SEName, ManufacturerID.ToString(), IncludeATag.ToString());
                        break;
                    }
                case "productpropername":
                    {
                        // (!ProductProperName ProductID="N" VariantID="M"!)
                        int ProductID = CommonLogic.HashtableParamUSInt(parameters, "productid");
                        int VariantID = CommonLogic.HashtableParamUSInt(parameters, "variantid");
                        result = ExtObj.ProductProperName(ProductID.ToString(), VariantID.ToString());
                        break;
                    }
                case "documentandlibrarylink":
                    {
                        // (!DocumentAndLibraryLink DocumentID="N" LibraryID="M" SEName="xxx" IncludeATag="true/false"!)
                        int DocumentID = CommonLogic.HashtableParamUSInt(parameters, "documentid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        int LibraryID = CommonLogic.HashtableParamUSInt(parameters, "libraryid");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        result = ExtObj.DocumentandLibraryLink(DocumentID.ToString(), SEName, LibraryID.ToString(), IncludeATag.ToString());
                        break;
                    }
                case "entitylink":
                    {
                        // (!EntityLink EntityID="N" EntityName="xxx" SEName="xxx" IncludeATag="true/false"!)
                        int EntityID = CommonLogic.HashtableParamUSInt(parameters, "entityid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        string EntityName = CommonLogic.HashtableParam(parameters, "entityname");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        result = ExtObj.EntityLink(EntityID.ToString(), SEName, EntityName, IncludeATag.ToString());
                        break;
                    }
                case "objectlink":
                    {
                        // (!ObjectLink ObjectID="N" ObjectName="xxx" SEName="xxx" IncludeATag="true/false"!)
                        int ObjectID = CommonLogic.HashtableParamUSInt(parameters, "objectid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        string ObjectName = CommonLogic.HashtableParam(parameters, "objectname");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        result = ExtObj.ObjectLink(ObjectID.ToString(), SEName, ObjectName, IncludeATag.ToString());
                        break;
                    }
                case "productandentitylink":
                    {
                        // (!ProductAndEntityLink ProductID="N" EntityID="M" EntityName="xxx" SEName="xxx" IncludeATag="true/false"!)
                        int ProductID = CommonLogic.HashtableParamUSInt(parameters, "productid");
                        string SEName = CommonLogic.HashtableParam(parameters, "sename");
                        int EntityID = CommonLogic.HashtableParamUSInt(parameters, "entityid");
                        string EntityName = CommonLogic.HashtableParam(parameters, "entityname");
                        bool IncludeATag = CommonLogic.HashtableParamBool(parameters, "includeatag");
                        string InnerText = CommonLogic.HashtableParam(parameters, "innertext");
                        result = ExtObj.ProductandEntityLink(ProductID.ToString(), SEName, EntityID.ToString(), EntityName, IncludeATag.ToString());
                        break;
                    }
                case "topic":
                    {
                        // (!Topic TopicID="M"!) or (!Topic ID="M"!) or (!Topic Name="xxx"!)
                        int TopicID = CommonLogic.HashtableParamUSInt(parameters, "id");
                        if (TopicID == 0)
                        {
                            TopicID = CommonLogic.HashtableParamUSInt(parameters, "topicid");
                        }
                        string LS = ThisCustomer.LocaleSetting;
                        if (ThisCustomer != null)
                        {
                            LS = ThisCustomer.LocaleSetting;
                        }
                        if (TopicID != 0)
                        {
                            Topic t = new Topic(TopicID.ToString(), LS);
                            result = t.Contents;
                        }
                        else
                        {
                            string TopicName = CommonLogic.HashtableParam(parameters, "name");
                            if (TopicName.Length != 0)
                            {
                                var t = new Topic(TopicName, LS, SkinID, null);
                                result = t.Contents;
                            }
                        }
                        break;
                    }
                case "appconfig":
                    {
                        // (!AppConfig Name="xxx"!)
                        string AppConfigName = CommonLogic.HashtableParam(parameters, "name");
                        result = ExtObj.AppConfig(AppConfigName);
                        break;
                    }
                case "stringresource":
                    {
                        // (!StringResource Name="xxx"!)
                        string StringResourceName = CommonLogic.HashtableParam(parameters, "name");
                        result = ExtObj.StringResource(StringResourceName);
                        break;
                    }
                case "getstring":
                    {
                        // (!GetString Name="xxx"!)
                        string StringResourceName = CommonLogic.HashtableParam(parameters, "name");
                        result = ExtObj.StringResource(StringResourceName);
                        break;
                    }

                case "searchbox":
                    {
                        // (!SearchBox!)
                        result = ExtObj.SearchBox();
                        break;
                    }

                case "helpbox":
                    {
                        // (!HelpBox!)
                        result = ExtObj.HelpBox();
                        break;
                    }
                case "addtocartform":
                    {
                        // (!AddToCartForm ProductID="M" VariantID="N" ColorChangeProductImage="true/false"!)
                        string ProductID = CommonLogic.HashtableParam(parameters, "productid");
                        string VariantID = CommonLogic.HashtableParam(parameters, "variantid");
                        bool ColorChangeProductImage = CommonLogic.HashtableParamBool(parameters, "colorchangeproductimage");
                        result = ExtObj.ShowAddToCartForm(0, ProductID, VariantID, ColorChangeProductImage, decimal.Zero.ToString());
                        break;
                    }

                //Obsolete Code

                //case "emailproducttofriend":
                //    {
                //        // (!EmailProductToFriend ProductID="M" CategoryID="N"!)
                //        int ProductID = CommonLogic.HashtableParamUSInt(parameters, "productid");
                //        int CategoryID = CommonLogic.HashtableParamUSInt(parameters, "categoryid");
                //        result = ExtObj.EmailProductToFriend(ProductID.ToString(), CategoryID.ToString());
                //        break;
                //    }
                case "productdescriptionfile":
                    {
                        // (!ProductDescriptionFile ProductID="M" IncludeBRBefore="true/false"!)
                        int ProductID = CommonLogic.HashtableParamUSInt(parameters, "productid");
                        bool IncludeBRBefore = CommonLogic.HashtableParamBool(parameters, "includebrbefore");
                        result = ExtObj.ProductDescriptionFile(ProductID.ToString(), IncludeBRBefore.ToString());
                        break;
                    }
                case "productratings":
                    {
                        // (!ProductRatings ProductID="M" CategoryID="N" SectionID="P" ManufacturerID="Q" IncludeBRBefore="true/false"!)
                        int ProductID = CommonLogic.HashtableParamUSInt(parameters, "productid");
                        int CategoryID = CommonLogic.QueryStringUSInt("CategoryID"); // should really get them from parameters, NOT from the querystring, but whatever...
                        int DepartmentID = CommonLogic.QueryStringUSInt("DepartmentID"); // should really get them from parameters, NOT from the querystring, but whatever...
                        int ManufacturerID = CommonLogic.QueryStringUSInt("ManufacturerID"); // should really get them from parameters, NOT from the querystring, but whatever...
                        bool IncludeBRBefore = CommonLogic.HashtableParamBool(parameters, "includebrbefore");
                        result = ExtObj.ProductRatings(ProductID.ToString());
                        break;
                    }
                case "formatcurrency":
                    {
                        // (!FormatCurrency Value="xx.xx" LocaleSetting="en-US"!)
                        decimal CurrencyValue = CommonLogic.HashtableParamNativeDecimal(parameters, "value");
                        string LocaleSetting = CommonLogic.HashtableParam(parameters, "localesetting");
                        result = ExtObj.FormatCurrency(CurrencyValue.ToString());
                        break;
                    }
                case "getspecialsboxexpandedrandom":
                    {
                        // (!GetSpecialsBoxExpandedRandom CategoryID="M" ShowPics="true/false" IncludeFrame="true/false" Teaser="xxx"!)
                        int CategoryID = CommonLogic.HashtableParamUSInt(parameters, "categoryid");
                        bool ShowPics = CommonLogic.HashtableParamBool(parameters, "showpics");
                        bool IncludeFrame = CommonLogic.HashtableParamBool(parameters, "includeframe");
                        string Teaser = CommonLogic.HashtableParam(parameters, "teaser");
                        result = ExtObj.GetSpecialsBoxExpandedRandom(CategoryID.ToString(), ShowPics.ToString(), IncludeFrame.ToString(), Teaser);
                        break;
                    }
                case "getspecialsboxexpanded":
                    {
                        // (!GetSpecialsBoxExpanded CategoryID="M" ShowNum="N" ShowPics="true/false" IncludeFrame="true/false" Teaser="xxx"!)
                        int CategoryID = CommonLogic.HashtableParamUSInt(parameters, "categoryid");
                        int ShowNum = CommonLogic.HashtableParamUSInt(parameters, "shownum");
                        bool ShowPics = CommonLogic.HashtableParamBool(parameters, "showpics");
                        bool IncludeFrame = CommonLogic.HashtableParamBool(parameters, "includeframe");
                        string Teaser = CommonLogic.HashtableParam(parameters, "teaser");
                        result = ExtObj.GetSpecialsBoxExpanded(CategoryID.ToString(), ShowNum.ToString(), ShowPics.ToString(), IncludeFrame.ToString(), Teaser);
                        break;
                    }
                case "getnewsboxexpanded":
                    {
                        // (!GetNewsBoxExpanded ShowCopy="true/false" ShowNum="N" IncludeFrame="true/false"!)
                        bool ShowCopy = CommonLogic.HashtableParamBool(parameters, "showcopy");
                        int ShowNum = CommonLogic.HashtableParamUSInt(parameters, "shownum");
                        bool IncludeFrame = CommonLogic.HashtableParamBool(parameters, "includeframe");
                        string Teaser = CommonLogic.HashtableParam(parameters, "teaser");
                        result = ExtObj.GetNewsBoxExpanded(ShowCopy.ToString(), ShowNum.ToString(), IncludeFrame.ToString(), Teaser);
                        break;
                    }
                case "xmlpackage":
                    {
                        // (!XmlPackage Name="xxx" version="N"!)
                        // version can only be 2 at this time, or blank
                        string PackageName = CommonLogic.HashtableParam(parameters, "name");
                        string VersionID = CommonLogic.HashtableParam(parameters, "version"); // optional
                        var userruntimeparams = parameters;
                        userruntimeparams.Remove("name");
                        userruntimeparams.Remove("version");
                        string runtimeparams = string.Empty;
                        if (CommonLogic.HashtableParamBool(parameters, "isforattributes") == true)
                        {
                            AppLogic.CurrentEntity currentEntity = AppLogic.GetCurrentEntity(AppLogic.MakeEntityHelpers());
                            if (!currentEntity.IsNull && (currentEntity.EntityType.ToUpperInvariant() == "CATEGORY" || currentEntity.EntityType.ToUpperInvariant() == "DEPARTMENT"))
                            {
                                    runtimeparams += "EntityName" + "=" + currentEntity.EntityType + "&";
                                    runtimeparams += "EntityID" + "=" + currentEntity.EntityID.ToUrlEncode() + "&";
                            }
                            else if (!currentEntity.IsNull && (currentEntity.EntityType.ToUpperInvariant() == "ATTRIBUTE" ))
                            {
                                runtimeparams += "EntityName" + "=" + CommonLogic.IIF(CommonLogic.QueryStringCanBeDangerousContent("EntityName").Length>0,
                                 CommonLogic.QueryStringCanBeDangerousContent("EntityName"),currentEntity.EntityType) + "&";

                                runtimeparams += "EntityID" + "=" + CommonLogic.IIF(CommonLogic.QueryStringCanBeDangerousContent("EntityID").Length > 0,
                                 CommonLogic.QueryStringCanBeDangerousContent("EntityID"), currentEntity.EntityID.ToUrlEncode()) + "&";
                                
                                runtimeparams += "WebSiteCode" + "=" + InterpriseHelper.ConfigInstance.WebSiteCode + "&";

                                string attributteFilter = CommonLogic.GetAttributeFilter();
                                if (attributteFilter.Length > 0)
                                {
                                    runtimeparams += "AttributeFilter" + "=" + attributteFilter + "&";
                                }
                            }
                        }
                        userruntimeparams.Remove("isforattributes");
                        foreach (DictionaryEntry de in userruntimeparams)
                        {
                            runtimeparams += de.Key.ToString() + "=" + de.Value.ToString() + "&";
                        }
                        if (runtimeparams.Length > 0)
                        {
                            runtimeparams = runtimeparams.Substring(0, runtimeparams.Length - 1);
                        }
                        if (PackageName.Length != 0)
                        {
                            if (PackageName.EndsWith(".xslt", StringComparison.InvariantCultureIgnoreCase) && VersionID != "2")
                            {
                                throw new ArgumentException("Version 1 XmlPackages are no longer supported!");
                            }
                            else
                            {
                                using (var p = new XmlPackage2(PackageName, ThisCustomer, m_SkinID, string.Empty, AppLogic.MakeXmlPackageParamsFromString(runtimeparams), string.Empty, true))
                                {
                                    // WARNING YOU COULD CAUSE ENDLESS RECURSION HERE! if your XmlPackage refers to itself in some direct, or INDIRECT! way!!
                                    result = AppLogic.RunXmlPackage(p, this, ThisCustomer, SkinID, true, true);
                                }
                            }
                        }
                        break;
                    }
            }
            return result;
        }

        public int SkinID
        {
            get
            {
                return m_SkinID;
            }
            set
            {
                m_SkinID = value;
            }
        }

        public Customer ThisCustomer
        {
            get
            {
                return m_ThisCustomer;
            }
            set
            {
                m_ThisCustomer = value;
            }
        }

    }
}
