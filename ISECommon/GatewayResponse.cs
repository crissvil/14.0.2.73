// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace InterpriseSuiteEcommerceCommon
{
    [Serializable, DataContract]
    public class GatewayResponse
    {
        private string _gateway = string.Empty;
        private string _status = AppLogic.ro_OK;
        private string _details = string.Empty;
        private string _avsResult = string.Empty;
        private string _cv2Result = string.Empty;
        private string _authorizationResult = string.Empty;
        private string _authorizationCode = string.Empty;
        private string _authorizationTransID = string.Empty;
        private string _transactionCommandOut = string.Empty;
        private string _transactionResponse = string.Empty;
        private string _expirationMonth = String.Empty;
        private string _expirationYear = String.Empty;

        public GatewayResponse(string gateway)
        {
            _gateway = gateway;
        }

        public GatewayResponse()
        {
        }

        public string Gateway
        {
            get { return _gateway; }
        }

        [DataMember]
        public string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        [DataMember]
        public string Details
        {
            get { return _details; }
            set { _details = value; }
        }

        [DataMember]
        public string AVSResult
        {
            get { return _avsResult; }
            set { _avsResult = value; }
        }

        [DataMember]
        public string CV2Result
        {
            get { return _cv2Result; }
            set { _cv2Result = value; }
        }

        [DataMember]
        public string AuthorizationResult
        {
            get { return _authorizationResult; }
            set { _authorizationResult = value; }
        }

        [DataMember]
        public string AuthorizationCode
        {
            get { return _authorizationCode; }
            set { _authorizationCode = value; }
        }

        [DataMember]
        public string AuthorizationTransID
        {
            get { return _authorizationTransID; }
            set { _authorizationTransID = value; }
        }

        [DataMember]
        public string TransactionCommandOut
        {
            get { return _transactionCommandOut; }
            set { _transactionCommandOut = value; }
        }

        [DataMember]
        public string TransactionResponse
        {
            get { return _transactionResponse; }
            set { _transactionResponse = value; }
        }

        [DataMember]
        public string ExpirationMonth
        {
            get { return _expirationMonth; }
            set { _expirationMonth = value; }
        }

        [DataMember]
        public string ExpirationYear
        {
            get { return _expirationYear; }
            set { _expirationYear = value; }
        }

    }
}
