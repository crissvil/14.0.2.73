// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Actions
{
    public class InventoryPricingLevelAction : IActionHandler
    {
        #region IActionHandler Members

        public string Name
        {
            get { return "pricingLevel"; }
        }

        public void Handle(System.Web.HttpContext context)
        {
            string itemCode = CommonLogic.QueryStringCanBeDangerousContent("itemcode", true);
            if(!CommonLogic.IsStringNullOrEmpty(itemCode))
            {
                bool hasPricingLevel; 

                string response = InterpriseHelper.GetInventoryPricingLevelTable(Customer.Current, itemCode, out hasPricingLevel);

                if (hasPricingLevel)
                {
                    context.Response.StatusCode = 200;
                    context.Response.Clear();
                    context.Response.Write(response);
                }
                else
                {
                    context.Response.StatusCode = 404;
                }
                
                context.Response.Flush();
                context.Response.Close();
            }
        }

        #endregion
    }
}
