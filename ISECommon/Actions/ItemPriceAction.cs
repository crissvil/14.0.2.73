// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using InterpriseSuiteEcommerceCommon;
using Interprise.Facade.Customer;

namespace InterpriseSuiteEcommerceCommon.Actions
{
    public class ItemPriceAction : IActionHandler
    {
        #region IActionHandler Members

        public string Name
        {
            get { return "itemPrice"; }
        }

        public void Handle(HttpContext context)
        {
            string itemCode = CommonLogic.FormCanBeDangerousContent("ItemCode", true);
            string itemType = CommonLogic.FormCanBeDangerousContent("ItemType", true);
            string unitMeasureCode = CommonLogic.FormCanBeDangerousContent("UMCode", true);

            if (itemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
            {
                Customer thisCustomer = Customer.Current;
                KitComposition selectedComposition = KitComposition.FromForm(thisCustomer, CartTypeEnum.ShoppingCart, itemCode);
                selectedComposition.UnitMeasureCode = unitMeasureCode;
                UnitMeasureInfo nfo = UnitMeasureInfo.ForItem(itemCode, unitMeasureCode);
                selectedComposition.Quantity = decimal.One * nfo.Quantity;

                if (selectedComposition != null)
                {
                    decimal vat = decimal.Zero;
                    decimal price = selectedComposition.GetSalesPrice(ref vat, "");

                    context.Response.Clear();
                    context.Response.Write(thisCustomer.FormatBasedOnMyCurrency(price));
                    context.Response.Flush();
                    context.Response.Close();
                }
            }
        }

        #endregion
    }
}
