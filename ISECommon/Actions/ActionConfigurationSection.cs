// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace InterpriseSuiteEcommerceCommon.Actions
{
    public class ActionConfigurationSection : ConfigurationSection
    {
        #region Methods

        public static ActionConfigurationSection Current
        {
            get
            {
                try
                {
                    return System.Configuration.ConfigurationManager.GetSection("ise.action") as ActionConfigurationSection;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        [ConfigurationProperty("handlers",
            IsDefaultCollection = false)]
        public ActionHandlerCollection Handlers
        {

            get
            {
                ActionHandlerCollection handlers =
                (ActionHandlerCollection)base["handlers"];
                return handlers;
            }
        }


        #endregion

    }

    public class ActionHandlerCollection : ConfigurationElementCollection
    {
        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        new public ActionHandlerConfiguration this[string name]
        {
            get
            {
                return (ActionHandlerConfiguration)BaseGet(name);
            }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new ActionHandlerConfiguration();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return (element as ActionHandlerConfiguration).Name;
        }
    }

    public class ActionHandlerConfiguration : ConfigurationElement
    {
        #region Properties

        [ConfigurationProperty("name",
            IsRequired = true,
            IsKey = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        [ConfigurationProperty("type",
            IsRequired = true,
            IsKey = false)]
        public string Type
        {
            get { return (string)this["type"]; }
            set { this["type"] = value; }
        }

        #endregion

    }
}
