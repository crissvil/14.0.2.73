// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;

namespace InterpriseSuiteEcommerceCommon.Actions
{
    public interface IActionHandler
    {
        string Name { get;}
        void Handle(HttpContext context);
    }
}
