// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.JSONLib;

namespace InterpriseSuiteEcommerceCommon.Actions
{
    public class ShippingMethodAction : IActionHandler
    {
        #region IActionHandler Members

        public string Name
        {
            get { return "shippingMethod"; }
        }

        public void Handle(System.Web.HttpContext context)
        {
            Customer thisCustomer = context.User.Identity as Customer;
            if (null != thisCustomer)
            {
                bool overrideDefaultAddress = CommonLogic.FormBool("OVA"); 
                string addressId = CommonLogic.FormCanBeDangerousContent("AID"); 

                Address preferredAddress = null;

                InterpriseShoppingCart cart = new InterpriseShoppingCart(null, thisCustomer.SkinID, thisCustomer, CartTypeEnum.ShoppingCart, string.Empty, false, true);
                cart.BuildSalesOrderDetails();

                if (overrideDefaultAddress)
                {
                    preferredAddress = Address.FromForm(thisCustomer, AddressTypes.Shipping,context.Request.Form); //GetPrefferredAddress(context);
                }
                else if (!CommonLogic.IsStringNullOrEmpty(addressId))
                {
                    preferredAddress = Address.Get(thisCustomer, AddressTypes.Shipping, addressId);

                    if (cart.HasMultipleShippingAddresses())
                    {
                        InterpriseShoppingCart originalCart = cart;
                        cart = cart.ForAddress(preferredAddress);
                        originalCart.Dispose(); // dispose the original cart object
                    }
                }
                else
                {
                    preferredAddress = thisCustomer.PrimaryShippingAddress;
                }

                ShippingMethodDTOCollection availableShippingMethods = cart.GetShippingMethods(preferredAddress);

                GenerateResponse(context, availableShippingMethods);
                cart.Dispose();
            }
        }

        private void GenerateResponse(HttpContext context, ShippingMethodDTOCollection availableShippingMethods)
        {
            context.Response.Clear();

            JSONSerializer ser = new JSONSerializer(SerializeOption.All);

            string serialization = ser.SerializeArray(availableShippingMethods);
            context.Response.Write(serialization);

            context.Response.Flush();
        }
        #endregion
    }
}
