// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using InterpriseSuiteEcommerceCommon;
using InterpriseSuiteEcommerceCommon.InterpriseIntegration.JSONLib;

namespace InterpriseSuiteEcommerceCommon.Actions
{
    public  class AddNewShippingAddressAction : IActionHandler
    {
        #region IActionHandler Members

        public string Name
        {
            get { return "addNewShippingAddress"; }
        }

        public void Handle(System.Web.HttpContext context)
        {
            Address newAddress = Address.FromForm(Customer.Current, AddressTypes.Shipping, context.Request.Form);
            if (null != newAddress)
            {
                string shipToCode = InterpriseHelper.AddCustomerShipTo(newAddress);
                if (!CommonLogic.IsStringNullOrEmpty(shipToCode))
                {
                    context.Response.Clear();

                    JSONSerializer ser = new JSONSerializer(SerializeOption.Properties | SerializeOption.WithSerializedAttributeOnly);
                    string serialization = ser.Serialize(newAddress);
                    context.Response.Write(serialization);

                    context.Response.Flush();
                }
                else
                {
                    context.Response.Clear();
                    context.Response.StatusCode = 404;
                    context.Response.Flush();
                }
            }
        }
        #endregion
    }
}
