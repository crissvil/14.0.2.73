﻿using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;
using System;

namespace InterpriseSuiteEcommerceCommon.Tool
{
    public class JSONHelper
    {
        [Obsolete("Use this method: ServiceFactory.GetInstance<ICryptographyService>().SerializeToJson()")]
        public static string Serialize<T>(T obj)
        {
            var serializer = new DataContractJsonSerializer(obj.GetType());
            var ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.Default.GetString(ms.ToArray());
            ms.Dispose();
            return retVal;
        }

        [Obsolete("Use this method: ServiceFactory.GetInstance<ICryptographyService>().DeserializeJson()")]
        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            var ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            var serializer = new System.Runtime.Serialization.Json.DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            ms.Dispose();
            return obj;
        }
    }
}
