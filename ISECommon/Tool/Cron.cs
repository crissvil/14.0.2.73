﻿using System.Threading;
using InterpriseSuiteEcommerceCommon.DataAccess;
using System.Collections.Generic;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.Tool
{
    public class Cron
    {
        public static Thread CronRegistryExpirationRemoval = new Thread(Job_Registry_Expiration_Removal);
        public static Thread CronExpiredEcommerceCustomerSessionRemoval = new Thread(Job_Expired_Ecommerce_Customer_Session_Removal);

        public static void Job_Registry_Expiration_Removal()
        {
            int onehour = (3600 * 1000);
            try
            {
                while (true)
                {
                    int interval = AppLogic.AppConfigNativeInt("GiftRegistry.DeletionJobInterval");

                    //if configuration has been disabled after the thread ran. Sleep Immediately or reset the apppool to disable totally the thread.
                    if (!AppLogic.AppConfigBool("GiftRegistry.Enabled")) Thread.Sleep(onehour * interval); //sleep (h) interval

                    var lstIds = GiftRegistryDA.DeleteExpiredRegistryItemsReturnsPictureFileName(AppLogic.AppConfigNativeInt("GiftRegistry.ExtraDayBeforeExpiredRegistryDeletion"), InterpriseHelper.ConfigInstance.WebSiteCode);
                    GiftRegistry.DeleteRegistryImagesByIDs(lstIds, CurrentContext.MapPath(string.Empty));

                    Thread.Sleep(onehour * interval); //sleep (h) interval
                }
            }
            catch (ThreadAbortException) { }
        }

        public static void Job_Expired_Ecommerce_Customer_Session_Removal()
        {
            int onehour = (3600 * 1000);
            try
            {
                while (true)
                {
                    int interval = 20;

                    ServiceFactory.GetInstance<IAppConfigService>().CleanEcommerceCustomerRecords();

                    Thread.Sleep(onehour * interval); //sleep (h) interval
                }
            }
            catch (ThreadAbortException) { }
        }
    }
}