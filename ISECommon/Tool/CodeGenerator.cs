﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Tool
{
    public static class CodeGenerator
    {
        // Similar sets have been omitted, like 1,I; Q,O,0;
        private static readonly char[] alphabet = "ABCD6ABCD67890EFGHJKLMNPABCD67890EFGHJKLMNP67896789789ABCD67890EFGHJKLMNP67890EFGHJKLMNP67890BCDEFGHJKLMNPRSTRSTUVWXYZ1234567890".ToCharArray();
        private static readonly Random rand = new Random();

        public static string GetNext(int codeLength)
        {
            char[] randChars = randomAlphabetChars(codeLength);
            char[] formattedChars = new char[codeLength];
            for (int i = 0; i < randChars.Length; i++)
            {
                formattedChars[i] = randChars[i];
            }
            return new string(formattedChars);
        }

        private static char[] randomAlphabetChars(int length)
        {
            char[] newChars = new char[length];
            for (int i = 0; i < length; i++)
                newChars[i] = alphabet[(int)Math.Truncate(rand.NextDouble() * 1000) % alphabet.Length];
            return newChars;
        }

    }
}
