﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.Tool
{
    public interface IClonable
    {
        T Clone<T>();
    }
}
