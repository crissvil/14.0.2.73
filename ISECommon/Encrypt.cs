// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

namespace InterpriseSuiteEcommerceCommon
{
    public class Encrypt
    {
        public static String ComputeSaltedHash(int Salt, String ClearPassword)
        {
            return m_ComputeSaltedHash(Salt, ClearPassword);
        }
        private static String m_ComputeSaltedHash(int Salt, String ClearPassword)
        {
            // Create Byte array of password String
            ASCIIEncoding encoder = new ASCIIEncoding();
            byte[] _secretBytes = encoder.GetBytes(ClearPassword);

            // Create a new salt
            byte[] m_SaltBytes = new Byte[4];
            m_SaltBytes[0] = (byte)(Salt >> 24);
            m_SaltBytes[1] = (byte)(Salt >> 16);
            m_SaltBytes[2] = (byte)(Salt >> 8);
            m_SaltBytes[3] = (byte)(Salt);

            // append the two arrays
            byte[] toHash = new byte[_secretBytes.Length + m_SaltBytes.Length];
            Array.Copy(_secretBytes, 0, toHash, 0, _secretBytes.Length);
            Array.Copy(m_SaltBytes, 0, toHash, _secretBytes.Length, m_SaltBytes.Length);

            byte[] computedHash = new SHA384Managed().ComputeHash(toHash);

            return encoder.GetString(computedHash);
        }
    }
}
