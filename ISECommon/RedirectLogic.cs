﻿// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Collections;
using System.Data.SqlClient;
using System.Data;
using InterpriseSuiteEcommerceCommon.Tool;

namespace InterpriseSuiteEcommerceCommon
{
    public class RedirectLogic
    {
        public static SortedDictionary<string, string> redirectURL; // <old path> / <new path>

        public static bool Process()
        {
            if (HttpContext.Current.Request.Url.PathAndQuery.Length > HttpContext.Current.Request.ApplicationPath.Length)
            {
                string path = string.Empty;
                if (HttpContext.Current.Request.ApplicationPath.Length == 1)
                {
                    path = HttpContext.Current.Request.Url.PathAndQuery.Remove(0, 1).ToLowerInvariant();
                }
                else
                {
                    path = (HttpContext.Current.Request.AppRelativeCurrentExecutionFilePath.Remove(0, 2) + HttpContext.Current.Items["RequestedQuerystring"]).ToLowerInvariant();
                }

                if (IsToRedirect(path))
                {
                    //HttpContext.Current.Response.RedirectPermanent(HttpContext.Current.Request.ApplicationPath + "/" + redirectURL[path], true);
                    HttpContext.Current.Response.RedirectPermanent(CurrentContext.FullyQualifiedApplicationPath() + redirectURL[path], true);
                }
            }
            return false;
        }

        [Obsolete("Use the method : ServiceFactory.GetInstance<IAppConfigService>().GetRedirectUrl() if fully migrated to CB13.1.3/CB13.2 and above")]
        public static void InitializeRedirectURL()
        {
            redirectURL = new SortedDictionary<string, string>();

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, string.Format("SELECT OldPath, NewPath FROM EcommerceStoreURLRedirect with (NOLOCK) WHERE WebsiteCode = {0}", DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode))))
                {
                    while (reader.Read())
                    {
                        string oldPath = DB.RSField(reader, "OldPath").ToLowerInvariant();
                        string newPath = DB.RSField(reader, "NewPath").ToLowerInvariant();

                        if (oldPath.StartsWith("/"))
                        {
                            oldPath = oldPath.Remove(0, 1);
                        }

                        if (newPath.StartsWith("/"))
                        {
                            newPath = newPath.Remove(0, 1);
                        }

                        if (!redirectURL.ContainsKey(oldPath))
                        {
                            redirectURL.Add(oldPath, newPath);
                        }
                    }
                }
            }            
        }

        private static bool IsToRedirect(string path)
        {
            if (redirectURL.ContainsKey(path) && redirectURL[path] != string.Empty)
            {
                return true;
            }
            return false;
        }
    }
}