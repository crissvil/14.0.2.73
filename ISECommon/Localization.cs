// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Configuration;
using System.Xml;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon
{
    /// <summary>
    /// Summary description for Localization.
    /// </summary>
    public class Localization
    {
        static private CultureInfo USCulture = new CultureInfo("en-US");

        static private String _webConfigLocale = String.Empty;
        static private String SqlServerLocale = String.Empty;
        static private CultureInfo SqlServerCulture;
        static private Regex regInteger = new Regex(@"^-?\d+$");

        public Localization() { }

        static private XmlDocument LocalesDoc
        {
            get
            {
                XmlDocument d = (XmlDocument)HttpContext.Current.Cache.Get("LocalesDoc");
                if (d == null)
                {
                    d = DB.GetSqlXmlDoc("select LocaleSettingID, Name, Description, DisplayOrder, DefaultCurrencyID from dbo.LocaleSetting Locales with (NOLOCK) order by DisplayOrder,Name for xml auto", null);
                    HttpContext.Current.Cache.Insert("LocalesDoc", d, null, System.DateTime.Now.AddMinutes(AppLogic.AppConfigUSInt("Localization.CurrencyCacheMinutes")), TimeSpan.Zero);
                }
                return d;
            }
        }

        static public void FlushCache()
        {
            try
            {
                HttpContext.Current.Cache.Remove("LocalesDoc");
            }
            catch { }
        }
        
        static public bool isValidLocale(String Locale)
        {
            String tmpS = String.Empty;
            XmlNode n = LocalesDoc.SelectSingleNode("//Locales[@Name='" + Locale + "']");
            return (n != null);

        }

        static public String GetPrimaryCurrency()
        {
            return StoreCurrency();
        }

        static public String GetUSLocale()
        {
            return "en-US";
        }

        public static string WebConfigLocale
        {
            get 
            {
                if (_webConfigLocale.IsNullOrEmptyTrimmed())
                {
                    GlobalizationSection cultureSection = System.Configuration.ConfigurationManager.GetSection("system.web/globalization") as GlobalizationSection;
                    _webConfigLocale = cultureSection.Culture;
                }

                return _webConfigLocale;
            }
        }

        static public System.Data.DataSet GetLocales()
        {
            return DB.GetDS("select * from LocaleSetting with (NOLOCK) order by DisplayOrder,Name", true, System.DateTime.Now.AddHours(1));
        }

        static public String ValidateLocaleSetting(String Locale)
        {
            String tmp = Localization.WebConfigLocale;
            if (Localization.isValidLocale(Locale))
            {
                tmp = Locale;
            }
            return CheckLocaleSettingForProperCase(tmp);
        }

        static public String CheckLocaleSettingForProperCase(String LocaleSetting)
        {
            // make sure locale is xx-XX:
            int i = LocaleSetting.IndexOf("-");
            if (i != -1)
            {
                LocaleSetting = LocaleSetting.Substring(0, i) + "-" + LocaleSetting.Substring(i + 1, LocaleSetting.Length - (i + 1)).ToUpperInvariant();
            }
            return LocaleSetting;
        }

        static public String CheckCurrencySettingForProperCase(String CurrencySetting)
        {
            return CurrencySetting.ToUpperInvariant();
        }

        static public String GetSqlServerLocale()
        {
            if (SqlServerLocale.Length == 0)
            {
                SqlServerLocale = CommonLogic.Application("DBSQLServerLocaleSetting");
                SqlServerCulture = new CultureInfo(SqlServerLocale);
            }
            return SqlServerLocale;
        }

        public static CultureInfo SqlServerLocaleCulture
        {
            get { return SqlServerCulture; }
        }

        static public String WeightUnits()
        {
            return AppLogic.AppConfig("Localization.WeightUnits");
        }

        static public String ShortDateFormat()
        {
            String tmp = CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern.ToUpperInvariant();
            return tmp;
        }

        static public string JSCalendarDateFormatSpec()
        {
            // see jscalendar/calendar-setup.js for more info. Typical format would be: " + Localization.JSCalendarDateFormatSpec() + "
            String tmp = ShortDateFormat();
            tmp = tmp.Replace("MM", "%m").Replace("M", "%m").Replace("DD", "%d").Replace("D", "%d").Replace("YYYY", "%Y").Replace("YY", "%Y");
            return tmp;
        }

        static public String JSCalendarLanguageFile()
        {
            return "calendar-" + Customer.Current.LocaleSetting.Substring(0, 2) + ".js";
        }

        static public String StoreCurrency()
        {
            String tmpS = Currency.GetHomeCurrency();
            if (tmpS.Length == 0)
            {
                tmpS = "USD"; // set some default
            }
            return tmpS;
        }

        static public String StoreCurrencyNumericCode()
        {
            String tmpS = AppLogic.AppConfig("Localization.StoreCurrencyNumericCode");
            if (tmpS.Length == 0)
            {
                tmpS = "840"; // set some default
            }
            return tmpS;
        }

        static public bool ParseBoolean(String s)
        {
            try
            {
                return System.Boolean.Parse(s);
            }
            catch
            {
                return false;
            }
        }

        static public int ParseUSInt(String s)
        {
            int usi;
            System.Int32.TryParse(s, NumberStyles.Integer, USCulture, out usi); // use default locale setting
            return usi;
        }

        static public int ParseNativeInt(String s)
        {
            try
            {
                return Localization.ParseUSInt(s); // use default locale setting
            }
            catch
            {
                return 0;
            }
        }

        static public long ParseUSLong(String s)
        {
            long usl;
            System.Int64.TryParse(s, NumberStyles.Integer, USCulture, out usl); // use default locale setting
            return usl;
        }

        static public long ParseNativeLong(String s)
        {
            long nl;
            System.Int64.TryParse(s, NumberStyles.Integer, Thread.CurrentThread.CurrentUICulture, out nl); // use default locale setting
            return nl;
        }

        static public Single ParseUSSingle(String s)
        {
            Single uss;
            System.Single.TryParse(s, NumberStyles.Number, USCulture, out uss);
            return uss;
        }

        static public Single ParseNativeSingle(String s)
        {
            Single ns;
            System.Single.TryParse(s, NumberStyles.Number, Thread.CurrentThread.CurrentUICulture, out ns);
            return ns;
        }

        static public Double ParseUSDouble(String s)
        {
            Double usd;
            System.Double.TryParse(s, NumberStyles.Number, USCulture, out usd);
            return usd;
        }

        static public Double ParseNativeDouble(String s)
        {
            Double nd;
            System.Double.TryParse(s, NumberStyles.Number, Thread.CurrentThread.CurrentUICulture, out nd);
            return nd;
        }

        static public decimal ParseUSCurrency(String s)
        {
            Decimal usc;
            System.Decimal.TryParse(s, NumberStyles.Currency, USCulture, out usc);
            return usc;
        }

        static public decimal ParseNativeCurrency(String s)
        {
            Decimal nc;
            System.Decimal.TryParse(s, NumberStyles.Currency, Thread.CurrentThread.CurrentUICulture, out nc);
            return nc;
        }

        static public decimal ParseUSDecimal(String s)
        {
            Decimal usd;
            System.Decimal.TryParse(s, NumberStyles.Number, USCulture, out usd);
            return usd;
        }

        static public decimal ParseNativeDecimal(String s)
        {
            Decimal nd;
            System.Decimal.TryParse(s, NumberStyles.Number, Thread.CurrentThread.CurrentUICulture, out nd);
            return nd;
        }

        static public DateTime ParseUSDateTime(String s)
        {
            try
            {
                return System.DateTime.Parse(s, USCulture);
            }
            catch
            {
                return System.DateTime.MinValue;
            }
        }

        static public DateTime ParseNativeDateTime(String s)
        {
            try
            {
                return System.DateTime.Parse(s); // use default locale setting
            }
            catch
            {
                return System.DateTime.MinValue;
            }
        }


        static public String ToUSShortDateString(DateTime dt)
        {
            if (dt == System.DateTime.MinValue)
            {
                return String.Empty;
            }
            return dt.ToString("MM/dd/yy");
        }

        static public String ToNativeShortDateString(DateTime dt)
        {
            if (dt == System.DateTime.MinValue)
            {
                return String.Empty;
            }
            return dt.ToShortDateString();
        }

        static public String ToUSDateTimeString(DateTime dt)
        {
            if (dt == System.DateTime.MinValue)
            {
                return String.Empty;
            }
            return dt.ToString("MM/dd/yyyy HH:mm:ss");
        }

        static public String ToNativeDateTimeString(DateTime dt)
        {
            if (dt == System.DateTime.MinValue)
            {
                return String.Empty;
            }
            return dt.ToString(new CultureInfo(Customer.Current.LocaleSetting));
        }

        static public String ToDBDateTimeString(DateTime dt)
        {
            return DateTimeStringForDB(dt);
        }

        // no exchange rate is ever applied!
        // input amt is assumed to be in the store's PRIMARY CURRENCY!
        // converted to web.config locale format
        static public String CurrencyStringForDisplayWithoutExchangeRate(decimal amt)
        {
            return CurrencyStringForDisplayWithoutExchangeRate(amt, true);
        }

        // no exchange rate is ever applied!
        // input amt is assumed to be in the store's PRIMARY CURRENCY!
        // converted to web.config locale format
        static public String CurrencyStringForDisplayWithoutExchangeRate(decimal amt, bool ShowCurrency)
        {
            String tmpS = amt.ToString("C", new CultureInfo(Customer.Current.LocaleSetting));
            if (tmpS.StartsWith("("))
            {
                tmpS = "-" + tmpS.Replace("(", "").Replace(")", "");
            }
            if (ShowCurrency && Currency.NumPublishedCurrencies() > 1)
            {
                tmpS = String.Format("{0} ({1})", tmpS, Localization.GetPrimaryCurrency());
            }
            return tmpS;
        }

        // no exchange rate is ever applied!
        // input amt is assumed to be in the store's PRIMARY CURRENCY!
        // converted to format defined by LocaleSetting
        static public String CurrencyStringForDisplayWithoutExchangeRate(decimal amt, String TargetCurrencyCode)
        {
            String tmpS = String.Empty;
            // get currency specs for display control:
            String DisplaySpec = Currency.GetDisplaySpec(TargetCurrencyCode);
            String DisplayLocaleFormat = Currency.GetDisplayLocaleFormat(TargetCurrencyCode);

            if (DisplaySpec.Length != 0 && Currency.NumPublishedCurrencies() > 1)
            {
                String fmtCur = amt.ToString(DisplaySpec);
                tmpS = String.Format("{0} ({1})", fmtCur, TargetCurrencyCode);
            }
            else if (DisplayLocaleFormat.Length != 0)
            {
                tmpS = amt.ToString("C", new CultureInfo(DisplayLocaleFormat));
                if (tmpS.StartsWith("("))
                {
                    tmpS = "-" + tmpS.Replace("(", "").Replace(")", "");
                }
                if (Currency.NumPublishedCurrencies() > 1)
                {
                    tmpS = String.Format("{0} ({1})", tmpS, TargetCurrencyCode);
                }
            }
            else
            {
                tmpS = CurrencyStringForDisplayWithoutExchangeRate(amt); // use some generic default!
            }
            return tmpS;
        }

        // uses DisplaySpec if provided, else uses LocaleSetting
        // applies exchange rate!!
        // input amt is assumed to be in the store's PRIMARY CURRENCY!
        static public String CurrencyStringForDisplayWithExchangeRate(decimal amt, String TargetCurrencyCode)
        {
            String tmpS = String.Empty;
            // apply exchange rate if not outputting in primary store currency:
            amt = Currency.Convert(amt, Localization.GetPrimaryCurrency(), TargetCurrencyCode);
            return CurrencyStringForDisplayWithoutExchangeRate(amt, TargetCurrencyCode);
        }

        // input amt is assumed to be in the store's PRIMARY CURRENCY!
        static private String CurrencyStringForDisplayUSWithoutExchangeRate(decimal amt)
        {
            String tmpS = amt.ToString("C", USCulture);
            if (tmpS.StartsWith("("))
            {
                tmpS = "-" + tmpS.Replace("(", "").Replace(")", "");
            }
            if (Currency.NumPublishedCurrencies() > 1)
            {
                tmpS = String.Format("{0} ({1})", tmpS, "USD");
            }
            return tmpS;
        }

        // input amt is assumed to be in the store's PRIMARY CURRENCY!
        // NO formatting is applied. No exchange rates are applied
        // just returns xxxx.xx foramt
        static public String CurrencyStringForGatewayWithoutExchangeRate(decimal amt)
        {
            String s = amt.ToString("#.00", USCulture);
            if (s == ".00")
            {
                s = "0.00";
            }
            return s;
        }

        public static object IsNULLOrDBNull(object value, object replaceWith)
        {
            if (null == value || value is DBNull)
            {
                return replaceWith;
            }

            return value;
        }

        static public DateTime ParseDBDateTime(String s)
        {
            try
            {
                return System.DateTime.Parse(s, SqlServerCulture);
            }
            catch
            {
                return System.DateTime.MinValue;
            }
        }

        static public Double ParseDBDouble(String theval)
        {
            try
            {
                return System.Double.Parse(theval, SqlServerCulture);
            }
            catch
            {
                return 0.0D;
            }
        }

        static public Single ParseDBSingle(String theval)
        {
            try
            {
                return System.Single.Parse(theval, SqlServerCulture);
            }
            catch
            {
                return 0.0F;
            }
        }

        static public Decimal ParseDBDecimal(String theval)
        {
            try
            {
                return System.Decimal.Parse(theval, SqlServerCulture);
            }
            catch
            {
                return 0.0M;
            }
        }

        static public String ParseDBDecimal(Decimal theval)
        {
            try
            {
                return theval.ToString(SqlServerCulture);
            }
            catch
            {
                return Decimal.Zero.ToString();
            }
        }

        static public DateTime ParseLocaleDateTime(String theval, String LocaleSetting)
        {
            try
            {
                return System.DateTime.Parse(theval, new CultureInfo(LocaleSetting));
            }
            catch
            {
                return System.DateTime.MinValue;
            }
        }

        static public Double ParseLocaleDouble(String theval, String LocaleSetting)
        {
            try
            {
                return System.Double.Parse(theval, new CultureInfo(LocaleSetting));
            }
            catch
            {
                return 0.0D;
            }
        }

        static public Single ParseLocaleSingle(String theval, String LocaleSetting)
        {
            try
            {
                return System.Single.Parse(theval, new CultureInfo(LocaleSetting));
            }
            catch
            {
                return 0.0F;
            }
        }

        static public Decimal ParseLocaleDecimal(String theval, String LocaleSetting)
        {
            try
            {
                return System.Decimal.Parse(theval, new CultureInfo(LocaleSetting));
            }
            catch
            {
                return 0.0M;
            }
        }

        static public String ParseLocaleDecimal(Decimal value, String LocaleSetting)
        {            
            try
            {
                NumberFormatInfo formatter = (new CultureInfo(LocaleSetting)).NumberFormat;
                formatter.NumberDecimalDigits = InterpriseHelper.GetInventoryDecimalPlacesPreference();
                if (value.ToString("N", formatter).Contains(formatter.NumberDecimalSeparator.ToString()))
                {
                    string ret = value.ToString("N", formatter).TrimEnd(GetNumberZeroLocale(LocaleSetting));
                    if (ret.LastIndexOf(formatter.NumberDecimalSeparator) == ret.Length - 1)
                    {
                        ret = ret.Substring(0, ret.Length - 1);
                    }
                    return ret;
                }
                else
                {
                    return value.ToString("N", formatter);
                }
            }
            catch
            {
                return value.ToString();
            }
        }


        /// <summary>
        /// Converts a string in the SourceLocaleSetting format to a date string in the DestLocaleSetting format
        /// </summary>
        /// <param name="theval"></param>
        /// <param name="SourceLocaleSetting"></param>
        /// <param name="DestLocaleSetting"></param>
        /// <returns></returns>
        static public string ConvertLocaleDateTime(String theval, String SourceLocaleSetting, String DestLocaleSetting)
        {
            try
            {
                return System.DateTime.Parse(theval, new CultureInfo(SourceLocaleSetting)).ToString(new CultureInfo(DestLocaleSetting));
            }
            catch
            {
                return System.DateTime.MinValue.ToString(new CultureInfo(DestLocaleSetting));
            }
        }

        
        // ----------------------------------------------------------------------------------------------
        // the following routines must (should) work in ALL locales, no matter what SQL Server setting is
        // ----------------------------------------------------------------------------------------------
        [Obsolete("Use this extension method: dt.ToDateTimeStringForDB()")]
        static public String DateTimeStringForDB(DateTime dt)
        {
            return dt.ToString("s");
        }

        static public String CurrencyStringForDBWithoutExchangeRate(decimal amt)
        {
            String tmpS = amt.ToString("C", USCulture);
            if (tmpS.StartsWith("("))
            {
                tmpS = "-" + tmpS.Replace("(", "").Replace(")", "");
            }
            return tmpS.Replace("$", "").Replace(",", "");
        }

        static public String IntStringForDB(int amt)
        {
            return amt.ToString("G", USCulture).Replace(",", "");
        }

        static public String SingleStringForDB(Single amt)
        {
            return amt.ToString("G", USCulture).Replace(",", "");
        }

        static public String DoubleStringForDB(double amt)
        {
            return amt.ToString("G", USCulture).Replace(",", "");
        }

        static public String DecimalStringForDB(decimal amt)
        {
            return amt.ToString("G", USCulture).Replace(",", "");
        }

        public static string CompanyLocale()
        {
            string locale = Thread.CurrentThread.CurrentCulture.Name;

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "SELECT sl.ShortString FROM SystemCompanyInformation sci with (NOLOCK) INNER JOIN SystemLanguage sl with (NOLOCK) ON sci.CompanyLanguage = sl.LanguageCode"))
                {
                    if (reader.Read())
                    {
                        locale = DB.RSField(reader, "ShortString");
                    }
                }
            }

            return locale;
        }

        // ------------------------------------------------------------------------------
        // W3C DateTime Formats:
        // ------------------------------------------------------------------------------
        public struct W3CDateTime
        {
            private DateTime dtime;
            private TimeSpan ofs;

            public W3CDateTime(DateTime dt, TimeSpan off)
            {
                ofs = off;
                dtime = dt;
            }

            public DateTime UtcTime
            {
                get { return dtime; }
            }

            public DateTime DateTime
            {
                get { return dtime + ofs; }
            }

            public TimeSpan UtcOffset
            {
                get { return ofs; }
            }

            static public W3CDateTime Parse(string s)
            {
                const string Rfc822DateFormat =
                          @"^((Mon|Tue|Wed|Thu|Fri|Sat|Sun), *)?(?<day>\d\d?) +" +
                          @"(?<month>Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec) +" +
                          @"(?<year>\d\d(\d\d)?) +" +
                          @"(?<hour>\d\d):(?<min>\d\d)(:(?<sec>\d\d))? +" +
                          @"(?<ofs>([+\-]?\d\d\d\d)|UT|GMT|EST|EDT|CST|CDT|MST|MDT|PST|PDT)$";
                const string W3CDateFormat =
                          @"^(?<year>\d\d\d\d)" +
                          @"(-(?<month>\d\d)(-(?<day>\d\d)(T(?<hour>\d\d):(?<min>\d\d)(:(?<sec>\d\d)(?<ms>\.\d+)?)?" +
                          @"(?<ofs>(Z|[+\-]\d\d:\d\d)))?)?)?$";

                string combinedFormat = string.Format(
                    @"(?<rfc822>{0})|(?<w3c>{1})", Rfc822DateFormat, W3CDateFormat);

                // try to parse it
                Regex reDate = new Regex(combinedFormat);
                Match m = reDate.Match(s);
                if (!m.Success)
                {
                    // Didn't match either expression. Throw an exception.
                    throw new FormatException("String is not a valid date time stamp.");
                }
                try
                {
                    bool isRfc822 = m.Groups["rfc822"].Success;
                    int year = int.Parse(m.Groups["year"].Value);
                    // handle 2-digit and 3-digit years
                    if (year < 1000)
                    {
                        if (year < 50) year = year + 2000; else year = year + 1999;
                    }

                    int month;
                    if (isRfc822)
                        month = ParseRfc822Month(m.Groups["month"].Value);
                    else
                        month = (m.Groups["month"].Success) ? int.Parse(m.Groups["month"].Value) : 1;

                    int day = m.Groups["day"].Success ? int.Parse(m.Groups["day"].Value) : 1;
                    int hour = m.Groups["hour"].Success ? int.Parse(m.Groups["hour"].Value) : 0;
                    int min = m.Groups["min"].Success ? int.Parse(m.Groups["min"].Value) : 0;
                    int sec = m.Groups["sec"].Success ? int.Parse(m.Groups["sec"].Value) : 0;
                    int ms = m.Groups["ms"].Success ? (int)Math.Round((1000 * double.Parse(m.Groups["ms"].Value))) : 0;

                    TimeSpan ofs = TimeSpan.Zero;
                    if (m.Groups["ofs"].Success)
                    {
                        if (isRfc822)
                            ofs = ParseRfc822Offset(m.Groups["ofs"].Value);
                        else
                            ofs = ParseW3COffset(m.Groups["ofs"].Value);
                    }
                    // datetime is stored in UTC
                    return new W3CDateTime(new DateTime(year, month, day, hour, min, sec, ms) - ofs, ofs);
                }
                catch (Exception ex)
                {
                    throw new FormatException("String is not a valid date time stamp.", ex);
                }
            }

            private static readonly string[] MonthNames = new string[]
	{
		"Jan", "Feb", "Mar", "Apr", "May", "Jun", 
		"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

            private static int ParseRfc822Month(string monthName)
            {
                for (int i = 0; i < 12; i++)
                {
                    if (monthName == MonthNames[i])
                    {
                        return i + 1;
                    }
                }
                throw new ApplicationException("Invalid month name");
            }

            private static TimeSpan ParseRfc822Offset(string s)
            {
                if (s == string.Empty)
                    return TimeSpan.Zero;
                int hours = 0;
                switch (s)
                {
                    case "UT":
                    case "GMT":
                        break;
                    case "EDT": hours = -4; break;
                    case "EST":
                    case "CDT": hours = -5; break;
                    case "CST":
                    case "MDT": hours = -6; break;
                    case "MST":
                    case "PDT": hours = -7; break;
                    case "PST": hours = -8; break;
                    default:
                        if (s[0] == '+')
                        {
                            string sfmt = s.Substring(1, 2) + ":" + s.Substring(3, 2);
                            return TimeSpan.Parse(sfmt);
                        }
                        else
                            return TimeSpan.Parse(s.Insert(s.Length - 2, ":"));
                }
                return TimeSpan.FromHours(hours);
            }

            private static TimeSpan ParseW3COffset(string s)
            {
                if (s == string.Empty || s == "Z")
                    return TimeSpan.Zero;
                else
                {
                    if (s[0] == '+')
                        return TimeSpan.Parse(s.Substring(1));
                    else
                        return TimeSpan.Parse(s);
                }
            }
        }

        // ------------------------------------------------------------------------------
        // Type Formatting
        // ------------------------------------------------------------------------------
        public static string FormatDecimal2Places(decimal temp)
        {
            return temp.ToString("N2");
        }

        public static string FormatDecimal2Places(string temp)
        {
            decimal dec = Localization.ParseDBDecimal(temp);
            return dec.ToString("N2");
        }

        public static string FormatDecimal(string sDecimalValue, string intFixPlaces)
        {
            return ServiceFactory.GetInstance<ILocalizationService>()
                                 .FormatDecimal(sDecimalValue, intFixPlaces);
        }

        //Locale Number Format Setting
        public static Char[] GetNumberDecimalSeparatorLocale(String LocaleSetting)
        {
            NumberFormatInfo numberInfo = new CultureInfo(LocaleSetting).NumberFormat;
            return numberInfo.NumberDecimalSeparator.ToCharArray();
        }

        public static String GetNumberDecimalSeparatorLocaleString(String LocaleSetting)
        {           
            return String.Join(string.Empty,GetNumberDecimalSeparatorLocale(LocaleSetting));
        }

        public static Char[] GetNumberGroupSeparatorLocale(String LocaleSetting)
        {
            NumberFormatInfo numberInfo = new CultureInfo(LocaleSetting).NumberFormat;
            return numberInfo.NumberGroupSeparator.ToCharArray();
        }

        public static String GetNumberGroupSeparatorLocaleString(String LocaleSetting)
        {
            return String.Join(string.Empty, GetNumberGroupSeparatorLocale(LocaleSetting));
        }

        public static Char[] GetNumberZeroLocale(String LocaleSetting)
        {
            try
            {
                NumberFormatInfo numberInfo = new CultureInfo(LocaleSetting).NumberFormat;
                return numberInfo.NativeDigits[0].ToCharArray();
            }
            catch
            {
                return decimal.Zero.ToString().ToCharArray();
            }
        }

        public static String GetNumberZeroLocaleString(String LocaleSetting)
        {
            return String.Join(string.Empty, GetNumberZeroLocale(LocaleSetting));
        }

        public static Int32 GetNumberGroupSizes(String LocaleSetting)
        {
            try
            {
                NumberFormatInfo numberInfo = new CultureInfo(LocaleSetting).NumberFormat;
                return numberInfo.NumberGroupSizes[0];
            }
            catch
            {
                return 3; //default
            }
        }
    }
}
