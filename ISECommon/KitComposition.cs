// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Interprise.Facade.Customer;
using Interprise.Facade.Base;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon
{
    public class KitCartItem
    {
        public string ItemKitCode = string.Empty;
        public string CustomerCode = string.Empty;
        public string ItemCode = string.Empty;
        public string GroupCode = string.Empty;
        public int ItemKitCounter = 0;

        public string UnitMeasureCode = string.Empty;
        public decimal UnitmeasureQuantity = decimal.Zero;

        public decimal Cost = decimal.Zero;
        public decimal CostRate = decimal.Zero;
        public decimal Price = decimal.Zero;
        public decimal PriceRate = decimal.Zero;

        public bool Match(KitCartItem other)
        {
            return  this.ItemKitCode.Equals(other.ItemKitCode) &&
                    this.CustomerCode.Equals(other.CustomerCode) &&
                    this.ItemCode.Equals(other.ItemCode) &&
                    this.GroupCode.Equals(other.GroupCode);
        }
    }

    public class KitComposition
    {
        private Guid _cartID = Guid.Empty;
        private List<KitCartItem> _compositions = new List<KitCartItem>();
        private Customer _thisCustomer = null;
        private string _itemKitCode = string.Empty;
        private string _pricingType = string.Empty;
        public string _unitMeasureCode = string.Empty;
        private decimal _quantity = decimal.One, _unitQuantity = decimal.One;

        private KitComposition(Customer forCustomer, string itemKitCode, Guid cartId)
        {
            this._thisCustomer = forCustomer;
            this._itemKitCode = itemKitCode;
            this._cartID = cartId;
        }

        public Guid CartID
        {
            get { return _cartID; }
            set { _cartID = value;; }
        }

        public string ItemKitCode
        {
            get{return _itemKitCode;}
        }

        public string PricingType
        {
            get { return _pricingType; }
            set { _pricingType = value; }
        }

        public List<KitCartItem> Compositions
        {
            get { return _compositions; }
        }

        public Customer ThisCustomer
        {
            get{return _thisCustomer;}
        }

        public string UnitMeasureCode
        {
            get { return _unitMeasureCode; }
            set { _unitMeasureCode = value; }
        }

        public decimal UnitQuantity
        {
            get { return _unitQuantity; }
            set { _unitQuantity = value; }
        }
        public decimal Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }

        public bool Matches(KitComposition other)
        {
            if (this.ItemKitCode != other.ItemKitCode || this.PricingType != other.PricingType) return false;
            if (this.Compositions.Count != other.Compositions.Count) return false;

            bool matchesAll = false;

            foreach (KitCartItem kitItem in Compositions)
            {
                matchesAll = (other.Compositions.Find(kitItem.Match) != null);

                if (!matchesAll)
                {
                    return false;
                }
            }

            return true;
        }

        public void AddToCart()
        {
            AppLogic.AddKitItem(CartID, ThisCustomer.CustomerCode, this.ItemKitCode, null, null, this.Compositions);
        }

        public void AddToGiftRegistry(Guid giftRegistryID, Guid registryItemCode)
        {
            AppLogic.AddKitItem(null, ThisCustomer.CustomerCode, this.ItemKitCode, giftRegistryID, registryItemCode, this.Compositions);
        }

        public decimal GetSalesPrice(ref decimal vat, string CategoryCode, string shipToCode = "")
        {
            decimal salesPrice = Decimal.Zero;

            decimal price = Decimal.Zero;
            decimal priceRate = Decimal.Zero;
            decimal cost = Decimal.Zero;
            decimal costRate = Decimal.Zero;
            decimal kitDiscount = ServiceFactory.GetInstance<IProductService>().GetKitDiscount(ItemKitCode);
            vat = Decimal.Zero;

            foreach (KitCartItem kitItem in this.Compositions)
            {
                price += kitItem.Price;
                priceRate += kitItem.PriceRate * this.UnitQuantity;
            }

            //This is for computing the correct price for White bag and sticker bundles.
            if (CategoryCode == "White bag and sticker bundles" || CategoryCode == "Brown bag and sticker bundles")
            {
                priceRate *= this.Quantity;
            }
            salesPrice = priceRate;
            // NOTE:
            //  In interprise, the kit details is defined to whatever is set on the Currency on the Selling Language
            //  If the current customer's currency is not                 
            bool isCustomerCurrencyIncludedForInventorySelling = InterpriseHelper.IsCurrencyIncludedForInventorySelling(ThisCustomer.CurrencyCode);

            // Special case 2 
            //  Currency is added in Inventory Selling Currency late
            //  after the kit has been created, ideally the kit should regenerate kit pricing for this currency
            //  but for the meantime, we should handle this by looking into the home currency
            bool currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem =
            InterpriseHelper.CurrencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYet(ThisCustomer.CurrencyCode, this.ItemKitCode);

            // if the currency is not included in the selling currencies for inventory
            // then get the rates for the Base currency -- The convert the exchange rate
            string lookupCurrency = String.Empty;
            // the current exchange rate if we should convert
            decimal exchangeRate = Decimal.Zero;
            if (isCustomerCurrencyIncludedForInventorySelling &&
                !currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem)
            {
                lookupCurrency = ThisCustomer.CurrencyCode;
            }
            else
            {
                lookupCurrency = Currency.GetHomeCurrency();
                exchangeRate = Currency.GetExchangeRate(ThisCustomer.CurrencyCode);
            }

            //compute the cost
            foreach (KitCartItem kitItem in this.Compositions)
            {
                var eCommerceProductPriceInfo = ServiceFactory.GetInstance<IProductService>().GetProductPriceInfo(kitItem.ItemCode,
                                                               kitItem.UnitMeasureCode, this.Quantity, kitItem.UnitmeasureQuantity, Decimal.Zero);
                cost += eCommerceProductPriceInfo.BaseAverageCost;
                costRate += SimpleFacade.Instance.ConvertCurrency(exchangeRate, cost, false, ThisCustomer.CurrencyCode, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.Total);
            }

            string customerCode = ThisCustomer.CustomerCode;
            if (shipToCode.IsNullOrEmptyTrimmed()) { shipToCode = ThisCustomer.DefaultShippingCode; }

            if (ThisCustomer.IsNotRegistered)
            {
                customerCode = ThisCustomer.AnonymousCustomerCode;
                if (shipToCode.IsNullOrEmptyTrimmed()) { shipToCode = ThisCustomer.AnonymousShipToCode; }
            }

            //price *= this.Quantity;
            //priceRate *= this.Quantity;
            //cost *= this.Quantity;
            //costRate *= this.Quantity;

            if (shipToCode.IsNullOrEmptyTrimmed())
            {
                vat = ItemTaxFacade.CalculateTax(customerCode,
                    this.ItemKitCode,
                    this.UnitMeasureCode,
                    price,
                    priceRate,
                    cost,
                    costRate,
                    this.Quantity);
            }
            else
            {
                vat = ItemTaxFacade.CalculateTaxByShipTo(customerCode,
                     shipToCode,
                     this.ItemKitCode,
                     this.UnitMeasureCode,
                     ThisCustomer.CurrencyCode,
                     price,
                     priceRate,
                     cost,
                     costRate,
                     this.Quantity);
            }



            if (ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
            {
                salesPrice += vat;
            }
            salesPrice = salesPrice - (kitDiscount * salesPrice);
            //recompute tax
            vat = vat - (kitDiscount * vat);
            return salesPrice;
        }

        public static KitComposition FromForm(Customer thisCustomer, CartTypeEnum cartType, string itemKitCode)
        {
            string kitContents = CommonLogic.FormCanBeDangerousContent("KitItems", false);
            return FromComposition(kitContents, thisCustomer, cartType, itemKitCode);
        }

        public static KitComposition FromForm(Customer thisCustomer, string itemKitCode)
        {
            string kitContents = CommonLogic.FormCanBeDangerousContent("KitItems", false);
            return FromComposition(kitContents, thisCustomer, CartTypeEnum.GiftRegistryCart, itemKitCode);
        }

        public static KitComposition FromComposition(string kitContents, Customer thisCustomer, CartTypeEnum cartType, string itemKitCode)
        {
            KitComposition composition = null;

            if (!kitContents.IsNullOrEmptyTrimmed())
            {
                // NOTE:
                //  In interprise, the kit details is defined to whatever is set on the Currency on the Selling Language
                //  If the current customer's currency is not 
                bool isCustomerCurrencyIncludedForInventorySelling = InterpriseHelper.IsCurrencyIncludedForInventorySelling(thisCustomer.CurrencyCode);

                // Special case 2 
                //  Currency is added in Inventory Selling Currency late
                //  after the kit has been created, ideally the kit should regenerate kit pricing for this currency
                //  but for the meantime, we should handle this by looking into the home currency
                bool currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem =
                InterpriseHelper.CurrencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYet(thisCustomer.CurrencyCode, itemKitCode);

                bool useCustomerPricing = ServiceFactory.GetInstance<IInventoryRepository>().UseCustomerPricingForKit(itemKitCode);

                // if the currency is not included in the selling currencies for inventory
                // then get the rates for the Base currency -- The convert the exchange rate
                string lookupCurrency = String.Empty;
                // the current exchange rate if we should convert
                decimal exchangeRate = Decimal.Zero;
                if (isCustomerCurrencyIncludedForInventorySelling &&
                    !currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem)
                {
                    lookupCurrency = thisCustomer.CurrencyCode;
                }
                else
                {
                    lookupCurrency = Currency.GetHomeCurrency();
                    exchangeRate = Currency.GetExchangeRate(thisCustomer.CurrencyCode);
                }

                string[] selectedItems = kitContents.Split(',');
                composition = new KitComposition(thisCustomer, itemKitCode, Guid.Empty);

                if (selectedItems.Length > 0)
                {
                    using (var con = new SqlConnection(DB.GetDBConn()))
                    {
                        con.Open();

                        using (var cmdGetKitCodes = new SqlCommand("eCommerceGetKitCodes", con))
                        {
                            cmdGetKitCodes.CommandType = CommandType.StoredProcedure;

                            var paramItemKitCode = new SqlParameter("@ItemKitcode", SqlDbType.NVarChar, 30);
                            var paramKitItemCounter = new SqlParameter("@KitItemCounter", SqlDbType.Int);
                            var paramKitGroupCounter = new SqlParameter("@KitGroupCounter", SqlDbType.Int);
                            var paramCurrencyCode = new SqlParameter("@CurrencyCode", SqlDbType.NVarChar, 30);

                            cmdGetKitCodes.Parameters.Add(paramItemKitCode);
                            cmdGetKitCodes.Parameters.Add(paramKitItemCounter);
                            cmdGetKitCodes.Parameters.Add(paramKitGroupCounter);
                            cmdGetKitCodes.Parameters.Add(paramCurrencyCode);

                            paramItemKitCode.Value = itemKitCode;
                            paramCurrencyCode.Value = lookupCurrency; 

                            foreach (string kitGroup in selectedItems)
                            {
                                string[] groups = kitGroup.Split(DomainConstants.KITCOMPOSITION_DELIMITER);
                                if (groups.Length != 2)
                                {
                                    // kit from form post composition is BROKEN..
                                    return null;
                                }

                                int kitGroupCounter, kitItemCounter;
                                if (int.TryParse(groups[0], out kitGroupCounter) && 
                                    int.TryParse(groups[1], out kitItemCounter))
                                {
                                    paramKitItemCounter.Value = kitItemCounter;
                                    paramKitGroupCounter.Value = kitGroupCounter;

                                    using (IDataReader reader = cmdGetKitCodes.ExecuteReader())
                                    {
                                        if (reader.Read())
                                        {
                                            KitCartItem selectedKitItem = new KitCartItem();
                                            selectedKitItem.CustomerCode = thisCustomer.CustomerCode;
                                            selectedKitItem.ItemKitCode = itemKitCode;

                                            selectedKitItem.ItemCode = DB.RSField(reader, "ItemCode");
                                            selectedKitItem.GroupCode = DB.RSField(reader, "GroupCode");

                                            // NOTE:
                                            // We don't need to cost price at this point
                                            // and it's an overhead to compute it
                                            // so we'll just fill it up later when we compute
                                            // the sales price, if we have tax (where needed)
                                            selectedKitItem.Cost = Decimal.Zero;
                                            selectedKitItem.CostRate = Decimal.Zero;

                                            selectedKitItem.UnitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                                            selectedKitItem.UnitmeasureQuantity = DB.RSFieldDecimal(reader, "UnitMeasureQuantity");

                                            selectedKitItem.Price = DB.RSFieldDecimal(reader, "Total");
                                            selectedKitItem.PriceRate = DB.RSFieldDecimal(reader, "TotalRate");

                                            decimal promotionalPrice = Decimal.Zero;

                                            if (useCustomerPricing)
                                            {
                                                selectedKitItem.PriceRate = InterpriseHelper.GetKitComponentPrice(thisCustomer.CustomerCode, selectedKitItem.ItemCode, thisCustomer.CurrencyCode, selectedKitItem.UnitmeasureQuantity, selectedKitItem.UnitmeasureQuantity, selectedKitItem.UnitMeasureCode, ref promotionalPrice);
                                                if (!promotionalPrice.Equals(Decimal.Zero)) { selectedKitItem.PriceRate = promotionalPrice; }
                                            }

                                            if (isCustomerCurrencyIncludedForInventorySelling &&
                                                currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem)
                                            {
                                                selectedKitItem.PriceRate = Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(exchangeRate, selectedKitItem.PriceRate, false, thisCustomer.CurrencyCode, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.Total);
                                            }

                                            composition.Compositions.Add(selectedKitItem);
                                        }
                                        else
                                        {
                                            // kit item non-existent, kit composition is therefore BROKEN
                                            return null;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    // kit composition details are missing
                    return null;
                }
            }

            return composition;
        }

        public static KitComposition FromCart(Customer thisCustomer, CartTypeEnum cartType, string itemKitCode, Guid cartID, decimal parentUOMQty)
        {
            // NOTE:
            //  In interprise, the kit details is defined to whatever is set on the Currency on the Selling Language
            //  If the current customer's currency is not 
            bool isCustomerCurrencyIncludedForInventorySelling = InterpriseHelper.IsCurrencyIncludedForInventorySelling(thisCustomer.CurrencyCode);

            // Special case 2 
            //  Currency is added in Inventory Selling Currency late
            //  after the kit has been created, ideally the kit should regenerate kit pricing for this currency
            //  but for the meantime, we should handle this by looking into the home currency
            bool currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem =
            InterpriseHelper.CurrencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYet(thisCustomer.CurrencyCode, itemKitCode);

            bool useCustomerPricing = ServiceFactory.GetInstance<IInventoryRepository>().UseCustomerPricingForKit(itemKitCode);

            // if the currency is not included in the selling currencies for inventory
            // then get the rates for the Base currency -- The convert the exchange rate
            string lookupCurrency = string.Empty;
            // the current exchange rate if we should convert
            decimal exchangeRate = Decimal.Zero;
            if (isCustomerCurrencyIncludedForInventorySelling &&
                !currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem)
            {
                lookupCurrency = thisCustomer.CurrencyCode;
            }
            else
            {
                lookupCurrency = Currency.GetHomeCurrency();
                exchangeRate = Currency.GetExchangeRate(thisCustomer.CurrencyCode);
            }

            var composition = new KitComposition(thisCustomer, itemKitCode, cartID);
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "exec EcommerceGetKitCartComposition @CustomerCode = {0}, @CartID = {1}, @CartType = {2}, @CurrencyCode = {3}", DB.SQuote(thisCustomer.CustomerCode), DB.SQuote(cartID.ToString()), (int)cartType, DB.SQuote(lookupCurrency)))
                {
                    while (reader.Read())
                    {
                        var kit = new KitCartItem();
                        kit.ItemKitCode = reader.ToRSField("ItemKitCode");
                        kit.ItemCode = reader.ToRSField("ItemCode");
                        kit.CustomerCode = reader.ToRSField("CustomerCode");
                        kit.GroupCode = reader.ToRSField("GroupCode");
                        kit.ItemKitCounter = reader.ToRSFieldInt("Counter");

                        // NOTE:
                        // We don't need to cost price at this point
                        // and it's an overhead to compute it
                        // so we'll just fill it up later when we compute
                        // the sales price, if we have tax (where needed)
                        kit.Cost = Decimal.Zero;
                        kit.CostRate = Decimal.Zero;
                        kit.UnitMeasureCode = reader.ToRSField("UnitMeasureCode");
                        kit.UnitmeasureQuantity = reader.ToRSFieldDecimal("UnitMeasureQuantity");
                        kit.Price = reader.ToRSFieldDecimal("Total");
                        kit.PriceRate = reader.ToRSFieldDecimal("TotalRate");

                        decimal promotionalPrice = Decimal.Zero, cost = Decimal.Zero;

                        if (useCustomerPricing)
                        {
                            kit.PriceRate = InterpriseHelper.GetKitComponentPrice(thisCustomer.CustomerCode, kit.ItemCode, thisCustomer.CurrencyCode, kit.UnitmeasureQuantity, parentUOMQty, kit.UnitMeasureCode, ref promotionalPrice, ref cost);
                            if (!promotionalPrice.Equals(Decimal.Zero)) { kit.PriceRate = promotionalPrice; }
                        }

                        if (isCustomerCurrencyIncludedForInventorySelling &&
                            currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem)
                        {
                            kit.PriceRate = Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(exchangeRate, kit.PriceRate, false, thisCustomer.CurrencyCode, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.Total);
                        }

                        composition.PricingType = reader.ToRSField("KitPricingType");

                        composition.Compositions.Add(kit);
                    }
                }
            }

            return composition;
        }

        public static KitComposition FromSalesOrder(Customer thisCustomer, string salesOrderCode, string itemKitCode, int lineNum)
        {
            KitComposition composition = new KitComposition(thisCustomer, itemKitCode, Guid.Empty);


            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "exec eCommerceGetKitOrderComposition @SalesOrderCode = {0}, @LineNum = {1}", DB.SQuote(salesOrderCode), lineNum))
                {
                    while (reader.Read())
                    {
                        KitCartItem kit = new KitCartItem();
                        kit.ItemKitCode = DB.RSField(reader, "ItemKitCode");
                        kit.ItemCode = DB.RSField(reader, "ItemCode");
                        kit.CustomerCode = thisCustomer.CustomerCode;
                        kit.GroupCode = DB.RSField(reader, "GroupCode");
                        kit.GroupCode = DB.RSField(reader, "GroupCode");
                        kit.GroupCode = DB.RSField(reader, "GroupCode");

                        composition.PricingType = DB.RSField(reader, "KitPricingType");

                        composition.Compositions.Add(kit);
                    }
                }
            }

            return composition;
        }
       
    }
}












