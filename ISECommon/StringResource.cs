// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Web;
using System.Xml;

namespace InterpriseSuiteEcommerceCommon
{
    public class StringResource
    {

        #region private variables

        private int m_Stringresourceid = -1;
        private Guid m_Stringresourceguid = new Guid("00000000000000000000000000000000");
        private string m_Name = string.Empty;
        private string m_Localesetting = string.Empty;
        private string m_Configvalue = string.Empty;
        private DateTime m_Createdon = DateTime.MinValue;
        private bool m_Modified = false;

        #endregion

        #region contructors

        public StringResource() { }


        public StringResource(int StringResourceID, Guid StringResourceGUID, string Name, string LocaleSetting, string ConfigValue, DateTime CreatedOn, bool Modified)
        {
            m_Stringresourceid = StringResourceID;
            m_Stringresourceguid = StringResourceGUID;
            m_Name = Name;
            m_Localesetting = LocaleSetting;
            m_Configvalue = ConfigValue;
            m_Createdon = CreatedOn;
            m_Modified = Modified;
        }

        #endregion

        #region public properties

        public int StringResourceID
        {
            get { return m_Stringresourceid; }
        }

        public Guid StringResourceGUID
        {
            get { return m_Stringresourceguid; }
        }

        public string Name
        {
            get { return m_Name; }
        }

        public string LocaleSetting
        {
            get { return m_Localesetting; }
        }

        public string ConfigValue
        {
            get { return m_Configvalue; }
            set { m_Configvalue = value; }
        }


        public DateTime CreatedOn
        {
            get { return m_Createdon; }
        }

        public bool Modified
        {
            get { return m_Modified; }
        }

        #endregion

        private StringResource(string locale, Guid guid, string name, string value)
        {
            m_Stringresourceid = -1;
            m_Stringresourceguid = guid;
            m_Name = name;
            m_Localesetting = locale;
            m_Configvalue = value;
            m_Createdon = DateTime.Now;
            m_Modified = false;
        }

        public static StringResource New(string locale, string name, string value)
        {
            return new StringResource(locale, Guid.NewGuid(), name, value);
        }

        public void Save()
        {
            using(SqlConnection con = new SqlConnection(DB.GetDBConn()))
            {
                con.Open();

                using(SqlCommand cmdSave = new SqlCommand("EcommerceSaveStringResource", con))
                {
                    cmdSave.CommandType = CommandType.StoredProcedure;

                    cmdSave.Parameters.Add(new SqlParameter("@StringResourceGuid", SqlDbType.UniqueIdentifier));
                    cmdSave.Parameters.Add(new SqlParameter("@WebsiteCode", SqlDbType.NVarChar, 30));
                    cmdSave.Parameters.Add(new SqlParameter("@Name", SqlDbType.NVarChar, 100));
                    cmdSave.Parameters.Add(new SqlParameter("@LocaleSetting", SqlDbType.NVarChar, 10));
                    cmdSave.Parameters.Add(new SqlParameter("@ConfigValue", SqlDbType.NVarChar, 25000));
                    cmdSave.Parameters.Add(new SqlParameter("@CreatedOn", SqlDbType.SmallDateTime));
                    cmdSave.Parameters.Add(new SqlParameter("@Modified", SqlDbType.Bit));
                    cmdSave.Parameters.Add(new SqlParameter("@UserCreated", SqlDbType.NVarChar, 30));
                    cmdSave.Parameters.Add(new SqlParameter("@DateCreated", SqlDbType.SmallDateTime));
                    cmdSave.Parameters.Add(new SqlParameter("@UserModified", SqlDbType.NVarChar, 30));
                    cmdSave.Parameters.Add(new SqlParameter("@DateModified", SqlDbType.SmallDateTime));

                    SqlParameter paramId = new SqlParameter("@StringResourceID", SqlDbType.Int);
                    paramId.Direction = ParameterDirection.Output;

                    cmdSave.Parameters.Add(paramId);

                    // save core routine
                    this.Save(cmdSave);
                }
            }

        }

        internal void Save(SqlCommand usingThisCommand)
        {
            usingThisCommand.Parameters["@StringResourceGuid"].Value = this.m_Stringresourceguid;
            usingThisCommand.Parameters["@WebsiteCode"].Value = InterpriseHelper.ConfigInstance.WebSiteCode;
            usingThisCommand.Parameters["@Name"].Value = this.m_Name;
            usingThisCommand.Parameters["@LocaleSetting"].Value = this.m_Localesetting;
            usingThisCommand.Parameters["@ConfigValue"].Value = this.m_Configvalue;
            usingThisCommand.Parameters["@CreatedOn"].Value = this.m_Createdon;
            usingThisCommand.Parameters["@Modified"].Value = this.m_Modified;
            usingThisCommand.Parameters["@UserCreated"].Value = InterpriseHelper.ConfigInstance.UserCode;
            usingThisCommand.Parameters["@DateCreated"].Value = DateTime.Now;
            usingThisCommand.Parameters["@UserModified"].Value = InterpriseHelper.ConfigInstance.UserCode;
            usingThisCommand.Parameters["@DateModified"].Value = DateTime.Now;

            usingThisCommand.ExecuteNonQuery();

            this.m_Stringresourceid = (int)usingThisCommand.Parameters["@StringResourceID"].Value;
        }

        public static List<string> GetAvailableLocales()
        {
            List<string> locales = new List<string>();

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "SELECT sl.ShortString FROM SystemSellingLanguage ssl with (NOLOCK) INNER JOIN SystemLanguage sl with (NOLOCK) ON sl.LanguageCode = ssl.LanguageCode WHERE ssl.IsIncluded = 1"))
                {
                    while (reader.Read())
                    {
                        locales.Add(DB.RSField(reader, "ShortString"));
                    }
                }
            }

            return locales;
        }
    }

    public class StringResources : NameObjectCollectionBase
    {
        [Obsolete("Use the method : ServiceFactory.GetInstance<IAppConfigService>().GetAllStringResource() if fully migrated to CB13.1.3/CB13.2 and above")]
        public static StringResources GetAll()
        {
            StringResources resources = new StringResources();

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "EcommerceGetStringresource @WebsiteCode = {0}", DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode)))
                {
                    while (reader.Read())
                    {
                        string locale = DB.RSField(reader, "LocaleSetting").ToLowerInvariant();
                        string name = DB.RSField(reader, "Name").ToLowerInvariant();
                        string lookupKey = string.Format("{0}_{1}", locale, name);

                        StringResource resource =
                        new StringResource(
                            DB.RSFieldInt(reader, "StringResourceID"),
                            DB.RSFieldGUID2(reader, "StringResourceGUID"),
                            name,
                            locale,
                            DB.RSField(reader, "ConfigValue"),
                            DB.RSFieldDateTime(reader, "CreatedOn"),
                            false
                        );

                        resources.Add(resource);
                    }
                }
                if (resources.Count == 0)
                {
                    throw new ArgumentException("No String Resource loaded. Please import String Resource using Connected Business Client.");
                }
            }

            return resources;
        }

        public StringResource this[int index]
        {
            get { return base.BaseGet(index) as StringResource; }
        }

        public StringResource this[string key]
        {
            get { return base.BaseGet(key) as StringResource; }
        }

        public StringResource this[string locale, string name]
        {
            get { return this[string.Format("{0}_{1}", locale,name)]; }
        }

        public void Add(StringResource resource)
        {
            BaseAdd(string.Format("{0}_{1}", resource.LocaleSetting, resource.Name), resource);
        }

        public override IEnumerator GetEnumerator()
        {
            for (int ctr = 0; ctr < this.Count; ctr++)
            {
                yield return BaseGet(ctr);
            }
        }

        public StringResources Merge(StringResources with)
        {
            foreach (StringResource resource in with)
            {
                this.Add(resource);
            }

            return this;
        }

        public void Clear()
        {
            BaseClear();
        }

        public void SaveAll()
        {
            if (this.Count > 0)
            {
                SqlParameter paramStringResourceGuid = new SqlParameter("@StringResourceGuid", SqlDbType.UniqueIdentifier);
                SqlParameter paramWebsiteCode = new SqlParameter("@WebsiteCode", SqlDbType.NVarChar, 30);
                SqlParameter paramName = new SqlParameter("@Name", SqlDbType.NVarChar, 100);
                SqlParameter paramLocaleSetting = new SqlParameter("@LocaleSetting", SqlDbType.NVarChar, 10);
                SqlParameter paramConfigValue = new SqlParameter("@ConfigValue", SqlDbType.NVarChar, 25000);
                SqlParameter paramCreatedOn = new SqlParameter("@CreatedOn", SqlDbType.SmallDateTime);
                SqlParameter paramModified = new SqlParameter("@Modified", SqlDbType.Bit);
                SqlParameter paramUserCreated = new SqlParameter("@UserCreated", SqlDbType.NVarChar, 30);
                SqlParameter paramDateCreated = new SqlParameter("@DateCreated", SqlDbType.SmallDateTime);
                SqlParameter paramUserModified = new SqlParameter("@UserModified", SqlDbType.NVarChar, 30);
                SqlParameter paramDateModified = new SqlParameter("@DateModified", SqlDbType.SmallDateTime);
                SqlParameter paramId = new SqlParameter("@StringResourceID", SqlDbType.Int);
                paramId.Direction = ParameterDirection.Output;

                using(SqlConnection con = new SqlConnection(DB.GetDBConn()))
                {
                    con.Open();

                    using (SqlCommand cmdSave = new SqlCommand("EcommerceSaveStringResource", con))
                    {
                        cmdSave.CommandType = CommandType.StoredProcedure;

                        cmdSave.Parameters.Add(paramStringResourceGuid);
                        cmdSave.Parameters.Add(paramWebsiteCode);
                        cmdSave.Parameters.Add(paramName);
                        cmdSave.Parameters.Add(paramLocaleSetting);
                        cmdSave.Parameters.Add(paramConfigValue);
                        cmdSave.Parameters.Add(paramCreatedOn);
                        cmdSave.Parameters.Add(paramModified);
                        cmdSave.Parameters.Add(paramUserCreated);
                        cmdSave.Parameters.Add(paramDateCreated);
                        cmdSave.Parameters.Add(paramUserModified);
                        cmdSave.Parameters.Add(paramDateModified);
                        cmdSave.Parameters.Add(paramId);

                        foreach(StringResource resource in this)
                        {
                            resource.Save(cmdSave);
                        }
                    }
                }
            }
        }
    }
}