// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Web;
using System.Configuration;
using System.Web.SessionState;
using System.Web.Caching;
using System.Web.Util;
using System.Data;
using System.Collections;
using System.IO;
using System.Text;
using System.Xml;
using System.Text.RegularExpressions;
using System.Globalization;


namespace InterpriseSuiteEcommerceCommon
{
    /// <summary>
    /// Summary description for SiteMapComponentArt.
    /// </summary>
    public class SiteMapComponentArt
    {
        private String m_Contents;

        public String Contents
        {
            get
            {
                return m_Contents;
            }
        }

        public SiteMapComponentArt(System.Collections.Generic.Dictionary<string, EntityHelper> EntityHelpers, int SkinID, Customer ThisCustomer)
        {
            bool FromCache = false;
            String CacheName = String.Format("SiteMapComponentArt_{0}_{1}", SkinID.ToString(), ThisCustomer.LocaleSetting);
            if (AppLogic.CachingOn)
            {
                m_Contents = (String)HttpContext.Current.Cache.Get(CacheName);
                if (m_Contents != null)
                {
                    FromCache = true;
                }
            }

            if (!FromCache)
            {
                StringBuilder tmpS = new StringBuilder(50000);
                tmpS.Append("<SiteMap>\n");

                if (AppLogic.AppConfigBool("SiteMap.ShowCategories"))
                {
                    // Categories:
                    String s = AppLogic.LookupHelper(EntityHelpers, "Category").GetEntityComponentArtNode(String.Empty, ThisCustomer.LocaleSetting, ThisCustomer.AffiliateID, string.Empty, true, AppLogic.AppConfigBool("SiteMap.ShowProducts") && AppLogic.NumProductsInDB < 250);
                    if (s.Length != 0)
                    {
                        tmpS.Append("<node Text=\"" + AppLogic.GetString("AppConfig.CategoryPromptPlural", true).ToUpper() + "\">\n");
                        tmpS.Append(s);
                        tmpS.Append("</node>");
                    }
                }

                if (AppLogic.AppConfigBool("SiteMap.ShowDepartments"))
                {
                    String s = AppLogic.LookupHelper(EntityHelpers, "Department").GetEntityComponentArtNode(String.Empty, ThisCustomer.LocaleSetting, ThisCustomer.AffiliateID, string.Empty, true, AppLogic.AppConfigBool("SiteMap.ShowProducts") && AppLogic.NumProductsInDB < 250);
                    if (s.Length != 0)
                    {
                        tmpS.Append("<node Text=\"" + AppLogic.GetString("AppConfig.DepartmentPromptPlural", true).ToUpper() + "\">\n");
                        tmpS.Append(s);
                        tmpS.Append("</node>");
                    }
                }

                if (AppLogic.AppConfigBool("SiteMap.ShowLibraries"))
                {
                    // Libraries:
                    String s = AppLogic.LookupHelper(EntityHelpers, "Library").GetEntityComponentArtNode(String.Empty, ThisCustomer.LocaleSetting, ThisCustomer.AffiliateID, string.Empty, true, AppLogic.AppConfigBool("SiteMap.ShowDocuments") && AppLogic.NumProductsInDB < 250);
                    if (s.Length != 0)
                    {
                        tmpS.Append("<node Text=\"" + AppLogic.GetString("AppConfig.LibraryPromptPlural", true).ToUpper() + "\">\n");
                        tmpS.Append(s);
                        tmpS.Append("</node>");
                    }
                }

                if (AppLogic.AppConfigBool("SiteMap.ShowManufacturers"))
                {
                    // Manufacturers:
                    String s = AppLogic.LookupHelper(EntityHelpers, "Manufacturer").GetEntityComponentArtNode(String.Empty, ThisCustomer.LocaleSetting, ThisCustomer.AffiliateID, string.Empty, true, AppLogic.AppConfigBool("SiteMap.ShowProducts") && AppLogic.NumProductsInDB < 250);
                    if (s.Length != 0)
                    {   
                        tmpS.Append("<node Text=\"" + AppLogic.GetString("AppConfig.ManufacturerPromptPlural", true).ToUpper() + "\">\n");
                        tmpS.Append(s);
                        tmpS.Append("</node>");
                    }
                }

                if (AppLogic.AppConfigBool("SiteMap.ShowCustomerService"))
                {
                    tmpS.Append("<node Text=\"CUSTOMER SERVICE\">\n");
                    tmpS.Append("	<node Text=\"" + XmlCommon.XmlEncodeAttribute(AppLogic.GetString("menu.YourAccount", true)) + "\" NavigateUrl=\"account.aspx\" />\n");
                    tmpS.Append("	<node Text=\"" + XmlCommon.XmlEncodeAttribute(AppLogic.GetString("menu.OrderHistory", true)) + "\" NavigateUrl=\"account.aspx\" />\n");
                    tmpS.Append("	<node Text=\"" + XmlCommon.XmlEncodeAttribute(AppLogic.GetString("menu.FAQs", true)) + "\" NavigateUrl=\"t-faq.aspx\" />\n");
                    tmpS.Append("	<node Text=\"" + XmlCommon.XmlEncodeAttribute(AppLogic.GetString("menu.PolicyReturns", true)) + "\" NavigateUrl=\"t-returns.aspx\" />\n");
                    tmpS.Append("	<node Text=\"" + XmlCommon.XmlEncodeAttribute(AppLogic.GetString("menu.Shipping", true)) + "\" NavigateUrl=\"t-shipping.aspx\" />\n");
                    tmpS.Append("	<node Text=\"" + XmlCommon.XmlEncodeAttribute(AppLogic.GetString("menu.Contact", true)) + "\" NavigateUrl=\"contactus.aspx\" />\n");
                    tmpS.Append("	<node Text=\"" + XmlCommon.XmlEncodeAttribute(AppLogic.GetString("menu.PolicyPrivacy", true)) + "\" NavigateUrl=\"t-privacy.aspx\" />\n");
                    tmpS.Append("	<node Text=\"" + XmlCommon.XmlEncodeAttribute(AppLogic.GetString("menu.PolicySecurity", true)) + "\" NavigateUrl=\"t-security.aspx\" />\n");
                    tmpS.Append("</node>\n");
                }

                if (AppLogic.AppConfigBool("SiteMap.ShowTopics"))
                {
                    // Topics:
                    tmpS.Append("<node Text=\"" + AppLogic.GetString("sitemap.aspx.2", true).ToUpperInvariant() + "\" NavigateUrl=\"\">\n");
                    
                    DataSet ds = DB.GetDS("SELECT * FROM EcommerceWebTopicView with (NOLOCK) " +
                        " WHERE ShowInSiteMap = 1 AND (SkinID IS NULL OR SkinID  = 0 OR SkinID=" + SkinID.ToString() +
                        " ) AND LocaleSetting=" + DB.SQuote(ThisCustomer.LocaleSetting) + " AND WebSiteCode=" + DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode),
                         AppLogic.CachingOn, System.DateTime.Now.AddMinutes(AppLogic.CacheDurationMinutes()));

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        String URL = String.Empty;
                        URL = SE.MakeDriverLink(Convert.ToString(DB.RowFieldByLocale(row, "Name", ThisCustomer.LocaleSetting)).Replace(" ","-"));
                        tmpS.Append("<node Text=\"" + XmlCommon.XmlEncodeAttribute(DB.RowFieldByLocale(row, "Title", ThisCustomer.LocaleSetting)) + "\" NavigateUrl=\"" + XmlCommon.XmlEncodeAttribute(URL) + "\" />\n");
                    }
                    ds.Dispose();

                    // File Topics:
                    // create an array to hold the list of files
                    ArrayList fArray = new ArrayList();

                    // get information about our initial directory
                    String SFP = CommonLogic.SafeMapPath("skins/skin_" + SkinID.ToString() + "/template.htm").Replace("template.htm", "");

                    DirectoryInfo dirInfo = new DirectoryInfo(SFP);

                    // retrieve array of files & subdirectories
                    FileSystemInfo[] myDir = dirInfo.GetFileSystemInfos();

                    for (int i = 0; i < myDir.Length; i++)
                    {
                        // check the file attributes

                        // if a subdirectory, add it to the sArray    
                        // otherwise, add it to the fArray
                        if (((Convert.ToUInt32(myDir[i].Attributes) & Convert.ToUInt32(FileAttributes.Directory)) > 0))
                        {
                        }
                        else
                        {
                            bool skipit = false;
                            if (!myDir[i].FullName.EndsWith("HTM", StringComparison.InvariantCultureIgnoreCase) || 
                                (myDir[i].FullName.IndexOf("TEMPLATE", StringComparison.InvariantCultureIgnoreCase) != -1) ||
                                (myDir[i].FullName.IndexOf("AFFILIATE_", StringComparison.InvariantCultureIgnoreCase) != -1) ||
                                (myDir[i].FullName.IndexOf(AppLogic.ro_PMMicropay, StringComparison.InvariantCultureIgnoreCase) != -1))
                            {
                                skipit = true;
                            }
                            if (!skipit)
                            {
                                fArray.Add(Path.GetFileName(myDir[i].FullName));
                            }
                        }
                    }

                    if (fArray.Count != 0)
                    {
                        // sort the files alphabetically
                        fArray.Sort(0, fArray.Count, null);
                        for (int i = 0; i < fArray.Count; i++)
                        {
                            String URL = String.Empty;
                            URL = SE.MakeDriverLink(fArray[i].ToString().Replace(".htm", ""));
                            tmpS.Append("<node Text=\"" + XmlCommon.XmlEncodeAttribute(CommonLogic.Capitalize(fArray[i].ToString().Replace(".htm", ""))) + "\" " + CommonLogic.IIF(URL.Length != 0, "NavigateUrl=\"" + XmlCommon.XmlEncodeAttribute(URL) + "\"", "") + "/>\n");
                        }
                    }
                    tmpS.Append("</node>");
                }

                tmpS.Append("</SiteMap>\n");
                m_Contents = tmpS.ToString();
                if (AppLogic.CachingOn)
                {
                    HttpContext.Current.Cache.Insert(CacheName, m_Contents, null, System.DateTime.Now.AddMinutes(AppLogic.CacheDurationMinutes()), TimeSpan.Zero);
                }
            }

        }
    }
}



