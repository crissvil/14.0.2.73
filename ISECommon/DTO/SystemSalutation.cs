﻿namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class SystemSalutation
    {
        public string SalutationDescription { get; set; }
    }
}
