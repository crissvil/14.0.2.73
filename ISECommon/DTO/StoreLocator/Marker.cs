﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    [Serializable]
    public class Marker
    {
        public string Description { get; set; }
        public Coordinate Coordinate { get; set; }
        public AddressDTO AddressInfo { get; set; }
    }
}
