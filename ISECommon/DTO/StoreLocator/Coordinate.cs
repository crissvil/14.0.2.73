﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    [Serializable, DataContract]
    public class Coordinate
    {
        private decimal _latitude;
        private decimal _longtitude;

        [DataMember]
        public decimal Latitude 
        {
            get {
                return _latitude;
            }
            set {
                _latitude = value;
            }
        }

        [DataMember]
        public decimal Longtitude
        {
            get
            {
                return _longtitude;
            }
            set
            {
                _longtitude = value;
            }
        }
    }
}
