﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public enum LocatorMapType
    {
        ROADMAP,
        SATELLITE,
        HYBRID,
        TERRAIN
    }

    public enum StoreType
    {
        All = 0,
        Warehouse = 1,
        Dealer = 2
    }

    [Serializable]
    public class LocatorOption
    {
        public int Zoom { get; set; }
        public LocatorMapType MapType { get; set; }
        public decimal MyProperty { get; set; }
        public Coordinate Center { get; set; }
    }
}