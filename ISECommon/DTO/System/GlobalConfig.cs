﻿using System.Runtime.Serialization;
namespace InterpriseSuiteEcommerceCommon.DTO
{
    [DataContract]
    public class GlobalConfig
    {
        public GlobalConfig() { }

        public GlobalConfig(string key, string value)
        {
            Key = key;
            Value = value;
        }

        [DataMember]
        public string Key { get; set; }

        [DataMember]
        public string Value { get; set; }
    }
}
