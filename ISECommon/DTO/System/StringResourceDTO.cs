﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    [DataContract]
    public class StringResourceDTO : GlobalConfig
    {
        public StringResourceDTO(string key, string value)
        {
            Key = key;
            Value = value;
        }
    }
}
