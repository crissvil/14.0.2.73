// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class KitGroupDataCollection : Dictionary<int, KitGroupData>
    {
        public KitGroupDataCollection() { }

        public void Add(KitGroupData group)
        {
            Add(group.Id, group);
        }

        public bool Contains(int id)
        {
            return this.ContainsKey(id);
        }

        new public IEnumerator<KitGroupData> GetEnumerator()
        {
            return this.Values.GetEnumerator();
        }

    }
}
