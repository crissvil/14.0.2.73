﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class ImageNameData
    {
        public ImageNameData() {
            IconList = new List<ImageNameDataItem>();
            MediumList = new List<ImageNameDataItem>();
            LargeList = new List<ImageNameDataItem>();
            MultiImageList = new List<ImageNameDataItem>();
        }

        public List<ImageNameDataItem> IconList { get; set; }
        public List<ImageNameDataItem> MediumList { get; set; }
        public List<ImageNameDataItem> LargeList { get; set; }
        public List<ImageNameDataItem> MultiImageList { get; set; }

    }

    public class ImageNameDataItem
    {
        public string Filename { get; set; }
        public string ItemCode { get; set; }
    }

}
