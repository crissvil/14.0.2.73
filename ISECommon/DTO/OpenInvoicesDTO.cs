﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class OpenInvoicesDTO
    {
        public int Row { get; set; }
        public string InvoiceCode { get; set; }
        public string InvoiceDate { get; set; }
        public string InvoiceDueDate { get; set; }
        public string DueTotal { get; set; }
        public string Payments { get; set; }
        public string Balance { get; set; }
        public string Status { get; set; }
    }
}
