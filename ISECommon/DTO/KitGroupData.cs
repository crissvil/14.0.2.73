// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class KitGroupData
    {
        public int Id;
        public string Code;
        public string Description;
        public string Type;
        public string ControlType;
        public string CurrencySymbol;
        public List<KitItem> Items = new List<KitItem>();
    }
}
