﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class ShippingCalculationSummary
    {
        public string Freight { get; set; }
        public string Tax { get; set; }
        public string SubTotal { get; set; }
        public string Discount { get; set; }
        public string DueTotal { get; set; }
        public string Balance { get; set; }
    }
}
