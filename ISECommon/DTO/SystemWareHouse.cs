﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    [Serializable, DataContract]
    public class SystemWareHouse
    {
        [DataMember]
        public string Location { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public AddressDTO Address { get; set; }

        [DataMember]
        public Coordinate Coordinate { get; set; }

        [DataMember]
        public double Distance { get; set; }
    }
}
