﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class ItemAttribute
    {
        public string ItemCode { get; set; }
        public string Alt { get; set; }
        public string Title { get; set; }
    }
}
