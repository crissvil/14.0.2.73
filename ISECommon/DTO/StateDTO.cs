// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class StateDTO
    {
        public StateDTO() { }

        public StateDTO(string code, string description)
        {
            this.code = code;
            this.description = description;
        }

        public string code;
        public string description;
    }
}
