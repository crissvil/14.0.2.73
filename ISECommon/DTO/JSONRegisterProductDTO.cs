﻿using System;
using System.Collections.Generic;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class JSONRegisterProductDTO
    {
        public string ItemCounter { get; set; }
        public string ItemCode { get; set; }
        public string ItemType { get; set; }

        public IEnumerable<Decimal> RestrictedQuantities { get; set; }

        public bool RequiresRegistration { get; set; }
        public bool ShowBuyButton { get; set; }
        public bool IsCallToOrder { get; set; }
        public bool HidePriceUntilCart { get; set; }
        public decimal MinimumOrderQuantity { get; set; }

        public ProductImageData ImageData { get; set; }

        public IEnumerable<ProductPricePerUnitMeasure> UnitMeasureIntrinsic { get; set; }

        public bool VatEnabled { get; set; }
        public int VatSetting { get; set; }
        public string QuantityRegex { get; set; }
    }
}
