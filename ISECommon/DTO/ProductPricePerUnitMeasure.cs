// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using InterpriseSuiteEcommerceCommon.Extensions;
using System;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    [DataContractAttribute]
    public class ProductPricePerUnitMeasure
    {
        public ProductPricePerUnitMeasure(string unitMeasureCode)
        {
            this.code = unitMeasureCode;
        }

        [DataMemberAttribute]
        public string code;

        [DataMemberAttribute]
        public string description;

        [DataMemberAttribute]
        public decimal price;

        [DataMemberAttribute]
        public string priceFormatted;

        [DataMemberAttribute]
        public bool hasPromotionalPrice;

        [DataMemberAttribute]
        public decimal promotionalPrice;

        [DataMemberAttribute]
        public string promotionalPriceFormatted;

        [DataMemberAttribute]
        public decimal unitMeasureQuantity;

        [DataMemberAttribute]
        public decimal freeStock = decimal.Zero;

        [DataMemberAttribute]
        public decimal originalSalePrice = decimal.Zero;

        [DataMemberAttribute]
        public string originalSalePriceFormatted;

        [DataMemberAttribute]
        public bool hasDiscount;

        [DataMemberAttribute]
        public int numberOfDigitsAfterDecimal = 0;

        [DataMemberAttribute]
        public string upcCode = String.Empty;

        public static List<ProductPricePerUnitMeasure> GetAll(string itemCode, Customer forCustomer)
        {
            return GetAll(itemCode, forCustomer, false);
        }

        public static List<ProductPricePerUnitMeasure> GetAll(string itemCode, Customer forCustomer, bool hideUntilCart)
        {
            return GetAll(itemCode, forCustomer, hideUntilCart, false);
        }

        public static List<ProductPricePerUnitMeasure> GetAll(string itemCode, Customer forCustomer, bool hideUntilCart, bool showFreeStock, bool includePrice = true )
        {
            string customerCode = forCustomer.CustomerCode;
            if (forCustomer.IsNotRegistered)
            {
                customerCode = forCustomer.AnonymousCustomerCode;
            }

            var availableUnitMeasures = new List<ProductPricePerUnitMeasure>();
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "exec eCommerceGetProductUnitMeasureAvailability @CustomerCode = {0}, @ItemCode = {1}, @IncludeAllWarehouses = {2}, @Anon = {3}",
                                                        customerCode.ToDbQuote(), itemCode.ToDbQuote(), AppLogic.AppConfigBool("ShowInventoryFromAllWarehouses"), "false"))
                {
                    while (reader.Read())
                    {
                        var pricePerUnitMeasure = new ProductPricePerUnitMeasure(DB.RSField(reader, "UnitMeasureCode"))
                        {
                            freeStock = (showFreeStock) ? DB.RSFieldDecimal(reader, "FreeStock") : Decimal.Zero,
                            description = DB.RSField(reader, "UnitMeasureDescription"),
                            unitMeasureQuantity = DB.RSFieldDecimal(reader, "UnitMeasureQty"),
                            upcCode = DB.RSField(reader, "UPCCode")
                        };
                        availableUnitMeasures.Add(pricePerUnitMeasure);
                    }
                }
            }

            if (!includePrice) { return availableUnitMeasures; }

            var lstUnitMeasureInfo =  ServiceFactory.GetInstance<IProductService>()
                                                    .GetItemUnitMeassures(itemCode, availableUnitMeasures.Select(m => m.code))
                                                    .Select(u => new UnitMeasureInfo() 
                                                    {
                                                        Code = u.Code,
                                                        Quantity = u.Quantity
                                                    });
            decimal vat = 0;
            foreach (var unitMeasurePerProduct in availableUnitMeasures)
            {
                decimal salesPrice = Decimal.Zero;
                decimal promotionalPrice = Decimal.Zero;
                bool withVat = AppLogic.AppConfigBool("VAT.Enabled") && forCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive;

                decimal originalSalePrice = decimal.Zero;
                string originalSalePriceFormatted = String.Empty;
                bool hasDiscount = false;

                var item = lstUnitMeasureInfo.FirstOrDefault(c => c.Code == unitMeasurePerProduct.code);

                //check if has discount
                salesPrice = InterpriseHelper.GetSalesPriceAndTax(forCustomer.CustomerCode, itemCode, forCustomer.CurrencyCode, decimal.One, unitMeasurePerProduct.code, withVat, 
                                                                   ref promotionalPrice, ref vat, item, null, ref originalSalePrice, ref hasDiscount);

                string salesPriceFormatted = salesPrice.ToCustomerCurrency();
                string promotionalPriceFormatted = promotionalPrice.ToCustomerCurrency();
                // added additional logic to check if the promotional price is greater than the sales price. if it is then don't display the promotional price
                unitMeasurePerProduct.hasPromotionalPrice = promotionalPrice > Decimal.Zero && promotionalPrice < salesPrice;

                if (hideUntilCart ||
                    (!AppLogic.AppConfigBool("UseWebStorePricing") &&
                        AppLogic.AppConfigBool("WholesaleOnlySite") &&
                        forCustomer.DefaultPrice != Interprise.Framework.Base.Shared.Const.BUSINESS_TYPE_WHOLESALE))
                {
                    salesPrice = Decimal.Zero;
                    salesPriceFormatted = String.Empty;
                    promotionalPrice = Decimal.Zero;
                    promotionalPriceFormatted = String.Empty;
                    originalSalePrice = decimal.Zero;
                    hasDiscount = false;
                    originalSalePriceFormatted = String.Empty;
                }

                unitMeasurePerProduct.price = salesPrice;
                unitMeasurePerProduct.priceFormatted = salesPriceFormatted;

                unitMeasurePerProduct.promotionalPrice = promotionalPrice;
                unitMeasurePerProduct.promotionalPriceFormatted = promotionalPriceFormatted;

                // check if has discount
                unitMeasurePerProduct.hasDiscount = hasDiscount;
                unitMeasurePerProduct.originalSalePrice = originalSalePrice;
                originalSalePriceFormatted = originalSalePrice.ToCustomerCurrency();
                unitMeasurePerProduct.originalSalePriceFormatted = originalSalePriceFormatted;

                var currencyFormat = Currency.GetCurrencyFormat(Customer.Current.CurrencyCode);
                unitMeasurePerProduct.numberOfDigitsAfterDecimal = currencyFormat.CurrencyDecimalDigits;
            }

            return availableUnitMeasures;
        }
    }
}



