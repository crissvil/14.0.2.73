// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class MatrixAttributeValue
    {
        public MatrixAttributeValue(string value, string description)
        {
            this.AttributeValue = value;
            this.AttributeValueDescription = description;
        }

        public string AttributeValue;
        public string AttributeValueDescription;
    }
}
