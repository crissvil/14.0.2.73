﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class EcommerceCartRecordPerQuantity
    {
        public int CartRecId { get; set; }
        public decimal Total { get; set; }
    }
}
