﻿using System;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class ActiveShopper
    {
        public int Counter { get; set; }
        public string ActiveShopperId { get; set; }
        public string RequestCode { get; set; }
        public string CustomerCode { get; set; }
        public Guid ContactGuid { get; set; }
        public string WebsiteCode { get; set; }
        public DateTime TimeStarted { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
