﻿namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CustomFileDTO
    {
        private string _fileName;

        public string FullPath { get; set; }
        public string FileName 
        {
            get 
            {
                return _fileName;
            }
            set 
            {
                _fileName = value;
                FileNameWithoutExt = value.Substring(0, value.Length - value.Substring(value.LastIndexOf(".")).Length);
            } 
        }
        public string FileNameWithoutExt { get; set; }
        public string Ext { get; set; }
        public ImageType ImageType { get; set; }
        public int Index { get; set; }
        public string OtherPath { get; set; }

    }
}