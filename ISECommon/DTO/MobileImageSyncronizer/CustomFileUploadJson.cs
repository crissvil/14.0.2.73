﻿namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CustomFileUploadJson
    {
        public int TotalImages { get; set; }
        public int CurrentImageRow { get; set; }
    }
}
