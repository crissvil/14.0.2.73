﻿using System.Collections.Generic;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CustomUnionComparer : IEqualityComparer<CustomFileDTO>
    {
        public bool Equals(CustomFileDTO x, CustomFileDTO y)
        {
            return x.FileName.Equals(y.FileName);
        }

        public int GetHashCode(CustomFileDTO obj)
        {
            return obj.FileName.GetHashCode();
        }
    }
}
