﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class KitDetailItem
    {
        public string Name { get; set; } 
        public Decimal Quantity { get; set; } 
    }
}
