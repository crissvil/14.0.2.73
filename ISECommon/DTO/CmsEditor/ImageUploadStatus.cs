﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    [Serializable, DataContract]
    public class ImageUploadStatus
    {
        public string ResponseText { get; set; }
        public string UpdatedImageSrc { get; set; }
    }
}
