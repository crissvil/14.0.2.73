﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class GoogleAnalytics
    {
        public string Dimension { get; set; }
        public int Visits { get; set; }
    }
}
