﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class InventoryPreference
    {
        public bool IsAllowFractional { get; set; }
        public int InventoryDecimalPlacesPreference { get; set; }
    }
}
