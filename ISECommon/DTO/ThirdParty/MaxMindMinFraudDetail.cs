﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class MaxMindMinFraudDetail
    {
        public string FraudDetails { get; set; }
        public decimal RiskScore { get; set; }
    }
}
