﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class EntityModel
    {
        public string EntityName { get; set; }
        public string Counter { get; set; }
        public string SEName { get; set; }
        public string NameLocale { get; set; }
    }
}
