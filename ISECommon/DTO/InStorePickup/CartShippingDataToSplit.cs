﻿using System;
namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CartShippingDataToSplit
    {
        public string ShippingMethod { get; set; }
        public int CartId { get; set; }
        public string WarehouseCode { get; set; }
        public Guid RealTimeRateGuid { get; set; }
        public string Freight { get; set; }
        public bool IsRealTime { get; set; }
    }
}