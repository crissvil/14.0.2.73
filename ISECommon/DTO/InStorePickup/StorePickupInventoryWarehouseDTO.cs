﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class StorePickupInventoryWarehousetTransform
    {
        public long RowNumber { get; set; }
        public string WareHouseCode { get; set; }
        public string WareHouseDescription { get; set; }
        public decimal FreeStock { get; set; }
        public decimal Distance { get; set; }
        public Coordinate Coordinate { get; set; }
        public LiteAddressInfo Address { get; set; }
        public string Telephone { get; set; }
        public string TelephoneLocalNumber { get; set; }
        public string TelephoneExtension { get; set; }
        public string Fax { get; set; }
        public string FaxLocalNumber { get; set; }
        public string FaxExtension { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
    }
}
