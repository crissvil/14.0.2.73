﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Model;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class StorePickupAvailabilityInfo
    {
        public string ItemCode { get; set; }
        public LiteAddressInfo Address { get; set; }
        public LiteProductInfo ProductInfo { get; set; }
        public string JsonCountries { get; set; }
    }

}
