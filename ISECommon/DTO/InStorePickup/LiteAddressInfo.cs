﻿using System.Runtime.Serialization;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class LiteAddressInfo
    {
        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string PostalCode { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string Telephone { get; set; }
    }

    public class LiteAddressInfoMatch
    {

        [DataMember]
        public string Billing { get; set; }

        [DataMember]
        public string Shipping { get; set; }

        [DataMember]
        public int AddressMatchResultLimit { get; set; }
    }
}
