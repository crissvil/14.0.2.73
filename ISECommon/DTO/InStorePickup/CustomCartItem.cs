﻿using System.Collections.Generic;
namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CustomCartItem
    {
        public int GroupID { get; set; }
        public string ShippinAddressID { get; set; }
        public string ItemDescription { get; set; }
        public decimal Quantity { get; set; }
        public string ItemCode { get; set; }
        public string UnitMeassureCode { get; set; }
        public bool IsService { get; set; }
        public bool IsDownload { get; set; }
        public int Counter { get; set; }
        public string InStoreWarehouseCode { get; set; }
        public string ShippingMethod { get; set; }
        public KitComposition KitComposition { get; set; }
    }
}
