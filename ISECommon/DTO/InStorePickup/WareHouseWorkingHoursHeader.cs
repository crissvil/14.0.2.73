﻿using System;
using System.Collections.Generic;
namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class WareHouseWorkingHoursHeader
    {
        public WareHouseWorkingHoursHeader() 
        {
            WorkingHours = new List<StoreWorkingHoursDTO>();
            Holidays = new List<StoreHolidayHourDTO>();
        }

        public IEnumerable<StoreWorkingHoursDTO> WorkingHours { get; set; }
        public IEnumerable<StoreHolidayHourDTO> Holidays { get; set; }
    }

    public class StoreWorkingHoursDTO
    {
        public string Day { get; set; }
        public WorkingHourDTO WorkingHour { get; set; }
    }

    public class StoreHolidayHourDTO
    {
        public string Name { get; set; }
        public string Date { get; set; }
        public WorkingHourDTO WorkingDay { get; set; }
    }

    public class WorkingHourDTO
    {
        public string Opening { get; set; }
        public string Closing { get; set; }
        public bool Close { get; set; }
    }
}
