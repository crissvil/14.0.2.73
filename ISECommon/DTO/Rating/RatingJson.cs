﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    [DataContract]
    [Serializable]
    public class RatingJson
    {
        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Salutation { get; set; }

        [DataMember]
        public int Rating { get; set; }

        [DataMember]
        public string CreatedOn { get; set;
 }
        [DataMember]
        public string Comment { get; set; }

        [DataMember]
        public int FoundHelpful { get; set; }

        [DataMember]
        public int NotHelpful { get; set; }

        [DataMember]
        public string CustomerId { get; set; }

        [DataMember]
        public string ContactId { get; set; }

        [DataMember]
        public bool HasRate { get; set; }

        [DataMember]
        public int Row { get; set; }
    }
}
