﻿using System.Collections.Generic;
namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class RatingHeader
    {
        public bool HasRating { get; set; }
        public int Rate { get; set; }
        public bool IsRegistered { get; set; }
        public IEnumerable<Rating> Ratings { get; set; }
        public decimal Average { get; set; }
        public int Total { get; set; }
        public int Count { get; set; }
        public decimal Great { get; set; }
        public decimal Good { get; set; }
        public decimal OK { get; set; }
        public decimal BAD { get; set; }
        public decimal TERRIBLE { get; set; }
        public string ContactCode { get; set; }

        public int RatingPageSize { get; set; }
    }
}