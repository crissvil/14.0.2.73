﻿namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class LoginStatus
    {
        public LoginStatus() { }
        public LoginStatus(bool isValid, bool isAccountExpired) 
        {
            IsValid = isValid;
            IsAccountExpired = isAccountExpired;
        }

        public bool IsValid { get; set; }
        public bool IsAccountExpired { get; set; }
    }
}
