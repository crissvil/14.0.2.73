// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using InterpriseSuiteEcommerceCommon.DataAccess;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class PaymentTermDTO
    {
        private string _paymentTermCode;
        private string _paymentMethod;
        private string _description;
        private bool _isActive;
        private bool _isSelected;
        private int _counter;

        public EventHandler SelectedChanged;

        public PaymentTermDTO(string code, string method, string description, string type, bool active, int counter)
        {
            _paymentTermCode = code;
            _paymentMethod = method;
            if (type == "Cash/Other")
            {
                _description = description + "<div class=\"payment-option-cash-desc\">Goods are not dispatched until payment received.</div>";
            }
            else
            {
                _description = description;
            }
            _isActive = active;
            _counter = counter;
        }

        public string PaymentTermCode
        {
            get { return _paymentTermCode; }
        }

        public string PaymentMethod
        {
            get { return _paymentMethod; }
        }

        public string Description
        {
            get { return _description; }
        }

        public bool IsActive
        {
            get { return _isActive; }
        }

        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                OnSelectedChanged();
            }
        }

        public int Counter
        {
            get { return _counter; }
        }

        private void OnSelectedChanged()
        {
            if (null != this.SelectedChanged)
            {
                this.SelectedChanged(this, EventArgs.Empty);
            }
        }

        public static PaymentTermDTO Find(string withCode)
        {
            PaymentTermDTO term = null;

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "exec eCommerceGetPaymentTerm @PaymentTermCode = {0}", DB.SQuote(withCode)))
                {
                    int ctr = 1;
                    if (reader.Read())
                    {
                        term = new PaymentTermDTO(reader.ToRSField("PaymentTermCode"),
                                reader.ToRSField("PaymentMethodCode"),
                                reader.ToRSField("PaymentTermDescription"),
                                reader.ToRSField("PaymentType"),
                                true,
                                ctr++);

                        term.IsSelected = true; //not needed here anyway...
                    }
                }
            }

            return term;
        }

        public static IEnumerable<PaymentTermDTO> GetAllForGroup(string ofContact, Address preferredShippingAddress)
        {
            return CustomerDA.GetAllForGroup(ofContact, preferredShippingAddress);
        }

        public static IEnumerable<PaymentTermDTO> GetOnlinePaymentTerms(string contactCode, Address preferredShippingAddress)
        {
            return CustomerDA.GetOnlinePaymentTerms(contactCode, preferredShippingAddress);
        }

        public class PaymentTermGroup : List<PaymentTermDTO>
        {
            public void AddTerm(PaymentTermDTO term)
            {
                term.SelectedChanged += PaymentTerm_SelectedChanged;
                if (term.PaymentMethod == DomainConstants.PAYMENT_METHOD_CREDITCARD &&
                    !term.PaymentTermCode.Equals("PURCHASE ORDER", StringComparison.InvariantCultureIgnoreCase) && 
                    !term.PaymentTermCode.Equals("REQUEST QUOTE", StringComparison.InvariantCultureIgnoreCase) &&
                    term.PaymentTermCode.ToUpperInvariant() != ServiceFactory.GetInstance<IAppConfigService>().SagePayPaymentTerm.ToUpperInvariant())
                {
                    this.Insert(0, term);
                    term.IsSelected = true;
                }
                else
                {
                    this.Add(term);
                }
            }

            private void PaymentTerm_SelectedChanged(object sender, EventArgs e)
            {
                PaymentTermDTO term = sender as PaymentTermDTO;
                if (term.IsSelected)
                {
                    foreach (PaymentTermDTO otherTerm in this)
                    {
                        if (otherTerm != term)
                        {
                            otherTerm.IsSelected = false;
                        }
                    }
                }
            }

            public void EnsureHasSelected()
            { 
            }


            
        }

        public static IEnumerable<PaymentTermDTO> GetAvailablePaymentTerms(string contactCode, string country)
        {
            return CustomerDA.GetAvailablePaymentTerms(contactCode, country);
        }
    }
}






