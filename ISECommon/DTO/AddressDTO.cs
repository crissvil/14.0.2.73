// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.Serialization;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    [Serializable, DataContract]
    public class AddressDTO
    {
        public AddressDTO() { }

        [DataMember]
        public string id { get; set; }

        [DataMember]
        public string accountName { get; set; }

        [DataMember]
        public string firstName { get; set; }

        [DataMember]
        public string lastName { get; set; }

        [DataMember]
        public string address { get; set; }

        [DataMember]
        public string city { get; set; }

        [DataMember]
        public string state { get; set; }

        [DataMember]
        public string postalCode { get; set; }

        [DataMember]
        public string country { get; set; }

        [DataMember]
        public bool withState { get; set; }

        [DataMember]
        public string phone { get; set; }

        [DataMember]
        public string county { get; set; }

        [DataMember]
        public string email { get; set; }

        [DataMember]
        public ResidenceTypes residenceType = ResidenceTypes.Unknown;

        [DataMember]
        public string cardType { get; set; }

        [DataMember]
        public string nameOnCard { get; set; }

        [DataMember]
        public string cardExpMonth { get; set; }

        [DataMember]
        public string cardExpYear { get; set; }

        [DataMember]
        public string full { get; set; }

        [DataMember]
        public string cardNumber { get; set; }
    }

}
