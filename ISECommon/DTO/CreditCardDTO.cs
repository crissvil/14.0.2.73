﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CreditCardDTO
    {
        public CreditCardDTO()
        {
        }

        #region Variables
        private string _customerCode;
        private string _description;
        private string _cardNumber;
        private string _nameOnCard;
        private string _expMonth;
        private string _expYear;
        private string _cardType;
        private string _creditCardCode;
        private string _accountName;
        private string _country;
        private string _county;
        private string _state;
        private string _postal;
        private string _city;
        private string _address;
        private string _phoneNumber;
        private int _refNo;
        private string _startMonth;
        private string _startYear;
        private string _vault;
        #endregion

        #region Properties
        public string CustomerCode
        {
            get { return _customerCode; }
        }
        public string Description
        {
            get { return _description; }
        }
        public string CardNumber
        {
            get { return _cardNumber; }
        }
        public string NameOnCard
        {
            get { return _nameOnCard; }
        }
        public string CardType
        {
            get { return _cardType; }
        }
        public string ExpMonth
        {
            get { return _expMonth; }
        }
        public string ExpYear
        {
            get { return _expYear; }
        }
        public string CreditCardCode
        {
            get { return _creditCardCode; }
            set { _creditCardCode = value; }
        }
        public string AccountName
        {
            get { return _accountName; }
        }
        public string Country
        {
            get { return _country; }
        }
        public string State
        {
            get { return _state; }
        }
        public string Postal
        {
            get { return _postal; }
        }
        public string City
        {
            get { return _city; }
        }
        public string Address
        {
            get { return _address; }
        }
        public string PhoneNumber
        {
            get { return _phoneNumber; }
        }
        public int RefNo
        {
            get { return _refNo; }
        }
        public string StartMonth
        {
            get { return _startMonth; }
        }
        public string StartYear
        {
            get { return _startYear; }
        }

        public string County
        {
            get { return _county; }
        }
        public string Vault
        {
            get { return _vault; }
        }
        #endregion


        public CreditCardDTO(string customerCode, string description, string cardNumber, string nameOnCard, string expMonth, string expYear, string cardType, string creditCardCode, int refNo)
        {
            _customerCode = customerCode;
            _description = description;
            _cardNumber = cardNumber;
            _nameOnCard = nameOnCard;
            _expMonth = expMonth;
            _expYear = expYear;
            _cardType = cardType;
            _creditCardCode = creditCardCode;
            _refNo = refNo;
        }


        public CreditCardDTO(string customerCode, string description, string cardNumber, string nameOnCard, string expMonth, string expYear, string startMonth, string startYear, string cardType, string creditCardCode, string accountName, string country, string state, string postal, string city, string address, string phone, int refno)
        {
            _customerCode = customerCode;
            _description = description;
            _cardNumber = cardNumber;
            _nameOnCard = nameOnCard;
            _expMonth = expMonth;
            _expYear = expYear;
            _cardType = cardType;
            _creditCardCode = creditCardCode;

            _accountName = accountName;
            _country = country;
            _state = state;
            _postal = postal;
            _city = city;
            _address = address;
            _phoneNumber = phone;
            _refNo = refno;

            _startMonth = startMonth;
            _startYear = startYear;
        }

        public CreditCardDTO(string customerCode, string description, string cardNumber, string nameOnCard, string expMonth, string expYear, string startMonth, string startYear, string cardType, string creditCardCode, string accountName, string country, string state, string postal, string city, string address, string phone, int refno, string county, string vault)
        {
            _customerCode = customerCode;
            _description = description;
            _cardNumber = cardNumber;
            _nameOnCard = nameOnCard;
            _expMonth = expMonth;
            _expYear = expYear;
            _cardType = cardType;
            _creditCardCode = creditCardCode;

            _accountName = accountName;
            _country = country;
            _state = state;
            _postal = postal;
            _city = city;
            _address = address;
            _phoneNumber = phone;
            _refNo = refno;

            _startMonth = startMonth;
            _startYear = startYear;
            _county = county;
            _vault = vault;
        }

        public static CreditCardDTO Find(string withCode)
        {
            CreditCardDTO credit = null;

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "SELECT * FROM CustomerCreditCard with (NOLOCK) WHERE CreditCardCode= {0}", DB.SQuote(withCode)))
                {
                    if (reader.Read())
                    {
                        string postalCode = CommonLogic.IIF(DB.RSFieldInt(reader, "Plus4") > 0, String.Format("{0}-{1}", DB.RSField(reader, "PostalCode"), DB.RSFieldInt(reader, "Plus4").ToString("0000.##")), DB.RSField(reader, "PostalCode"));

                        credit = new CreditCardDTO(DB.RSField(reader, "CustomerCode"),
                                                   DB.RSField(reader, "CreditCardDescription"),
                                                   DB.RSField(reader, "MaskedCardNumber"),
                                                   DB.RSField(reader, "NameOnCard"),
                                                   InterpriseHelper.FromInterpriseExpMonth(DB.RSField(reader, "ExpMonth")),
                                                   DB.RSField(reader, "ExpYear"),
                                                   InterpriseHelper.FromInterpriseExpMonth(DB.RSField(reader, "StartMonth")),
                                                   DB.RSField(reader, "StartYear"),
                                                   DB.RSField(reader, "CreditCardType"),
                                                   DB.RSField(reader, "CreditCardCode"),
                                                   DB.RSField(reader, "NameOnCard"),
                                                   DB.RSField(reader, "Country"),
                                                   DB.RSField(reader, "State"),
                                                   postalCode,
                                                   DB.RSField(reader, "City"),
                                                   DB.RSField(reader, "Address"),
                                                   DB.RSField(reader, "Telephone"),
                                                   DB.RSFieldInt(reader, "InterpriseGatewayRefNo"),
                                                   DB.RSField(reader, "County"),
                                                   DB.RSField(reader, "Vault"));

                    }
                }
            }

            return credit;
        }

        public static List<CreditCardDTO> GetCreditCards(string custCode)
        {
            var creditCard = new List<CreditCardDTO>();
            using (var con = DB.NewSqlConnection())
            {
                string query = "SELECT distinct cc.* FROM CustomerCreditcard cc LEFT JOIN CRMContact c ON cc.creditcardcode = c.defaultbillingcode " +
                                String.Format("WHERE cc.Customercode = {0}", DB.SQuote(custCode)) + "ORDER BY cc.creditcardcode";

                con.Open();
                using (var reader = DB.GetRSFormat(con, query))
                {
                    while (reader.Read())
                    {
                        string customerCode = DB.RSField(reader, "CustomerCode");
                        string creditCardCode = DB.RSField(reader, "CreditCardCode");
                        string description = DB.RSField(reader, "CreditCardDescription");
                        string cardNumber = DB.RSField(reader, "MaskedCardNumber");
                        string nameOnCard = DB.RSField(reader, "NameOnCard");
                        string address = DB.RSField(reader, "Address");
                        string city = DB.RSField(reader, "City");
                        string state = DB.RSField(reader, "State");
                        string postalCode = DB.RSField(reader, "PostalCode");
                        string contry = DB.RSField(reader, "Country");
                        string expMonth = DB.RSField(reader, "ExpMonth");
                        string expYear = DB.RSField(reader, "ExpYear");
                        string telephone = DB.RSField(reader, "Telephone");
                        string creditCardType = DB.RSField(reader, "CreditCardType");
                        int refNo = DB.RSFieldInt(reader, "InterpriseGatewayRefNo");

                        if (!customerCode.IsNullOrEmptyTrimmed() && customerCode.Length > 0)
                        {
                            creditCard.Add(new CreditCardDTO(customerCode, description, cardNumber, nameOnCard, expMonth, expYear, creditCardType, creditCardCode, refNo));
                        }
                    }
                }
            }

            return creditCard;
        }
    }
}
