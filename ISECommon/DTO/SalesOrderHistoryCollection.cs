using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class SalesOrderHistoryCollection
    {
        public int rows { get; set; }
        public int current { get; set; }
        public int pages { get; set; }
        public int allPages { get; set; }
        public int start { get; set; }
        public int end { get; set; }
        public List<SalesOrderHistoryDTO> orders = new List<SalesOrderHistoryDTO>();

        public void Add(SalesOrderHistoryDTO order)
        {
            orders.Add(order);
        }
    }
}