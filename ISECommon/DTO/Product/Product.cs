﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class Product
    {
        public int Counter { get; set; }
        public int CBNItemID { get; set; }

        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ItemType { get; set; }
        public string ItemDescription { get; set; }

        public string ExtendedDescription { get; set; }
        public string WebDescription { get; set; }

        public ProductStockTotal StockTotal { get; set; }

        public string AccessoryName { get; set; }
        public string AccessoryCode { get; set; }

        public bool IsDropShip { get; set; }
        public bool IsSpecialOrder { get; set; }
        public bool IsCBN { get; set; }
    }
}
