﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class MatrixGroupItems
    {
        public int TotalNumberOfItems { get; set; }
        public int ItemCounter { get; set; }
        public int ItemStock { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public string ItemName { get; set; }
        public string ItemHTMLDescription { get; set; }
        public List<ProductPricePerUnitMeasure> ItemUnitMeasure { get; set; }
        public string ItemPrice { get; set; }
        public string Images { get; set; }
        public string ZoomOption { get; set; }
        public string QuantityRegExp { get; set; }

        public decimal PurchaseMultiplier { get; set; }
        public bool IsDontEarnPoints { get; set; }
        public bool ShowBuyButton { get; set; }
        public bool HidePriceUntilCart { get; set; }
        public bool IsCallToOrder { get; set; }
        public static string GetMultipleImages(string itemCode, string imageSize)
        {
            StringBuilder images = new StringBuilder();

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT FileName, IsDefaultIcon, IsDefaultMedium FROM InventoryOverrideImage with (NOLOCK) WHERE ItemCode = {0} ORDER BY ImageIndex ASC", DB.SQuote(itemCode)))
                {

                    string defaultField = string.Empty;
                    while (reader.Read())
                    {

                        ProductImage img = ProductImage.LocateMultiImage("Product", itemCode, DB.RSField(reader, "FileName"), imageSize);

                        defaultField = "IsDefaultIcon";
                        if (imageSize.ToLower() == "medium") defaultField = "IsDefaultMedium";
                        images.AppendFormat("{0} {1}::", img.src, DB.RSFieldBool(reader, defaultField));

                    }
                }
            }
            return images.ToString();
        }

        public static List<MatrixGroupItems> GetMatrixItems(string matrixGroupItemCode, int pageSize, int pageNumber, string imageSize)
        {
            var lstMatrixGroupItems = new List<MatrixGroupItems>();
            var thisCustomer = Customer.Current;

            var eCommerceMatrixItems = ServiceFactory.GetInstance<IProductService>().GetMatrixItemDetails(String.Empty, matrixGroupItemCode, pageSize, pageNumber);
            foreach (var matrixItem in eCommerceMatrixItems)
            {
                 var tmpUOM = new StringBuilder();

    
                 lstMatrixGroupItems.Add(new MatrixGroupItems
                 {
                     TotalNumberOfItems = matrixItem.ItemCount,
                     ItemCounter = matrixItem.Counter,
                     ItemCode = matrixItem.MatrixItemCode,
                     ItemName = matrixItem.MatrixItemName,
                     ItemDescription = matrixItem.MatrixItemDescription,
                     ItemHTMLDescription = matrixItem.WebDescription,
                     ItemStock = InterpriseHelper.InventoryFreeStock(matrixItem.MatrixItemCode, thisCustomer),
                     ItemUnitMeasure = ProductPricePerUnitMeasure.GetAll(matrixItem.MatrixItemCode, thisCustomer),
                     ItemPrice = GetProductPrice(matrixItem.MatrixItemCode),
                     Images = GetMultipleImages(matrixItem.MatrixItemCode, imageSize),
                     ZoomOption = matrixItem.ZoomOption,
                     QuantityRegExp = AppLogic.GetQuantityRegularExpression(String.Empty, false),
                     ShowBuyButton = ((matrixItem.RequiresRegistration && thisCustomer.IsNotRegistered) || matrixItem.ShowBuyButton == false) ? false : true,
                     HidePriceUntilCart = matrixItem.HidePriceUntilCart,
                     IsCallToOrder = matrixItem.IsCallToOrder
                 });
              }
            return lstMatrixGroupItems;
        }

        public static string GetProductPrice(string itemCode)
        {
            Customer ThisCustomer = Customer.Current;

            string Price = string.Empty;

            var unitMeasureInfo = InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);

            decimal vat = 0;
            decimal salesPrice = Decimal.Zero;
            decimal promotionalPrice = Decimal.Zero;
            bool withVat = AppLogic.AppConfigBool("VAT.Enabled") && ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive;

            salesPrice = InterpriseHelper.GetSalesPriceAndTax(ThisCustomer.CustomerCode, itemCode, ThisCustomer.CurrencyCode, decimal.One, unitMeasureInfo.Code, withVat, ref promotionalPrice, ref vat, unitMeasureInfo, ThisCustomer);

            string salesPriceFormatted = ThisCustomer.FormatBasedOnMyCurrency(salesPrice);
            string promotionalPriceFormatted = ThisCustomer.FormatBasedOnMyCurrency(promotionalPrice);

            return (promotionalPrice > Decimal.Zero && promotionalPrice < salesPrice) ? promotionalPriceFormatted : salesPriceFormatted;

        }
    }
}
