﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class MatrixItemInfo
    {
        public bool HidePriceUntilCart { get; set; }
        public bool IsCallToOrder { get; set; }
        public string ItemCode { get; set; }
        public decimal MinimumOrderQuantity { get; set; }
        public bool RequiresRegistration { get; set; }
        public List<decimal> RestrictedQuantities { get; set; }
        public bool ShowBuyButton { get; set; }
        public int ItemCounter { get; set; }
        public List<ProductPricePerUnitMeasure> UnitMeasures { get; set; }
    }
}
