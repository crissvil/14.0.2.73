﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class ItemAttributesContainer
    {
        public string IconTitle { get; set; }
        public string IconAlt { get; set; }

        public string MediumTitle { get; set; }
        public string MediumAlt { get; set; }

        public string LargeTitle { get; set; }
        public string LargeAlt { get; set; }
    }
}
