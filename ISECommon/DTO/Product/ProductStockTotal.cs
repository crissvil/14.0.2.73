﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class ProductStockTotal
    {
        public decimal InStock { get; set; }
        public decimal FreeStock { get; set; }
        public decimal Committed { get; set; }
    }
}
