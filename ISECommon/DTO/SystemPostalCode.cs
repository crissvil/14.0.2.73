﻿using System;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class SystemPostalCode
    {
        public string PostalCode { get; set; }
        public int Plus4 { get; set; }
    }

}
