﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class JSONAddToCartParamDTO
    {
        public int ItemCounter { get; set; }
        public string ItemCode { get; set; }
        public string UnitMeasureCode { get; set; }
        public decimal InitialQuantity { get; set; }
        public bool IgnoreStockLevel { get; set; }
        public bool ItemTypeIsKitAndWeAreInEditMode { get; set; }
        public bool HideUnitMeasure { get; set; }
        
        public string AttributeNotAvailableText { get; set; }
        public string ProductNoEnuoughStockText { get; set; }
        public string QuantityText { get; set; }
        public string UnitMeasureText { get; set; }
        public string OrderLessThanText { get; set; }
        public string NoEnoughStockText { get; set; }
        public string SelectedItemNoEnoughStockText { get; set; }
        public string NotificationAvailabilityText { get; set; }
        public string EnterNoQuantityText { get; set; }
        public string SpecifyQuantityText { get; set; }
    }
}
