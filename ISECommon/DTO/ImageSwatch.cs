// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class ImageSwatch
    {
        public ImageSwatch(int id, string code)
        {
            this.id = id;
            this.code = code;
        }

        public int id = 0;
        public string code = string.Empty;

        public ProductImage swatch;
        public List<ProductImage> mediumImages = new List<ProductImage>();
        public List<ProductImage> largeImages = new List<ProductImage>();
    }
}
