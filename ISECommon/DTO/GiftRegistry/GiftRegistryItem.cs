﻿using System;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class GiftRegistryItem
    {
        public GiftRegistryItem() 
        {
            Visible = true;
        }
        public int Counter { get; set; }
        public Guid RegistryItemCode { get; set; }
        public Guid RegistryID { get; set; }
        public string ItemCode { get; set; }
        public string UnitMeasureCode { get; set; }
        public decimal Quantity { get; set; }
        public decimal Ordered { get; set; }
        public string Comment { get; set; }
        public int SortOrder { get; set; }
        public GiftRegistryItemType GiftRegistryItemType { get; set; }

        //For Display
        public string ProductPhotoPath { get; set; }
        public string ProductURL { get; set; }
        public int ProductCounter { get; set; }
        public string ProductName { get; set; }
        public decimal ProductPrice { get; set; }
        public string ProductPriceFormatted { get; set; }
        public string QuantityFormatted { get; set; }

        public string KitComposition { get; set; }
        //for logic
        public bool IsKit { get; set; }

        public bool Visible { get; set; }

    }
}