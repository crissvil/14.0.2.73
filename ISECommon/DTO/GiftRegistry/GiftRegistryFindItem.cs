﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{

    public class GiftRegistryFindItem
    {
        public GiftRegistryFindItem() { }

        public int Counter { get; set; }
        public Guid RegistryID { get; set; }
        public Guid ContactGUID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Title { get; set; }
        public string PictureFileName { get; set; }
        public string URLForViewing { get; set; }
        public int RowNumber { get; set; }
        public string OwnersFullName { get; set; }
    }

    public class GiftRegistryFindHeader
    {
        public GiftRegistryFindHeader() { }
        public double TotalRecord { get; set; }
        public IEnumerable<GiftRegistry> RawItems { get; set; }
    }

    public class GiftRegistryFindHeaderDTO
    {
        public GiftRegistryFindHeaderDTO() { }
        public double TotalRecord { get; set; }
        public int TotalSet { get; set; }
        public IEnumerable<GiftRegistryFindItem> Items { get; set; }
        public int CurrentRecord { get; set; }
        public int DefaultRecordPerSet { get; set; }
    }

}
