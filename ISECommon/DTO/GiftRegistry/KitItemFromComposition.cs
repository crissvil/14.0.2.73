﻿using System;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class KitItemFromComposition
    {
        public Guid? CartID { get; set; }
        public string ItemKitCode { get; set; }
        public string CustomerCode { get; set; }
        public string ItemCode { get; set; }
        public string GroupCode { get; set; }
        public DateTime CreatedOn { get; set; }
        public Guid? GiftRegistryID { get; set; }
    }
}
