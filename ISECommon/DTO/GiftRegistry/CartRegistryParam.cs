﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CartRegistryParam
    {
        public Guid? RegistryID { get; set; }
        public Guid? RegistryItemCode { get; set; }
    }
}
