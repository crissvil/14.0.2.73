﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class SalesOrderHistoryDTO
    {
        public int row { get; set; }
        public string salesOrderCode { get; set; }
        public string invoiceCode { get; set; }
        public string salesOrderDate { get; set; }
        public string total { get; set; }
        public string notes { get; set; }
        public string shippingDate { get; set; }
        public string paymentMethod { get; set; }
        public string paymentStatus { get; set; }
        public int electronicDownloadItemCount { get; set; }
        public string shippingStatus { get; set; }
        public string trackingNumber { get; set; }
        public string trackingURL { get; set; }
        public string location { get; set; }
    }
}
