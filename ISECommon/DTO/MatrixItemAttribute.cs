// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class MatrixItemAttribute
    {
        public MatrixItemAttribute(string attribute, string value)
        {
            this.Attribute = attribute;
            this.AttributeValue = value;
        }

        public string Attribute;
        public string AttributeValue;
    }
}
