// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class KitItem
    {
        public string Code;
        public string Name;
        public int Id;
        public string Type;
        public bool IsSelected = false;
        public bool IsDefault { get; set; }
        public List<ProductPricePerUnitMeasure> UnitMeasures = new List<ProductPricePerUnitMeasure>();
    }
}
