﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CustomerProductNotificationSubscription
    {
        public bool NotifyOnPriceDrop { get; set; }
        public bool NotifyOnItemAvail { get; set; }
    }
}
