﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CustomerSalesParam
    {
        public string Dimension { get; set; }
        public decimal Total { get; set; }
    }
}
