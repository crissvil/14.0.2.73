﻿using System.Runtime.Serialization;
namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class LiteOrderSummaryInfo
    {
        [DataMember]
        public string FreightRate {get;set;}

        [DataMember]
        public string Tax { get; set; }

        [DataMember]
        public string FreightTax { get; set; }
      
        [DataMember]
        public string ShippingMethod{get;set;}


        [DataMember]
        public string SubTotal { get; set; }

        [DataMember]
        public string GrandTotal { get; set; }

    }
}
