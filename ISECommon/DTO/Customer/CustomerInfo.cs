﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CustomerInfo
    {
        public string CustomerCode { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public DateTime DateRegistered { get; set; }
        public IEnumerable<AddressDTO> Address { get; set; }
        public string ContactCode { get; set; }

        //For update
        public Guid ContactGuid { get; set; }
        public string Salutation { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool IsOver13 { get; set; }
        public string EmailRule { get; set; }
        public bool IsOkToEmail { get; set; }
        public Customer.BusinessTypes BusinessType { get; set; }
        public string TaxNumber { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }
        public string Vector { get; set; }
        public string Mobile { get; set; }
    }

    public class CustomerInfoJSON
    {
        public string CustomerCode { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string FullName { get; set; }
        public string DateRegistered { get; set; }
    }
}
