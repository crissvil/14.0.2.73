﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CustomerInvoice
    {
        public DateTime InvoiceDate { get; set; }
        public string InvoiceCode { get; set; }
        public decimal Total { get; set; }
    }
}
