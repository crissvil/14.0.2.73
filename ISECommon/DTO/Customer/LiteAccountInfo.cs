﻿using System.Runtime.Serialization;
namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class LiteAccountInfo
    {
        [DataMember]
        public string Salutation { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public string Email { get; set; }

        [DataMember]
        public string ContactNumber { get; set; }

        [DataMember]
        public string Mobile { get; set; }

        [DataMember]
        public bool IsOkToEmail { get; set; }

        [DataMember]
        public bool IsOver13Checked { get; set; }

         
    }
}



