﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class AnonCustomerSettings
    {
        public string LocaleSettings { get; set; }
        public string CurrencyCode { get; set; }
    }

    public class AnonLatestContactGuidAndCustomerCode
    {
        public string CustomerCode { get; set; }
        public Guid ContactGuid { get; set; }
    }
}
