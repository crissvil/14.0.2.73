﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CustomerSalesOrder
    {
        public string SalesOrderCode { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public decimal Total { get; set; }
        public DateTime SalesOrderDate { get; set; }
        public string CurrencyCode { get; set; }
    }
}
