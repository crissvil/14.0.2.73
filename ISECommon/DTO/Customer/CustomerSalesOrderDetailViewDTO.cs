﻿using System;
using System.Collections.Generic;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CustomerSalesOrderDetailViewDTO
    {
        public decimal QuantityAllocated { get; set; }
        public string ItemCode { get; set; }
        public string UnitMeasureCode { get; set; }
    }
}
