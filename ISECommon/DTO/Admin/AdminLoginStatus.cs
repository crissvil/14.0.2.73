﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class AdminLoginStatus
    {
        public bool IsSuccess { get; set; }
        public bool IsLocked { get; set; }
    }
}
