// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using InterpriseSuiteEcommerceCommon.DataAccess;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class CountryAddressDTO
    {
        private string _code;
        private bool _isWithState;
        private bool _isSearchablePostal;

        public CountryAddressDTO(string countryCode, bool isWithState, bool isSearchablePostal)
        {
            _code = countryCode;
            _isWithState = isWithState;
            _isSearchablePostal = isSearchablePostal;
        }

        public string code
        {
            get { return _code; }
        }

        public bool withState
        {
            get { return _isWithState; }
        }

        public bool searchablePostal
        {
            get { return _isSearchablePostal; }
        }

        public List<StateDTO> states
        {
            get { return null; }
        }

        public static CountryAddressDTO Find(string withCode)
        {
            CountryAddressDTO country = null;

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "SELECT CountryCode, IsWithState, IsSearchablePostal FROM SystemCountry with (NOLOCK) WHERE CountryCode = {0}", DB.SQuote(withCode)))
                {
                    if (reader.Read())
                    {
                        country = new CountryAddressDTO(DB.RSField(reader, "CountryCode"), DB.RSFieldBool(reader, "IsWithState"), DB.RSFieldBool(reader, "IsSearchablePostal"));
                    }
                }
            }

            return country;
        }

        public List<StateDTO> GetStates()
        {
            return LookupDA.GetStates(_code);
        }

        public static List<CountryAddressDTO> GetAllCountries()
        {
            return LookupDA.GetAllCountries();
        }

        public static string GetLanguageCode(string countryCode)
        {
            return LookupDA.GetLanguageCodes(countryCode);
        }

    }
}
