// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class ItemWebOption
    {
        public bool RequiresRegistration = false;
        public bool ShowBuyButton = true;
        public bool IsCallToOrder = true;
        public bool HidePriceUntilCart = false;
        public decimal MinimumOrderQuantity = 0;
        public string ZoomOption = string.Empty;
        public List<decimal> RestrictedQuantities = new List<decimal>();
        public string ItemCode { get; set; }
        public int ItemCounter { get; set; }
        public bool ShowPurchasedItemsFromSameCategory { get; set; }
        public bool ShowViewedItemsFromSameCategory { get; set; }

        #region GetWebOption

        public static ItemWebOption GetWebOption(string itemCode)
        {
            var settings = new ItemWebOption();

            string query = @"SELECT iw.ShowBuyButton, iw.IsCallToOrder, iw.RestrictedQuantity, iw.HidePriceUntilCart, iw.MinOrderQuantity, iw.RequiresRegistration, 
                                    iw.ZoomOption, i.Counter, iw.ShowPurchasedItemsFromSameCategory, iw.ShowViewedItemsFromSameCategory 
                             FROM InventoryItem i with (NOLOCK) 
                             INNER JOIN InventoryItemWebOption iw with (NOLOCK) ON i.ItemCode = iw.ItemCode AND iw.WebsiteCode = {0} 
                             WHERE i.ItemCode = {1}".FormatWith(InterpriseHelper.ConfigInstance.WebSiteCode.ToDbQuote(), itemCode.ToDbQuote());   

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, query))
                {
                    if (reader.Read())
                    {
                        settings.RequiresRegistration = DB.RSFieldBool(reader, "RequiresRegistration");
                        settings.ShowBuyButton = DB.RSFieldBool(reader, "ShowBuyButton");
                        settings.HidePriceUntilCart = DB.RSFieldBool(reader, "HidePriceUntilCart");
                        settings.IsCallToOrder = DB.RSFieldBool(reader, "IsCallToOrder");
                        settings.MinimumOrderQuantity = DB.RSFieldDecimal(reader, "MinOrderQuantity");
                        settings.ZoomOption = DB.RSField(reader, "ZoomOption");
                        settings.ItemCounter = DB.RSFieldInt(reader, "Counter");
                        settings.ShowPurchasedItemsFromSameCategory = DB.RSFieldBool(reader, "ShowPurchasedItemsFromSameCategory");
                        settings.ShowViewedItemsFromSameCategory = DB.RSFieldBool(reader, "ShowViewedItemsFromSameCategory");


                        string[] quantites = DB.RSField(reader, "RestrictedQuantity").Split(',');

                        decimal quantity = Decimal.Zero;
                        foreach (string brokenQuantity in quantites)
                        {
                            if (Decimal.TryParse(brokenQuantity, out quantity)) { settings.RestrictedQuantities.Add(quantity); }
                        }
                    }
                }
            }

            return settings;
        }

        public static IEnumerable<ItemWebOption> GetWebOptions(IEnumerable<string> itemCodes)
        {
            var settings = new List<ItemWebOption>();
            string codes = String.Empty;

            if (itemCodes.Count() > 0)
            {
                codes = string.Join(",", itemCodes.Select(c => c.ToDbQuote()));
            }
            else
            {
                codes = "NULL";
            }
            
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT i.ItemCode, iw.ShowBuyButton, iw.IsCallToOrder, iw.RestrictedQuantity, iw.HidePriceUntilCart, iw.MinOrderQuantity, iw.RequiresRegistration FROM InventoryItem i with (NOLOCK) INNER JOIN InventoryItemWebOption iw with (NOLOCK) ON i.ItemCode = iw.ItemCode AND iw.WebsiteCode = {0} WHERE i.ItemCode IN ({1})", InterpriseHelper.ConfigInstance.WebSiteCode.ToDbQuote(), codes))
                {
                    while (reader.Read())
                    {

                        var itemOption = new ItemWebOption()
                        {
                            RequiresRegistration = DB.RSFieldBool(reader, "RequiresRegistration"),
                            ShowBuyButton = DB.RSFieldBool(reader, "ShowBuyButton"),
                            HidePriceUntilCart = DB.RSFieldBool(reader, "HidePriceUntilCart"),
                            IsCallToOrder = DB.RSFieldBool(reader, "IsCallToOrder"),
                            MinimumOrderQuantity = DB.RSFieldDecimal(reader, "MinOrderQuantity"),
                            ItemCode = DB.RSField(reader, "ItemCode")
                        };

                        string[] quantites = DB.RSField(reader, "RestrictedQuantity").Split(',');
                        decimal quantity = 0;
                        foreach (string brokenQuantity in quantites)
                        {
                            if (decimal.TryParse(brokenQuantity, out quantity))
                            {
                                itemOption.RestrictedQuantities.Add(quantity);
                            }
                        }

                        settings.Add(itemOption);
                    }
                }
            }

            return settings;
        }

        #endregion
    }
}
