// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class ShippingMethodDTO
    {
        private string _code = string.Empty;
        private string _description = string.Empty;
        private decimal _freight = decimal.Zero;
        private string _freightDisplay = string.Empty;
        private Guid _rateId = Guid.Empty;
        private string _freightCurrency = string.Empty;
        private bool _isError = false;
        private bool _isDefault = false;
        private string _carrierCode = string.Empty;
        private string _carrierDescription = string.Empty;

        private string _packagingType = string.Empty;
        private string _serviceType = string.Empty;
        private int _freightCalculation = 0;
        private int _chargeType = 0;
        private decimal _miscAmount = 0;
        private decimal _returnedRate = 0;
        private bool _isForOversizedItem = false;
        private string _oversizedItemName = string.Empty;
        private int _length = 0;
        private int _width = 0;
        private int _height = 0;
        private decimal _weightThreshold = 0;
        private bool _isDummyShippingMethod = false;

        public ShippingMethodDTO() {}

        public ShippingMethodDTO(string code, string description)
        {
            _code = code;
            _description = description;
            _isError = false;
            _isDefault = false;
        }

        public string Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public decimal Freight
        {
            get { return _freight; }
            set { _freight = value; }
        }

        public string FreightDisplay
        {
            get { return _freightDisplay; }
            set { _freightDisplay = value; }
        }

        public Guid RateID
        {
            get { return _rateId; }
            set { _rateId = value; }
        }

        public string FreightCurrencyCode
        {
            get { return _freightCurrency; }
            set { _freightCurrency = value; }
        }

        public bool IsError
        {
            get { return _isError; }
            set { _isError = value; }
        }

        public bool IsDefault
        {
            get { return _isDefault; }
            set { _isDefault = value; }
        }

        public string CarrierCode
        {
            get { return _carrierCode; }
            set { _carrierCode = value; }
        }

        public string CarrierDescription
        {
            get { return _carrierDescription; }
            set { _carrierDescription = value; }
        }

        public string PackagingType
        {
            get { return _packagingType; }
            set { _packagingType = value; }
        }
        public string ServiceType
        {
            get { return _serviceType; }
            set { _serviceType = value; }
        }
        public int FreightCalculation
        {
            get { return _freightCalculation; }
            set { _freightCalculation = value; }
        }
        public int ChargeType
        {
            get { return _chargeType; }
            set { _chargeType = value; }
        }
        public decimal MiscAmount
        {
            get { return _miscAmount; }
            set { _miscAmount = value; }
        }

        public decimal ReturnedRate
        {
            get { return _returnedRate; }
            set { _returnedRate = value; }
        }

        public bool ForOversizedItem
        {
            get { return _isForOversizedItem; }
            set { _isForOversizedItem = value; }
        }

        public string OversizedItemName
        {
            get { return _oversizedItemName; }
            set { _oversizedItemName = value; }
        }

        public string OverSizedItemCode { get; set; }

        public int Length
        {
            get { return _length; }
            set { _length = value; }
        }

        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        public int Height
        {
            get { return _height; }
            set { _height = value; }
        }

        public decimal WeightThreshold
        {
            get { return _weightThreshold; }
            set { _weightThreshold = value; }
        }

        public bool IsDummyShippingMethod
        {
            get { return _isDummyShippingMethod; }
            set { _isDummyShippingMethod = value; }
        }

        public string FreightChargeType { get; set; }

    }
}
