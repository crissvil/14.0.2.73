// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace InterpriseSuiteEcommerceCommon.DTO
{
    public class Order
    {
        private Guid _customerGuid;
        private string _customerCode;
        private string _localeSetting;
        private string _orderNumber;
        private string _orderType;
        private string _paymentMethod;
        private string _paymentTermCode;
        private string _affiliateName;
        private Customer _thisCustomer;
        private DateTime _orderDate;
        
        private decimal _orderTotalRate;
        private decimal _orderSubTotalRate;
        private decimal _freightRate;
        private decimal _taxRate;

        private List<CartItem> _cartItems;
        private Address _shippingAddress;
        private Address _billingAddress;

        public Order(string ordernumber)
        {
            _orderNumber = ordernumber;
            LoadFromDB();
        }

        private void LoadFromDB()
        {

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "dbo.eCommercegetOrder " + DB.SQuote(_orderNumber)))
                {
                    if (reader.Read())
                    {

                        this._customerGuid = DB.RSFieldGUID2(reader, "CustomerGUID");
                        this._customerCode = DB.RSField(reader, "CustomerCode");
                        this._localeSetting = DB.RSField(reader, "LocaleSetting");

                        this._orderType = DB.RSField(reader, "Type");
                        this._orderDate = DB.RSFieldDateTime(reader, "OrderDate");

                        this._paymentMethod = DB.RSField(reader, "PaymentMethod");
                        this._paymentTermCode = DB.RSField(reader, "PaymentTermCode");

                        this._affiliateName = DB.RSField(reader, "AffiliateName");

                        this._orderTotalRate = DB.RSFieldDecimal(reader, "OrderTotalRate");
                        this._orderSubTotalRate = DB.RSFieldDecimal(reader, "OrderSubTotalRate");
                        this._freightRate = DB.RSFieldDecimal(reader, "FreightRate");
                        this._taxRate = DB.RSFieldDecimal(reader, "TaxRate");

                        _thisCustomer = Customer.Find(this._customerGuid);

                        if (null == _thisCustomer)
                        {
                            _thisCustomer = Customer.MakeAnonymous();
                        }

                        this._billingAddress = Address.New(_thisCustomer, AddressTypes.Billing);
                        this._billingAddress.AddressID = DB.RSField(reader, "BillToCode");
                        this._billingAddress.Name = DB.RSField(reader, "BillToName");
                        this._billingAddress.Address1 = DB.RSField(reader, "BillToAddress");
                        this._billingAddress.City = DB.RSField(reader, "BillToCity");
                        this._billingAddress.State = DB.RSField(reader, "BillToState");
                        this._billingAddress.PostalCode = DB.RSField(reader, "BillToPostalCode");
                        this._billingAddress.County = DB.RSField(reader, "BillToCounty");
                        this._billingAddress.Country = DB.RSField(reader, "BillToCountry");
                        this._billingAddress.Phone = DB.RSField(reader, "BillToPhone");

                        this._shippingAddress = Address.New(_thisCustomer, AddressTypes.Shipping);
                        this._shippingAddress.AddressID = DB.RSField(reader, "ShipToCode");
                        this._shippingAddress.Name = DB.RSField(reader, "ShipToName");
                        this._shippingAddress.Address1 = DB.RSField(reader, "ShipToAddress");
                        this._shippingAddress.City = DB.RSField(reader, "ShipToCity");
                        this._shippingAddress.State = DB.RSField(reader, "ShipToState");
                        this._shippingAddress.PostalCode = DB.RSField(reader, "ShipToPostalCode");
                        this._shippingAddress.County = DB.RSField(reader, "ShipToCounty");
                        this._shippingAddress.Country = DB.RSField(reader, "ShipToCountry");
                        this._shippingAddress.Phone = DB.RSField(reader, "ShipToPhone");
                    }
                }
            }

            this._cartItems = new List<CartItem>();


            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "dbo.eCommerceGetOrderItems " + DB.SQuote(_orderNumber)))
                {
                    while (reader.Read())
                    {
                        CartItem newItem = new CartItem();

                        newItem.ItemCounter = DB.RSFieldInt(reader, "Counter");
                        newItem.ItemCode = DB.RSField(reader, "ItemCode");
                        newItem.ItemName = DB.RSField(reader, "ItemDescription");
                        newItem.UnitMeasureCode = DB.RSField(reader, "UnitMeasureCode");
                        newItem.m_Quantity = Convert.ToInt32(DB.RSFieldDecimal(reader, "QuantityOrdered"));
                        newItem.SetPrice(DB.RSFieldDecimal(reader, "ExtPriceRate"));

                        this._cartItems.Add(newItem);
                    }
                }
            }
        }

        public string CustomerCode
        {
            get { return _customerCode; }
        }

        public Customer Customer
        {
            get { return _thisCustomer; }
        }

        public string LocaleSetting
        {
            get { return _localeSetting; }
        }

        public string OrderNumber
        {
            get { return _orderNumber; }
        }

        public string OrderType
        {
            get { return _orderType; }
        }

        public string PaymentMethod
        {
            get { return _paymentMethod; }
        }

        public string PaymentTermCode
        {
            get { return _paymentTermCode; }
        }

        public string AffiliateName
        {
            get { return _affiliateName; }
        }
  
        public DateTime OrderDate
        {
            get { return _orderDate; }
        }

        public decimal OrderTotal
        {
            get { return _orderTotalRate; }
        }

        public decimal OrderSubTotal
        {
            get { return _orderSubTotalRate; }
        }

        public decimal Freight
        {
            get { return _freightRate; }
        }

        public decimal Tax
        {
            get { return _taxRate; }
        }

        public Address BillingAddress
        {
            get { return _billingAddress; }
        }

        public Address ShippingAddress
        {
            get { return _shippingAddress; }
        }

        public List<CartItem> CartItems
        {
            get { return _cartItems; }
        }
    }
}






