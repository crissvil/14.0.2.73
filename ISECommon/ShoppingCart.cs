// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using Interprise.Framework.Customer.DatasetGateway;
using InterpriseSuiteEcommerceCommon.DataAccess;
using InterpriseSuiteEcommerceCommon.DTO;
using InterpriseSuiteEcommerceCommon.Extensions;
using InterpriseSuiteEcommerceCommon.Tool;
using InterpriseSuiteEcommerceCommon.Domain.Infrastructure;
using Interprise.Framework.Base.Shared;
using InterpriseSuiteEcommerceCommon.Domain.Model.CustomModel;
using InterpriseSuiteEcommerceCommon.Domain.CustomModel;

namespace InterpriseSuiteEcommerceCommon
{
    /// <summary>
    /// Summary description for ShoppingCart.
    /// </summary>
    ///
    public enum CartTypeEnum
    {
        ShoppingCart = 0,
        WishCart = 1,
        RecurringCart = 2,
        GiftRegistryCart = 3,
        Design = 4
    }

    public enum CouponTypeEnum
    {
        OrderCoupon = 0,
        ProductCoupon = 1
    }

    public enum CouponDiscountTypeEnum
    {
        PercentDiscount = 0,
        AmountDiscount = 1
    }

    public enum DateIntervalTypeEnum
    {
        Unknown = 0,
        Daily = 1,
        Weekly = 2,
        Monthly = 3,
        Yearly = 4
    }

    public enum VatDefaultSetting
    {
        Inclusive = 1,
        Exclusive = 2
    }

    public class CartItem : IComparable, IClonable
    {
        public Customer ThisCustomer = Customer.Current;
        public DateTime m_CreatedOn = DateTime.MinValue;
        public int m_ShoppingCartRecordID = 0;
        public CartTypeEnum m_CartType = CartTypeEnum.ShoppingCart;
        public string _customerCode = string.Empty;
        public decimal m_Quantity;
        public decimal m_AllocatedQty;
        public decimal m_MinimumQuantity;
        public string m_TextOption;
        public decimal Weight;

        private decimal m_unitPrice = decimal.Zero;//ProductPrice
        private decimal _price = decimal.Zero;//ProductExtPrice
        private decimal _discountedPrice = decimal.Zero;
        private decimal _subTotal = decimal.Zero;
        private decimal _discountedSubTotal = decimal.Zero;
        private decimal _taxRate = decimal.Zero;//ProductTax
        private decimal _couponDiscount = decimal.Zero;// discounted amount based on coupon

        public string m_ShippingAddressCounter;
        public string m_ShippingAddressID;
        public string m_ShippingMethodID;
        public string m_ShippingMethod;
        public Address m_ShippingDetail;
        public string m_Notes;
        public string ItemCode;
        public int ItemCounter;
        public int CBNItemID;
        public string ItemName;
        public string ItemDescription;
        public string Status;
        public Guid Id;
        public Guid RealTimeRateId;
        public string ItemType;
        public string UnitMeasureCode;
        public decimal UnitMeasureQty;
        public List<decimal> RestrictedQuantities = new List<decimal>();
        public decimal InStock;
        public decimal FreeStock;
        public string KitPricingType;
        public string SupplierCode;
        public string ItemSupplierCode;
        public string ItemSupplierName;
        public bool IsDropShip;
        public bool IsSpecialOrder;
        public bool IsCheckoutOption;
        public bool IsPrePacked;
        public bool IsOverSized;
        public bool IsFeatured;
        public bool IsCBN;
        public string OverSizedShippingMethodCode;
        public string POStatus;
        public bool IsVDP;
        public string VDPDocumentCode;
        public string VDPCustomisationCode;
        public string DesignComments;
        public string DesignFilename;
        public string DesignCheckbox;
        public byte[] DesignFile;
        public int VDPType;
        public int MoveableQuantity;
        public bool IsArtworkItem;
        public string CategoryCode = string.Empty;

        public int CompareTo(Object item)
        {
            if (item is CartItem)
            {
                var citem = item as CartItem;
                return this.ItemCode.CompareTo(citem.ItemCode);
            }
            else
                throw new ArgumentException("Object is not a CartItem");
        }

        public enum CartComparableArgs
        {
            ITEM_CODE,
            ITEM_NAME,
            ITEM_DESCRIPTION,
            SUPPLIER_CODE,
            PRICE,
            ITEM_SUPPLIER_CODE,
            ITEM_SUPPLIER_NAME,
            DEFAULT
        }

        public int CompareTo(Object item, CartComparableArgs args)
        {
            if (item is CartItem)
            {
                CartItem citem = (CartItem)item;
                switch (args)
                {
                    case CartComparableArgs.ITEM_NAME:
                        return ItemName.CompareTo(citem.ItemName);
                    case CartComparableArgs.ITEM_DESCRIPTION:
                        return ItemDescription.CompareTo(citem.ItemDescription);
                    case CartComparableArgs.SUPPLIER_CODE:
                        return SupplierCode.CompareTo(citem.SupplierCode);
                    case CartComparableArgs.PRICE:
                        return Price.CompareTo(citem.Price);
                    case CartComparableArgs.ITEM_SUPPLIER_CODE:
                        return ItemSupplierCode.CompareTo(citem.ItemSupplierCode);
                    case CartComparableArgs.ITEM_SUPPLIER_NAME:
                        return ItemSupplierName.CompareTo(citem.ItemSupplierName);
                    default:
                        return ItemCode.CompareTo(citem.ItemCode);
                }
            }
            else
                throw new ArgumentException("Object is not a CartItem");

        }

        public class CartItemComparer : System.Collections.Generic.IComparer<CartItem>
        {

            private CartItem.CartComparableArgs compArg;

            public CartItemComparer(CartItem.CartComparableArgs arg)
            {
                compArg = arg;
            }

            int System.Collections.Generic.IComparer<CartItem>.Compare(CartItem itm1, CartItem itm2)
            {
                return itm1.CompareTo(itm2, compArg);
            }
        }

        public bool IsInitialized { get; set; }

        public bool PriceAndTaxAlreadyComputed { get; set; }
        public bool UnitPriceAlreadyComputed { get; set; }

        public bool DiscountAmountAlreadyComputed { get; set; }

        public decimal Price
        {
            get
            {
                if (!PriceAndTaxAlreadyComputed && IsInitialized)
                {
                    // NOTE:
                    // Retrieving the price is expensive to do in IS
                    // We'll just get it by demand
                    ComputePriceAndTax();
                    SaveCartItemPriceAndTax();
                }

                return _price;
            }

        }

        public decimal DiscountedPrice
        {
            get { return _discountedPrice; }
            set { _discountedPrice = value; }
        }
        public decimal SubTotal
        {
            get { return _subTotal; }
            set { _subTotal = value; }
        }
        public decimal DiscountedSubTotal
        {
            get { return _discountedSubTotal; }
            set { _discountedSubTotal = value; }
        }

        public void SetPrice(decimal price)
        {
            _price = price;
            if (IsInitialized)
                SaveCartItemPriceAndTax();
        }

        public decimal TaxRate
        {
            get
            {
                if (!PriceAndTaxAlreadyComputed && IsInitialized)
                {
                    // NOTE:
                    // Retrieving the price is expensive to do in IS
                    // We'll just get it by demand
                    ComputePriceAndTax();
                    SaveCartItemPriceAndTax();
                }

                return _taxRate;
            }
            set
            {
                _taxRate = value;
            }
        }

        public decimal CouponDiscount
        {
            get
            {
                return _couponDiscount;
            }
            set
            {
                _couponDiscount = value;
                DiscountAmountAlreadyComputed = true;
            }
        }

        private void ComputePriceAndTax()
        {
            decimal price = decimal.Zero;
            decimal vat = decimal.Zero;
            string shipToCode = (this.GiftRegistryID.IsNullOrEmptyTrimmed() && ThisCustomer.IsRegistered) ? this.ShippingAddressId : String.Empty;
            var isZeroPrice = AppLogic.IsZeroPrice(this.ItemCode);

            if (isZeroPrice)
            {
                _price = 0;
                _taxRate = 0;
            }
            else
            {
                switch (this.ItemType)
                {
                    case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT:
                        KitComposition kitCartComposition = KitComposition.FromCart(ThisCustomer, this.m_CartType, this.ItemCode, this.Id, (this.m_Quantity));
                        kitCartComposition.UnitQuantity = Convert.ToDecimal(this.m_Quantity);
                        kitCartComposition.Quantity = Convert.ToDecimal(this.UnitMeasureQty);
                        _price = kitCartComposition.GetSalesPrice(ref vat, CategoryCode, shipToCode);
                        SubTotal = _price;
                        _taxRate = vat;
                        break;
                    default:
                        bool withVat = (ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive);

                        string itemCode = this.ItemCode;
                        if (this.ItemType == "Matrix Item")
                        {
                            itemCode = AppLogic.GetItemCode(this.ItemCode);
                        }

                        decimal promotionalPrice = decimal.Zero;

                        price = InterpriseHelper.GetSalesPriceAndVat(ThisCustomer.CustomerCode,
                                    itemCode,
                                    ThisCustomer.CurrencyCode,
                                    Convert.ToDecimal(this.m_Quantity),
                                    this.UnitMeasureCode,
                                    withVat,
                                    ref promotionalPrice,
                                    ref vat, new UnitMeasureInfo { Code = string.Empty }, ThisCustomer, shipToCode);

                        if (promotionalPrice != decimal.Zero)
                        {
                            price = promotionalPrice;
                        }

                        _price = price;
                        SubTotal = price;
                        _taxRate = vat;
                        break;
                }
            }
            PriceAndTaxAlreadyComputed = true;
        }

        public decimal GetProductSalePrice()
        {
            decimal price = decimal.Zero;
            decimal vat = decimal.Zero;
            string shipToCode = (this.GiftRegistryID.IsNullOrEmptyTrimmed() && ThisCustomer.IsRegistered) ? this.ShippingAddressId : String.Empty;
            switch (this.ItemType)
            {
                case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT:
                    KitComposition kitCartComposition = KitComposition.FromCart(ThisCustomer, this.m_CartType, this.ItemCode, this.Id, this.m_Quantity);
                    kitCartComposition.Quantity = decimal.One;
                    kitCartComposition.UnitQuantity = this.UnitMeasureQty;
                    kitCartComposition.UnitMeasureCode = this.UnitMeasureCode;
                    price = kitCartComposition.GetSalesPrice(ref vat, CategoryCode, shipToCode);
                    break;
                default:
                    bool withVat = (ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive);
                    decimal promotionalPrice = decimal.Zero;
                    //Properly get price when item has pricelist
                    price = InterpriseHelper.GetSalesPriceAndTax(ThisCustomer.CustomerCode,
                                this.ItemCode,
                                ThisCustomer.CurrencyCode,
                                Convert.ToDecimal(this.m_Quantity),
                                this.UnitMeasureCode,
                                withVat,
                                ref promotionalPrice,
                                ref vat, new UnitMeasureInfo { Code = string.Empty }, ThisCustomer, shipToCode);
                    //Compute price per 1 qty
                    price = price / this.m_Quantity;
                    break;
            }

            return price;
        }

        private void ComputeUnitPrice()
        {
            decimal price = decimal.Zero;
            decimal vat = decimal.Zero;
            string shipToCode = (this.GiftRegistryID.IsNullOrEmptyTrimmed() && ThisCustomer.IsRegistered) ? this.ShippingAddressId : String.Empty;

            switch (this.ItemType)
            {
                case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT:
                    //If kit is coming from expressprint load only express bag kit item price otherwise get price for all kit items
                    KitComposition kitCartComposition = KitComposition.FromCart(ThisCustomer, this.m_CartType, this.ItemCode, this.Id, this.m_Quantity);
                    kitCartComposition.Quantity = decimal.One;
                    kitCartComposition.UnitQuantity = this.UnitMeasureQty;
                    kitCartComposition.UnitMeasureCode = this.UnitMeasureCode;
                    price = kitCartComposition.GetSalesPrice(ref vat, CategoryCode, shipToCode);
                    break;
                default:
                    //no vat / tax
                    bool withVat = false;//(ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive);

                    decimal promotionalPrice = decimal.Zero;
                    price = InterpriseHelper.GetSalesPriceAndVat(ThisCustomer.CustomerCode,
                                this.ItemCode,
                                ThisCustomer.CurrencyCode,
                                Convert.ToDecimal(this.m_Quantity),
                                this.UnitMeasureCode,
                                withVat,
                                ref promotionalPrice,
                                ref vat, new UnitMeasureInfo { Code = string.Empty }, ThisCustomer, shipToCode);

                    //Compute price per 1 qty
                    price = price / this.m_Quantity;
                    if (promotionalPrice != decimal.Zero)
                    {
                        price = promotionalPrice;
                    }
                    break;
            }
            m_unitPrice = price;
            UnitPriceAlreadyComputed = true;

        }

        public decimal UnitPrice
        {
            get
            {
                if (!UnitPriceAlreadyComputed && IsInitialized)
                {
                    ComputeUnitPrice();
                    SaveCartItemUnitPrice();
                }
                return m_unitPrice;
            }
            set
            {
                if (!UnitPriceAlreadyComputed)
                {
                    ComputeUnitPrice();
                    SaveCartItemUnitPrice();
                }
                m_unitPrice = value;
                UnitPriceAlreadyComputed = true;
            }
        }

        public bool HasSupplier
        {
            get { return !CommonLogic.IsStringNullOrEmpty(SupplierCode); }
        }

        public bool CanShipSeparately
        {
            get { return (this.IsPrePacked || this.IsOverSized) && !this.IsDownload && !this.IsService; }
        }

        public string DisplayName
        {
            get
            {
                if (string.IsNullOrEmpty(ItemDescription))
                {
                    return ItemName;
                }

                return ItemDescription;
            }
        }

        public bool IsOverFreeStockCount
        {
            get { return m_Quantity > FreeStock; }
        }

        public bool IsOutOfStock
        {
            get { return FreeStock <= 0; }
        }

        public bool IsDownload
        {
            get { return this.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ELECTRONIC_DOWNLOAD; }
        }

        public bool IsService
        {
            get { return this.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_SERVICE; }
        }

        public bool IsAKit
        {
            get { return this.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT; }
        }

        public bool IsStock
        {
            get { return this.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_STOCK; }
        }

        public string ShippingMethod
        {
            get
            {
                return m_ShippingMethod;
            }
            set
            {
                m_ShippingMethod = value;
            }
        }

        public string ShippingAddressId
        {
            get
            {
                return m_ShippingAddressID;
            }
            set
            {
                m_ShippingAddressID = value;
            }
        }

        public Guid? GiftRegistryID { get; set; }
        public Guid? RegistryItemCode { get; set; }

        public string InStoreWarehouseCode { get; set; }

        public string Dimensions;

        public SalesOrderDatasetGateway.CustomerSalesOrderDetailViewRow AssociatedLineItemRow = null;

        public CartItem Copy()
        {
            // Copy all the value types as is
            // but the leave special case for some reference types...
            var newCopy = new CartItem();
            newCopy.m_CreatedOn = this.m_CreatedOn;
            newCopy.m_ShoppingCartRecordID = this.m_ShoppingCartRecordID;
            newCopy.m_CartType = this.m_CartType;
            newCopy._customerCode = this._customerCode;
            newCopy.m_Quantity = this.m_Quantity;
            newCopy.m_TextOption = this.m_TextOption;
            newCopy.Weight = this.Weight;
            newCopy.m_ShippingAddressCounter = this.m_ShippingAddressCounter;
            newCopy.m_ShippingAddressID = this.m_ShippingAddressID;
            newCopy.m_ShippingMethodID = this.m_ShippingMethodID;
            newCopy.m_ShippingMethod = this.m_ShippingMethod;
            newCopy.m_ShippingDetail = this.m_ShippingDetail;
            newCopy.m_Notes = this.m_Notes;
            newCopy.ItemCode = this.ItemCode;
            newCopy.ItemCounter = this.ItemCounter;
            newCopy.ItemName = this.ItemName;
            newCopy.ItemDescription = this.ItemDescription;
            newCopy.Id = this.Id;
            newCopy.RealTimeRateId = this.RealTimeRateId;
            newCopy.ItemType = this.ItemType;
            newCopy.UnitMeasureCode = this.UnitMeasureCode;
            newCopy.UnitMeasureQty = this.UnitMeasureQty;
            newCopy.Status = this.Status;
            List<int> copyOfRestrictedQuantities = new List<int>();
            foreach (int quantity in this.RestrictedQuantities.ToArray())
            {
                newCopy.RestrictedQuantities.Add(quantity);
            }
            newCopy.InStock = this.InStock;
            newCopy.FreeStock = this.FreeStock;

            newCopy.AssociatedLineItemRow = this.AssociatedLineItemRow;
            newCopy.KitPricingType = this.KitPricingType;
            newCopy.SupplierCode = this.SupplierCode;

            newCopy.IsOverSized = this.IsOverSized;
            newCopy.IsPrePacked = this.IsPrePacked;
            newCopy.IsFeatured = this.IsFeatured;
            newCopy.Dimensions = this.Dimensions;
            newCopy.OverSizedShippingMethodCode = this.OverSizedShippingMethodCode;
            return newCopy;
        }

        public void RecomputeCartItemPrice()
        {
            this.PriceAndTaxAlreadyComputed = false;
            this.UnitPriceAlreadyComputed = false;
            this.DiscountAmountAlreadyComputed = false;
            this.ComputeUnitPrice();
            this.SaveCartItemUnitPrice();
            this.ComputePriceAndTax();
            this.SaveCartItemPriceAndTax();
        }

        private void SaveCartItemPriceAndTax()
        {
            DB.ExecuteSQL("UPDATE EcommerceShoppingCart SET ProductExtPrice = {0} , ProductTax = {1}"
                + " WHERE ShoppingCartRecID = {2}", this.Price.ToSqlDecimalFormat(), this.TaxRate.ToSqlDecimalFormat(), this.m_ShoppingCartRecordID);
        }

        private void SaveCartItemUnitPrice()
        {
            DB.ExecuteSQL("UPDATE EcommerceShoppingCart SET ProductPrice = {0} "
                + " WHERE ShoppingCartRecID = {1}", this.UnitPrice.ToSqlDecimalFormat(), this.m_ShoppingCartRecordID);
        }

        private void SaveCartItemDisocuntAmount()
        {
            DB.ExecuteSQL("UPDATE EcommerceShoppingCart SET ProductCouponDiscount = {0}"
                + " WHERE ShoppingCartRecID = {1}", this.CouponDiscount.ToSqlDecimalFormat(), this.m_ShoppingCartRecordID);
        }

        public void IncrementQuantity(decimal quantity)
        {
            quantity = CommonLogic.RoundQuantity(quantity);
            DB.ExecuteSQL("UPDATE EcommerceShoppingCart SET Quantity = " + CommonLogic.IIF(this.RestrictedQuantities.Count == 0, "Quantity+{0}", "{0}") + " WHERE ShoppingCartRecID = {1}", quantity.ToSqlDecimalFormat(), this.m_ShoppingCartRecordID);
            this.m_Quantity = (this.RestrictedQuantities.Count == 0) ? CommonLogic.RoundQuantity(this.m_Quantity + quantity) : quantity;
            this.RecomputeCartItemPrice();
        }

        public void SetQuantity(decimal quantity)
        {
            quantity = CommonLogic.RoundQuantity(quantity);
            if (quantity > 0)
            {
                DB.ExecuteSQL("UPDATE EcommerceShoppingCart SET Quantity = {0} WHERE ShoppingCartRecID = {1}", quantity.ToSqlDecimalFormat(), this.m_ShoppingCartRecordID);
                this.m_Quantity = quantity;
            }
            else
            {
                if (this.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                {
                    DB.ExecuteSQL("DELETE wkc FROM EcommerceKitCart wkc WHERE wkc.CartID = '{0}'", this.Id.ToString());
                }

                DB.ExecuteSQL("DELETE wc FROM EcommerceShoppingCart wc WHERE wc.ShoppingCartRecGuid = '{0}'", this.Id.ToString());
            }
        }

        public void SetLineItemNotes(string notes)
        {
            if (!notes.IsNullOrEmptyTrimmed())
            {
                DB.ExecuteSQL("UPDATE EcommerceShoppingCart SET Notes = {0} WHERE ShoppingCartRecID = {1} AND CustomerCode = {2} AND ContactCode = {3} "
                    , notes.ToDbQuote(), this.m_ShoppingCartRecordID, ThisCustomer.CustomerCode.ToDbQuote(), ThisCustomer.ContactCode.ToDbQuote());

                this.m_Notes = notes;
            }
        }

        public bool Matches(string itemCode, string addressCode, int itemCounter, string unitMeasureCode)
        {
            return Matches(itemCode, addressCode, itemCounter, unitMeasureCode, null);
        }

        public bool Matches(string itemCode, string addressCode, int itemCounter, string unitMeasureCode, KitComposition composition)
        {
            if (this.ItemCode == itemCode &&
                this.ItemCounter == itemCounter &&
                this.m_ShippingAddressID == addressCode &&
                this.UnitMeasureCode == unitMeasureCode)
            {
                if (this.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT &&
                    composition != null)
                {
                    KitComposition thisComposition = GetKitComposition();
                    if (null != thisComposition)
                    {
                        return thisComposition.Matches(composition);
                    }
                }
                else
                {
                    return true;
                }
            }

            return false;
        }

        public KitComposition GetKitComposition()
        {
            if (this.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
            {
                KitComposition cartItemKitComposition = KitComposition.FromCart(ThisCustomer, this.m_CartType, this.ItemCode, this.Id, (this.UnitMeasureQty * this.m_Quantity));
                return cartItemKitComposition;
            }

            return null;
        }

        public class ReserveItemCollection : List<ReserveItem>
        {
        }

        public T Clone<T>()
        {
            return (T)this.MemberwiseClone();
        }

        public CartItem Clone()
        {
            return this.Clone<CartItem>();
        }

        public decimal? RegistryItemQuantity
        {
            get
            {
                if (!RegistryItemCode.HasValue) return null;
                return GiftRegistryDA.GetGiftRegistryItemQuantityByRegistryItemCode(RegistryItemCode.Value);
            }
        }

        /// <summary>
        /// test if the cart quantity is greater the Remaining registry item quantity
        /// </summary>
        /// <returns>Boolean</returns>
        public bool HasRegistryItemQuantityConflict()
        {
            return ((RegistryItemQuantity.HasValue) && (this.m_Quantity > RegistryItemQuantity));
        }

        public bool IsRegistryStillExist()
        {
            if (!GiftRegistryID.HasValue) return false;
            return GiftRegistryDA.IsRegistryExist(GiftRegistryID.Value);
        }



    }

    public class ReserveItem
    {
        private decimal _qtyReserved = decimal.Zero;
        private DateTime _shipDate = DateTime.Now;
        private string _itemCode = string.Empty;


        public decimal QtyReserved
        {
            get { return _qtyReserved; }
            set { _qtyReserved = value; }
        }

        public DateTime ShipDate
        {
            get { return _shipDate.Date; }
            set { _shipDate = value; }
        }

        public string ItemCode
        {
            get { return _itemCode; }
            set { _itemCode = value; }
        }
    }

    public struct CouponStruct
    {
        public string m_Code;
        public string m_id;
        public CouponTypeEnum m_CouponType;
        public string m_Description;
        public DateTime m_ExpirationStartDate;
        public DateTime m_ExpirationDate;
        public CouponDiscountTypeEnum m_DiscountType;
        public decimal m_DiscountAmount;
        public decimal m_DiscountPercent;
        public bool m_IncludesFreeShipping;
        public bool m_IncludesAllCustomer;
        public bool m_IncludesAllProduct;
        public bool m_ExpiresOnFirstUseByAnyCustomer;
        public bool m_ExpiresAfterOneUsageByEachCustomer;
        public int m_ExpiresAfterNUses;
        /// <summary>
        /// The assigned coupon minimum order amount that will be compared to the order total - exclusive of tax
        /// </summary>
        public decimal m_RequiresMinimumOrderAmount;
        public string m_ValidForThisEntity;
        public int m_NumUses;
        public string m_CouponComputation;
        public bool m_IsInitialized;
    }

    public struct AddressInfo
    {
        public string m_NickName;
        public string m_FirstName;
        public string m_LastName;
        public string m_Company;
        public ResidenceTypes m_ResidenceType;
        public string m_Address1;
        public string m_Address2;
        public string m_Suite;
        public string m_City;
        public string m_State;
        public string m_Zip;
        public string m_Country;
        public string m_Phone;
        public string m_EMail;
    }

    public class CartItemCollection : List<CartItem>
    {
    }

    public class ShoppingCart
    {
        #region Private Variables
        private int m_SkinID;
        protected CartTypeEnum m_CartType;
        protected Customer m_ThisCustomer;
        private CouponStruct m_Coupon;
        private string m_EMail;
        protected CartItemCollection m_CartItems = new CartItemCollection();
        private System.Collections.Generic.Dictionary<string, EntityHelper> m_EntityHelpers;
        private string m_DeleteString;
        private string m_OrderNotes;
        private bool m_InventoryTrimmed;
        private bool m_HasNoStockPhasedOutItem;
        private bool m_MinimumQuantitiesUpdated;
        private bool m_OnlyLoadRecurringItemsThatAreDue;
        private string m_OriginalRecurringOrderNumber = string.Empty;
        private Addresses m_CustAddresses = new Addresses();
        private List<GiftCodeCustomModel> _currentGiftCodes = new List<GiftCodeCustomModel>();
        private List<CustomerCreditCustomModel> _currentCreditMemos = new List<CustomerCreditCustomModel>();
        private List<CustomerCreditCustomModel> _availableCreditMemos = new List<CustomerCreditCustomModel>();

        private decimal m_FreeShippingThreshold = 0.0M;
        private decimal m_MoreNeededToReachFreeShipping = 0.0M;
        private bool m_ShippingIsFree = false;
        private bool m_CouponsAllowed = false;
        private bool m_HasNoStockAndNoOpenPOItem = false;


        #endregion

        protected ShoppingCart() { }
        public ShoppingCart(int SkinID, Customer ThisCustomer, CartTypeEnum CartType, string OriginalRecurringOrderNumber, bool OnlyLoadRecurringItemsThatAreDue)
            : this(null, SkinID, ThisCustomer, CartType, OriginalRecurringOrderNumber, OnlyLoadRecurringItemsThatAreDue, true)
        { }

        /// <param name="itemCode">Add the item code to load only the particular shopping cart item</param>
        public ShoppingCart(System.Collections.Generic.Dictionary<string, EntityHelper> EntityHelpers, int SkinID, Customer ThisCustomer, CartTypeEnum CartType, string OriginalRecurringOrderNumber, bool OnlyLoadRecurringItemsThatAreDue, bool LoadDatafromDB, string pagename = "", string itemCode = "")
        {
            m_EntityHelpers = EntityHelpers;
            m_SkinID = SkinID;
            m_ThisCustomer = ThisCustomer;
            m_CartType = CartType;
            m_OriginalRecurringOrderNumber = OriginalRecurringOrderNumber;
            m_OnlyLoadRecurringItemsThatAreDue = OnlyLoadRecurringItemsThatAreDue;
            m_DeleteString = AppLogic.GetString("shoppingcart.cs.31", true);

            if (m_DeleteString.Length == 0)
            {
                m_DeleteString = "Delete";
            }

            if (!AppLogic.AppConfigBool("DisallowCoupons"))
            {
                m_CouponsAllowed = true;
            }

            if (LoadDatafromDB)
            {
                //deadlock
                int nooftries = 0;
                while (nooftries < AppLogic.InterpriseExceptionFacadeNumberOfTries)
                {
                    try
                    {
                        LoadFromDB(CartType, itemCode);
                        break;
                    }
                    catch (SqlException ex)
                    {
                        if (ex.ErrorCode == 1205)
                        {
                            nooftries += 1;
                            m_CartItems.Clear();
                        }
                        else
                        {
                            throw;
                        }
                    }
                }

                if (!m_CouponsAllowed)
                {
                    string customerCode = CommonLogic.IIF(ThisCustomer.IsRegistered, ThisCustomer.CustomerCode, ThisCustomer.CustomerID);
                    if (ThisCustomer.IsRegistered)
                    {
                        DB.ExecuteSQL("UPDATE Customer SET CouponCode=NULL WHERE CustomerCode=" + DB.SQuote(customerCode));
                    }
                    else
                    {
                        DB.ExecuteSQL("UPDATE EcommerceCustomer SET CouponCode=NULL WHERE CustomerID=" + DB.SQuote(customerCode));
                    }
                }
            }

            #region GiftCode, CreditMemo

            var appconfigService = ServiceFactory.GetInstance<IAppConfigService>();
            var shoppingCartService = ServiceFactory.GetInstance<IShoppingCartService>();
            var customerService = ServiceFactory.GetInstance<ICustomerService>();

            if (appconfigService.GiftCodeEnabled)
            {
                _currentGiftCodes = shoppingCartService.GetAppliedGiftCodes(true).ToList();
            }

            if (appconfigService.CreditRedemptionIsEnabled)
            {
                _availableCreditMemos = customerService.GetCustomerCreditMemosWithRemainingBalance().ToList();
                _currentCreditMemos = shoppingCartService.GetAppliedCreditMemos()
                                                         .Where(x => _availableCreditMemos.Exists(y => y.CreditCode == x.CreditCode))
                                                         .ToList();
            }

            #endregion

        }

        public decimal TotalQuantity()
        {
            decimal sum = 0;
            foreach (CartItem c in m_CartItems)
            {
                sum += c.m_Quantity;
            }
            return sum;
        }

        public virtual bool MeetsMinimumOrderQuantity(Decimal MinOrderQuantity)
        {
            if (MinOrderQuantity <= 0 || IsEmpty())
            {
                return true; // disable checking for empty cart or all system products cart (those are handled separately)
            }
            decimal N = 0;
            foreach (CartItem c in this.CartItems)
            {
                if (!c.IsService)
                {
                    N += c.m_Quantity;
                }
            }
            return (N >= MinOrderQuantity);
        }

        public static void Age(int numDays, CartTypeEnum cartType)
        {
            if (numDays > 0)
            {
                var ageDate = System.DateTime.Now.AddDays(-numDays);
                ServiceFactory.GetInstance<IShoppingCartService>().DeleteEcommerceCustomerCartRecord(((int)cartType), ageDate);
            }
        }

        public bool IsEmpty(string CustomerID, CartTypeEnum CartType, string ContactCode)
        {
            return (NumItems(CustomerID, CartType, ContactCode) == 0);
        }

        public static bool CartIsEmpty(string customerCode, CartTypeEnum CartType)
        {
            return DB.GetSqlN("select count(*) as N from EcommerceShoppingCart  with (NOLOCK) where CartType=" + ((int)CartType).ToString() + " and WebSiteCode = " + DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode) + " and CustomerCode=" + DB.SQuote(customerCode)) == 0;
        }

        public static decimal NumItems(string CustomerID, CartTypeEnum CartType, string ContactCode)
        {
            var cart = ServiceFactory.GetInstance<IShoppingCartService>().New(CartTypeEnum.ShoppingCart, true);
            return cart.NumItems();
        }

        private decimal GetKitItemPrice(CartItem item)
        {
            decimal price = Decimal.Zero;

            bool isCurrencyIncludedForInventorySelling = InterpriseHelper.IsCurrencyIncludedForInventorySelling(ThisCustomer.CurrencyCode);

            // Special case 2 
            //  Currency is added in Inventory Selling Currency late
            //  after the kit has been created, ideally the kit should regenerate kit pricing for this currency
            //  but for the meantime, we should handle this by looking into the home currency
            bool currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem =
            InterpriseHelper.CurrencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYet(ThisCustomer.CurrencyCode, item.ItemCode);

            string kitDetailQuery =
                string.Format(
                    "exec GetEcommerceKitCartDetail @ItemKitCode = {0}, @CurrencyCode = {1}, @LanguageCode = {2}, @CustomerCode = {3}, @CartID = {4}, @ContactCode = {5}",
                    item.ItemCode.ToDbQuote(),
                    (isCurrencyIncludedForInventorySelling && !currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem) ? ThisCustomer.CurrencyCode.ToDbQuote() : Currency.GetHomeCurrency().ToDbQuote(),
                    ThisCustomer.LocaleSetting.ToDbQuote(),
                    ThisCustomer.CustomerCode.ToDbQuote(),
                    item.Id.ToString().ToDbQuote(),
                    ThisCustomer.ContactCode.ToDbQuote()
                );

            // The exchange rate to be used if we must convert
            decimal exchangeRate = Decimal.Zero;
            if (!isCurrencyIncludedForInventorySelling ||
                currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem)
            {
                exchangeRate = Currency.GetExchangeRate(ThisCustomer.CurrencyCode);
            }

            // read the records from the EcommerceKitCart table...

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, kitDetailQuery))
                {
                    while (reader.Read())
                    {
                        decimal total = Decimal.Zero;
                        if (isCurrencyIncludedForInventorySelling &&
                            !currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem)
                        {
                            total = DB.RSFieldDecimal(reader, "TotalRate");
                        }
                        else
                        {
                            total = Interprise.Facade.Base.SimpleFacade.Instance.ConvertCurrency(exchangeRate, DB.RSFieldDecimal(reader, "TotalRate"), false, ThisCustomer.CurrencyCode, Interprise.Framework.Base.Shared.Enum.CurrencyFormat.Total);
                        }

                        price += total;
                    }
                }
            }

            return price;
        }

        private Guid? GiftRegistryID { get; set; }
        private Guid? GiftRegistryItemCode { get; set; }

        private bool HasNoStockPhasedOutComponent(string itemcode, string itemid)
        {
            bool hasNoStockPhasedOutComp = false;
            bool isCurrencyIncludedForInventorySelling = InterpriseHelper.IsCurrencyIncludedForInventorySelling(ThisCustomer.CurrencyCode);
            bool currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem =
            InterpriseHelper.CurrencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYet(ThisCustomer.CurrencyCode, itemcode);

            string kitDetailQuery =
                string.Format(
                    "exec GetEcommerceKitCartDetail @ItemKitCode = {0}, @CurrencyCode = {1}, @LanguageCode = {2}, @CustomerCode = {3}, @CartID = {4}, @ContactCode = {5}",
                    itemcode.ToDbQuote(),
                    (isCurrencyIncludedForInventorySelling && !currencyIsIncludedInInventorySellingCurrencyButHasNoKitPricingDetailYetForThisItem) ? ThisCustomer.CurrencyCode.ToDbQuote() : Currency.GetHomeCurrency().ToDbQuote(),
                    ThisCustomer.LocaleSetting.ToDbQuote(),
                    ThisCustomer.CustomerCode.ToDbQuote(),
                    itemid.ToDbQuote(),
                    ThisCustomer.ContactCode.ToDbQuote());

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, kitDetailQuery))
                {
                    while (reader.Read())
                    {
                        string status = DB.RSField(reader, "Status");
                        decimal freeStock = Convert.ToDecimal(DB.RSFieldDecimal(reader, "FreeStock"));
                        string itemType = DB.RSField(reader, "ItemType");
                        string[] itemTypesToCheck = {   Interprise.Framework.Base.Shared.Const.ITEM_TYPE_STOCK.ToLowerInvariant() , 
                                                        Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM.ToLowerInvariant(),
                                                        Interprise.Framework.Base.Shared.Const.ITEM_TYPE_ASSEMBLY.ToLowerInvariant() };

                        if (status == "P" && freeStock <= 0 && itemTypesToCheck.Contains(itemType.ToLowerInvariant()))
                        {
                            hasNoStockPhasedOutComp = true;
                            return hasNoStockPhasedOutComp;
                        }
                    }
                }
            }

            return hasNoStockPhasedOutComp;
        }

        protected void LoadFromDB(CartTypeEnum CartType, string itemCode = "")
        {
            m_InventoryTrimmed = false;
            m_MinimumQuantitiesUpdated = false;
            m_HasNoStockPhasedOutItem = false;
            m_HasNoStockAndNoOpenPOItem = false;
            bool shouldLimitCartToQuantityOnHand = AppLogic.AppConfigBool("Inventory.LimitCartToQuantityOnHand");
            bool isTrimmed = false;
            var shoppingCartService = ServiceFactory.GetInstance<IShoppingCartService>();
            var appConfigService = ServiceFactory.GetInstance<IAppConfigService>();

            if (CartType == CartTypeEnum.ShoppingCart && m_ThisCustomer.CustomerCode.Length != 0)
            {

                shoppingCartService.UpdateInStorePickupCartItemStock();

                // only do these on the shopping cart
                m_InventoryTrimmed = InterpriseHelper.CheckCartInventoryIfTrimmed(m_ThisCustomer, shouldLimitCartToQuantityOnHand);
                //Checking for Minimum Order Quantity of Item(s) will be temporarily deferred.
                // Concerning issue : Multiple Uom Items
                if (ThisCustomer.PrimaryShippingAddressID != string.Empty)
                {
                    // repair addresses if required
                    // force all "0" or "invalid" address id records in the cart to be set to the primary shipping address. This is to avoid total confusion
                    // later in the cart where gift registry items, multi-ship items, etc, are all involved!
                    string msql = "UPDATE EcommerceShoppingCart SET " +
                        " ShippingAddressID=" + DB.SQuote(ThisCustomer.PrimaryShippingAddressID) +
                        " WHERE CartType=" + ((int)CartTypeEnum.ShoppingCart).ToString() +
                        " AND CustomerCode=" + DB.SQuote(ThisCustomer.CustomerCode) +
                        " AND ContactCode=" + DB.SQuote(ThisCustomer.ContactCode) +
                        " AND ShippingAddressID NOT IN (SELECT ShiptoCode FROM CustomerShipTo with (NOLOCK) WHERE CustomerCode=" + DB.SQuote(m_ThisCustomer.CustomerCode) +
                        " UNION ALL " +
                        " SELECT CreditCardCode AS ShiptoCode FROM CustomerCreditcard with (NOLOCK) WHERE CustomerCode=" + DB.SQuote(m_ThisCustomer.DefaultBillingCode) + ")" +
                        " AND ShippingAddressID NOT IN (SELECT cs.shiptocode FROM EcommerceGiftRegistry egr " +
                                                        "INNER JOIN CRMcontact cc on egr.contactguid = cc.contactguid " +
                                                        "INNER JOIN Customer c on cc.entitycode = c.customercode " +
                                                        "INNER JOIN customershipto cs on c.customercode = cs.customercode)";

                    DB.ExecuteSQL(msql);
                }
            }

            // Need to pull the Shipping Address from Address table
            var sql = new StringBuilder(4096);
            sql.AppendFormat(
                "dbo.GetEcommerceShoppingCart {0}, {1}, {2}, {3}, {4}, {5}, {6}",
                ((int)CartType).ToString(),
                m_ThisCustomer.CustomerCode.ToString().ToDbQuote(),
                InterpriseHelper.ConfigInstance.WebSiteCode.ToDbQuote(),
                ThisCustomer.WarehouseCode.ToDbQuote(),
                ThisCustomer.LanguageCode.ToDbQuote(),
                ThisCustomer.ContactCode.ToDbQuote(),
                InterpriseHelper.ConfigInstance.UserCode.ToDbQuote()
            );
            sql = sql.Replace("^", " with (NOLOCK) ");

            m_CartItems.Clear();
            int i = 0;

            bool couponSet = false;

            m_EMail = string.Empty;
            m_OrderNotes = string.Empty;

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var rs = DB.GetRSFormat(con, sql.ToString()))
                {
                    while (rs.Read())
                    {
                        string code = DB.RSField(rs, "ItemCode");
                        if (!itemCode.IsNullOrEmptyTrimmed() && code != itemCode) continue;

                        var newItem = new CartItem();
                        newItem.IsInitialized = false;
                        newItem.m_CreatedOn = DB.RSFieldDateTime(rs, "CreatedOn");
                        newItem.m_ShoppingCartRecordID = DB.RSFieldInt(rs, "ShoppingCartRecID");
                        newItem.Id = (Guid)rs["ShoppingCartRecGUID"];
                        newItem.m_CartType = (CartTypeEnum)DB.RSFieldInt(rs, "CartType");
                        newItem.ItemCounter = DB.RSFieldInt(rs, "ItemCounter");
                        newItem._customerCode = m_ThisCustomer.CustomerCode.ToString();
                        newItem.m_Quantity = DB.RSFieldDecimal(rs, "Quantity");
                        newItem.m_TextOption = DB.RSField(rs, "TextOption");
                        newItem.m_ShippingMethodID = DB.RSFieldInt(rs, "ShippingMethodID").ToString();
                        newItem.m_ShippingMethod = DB.RSFieldByLocale(rs, "ShippingMethod", m_ThisCustomer.LocaleSetting);
                        newItem.UnitMeasureCode = DB.RSField(rs, "UnitMeasureCode");
                        newItem.UnitMeasureQty = DB.RSFieldDecimal(rs, "UnitMeasureQty");
                        newItem.ItemDescription = DB.RSField(rs, "ItemDescription");
                        newItem.Dimensions = DB.RSField(rs, "ProductDimensions");
                        newItem.IsPrePacked = DB.RSFieldBool(rs, "IsPrePacked");
                        newItem.IsOverSized = DB.RSFieldBool(rs, "IsOverSized");
                        newItem.IsFeatured = DB.RSFieldBool(rs, "IsFeatured");
                        newItem.OverSizedShippingMethodCode = DB.RSField(rs, "OverSizedShippingMethodCode");
                        newItem.m_AllocatedQty = DB.RSFieldDecimal(rs, "AllocatedQty");
                        newItem.GiftRegistryID = DB.RSFieldGUID3(rs, "GiftRegistryID");
                        newItem.RegistryItemCode = DB.RSFieldGUID3(rs, "RegistryItemCode");
                        newItem.Status = DB.RSField(rs, "Status");

                        // parse the restricted quantities
                        string restrictedQuantitiesValue = DB.RSField(rs, "RestrictedQuantity");
                        if (!restrictedQuantitiesValue.IsNullOrEmptyTrimmed())
                        {
                            string[] quantityValues = restrictedQuantitiesValue.Split(',');
                            if (quantityValues.Length > 0)
                            {
                                foreach (string quantityValue in quantityValues)
                                {
                                    decimal quantity = 0;
                                    if (decimal.TryParse(quantityValue, out quantity))
                                    {
                                        newItem.RestrictedQuantities.Add(quantity);
                                    }
                                }
                            }
                        }
                        else
                        {
                            newItem.RestrictedQuantities = new List<decimal>();
                        }

                        newItem.InStock = DB.RSFieldDecimal(rs, "InStock");
                        newItem.ItemCode = code;
                        newItem.ItemName = DB.RSField(rs, "ItemName");
                        newItem.ItemType = DB.RSField(rs, "ItemType");
                        newItem.KitPricingType = DB.RSField(rs, "KitPricingType");
                        newItem.SupplierCode = DB.RSField(rs, "SupplierCode");
                        newItem.IsDropShip = DB.RSFieldBool(rs, "IsDropShip");
                        newItem.IsSpecialOrder = rs.ToRSFieldBool("IsSpecialOrder");
                        newItem.Weight = DB.RSFieldDecimal(rs, "Weight");
                        newItem.m_Notes = DB.RSField(rs, "Notes");
                        newItem.m_MinimumQuantity = Convert.ToInt32(DB.RSFieldDecimal(rs, "MinOrderQuantity"));
                        newItem.InStoreWarehouseCode = rs.ToRSField("InStoreWarehouseCode");
                        newItem.VDPCustomisationCode = rs.ToRSField("VDPCustomisationCode_DEV004817");
                        newItem.VDPType = rs.ToRSFieldInt("VDPType_C");
                        newItem.DesignComments = rs.ToRSField("DesignComments_C");
                        newItem.DesignFilename = rs.ToRSField("DesignFilename_C");
                        newItem.DesignCheckbox = rs.ToRSField("DesignCheckbox_C");
                        newItem.DesignFile = rs.ToRSFieldImage("DesignFile_C");

                        // get cart price values
                        newItem.DiscountAmountAlreadyComputed = false;

                        var isZeroPrice = AppLogic.IsZeroPrice(code);
                        if (isZeroPrice)
                        {
                            newItem.SetPrice(decimal.Zero);
                            newItem.TaxRate = decimal.Zero;
                            newItem.PriceAndTaxAlreadyComputed = true;
                            newItem.UnitPrice = decimal.Zero;
                            newItem.UnitPriceAlreadyComputed = true;
                        }
                        else
                        {
                            //newItem.SetPrice(rs.ToRSFieldDecimal("ProductExtPrice"));
                            //newItem.TaxRate = rs.ToRSFieldDecimal("ProductTax");
                            //newItem.PriceAndTaxAlreadyComputed = (newItem.Price > Decimal.Zero);
                            //newItem.UnitPrice = rs.ToRSFieldDecimal("ProductPrice");
                            //newItem.UnitPriceAlreadyComputed = (newItem.UnitPrice > decimal.Zero);
                        }

                        if (newItem.m_CartType == CartTypeEnum.RecurringCart)
                        {
                            newItem.m_ShippingAddressID = DB.RSField(rs, "ShoppingCartShippingAddressID");
                            newItem.m_ShippingAddressCounter = DB.RSField(rs, "ShippingAddressCounter");
                        }
                        else
                        {
                            newItem.m_ShippingAddressID = DB.RSField(rs, "ShoppingCartShippingAddressID");
                            newItem.m_ShippingAddressCounter = DB.RSFieldInt(rs, "ShippingAddressCounter").ToString();
                        }

                        newItem.IsCheckoutOption = DB.RSFieldBool(rs, "CheckoutOption");

                        m_CartItems.Add(newItem);
                        m_EMail = DB.RSField(rs, "EMail");
                        m_OrderNotes = DB.RSField(rs, "OrderNotes").ToHtmlDecode();
                        newItem.IsCBN = DB.RSFieldBool(rs, "IsCBN");
                        newItem.CBNItemID = DB.RSFieldInt(rs, "CBNItemID");

                        if (newItem.ItemType == Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                        {
                            bool showErrorMessage = false;
                            if (AppLogic.AppConfigBool("HideOutOfStockProducts"))
                            {
                                showErrorMessage = shoppingCartService.HasNoStockAndNoOpenPOComponent(newItem.ItemCode, newItem.Id.ToString());
                                if (showErrorMessage)
                                {
                                    newItem.FreeStock = 0;
                                    m_HasNoStockAndNoOpenPOItem = true;
                                }
                                else
                                {
                                    newItem.FreeStock = 1;
                                }
                            }
                            else
                            {
                                showErrorMessage = HasNoStockPhasedOutComponent(newItem.ItemCode, newItem.Id.ToString());
                                if (showErrorMessage)
                                {
                                    newItem.FreeStock = 0;
                                    m_HasNoStockPhasedOutItem = true;
                                    newItem.Status = Interprise.Framework.Inventory.Shared.Const.ITEM_STATUS_PHASEOUT[0].ToString();
                                }
                                else
                                {
                                    newItem.FreeStock = 1;
                                }
                            }
                        }
                        else if (newItem.IsCBN && !newItem.CBNItemID.IsNullOrEmptyTrimmed())
                        {
                            string cbnUMCode = InterpriseHelper.GetCBNUnitMeasureCode(newItem.ItemCode, newItem.UnitMeasureCode);
                            Interprise.Facade.Base.CBN.CBNTransactionFacade cbnTransactionFacade = new Interprise.Facade.Base.CBN.CBNTransactionFacade();
                            decimal freeStock = Decimal.Zero;
                            freeStock = cbnTransactionFacade.ConfirmStockCount(newItem.CBNItemID.ToString(), Convert.ToInt32(newItem.m_Quantity).ToString(), cbnUMCode);
                            freeStock = rs.ToRSFieldDecimal("FreeStock") + (freeStock < 0 ? Decimal.Zero : freeStock);
                            newItem.FreeStock = freeStock;
                        }
                        else
                        {
                            newItem.FreeStock = DB.RSFieldDecimal(rs, "FreeStock");
                        }

                        newItem.POStatus = rs.ToRSField("POStatus");
                        newItem.IsVDP = rs.ToRSFieldBool("IsVDP_DEV004817");
                        newItem.VDPDocumentCode = rs.ToRSField("VDPDocumentCode_DEV004817");
                        newItem.IsArtworkItem = rs.ToRSFieldBool("AddArtworkFee_C");
                        newItem.CategoryCode = rs.ToRSField("CategoryCode");
                        
                        string rateId = DB.RSFieldGUID(rs, "RealTimeRateID");
                        if (rateId.IsNullOrEmptyTrimmed())
                        {
                            newItem.RealTimeRateId = Guid.Empty;
                        }
                        else
                        {
                            newItem.RealTimeRateId = new Guid(rateId);
                        }

                        if (newItem.Status == "P" && newItem.ItemType != Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                        {
                            if (newItem.IsOutOfStock)
                            {
                                m_HasNoStockPhasedOutItem = true;
                            }
                            else if (newItem.IsOverFreeStockCount)
                            {
                                shouldLimitCartToQuantityOnHand = true;
                                isTrimmed = InterpriseHelper.CheckCartInventoryIfTrimmed(m_ThisCustomer, shouldLimitCartToQuantityOnHand);
                                if (isTrimmed)
                                {
                                    m_InventoryTrimmed = isTrimmed;
                                }
                                newItem.m_Quantity = DB.GetSqlNDecimal(String.Format("SELECT Quantity AS N FROM EcommerceShoppingCart WITH (NOLOCK) WHERE ShoppingCartRecID = {0}", DB.SQuote(newItem.m_ShoppingCartRecordID.ToString())));
                            }

                            if (newItem.IsOutOfStock)
                            {
                                m_InventoryTrimmed = false;
                            }
                        }

                        if (appConfigService.HideOutOfStockProducts && newItem.ItemType != Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                        {
                            string[] poStatusToCheck = { "Open".ToLowerInvariant(), "Partial".ToLowerInvariant() };
                            if (newItem.Status == "A" && newItem.IsOutOfStock && !poStatusToCheck.Contains(newItem.POStatus.ToLowerInvariant()))
                            {
                                m_HasNoStockAndNoOpenPOItem = true;
                            }
                        }

                        if (shouldLimitCartToQuantityOnHand && m_HasNoStockPhasedOutItem && newItem.Status == "P" &&
                            newItem.ItemType != Interprise.Framework.Base.Shared.Const.ITEM_TYPE_KIT)
                        {
                            m_InventoryTrimmed = false;
                        }
                        i = i + 1;
                        if (m_InventoryTrimmed) { newItem.PriceAndTaxAlreadyComputed = false; }
                        newItem.IsInitialized = true;
                    }
                }
                con.Close();
            }

            if (!couponSet)
            {
                // create an "empty" coupon, but still set the code:
                m_Coupon.m_Code = m_ThisCustomer.CouponCode;
                m_Coupon.m_CouponType = CouponTypeEnum.OrderCoupon;
                m_Coupon.m_Description = string.Empty;
                m_Coupon.m_ExpirationStartDate = System.DateTime.MinValue;
                m_Coupon.m_ExpirationDate = System.DateTime.MinValue;
                m_Coupon.m_DiscountType = CouponDiscountTypeEnum.PercentDiscount;
                m_Coupon.m_DiscountAmount = System.Decimal.Zero;
                m_Coupon.m_DiscountPercent = 0.0M;
                m_Coupon.m_IncludesFreeShipping = false;
                m_Coupon.m_IncludesAllCustomer = false;
                m_Coupon.m_IncludesAllProduct = false;
                m_Coupon.m_ExpiresOnFirstUseByAnyCustomer = false;
                m_Coupon.m_ExpiresAfterOneUsageByEachCustomer = false;
                m_Coupon.m_ExpiresAfterNUses = 0;
                m_Coupon.m_RequiresMinimumOrderAmount = System.Decimal.Zero;
                m_Coupon.m_ValidForThisEntity = string.Empty;
                m_Coupon.m_NumUses = 0;
                m_Coupon.m_IsInitialized = false;
            }
        }

        public CouponStruct GetCoupon()
        {
            return m_Coupon;
        }

        public CartItem.ReserveItemCollection GetReservation(string itemCode)
        {
            //Load reservation information
            //Create a new instance of the reserved transactions collection for the cart item
            var reserveCol = new CartItem.ReserveItemCollection();
            string sql = string.Format("EcommerceGetShoppingCartReservation {0}, {1}", DB.SQuote(ThisCustomer.ContactCode), DB.SQuote(itemCode));

            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var rs = DB.GetRSFormat(con, sql))
                {
                    while (rs.Read())
                    {
                        var reserve = new ReserveItem()
                        {
                            ItemCode = DB.RSField(rs, "ItemCode"),
                            QtyReserved = DB.RSFieldDecimal(rs, "ReservedQty"),
                            ShipDate = DB.RSFieldDateTime(rs, "DueDate")
                        };
                        reserveCol.Add(reserve);
                    }
                }
                con.Close();
            }
            return reserveCol;
        }

        public CartItem.ReserveItemCollection GetReservation(IEnumerable<ItemReservationCustomModel> itemReservations, CartItem cartItem)
        {
            var appConfigService = ServiceFactory.GetInstance<IAppConfigService>();
            var reserveCol = new CartItem.ReserveItemCollection();

            if (!appConfigService.ShowStockHints) { return reserveCol; }
            if (!appConfigService.ShowShipDateInCart) { return reserveCol; }
            if (cartItem.IsDropShip) { return reserveCol; }

            var reservations = itemReservations.Where(x => x.CartRecordID == cartItem.m_ShoppingCartRecordID);

            foreach (var itemReservation in reservations)
            {
                if (itemReservation.CartRecordID == cartItem.m_ShoppingCartRecordID)
                {
                    cartItem.m_AllocatedQty = itemReservation.AllocatedQty;
                    if (itemReservation.ReservedQty > Decimal.Zero)
                    {
                        reserveCol.Add(new ReserveItem() { ItemCode = itemReservation.ItemCode, QtyReserved = itemReservation.ReservedQty, ShipDate = itemReservation.DueDate });
                    }
                }
            }
            return reserveCol;
        }

        public void ClearCoupon()
        {
            string customerCode = CommonLogic.IIF(ThisCustomer.IsRegistered, ThisCustomer.CustomerCode, ThisCustomer.CustomerID);
            if (ThisCustomer.IsRegistered)
            {
                DB.ExecuteSQL("UPDATE Customer SET CouponCode=NULL WHERE CustomerCode=" + DB.SQuote(customerCode));
            }
            else
            {
                DB.ExecuteSQL("UPDATE EcommerceCustomer SET CouponCode=NULL WHERE CustomerID=" + DB.SQuote(customerCode));
            }
            m_Coupon.m_Code = string.Empty;
            m_Coupon.m_id = string.Empty;
            m_Coupon.m_CouponType = CouponTypeEnum.OrderCoupon;
            m_Coupon.m_Description = string.Empty;
            m_Coupon.m_ExpirationStartDate = System.DateTime.MinValue;
            m_Coupon.m_ExpirationDate = System.DateTime.MinValue;
            m_Coupon.m_DiscountType = CouponDiscountTypeEnum.PercentDiscount;
            m_Coupon.m_DiscountAmount = System.Decimal.Zero;
            m_Coupon.m_DiscountPercent = 0.0M;
            m_Coupon.m_IncludesFreeShipping = false;
            m_Coupon.m_IncludesAllCustomer = false;
            m_Coupon.m_IncludesAllProduct = false;
            m_Coupon.m_ExpiresOnFirstUseByAnyCustomer = false;
            m_Coupon.m_ExpiresAfterOneUsageByEachCustomer = false;
            m_Coupon.m_ExpiresAfterNUses = 0;
            m_Coupon.m_RequiresMinimumOrderAmount = System.Decimal.Zero;
            m_Coupon.m_ValidForThisEntity = string.Empty;
            m_Coupon.m_NumUses = 0;
            m_ThisCustomer.CouponCode = string.Empty;
        }

        public string FilterItemAddressList(ArrayList cartItems, string AddressID)
        {
            string result = null;
            var c = (CartItem)cartItems[0];
            while ((cartItems.Count > 0) && (c.m_ShippingAddressID != AddressID))
            {
                cartItems.Remove(c);
                c = (CartItem)cartItems[0];
            }
            int i = 0;
            do
            {
                c = (CartItem)cartItems[i];
                i++;
            }
            while ((i < cartItems.Count) && (c.m_ShippingAddressID == AddressID));
            if (c.m_ShippingAddressID != AddressID)
            {
                result = c.m_ShippingAddressID;
                i--;
                do
                {
                    c = (CartItem)cartItems[i];
                    cartItems.Remove(c);
                }
                while (i < cartItems.Count);
            }
            else
            {
                result = null;
            }

            return result;
        }

        public string GetLineItemDescription(CartItem c, bool ShowLinkBack, bool VarReadOnly, bool ShowMultiShipAddressUnderItemDescription)
        {
            StringBuilder tmpS = new StringBuilder(4096);

            if (ShowLinkBack)
            {
                tmpS.Append("<a href=\"" + SE.MakeProductLink(c.ItemCounter.ToString(), "") + "\">");
            }
            tmpS.Append("<b>");
            tmpS.Append(AppLogic.MakeProperObjectName(c.ItemName, c.ItemDescription, ThisCustomer.LocaleSetting));
            tmpS.Append("</b>");
            if (ShowLinkBack)
            {
                tmpS.Append("</a>");
            }

            if (c.m_TextOption.Length != 0)
            {
                tmpS.Append("<br>");
                if (c.m_TextOption.IndexOf("\n") == -1)
                {
                    tmpS.Append(":&nbsp;");
                    tmpS.Append(HttpContext.Current.Server.HtmlDecode(c.m_TextOption));
                }
                else
                {
                    tmpS.Append(":<br>");
                    tmpS.Append(HttpContext.Current.Server.HtmlDecode(c.m_TextOption).Replace("\n", "<br>"));
                }
            }

            if (c.IsAKit)
            {
                tmpS.Append("&nbsp;&nbsp;<a href=\"ShoppingCart_change.aspx?recid=" + c.m_ShoppingCartRecordID.ToString() + "&productid=" + c.ItemCounter.ToString() + "\"><img src=\"" + AppLogic.LocateImageURL("skins/Skin_" + m_SkinID.ToString() + "/images/edit.gif") + "\" align=\"absmiddle\" border=\"0\" alt=\"" + AppLogic.GetString("shoppingcart.cs.4", true) + "\"></a>&nbsp;");

                StringBuilder tmp = new StringBuilder(4096);
                bool first = true;

                using (SqlConnection con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (IDataReader rsx = DB.GetRSFormat(con, "select KitItem.Name, KitCart.Quantity, KitCart.TextOption from EcommerceKitCart with (NOLOCK) inner join KitItem with (NOLOCK) on KitCart.KitItemID=KitItem.KitItemID where ShoppingCartRecID=" + c.m_ShoppingCartRecordID.ToString()))
                    {
                        while (rsx.Read())
                        {
                            if (!first)
                            {
                                tmp.Append("<br>");
                            }
                            tmp.Append("&nbsp;&nbsp;-&nbsp;");
                            if (!AppLogic.AppConfigBool("HideKitQuantity") || DB.RSFieldDecimal(rsx, "Quantity") > 1)
                            {
                                tmp.Append("(");
                                tmp.Append(DB.RSFieldDecimal(rsx, "Quantity").ToString());
                                tmp.Append(") ");
                            }
                            tmp.Append(DB.RSFieldByLocale(rsx, "Name", m_ThisCustomer.LocaleSetting));
                            if (DB.RSField(rsx, "TextOption").Length > 0)
                            {
                                tmpS.Append("&nbsp;");
                                if (DB.RSField(rsx, "TextOption").ToLower().StartsWith("images/orders/image/"))
                                {
                                    tmp.Append(": <a target=\"_blank\" href=\"" + DB.RSField(rsx, "TextOption") + "\">" + AppLogic.GetString("shoppingcart.cs.36") + "</a>");
                                }
                                else
                                {
                                    tmp.Append(": " + DB.RSField(rsx, "TextOption"));
                                }
                            }
                            first = false;
                        }
                    }
                }

                tmpS.Append("<div style=\"margin-left: 10px;\">");
                tmpS.Append(tmp.ToString());
                tmpS.Append("</div>");
            }

            if (c.IsDownload)
            {
                tmpS.Append("<br>");
                tmpS.Append(AppLogic.GetString("shoppingcart.cs.22"));
            }

            if ((this.HasMultipleShippingAddresses()) && ShowMultiShipAddressUnderItemDescription && !c.IsDownload)
            {
                if (!c.IsAKit)
                {
                    tmpS.Append("<br>");
                }
                tmpS.Append(AppLogic.GetString("shoppingcart.cs.24"));
                tmpS.Append(" ");
                if (!Customer.OwnsThisAddress(ThisCustomer.CustomerCode, c.m_ShippingAddressID))
                {
                    tmpS.Append(AppLogic.GetString("checkoutshippingmult.aspx.4"));
                }
                tmpS.Append("<div>");
                tmpS.Append(AppLogic.GetString("order.cs.23"));
                tmpS.Append(c.m_ShippingMethod);
                tmpS.Append("</div>");
            }

            if (CartType == CartTypeEnum.ShoppingCart)
            {
                if (!VarReadOnly)
                {
                    tmpS.Append("<br><br><b>" + AppLogic.GetString("shoppingcart.cs.23") + "</b><br><textarea class=\"addressselect\" rows=\"" + "\" cols=\"" + "\" name=\"Notes_" + c.m_ShoppingCartRecordID.ToString() + "\" id=\"Notes_" + c.m_ShoppingCartRecordID.ToString() + "\">" + c.m_Notes.ToHtmlEncode() + "</textarea>");
                }
                else
                {
                    if (c.m_Notes.Length != 0)
                    {
                        tmpS.Append("<br><br><b>" + AppLogic.GetString("shoppingcart.cs.23") + "</b><br>" + c.m_Notes.ToHtmlEncode());
                    }
                }
            }
            return tmpS.ToString();
        }

        public string DisplayMultiShipMethodSelector(bool VarReadOnly, Customer ViewingCustomer)
        {
            int ix = 1;
            StringBuilder tmpS = new StringBuilder(4096);
            if (CartType != CartTypeEnum.ShoppingCart)
            {
                return tmpS.ToString(); // can only do this action on the shoppingcart proper, not wish or registry
            }
            if (IsEmpty())
            {
                tmpS.Append("<br><br>");
                if (CartType == CartTypeEnum.ShoppingCart)
                {
                    Topic t1 = new Topic("EmptyCartText", m_ThisCustomer.LocaleSetting, m_SkinID, null);
                    tmpS.Append(t1.Contents);
                }
                if (CartType == CartTypeEnum.WishCart)
                {
                    Topic t1 = new Topic("EmptyWishListText", m_ThisCustomer.LocaleSetting, m_SkinID, null);
                    tmpS.Append(t1.Contents);
                }
                if (CartType == CartTypeEnum.GiftRegistryCart)
                {
                    Topic t1 = new Topic("EmptyGiftRegistryText", m_ThisCustomer.LocaleSetting, m_SkinID, null);
                    tmpS.Append(t1.Contents);
                }
                tmpS.Append("<br><br><br>");
            }
            else
            {
                bool ShowLinkBack = AppLogic.AppConfigBool("LinkToProductPageInCart");
                int ColSpan = 2;

                tmpS.Append("<table width=\"100%\" cellpadding=\"2\" cellspacing=\"0\" border=\"0\">");

                string LastID = string.Empty;
                if (this.HasDownloadComponents())
                {
                    tmpS.Append("<tr>");
                    tmpS.Append("<td height=\"15\" bgcolor=\"#" + AppLogic.AppConfig("LightCellColor") + "\" align=\"left\" valign=\"middle\"><b>");
                    tmpS.Append(string.Format(AppLogic.GetString("shoppingcart.cs.29"), ix.ToString()));
                    ix++;
                    tmpS.Append("</b></td>");
                    tmpS.Append("<td height=\"15\" bgcolor=\"#" + AppLogic.AppConfig("LightCellColor") + "\"  align=\"left\" valign=\"middle\"><b>");
                    tmpS.Append(AppLogic.GetString("shoppingcart.cs.28"));
                    tmpS.Append("</b></td>");
                    tmpS.Append("</tr>");
                    tmpS.Append("<tr><td colspan=\"" + ColSpan.ToString() + "\"><img src=\"images/spacer.gif\" width=\"100%\" height=\"4\"></td></tr>");
                }
                foreach (CartItem c in m_CartItems)
                {
                    bool first = true;
                    if (c.IsDownload)
                    {
                        tmpS.Append("<tr>");

                        // PRODUCT DESCRIPTION COL
                        tmpS.Append("<td align=\"left\" valign=\"top\">");
                        tmpS.Append(GetLineItemDescription(c, ShowLinkBack, VarReadOnly, false));
                        // include Quantity now:
                        tmpS.Append("<br>");
                        tmpS.Append(AppLogic.GetString("shoppingcart.cs.25"));
                        tmpS.Append(": ");
                        tmpS.Append(Localization.ParseLocaleDecimal(c.m_Quantity, m_ThisCustomer.LocaleSetting));
                        tmpS.Append("</td>");

                        // SHIPPING METHOD COL
                        tmpS.Append("<td align=\"left\" valign=\"top\">");
                        if (first)
                        {
                            tmpS.Append(AppLogic.GetString("checkoutshippingmult.aspx.8"));
                        }
                        else
                        {
                            tmpS.Append("&nbsp;");
                        }
                        tmpS.Append("</td>");
                        tmpS.Append("</tr>");
                        tmpS.Append("<tr><td colspan=\"" + ColSpan.ToString() + "\"><img src=\"images/spacer.gif\" width=\"100%\" height=\"6\"></td></tr>");
                        first = false;
                    }
                }

                string DistinctAddrIds = GetDistinctShippingAddressIDs(false, false);
                if (DistinctAddrIds.Length != 0)
                {
                    foreach (string AddressID in DistinctAddrIds.Split(','))
                    {
                        string ThisAddressID = AddressID;
                        tmpS.Append("<tr>");
                        tmpS.Append("<td height=\"15\" bgcolor=\"#" + AppLogic.AppConfig("LightCellColor") + "\" align=\"left\" valign=\"middle\"><b>");
                        tmpS.Append(string.Format(AppLogic.GetString("shoppingcart.cs.29"), ix.ToString()));
                        ix++;
                        tmpS.Append("</b></td>");
                        tmpS.Append("<td height=\"15\" bgcolor=\"#" + AppLogic.AppConfig("LightCellColor") + "\" align=\"left\" valign=\"middle\"><b>");
                        tmpS.Append(AppLogic.GetString("shoppingcart.cs.28"));
                        tmpS.Append("</b></td>");
                        tmpS.Append("</tr>");
                        tmpS.Append("<tr><td colspan=\"" + ColSpan.ToString() + "\"><img src=\"images/spacer.gif\" width=\"100%\" height=\"2\"></td></tr>");

                        tmpS.Append("<tr>");

                        // PRODUCT DESCRIPTION COL
                        tmpS.Append("<td align=\"left\" valign=\"top\">");
                        bool FirstItemInThisGroup = true;
                        foreach (CartItem c in m_CartItems)
                        {
                            if (!c.IsDownload && c.m_ShippingAddressID == ThisAddressID)
                            {
                                if (!FirstItemInThisGroup)
                                {
                                    tmpS.Append("<br><br>");
                                }
                                tmpS.Append(GetLineItemDescription(c, ShowLinkBack, VarReadOnly, false));
                                // include Quantity now:
                                tmpS.Append("<br>");
                                tmpS.Append(AppLogic.GetString("shoppingcart.cs.25"));
                                tmpS.Append(": ");
                                tmpS.Append(Localization.ParseLocaleDecimal(c.m_Quantity, m_ThisCustomer.LocaleSetting));
                                FirstItemInThisGroup = false;
                            }
                        }
                        tmpS.Append("</td>");

                        // SHIPING METHOD COL
                        tmpS.Append("<td align=\"left\" valign=\"top\">");
                        var adr = new Address();
                        adr.LoadByCustomer(ThisCustomer, AddressTypes.Shipping, DistinctAddrIds);
                        tmpS.Append("<b>");
                        tmpS.Append(AppLogic.GetString("shoppingcart.cs.30"));
                        tmpS.Append("</b>");
                        tmpS.Append("<br>");
                        if (m_ThisCustomer.CustomerCode == adr.CustomerCode)
                        {
                            tmpS.Append(adr.DisplayHTML(true));
                        }
                        tmpS.Append("<br><br>");

                        // GET CART TO SHOW SHIPPING METHODS & COSTS FOR ONLY THIS GROUP OF ITEMS BY ADDRESS ID
                        tmpS.Append("<b>");
                        tmpS.Append(AppLogic.GetString("shoppingcart.cs.10"));
                        tmpS.Append("</b>");
                        tmpS.Append("<br>");

                        tmpS.Append("</td>");
                        tmpS.Append("</tr>");

                        tmpS.Append("<tr><td colspan=\"" + ColSpan.ToString() + "\"><img src=\"images/spacer.gif\" width=\"100%\" height=\"8\"></td></tr>");
                    }
                }
                tmpS.Append("</table>");
            }
            return tmpS.ToString();
        }

        public string GetDistinctShippingAddressIDs(bool IncludeDownloadItems, bool IncludeSystemItems)
        {
            string tmpS = ",";
            foreach (CartItem c in CartItems)
            {
                if (tmpS.IndexOf("," + c.m_ShippingAddressID.ToString() + ",") == -1)
                {
                    tmpS += c.m_ShippingAddressID.ToString() + ",";
                }
            }
            return tmpS.Trim(',');
        }

        public string GetDistinctShippingAddressCounters(bool IncludeDownloadItems, bool IncludeSystemItems)
        {
            string tmpS = ",";
            foreach (CartItem c in CartItems)
            {
                if (tmpS.IndexOf("," + c.m_ShippingAddressCounter.ToString() + ",") == -1)
                {
                    if ((IncludeDownloadItems == c.IsDownload))
                    {
                        tmpS += c.m_ShippingAddressCounter.ToString() + ",";
                    }
                }
            }
            return tmpS.Trim(',');
        }

        public virtual Guid AddItem(Customer thisCustomer,
                                    string addressCode,
                                    string itemCode,
                                    int itemCounter,
                                    decimal quantity,
                                    string unitMeasureCode,
                                    CartTypeEnum type)
        {
            return AddItem(thisCustomer,
                        addressCode,
                        itemCode,
                        itemCounter,
                        quantity,
                        unitMeasureCode,
                        type,
                        null);
        }

        public virtual Guid AddItem(Customer thisCustomer,
                                    string addressCode,
                                    string itemCode,
                                    int itemCounter,
                                    decimal quantity,
                                    string unitMeasureCode,
                                    CartTypeEnum type,
                                    string notes)
        {
            return AddItem(thisCustomer,
                        addressCode,
                        itemCode,
                        itemCounter,
                        quantity,
                        unitMeasureCode,
                        type,
                        null,
                        null,
                        CartTypeEnum.ShoppingCart,
                        false,
                        notes);
        }

        public void BatchUpdateUnitMeasureForItem(List<KeyValuePair<int, string>> cartIdsAndunitMeasureCodes, bool multiShip)
        {
            StringBuilder updateShoppingCartUnitMeasureCommand = new StringBuilder();
            int cartId;
            string unitMeasureCode;
            foreach (KeyValuePair<int, string> cartIdAndunitMeasureCode in cartIdsAndunitMeasureCodes)
            {
                cartId = cartIdAndunitMeasureCode.Key;
                unitMeasureCode = cartIdAndunitMeasureCode.Value;

                // update ship address and recompute values based on new unitmeasure
                var currentCartItems = this.CartItems.Where(item => item.m_ShoppingCartRecordID == cartId).ToList();
                if (currentCartItems.Count == 0) { continue; }
                foreach (var item in currentCartItems)
                {
                    // create bacth SQL command
                    if (multiShip || item.GiftRegistryID.HasValue)
                    {
                        updateShoppingCartUnitMeasureCommand.AppendLine(string.Format("update EcommerceShoppingCart set UnitMeasureCode = {0} where ShoppingCartRecID={1} and CustomerCode={2};",
                                                                                DB.SQuote(unitMeasureCode),
                                                                                cartId,
                                                                                DB.SQuote(ThisCustomer.CustomerCode)));
                    }
                    else
                    {
                        updateShoppingCartUnitMeasureCommand.AppendLine(string.Format("update EcommerceShoppingCart set UnitMeasureCode = {0}, ShippingAddressID = {1} where ShoppingCartRecID={2} and CustomerCode={3};",
                                                                                DB.SQuote(unitMeasureCode),
                                                                                DB.SQuote(ThisCustomer.PrimaryShippingAddressID),
                                                                                cartId,
                                                                                DB.SQuote(ThisCustomer.CustomerCode)));
                    }

                    // recompute and assign shippingAddressID
                    if (!multiShip && item.GiftRegistryID.IsNullOrEmptyTrimmed() && item.ShippingAddressId != ThisCustomer.PrimaryShippingAddressID)
                    { item.ShippingAddressId = ThisCustomer.PrimaryShippingAddressID; }
                    if (item.UnitMeasureCode != unitMeasureCode)
                    {
                        item.UnitMeasureCode = unitMeasureCode;
                        item.RecomputeCartItemPrice();
                    }
                }

            }

            if (updateShoppingCartUnitMeasureCommand.Length == 0) { return; }
            DB.ExecuteSQL(updateShoppingCartUnitMeasureCommand.ToString());


        }

        public void UpdateUnitMeasureForItem(int cartId, string unitMeasureCode, bool multiShip, bool isRegistryItem = false)
        {
            string updateShoppingCartUnitMeasureCommand = string.Empty;
            if (multiShip || isRegistryItem)
            {
                updateShoppingCartUnitMeasureCommand = string.Format("update EcommerceShoppingCart set UnitMeasureCode = {0} where ShoppingCartRecID={1} and CustomerCode={2}",
                                                                        DB.SQuote(unitMeasureCode),
                                                                        cartId,
                                                                        DB.SQuote(ThisCustomer.CustomerCode));
            }
            else
            {
                updateShoppingCartUnitMeasureCommand = string.Format("update EcommerceShoppingCart set UnitMeasureCode = {0}, ShippingAddressID = {1} where ShoppingCartRecID={2} and CustomerCode={3}",
                                                                        DB.SQuote(unitMeasureCode),
                                                                        DB.SQuote(ThisCustomer.PrimaryShippingAddressID),
                                                                        cartId,
                                                                        DB.SQuote(ThisCustomer.CustomerCode));
            }
            DB.ExecuteSQL(updateShoppingCartUnitMeasureCommand);

            foreach (var cartItem in this.CartItems)
            {
                if (cartItem.m_ShoppingCartRecordID == cartId && cartItem.UnitMeasureCode != unitMeasureCode)
                {
                    cartItem.UnitMeasureCode = unitMeasureCode;
                    cartItem.RecomputeCartItemPrice();
                    break;
                }
            }

        }

        public virtual Guid AddItem(Customer thisCustomer,
                                    string addressCode,
                                    string itemCode,
                                    int itemCounter,
                                    decimal quantity,
                                    string unitMeasureCode,
                                    CartTypeEnum type,
                                    KitComposition preferredComposition,
                                    CartRegistryParam registryParam = null,
                                    CartTypeEnum cartOrigin = CartTypeEnum.ShoppingCart,
                                    bool isReorder = false,
                                    string notes = "",
                                    string warehouseCode = "", 
                                    string vdpDocumentCode = "")
        {
            var inventoryRepository = ServiceFactory.GetInstance<IInventoryRepository>();
            var appConfigurationService = ServiceFactory.GetInstance<IAppConfigService>();
            var shoppingCartService = ServiceFactory.GetInstance<IShoppingCartService>();

            Guid newCartID = Guid.NewGuid();
            string webSiteCode = appConfigurationService.WebSiteCode;

            string itemType = inventoryRepository.GetInventoryItemType(itemCode);

            bool isKit = (itemType == Const.ITEM_TYPE_KIT);

            if (isKit && null == preferredComposition) { return Guid.Empty; }
            if (Const.ITEM_TYPE_MATRIX_ITEM == itemType) { itemCounter = inventoryRepository.GetInventoryItemCounter(itemCode); }

            int RequiresCount = 0;

            if (type == CartTypeEnum.ShoppingCart && cartOrigin == CartTypeEnum.WishCart)
            {
                preferredComposition.CartID = Guid.Empty;
            }

            // No matching kit is found, but we are in edit mode, just re-compose
            bool isKitIsInEditMode = (preferredComposition != null &&
                                     preferredComposition.CartID != Guid.Empty);

            if (isKit && isKitIsInEditMode)
            {
                foreach (var currentCartItem in m_CartItems)
                {
                    if (currentCartItem.ItemType != Const.ITEM_TYPE_KIT && preferredComposition.CartID != currentCartItem.Id) { continue; }

                    decimal tempQty = quantity;

                    var composition = currentCartItem.GetKitComposition();
                    bool isSameComposition = composition.Matches(preferredComposition);
                    if (isSameComposition && preferredComposition.CartID != currentCartItem.Id)
                    {
                        shoppingCartService.ClearKitItems(preferredComposition.ItemKitCode, preferredComposition.CartID);
                        tempQty = currentCartItem.m_Quantity + quantity;
                    }
                    else
                    {
                        shoppingCartService.ClearKitItems(preferredComposition.ItemKitCode, preferredComposition.CartID);
                        //test if the current item is the same cartid
                        if (preferredComposition.CartID == currentCartItem.Id)
                        {
                            //force the cart item to be deleted
                            tempQty = 0;
                        }
                        else
                        {
                            //to avoid adding again to the database
                            if (composition.Compositions.Count() > 0)
                            {
                                preferredComposition.AddToCart();
                            }
                            else
                            {
                                //force the cart item to be deleted
                                tempQty = 0;
                            }
                        }
                    }

                    currentCartItem.SetQuantity(tempQty);
                    currentCartItem.m_Quantity = tempQty;

                    this.UpdateUnitMeasureForItem(currentCartItem.m_ShoppingCartRecordID, unitMeasureCode, true);
                    currentCartItem.UnitMeasureCode = unitMeasureCode;

                    currentCartItem.RecomputeCartItemPrice();

                    if (isReorder)
                    {
                        return currentCartItem.Id;
                    }
                }
            }

            //Anonymous customer 
            //Verify if the anonimous user add to cart again after skip login and continue.
            //All items will be Shipping Address will be assigned to the new address created by the user
            //Since use go back to add to cart again after checkout. Clear the addresss code to allow the cart item to be incremented.
            bool isAnonimousAddToCartAgainAfterSkipSigninAndContinue =
                                ThisCustomer.IsNotRegistered && !ThisCustomer.IsUnregisteredAnonymous &&
                                ThisCustomer.PrimaryShippingAddress != null &&
                                !ThisCustomer.PrimaryShippingAddress.AddressID.IsNullOrEmptyTrimmed();

            if (isAnonimousAddToCartAgainAfterSkipSigninAndContinue)
            {
                shoppingCartService.ClearEcommerceCartShippingAddressIDForAnonCustomer();
                addressCode = String.Empty;
            }

            LoadFromDB(CartType);

            for (int ctr = 0; ctr < m_CartItems.Count; ctr++)
            {
                var currentCartItem = m_CartItems[ctr];

                if (itemType == Const.ITEM_TYPE_KIT && preferredComposition != null)
                {
                    if (!currentCartItem.Matches(itemCode, addressCode, itemCounter, unitMeasureCode, preferredComposition)) continue;

                    var otherMatchingKitCartItems = new List<CartItem>();
                    for (int ctr2 = ctr + 1; ctr2 < m_CartItems.Count; ctr2++)
                    {
                        var otherKitCartItem = m_CartItems[ctr2];
                        if (otherKitCartItem.Matches(itemCode, addressCode, itemCounter, unitMeasureCode, preferredComposition))
                        {
                            otherMatchingKitCartItems.Add(otherKitCartItem);
                        }
                    }

                    if (!isKitIsInEditMode)
                    {
                        currentCartItem.IncrementQuantity(quantity);
                    }

                    foreach (var otherMatchingKitcartItem in otherMatchingKitCartItems)
                    {
                        this.RemoveItem(otherMatchingKitcartItem.m_ShoppingCartRecordID);
                    }

                    return currentCartItem.Id;
                }
                else
                {
                    if (!currentCartItem.Matches(itemCode, addressCode, itemCounter, unitMeasureCode)) continue;

                    currentCartItem.IncrementQuantity(quantity);

                    if (!warehouseCode.IsNullOrEmptyTrimmed())
                    {
                        ServiceFactory.GetInstance<IShoppingCartService>()
                                      .UpdateShoppingCartInStoreShippingInfo(currentCartItem.m_ShoppingCartRecordID,
                                                                             warehouseCode,
                                                                             currentCartItem.RealTimeRateId);
                    }

                    return currentCartItem.Id;
                }
            }

            string sku = String.Empty;
            string description = String.Empty;
            decimal price = Decimal.Zero;
            string dimensions = String.Empty; // formatted as: LxWxH
            decimal weight = Decimal.Zero;

            string sql = "dbo.GetEcommerceItemDetails {0}, {1}, {2}, {3}".FormatWith(itemCode.ToDbQuote(),
                                                                                    unitMeasureCode.ToDbQuote(),
                                                                                    ThisCustomer.LanguageCode.ToDbQuote(),
                                                                                    appConfigurationService.UserCode.ToDbQuote());
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var rs = DB.GetRSFormat(con, sql))
                {
                    if (rs.Read())
                    {
                        sku = AppLogic.MakeProperProductSKU(rs.ToRSField("SKU"), rs.ToRSField("SKUSuffix"), String.Empty, String.Empty);
                        description = DB.RSFieldByLocale(rs, "Name", m_ThisCustomer.LocaleSetting);
                        if (DB.RSFieldByLocale(rs, "VariantName", m_ThisCustomer.LocaleSetting).Length != 0)
                        {
                            if (DB.RSFieldByLocale(rs, "VariantName", m_ThisCustomer.LocaleSetting) != DB.RSFieldByLocale(rs, "Name", m_ThisCustomer.LocaleSetting))
                            {
                                description += " - " + DB.RSFieldByLocale(rs, "VariantName", m_ThisCustomer.LocaleSetting);
                            }
                            else
                            {
                                description += " - " + CommonLogic.Ellipses(DB.RSFieldByLocale(rs, "Description", m_ThisCustomer.LocaleSetting), 30, true);
                            }
                        }
                        else if (DB.RSFieldByLocale(rs, "Description", m_ThisCustomer.LocaleSetting).Length != 0)
                        {
                            description += " - " + CommonLogic.Ellipses(DB.RSFieldByLocale(rs, "Description", m_ThisCustomer.LocaleSetting), 30, true);
                        }
                        price = rs.ToRSFieldDecimal("Price");

                        decimal value = rs.ToRSFieldDecimal("SalePrice");
                        if (value != Decimal.Zero) { price = value; }

                        weight = rs.ToRSFieldDecimal("Weight");
                        decimal length, width, height;
                        length = rs.ToRSFieldDecimal("LengthInInches");
                        width = rs.ToRSFieldDecimal("WidthInInches");
                        height = rs.ToRSFieldDecimal("HeightInInches");

                        if (length != 0 && width != 0 && height != 0)
                        {
                            dimensions = "{0}x{1}x{2}".FormatWith(length.ToString(), width.ToString(), height.ToString());
                        }

                    }
                }
            }

            if (sku.Length != 0 || appConfigurationService.AllowEmptySkuAddToCart)
            {
                string tmpSQL = "select 0 as IsTaxable, 0 as IsShipSeparately, CASE WHEN ItemType = 'Electronic Download' THEN 1 ELSE 0 END AS IsDownload,";
                tmpSQL += "0 as FreeShipping, 1 as RecurringInterval, 3 as RecurringIntervalType, null as DownloadLocation, ";
                tmpSQL += "CASE WHEN ItemType = 'Service' THEN 1 ELSE 0 END AS IsSystem, 0 as IsAKit, 0 as IsAPack from InventoryItem with (NOLOCK) where counter=" + itemCounter.ToString();

                using (var con = DB.NewSqlConnection())
                {
                    con.Open();
                    using (var rst = DB.GetRSFormat(con, tmpSQL))
                    {
                        if (rst.Read())
                        {
                            int isTaxable = rst.ToRSFieldBool("IsTaxable").ToBit();
                            int isShipSeparately = rst.ToRSFieldBool("IsShipSeparately").ToBit();
                            int isDownload = rst.ToRSFieldBool("IsDownload").ToBit();
                            int FreeShipping = rst.ToRSFieldBool("FreeShipping").ToBit();
                            string downloadLocation = rst.ToRSField("DownloadLocation");
                            int RecurringInterval = rst.ToRSFieldInt("RecurringInterval");
                            int IsSystem = rst.ToRSFieldBool("IsSystem").ToBit();
                            int IsAKit = rst.ToRSFieldBool("IsAKit").ToBit();
                            int IsAPack = rst.ToRSFieldBool("IsAPack").ToBit();
                            if (RecurringInterval == 0)
                            {
                                RecurringInterval = 1; // for backwards compatability
                            }
                            var RecurringIntervalType = (DateIntervalTypeEnum)rst.ToRSFieldInt("RecurringIntervalType");
                            if (RecurringIntervalType == DateIntervalTypeEnum.Unknown)
                            {
                                RecurringIntervalType = DateIntervalTypeEnum.Monthly; // for backwards compatibility
                            }

                            // added the id to be used when adding kit items...
                            string NewGUID = newCartID.ToString();

                            StringBuilder sql2 = null;
                            if (registryParam != null)
                            {
                                GiftRegistryID = registryParam.RegistryID;
                                GiftRegistryItemCode = registryParam.RegistryItemCode;
                            }

                            bool isUseGiftRegistry = (registryParam != null && registryParam.RegistryID.HasValue && registryParam.RegistryItemCode.HasValue && appConfigurationService.GiftRegistryEnabled);

                            sql2 = new StringBuilder("insert into EcommerceShoppingCart(CartType,ShoppingCartRecGUID,CustomerCode,ShippingAddressID,ItemCode, ProductID,SubscriptionInterval,SubscriptionIntervalType,VariantID,ProductSKU,ProductPrice,CustomerEntersPrice,ProductWeight,ProductDimensions,Quantity,RequiresCount,ChosenColor,ChosenColorSKUModifier,ChosenSize,ChosenSizeSKUModifier,TextOption,IsTaxable,IsDownload,DownloadLocation,FreeShipping,RecurringInterval,RecurringIntervalType, IsSystem, IsAKit, IsAPack, UnitMeasureCode, WebsiteCode, KitPricingType,ContactCode,Notes,VDPDocumentCode_DEV004817");

                            if (isUseGiftRegistry) { sql2.Append(", GiftRegistryID, RegistryItemCode"); }

                            string storePickDescription = ServiceFactory.GetInstance<IShippingService>()
                                                                        .GetStorePickupShippingMethodDescription();

                            if (!warehouseCode.IsNullOrEmptyTrimmed() && !storePickDescription.IsNullOrEmptyTrimmed())
                            {
                                sql2.Append(", ShippingMethod, InStoreWarehouseCode");
                            }

                            sql2.Append(") values(");

                            sql2.Append(((int)CartType).ToString() + ",");
                            sql2.Append(NewGUID.ToDbQuote() + ",");
                            sql2.Append(m_ThisCustomer.CustomerCode.ToString().ToDbQuote() + ",");

                            if (addressCode.ToString().Length > 0)
                            {
                                sql2.Append(addressCode.ToDbQuote() + ",");
                            }
                            else
                            {
                                sql2.Append("NULL,");
                            }

                            sql2.Append(itemCode.ToDbQuote() + ",");
                            sql2.Append(itemCounter.ToString() + ",");

                            int IntVal = 0;
                            DateIntervalTypeEnum IntValType = DateIntervalTypeEnum.Monthly;
                            sql2.Append(IntVal.ToString() + ",");
                            sql2.Append(((int)IntValType).ToString() + ",");
                            sql2.Append(itemCounter.ToString() + ",");
                            sql2.Append(sku.ToDbQuote() + ",");
                            sql2.Append(Localization.CurrencyStringForDBWithoutExchangeRate(Decimal.Zero) + ",");
                            sql2.Append("0,");
                            sql2.Append(Localization.DecimalStringForDB(weight) + ",");
                            sql2.Append(dimensions.ToDbQuote() + ",");
                            sql2.Append(quantity.ToString() + ",");
                            sql2.Append(RequiresCount.ToString() + ",");
                            sql2.Append(String.Empty.ToDbQuote() + ",");
                            sql2.Append(String.Empty.ToDbQuote() + ",");
                            sql2.Append(String.Empty.ToDbQuote() + ",");
                            sql2.Append(String.Empty.ToDbQuote() + ",");
                            sql2.Append(String.Empty.ToDbQuote() + ",");
                            sql2.Append(isTaxable.ToString() + ",");
                            sql2.Append(isDownload.ToString() + ",");
                            sql2.Append(downloadLocation.ToDbQuote() + ",");
                            sql2.Append(FreeShipping.ToString() + ",");
                            sql2.Append(RecurringInterval.ToString() + ",");
                            sql2.Append(((int)RecurringIntervalType).ToString() + ",");
                            sql2.Append(IsSystem.ToString() + ",");
                            sql2.Append(IsAKit.ToString() + ",");
                            sql2.Append(IsAPack.ToString() + ",");
                            sql2.Append(unitMeasureCode.ToDbQuote() + ",");
                            sql2.Append(webSiteCode.ToDbQuote() + ",");

                            if (itemType == Const.ITEM_TYPE_KIT && null != preferredComposition)
                            {
                                sql2.Append(preferredComposition.PricingType.ToDbQuote() + ",");
                            }
                            else
                            {
                                sql2.Append("'', ");
                            }

                            sql2.Append(thisCustomer.ContactCode.ToDbQuote());

                            if (notes.IsNullOrEmptyTrimmed())
                            {
                                sql2.Append(",NULL");
                            }
                            else
                            {
                                sql2.Append("," + notes.ToDbQuote());
                            }

                            if (isUseGiftRegistry)
                            {
                                sql2.Append("," + registryParam.RegistryID.Value.ToString().ToDbQuote());
                                sql2.Append("," + registryParam.RegistryItemCode.Value.ToString().ToDbQuote());
                            }

                            if (!warehouseCode.IsNullOrEmptyTrimmed() && !storePickDescription.IsNullOrEmptyTrimmed())
                            {
                                sql2.Append("," + storePickDescription.ToDbQuote());
                                sql2.Append("," + warehouseCode.ToDbQuote());
                            }

                            sql2.Append("," + vdpDocumentCode.ToDbQuote());
                            sql2.Append(")");
                            DB.ExecuteSQL(sql2.ToString());
                        }
                    }
                }

                if (itemType == Const.ITEM_TYPE_KIT)
                {
                    preferredComposition.CartID = newCartID;
                    preferredComposition.AddToCart();
                }
            }

            return newCartID;
        }

        public virtual Guid AddItem(Customer thisCustomer,
                            string addressCode,
                            string itemCode,
                            int itemCounter,
                            decimal quantity,
                            string unitMeasureCode,
                            CartTypeEnum type,
                            KitComposition preferredComposition,
                            bool isReorder)
        {
            return AddItem(thisCustomer, addressCode, itemCode, itemCounter, quantity, unitMeasureCode, type, preferredComposition, null, CartTypeEnum.ShoppingCart, isReorder);
        }

        /// <summary>
        /// Use to support instore pickup add to cart in product page
        /// </summary>
        public virtual Guid AddItem(Customer thisCustomer,
                    string addressCode,
                    string itemCode,
                    int itemCounter,
                    decimal quantity,
                    string unitMeasureCode,
                    CartTypeEnum type,
                    KitComposition preferredComposition,
                    string wareHouseCode)
        {
            return AddItem(thisCustomer, addressCode, itemCode, itemCounter, quantity, unitMeasureCode, type, preferredComposition, null, CartTypeEnum.ShoppingCart, false, String.Empty, wareHouseCode);
        }
        
        public virtual void AddArtworkFeeItem()
        {
            string itemCode = string.Empty;
            int itemCounter = 0;
            using (var con = DB.NewSqlConnection())
            {
                con.Open();
                using (var reader = DB.GetRSFormat(con, "SELECT Counter, ItemCode FROM InventoryItem A with (NOLOCK) WHERE ItemName = {0}", DB.SQuote(AppLogic.AppConfig("custom.smartbag.artwork.fee.item"))))
                {
                    if (reader.Read())
                    {
                        itemCounter = reader.ToRSFieldInt("Counter");
                        itemCode = reader.ToRSField("ItemCode");
                    }
                }
            }

            if (itemCode.Length > 0)
            {
                var itemDefaultUM = InterpriseSuiteEcommerceCommon.InterpriseHelper.GetItemDefaultUnitMeasure(itemCode);
                AddItem(ThisCustomer, ThisCustomer.PrimaryShippingAddressID, itemCode, itemCounter, 1, itemDefaultUM.Code, CartTypeEnum.ShoppingCart);
            }
        }

        public virtual void SetItemQuantity(int cartRecordID, decimal Quantity)
        {
            //Do not update quantity if item is shipping itemcode
            var shippingItemCode = AppLogic.GetShippingItemCode();
            var artworkItemCode = AppLogic.GetArtworkFeeItemCode();
            //var item = m_CartItems.Where(m => m.m_ShoppingCartRecordID == cartRecordID);
            CartItem item;
            //Handle if item is already deleted. This happens on Artwork Fee item.
            try
            {
                item = m_CartItems.First(m => m.m_ShoppingCartRecordID == cartRecordID);
            }
            catch{return;}
            
            if (shippingItemCode == item.ItemCode || artworkItemCode == item.ItemCode)
            {
                return;
            }

            Quantity = CommonLogic.RoundQuantity(Quantity);
            if (Quantity == 0)
            {
                RemoveItem(cartRecordID);
                if (item.IsArtworkItem)
                {
                    item = m_CartItems.First(m => m.ItemCode == artworkItemCode);
                    SetArtworkFeeItemQuantity(item.m_ShoppingCartRecordID, item.m_Quantity - 1);
                }
            }
            else
            {
                for (int i = 0; i < m_CartItems.Count; i++)
                {
                    if (((CartItem)m_CartItems[i]).m_ShoppingCartRecordID == cartRecordID)
                    {
                        CartItem ci = (CartItem)m_CartItems[i];
                        bool recomputePrice = (ci.m_Quantity != Quantity);
                        ci.m_Quantity = Quantity;
                        if (recomputePrice)
                        {
                            string sql = "UPDATE EcommerceShoppingCart SET Quantity=" + Localization.ParseDBDecimal(Quantity) + " WHERE ShoppingCartRecID=" +
                                          cartRecordID.ToString() + " AND CustomerCode=" + DB.SQuote(m_ThisCustomer.CustomerCode.ToString()) + " and ContactCode=" +
                                          DB.SQuote(m_ThisCustomer.ContactCode.ToString());
                            DB.ExecuteSQL(sql);
                            ci.RecomputeCartItemPrice();
                        }
                        m_CartItems[i] = ci;
                        break;
                    }
                }
            }
            return;
        }

        public virtual void SetArtworkFeeItemQuantity(int cartRecordID, decimal Quantity)
        {
            Quantity = CommonLogic.RoundQuantity(Quantity);
            if (Quantity == 0)
            {
                RemoveItem(cartRecordID);
            }
            else
            {
                var item = m_CartItems.First(m => m.m_ShoppingCartRecordID == cartRecordID);
                bool recomputePrice = (item.m_Quantity != Quantity);
                item.m_Quantity = Quantity;
                if (recomputePrice)
                {
                    string sql = "UPDATE EcommerceShoppingCart SET Quantity=" + Localization.ParseDBDecimal(Quantity) + " WHERE ShoppingCartRecID=" +
                                  cartRecordID.ToString() + " AND CustomerCode=" + DB.SQuote(m_ThisCustomer.CustomerCode.ToString()) + " and ContactCode=" +
                                  DB.SQuote(m_ThisCustomer.ContactCode.ToString());
                    DB.ExecuteSQL(sql);
                    item.RecomputeCartItemPrice();
                }
            }
            return;
        }

        public List<GlobalConfig> SetItemQuantity(InterpriseIntegration.InterpriseShoppingCart cart, int CartRecID, decimal Quantity)
        {
            var listConfiguration = new List<GlobalConfig>();
            string couponCode = string.Empty;
            string couponErrorMessage = string.Empty;
            decimal itemSubTotal = 0, unitPrice = 0;

            bool hasCoupon = cart.HasCoupon(ref couponCode);
            if (hasCoupon && !cart.IsCouponValid(ThisCustomer, couponCode, ref couponErrorMessage))
            {
                couponCode = "";
            }
            cart.BuildSalesOrderDetails(false, true, couponCode, true);
            decimal subTotal = 0; 
            for (int i = 0; i < m_CartItems.Count; i++)
            {
                if (((CartItem)m_CartItems[i]).m_ShoppingCartRecordID == CartRecID)
                {
                    CartItem ci = (CartItem)m_CartItems[i];
                    var coupondiscount = (ci.DiscountAmountAlreadyComputed) ? ci.CouponDiscount : cart.GetCartItemCouponDiscount(ci);
                    if (coupondiscount > Decimal.Zero)
                    {
                        unitPrice = ci.CouponDiscount;
                    }
                    else
                    {
                        unitPrice = ci.UnitPrice;
                    }
                    //Differentiate express bag computation from white bag and sticker bundles
                    if (ci.ItemType == "Kit" && (ci.CategoryCode != "White bag and sticker bundles" && ci.CategoryCode != "Brown bag and sticker bundles"))
                    {
                        itemSubTotal = unitPrice * (Quantity / ci.UnitMeasureQty);
                    }
                    else
                    {
                        itemSubTotal = unitPrice * Quantity;
                    }
                    // recalculate tax rate for item based on discounted price
                    if (coupondiscount > Decimal.Zero && ci.TaxRate > Decimal.Zero)
                    {
                        ci.TaxRate = (itemSubTotal * (ci.TaxRate / Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(ci.Price)));
                    }
                    
                    //ci.RecomputeCartItemPrice();
                    //m_CartItems[i] = ci;
                    //itemSubTotal = ci.Price;
                    ////Differentiate express bag computation from white bag and sticker bundles
                    //if (ci.ItemType == "Kit" && (ci.CategoryCode != "White bag and sticker bundles" && ci.CategoryCode != "Brown bag and sticker bundles"))
                    //{
                    //    unitPrice = ci.Price / (Quantity / ci.UnitMeasureQty);
                    //}
                    //else
                    //{
                    //    unitPrice = (ci.Price / Quantity);
                    //}
                    //break;
                    string numItems = Localization.ParseLocaleDecimal(cart.NumItems(), ThisCustomer.LocaleSetting);
                    //Dont format returned value of it will not set properly for select option dropdown
                    listConfiguration.Add(new GlobalConfig("Quantity", Quantity.ToString()));
                    listConfiguration.Add(new GlobalConfig("CartItems", numItems));
                    listConfiguration.Add(new GlobalConfig("UnitPrice", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(unitPrice).ToCustomerCurrency()));
                    listConfiguration.Add(new GlobalConfig("ItemSubTotal", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(itemSubTotal).ToCustomerCurrency()));
                }
                else
                {
                    CartItem ci = (CartItem)m_CartItems[i];
                    var coupondiscount = (ci.DiscountAmountAlreadyComputed) ? ci.CouponDiscount : cart.GetCartItemCouponDiscount(ci);
                    if (coupondiscount > Decimal.Zero)
                    {
                        unitPrice = ci.CouponDiscount;
                    }
                    else
                    {
                        unitPrice = ci.UnitPrice;
                    }
                    //Differentiate express bag computation from white bag and sticker bundles
                    if (ci.ItemType == "Kit" && (ci.CategoryCode != "White bag and sticker bundles" && ci.CategoryCode != "Brown bag and sticker bundles"))
                    {
                        itemSubTotal = unitPrice * (ci.m_Quantity / ci.UnitMeasureQty);
                    }
                    else
                    {
                        itemSubTotal = unitPrice * ci.m_Quantity;
                    }
                    // recalculate tax rate for item based on discounted price
                    if (coupondiscount > Decimal.Zero && ci.TaxRate > Decimal.Zero)
                    {
                        ci.TaxRate = (itemSubTotal * (ci.TaxRate / Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(ci.Price)));
                    }
                }
                subTotal += itemSubTotal;
            }

            //listConfiguration.Add(new GlobalConfig("Quantity", Localization.ParseLocaleDecimal(Quantity, ThisCustomer.LocaleSetting)));
            //decimal subTotal = cart.GetCartSubTotal();
            listConfiguration.Add(new GlobalConfig("SubTotal", subTotal.ToCustomerCurrency()));

            decimal total = 0, totalTax = 0;
            decimal freightRate = 0;
            // compute freight tax
            var availableShippingMethods = cart.GetShippingMethods(ThisCustomer.PrimaryShippingAddress, "", this.FirstItem().GiftRegistryID);
            if (availableShippingMethods != null && availableShippingMethods.Count > 0)
            {
                for (int i = 0; i < availableShippingMethods.Count; i++)
                {
                    if (i == 0 || availableShippingMethods[i].ForOversizedItem)
                    { freightRate += availableShippingMethods[i].Freight; }
                }
            }

            //freightRate = ThisCustomer.ThisCustomerSession["FreightRate"].ToDecimal();
            decimal freightTax = cart.GetCartFreightRateTax(ThisCustomer.CurrencyCode, freightRate, ThisCustomer.FreightTaxCode, ThisCustomer.PrimaryShippingAddress);
            // get item tax
            decimal itemTax = Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(cart.GetCartTaxTotal());
            if (CommonLogic.GetWebsiteType() == WebsiteType.Smartbag)
            {
                totalTax = Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency((itemTax + freightTax), ThisCustomer.CurrencyCode);
                total = subTotal + totalTax + freightRate;
            }
            else if (CommonLogic.GetWebsiteType() == WebsiteType.Company)
            {
                totalTax = Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency((itemTax + freightTax), ThisCustomer.CurrencyCode);
                total = subTotal + totalTax + freightRate;
            }
            listConfiguration.Add(new GlobalConfig("Freight", freightRate.ToCustomerCurrency()));
            listConfiguration.Add(new GlobalConfig("VAT", totalTax.ToCustomerCurrency()));
            listConfiguration.Add(new GlobalConfig("GrandTotal", total.ToCustomerCurrency()));
            return listConfiguration;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CartRecID">ShoppingcartRecID of an item on EcommerceShoppingCart table</param>
        /// <param name="IsAdd"></param>
        /// <returns></returns>
        public List<GlobalConfig> UpdateQuantityToCartPortal(InterpriseIntegration.InterpriseShoppingCart cart, int CartRecID, bool IsAdd)
        {
            var listConfiguration = new List<GlobalConfig>();
            string sql = string.Empty;
            decimal quantity = 0, itemSubTotal = 0;

            if (IsAdd)
            {
                sql = string.Format("UPDATE EcommerceShoppingCart SET Quantity = Quantity + 1 WHERE ShoppingCartRecID = {0}", CartRecID);
            }
            else
            {
                sql = string.Format("UPDATE EcommerceShoppingCart SET Quantity = Quantity - 1 WHERE ShoppingCartRecID = {0}", CartRecID);
            }
            DB.ExecuteSQL(sql);

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader reader = DB.GetRSFormat(con, "SELECT Quantity FROM EcommerceShoppingCart WHERE ShoppingCartRecID = {0}", CartRecID))
                {
                    if (reader.Read())
                    {
                        quantity = reader.ToRSFieldDecimal("Quantity");
                    }
                }
            }
            if (quantity < 1)
            {
                sql = string.Format("UPDATE EcommerceShoppingCart SET Quantity = 1 WHERE ShoppingCartRecID = {0}", CartRecID);
                DB.ExecuteSQL(sql);
                quantity = 1;
            }

            for (int i = 0; i < m_CartItems.Count; i++)
            {
                if (((CartItem)m_CartItems[i]).m_ShoppingCartRecordID == CartRecID)
                {
                    CartItem ci = (CartItem)m_CartItems[i];
                    ci.m_Quantity = quantity;
                    ci.RecomputeCartItemPrice();
                    m_CartItems[i] = ci;
                    itemSubTotal = ci.Price;

                    break;
                }
            }

            string numItems = Localization.ParseLocaleDecimal(ShoppingCart.NumItems(ThisCustomer.CustomerID, CartTypeEnum.ShoppingCart, ThisCustomer.ContactCode), ThisCustomer.LocaleSetting);
            listConfiguration.Add(new GlobalConfig("Quantity", Localization.ParseLocaleDecimal(quantity, ThisCustomer.LocaleSetting)));
            listConfiguration.Add(new GlobalConfig("CartItems", numItems));
            listConfiguration.Add(new GlobalConfig("ItemSubTotal", Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(itemSubTotal).ToCustomerCurrency()));
            decimal subTotal = cart.GetCartSubTotal();
            listConfiguration.Add(new GlobalConfig("SubTotal", subTotal.ToCustomerCurrency()));
            // compute freight tax
            decimal freightRate = ThisCustomer.ThisCustomerSession["FreightRate"].ToDecimal();
            decimal freightTax = cart.GetCartFreightRateTax(ThisCustomer.CurrencyCode, freightRate, ThisCustomer.FreightTaxCode, ThisCustomer.PrimaryShippingAddress);
            //Set Freight to zero for Portal website
            if (CommonLogic.GetWebsiteType() == WebsiteType.Company)
            {
                freightTax = 0;
                freightRate = 0;
            }
            // get item tax
            decimal itemTax = Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency(cart.GetCartTaxTotal());
            decimal totalTax = Interprise.Facade.Base.SimpleFacade.Instance.RoundCurrency((itemTax + freightTax), ThisCustomer.CurrencyCode);
            listConfiguration.Add(new GlobalConfig("VAT", totalTax.ToCustomerCurrency()));
            decimal total = subTotal + totalTax + freightRate;
            listConfiguration.Add(new GlobalConfig("GrandTotal", total.ToCustomerCurrency()));

            return listConfiguration;
        }

        public void ShipAllItemsToThisAddress(Address shippingAddress)
        {
            StringBuilder sqlCommandText = new StringBuilder();
            //Exclude the updating of addressid if registry item
            this.CartItems.Where(item => !item.GiftRegistryID.HasValue)
                          .AsParallel()
                          .ForEach(itm =>
                          {
                              sqlCommandText.Append(
                                  string.Format("UPDATE EcommerceShoppingCart SET ShippingAddressID= {0}, ContactCode = {1} WHERE ShoppingCartRecID= {2} AND CustomerCode= {3}; ", DB.SQuote(shippingAddress.AddressID), DB.SQuote(ThisCustomer.ContactCode), itm.m_ShoppingCartRecordID.ToString(), DB.SQuote(m_ThisCustomer.CustomerCode))
                                  );
                          });
            if (sqlCommandText.Length > 0) { DB.ExecuteSQL(sqlCommandText.ToString()); }
            return;
        }

        public void SetItemAddress(int cartRecordID, string AddressID)
        {
            string sql = string.Format("UPDATE EcommerceShoppingCart SET ShippingAddressID= {0}, ContactCode = {1} WHERE ShoppingCartRecID= {2} AND CustomerCode= {3}", DB.SQuote(AddressID), DB.SQuote(ThisCustomer.ContactCode), cartRecordID.ToString(), DB.SQuote(m_ThisCustomer.CustomerCode));
            DB.ExecuteSQL(sql);
            return;
        }


        public virtual void SetItemNotes(Guid cartItemId, string notes, bool refreshCartItems = false)
        {
            if (!notes.IsNullOrEmptyTrimmed())
            {
                bool hasItem = false;
                for (int i = 0; i < m_CartItems.Count; i++)
                {
                    if (((CartItem)m_CartItems[i]).Id == cartItemId)
                    {
                        CartItem ci = (CartItem)m_CartItems[i];
                        ci.SetLineItemNotes(notes);
                        m_CartItems[i] = ci;
                        hasItem = true;
                        break;
                    }
                }
                if (!hasItem && !refreshCartItems)
                {
                    this.LoadFromDB(this.CartType);
                    SetItemNotes(cartItemId, notes, true);
                }
            }
            return;
        }

        public void RemoveItem(int cartRecordID)
        {
            if (cartRecordID == 0) return;

            var shoppingCartService = ServiceFactory.GetInstance<IShoppingCartService>();
            var shoppingCartModel = shoppingCartService.GetEcommerceShoppingCartByCartRecordId(cartRecordID);

            if (shoppingCartModel == null) return;
            string itemCode = shoppingCartModel.ItemCode;
            decimal quantity = shoppingCartModel.Quantity;
            int RequiresCount = shoppingCartModel.RequiresCount;
            CartTypeEnum cartType = shoppingCartModel.CartType;

            shoppingCartService.ClearLineItems(new String[] { shoppingCartModel.ShoppingCartRecGuid.ToString() });
            m_CartItems.RemoveAt(m_CartItems.FindIndex(p => p.m_ShoppingCartRecordID == cartRecordID));

            //remove all order options if those are the only items left in the cart
            bool blnRemoveAllOrderOptions = (m_CartItems.Count() > 0) && !m_CartItems.Any(m => !m.IsCheckoutOption);
            if (blnRemoveAllOrderOptions && cartType != CartTypeEnum.WishCart)
            {
                //the only items in the cart are order options, so delete everything
                shoppingCartService.ClearCartByLoggedInCustomerCartType(cartType);

                m_CartItems.Clear();

                //remove all reserved
                shoppingCartService.ClearCartReservationByCurrentlyLoggedInCustomer();
            }

            //remove records with non-existing shoppingcart record (ShoppingCartRecID)
            shoppingCartService.CleanupShoppingCartGiftEmail();

            //cleanup applied amount for otherpayment options
            if (m_CartItems.Count == 0)
            {
                shoppingCartService.ClearAppliedCreditMemos();
                shoppingCartService.ClearAppliedLoyaltyPoints();
                shoppingCartService.ClearAppliedGiftCodes();
            }
        }

        public bool IsEmpty()
        {
            return (m_CartItems.Count == 0);
        }

        public void ClearContents()
        {
            m_CartItems.Clear();
            DB.ExecuteSQL(string.Format("DELETE FROM EcommerceKitCart WHERE CartID IN (SELECT ShoppingCartRecGUID FROM EcommerceShoppingCart WHERE CartType={0} AND WebSiteCode={1} AND ContactCode={2} AND CustomerCode={3})", ((int)m_CartType).ToString(), DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode), DB.SQuote(m_ThisCustomer.ContactCode), DB.SQuote(m_ThisCustomer.CustomerCode)));
            DB.ExecuteSQL(string.Format("DELETE FROM EcommerceShoppingCart WHERE CartType={0} AND WebSiteCode={1} AND ContactCode={2} AND CustomerCode={3}", ((int)m_CartType).ToString(), DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode), DB.SQuote(m_ThisCustomer.ContactCode), DB.SQuote(m_ThisCustomer.CustomerCode)));
        }

        public virtual bool HasCoupon()
        {
            throw new NotSupportedException("Use InterpriseShopping Cart's override for the implementation");
        }

        // returns true if this cart has any items which are download items:
        public bool HasDownloadComponents()
        {
            return m_CartItems.Count(item => item.IsDownload == true) > 0;
        }

        // returns true if this cart has any items which are service items:
        public bool HasServiceComponents()
        {
            return m_CartItems.Count(item => item.IsService == true) > 0;
        }

        // returns true if this cart has any items which are download goods:
        public bool HasSupplierDropShipComponents()
        {
            return m_CartItems.Count(item => item.IsDropShip == true && !item.SupplierCode.IsNullOrEmptyTrimmed()) > 0;
        }

        // returns true if this order has ONLY download, service items, gift card items, and gift certificate items:
        public bool IsNoShippingRequired()
        {
            return m_CartItems.Count(item => item.IsDownload == false &&
                   item.ItemType != Interprise.Framework.Base.Shared.Const.ITEM_TYPE_SERVICE &&
                   item.ItemType != Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CARD &&
                   item.ItemType != Interprise.Framework.Base.Shared.Const.ITEM_TYPE_GIFT_CERTIFICATE) == 0;
        }

        public int NumAtThisShippingAddress(string ShippingAddressID)
        {
            int i = 0;
            foreach (CartItem c in m_CartItems)
            {
                if (c.m_ShippingAddressID == ShippingAddressID)
                {
                    i++;
                }
            }
            return i;
        }

        public bool IsAllDefaultShippingAddressItems()
        {
            foreach (CartItem c in m_CartItems)
            {
                if (!(c.m_ShippingAddressID == string.Empty || c.m_ShippingAddressID == ThisCustomer.PrimaryShippingAddressID))
                {
                    return false;
                }
            }
            return true;
        }

        public CartItem FirstItem()
        {
            // don't call this on an empty cart, or you will get an exception (duh)
            return ((CartItem)CartItems[0]);
        }

        public string FirstItemShippingAddressID()
        {
            string ID = string.Empty;
            try
            {
                if (FirstItem().GiftRegistryID.HasValue) ID = ThisCustomer.PrimaryShippingAddressID;
                else ID = FirstItem().m_ShippingAddressID;
            }
            catch
            { }
            return ID;
        }

        public bool HasPrimaryShippingAddressItems()
        {
            foreach (CartItem c in m_CartItems)
            {
                if (c.m_ShippingAddressID == ThisCustomer.PrimaryShippingAddressID)
                {
                    return true;
                }
            }
            return false;
        }

        public decimal NumItems()
        {
            var artworkItem = AppLogic.AppConfig("custom.smartbag.artwork.fee.item");
            decimal tmp = 0;
            if (CommonLogic.GetWebsiteType() == WebsiteType.Smartbag)
            {
                tmp = m_CartItems.Count(item => item.ItemName != artworkItem);
            }
            else if (CommonLogic.GetWebsiteType() == WebsiteType.Company)
            {
                tmp = m_CartItems.Sum(item => item.m_Quantity);
            }
            //if (m_CartItems.Count() > 0)
            //{
            //    foreach(CartItem c in m_CartItems)
            //    {
            //        if (c.ItemName != artworkItem)
            //        {
            //            if (c.ItemType == "Kit")
            //            {
            //                tmp += c.m_Quantity / c.UnitMeasureQty;
            //            }
            //            else
            //            {
            //                tmp += c.m_Quantity;
            //            }
            //        }
            //    }
            //}
            return tmp;
        }

        public bool HasNonPrimaryShippingAddressItems()
        {
            foreach (CartItem c in m_CartItems)
            {
                if (c.m_ShippingAddressID != ThisCustomer.PrimaryShippingAddressID)
                {
                    return true;
                }
            }
            return false;
        }

        public bool HasMultipleShippingAddresses()
        {
            string LastAddressID = string.Empty;
            bool first = true;
            foreach (CartItem c in m_CartItems)
            {
                if (first)
                {
                    LastAddressID = c.m_ShippingAddressID;
                }
                else
                {
                    if (c.m_ShippingAddressID != LastAddressID)
                    {
                        return true;
                    }
                }
                first = false;
            }
            return false;
        }

        // NOT updated for multi-ship or gift registry (yet)
        public static string DisplayMiniCart(System.Collections.Generic.Dictionary<string, EntityHelper> EntityHelpers, Customer ThisCustomer, int m_SkinID, bool IncludeFrame)
        {
            string ImgFilename = string.Empty;
            bool existing = false;
            bool exists = false;

            InterpriseIntegration.InterpriseShoppingCart cart = new InterpriseIntegration.InterpriseShoppingCart(EntityHelpers, m_SkinID, ThisCustomer, CartTypeEnum.ShoppingCart, string.Empty, false, true);
            if (cart.IsEmpty())
            {
                return string.Empty;
            }

            StringBuilder tmpS = new StringBuilder(10000);

            if (IncludeFrame)
            {
                tmpS.Append("<table width=\"150\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-style: solid; border-width: 0px; border-color: #" + AppLogic.AppConfig("HeaderBGColor") + "\">\n");
                tmpS.Append("<tr><td align=\"left\" valign=\"top\">\n");
                tmpS.Append("<img src=\"" + AppLogic.LocateImageURL("skins/Skin_" + m_SkinID.ToString() + "/images/minicart.gif") + "\" border=\"0\"><br>");
                tmpS.Append("<table width=\"190\" cellpadding=\"4\" cellspacing=\"0\" border=\"0\" style=\"" + AppLogic.AppConfig("BoxFrameStyle") + "\">\n");
                tmpS.Append("<tr><td align=\"center\" valign=\"top\">\n");
            }

            bool ShowLinkBack = AppLogic.AppConfigBool("LinkToProductPageInCart");
            foreach (CartItem c in cart.m_CartItems)
            {
                if (ShowLinkBack)
                {
                    switch (c.ItemType)
                    {
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_GROUP:
                        case Interprise.Framework.Base.Shared.Const.ITEM_TYPE_MATRIX_ITEM:
                            using (SqlConnection con = DB.NewSqlConnection())
                            {
                                con.Open();
                                using (IDataReader reader = DB.GetRSFormat(con, string.Format("SELECT ItemCode FROM InventoryMatrixItem with (NOLOCK) WHERE MatrixItemCode = {0}", DB.SQuote(c.ItemCode))))
                                {
                                    if (reader.Read())
                                    {
                                        tmpS.Append("<a href=\"" + InterpriseHelper.MakeItemLink(DB.RSField(reader, "ItemCode")) + "\">");
                                    }
                                }
                            }
                            break;
                        default:
                            tmpS.Append("<a href=\"" + InterpriseHelper.MakeItemLink(c.ItemCode) + "\">");
                            break;
                    }
                }
                if (AppLogic.AppConfigBool("ShowPicsInMiniCart"))
                {
                    string ProdPic = string.Empty;
                    ImgFilename = AppLogic.NoPictureImageURL(true, ThisCustomer.SkinID, ThisCustomer.LocaleSetting);
                    using (SqlConnection con = DB.NewSqlConnection())
                    {
                        con.Open();
                        using (IDataReader reader = DB.GetRSFormat(con, "SELECT Filename FROM InventoryOverrideImage with (NOLOCK) WHERE ItemCode = {0} AND WebSiteCode = {1} AND IsDefaultIcon = 1", DB.SQuote(c.ItemCode), DB.SQuote(InterpriseHelper.ConfigInstance.WebSiteCode)))
                        {
                            existing = reader.Read();
                            if (existing)
                            {
                                ImgFilename = (DB.RSField(reader, "Filename"));
                            }
                        }
                    }

                    //use filename instead of product counter. will be applicable on 5.5.1 onwards                        
                    ProdPic = AppLogic.LocateImageFilenameUrl("Product", c.ItemCode.ToString(), "icon", ImgFilename, AppLogic.AppConfigBool("Watermark.Enabled"), out exists);


                    int MaxWidth = AppLogic.AppConfigNativeInt("MiniCartMaxIconWidth");
                    if (MaxWidth == 0)
                    {
                        MaxWidth = 125;
                    }
                    int MaxHeight = AppLogic.AppConfigNativeInt("MiniCartMaxIconHeight");
                    if (MaxHeight == 0)
                    {
                        MaxHeight = 125;
                    }
                    if (ProdPic.Length != 0)
                    {
                        //Additional img prooerties...
                        string seTitle = "";
                        string seAltText = "";
                        AppLogic.GetSEImageAttributes(c.ItemCode.ToString(), "ICON", AppLogic.GetLanguageCode(ThisCustomer.LocaleSetting), ref seTitle, ref seAltText);

                        System.Drawing.Size size = CommonLogic.GetImagePixelSize(ProdPic);
                        if (size.Width > MaxWidth)
                        {
                            tmpS.Append("<img align=\"center\" src=\"" + ProdPic + "\" width=\"" + MaxWidth.ToString() + "\" border=\"0\" alt=\"" + seAltText + "\" title=\"" + seTitle + "\"><br>");
                        }
                        else if (size.Height > MaxHeight)
                        {
                            tmpS.Append("<img align=\"center\" src=\"" + ProdPic + "\" height=\"" + MaxHeight + "\" border=\"0\" alt=\"" + seAltText + "\" title=\"" + seTitle + "\"><br>");
                        }
                        else
                        {
                            tmpS.Append("<img align=\"center\" src=\"" + ProdPic + "\" border=\"0\" alt=\"" + seAltText + "\" title=\"" + seTitle + "\"><br>");
                        }
                    }
                }

                tmpS.Append(HttpUtility.HtmlEncode(c.DisplayName));
                if (ShowLinkBack)
                {
                    tmpS.Append("</a>");
                }
                tmpS.Append("<br>");

                tmpS.Append("Qty " + Localization.ParseLocaleDecimal(c.m_Quantity, ThisCustomer.LocaleSetting) + "&nbsp;" + c.Price.ToCustomerCurrency()); //.CurrencyString(PR));
                if (AppLogic.AppConfigBool("VAT.Enabled"))
                {
                    if (ThisCustomer.VATSettingReconciled == VatDefaultSetting.Inclusive)
                    {
                        tmpS.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.38") + "</span>\n");
                    }
                    else
                    {
                        tmpS.Append(" <span class=\"VATLabel\">" + AppLogic.GetString("showproduct.aspx.37") + "</span>\n");
                    }
                }
                tmpS.Append("<br/><br/>");
            }

            tmpS.Append(AppLogic.GetString("shoppingcart.cs.26") + " " + cart.GetCartSubTotal().ToCustomerCurrency() + "<br/><br/>");

            tmpS.Append("<a href=\"ShoppingCart.aspx\"><font color=\"BLUE\"><b>" + AppLogic.GetString("shoppingcart.cs.21") + "</b></font></a>");

            if (IncludeFrame)
            {
                tmpS.Append("</td></tr>\n");
                tmpS.Append("</table>\n");
                tmpS.Append("</td></tr>\n");
                tmpS.Append("</table>\n");
            }

            return tmpS.ToString();
        }

        public CartTypeEnum CartType
        {
            get
            {
                return m_CartType;
            }
            set
            {
                m_CartType = value;
            }
        }

        public bool ShippingIsFree
        {
            get
            {
                return m_ShippingIsFree;
            }
            set
            {
                m_ShippingIsFree = value;
            }
        }

        public bool CouponsAllowed
        {
            get
            {
                return m_CouponsAllowed;
            }
            set
            {
                m_CouponsAllowed = value;
            }
        }

        public bool CartAllowsShippingMethodSelection
        {
            get
            {
                return !this.IsNoShippingRequired();
            }
        }

        public string OrderNotes
        {
            get
            {
                return m_OrderNotes;
            }
        }

        public Dictionary<string, EntityHelper> EntityHelpers
        {
            get
            {
                return m_EntityHelpers;
            }
        }

        public bool InventoryTrimmed
        {
            get
            {
                return m_InventoryTrimmed;
            }
        }

        public bool HasNoStockPhasedOutItem
        {
            get
            {
                return m_HasNoStockPhasedOutItem;
            }
        }

        public bool HaNoStockAndNoOpenPOItem
        {
            get
            {
                return m_HasNoStockAndNoOpenPOItem;
            }
        }

        public bool MinimumQuantitiesUpdated
        {
            get
            {
                return m_MinimumQuantitiesUpdated;
            }
        }

        public decimal FreeShippingThreshold
        {
            get
            {
                return m_FreeShippingThreshold;
            }
        }

        public decimal MoreNeededToReachFreeShipping
        {
            get
            {
                return m_MoreNeededToReachFreeShipping;
            }
        }

        public string EMail
        {
            get
            {
                return m_EMail;
            }
        }

        public CartItemCollection CartItems
        {
            get
            {
                return m_CartItems;
            }
            set
            {
                m_CartItems = value;
            }
        }

        public Customer ThisCustomer
        {
            get
            {
                return m_ThisCustomer;
            }
        }

        public int SkinID
        {
            get
            {
                return m_SkinID;
            }
        }

        public string OriginalRecurringOrderNumber
        {
            get
            {
                return m_OriginalRecurringOrderNumber;
            }
        }

        public CouponStruct Coupon
        {
            get
            {
                return m_Coupon;
            }
            set
            {
                m_Coupon = value;
            }
        }

        public int IsShipSeparatelyCount()
        {
            int ret = 0;
            foreach (CartItem item in m_CartItems)
            {
                if (item.CanShipSeparately)
                    ret++;
            }
            return ret;
        }

        public bool HasRegistryItems()
        {
            if (m_CartItems == null || m_CartItems.Count() == 0) return false;
            return (m_CartItems.Any(itm => itm.GiftRegistryID.HasValue));
        }

        public bool HasOversizedItems()
        {
            if (m_CartItems == null || m_CartItems.Count() == 0) return false;
            return (m_CartItems.Any(item => item.IsOverSized));
        }

        #region Credit Memos, Loyalty Points, Gift Codes

        public bool IsGiftCodeEnabled
        {
            get { return ServiceFactory.GetInstance<IAppConfigService>().GiftCodeEnabled; }
        }
        public bool IsLoyaltyPointsEnabled
        {
            get { return ServiceFactory.GetInstance<IAppConfigService>().LoyaltyPointsEnabled; }
        }
        public bool IsCreditMemosEnabled
        {
            get { return ServiceFactory.GetInstance<IAppConfigService>().CreditRedemptionIsEnabled; }
        }
        public IEnumerable<GiftCodeCustomModel> GiftCodesApplied
        {
            get { return _currentGiftCodes; }
        }
        public IEnumerable<CustomerCreditCustomModel> CreditMemos
        {
            get { return _currentCreditMemos; }
        }
        public decimal GiftCodesTotalAmountApplied
        {
            get
            {
                return _currentGiftCodes.Sum(code => code.AmountApplied);
            }
        }
        public decimal GiftCardsTotalAmountApplied
        {
            get
            {
                return _currentGiftCodes.Where(i => i.Type.EqualsIgnoreCase(Const.ITEM_TYPE_GIFT_CARD)).Sum(i => i.AmountApplied);
            }
        }
        public decimal GiftCertsTotalAmountApplied
        {
            get
            {
                return _currentGiftCodes.Where(i => i.Type.EqualsIgnoreCase(Const.ITEM_TYPE_GIFT_CERTIFICATE)).Sum(i => i.AmountApplied);
            }
        }
        public decimal GiftCardsTotalCreditAllocated { get; set; }
        public decimal GiftCertsTotalCreditAllocated { get; set; }
        public decimal LoyaltyPointsAmountApplied
        {
            get
            {
                decimal multiplier = ServiceFactory.GetInstance<ICustomerService>()
                                                   .GetRedemptionMultiplier();
                decimal points = ServiceFactory.GetInstance<IShoppingCartService>()
                                               .GetAppliedLoyaltyPoints();
                return points * multiplier;
            }
        }
        public decimal LoyaltyPointsCreditAllocated { get; set; }
        public decimal CreditMemosTotalAmountApplied
        {
            get
            {
                return _currentCreditMemos.Sum(x => x.CreditAppliedInShoppingCart);
            }
        }
        public decimal CreditMemosCreditAllocated { get; set; }
        public bool IsGiftEmailNotSet()
        {
            return ServiceFactory.GetInstance<IShoppingCartService>()
                                 .GetShoppingCartGiftEmails()
                                 .ToList()
                                 .Count == 0;
        }
        public bool HasGiftItems()
        {
            if (m_CartItems == null || m_CartItems.Count == 0) { return false; }
            return (m_CartItems.Any(item => item.ItemType.EqualsIgnoreCase(Const.ITEM_TYPE_GIFT_CARD) ||
                                            item.ItemType.EqualsIgnoreCase(Const.ITEM_TYPE_GIFT_CERTIFICATE)));
        }
        public bool HasAppliedGiftCode()
        {
            if (!this.IsGiftCodeEnabled) { return false; }
            return this.HasAppliedGiftCardCode() || this.HasAppliedGiftCertCode();
        }
        public bool HasAppliedGiftCardCode()
        {
            if (!this.IsGiftCodeEnabled) { return false; }
            return _currentGiftCodes.Any(code => code.Type.EqualsIgnoreCase(Const.ITEM_TYPE_GIFT_CARD));
        }
        public bool HasAppliedGiftCertCode()
        {
            if (!this.IsGiftCodeEnabled) { return false; }
            return _currentGiftCodes.Any(code => code.Type.EqualsIgnoreCase(Const.ITEM_TYPE_GIFT_CERTIFICATE));
        }
        public bool HasAppliedInvalidGiftCode()
        {
            //should be valid since giftcode has been disabled
            if (!this.IsGiftCodeEnabled) { return false; }

            //check if there is something to be validated
            if (_currentGiftCodes.Count == 0) { return false; }

            //check if amount applied is greater than available
            if (_currentGiftCodes.Any(code => code.AmountApplied > code.CreditAvailable)) { return true; }

            //get activated giftcodes
            var activated = _currentGiftCodes.Where(code => code.IsActivated).ToList();
            if (activated.Count > 0)
            {
                //check if there is any giftcodes that has been 
                //activated that does not belong to the current customer
                var customer = ServiceFactory.GetInstance<IAuthenticationService>().GetCurrentLoggedInCustomer();
                return activated.Any(code => code.BillToCode != customer.CustomerCode);
            }

            return false;
        }
        public bool HasAppliedInvalidGiftCard()
        {
            //should be valid since giftcode has been disabled
            if (!this.IsGiftCodeEnabled) { return false; }

            var currentGiftCards = _currentGiftCodes.Where(x => x.Type.EqualsIgnoreCase(Const.ITEM_TYPE_GIFT_CARD))
                                                    .ToList();

            //check if there is something to be validated
            if (currentGiftCards.Count == 0) { return false; }

            //check if amount applied is greater than available
            if (currentGiftCards.Any(code => code.AmountApplied > code.CreditAvailable)) { return true; }

            //get activated giftcodes
            var activated = currentGiftCards.Where(code => code.IsActivated).ToList();
            if (activated.Count > 0)
            {
                //check if there is any giftcodes that has been 
                //activated that does not belong to the current customer
                var customer = ServiceFactory.GetInstance<IAuthenticationService>().GetCurrentLoggedInCustomer();
                return activated.Any(code => code.BillToCode != customer.CustomerCode);
            }

            return false;
        }
        public bool HasAppliedInvalidGiftCert()
        {
            //should be valid since giftcode has been disabled
            if (!this.IsGiftCodeEnabled) { return false; }

            var currentGiftCards = _currentGiftCodes.Where(x => x.Type.EqualsIgnoreCase(Const.ITEM_TYPE_GIFT_CERTIFICATE))
                                                    .ToList();

            //check if there is something to be validated
            if (currentGiftCards.Count == 0) { return false; }

            //check if amount applied is greater than available
            if (currentGiftCards.Any(code => code.AmountApplied > code.CreditAvailable)) { return true; }

            //get activated giftcodes
            var activated = currentGiftCards.Where(code => code.IsActivated).ToList();
            if (activated.Count > 0)
            {
                //check if there is any giftcodes that has been 
                //activated that does not belong to the current customer
                var customer = ServiceFactory.GetInstance<IAuthenticationService>().GetCurrentLoggedInCustomer();
                return activated.Any(code => code.BillToCode != customer.CustomerCode);
            }

            return false;
        }
        public bool HasAppliedLoyaltyPoints()
        {
            if (!this.IsLoyaltyPointsEnabled) { return false; }
            decimal points = ServiceFactory.GetInstance<IShoppingCartService>().GetAppliedLoyaltyPoints();
            return (points > Decimal.Zero);
        }
        public bool HasAppliedInvalidLoyaltyPoints()
        {
            //should be valid since loyalty points has been disabled
            if (!this.IsLoyaltyPointsEnabled) { return false; }

            var customer = ServiceFactory.GetInstance<IAuthenticationService>().GetCurrentLoggedInCustomer();
            decimal multiplier = ServiceFactory.GetInstance<ICustomerService>().GetRedemptionMultiplier();
            decimal points = ServiceFactory.GetInstance<IShoppingCartService>().GetAppliedLoyaltyPoints();
            decimal amount = multiplier * points;

            if (amount == Decimal.Zero) { return false; }

            //check if applied loyalty points is equal or lesser than  remaining points
            var loyaltyPoints = ServiceFactory.GetInstance<ICustomerService>()
                                              .GetLoyaltyPoints();

            return (loyaltyPoints != null) ? (amount > loyaltyPoints.MonetizedRemainingPoints.ToCustomerRoundedMonetary()) : true;
        }
        public bool HasAppliedCreditMemo()
        {
            if (!this.IsCreditMemosEnabled) { return false; }
            return (_currentCreditMemos.Count > 0);
        }
        public bool HasAppliedInvalidCreditMemo()
        {
            //should be valid since creditmemo has been disabled
            if (!this.IsCreditMemosEnabled) { return false; }

            bool isInvalid = false;
            foreach (var creditMemo in _currentCreditMemos)
            {
                decimal remaining = _availableCreditMemos.Where(x => x.CreditCode == creditMemo.CreditCode)
                                                         .Sum(x => x.CreditRemainingBalance);
                if (creditMemo.CreditAppliedInShoppingCart > remaining) { isInvalid = true; }
            }
            return isInvalid;
        }
        public void ClearGiftCodes()
        {
            ServiceFactory.GetInstance<IShoppingCartService>()
                          .ClearAppliedGiftCodes();
        }
        public void ClearLoyaltyPoints()
        {
            ServiceFactory.GetInstance<IShoppingCartService>()
                          .ClearAppliedLoyaltyPoints();
        }
        public void ClearCreditMemos()
        {
            ServiceFactory.GetInstance<IShoppingCartService>()
                          .ClearAppliedCreditMemos();
        }
        public decimal GetOtherPaymentTotal()
        {
            decimal amountApplied = Decimal.Zero;

            if (this.HasAppliedGiftCode() && !this.HasAppliedInvalidGiftCode()) { amountApplied += this.GiftCodesTotalAmountApplied; }
            if (this.HasAppliedLoyaltyPoints() && !this.HasAppliedInvalidLoyaltyPoints()) { amountApplied += this.LoyaltyPointsAmountApplied; }
            if (this.HasAppliedCreditMemo() && !this.HasAppliedInvalidCreditMemo()) { amountApplied += this.CreditMemosTotalAmountApplied; }

            return amountApplied;
        }

        #endregion


    }

    public class StockByUnitMeasure
    {
        public string unitMeasureCode;
        public decimal unitMeasureQuantity;
        public decimal inStock;
        public decimal freeStock;
    }
}















