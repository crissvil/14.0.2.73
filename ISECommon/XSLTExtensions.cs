// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
namespace InterpriseSuiteEcommerceCommon
{
    public class XSLTExtensions : XSLTExtensionBase
    {
        public XSLTExtensions(Customer cust, int SkinID) : base(cust, SkinID) { }

        public string CheckLocaleSettingForProperCase(string locale)
        {
            return Localization.CheckLocaleSettingForProperCase(locale);
        }

        public string XmlEncode(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return XmlCommon.XmlEncode(value);
            }
            else
            {
                return string.Empty;
            }
        }

        public string XmlDecode(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                return XmlCommon.XmlDecode(value);
            }
            else
            {
                return string.Empty;
            }
        }
    }
}
