// ------------------------------------------------------------------------------------------
// Licensed by Interprise Solutions.
// http://www.InterpriseSolutions.com
// For details on this license please visit  the product homepage at the URL above.
// THE ABOVE NOTICE MUST REMAIN INTACT.
// ------------------------------------------------------------------------------------------
using System;
using System.Threading;
using System.Web;
using System.Configuration;
using System.Web.SessionState;
using System.Data;
using System.Collections;
using System.Text;
using System.Xml;
using System.IO;
using System.Xml.Xsl;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using InterpriseSuiteEcommerceCommon.Extensions;

namespace InterpriseSuiteEcommerceCommon
{
    /// <summary>
    /// Summary description for DB.
    /// </summary>
    public class DB
    {

        static CultureInfo USCulture = new CultureInfo("en-US");
        static CultureInfo SqlServerCulture = new CultureInfo(CommonLogic.Application("DBSQLServerLocaleSetting"));

        public DB() { }

        static private string _activeDBConn = SetDBConn();

        static private string SetDBConn()
        {
            string s = CommonLogic.ApplicationIS("DBConn");
            return s;
        }

        static public string GetDBConn()
        {
            return _activeDBConn;
        }

        public static SqlConnection NewSqlConnection()
        {
            return new SqlConnection(GetDBConn());
        }

        static public string GetTableIdentityField(string TableName)
        {
            if (TableName.Length == 0)
            {
                return string.Empty;
            }
            string tmpS = string.Empty;

            using (SqlConnection con = new SqlConnection(DB.GetDBConn()))
            {
                con.Open();
                using (IDataReader rs = DB.GetRS("select name from syscolumns with (NOLOCK) where id = object_id(" + DB.SQuote(TableName) + ") and colstat & 1 = 1", con))
                {
                    if (rs.Read())
                    {
                        tmpS = DB.RSField(rs, "name");
                    }
                }
            }

            return tmpS;
        }

        static public string GetTableColumnDataType(string TableName, string ColumnName)
        {
            if (TableName.Length == 0 || ColumnName.Length == 0)
            {
                return String.Empty;
            }
            string tmpS = String.Empty;

            using (SqlConnection con = new SqlConnection(DB.GetDBConn()))
            {
                con.Open();
                using (IDataReader rs = DB.GetRS("select st.name from syscolumns sc with (NOLOCK) join systypes st with (NOLOCK) on sc.xtype = st.xtype where id = object_id(" + DB.SQuote(TableName) + ") and sc.name = " + DB.SQuote(ColumnName) + "", con))
                {
                    if (rs.Read())
                    {
                        tmpS = DB.RSField(rs, "name");
                    }
                }
            }

            return tmpS;
        }

        public static string SQuote(string s)
        {
            return s.ToDbQuote();
        }

        public static string SQuoteNotUnicode(string s)
        {
            int len = s.Length + 25;
            var tmpS = new StringBuilder(len); // hopefully only one alloc
            tmpS.Append("'");
            tmpS.Append(s.Replace("'", "''"));
            tmpS.Append("'");
            return tmpS.ToString();
        }

        public static string DateQuote(string s)
        {
            return s.ToDbDateQuote();
        }

        public static string DateQuote(DateTime dt)
        {
            return DateQuote(Localization.ToDBDateTimeString(dt));
        }

        public static IDataReader GetRSFormat(string format, params object[] args)
        {
            return GetRS(string.Format(format, args));
        }

        public static IDataReader GetRSFormat(SqlConnection con, string format, params object[] args)
        {
            return GetRS(string.Format(format, args), con);
        }

        static public IDataReader GetRS(String Sql, SqlConnection dbconn)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            using (SqlCommand cmd = new SqlCommand(Sql, dbconn))
            {
                return cmd.ExecuteReader();
            }
        }

        static public IDataReader GetRS(String Sql, SqlParameter[] spa, SqlConnection dbconn)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }

            using (SqlCommand cmd = new SqlCommand(Sql, dbconn))
            {
                foreach (SqlParameter sp in spa)
                {
                    cmd.Parameters.Add(sp);
                }
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
        }

        static public IDataReader GetRS(String Sql, SqlTransaction trans)
        {
            if (trans == null)
            {
                // we can't use the other overloads for this
                // since one is obsolete and the other one requires an 
                // SqlConnection object, creating one here, there's no way to tell
                // when we can/should dispose it.
                throw new ArgumentNullException("Transaction cannot be null!!");
            }
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            using (SqlCommand cmd = new SqlCommand(Sql, trans.Connection, trans))
            {
                return cmd.ExecuteReader();
            }
        }
        
        public static SqlDataReader SPGetRS(String procCall, SqlConnection dbconn)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SP=" + procCall + "<br/>\n");
            }

            using (SqlCommand cmd = new SqlCommand(procCall, dbconn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
        }

        public static SqlDataReader ExecuteStoredProcReader(String StoredProcName, SqlParameter[] spa, SqlConnection dbconn)
        {
            using (SqlCommand dbCommand = new SqlCommand(StoredProcName, dbconn))
            {
                dbCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter sp in spa)
                { dbCommand.Parameters.Add(sp); }

                SqlDataReader dbReader;
                dbReader = dbCommand.ExecuteReader(CommandBehavior.CloseConnection);
                // Always call Read before accessing data.
                return dbReader;
            }

        }


        [Obsolete("Deprecated.  Use the GetRS that takes the SqlConnection as a parameter.")]
        static public IDataReader GetRS(String Sql)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            SqlConnection dbconn = new SqlConnection();
            dbconn.ConnectionString = DB.GetDBConn();
            dbconn.Open();
            using (SqlCommand cmd = new SqlCommand(Sql, dbconn))
            {
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
        }

        [Obsolete("Deprecated.  Use the GetRS that takes the SqlConnection as a parameter.")]
        static public IDataReader GetRS(String Sql, SqlParameter[] spa)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            using (SqlConnection dbconn = new SqlConnection())
            {
                dbconn.ConnectionString = DB.GetDBConn();
                dbconn.Open();
                using (SqlCommand cmd = new SqlCommand(Sql, dbconn))
                {
                    foreach (SqlParameter sp in spa)
                    {
                        cmd.Parameters.Add(sp);
                    }
                    return cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
            }
        }


        [Obsolete("Deprecated.  Use the SPGetRS that takes the SqlConnection as a parameter.")]
        public static SqlDataReader SPGetRS(String procCall)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SP=" + procCall + "<br/>\n");
            }
            using (SqlConnection dbconn = new SqlConnection())
            {
                dbconn.ConnectionString = GetDBConn();
                dbconn.Open();
                using (SqlCommand cmd = new SqlCommand(procCall, dbconn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    return cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
            }
        }

        public static void ExecuteSQL(string sql, params object[] args)
        {
            ExecuteSQL(string.Format(sql, args));
        }

        static public void ExecuteSQL(String Sql)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            SqlConnection dbconn = new SqlConnection();
            dbconn.ConnectionString = DB.GetDBConn();
            dbconn.Open();
            SqlCommand cmd = new SqlCommand(Sql, dbconn);

             //Handle timeout error on SQL.
            int tries = 1;
            cmd.CommandTimeout = 30;
            while (tries <= 3)
            {
                try
                {
                    if (dbconn.State == ConnectionState.Closed)
                    {
                        dbconn.Open();
                    }
                    cmd.ExecuteNonQuery();
                    tries = 4;
                }
                catch (SqlException ex)
                {
                    if (ex.Message.Contains("Timeout expired"))
                    {
                        if (tries == 3)
                        {
                            throw ex;
                        }
                        else
                        {
                            tries += 1;
                            cmd.CommandTimeout *= 10;
                        }
                    }
                    else
                    {
                        throw ex;
                    }
                }
                finally
                {
                    cmd.Dispose();
                    dbconn.Close();
                    dbconn.Dispose();
                }
            }
        }

        static public void ExecuteSQL(String Sql, SqlConnection dbconn)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            SqlCommand cmd = new SqlCommand(Sql, dbconn);
            try
            {
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                throw (ex);
            }
        }

        static public void ExecuteSQL(String Sql, SqlTransaction trans)
        {
            if (trans == null)
            {
                ExecuteSQL(Sql);
                return;
            }
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            SqlCommand cmd = new SqlCommand(Sql, trans.Connection, trans);
            try
            {
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                throw (ex);
            }
        }

        public static object ExecuteSQLScalar(string sql, params object[] args)
        {
            return ExecuteSQLScalar(string.Format(sql, args));
        }

        public static object ExecuteSQLScalar(string Sql)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            SqlConnection dbconn = new SqlConnection();
            dbconn.ConnectionString = DB.GetDBConn();
            dbconn.Open();
            SqlCommand cmd = new SqlCommand(Sql, dbconn);

            object returnValue = null;
            try
            {
                returnValue = cmd.ExecuteScalar();
                cmd.Dispose();
                dbconn.Close();
                dbconn.Dispose();
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                dbconn.Close();
                dbconn.Dispose();
                throw (ex);
            }

            return returnValue;
        }

        // NOTE FOR DB ACCESSOR FUNCTIONS: AdminSite try/catch block is needed until
        // we convert to the new admin page styles. Our "old" db accessors handled empty
        // recordset conditions, so we need to preserve that for the admin site to add 
        // new products/categories/etc...
        //
        // We do not use try/catch on the store site for speed

        // ----------------------------------------------------------------
        //
        // SIMPLE ROW FIELD ROUTINES
        //
        // ----------------------------------------------------------------
        public static String RowField(DataRow row, string fieldname)
        {
                if (Convert.IsDBNull(row[fieldname]))
                {
                    return String.Empty;
                }
                return Convert.ToString(row[fieldname]);
        }

        public static String RowFieldByLocale(DataRow row, string fieldname, string LocaleSetting)
        {
            String tmpS = String.Empty;
                if (Convert.IsDBNull(row[fieldname]))
                {
                    tmpS = String.Empty;
                }
                else
                {
                    tmpS = Convert.ToString(row[fieldname]);
                }

            return XmlCommon.GetLocaleEntry(tmpS, LocaleSetting, true);
        }

        // uses xpath spec to look into the field value and return a node innertext
        public static String RowFieldByXPath(DataRow row, string fieldname, string XPath)
        {
            String tmpS = String.Empty;
                if (Convert.IsDBNull(row[fieldname]))
                {
                    tmpS = String.Empty;
                }
                else
                {
                    tmpS = Convert.ToString(row[fieldname]);
                }
            return XmlCommon.GetXPathEntry(tmpS, XPath);
        }

        public static bool RowFieldBool(DataRow row, string fieldname)
        {
            if (Convert.IsDBNull(row[fieldname]))
            {
                return false;
            }

            String s = row[fieldname].ToString();

            return "TRUE".Equals(s, StringComparison.InvariantCultureIgnoreCase) ||
                    "YES".Equals(s, StringComparison.InvariantCultureIgnoreCase) ||
                    "1".Equals(s, StringComparison.InvariantCultureIgnoreCase);
        }

        public static Byte RowFieldByte(DataRow row, string fieldname)
        {
                if (Convert.IsDBNull(row[fieldname]))
                {
                    return 0;
                }
                return Convert.ToByte(row[fieldname]);
        }

        public static String RowFieldGUID(DataRow row, string fieldname)
        {
                if (Convert.IsDBNull(row[fieldname]))
                {
                    return String.Empty;
                }
                return Convert.ToString(row[fieldname]);
        }

        public static int RowFieldInt(DataRow row, string fieldname)
        {
                if (Convert.IsDBNull(row[fieldname]))
                {
                    return 0;
                }
                return Convert.ToInt32(row[fieldname]);
        }

        public static long RowFieldLong(DataRow row, string fieldname)
        {
                if (Convert.IsDBNull(row[fieldname]))
                {
                    return 0;
                }
                return Convert.ToInt64(row[fieldname]);
        }

        public static Single RowFieldSingle(DataRow row, string fieldname)
        {
                if (Convert.IsDBNull(row[fieldname]))
                {
                    return 0.0F;
                }
                return Convert.ToSingle(row[fieldname]);
        }

        public static Double RowFieldDouble(DataRow row, string fieldname)
        {
                if (Convert.IsDBNull(row[fieldname]))
                {
                    return 0.0F;
                }
                return Convert.ToDouble(row[fieldname]);
        }

        public static Decimal RowFieldDecimal(DataRow row, string fieldname)
        {
                if (Convert.IsDBNull(row[fieldname]))
                {
                    return System.Decimal.Zero;
                }
                return Convert.ToDecimal(row[fieldname]);
        }


        public static DateTime RowFieldDateTime(DataRow row, string fieldname)
        {
                if (Convert.IsDBNull(row[fieldname]))
                {
                    return System.DateTime.MinValue;
                }
                return Convert.ToDateTime(row[fieldname], SqlServerCulture);
        }

        // ----------------------------------------------------------------
        //
        // SIMPLE RS FIELD ROUTINES
        //
        // ----------------------------------------------------------------
        [Obsolete("Use this method: rs.ToRsField() Extension Method")]
        public static string RSField(IDataReader rs, string fieldname)
        {
                int idx = rs.GetOrdinal(fieldname);
                if (rs.IsDBNull(idx) || rs.GetString(idx) == "\0")
                {
                    return String.Empty;
                }
                return rs.GetString(idx);    
        }

        public static string RSFieldByLocale(IDataReader rs, string fieldname, string LocaleSetting)
        {
            string tmpS = String.Empty;
            int idx = rs.GetOrdinal(fieldname);
            if (rs.IsDBNull(idx))
            {
                tmpS = String.Empty;
            }
            else
            {
                tmpS = rs.GetString(idx);
            }
            return XmlCommon.GetLocaleEntry(tmpS, LocaleSetting, true);
        }

        // uses xpath spec to look into the field value and return a node innertext
        public static string RSFieldByXPath(IDataReader rs, string fieldname, string XPath)
        {
            String tmpS = String.Empty;
                int idx = rs.GetOrdinal(fieldname);
                if (rs.IsDBNull(idx))
                {
                    tmpS = String.Empty;
                }
                else
                {
                    tmpS = rs.GetString(idx);
                }
            return XmlCommon.GetXPathEntry(tmpS, XPath);
        }

        [Obsolete("Use this method: rs.ToRsFieldBool() Extension Method")]
        public static bool RSFieldBool(IDataReader rs, string fieldname)
        {
            int idx = rs.GetOrdinal(fieldname);
            if (rs.IsDBNull(idx))
            {
                return false;
            }
            String s = rs[fieldname].ToString();

            return "TRUE".Equals(s, StringComparison.InvariantCultureIgnoreCase) ||
                    "YES".Equals(s, StringComparison.InvariantCultureIgnoreCase) ||
                    "1".Equals(s, StringComparison.InvariantCultureIgnoreCase);
        }

        public static String RSFieldGUID(IDataReader rs, string fieldname)
        {
                int idx = rs.GetOrdinal(fieldname);
                if (rs.IsDBNull(idx))
                {
                    return String.Empty;
                }
                return rs.GetGuid(idx).ToString();
        }

        public static Guid RSFieldGUID2(IDataReader rs, string fieldname)
        {
                int idx = rs.GetOrdinal(fieldname);
                if (rs.IsDBNull(idx))
                {
                    return new Guid("00000000000000000000000000000000");
                }
                return rs.GetGuid(idx);
        }

        public static Guid? RSFieldGUID3(IDataReader rs, string fieldname)
        {
            int idx = rs.GetOrdinal(fieldname);
            if (rs.IsDBNull(idx))
            {
                return null;
            }
            return rs.GetGuid(idx);
        }

        public static Byte RSFieldByte(IDataReader rs, string fieldname)
        {
                int idx = rs.GetOrdinal(fieldname);
                if (rs.IsDBNull(idx))
                {
                    return 0;
                }
                return rs.GetByte(idx);
        }

        [Obsolete("Use this method: rs.ToRsFieldInt() Extension Method")]
        public static int RSFieldInt(IDataReader rs, string fieldname)
        {
                int idx = rs.GetOrdinal(fieldname);
                if (rs.IsDBNull(idx) || string.IsNullOrEmpty(rs[fieldname].ToString())) 
                {
                    return 0;
                }
                return rs.GetInt32(idx);
        }

        public static int RSFieldTinyInt(IDataReader rs, string fieldname)
        {
                int idx = rs.GetOrdinal(fieldname);
                if (rs.IsDBNull(idx))
                {
                    return 0;
                }
                return Localization.ParseNativeInt(rs[idx].ToString());
        }

        [Obsolete("Use this method: rs.ToRSFieldLong() Extension Method")]
        public static long RSFieldLong(IDataReader rs, string fieldname)
        {
                int idx = rs.GetOrdinal(fieldname);
                if (rs.IsDBNull(idx))
                {
                    return 0;
                }
                return rs.GetInt64(idx);
        }

        [Obsolete("Use this method: rs.RSFieldSingle() Extension Method")]
        public static Single RSFieldSingle(IDataReader rs, string fieldname)
        {
                int idx = rs.GetOrdinal(fieldname);
                if (rs.IsDBNull(idx))
                {
                    return 0.0F;
                }
                return (Single)rs.GetDouble(idx); // SQL server seems to fail the GetFloat calls, so we have to do this
        }

        [Obsolete("Use this method: rs.ToRSFieldDouble() Extension Method")]
        public static Double RSFieldDouble(IDataReader rs, string fieldname)
        {
                int idx = rs.GetOrdinal(fieldname);
                if (rs.IsDBNull(idx))
                {
                    return 0.0F;
                }
                return rs.GetDouble(idx);
        }

        [Obsolete("Use this method: rs.ToRsFieldDecimal() Extension Method")]
        public static Decimal RSFieldDecimal(IDataReader rs, string fieldname)
        {
                int idx = rs.GetOrdinal(fieldname);
                if (rs.IsDBNull(idx))
                {
                    return System.Decimal.Zero;
                }
                return rs.GetDecimal(idx);
        }

        [Obsolete("Use this method: rs.ToRSFieldDateTime() Extension Method")]
        public static DateTime RSFieldDateTime(IDataReader rs, String fieldname)
        {
                int idx = rs.GetOrdinal(fieldname);
                if (rs.IsDBNull(idx))
                {
                    return System.DateTime.MinValue;
                }
                return Convert.ToDateTime(rs[idx], SqlServerCulture);                
        }

        public static DataSet GetTable(String tablename, String orderBy, String cacheName, bool useCache)
        {
            if (useCache)
            {
                DataSet cacheds = (DataSet)HttpContext.Current.Cache.Get(cacheName);
                if (cacheds != null)
                {
                    return cacheds;
                }
            }
            DataSet ds = new DataSet();
            String Sql = String.Empty;
            SqlConnection dbconn = new SqlConnection();
            dbconn.ConnectionString = DB.GetDBConn();
            dbconn.Open();
            Sql = "select * from " + tablename + " with (NOLOCK) order by " + orderBy;
            SqlDataAdapter da = new SqlDataAdapter(Sql, dbconn);
            da.Fill(ds, tablename);
            dbconn.Close();
            if (useCache)
            {
                HttpContext.Current.Cache.Insert(cacheName, ds);
            }
            return ds;
        }

        /// <summary>
        /// Incrementally adds tables results to a dataset
        /// </summary>
        /// <param name="ds">Dataset to add the table to</param>
        /// <param name="tableName">Name of the table to be created in the DataSet</param>
        /// <param name="sqlQuery">Query to retrieve the data for the table.</param>
        static public int FillDataSet(DataSet ds, string tableName, string sqlQuery)
        {
            int n = 0;
            SqlConnection dbconn = new SqlConnection();
            dbconn.ConnectionString = DB.GetDBConn();
            dbconn.Open();
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, dbconn);
            n = da.Fill(ds, tableName);
            dbconn.Close();
            return n;
        }

        public static DataSet GetDS(String Sql, String cacheName, bool useCache, System.DateTime expiresAt)
        {
            bool canUseCache = (useCache && HttpContext.Current != null);
            if (canUseCache)
            {
                DataSet cacheds = (DataSet)HttpContext.Current.Cache.Get(cacheName);
                if (cacheds != null)
                {
                    if (CommonLogic.ApplicationBool("DumpSQL"))
                    {
                        HttpContext.Current.Response.Write("SQL=[From Cache]" + Sql + "<br/>");
                    }
                    return cacheds;
                }
            }

            if (CommonLogic.ApplicationBool("DumpSQL") && HttpContext.Current != null)
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>");
            }
            DataSet ds = new DataSet();
            SqlConnection dbconn = new SqlConnection();
            dbconn.ConnectionString = DB.GetDBConn();
            dbconn.Open();
            SqlDataAdapter da = new SqlDataAdapter(Sql, dbconn);
            da.Fill(ds, "Table");
            dbconn.Close();

            if (canUseCache)
            {
                HttpContext.Current.Cache.Insert(cacheName, ds, null, expiresAt, TimeSpan.Zero);
            }
            return ds;
        }

        public static DataSet GetDS(String Sql, String cacheName, bool useCache, System.DateTime expiresAt, SqlConnection dbconn)
        {
            bool canUseCache = (useCache && HttpContext.Current != null);
            if (canUseCache)
            {
                DataSet cacheds = (DataSet)HttpContext.Current.Cache.Get(cacheName);
                if (cacheds != null)
                {
                    if (CommonLogic.ApplicationBool("DumpSQL"))
                    {
                        HttpContext.Current.Response.Write("SQL=[From Cache]" + Sql + "<br/>");
                    }
                    return cacheds;
                }
            }

            if (CommonLogic.ApplicationBool("DumpSQL") && HttpContext.Current != null)
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>");
            }

            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(Sql, dbconn);
            da.Fill(ds, "Table");

            if (canUseCache)
            {
                HttpContext.Current.Cache.Insert(cacheName, ds, null, expiresAt, TimeSpan.Zero);
            }
            return ds;
        }

        public static DataSet GetDS(String Sql, String cacheName, bool useCache)
        {
            return GetDS(Sql, cacheName, useCache, System.DateTime.Now.AddMinutes(AppLogic.CacheDurationMinutes()));
        }

        public static DataSet GetDS(String Sql, bool useCache, System.DateTime expiresAt)
        {
            return GetDS(Sql, "DataSet: " + Sql.ToUpperInvariant(), useCache, expiresAt);
        }

        public static DataSet GetDS(String Sql, bool useCache)
        {
            return GetDS(Sql, useCache, System.DateTime.Now.AddMinutes(AppLogic.CacheDurationMinutes()));
        }

        public static DataSet GetDS(String Sql, String cacheName, bool useCache, SqlConnection dbconn)
        {
            return GetDS(Sql, cacheName, useCache, System.DateTime.Now.AddMinutes(AppLogic.CacheDurationMinutes()), dbconn);
        }

        public static DataSet GetDS(String Sql, bool useCache, System.DateTime expiresAt, SqlConnection dbconn)
        {
            return GetDS(Sql, "DataSet: " + Sql.ToUpperInvariant(), useCache, expiresAt, dbconn);
        }

        public static DataSet GetDS(String Sql, bool useCache, SqlConnection dbconn)
        {
            return GetDS(Sql, useCache, System.DateTime.Now.AddMinutes(AppLogic.CacheDurationMinutes()), dbconn);
        }


        public static String GetNewGUID()
        {
            return System.Guid.NewGuid().ToString();
        }

        static public int GetSqlN(String Sql)
        {
            int N = 0;

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader rs = DB.GetRSFormat(con, Sql))
                {
                    if (rs.Read())
                    {
                        N = DB.RSFieldInt(rs, "N");
                    }
                }
            }
            
            return N;
        }

        static public Single GetSqlNSingle(String Sql)
        {
            Single N = 0.0F;

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader rs = DB.GetRSFormat(con, Sql))
                {
                    if (rs.Read())
                    {
                        N = DB.RSFieldSingle(rs, "N");
                    }
                }
            }

            return N;
        }

        static public bool GetSqlNBool(String Sql)
        {
            bool N = false;

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader rs = DB.GetRSFormat(con, Sql))
                {
                    if (rs.Read())
                    {
                        N = rs.ToRSFieldBool("N");
                    }
                }
            }

            return N;
        }

        static public decimal GetSqlNDecimal(String Sql)
        {
            decimal N = System.Decimal.Zero;

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader rs = DB.GetRSFormat(con, Sql))
                {
                    if (rs.Read())
                    {
                        N = DB.RSFieldDecimal(rs, "N");
                    }
                }
            }

            return N;
        }

        static public void ExecuteLongTimeSQL(String Sql, int TimeoutSecs)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            SqlConnection dbconn = new SqlConnection();
            dbconn.ConnectionString = DB.GetDBConn();
            dbconn.Open();
            SqlCommand cmd = new SqlCommand(Sql, dbconn);
            cmd.CommandTimeout = TimeoutSecs;
            try
            {
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                dbconn.Close();
                dbconn.Dispose();
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                dbconn.Close();
                dbconn.Dispose();
                throw (ex);
            }
        }

        static public String GetSqlS(String Sql)
        {
            String S = String.Empty;

            using (SqlConnection con = DB.NewSqlConnection())
            {
                con.Open();
                using (IDataReader rs = DB.GetRSFormat(con, Sql))
                {
                    if (rs.Read())
                    {
                        S = DB.RSFieldByLocale(rs, "S", System.Threading.Thread.CurrentThread.CurrentUICulture.Name);
                        if (S.Equals(DBNull.Value))
                        {
                            S = String.Empty;
                        }
                    }
                }
            }

            return S;
        }

        static public int GetSqlN(String Sql, SqlConnection dbconn)
        {
            int N = 0;
            IDataReader rs = DB.GetRS(Sql, dbconn);
            if (rs.Read())
            {
                N = DB.RSFieldInt(rs, "N");
            }
            rs.Close();
            return N;
        }

        static public int GetSqlN(String Sql, SqlTransaction trans)
        {
            if (trans == null)
            {
                return GetSqlN(Sql);
            }
            int N = 0;
            IDataReader rs = DB.GetRS(Sql, trans);
            if (rs.Read())
            {
                N = DB.RSFieldInt(rs, "N");
            }
            rs.Close();
            return N;
        }

        static public Single GetSqlNSingle(String Sql, SqlConnection dbconn)
        {
            Single N = 0.0F;
            IDataReader rs = DB.GetRS(Sql, dbconn);
            if (rs.Read())
            {
                N = DB.RSFieldSingle(rs, "N");
            }
            rs.Close();
            return N;
        }

        static public Single GetSqlNSingle(String Sql, SqlTransaction trans)
        {
            if (trans == null)
            {
                return GetSqlNSingle(Sql);
            }
            Single N = 0.0F;
            IDataReader rs = DB.GetRS(Sql, trans);
            if (rs.Read())
            {
                N = DB.RSFieldSingle(rs, "N");
            }
            rs.Close();
            return N;
        }

        static public decimal GetSqlNDecimal(String Sql, SqlConnection dbconn)
        {
            decimal N = System.Decimal.Zero;
            IDataReader rs = DB.GetRS(Sql, dbconn);
            if (rs.Read())
            {
                N = DB.RSFieldDecimal(rs, "N");
            }
            rs.Close();
            return N;
        }

        static public decimal GetSqlNDecimal(String Sql, SqlTransaction trans)
        {
            if (trans == null)
            {
                return GetSqlNDecimal(Sql);
            }
            decimal N = System.Decimal.Zero;
            IDataReader rs = DB.GetRS(Sql, trans);
            if (rs.Read())
            {
                N = DB.RSFieldDecimal(rs, "N");
            }
            rs.Close();
            return N;
        }

        static public void ExecuteLongTimeSQL(String Sql, int TimeoutSecs, SqlConnection dbconn)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }

            SqlCommand cmd = new SqlCommand(Sql, dbconn);
            cmd.CommandTimeout = TimeoutSecs;
            try
            {
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                throw (ex);
            }
        }

        static public void ExecuteLongTimeSQL(String Sql, int TimeoutSecs, SqlTransaction trans)
        {
            if (trans == null)
            {
                ExecuteLongTimeSQL(Sql, TimeoutSecs);
                return;
            }
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            SqlCommand cmd = new SqlCommand(Sql, trans.Connection, trans);
            cmd.CommandTimeout = TimeoutSecs;
            try
            {
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                throw (ex);
            }
        }

        static public String GetSqlS(String Sql, SqlConnection dbconn)
        {
            String S = String.Empty;
            IDataReader rs = DB.GetRS(Sql, dbconn);
            if (rs.Read())
            {
                S = DB.RSFieldByLocale(rs, "S", System.Threading.Thread.CurrentThread.CurrentUICulture.Name);
                if (S.Equals(DBNull.Value))
                {
                    S = String.Empty;
                }
            }
            rs.Close();
            return S;
        }

        static public String GetSqlS(String Sql, SqlTransaction trans)
        {
            if (trans == null)
            {
                return GetSqlS(Sql);
            }
            return GetSqlS(Sql, trans.Connection);
        }

        static public String GetSqlSAllLocales(String Sql)
        {
            String S = String.Empty;
            IDataReader rs = DB.GetRS(Sql);
            if (rs.Read())
            {
                S = DB.RSField(rs, "S");
                if (S.Equals(DBNull.Value))
                {
                    S = String.Empty;
                }
            }
            rs.Close();
            return S;
        }

        static public string GetSqlXml(String Sql, string xslTranformFile)
        {
            if (Sql.ToUpper(CultureInfo.InvariantCulture).IndexOf("FOR XML") < 1)
            {
                Sql += " FOR XML AUTO";
            }
            StringBuilder s = new StringBuilder(4096);
            IDataReader rs = DB.GetRS(Sql);
            while (rs.Read())
            {
                s.Append(rs.GetString(0));
            }
            rs.Close();
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml("<root>" + s.ToString() + "</root>");
            if (xslTranformFile != null && xslTranformFile.Trim() != "")
            {
                XslCompiledTransform xsl = new XslCompiledTransform();
                xsl.Load(xslTranformFile);
                TextWriter tw = new StringWriter();
                xsl.Transform(xdoc, null, tw);
                return tw.ToString();
            }
            else
            {
                return xdoc.DocumentElement.InnerXml;
            }
        }

        static public XmlDocument GetSqlXmlFromProc(String ProcInvocation)
        {
            StringBuilder s = new StringBuilder(4096);
            IDataReader rs = DB.GetRS(ProcInvocation);
            while (rs.Read())
            {
                s.Append(rs.GetString(0));
            }
            rs.Close();
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(s.ToString());
            return xdoc;
        }

        static public XmlDocument GetSqlXmlDoc(String Sql, string xslTranformFile)
        {
            if (Sql.ToUpper(CultureInfo.InvariantCulture).IndexOf("FOR XML") < 1)
            {
                Sql += " FOR XML AUTO";
            }
            StringBuilder s = new StringBuilder(4096);
            IDataReader rs = DB.GetRS(Sql);
            while (rs.Read())
            {
                s.Append(rs.GetString(0));
            }
            rs.Close();
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml("<root>" + s.ToString() + "</root>");
            if (xslTranformFile != null && xslTranformFile.Trim() != "")
            {
                XslCompiledTransform xsl = new XslCompiledTransform();
                xsl.Load(xslTranformFile);
                TextWriter tw = new StringWriter();
                xsl.Transform(xdoc, null, tw);
                xdoc.LoadXml(tw.ToString());
            }
            return xdoc;
        }

        static public string GetSqlXml(String Sql, XslCompiledTransform xsl, XsltArgumentList xslArgs)
        {
            if (Sql.IndexOf("for xml", StringComparison.InvariantCultureIgnoreCase) < 1)
            {
                Sql += " for xml auto";
            }
            StringBuilder s = new StringBuilder(4096);
            IDataReader rs = DB.GetRS(Sql);
            while (rs.Read())
            {
                s.Append(rs.GetString(0));
            }
            rs.Close();
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml("<root>" + s.ToString() + "</root>");
            if (xsl != null)
            {
                TextWriter tw = new StringWriter();
                xsl.Transform(xdoc, xslArgs, tw);
                return tw.ToString();
            }
            else
            {
                return xdoc.DocumentElement.InnerXml;
            }
        }

        static public int GetXml(IDataReader dr, string rootEl, string rowEl, bool IsPagingProc, ref StringBuilder Xml)
        {
            int rows = 0;
            if (rootEl.Length != 0)
            {
                Xml.Append("<");
                Xml.Append(rootEl);
                    Xml.Append(">");
            }
            while (dr.Read())
            {
                ++rows;
                if (rowEl.Length == 0)
                {
                    Xml.Append("<row>");
                }
                else
                {
                    Xml.Append("<");
                    Xml.Append(rowEl); 
                    Xml.Append(">");
                }
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    string elname = dr.GetName(i).Replace(" ", "_");
                    if (dr.IsDBNull(i))
                    {
                        Xml.Append("<");
                        Xml.Append(elname);
                        Xml.Append("/>");
                    }
                    else
                    {
                        if (System.Convert.ToString(dr.GetValue(i)).StartsWith("<ml>", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Xml.Append("<");
                            Xml.Append(elname);
                            Xml.Append(">");
                            Xml.Append(System.Convert.ToString(dr.GetValue(i)));
                            Xml.Append("</");
                            Xml.Append(elname);
                            Xml.Append(">");
                        }
                        else
                        {
                            Xml.Append("<");
                            Xml.Append(elname);
                            Xml.Append(">");
                            Xml.Append(XmlCommon.XmlEncodeAsIs(System.Convert.ToString(dr.GetValue(i))));
                            Xml.Append("</");
                            Xml.Append(elname);
                            Xml.Append(">");
                        }
                    }
                }
                if (rowEl.Length == 0)
                {
                    Xml.Append("</row>");
                }
                else
                {
                    Xml.Append("</"); 
                    Xml.Append(rowEl);
                    Xml.Append(">");
                }

            }
            if (rootEl.Length != 0)
            {
                Xml.Append("</"); 
                Xml.Append(rootEl); 
                Xml.Append(">");
            }
            int ResultSet = 1;
            while (dr.NextResult())
            {
                ResultSet++;
                if (IsPagingProc)
                {
                    if (rootEl.Length != 0)
                    {
                        Xml.Append("<");
                        Xml.Append(rootEl);
                        Xml.Append("Paging>");
                    }
                    else
                    {
                        Xml.Append("<Paging>");
                    }
                }
                else
                {
                    if (rootEl.Length != 0)
                    {
                        Xml.Append("<");
                        Xml.Append(rootEl);
                        Xml.Append(ResultSet.ToString());
                        Xml.Append(">");
                    }
                }
                while (dr.Read())
                {
                    if (!IsPagingProc)
                    {
                        if (rowEl.Length == 0)
                        {
                            Xml.Append("<row>");
                        }
                        else
                        {
                            Xml.Append("<");
                            Xml.Append(rowEl); 
                            Xml.Append(">");
                        }
                    }
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        string elname = dr.GetName(i).Replace(" ", "_");
                        if (dr.IsDBNull(i))
                        {
                            Xml.Append("<");
                            Xml.Append(elname);
                            Xml.Append("/>");
                        }
                        else
                        {
                            if (System.Convert.ToString(dr.GetValue(i)).StartsWith("<ml>", StringComparison.InvariantCultureIgnoreCase))
                            {
                                Xml.Append("<");
                                Xml.Append(elname);
                                Xml.Append(">");
                                Xml.Append(System.Convert.ToString(dr.GetValue(i)));
                                Xml.Append("</");
                                Xml.Append(elname);
                                Xml.Append(">");
                            }
                            else
                            {
                                Xml.Append("<");
                                Xml.Append(elname);
                                Xml.Append(">");
                                Xml.Append(XmlCommon.XmlEncodeAsIs(System.Convert.ToString(dr.GetValue(i))));
                                Xml.Append("</");
                                Xml.Append(elname);
                                Xml.Append(">");
                            }
                        }
                    }
                    if (!IsPagingProc)
                    {
                        if (rowEl.Length == 0)
                        {
                            Xml.Append("</row>");
                        }
                        else
                        {
                            Xml.Append("</");
                            Xml.Append(rowEl);
                            Xml.Append(">");
                        }
                    }
                }
                if (IsPagingProc)
                {
                    if (rootEl.Length != 0)
                    {
                        Xml.Append("</");
                        Xml.Append(rootEl);
                        Xml.Append("Paging>");
                    }
                    else
                    {
                        Xml.Append("</Paging>");
                    }
                }
                else
                {
                    if (rootEl.Length != 0)
                    {
                        Xml.Append("</");
                        Xml.Append(rootEl);
                        Xml.Append(ResultSet.ToString());
                        Xml.Append(">");
                    }
                }
            }
            dr.Close();
            return rows;
        }


        // assumes a 2nd result set is the PAGING INFO back from the aspdnsf_PageQuery proc!!!
        // looks for aspdnsf_PageQuery in the Sql input to determine this!
        static public int GetXml(string Sql, string rootEl, string rowEl, ref string Xml)
        {
            SqlConnection dbconn = new SqlConnection();
            dbconn.ConnectionString = DB.GetDBConn();
            dbconn.Open();
            int i = GetXml(Sql, rootEl, rowEl, ref Xml, dbconn);
            dbconn.Close();
            dbconn.Dispose();
            return i;
        }


        // assumes a 2nd result set is the PAGING INFO back from the aspdnsf_PageQuery proc!!!
        // looks for aspdnsf_PageQuery in the Sql input to determine this!
        static public int GetXml(string Sql, string rootEl, string rowEl, ref string Xml, SqlConnection dbconn)
        {
            bool IsPagingProc = (Sql.IndexOf("aspdnsf_PageQuery") != -1);
            StringBuilder s = new StringBuilder(4096);
            IDataReader rs = DB.GetRS(Sql, dbconn);
            int rows = 0;
            if (rootEl.Length != 0)
            {
                s.Append("<");
                s.Append(rootEl);
                s.Append(">");
            }
            while (rs.Read())
            {
                ++rows;
                if (rowEl.Length == 0)
                {
                    s.Append("<row>");
                }
                else
                {
                    s.Append("<");
                    s.Append(rowEl);
                    s.Append(">");
                }
                for (int i = 0; i < rs.FieldCount; i++)
                {
                    string elname = rs.GetName(i).Replace(" ", "_");
                    if (rs.IsDBNull(i))
                    {
                        s.Append("<");
                        s.Append(elname);
                        s.Append("/>");
                    }
                    else
                    {
                        if (System.Convert.ToString(rs.GetValue(i)).StartsWith("<ml>", StringComparison.InvariantCultureIgnoreCase))
                        {
                            s.Append("<");
                            s.Append(elname);
                            s.Append(">");
                            s.Append(System.Convert.ToString(rs.GetValue(i)));
                            s.Append("</");
                            s.Append(elname);
                            s.Append(">");
                        }
                        else
                        {
                            s.Append("<");
                            s.Append(elname);
                            s.Append(">");
                            s.Append(System.Convert.ToString(rs.GetValue(i)).Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("'", "&apos;").Replace("\"", "&quot;"));
                            s.Append("</");
                            s.Append(elname);
                            s.Append(">");
                        }
                    }
                }
                if (rowEl.Length == 0)
                {
                    s.Append("</row>");
                }
                else
                {
                    s.Append("</");
                    s.Append(rowEl);
                    s.Append(">");
                }

            }
            if (rootEl.Length != 0)
            {
                s.Append("</");
                s.Append(rootEl);
                s.Append(">");
            }
            int ResultSet = 1;
            while (rs.NextResult())
            {
                ResultSet++;
                if (IsPagingProc)
                {
                    if (rootEl.Length != 0)
                    {
                        s.Append("<");
                        s.Append(rootEl);
                        s.Append("Paging>");
                    }
                    else
                    {
                        s.Append("<Paging>");
                    }
                }
                else
                {
                    if (rootEl.Length != 0)
                    {
                        s.Append("<");
                        s.Append(rootEl);
                        s.Append(ResultSet.ToString());
                        s.Append(">");
                    }
                }
                while (rs.Read())
                {
                    if (!IsPagingProc)
                    {
                        if (rowEl.Length == 0)
                        {
                            s.Append("<row>");
                        }
                        else
                        {
                            s.Append("<");
                            s.Append(rowEl);
                            s.Append(">");
                        }
                    }
                    for (int i = 0; i < rs.FieldCount; i++)
                    {
                        string elname = rs.GetName(i).Replace(" ", "_");
                        if (rs.IsDBNull(i))
                        {
                            s.Append("<");
                            s.Append(elname);
                            s.Append("/>");
                        }
                        else
                        {
                            if (System.Convert.ToString(rs.GetValue(i)).StartsWith("<ml>", StringComparison.InvariantCultureIgnoreCase))
                            {
                                s.Append("<");
                                s.Append(elname);
                                s.Append(">");
                                s.Append(System.Convert.ToString(rs.GetValue(i)));
                                s.Append("</");
                                s.Append(elname);
                                s.Append(">");
                            }
                            else
                            {
                                s.Append("<");
                                s.Append(elname);
                                s.Append(">");
                                s.Append(System.Convert.ToString(rs.GetValue(i)).Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("'", "&apos;").Replace("\"", "&quot;"));
                                s.Append("</");
                                s.Append(elname);
                                s.Append(">");
                            }
                        }
                    }
                    if (!IsPagingProc)
                    {
                        if (rowEl.Length == 0)
                        {
                            s.Append("</row>");
                        }
                        else
                        {
                            s.Append("</");
                            s.Append(rowEl);
                            s.Append(">");
                        }
                    }
                }
                if (IsPagingProc)
                {
                    if (rootEl.Length != 0)
                    {
                        s.Append("</");
                        s.Append(rootEl);
                        s.Append("Paging>");
                    }
                    else
                    {
                        s.Append("</Paging>");
                    }
                }
                else
                {
                    if (rootEl.Length != 0)
                    {
                        s.Append("</");
                        s.Append(rootEl);
                        s.Append(ResultSet.ToString());
                        s.Append(">");
                    }
                }
            }
            rs.Close();
            Xml = s.ToString();
            return rows;
        }

        static public int GetXml(string Sql, string rootEl, string rowEl, ref string Xml, SqlTransaction trans)
        {
            if (trans == null)
            {
                return GetXml(Sql, rootEl, rowEl, ref Xml);
            }
            return GetXml(Sql, rootEl, rowEl, ref Xml, trans.Connection);
        }

        static public int GetENLocaleXml(SqlDataReader dr, string rootEl, string rowEl, ref StringBuilder Xml)
        {
            int rows = 0;
            if (rootEl.Length != 0)
            {
                Xml.Append("<" + rootEl + ">");
            }
            while (dr.Read())
            {
                ++rows;
                if (rowEl.Length == 0)
                {
                    Xml.Append("<row>");
                }
                else
                {
                    Xml.Append("<" + rowEl + ">");
                }
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    string elname = dr.GetName(i).Replace(" ", "_");
                    if (dr.IsDBNull(i))
                    {
                        Xml.Append("<" + elname + "/>");
                    }
                    else
                    {
                        if (System.Convert.ToString(dr.GetValue(i)).StartsWith("<ml>", StringComparison.InvariantCultureIgnoreCase))
                        {
                            Xml.Append("<" + elname + ">" + System.Convert.ToString(dr.GetValue(i)) + "</" + elname + ">");
                        }
                        else
                        {
                            string FieldVal = string.Empty;
                            string FieldDataType = dr.GetDataTypeName(i).ToLowerInvariant();
                            if ((FieldDataType == "decimal" || FieldDataType == "money") && CommonLogic.Application("DBSQLServerLocaleSetting") != "en-US")
                            {
                                FieldVal = Localization.ParseLocaleDecimal(dr.GetDecimal(i).ToString(), "en-US").ToString();
                            }
                            else if (dr.GetDataTypeName(i).Equals("datetime", StringComparison.InvariantCultureIgnoreCase) && 
                                CommonLogic.Application("DBSQLServerLocaleSetting") != "en-US")
                            {
                                FieldVal = Localization.ParseLocaleDateTime(dr.GetDateTime(i).ToString(), "en-US").ToString();
                            }
                            else
                            {
                                FieldVal = dr.GetValue(i).ToString().Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("'", "&apos;").Replace("\"", "&quot;");
                            }
                            Xml.Append("<" + elname + ">" + FieldVal + "</" + elname + ">");
                        }
                    }
                }
                if (rowEl.Length == 0)
                {
                    Xml.Append("</row>");
                }
                else
                {
                    Xml.Append("</" + rowEl + ">");
                }

            }
            if (rootEl.Length != 0)
            {
                Xml.Append("</" + rootEl + ">");
            }
            int ResultSet = 1;
            while (dr.NextResult())
            {
                ResultSet++;
                if (rootEl.Length != 0)
                {
                    Xml.Append("<" + rootEl + ResultSet.ToString() + ">");
                }
                while (dr.Read())
                {
                    if (rowEl.Length == 0)
                    {
                        Xml.Append("<row>");
                    }
                    else
                    {
                        Xml.Append("<" + rowEl + ">");
                    }
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        string elname = dr.GetName(i).Replace(" ", "_");
                        if (dr.IsDBNull(i))
                        {
                            Xml.Append("<" + elname + "/>");
                        }
                        else
                        {
                            if (System.Convert.ToString(dr.GetValue(i)).StartsWith("<ml>", StringComparison.InvariantCultureIgnoreCase))
                            {
                                Xml.Append("<" + elname + ">" + System.Convert.ToString(dr.GetValue(i)) + "</" + elname + ">");
                            }
                            else
                            {
                                string FieldVal = string.Empty;
                                if (dr.GetDataTypeName(i).Equals("decimal", StringComparison.InvariantCultureIgnoreCase) && 
                                    CommonLogic.Application("DBSQLServerLocaleSetting") != "en-US")
                                {
                                    FieldVal = Localization.ParseLocaleDecimal(dr.GetDecimal(i).ToString(), "en-US").ToString();
                                }
                                else if (dr.GetDataTypeName(i).Equals("datetime", StringComparison.InvariantCultureIgnoreCase) && 
                                    CommonLogic.Application("DBSQLServerLocaleSetting") != "en-US")
                                {
                                    FieldVal = Localization.ParseLocaleDateTime(dr.GetDateTime(i).ToString(), "en-US").ToString();
                                }
                                else
                                {
                                    FieldVal = dr.GetValue(i).ToString().Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("'", "&apos;").Replace("\"", "&quot;");
                                }
                                Xml.Append("<" + elname + ">" + System.Convert.ToString(dr.GetValue(i)).Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("'", "&apos;").Replace("\"", "&quot;") + "</" + elname + ">");
                            }
                        }
                    }
                    if (rowEl.Length == 0)
                    {
                        Xml.Append("</row>");
                    }
                    else
                    {
                        Xml.Append("</" + rowEl + ">");
                    }
                }
                if (rootEl.Length != 0)
                {
                    Xml.Append("</" + rootEl + ResultSet.ToString() + ">");
                }
            }
            dr.Close();
            return rows;
        }

        static public void ExecuteLongTimeSQL(String Sql, String DBConnString, int TimeoutSecs)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            SqlConnection dbconn = new SqlConnection();
            dbconn.ConnectionString = DB.GetDBConn();
            dbconn.Open();
            SqlCommand cmd = new SqlCommand(Sql, dbconn);
            cmd.CommandTimeout = TimeoutSecs;
            try
            {
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                dbconn.Close();
                dbconn.Dispose();
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                dbconn.Close();
                dbconn.Dispose();
                throw (ex);
            }
        }

        static public void ExecuteLongTimeSQL(String Sql, String DBConnString, int TimeoutSecs, SqlConnection dbconn)
        {
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            SqlCommand cmd = new SqlCommand(Sql, dbconn);
            cmd.CommandTimeout = TimeoutSecs;
            try
            {
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                throw (ex);
            }
        }

        static public void ExecuteLongTimeSQL(String Sql, String DBConnString, int TimeoutSecs, SqlTransaction trans)
        {
            if (trans == null)
            {
                ExecuteLongTimeSQL(Sql, DBConnString, TimeoutSecs);
                return;
            }
            if (CommonLogic.ApplicationBool("DumpSQL"))
            {
                HttpContext.Current.Response.Write("SQL=" + Sql + "<br/>\n");
            }
            SqlCommand cmd = new SqlCommand(Sql, trans.Connection, trans);
            cmd.CommandTimeout = TimeoutSecs;
            try
            {
                cmd.ExecuteNonQuery();
                cmd.Dispose();
            }
            catch (Exception ex)
            {
                cmd.Dispose();
                throw (ex);
            }
        }


        static public void LogSql(string LogText)
        {
            SqlConnection cn = new SqlConnection(DB.GetDBConn());
            try
            {
                cn.Open();
            }
            catch { return; }
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "insert dbo.sqllog (SQLText, ExecutedBy, ExecutedOn) values(@logText, 1, getdate())";
            cmd.Parameters.Add(new SqlParameter("@logText", SqlDbType.NText, 1000000000));
            cmd.Parameters["@logText"].Value = LogText;
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch { }

            cmd.Dispose();
            cn.Close();
            cn.Close();

        }
    }

    // currently only supported for SQL Server:
    public class DBTransaction
    {
        private ArrayList sqlCommands = new ArrayList(10);

        public DBTransaction() { }

        public void AddCommand(String Sql)
        {
            sqlCommands.Add(Sql);
        }

        public void ClearCommands()
        {
            sqlCommands.Clear();
        }

        // returns true if no errors, or false if ANY Exception is found:
        public bool Commit()
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = DB.GetDBConn();
            conn.Open();
            SqlTransaction trans = conn.BeginTransaction();
            bool status = false;
            try
            {

                foreach (String s in sqlCommands)
                {
                    SqlCommand comm = new SqlCommand(s, conn);
                    comm.Transaction = trans;
                    comm.ExecuteNonQuery();
                }
                trans.Commit();
                status = true;
            }
            catch 
            {
                trans.Rollback();
                status = false;
            }
            finally
            {
                conn.Close();
            }
            return status;
        }

    }
}
