﻿using InterpriseSuiteEcommerceCommon.DTO;
using System.Collections.Generic;

namespace InterpriseSuiteEcommerceCommon.StoreLocator
{
    public interface ILocatorSetting
    {
        string AuthenticationTokenKey { get; set; }
        bool Sensor { get; set; }
        string Version { get; set; }

        LocatorOption Opton { get; set; }
    }
}