﻿using InterpriseSuiteEcommerceCommon.DTO;
using System.Collections.Generic;

namespace InterpriseSuiteEcommerceCommon.StoreLocator
{
    public class GooleMapLocatorSetting : ILocatorSetting
    {
        public GooleMapLocatorSetting() { }

        public GooleMapLocatorSetting(string authenticationTokenKey, bool sensor, string version)
        {
            AuthenticationTokenKey = authenticationTokenKey;
            Sensor = sensor;
            Version = version;
        }

        public string AuthenticationTokenKey { get; set; }
        public string Version { get; set; }
        public bool Sensor { get; set; }

        public LocatorOption Opton { get; set; }

        public IEnumerable<Marker> Markers { get; set; }
    }


}