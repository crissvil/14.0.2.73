--ALTER TABLE CRMCOntact
--ADD BillingCCCode_DEV004817 nvarchar(30);

--ALTER TABLE CRMCOntact
--ADD ShipToCode_DEV004817 nvarchar(30);
/************************************************************************************************************************/

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'blog.aspx.1')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'blog.aspx.1', B.ShortString, 'Blog', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.6')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.6', B.ShortString, '<font style="font-size:18px">AVAILABLE NOW</font><br/><font style="font-weight:100; font-size:14px">(Unprinted) Next day dispatch</font>', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.7')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.7', B.ShortString, '<font style="font-size:18px">CUSTOM BRANDED BAGS</font><br/><font style="font-weight:100; font-size:14px">6-8 weeks delivery</font>', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.8')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.8', B.ShortString, '<font style="font-size:18px">EXPRESS PRINTED RETAIL</font><br/><font style="font-weight:100; font-size:14px">10 Days delivery*</font>', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.9')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.9', B.ShortString, '<font style="font-size:18px">AVAILABLE NOW</font><br/><font style="font-weight:100; font-size:14px">(Express Printed) 10 days delivery</font>', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.10')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.10', B.ShortString, 'Use First Name & Last Name if not from a Company', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'opc.shipping.method.description')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'opc.shipping.method.description', B.ShortString, 'Orders after 3pm will not be dispatched next day. Please allow extra days for orders in Northern Queensland, WA, NT and Tasmania.', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.1')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.1', B.ShortString, '(Corporate Office)', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.11')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.11', B.ShortString, '{0} Portal Order Confirmation', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.12')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.12', B.ShortString, 'Price (ex GST)', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.13')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.13', B.ShortString, 'Your Store ID is:', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.14')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.14', B.ShortString, 'Your Store Name is:', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.15')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.15', B.ShortString, 'Movex Number:', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.16')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.16', B.ShortString, 'My Shipping Address', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.17')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.17', B.ShortString, 'Shipping Contact', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.18')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.18', B.ShortString, 'Billing Contact', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.19')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.19', B.ShortString, 'Terms & Conditions', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.20')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.20', B.ShortString, 'This is the terms and condition. You can change the value by going to String Resource and find the value custom.text.20', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.21')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.21', B.ShortString, 'Charge Account', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.22')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.22', B.ShortString, 'Smartbag New Quote Notification', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.23')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.23', B.ShortString, '<i class="fa fa-cart-plus"></i>&nbsp;Start your Design', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.24')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.24', B.ShortString, 'Estimated freight amount (ex GST):', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.25')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.25', B.ShortString, 'The password you entered is incorrect. Please try again or enter your email in �Forgot your password� where a new password will be emailed.', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.26')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.26', B.ShortString, 'Problem encountered computing your shipping rate. Please check your shipping address if correct.', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.27')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.27', B.ShortString, '<div style="font-weight: 700; font-size: 22px; color: #656262;">15% off your next order!</div>', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'custom.text.28')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'custom.text.28', B.ShortString, 'You have existing account, please <a href="signin.aspx" style="font-size:15px;">login.</a>', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.2')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.2', B.ShortString, 'Product Code', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.3')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.3', B.ShortString, 'Name', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.4')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.4', B.ShortString, 'In WH', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.5')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.5', B.ShortString, 'Order Point', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.6')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.6', B.ShortString, 'On Order', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.7')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.7', B.ShortString, 'Re-Order Qty', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.8')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.8', B.ShortString, 'Date of Approval', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.9')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.9', B.ShortString, 'ETA', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.10')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.10', B.ShortString, 'Approved', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.11')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.11', B.ShortString, 'Purchase Order', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.12')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.12', B.ShortString, 'Backorder Item', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.13')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.13', B.ShortString, 'Product Description', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.14')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.14', B.ShortString, 'Price', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.15')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.15', B.ShortString, '1st Qtr Sales', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.16')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.16', B.ShortString, '2nd Qtr Sales', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.17')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.17', B.ShortString, '3rd Qtr Sales', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.18')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.18', B.ShortString, '4th Qtr Sales', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.19')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.19', B.ShortString, 'YTD Sales', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.20')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.20', B.ShortString, '2016/2017 Sales', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.21')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.21', B.ShortString, 'SOH Units', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.22')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.22', B.ShortString, 'UOM', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.23')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.23', B.ShortString, 'SOH in Months', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.24')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.24', B.ShortString, 'Stock On Order (SOO)', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.25')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.25', B.ShortString, 'Stock On Order Value $', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.26')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.26', B.ShortString, 'SOH + SOO in Months', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.27')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.27', B.ShortString, 'Value $', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.28')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.28', B.ShortString, 'Production Source', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.29')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.29', B.ShortString, 'On Backorder', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.30')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.30', B.ShortString, 'YTD Sales Value', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.31')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.31', B.ShortString, 'Status', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.32')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.32', B.ShortString, 'Min Order', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.33')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.33', B.ShortString, 'Max Order', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.34')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.34', B.ShortString, 'Any cancellations must be made no later than 1:00PM on the same day as the order.', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.35')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.35', B.ShortString, 'Filename', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.36')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.36', B.ShortString, 'Date Created', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.37')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.37', B.ShortString, 'Type', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.38')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.38', B.ShortString, 'Re-Order Status', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.39')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.39', B.ShortString, 'PO Number', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.39')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.39', B.ShortString, 'PO Number', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.40')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.40', B.ShortString, 'Declined', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.41')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.41', B.ShortString, 'Declined Date', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.42')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.42', B.ShortString, 'Approved', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.43')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.43', B.ShortString, 'Harvey Norman Purchase order ready for your approval', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.44')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.44', B.ShortString, 'You are now ready to complete your order. Please review your order below, and then click the ''{0}'' button only once below to process your order...', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.45')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.45', B.ShortString, '<b>NOTE:</b> Please input your first and last name in the comments above to confirm the order.', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.46')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.46', B.ShortString, 'Comments', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.47')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.47', B.ShortString, 'Please enter your first and last name', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.48')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.48', B.ShortString, 'Unit Price <br/> (ex GST)', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.49')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.49', B.ShortString, 'Sub Total <br/> (ex GST)', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.50')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.50', B.ShortString, 'On Backorder', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.51')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.51', B.ShortString, '2017/2018 Sales', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.52')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.52', B.ShortString, 'SOH Value', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.53')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.53', B.ShortString, 'APG Purchase order ready for your approval', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.54')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.54', B.ShortString, 'CUE Purchase order ready for your approval', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.55')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.55', B.ShortString, 'Dion Lee Purchase order ready for your approval', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END

IF NOT EXISTS (SELECT 1 FROM EcommerceStringResource WHERE Name = 'portal.aspx.56')
BEGIN
INSERT INTO EcommerceStringResource (StringResourceGUID, WebSiteCode, Name, LocaleSetting, ConfigValue, CreatedOn, UserCreated, Published)
SELECT NEWID(), WebSiteCode, 'portal.aspx.56', B.ShortString, 'Smart Colour Print Purchase order ready for your approval', GETDATE(), 'Admin', 1 FROM EcommerceSite A CROSS JOIN (SELECT ShortString FROM SystemSellingLanguageView WHERE IsIncluded = 1) B
END


UPDATE EcommerceStringResource SET Configvalue = 'Search Products' WHERE Name = 'search.aspx.1'
UPDATE EcommerceStringResource SET Configvalue = 'Sign In' WHERE Name = 'signin.aspx.5'
UPDATE EcommerceStringResource SET Configvalue = 'LOGIN' WHERE Name = 'signin.aspx.16'
UPDATE EcommerceStringResource SET Configvalue = 'Continue Shopping' WHERE Name = 'shoppingcart.cs.12'
UPDATE EcommerceStringResource SET Configvalue = 'CHECKOUT NOW' WHERE Name = 'shoppingcart.cs.34'
UPDATE EcommerceStringResource SET Configvalue = 'Carton Selling Price' WHERE Name = 'common.cs.25'
UPDATE EcommerceStringResource SET Configvalue = 'Shipping:' WHERE Name = 'shoppingcart.aspx.10'
UPDATE EcommerceStringResource SET Configvalue = 'GST:' WHERE Name = 'shoppingcart.aspx.11'
UPDATE EcommerceStringResource SET Configvalue = 'Estimate Shipping' WHERE Name = 'shoppingcart.aspx.20'
UPDATE EcommerceStringResource SET Configvalue = 'First Name<font style="color:red">*</font>' WHERE Name = 'createaccount.aspx.134'
UPDATE EcommerceStringResource SET Configvalue = 'Last Name<font style="color:red">*</font>' WHERE Name = 'createaccount.aspx.135'
UPDATE EcommerceStringResource SET Configvalue = 'Telephone<font style="color:red">*</font>' WHERE Name = 'createaccount.aspx.142'
UPDATE EcommerceStringResource SET Configvalue = 'Email Address<font style="color:red">*</font>' WHERE Name = 'createaccount.aspx.155'
UPDATE EcommerceStringResource SET Configvalue = 'Password<font style="color:red">*</font>' WHERE Name = 'createaccount.aspx.140'
UPDATE EcommerceStringResource SET Configvalue = 'Confirm Password<font style="color:red">*</font>' WHERE Name = 'createaccount.aspx.141'
UPDATE EcommerceStringResource SET Configvalue = 'Address<font style="color:red">*</font>' WHERE Name = 'createaccount.aspx.128'
UPDATE EcommerceStringResource SET Configvalue = 'Postcode<font style="color:red">*</font>' WHERE Name = 'createaccount.aspx.130'
UPDATE EcommerceStringResource SET Configvalue = 'State<font style="color:red">*</font>' WHERE Name = 'createaccount.aspx.129'
UPDATE EcommerceStringResource SET Configvalue = 'City/Suburb<font style="color:red">*</font>' WHERE Name = 'createaccount.aspx.84'
UPDATE EcommerceStringResource SET Configvalue = 'Thanks for your Smartbag order' WHERE Name = 'orderconfirmation.aspx.3'
UPDATE EcommerceStringResource SET Configvalue = 'We will send a confirmation email shortly.' WHERE Name = 'orderconfirmation.aspx.9'
UPDATE EcommerceStringResource SET Configvalue = 'For a printable tax invoice:' WHERE Name = 'orderconfirmation.aspx.13'
UPDATE EcommerceStringResource SET Configvalue = 'Download PDF Invoice' WHERE Name = 'orderconfirmation.aspx.14'
UPDATE EcommerceStringResource SET Configvalue = 'Payment Contact' WHERE Name = 'checkout1.aspx.37'
UPDATE EcommerceStringResource SET Configvalue = 'Smartbag New Order Receipt' WHERE Name = 'common.cs.1'
UPDATE EcommerceStringResource SET Configvalue = 'Smartbag VIP' WHERE Name = 'createaccount.aspx.27'
UPDATE EcommerceStringResource SET Configvalue = 'That email address is already used by another customer' WHERE Name IN ('createaccount.aspx.94','account.aspx.70')
UPDATE EcommerceStringResource SET Configvalue = 'Billing Address' WHERE Name = 'checkoutreview.aspx.8'
UPDATE EcommerceStringResource SET Configvalue = 'Contact Name<font style="color:red">*</font>' WHERE Name = 'checkout1.aspx.79'
UPDATE EcommerceStringResource SET Configvalue = 'Telephone<font style="color:red">*</font>' WHERE Name = 'checkout1.aspx.66'
UPDATE EcommerceStringResource SET Configvalue = 'Address<font style="color:red">*</font>' WHERE Name = 'checkout1.aspx.62'
UPDATE EcommerceStringResource SET Configvalue = 'City<font style="color:red">*</font>' WHERE Name = 'checkout1.aspx.71'
UPDATE EcommerceStringResource SET Configvalue = 'State<font style="color:red">*</font>' WHERE Name = 'checkout1.aspx.72'
UPDATE EcommerceStringResource SET Configvalue = 'Postcode<font style="color:red">*</font>' WHERE Name = 'checkout1.aspx.70'
UPDATE EcommerceStringResource SET Configvalue = 'Company Name<font style="color:red">*</font>' WHERE Name = 'createaccount.aspx.139'
UPDATE EcommerceStringResource SET Configvalue = ' ' WHERE Name IN ('createaccount.aspx.131', 'checkout1.aspx.76', 'customersupport.aspx.40', 'shoppingcart.aspx.47', 'selectaddress.aspx.21', 'leadform.aspx.36', 'editaddress.aspx.9', 'checkoutpayment.aspx.32')
UPDATE EcommerceStringResource SET Configvalue = 'Email<font style="color:red">*</font>' WHERE Name = 'checkout1.aspx.65'
UPDATE EcommerceStringResource SET Configvalue = 'First Name<font style="color:red">*</font>' WHERE Name = 'account.aspx.55'
UPDATE EcommerceStringResource SET Configvalue = 'Last Name<font style="color:red">*</font>' WHERE Name = 'account.aspx.56'
UPDATE EcommerceStringResource SET Configvalue = 'Telephone<font style="color:red">*</font>' WHERE Name = 'account.aspx.58'
UPDATE EcommerceStringResource SET Configvalue = 'Emaill Address<font style="color:red">*</font>' WHERE Name = 'account.aspx.88'
UPDATE EcommerceStringResource SET Configvalue = 'Your current password<font style="color:red">*</font>' WHERE Name = 'account.aspx.36'
UPDATE EcommerceStringResource SET Configvalue = 'New Password<font style="color:red">*</font>' WHERE Name = 'account.aspx.59'
UPDATE EcommerceStringResource SET Configvalue = 'Retype your new password<font style="color:red">*</font>' WHERE Name = 'account.aspx.60'
UPDATE EcommerceStringResource SET Configvalue = 'Select One' WHERE Name = 'account.aspx.61'
UPDATE EcommerceStringResource SET Configvalue = 'Save Changes' WHERE Name = 'editaddress.aspx.3'
UPDATE EcommerceStringResource SET Configvalue = 'Email Address<font style="color:red">*</font>' WHERE Name = 'contactus.aspx.5'
UPDATE EcommerceStringResource SET Configvalue = 'Anti-Spam Code<font style="color:red">*</font>' WHERE Name = 'customersupport.aspx.13'
UPDATE EcommerceStringResource SET Configvalue = 'My Billing and Shipping Address' WHERE Name = 'account.aspx.67'
UPDATE EcommerceStringResource SET Configvalue = 'My Billing Address' WHERE Name = 'selectaddress.aspx.1'
UPDATE EcommerceStringResource SET Configvalue = 'Shipping Address' WHERE Name = 'checkout1.aspx.67'
UPDATE EcommerceStringResource SET Configvalue = 'Postcode' WHERE Configvalue = 'Postal'
UPDATE EcommerceStringResource SET Configvalue = 'Company<font style="color:red">*</font>' WHERE Name = 'account.aspx.38'
UPDATE EcommerceStringResource SET Configvalue = 'Name On Card<font style="color:red">*</font>' WHERE Name = 'checkout1.aspx.50'
UPDATE EcommerceStringResource SET Configvalue = 'Credit Card Number<font style="color:red">*</font>' WHERE Name = 'checkout1.aspx.51'
UPDATE EcommerceStringResource SET Configvalue = 'Credit Card Verification Code<font style="color:red">*</font>' WHERE Name = 'checkout1.aspx.52'
UPDATE EcommerceStringResource SET Configvalue = 'Card Type<font style="color:red">*</font>' WHERE Name = 'checkout1.aspx.53'
UPDATE EcommerceStringResource SET Configvalue = 'Expiration Date<font style="color:red">*</font>' WHERE Name = 'checkout1.aspx.54'
UPDATE EcommerceStringResource SET Configvalue = 'Submit' WHERE Name = 'portal.aspx.10'
UPDATE EcommerceStringResource SET Configvalue = '<font class="left-menu-title">AVAILABLE NOW</font><br/><font class="left-menu-sub-title">(Unprinted) Next day dispatch</font>' WHERE Name = 'custom.text.6'
UPDATE EcommerceStringResource SET Configvalue = '<font class="left-menu-title">CUSTOM BRANDED BAGS</font><br/><font class="left-menu-sub-title">6-8 weeks delivery</font>' WHERE Name = 'custom.text.7'
UPDATE EcommerceStringResource SET Configvalue = '<font class="left-menu-title">EXPRESS PRINTED RETAIL</font><br/><font class="left-menu-sub-title">10 Days delivery*</font>' WHERE Name = 'custom.text.8'
UPDATE EcommerceStringResource SET Configvalue = '<font class="left-menu-title">AVAILABLE NOW</font><br/><font class="left-menu-sub-title">(Express Printed) 10 days delivery</font>' WHERE Name = 'custom.text.9'
/************************************************************************************************************************/
IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.default.shipping.method')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.default.shipping.method', 'This is the default shipping method for all users.', 'Next Day Dispatch', 'CUSTOM', 0, GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.states')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.states', 'Smartbag supported states.', 'QLD:Queensland,NSW:New South Wales,VIC:Victoria,SA:South Australia,TAS:Tasmania,WA:Western Australia,NT:Northern Territory,ACT:Australian Capital Territory', 'CUSTOM', 0, GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.postal.codes')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.postal.codes', 'Smartbag supported Postal codes.', 'QLD:Queensland,NSW:New South Wales,VIC:Victoria,SA:South Australia,TAS:Tasmania,WA:Western Australia,NT:Northern Territory,ACT:Australian Capital Territory', 'CUSTOM', 0, GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.industrial.group')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.industrial.group', 'Custom appconfig.', 'Architecture,Automotive,Chemicals,Dental,Education,Engineering,Financial Service,Government,Hospitality,IT,Manufacturing,Marine,Primary,Retail,Other', 'CUSTOM', 0, GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.contact.us.title')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.contact.us.title', 'Custom appconfig.', 'Smart Bag Customer Inquiry', 'CUSTOM', 0, GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.template')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.template', 'Company Portal template to use. Include the .ascx extension also in what you enter.', 'portal.ascx', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.soh.report')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.soh.report', 'Company Portal SOH report.', 'RPT-000026', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.delivery.item')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.delivery.item', 'Harvey Norman Delivery Item. Use Item Name as value', 'HN Delivery', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.order.history.report')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.order.history.report', 'Harvey Norman Order History Report. Specify the Report Code.', 'RPT-000023', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.email.notification', 'Enter email address to be used in sending Order Notification for Harvey Norman', 'portals@colorcraftprinting.com.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.invoice.history.report')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.invoice.history.report', 'Harvey Norman Invoice History Report. Specify the Report Code.', 'RPT-000018', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.ftp.url')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.ftp.url', 'Harvey Norman FTP URL for CSV upload', 'ftp://myobftp.sge.net.au/', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.ftp.username')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.ftp.username', 'Harvey Norman FTP username for CSV upload', 'myob.feeds', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.ftp.password')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.ftp.password', 'Harvey Norman FTP password for CSV upload', 'Bucket101', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.print.api.url')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.print.api.url', 'URL location of print api', 'http://111.67.4.85/sgeprintapi', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.print.api.username')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.print.api.username', 'Print api username', '8183eb64-d350-4e22-b42b-9d4c3d570267', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.print.api.password')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.print.api.password', 'Print api password', 'A06OSiiV59YPZaskNJFi0eewaBPT0SoHcJQXRcgi6yI=', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.userlist.report')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.userlist.report', 'Company Portal User List report.', 'RPT-000050', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.producthistory.report')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.producthistory.report', 'Company Portal Product History report.', 'RPT-000054', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.pickpack.item')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.pickpack.item', 'Harvey Norman Pick & Pack Item. Use Item Name as value', 'HNH1000002', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.approval.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.approval.email.notification', 'Harvey Norman Re-order Approvals email recipient notification', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.apg.order.history.report')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.apg.order.history.report', 'APG Order History Report. Specify the Report Code.', 'RPT-000023', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.apg.invoice.history.report')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.apg.invoice.history.report', 'APG Invoice History Report. Specify the Report Code.', 'RPT-000073', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.apg.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.apg.email.notification', 'Enter email address to be used in sending Order Notification for APG', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.cue.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.cue.email.notification', 'Enter email address to be used in sending Order Notification for CUE', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.smartbag.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.smartbag.email.notification', 'Enter email address to be used in sending Order Notification for Smartbag', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.smartbag.neworder.admin.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.smartbag.neworder.admin.email.notification', 'Who gets notified of new orders. This is typically the e-mail address that the store administrator monitors.', 'sales@smartbag.com.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.neworder.admin.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.neworder.admin.email.notification', 'Who gets notified of new orders. This is typically the e-mail address that the store administrator monitors.', 'portals@colorcraftprinting.com.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.apg.neworder.admin.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.apg.neworder.admin.email.notification', 'Who gets notified of new orders. This is typically the e-mail address that the store administrator monitors.', 'sgeportals@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.cue.neworder.admin.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.cue.neworder.admin.email.notification', 'Who gets notified of new orders. This is typically the e-mail address that the store administrator monitors.', 'sgeportals@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.cue.order.history.report')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.cue.order.history.report', 'CUE Order History Report. Specify the Report Code.', 'RPT-000023', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.cue.invoice.history.report')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.cue.invoice.history.report', 'CUE Invoice History Report. Specify the Report Code.', 'RPT-000073', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.dl.neworder.admin.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.dl.neworder.admin.email.notification', 'Who gets notified of new orders. This is typically the e-mail address that the store administrator monitors.', 'sgeportals@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.dl.order.history.report')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.dl.order.history.report', 'Dion Lee Order History Report. Specify the Report Code.', 'RPT-000023', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.dl.invoice.history.report')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.dl.invoice.history.report', 'Dion Lee Invoice History Report. Specify the Report Code.', 'RPT-000073', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.dl.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.dl.email.notification', 'Enter email address to be used in sending Order Notification for Dion Lee', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.approval.approved.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.approval.approved.email.notification', 'Harvey Norman Re-order Approvals Approved email recipient notification. This email will be sent when a Re-order Approval is approved.', 'HN-orders@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.approval.declined.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.approval.declined.email.notification', 'Harvey Norman Re-order Approvals Declined email recipient notification. This email will be sent when a Re-order Approval is declined.', 'HN-orders@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.apg.approval.approved.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.apg.approval.approved.email.notification', 'APG Re-order Approvals Approved email recipient notification. This email will be sent when a Re-order Approval is approved.', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.apg.approval.declined.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.apg.approval.declined.email.notification', 'APG Re-order Approvals Declined email recipient notification. This email will be sent when a Re-order Approval is declined.', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.cue.approval.approved.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.cue.approval.approved.email.notification', 'CUE Re-order Approvals Approved email recipient notification. This email will be sent when a Re-order Approval is approved.', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.cue.approval.declined.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.cue.approval.declined.email.notification', 'CUE Re-order Approvals Declined email recipient notification. This email will be sent when a Re-order Approval is declined.', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.dl.approval.approved.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.dl.approval.approved.email.notification', 'Dion Lee Re-order Approvals Approved email recipient notification. This email will be sent when a Re-order Approval is approved.', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.dl.approval.declined.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.dl.approval.declined.email.notification', 'Dion Lee Re-order Approvals Declined email recipient notification. This email will be sent when a Re-order Approval is declined.', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.dl.approval.undeclined.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.dl.approval.undeclined.email.notification', 'Dion Lee Re-order Approvals UnDeclined email recipient notification. This email will be sent when a Re-order Approval is undeclined.', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.cue.approval.undeclined.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.cue.approval.undeclined.email.notification', 'CUE Re-order Approvals UnDeclined email recipient notification. This email will be sent when a Re-order Approval is undeclined.', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.apg.approval.undeclined.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.apg.approval.undeclined.email.notification', 'APG Re-order Approvals UnDeclined email recipient notification. This email will be sent when a Re-order Approval is undeclined.', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.approval.undeclined.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.approval.undeclined.email.notification', 'Harvey Norman Re-order Approvals UnDeclined email recipient notification. This email will be sent when a Re-order Approval is undeclined.', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.pallet.fee.cartons.item')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.pallet.fee.cartons.item', 'Harvey Norman Pallet Fee Cartons Item. Use Item Name as value', 'HNH1000002', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.hn.pallet.fee.satchel.item')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.hn.pallet.fee.satchel.item', 'Harvey Norman Pallet Fee Satchel Item. Use Item Name as value', 'HNH1000003', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.smartbag.quote.report')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.smartbag.quote.report', 'Smartbag Quote Report. Specify the Report Code.', 'RPT-000023', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.apg.approval.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.apg.approval.email.notification', 'APG Re-order Approvals email recipient notification', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.dl.approval.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.dl.approval.email.notification', 'Dion-Lee Re-order Approvals email recipient notification', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.cue.approval.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.cue.approval.email.notification', 'Cue Re-order Approvals email recipient notification', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'LiveServer-Company')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'LiveServer-Company', 'The domain of the live site. Just domain.com, no HTTP, subdomains or other stuff.', 'companydomain.com', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.smartbag.wedesign.artcost')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.smartbag.wedesign.artcost', 'Let us design for you additional art cost. Enter item name.', 'zsetup', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.smartbag.printed.colours')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.smartbag.printed.colours', 'Custom printed colours dropdown content. Values should be Item Name and comma separated. E.g. ''item 1'',''item 2''', '', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.smartbag.shipping.method.rate.name')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.smartbag.shipping.method.rate.name', 'Shipping Method Name for setting custom rate. Shipping Method should be part of the Customer''s Shipping Method Group.', 'Next Day Dispatch', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.smartbag.shipping.rate.baseurl')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.smartbag.shipping.rate.baseurl', 'Toll Ipec shipping API URL', 'https://api.openfreight.com.au/', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.smartbag.shipping.rate.username')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.smartbag.shipping.rate.username', 'Toll Ipec shipping API Username', 'SMARTBAG', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.smartbag.shipping.rate.key')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.smartbag.shipping.rate.key', 'Toll Ipec shipping API key', '922251c2dc4957915dd14ba699e78dbfc370cd6ca8f7aee37fd66152dfb1d8f7', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.shipping.extra.handling.fee')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.shipping.extra.handling.fee', 'This extra percentage amount will be added to all shipping rate. Value should be whole number and will be computed in percent additional to the freight rate.', '', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.smartbag.artwork.fee.item')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.smartbag.artwork.fee.item', 'Item Name to be added for Artwork Fee.', '', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.print.approval.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.print.approval.email.notification', 'Smart Colour Print Re-order Approvals email recipient notification', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.print.neworder.admin.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.print.neworder.admin.email.notification', 'Who gets notified of new orders. This is typically the e-mail address that the store administrator monitors.', 'sgeportals@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

IF NOT EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE Name = 'custom.print.email.notification')
BEGIN
	INSERT INTO EcommerceAppConfig (AppConfigGUID, WebSiteCode, Name, Description, ConfigValue, GroupName, SuperOnly, CreatedOn, DateModified, UserModified, DateCreated, UserCreated, Published, Modified)
	SELECT NEWID(), WebSiteCode, 'custom.print.email.notification', 'Enter email address to be used in sending Order Notification for Smart Colour Print', 'donna@sge.net.au', 'CUSTOM', '0', GETDATE(), GETDATE(), 'admin', GETDATE(), 'admin', 1, 0 FROM EcommerceSite
END

UPDATE EcommerceAppConfig SET Configvalue = 'false' WHERE Name = 'SiteMap.ShowDepartments'
UPDATE EcommerceAppConfig SET Configvalue = 'false' WHERE Name = 'ShippingCalculator.Enabled'
UPDATE EcommerceAppConfig SET Configvalue = 'false' WHERE Name = 'RequireOver13Checked'
UPDATE EcommerceAppConfig SET Configvalue = 'true' WHERE Name = 'PayPalCheckout.AllowAnonCheckout'
UPDATE EcommerceAppConfig SET Configvalue = 'true' WHERE Name = 'PayPalCheckout.ShowOnCartPage'
UPDATE EcommerceAppConfig SET Configvalue = 'Credit Card, Bank Transfer, Invoice' WHERE Name = 'OnlinePaymentTermsAllowed'
UPDATE EcommerceAppConfig SET Configvalue = 'false' WHERE Name = 'GiftCode.Enabled'
UPDATE EcommerceAppConfig SET Configvalue = 'false' WHERE Name = 'Inventory.LimitCartToQuantityOnHand'
UPDATE EcommerceAppConfig SET Configvalue = 'Smartportal' WHERE Name = 'StoreName'
UPDATE EcommerceAppConfig SET Configvalue = 'true' WHERE Name = 'RequireTermsAndConditionsAtCheckout'
DELETE EcommerceAppConfig WHERE Name = 'custom.hn.pickpack.item'
/************************************************************************************************************************/
UPDATE SystemSalutation SET IsActive = 0
UPDATE SystemSalutation SET IsActive = 1 WHERE SalutationDescription IN ('Mr','Miss','Mrs','Ms')
UPDATE InventoryItemWebOption SET CheckOutOption = 1 WHERE ItemCode IN (SELECT ItemCode FROM InventoryItem where ItemName = 'HN Delivery')
UPDATE CRMContact SET SubscriptionExpirationOn = NULL
/************************************************************************************************************************/
IF NOT EXISTS (SELECT 1 FROM CustomerType WHERE CustomerTypeCode = 'Company')
BEGIN
INSERT INTO CustomerType (CustomerTypeCode, CustomerTypeDescription, IsActive, UserCreated, DateCreated, UserModified, DateModified)
VALUES ('Company Portal', 'Smartbag Portal Company', 1, 'admin', GETDATE(), 'admin', GETDATE())
END

IF NOT EXISTS (SELECT 1 FROM SystemJobRole WHERE JobRoleCode = 'Store')
BEGIN
INSERT INTO SystemJobRole (JobRoleCode, JobDescription, IsActive, UserCreated, DateCreated, UserModified, DateModified)
VALUES ('Store', 'Smartbag Store Account', 1, 'admin', GETDATE(), 'admin', GETDATE())
END

IF NOT EXISTS (SELECT 1 FROM SystemJobRole WHERE JobRoleCode = 'Store Admin')
BEGIN
	INSERT INTO SystemJobRole (JobRoleCode, JobDescription, IsActive, UserCreated, DateCreated, UserModified, DateModified)
	VALUES('Store Admin', 'Smartbag Store Admin Account', 1, 'admin', GETDATE(), 'admin', GETDATE())
END

/************************************************************************************************************************/
exec createecommercetopic @TopicID=N'TOPI-000087',@WebSiteCode=N'WEB-000001',@TopicGUID='0C653B06-378D-4A02-8905-2C5EC00257A0',@Password=NULL,@PasswordIV=NULL,@PasswordSalt=NULL,
@RequiresSubscription=0,@RequiresDisclaimer=0,@XmlPackage=NULL,@ExtensionData=NULL,@ShowInSiteMap=1,@SKinID=1,@ContentsBGColor=NULL,@PageBGColor=NULL,@GraphicsColor=NULL,@HTMLOk=1,
@CreatedOn='2017-05-05 23:12:38.143',@UserCreated=N'admin',@DateCreated='2017-05-05 23:16:32.747',@UserModified=N'admin',@DateModified='2017-05-05 23:16:32.747',@MLID=NULL,@Modified=NULL

exec createecommercetopiclanguage @TopicID=N'TOPI-000087',@WebSiteCode=N'WEB-000001',@LanguageCode=N'English - Australia',@Name=N'Portal Homepage',@Title=N'Portal',@TopicContent=N'&nbsp;
<DIV style="FONT-SIZE: 14px">
<DIV style="FONT-SIZE: 25px; FONT-WEIGHT: 700">Welcome (!PORTAL_COMPANY!) </DIV>
<DIV style="PADDING-TOP: 20px">This is your ONLINE ordering portal for all your Packaging products </DIV>
<DIV style="PADDING-TOP: 20px">Following are 4 simple steps to placing your order for all your packaging needs. </DIV>
<DIV style="TEXT-DECORATION: underline; FONT-WEIGHT: 700; PADDING-TOP: 20px">TO PLACE AN ORDER </DIV>
<DIV style="PADDING-TOP: 5px">1.&nbsp;&nbsp;Click on PLACE ORDER</DIV>
<DIV style="PADDING-TOP: 5px">2.&nbsp;&nbsp;Select a QTY then PRODUCTS</DIV>
<DIV style="PADDING-TOP: 5px">3.&nbsp;&nbsp;Check Order Items �you can Cancel, change or delete any item</DIV>
<DIV style="PADDING-TOP: 5px">4.&nbsp;&nbsp;Confirm Shipping address</DIV>
<DIV class=height-20></DIV>
<DIV style="TEXT-DECORATION: underline; FONT-WEIGHT: 700; PADDING-TOP: 20px">ORDER HISTORY </DIV>
<DIV style="LINE-HEIGHT: 20px">You can view all your order details by clicking on ORDER HISTORY <BR>By clicking on the + symbol next to the order number you can view exactly what was ordered. <BR>For any problems with your order or your delivery, please call <B>1300 874 559.</B> </DIV></DIV>
<DIV class=height-20></DIV>
<DIV class=height-20></DIV>',@SETitle=NULL,@SEDescription=NULL,@SEKeywords=NULL,@SENoScript=NULL,@ExtensionData=NULL,@UserCreated=N'admin',@DateCreated='2017-05-05 23:16:32.747',@UserModified=N'admin',@DateModified='2017-05-05 23:16:32.747',@MLID=NULL,@Modified=0

exec createecommercetopiclanguage @TopicID=N'TOPI-000087',@WebSiteCode=N'WEB-000001',@LanguageCode=N'English - United States',@Name=N'Portal Homepage',@Title=N'Portal',@TopicContent=N'&nbsp;
<DIV style="FONT-SIZE: 14px">
<DIV style="FONT-SIZE: 25px; FONT-WEIGHT: 700">Welcome (!PORTAL_COMPANY!) </DIV>
<DIV style="PADDING-TOP: 20px">This is your ONLINE ordering portal for all your Packaging products </DIV>
<DIV style="PADDING-TOP: 20px">Following are 4 simple steps to placing your order for all your packaging needs. </DIV>
<DIV style="TEXT-DECORATION: underline; FONT-WEIGHT: 700; PADDING-TOP: 20px">TO PLACE AN ORDER </DIV>
<DIV style="PADDING-TOP: 5px">1.&nbsp;&nbsp;Click on PLACE ORDER</DIV>
<DIV style="PADDING-TOP: 5px">2.&nbsp;&nbsp;Select a QTY then PRODUCTS</DIV>
<DIV style="PADDING-TOP: 5px">3.&nbsp;&nbsp;Check Order Items �you can Cancel, change or delete any item</DIV>
<DIV style="PADDING-TOP: 5px">4.&nbsp;&nbsp;Confirm Shipping address</DIV>
<DIV class=height-20></DIV>
<DIV style="TEXT-DECORATION: underline; FONT-WEIGHT: 700; PADDING-TOP: 20px">ORDER HISTORY </DIV>
<DIV style="LINE-HEIGHT: 20px">You can view all your order details by clicking on ORDER HISTORY <BR>By clicking on the + symbol next to the order number you can view exactly what was ordered. <BR>For any problems with your order or your delivery, please call <B>1300 874 559.</B> </DIV></DIV>
<DIV class=height-20></DIV>
<DIV class=height-20></DIV>',@SETitle=NULL,@SEDescription=NULL,@SEKeywords=NULL,@SENoScript=NULL,@ExtensionData=NULL,@UserCreated=N'admin',@DateCreated='2017-05-05 23:16:32.747',@UserModified=N'admin',@DateModified='2017-05-05 23:16:32.747',@MLID=NULL,@Modified=0

exec createecommercetopic @TopicID=N'TOPI-000088',@WebSiteCode=N'WEB-000001',@TopicGUID='3DF7FC03-14F6-41C4-962E-063F1625EE8A',@Password=NULL,@PasswordIV=NULL,@PasswordSalt=NULL,
@RequiresSubscription=0,@RequiresDisclaimer=0,@XmlPackage=NULL,@ExtensionData=NULL,@ShowInSiteMap=1,@SKinID=1,@ContentsBGColor=NULL,@PageBGColor=NULL,@GraphicsColor=NULL,@HTMLOk=1,
@CreatedOn='2017-05-05 23:12:38.143',@UserCreated=N'admin',@DateCreated='2017-05-05 23:16:32.747',@UserModified=N'admin',@DateModified='2017-05-05 23:16:32.747',@MLID=NULL,@Modified=NULL

exec createecommercetopiclanguage @TopicID=N'TOPI-000088',@WebSiteCode=N'WEB-000001',@LanguageCode=N'English - Australia',@Name=N'Contact Us Form Thank You Page',@Title=N'ContactUsFormThankYouPage',@TopicContent=N'&nbsp;
<div style="text-align:center;font-size:18pt;padding-top:30px;">
  <div style="font-size:30pt;font-weight:700;padding-bottom:30px;"> Thank You! </div>
  <div style="padding-bottom:10px;"> We appreciate your enquiry, please allow 24 to 48 hours for a response. </div>
  <div style="padding-bottom:30px;"> For urgent enquiries please ring the office on 1800 874 559 </div>
  <div> Click <a href="contact-us.aspx" style="font-size:18pt;text-decoration:underline;color:#0000EE;">HERE</a> to return to contact us form </div>
</div>',@SETitle=NULL,@SEDescription=NULL,@SEKeywords=NULL,@SENoScript=NULL,@ExtensionData=NULL,@UserCreated=N'admin',@DateCreated='2017-05-05 23:16:32.747',@UserModified=N'admin',@DateModified='2017-05-05 23:16:32.747',@MLID=NULL,@Modified=0

exec createecommercetopiclanguage @TopicID=N'TOPI-000088',@WebSiteCode=N'WEB-000001',@LanguageCode=N'English - United States',@Name=N'Contact Us Form Thank You Page',@Title=N'ContactUsFormThankYouPage',@TopicContent=N'&nbsp;
<div style="text-align:center;font-size:18pt;padding-top:30px;">
  <div style="font-size:30pt;font-weight:700;padding-bottom:30px;"> Thank You! </div>
  <div style="padding-bottom:10px;"> We appreciate your enquiry, please allow 24 to 48 hours for a response. </div>
  <div style="padding-bottom:30px;"> For urgent enquiries please ring the office on 1800 874 559 </div>
  <div> Click <a href="contact-us.aspx" style="font-size:18pt;text-decoration:underline;color:#0000EE;">HERE</a> to return to contact us form </div>
</div>',@SETitle=NULL,@SEDescription=NULL,@SEKeywords=NULL,@SENoScript=NULL,@ExtensionData=NULL,@UserCreated=N'admin',@DateCreated='2017-05-05 23:16:32.747',@UserModified=N'admin',@DateModified='2017-05-05 23:16:32.747',@MLID=NULL,@Modified=0
/************************************************************************************************************************/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF OBJECT_ID ('CreateOnSupplierPurchaseOrder', 'TR') IS NOT NULL  
DROP TRIGGER CreateOnSupplierPurchaseOrder; 
GO

CREATE TRIGGER [dbo].[CreateOnSupplierPurchaseOrder] ON [dbo].[SupplierPurchaseOrder] FOR INSERT
AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE @PurchaseOrderCode NVARCHAR(30)
	DECLARE @url NVARCHAR(400)
	declare @object int
	declare @return int
	declare @valid int
	set @valid = 0

	SELECT @PurchaseOrderCode = PurchaseOrderCode FROM Inserted  
	SET @url = 'open("GET","http://localhost/sge/default.aspx?POCode=' + @PurchaseOrderCode + '", false)'
	
	--create the XMLHTTP object
	exec @return = sp_oacreate 'Microsoft.XMLHTTP', @object output
	if @return = 0
	begin

	--Open the connection
	exec @return = sp_oamethod @object, @url

	if @return = 0
	begin
	--Send the request
	exec @return = sp_oamethod @object, 'send()'
	--PRINT @return
	end

	if @return = 0
	begin
	declare @output int
	exec @return = sp_oamethod @object, 'status', @output output

	if @output = 200
	begin
	set @valid = 1
	end
	end
	end

	--destroy the object
	exec sp_oadestroy @object

	if @valid = 1
	print 'valid'
	else
	print 'invalid' 
    
END
GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[EcommerceGetProducts]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EcommerceGetProducts]
GO

CREATE PROCEDURE [dbo].[EcommerceGetProducts]                                                                                      
  @CategoryCode      NVARCHAR(1000),                                                                                        
  @DepartmentCode    NVARCHAR(1000),                                                                                       
  @ManufacturerCode  NVARCHAR(1000),                                                        
  @AttributeCode  NVARCHAR(MAX) = NULL,                                                                                
  @localeID   INT = NULL,                                                                                        
  @sortEntity  INT = 0, -- 1 = category, 2 = department, 3 = manufacturer, 4 = distributor                                                                                        
  @pagenum   INT = 1,                                                                                        
  @pagesize   INT = NULL,                                                                                        
  @StatsFirst  TINYINT = 1,                                                                                        
  @searchstr   NVARCHAR(4000) = NULL,                                                                                        
  @extSearch   TINYINT = 0,                                                                                        
  @publishedonly  TINYINT = 0,                                                                                        
  @sortEntityName VARCHAR(20) = '', -- usely only when the entity id is provided, allowed values: category, section, manufacturer, distributor                                                                                        
  @localeName  VARCHAR(20) = '',                                                                                        
  @WebSiteCode  NVARCHAR(30) ,                                                                              
  @CurrentDate Datetime,                                                                      
  @inventoryItemType INT=0,                                                                
  @ProductFilterID NVARCHAR(50) = null,                                                    
  @AttributeFilter NVARCHAR(MAX) =null,                                                    
  @ContactCode NVARCHAR(30),                                                  
  @SortingOption INT = 1,                                
  @CBMode BIT = 0,                  
  @MinPrice NUMERIC(18,6),                  
  @MaxPrice NUMERIC(18,6),
  @CurrencyCode NVARCHAR(30) = NULL
AS                                                                                                        
BEGIN                       
                                                                                        
 SET NOCOUNT ON                   
                                                                                                                                    
 DECLARE @EntityType NVARCHAR(1000)                                                    
 DECLARE @EntityCode  NVARCHAR(1000)                      
 DECLARE @AdditionalFilter NVARCHAR(200)               
 SET @AdditionalFilter=''                                                                                  
                                                                                   
 DECLARE @rcount INT                                                                                           
 CREATE TABLE #productfilter (RowNum INT NOT NULL IDENTITY PRIMARY KEY, ProductID INT NOT NULL,                                                                                 
   ItemCode NVARCHAR(1000) NOT NULL, DisplayOrder INT NOT NULL, ProductName NVARCHAR(1000) NULL)                                                                                
 DECLARE @FilterProductsByAffiliate TINYINT                   
                                                                                                                                                  
 CREATE TABLE #displayorder (ProductID INT NOT NULL PRIMARY KEY, ItemCode NVARCHAR(100) NOT NULL, displayorder INT NOT NULL)                                                                                        
                                                                              
 CREATE TABLE #inventoryfilter (ProductID INT NOT NULL, ItemCode NVARCHAR(30), InvQty INT NOT NULL)                                                                                        
 CREATE CLUSTERED INDEX tmp_inventoryfilter ON #inventoryfilter (productid, ItemCode)                                                                                        
                                                                                 
 CREATE TABLE #inventoryitemtype (Counter INT NOT NULL, ItemType NVARCHAR(50) NOT NULL)                                                                   
                                
 DECLARE @departmentcount INT, @localecount INT, @categorycount INT, @attributecount INT, @CustomerLevel0SeesAllUnmappedProducts BIT, @CustomerLevelFilteringIsAscending BIT, @manufacturercount INT 
    
 SET @FilterProductsByAffiliate = 0                                                                        
                                                                                        
 SET @CategoryCode =    NULLIF(@CategoryCode, '')                                                        
 SET @DepartmentCode =   NULLIF(@DepartmentCode, '')                                                                 
 SET @ManufacturerCode = NULLIF(@ManufacturerCode, '')                                                                                     
 SET @AttributeCode =  NULLIF(@AttributeCode,'')                
 SET @AttributeFilter = NULLIF(@AttributeFilter, '')                                                   
                                                     
 SET @EntityType=NULL                                                    
                                                     
 IF @CategoryCode <>''                                                     
 SET @EntityType ='Category'                                                    
                                                     
 IF @DepartmentCode <>''                                               
 SET @EntityType ='Department'                                                    
IF @EntityType ='Department'                                                    
SET @EntityCode =@DepartmentCode                                                    
IF @EntityType ='Category'                                                    
SET @EntityCode =@CategoryCode                          
                                                                       
 SET @CustomerLevel0SeesAllUnmappedProducts = 0                                                                                        
 SELECT @CustomerLevel0SeesAllUnmappedProducts = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END                                                                                        
 FROM EcommerceAppConfigModel WITH (NOLOCK)                                                                         
 WHERE name LIKE 'CustomerLevel0SeesAllUnmappedProducts'                                                                                        
                                                                                        
 SET @CustomerLevelFilteringIsAscending  = 0                                                                                        
 SELECT @CustomerLevelFilteringIsAscending  = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END      
 FROM EcommerceAppConfigModel WITH (NOLOCK)               
 WHERE name LIKE 'CustomerLevelFilteringIsAscending'                                                               
                                                                                        
 IF @localeID is NULL AND ltrim(rtrim(@localeName)) <> ''                                                                               
 SELECT @localeID = LocaleSettingID                                                                         
 FROM EcommerceLocaleSetting WITH (NOLOCK)                                                                         
 WHERE Name = ltrim(rtrim(@localeName))                                                                                        
                                                                        
 SELECT @categorycount = si.rows FROM sysobjects so WITH (NOLOCK) JOIN sysindexes si WITH (NOLOCK) ON so.id = si.id WHERE so.id = object_id('SystemCategory') AND si.indid < 2 AND type = 'u'                      
 SELECT @departmentcount   = si.rows FROM sysobjects so WITH (NOLOCK) JOIN sysindexes si WITH (NOLOCK) ON so.id = si.id WHERE so.id = object_id('InventorySellingDepartment') AND si.indid < 2 AND type = 'u'                      
 SELECT @localecount       = si.rows FROM sysobjects so WITH (NOLOCK) JOIN sysindexes si WITH (NOLOCK) ON so.id = si.id WHERE so.id = object_id('ProductLocaleSetting') AND si.indid < 2 AND type = 'u'                      
 SELECT @manufacturercount = si.rows FROM sysobjects so WITH (NOLOCK) JOIN sysindexes si WITH (NOLOCK) ON so.id = si.id WHERE so.id = object_id('SystemManufacturer') AND si.indid < 2 AND type = 'u'                      
                                
 -- get requested itemtype                                                                      
 SET @inventoryItemType = NULLIF(@inventoryItemType, '')                                                                         
 IF @inventoryItemType is NULL or @inventoryItemType=0                                                                      
  BEGIN                                                                     
 INSERT #inventoryitemtype                                         
    SELECT Counter, ItemType FROM InventoryItemType                                                              
    WHERE ItemType IN ('Stock', 'Kit', 'Matrix Group', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly', 'Gift Card', 'Gift Certificate')                                                              
  END                                                          
 ELSE                                                                      
  BEGIN                                                                      
 INSERT #inventoryitemtype                                                                      
    SELECT Counter, ItemType FROM InventoryItemType                                                                      
    WHERE Counter = @inventoryItemType                                                                      
  END                                       
                                                                               
 -- get page size                                                                                        
 IF @pagesize is NULL or @pagesize = 0                                               
 BEGIN                                                                                          
  IF @CategoryCode IS NOT NULL                                                                                        
   SELECT @pagesize = PageSize                                                 
   FROM dbo.SystemCategoryWebOption WITH (NOLOCK)                                                         
   WHERE CategoryCode = @CategoryCode AND                                          
     WebSiteCode = @WebSiteCode                                          
  ELSE IF @DepartmentCode IS NOT NULL                                                                                        
   SELECT @pagesize = PageSize                                                                         
   FROM dbo.InventorySellingDepartmentWebOption WITH (NOLOCK)                                                                         
   WHERE DepartmentCode = @DepartmentCode AND                                                                         
     WebSiteCode = @WebSiteCode                                                                        
  ELSE IF @ManufacturerCode IS NOT NULL                                                                        
   SELECT @pagesize = PageSize                                                                        
   FROM dbo.SystemManufacturerWebOption WITH (NOLOCK)                                                                         
   WHERE ManufacturerCode = @ManufacturerCode AND                                                                         
     WebSiteCode = @WebSiteCode                                                      
  ELSE IF @AttributeCode IS NOT NULL                                                                        
   SELECT @pagesize = PageSize                                                                        
   FROM dbo.SystemItemAttributeSourceFilterValueWebOption WITH (NOLOCK)                                                                         
   WHERE SourceFilterName = @AttributeCode AND                                                                         
     WebSiteCode = @WebSiteCode                                                                             
  ELSE                                                
   SELECT @pagesize = convert(INT, ConfigValue)                                                                         
   FROM dbo.[EcommerceAppConfig] wsa WITH (NOLOCK)                                                                         
   WHERE wsa.[Name] = 'Default_CategoryPageSize' AND                                                                        
     wsa.WebsiteCode = @WebsiteCode                                                                                                
 END                                                    
                      
 IF @pagesize IS NULL or @pagesize = 0                                                                                        
 SET @pagesize = 999999999                       

IF @CurrencyCode IS NULL
BEGIN
	SELECT @CurrencyCode = CurrencyCode
	FROM Customer CU WITH (NOLOCK)
	INNER JOIN CRMContact CO WITH (NOLOCK) ON CO.EntityCode = CU.CustomerCode 
		AND CO.ContactCode = @ContactCode
END
                                                                      
 -- get sort order                                                
 IF @sortEntity = 1 or @sortEntityName = 'category'                                                                               
 BEGIN                                             
  INSERT #displayorder                                                                               
  SELECT II.Counter AS ProductID, II.ItemCode, IC.SortOrder AS DisplayOrder                                                                               
  FROM SystemCategoryWebOption SC WITH (NOLOCK)                                                        
  INNER JOIN InventoryCategory IC WITH (NOLOCK) ON IC.CategoryCode = SC.CategoryCode AND SC.WebSiteCode = @WebSiteCode                                                                                
 INNER JOIN InventoryItem II WITH (NOLOCK) ON IC.ItemCode = II.ItemCode                                                                             
  WHERE SC.CategoryCode = @CategoryCode                                                                     
  ORDER BY DisplayOrder  -- order by added by m.d (for item sorting) 11.04.2009                                                             
 END                                                                                        
 ELSE IF @sortEntity = 2 or @sortEntityName = 'department'                                                           
 BEGIN                                                                        
  INSERT #displayorder                                                                               
  SELECT II.Counter AS ProductID, II.ItemCode, IID.SortOrder AS DisplayOrder                                                                
  FROM InventorySellingDepartmentWebOption ISD WITH (NOLOCK)                                                                                
  INNER JOIN InventoryItemDepartment IID WITH (NOLOCK) ON ISD.DepartmentCode = IID.DepartmentCode AND ISD.WebSiteCode = @WebSiteCode                                                                              
  INNER JOIN InventoryItem II WITH (NOLOCK) ON IID.ItemCode = II.ItemCode                                                                             
  WHERE ISD.DepartmentCode = @DepartmentCode                                            
 END                                                                                        
 ELSE IF @sortEntity = 3 or @sortEntityName = 'manufacturer'                                                                               
 BEGIN                                                                   
  INSERT #DisplayOrder                                                                         
  SELECT ii.Counter AS ProductID,                                                                              
    ii.ItemCode,                      
    0 AS DisplayOrder                        
  FROM InventoryItem ii WITH (NOLOCK)                                                                              
  WHERE ((@ManufacturerCode IS NOT NULL AND ii.ManufacturerCode = @ManufacturerCode ) OR                                            
    ( @ManufacturerCode IS NULL AND (ii.ManufacturerCode IS NULL OR ii.ManufacturerCode = ii.ManufacturerCode)))                                                                               
 END                                                        
 ELSE IF @sortEntityName = 'attribute'                                                  
 BEGIN                                                        
                                                     
 INSERT #DisplayOrder                                                                              
 SELECT ii.Counter AS ProductID,                                                                              
     ii.ItemCode,                                                                              
     0 AS DisplayOrder                     
     FROM InventoryItem ii WITH (NOLOCK)                                                        
     INNER JOIN InventoryItemAttributeView IIAV WITH (NOLOCK) ON ii.ItemCode = IIAV.ItemCode                                                    
     INNER JOIN SystemLanguage SL WITH (NOLOCK) ON SL.ShortString = @localeName                                                     
     WHERE                                                     
     IIAV.AttributeValue = ISNULL(@AttributeCode,IIAV.AttributeValue) AND IIAV.LanguageCode = SL.LanguageCode                                                     
     AND SL.LanguageCode =IIAV.LanguageCode                                                    
  AND IIAV.EntityType =ISNULL(@EntityType,IIAV.EntityType)                                                    
     AND IIAV.EntityCode = ISNULL(@EntityCode,IIAV.EntityCode)                                                    
     AND IIAV.WebSiteCode = @WebSiteCode AND IIAV.Published=1                                                    
     AND CHARINDEX(                                                    
 ','+CAST(ISNULL(IIAV.FilterGroupCounter,'') AS NVARCHAR(100))+'-'+CAST(ISNULL(IIAV.AttributeCounter,'') AS NVARCHAR(100))+'-'+            
 CAST(ISNULL(IIAV.AttributeGroupGuid,'') AS NVARCHAR(100))+CAST(ISNULL(IIAV.AttributeGuid,'') AS NVARCHAR(100))+','                         
              
 ,','+ISNULL(@AttributeFilter,CAST(ISNULL(IIAV.FilterGroupCounter,'') AS NVARCHAR(100))+'-'+CAST(ISNULL(IIAV.AttributeCounter,'') AS NVARCHAR(100))+'-'+            
 CAST(ISNULL(IIAV.AttributeGroupGuid,'') AS NVARCHAR(100))+CAST(ISNULL(IIAV.AttributeGuid,'') AS NVARCHAR(100)))+',')>0                                                    
    Group BY ii.Counter,ii.ItemCode                                                  
                                
  --filter more                                                  
  Declare @myString nvarchar(max)                   
  Declare @deliminator varchar(10)                                                  
  Declare @iSpaces int                                                  
  Declare @part varchar(500)                                                  
  Declare @LanguageCode nvarchar(60)                                                    
                                                  
  set @myString = @AttributeFilter                                                  
  set @deliminator=','                                                  
  select @LanguageCode=LanguageCode FROM SystemLanguage where ShortString = @localeName                                                    
                                                    
        --initialize spaces                                                  
        Select @iSpaces = charindex(@deliminator,@myString,0)                                                  
    While @iSpaces > 0                
      Begin                                        
        Select @part = substring(@myString,0,charindex(@deliminator,@myString,0))                                                                                       
  --filter                                                  
  if len(@part)>0                          
     begin                                 
   Delete from #DisplayOrder WHERE ItemCode NOT IN                                                   
   (                                                   
     select distinct(ItemCode)                                                    
     from InventoryItemAttributeView                                                    
     where CHARINDEX(                                                    
      ','+CAST(ISNULL(FilterGroupCounter,'') AS NVARCHAR(100))+'-'+CAST(ISNULL(AttributeCounter,'') AS NVARCHAR(100))+'-'+            
      CAST(ISNULL(AttributeGroupGuid,'') AS NVARCHAR(100))+CAST(ISNULL(AttributeGuid,'') AS NVARCHAR(100))+','                      
      ,','+ISNULL(@part,CAST(ISNULL(FilterGroupCounter,'') AS NVARCHAR(100))+'-'+CAST(ISNULL(AttributeCounter,'') AS NVARCHAR(100))+'-'+            
      CAST(ISNULL(AttributeGroupGuid,'') AS NVARCHAR(100))+CAST(ISNULL(AttributeGuid,'') AS NVARCHAR(100)))+',')>0                       
            
     and EntityType =@EntityType                                                     
    and EntityCode=@EntityCode                                                     
     and LanguageCode=@LanguageCode                                               
     and WebSiteCode=@WebSiteCode                                      
     and Published =1                                                  
     and ItemCode IN (select ItemCode from #DisplayOrder)                                                  
   )                                                                        
     end                       
                                           
        Select @myString = substring(@mystring,charindex(@deliminator,@myString,0)+ len(@deliminator),len(@myString) - charindex(' ',@myString,0))                                                  
        Select @iSpaces = charindex(@deliminator,@myString,0)                              
     End                                                  
                                                  
    If len(@myString) > 0                                                  
    begin                              
    Delete from #DisplayOrder WHERE ItemCode NOT IN                                                   
   (                                                   
     select distinct(ItemCode)                      
     from InventoryItemAttributeView                      
     where CHARINDEX(                      
      ','+CAST(ISNULL(FilterGroupCounter,'') AS NVARCHAR(100))+'-'+CAST(ISNULL(AttributeCounter,'') AS NVARCHAR(100))+'-'+            
      CAST(ISNULL(AttributeGroupGuid,'') AS NVARCHAR(100))+CAST(ISNULL(AttributeGuid,'') AS NVARCHAR(100))+','                      
      ,','+ISNULL(@myString,CAST(ISNULL(FilterGroupCounter,'') AS NVARCHAR(100))+'-'+CAST(ISNULL(AttributeCounter,'') AS NVARCHAR(100))+'-'+            
      CAST(ISNULL(AttributeGroupGuid,'') AS NVARCHAR(100))+CAST(ISNULL(AttributeGuid,'') AS NVARCHAR(100)))+',')>0                   
                
     and EntityType =@EntityType                      
     and EntityCode=@EntityCode                      
     and LanguageCode=@LanguageCode                      
     and WebSiteCode=@WebSiteCode                      
     and Published =1                      
   )                      
  end                      
 END                      
 ELSE                                                                               
 BEGIN                                                                                  
  INSERT #displayorder                                                                               
 SELECT Counter AS ProductID, ItemCode, 0 AS DisplayOrder                                                                               
  FROM InventoryItem WITH (NOLOCK)                                                                              
  ORDER BY ItemCode                                                        
 END          
                                                                                                
 SET @searchstr = '%' + @searchstr + '%'                                     
                                                                                
 INSERT INTO #productfilter (ItemCode, ProductID, DisplayOrder, ProductName)                                                                                
 SELECT DISTINCT II.ItemCode, II.Counter AS ProductID, DO.DisplayOrder, II.ItemCode AS ProductName                                                                              
 FROM InventoryItem II WITH (NOLOCK)                                                               
 JOIN InventoryItemWebOption IIW WITH (NOLOCK) ON II.ItemCode = IIW.ItemCode AND IIW.WebSiteCode = @WebSiteCode                                                         
 JOIN #displayorder DO WITH (NOLOCK) ON II.ItemCode = DO.ItemCode                                                                              
 LEFT JOIN InventoryCategory IC WITH (NOLOCK) ON II.ItemCode = IC.ItemCode                                                                              
 LEFT JOIN InventoryItemDepartment IID WITH (NOLOCK) ON II.ItemCode = IID.ItemCode                                                                                
 WHERE II.Status IN ('A', 'P') AND                                                                         
   II.ItemType IN (SELECT ItemType FROM #inventoryitemtype) AND                                           
   IIW.CheckOutOption = 0 AND                                                                         
   ((iiw.StartDate IS NULL AND iiw.EndDate IS NULL) OR                                                
   (iiw.StartDate IS NOT NULL AND iiw.StartDate <= @CurrentDate AND iiw.EndDate IS NULL) OR                                                                 
   (iiw.StartDate IS NULL AND iiw.EndDate IS NOT NULL AND iiw.EndDate >= @CurrentDate) OR                                                                             
   (@CurrentDate BETWEEN iiw.StartDate AND iiw.EndDate)) AND                                                                               
   (                                                                        
   IC.CategoryCode IN (                                                                        
     SELECT CategoryCode                                                                         
     FROM SystemCategoryWebOption WITH (NOLOCK)                                                                          
   WHERE CategoryCode = @CategoryCode AND                                                                         
       WebSiteCode = @WebSiteCode                                                         
     ) OR                                                                     
     @CategoryCode IS NULL OR                                                                         
     @CategoryCount = 0                                                               
   ) AND                                                                         
   (                                                                        
    IID.DepartmentCode IN (                                                                        
     SELECT DepartmentCode                                                                         
     FROM InventorySellingDepartmentWebOption WITH (NOLOCK)                                                                          
     WHERE DepartmentCode = @DepartmentCode AND                                          
       WebSiteCode = @WebSiteCode                              
     ) OR                                                                         
     @DepartmentCode IS NULL OR                                                                         
     @DepartmentCount = 0                                                                        
   ) AND                                                                
   (                             
    II.ItemCode IN (                                                                        
     SELECT ItemCode                                                                         
     FROM InventoryItemWebOption WITH (NOLOCK)                                                                         
     WHERE WebSiteCode = @WebSiteCode AND Published = 1                                                                        
    )                 
   )                                                                              
   AND (@searchstr IS NULL OR PATINDEX(@searchstr, ISNULL(II.ItemCode, NULL)) > 0)                                                                                
   AND ((@ManufacturerCode IS NOT NULL AND ii.ManufacturerCode = @ManufacturerCode ) OR                                                                               
   (@ManufacturerCode IS NULL AND (ii.ManufacturerCode IS NULL OR ii.ManufacturerCode = ii.ManufacturerCode) ) )                                
   AND (@CBMode=0  OR (@CBMode=1 AND II.IsCBN = 1))                                                                               
 ORDER BY DO.DisplayOrder, II.ItemCode                        
                                                          /*For product filtering*/                                 
IF @ProductFilterID IS NULL OR ( @ProductFilterID = '00000000-0000-0000-0000-000000000000' AND EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE NAME = 'AllowProductFiltering' AND WebSiteCode = @WebsiteCode AND ConfigValue = 'TRUE') )                       


                                                                                                       
 BEGIN                                                                
  SET @ProductFilterID = ''                                                                
    END                                                               
 IF @ProductFilterID <> ''                                                                
    BEGIN                                                                
  DELETE FROM #productfilter WHERE ItemCode NOT IN (SELECT DISTINCT(ItemCode) FROM InventoryProductFilterTemplateItem WHERE TemplateID = @ProductFilterID)                                                                
    END      
                                    
/* remove giftcard/giftcertificate items that are not sellable to customer's currency */
DELETE FROM #productfilter
WHERE ItemCode IN ( SELECT II.ItemCode
					FROM InventoryItem II WITH (NOLOCK)
					INNER JOIN InventoryItemPricingDetail IP WITH (NOLOCK) ON IP.ItemCode = II.ItemCode
						AND IP.CurrencyCode = @CurrencyCode 
						AND IP.[Enabled] = 0 
						AND II.ItemType IN ('Gift Card', 'Gift Certificate'))			

/* Product Sorting */                                                        
DECLARE @AdditionalJoin NVARCHAR(1000)                                                   
DECLARE @AdditionalField NVARCHAR(1000)                                                  
SET @AdditionalJoin = ''                                                  
SET @AdditionalField = ''                                                  
                                                  
 IF @SortingOption = 2 -- Best Seller                                                  
 BEGIN                                                  
 CREATE TABLE #inventoryitemsalesorder (ItemCode NVARCHAR(30) NOT NULL, Quantity  NUMERIC(18,6))                                                           
                                                   
 INSERT INTO #inventoryitemsalesorder                                                  
 SELECT IM.ItemCode, SUM(CSOD.UnitMeasureQty * CSOD.QuantityOrdered) AS Quantity FROM CustomerSalesOrderDetail CSOD WITH (NOLOCK)                    
 INNER JOIN InventoryMatrixItem IM WITH (NOLOCK) ON CSOD.ItemCode = IM.MatrixItemCode                    
 GROUP BY IM.ItemCode                    
 UNION ALL                    
 SELECT CSOD.ItemCode, SUM(CSOD.UnitMeasureQty * CSOD.QuantityOrdered) AS Quantity FROM CustomerSalesOrderDetail CSOD WITH (NOLOCK)                    
 INNER JOIN InventoryItem II WITH (NOLOCK) ON CSOD.ItemCode = II.ItemCode                    
 WHERE II.ItemType NOT IN ('Matrix Item','Matrix Group')                     
 GROUP BY CSOD.ItemCode                                                 
                                                   
 SET @AdditionalJoin = ' LEFT JOIN #inventoryitemsalesorder IISO WITH (NOLOCK) ON IISO.ItemCode = II.ItemCode '                                                  
 SET @AdditionalField = ' ISNULL(IISO.Quantity,0) AS SoldItems, '                                                  
 END                               
                                                   
 IF @SortingOption = 3 -- Most Popular                                                  
 BEGIN                                                  
 CREATE TABLE #inventoryitemrating (ItemCode NVARCHAR(30) NOT NULL, TotalRating INT)                                                           
                                         
 INSERT INTO #inventoryitemrating                                                  
 SELECT ItemCode, SUM(Rating) AS TotalRating                                                   
 FROM EcommerceRating WITH (NOLOCK)                                                
 GROUP BY ItemCode                                                  
                                              
 SET @AdditionalJoin = ' LEFT JOIN #inventoryitemrating IIR WITH (NOLOCK) ON IIR.ItemCode = II.ItemCode '                                                  
 SET @AdditionalField = ' ISNULL(IIR.TotalRating,0) AS TotalRating, '                                                  
 END                                                    
                                                  
 IF @SortingOption = 4 -- Item Availability                                                   
 BEGIN                                                
 CREATE TABLE #inventoryitemfreestock (ItemCode NVARCHAR(30) NOT NULL, FreeStock  NUMERIC(18,6))                                                           
                                                   
 INSERT INTO #inventoryitemfreestock                                                  
 SELECT ItemCode, (UnitsInStock-UnitsCommitted) AS FreeStock FROM InventoryStockTotal WITH (NOLOCK)                                                  
 WHERE WareHouseCode = (SELECT DefaultWareHouse FROM InventoryPreference WITH (NOLOCK))                                                  
                                                   
 SET @AdditionalJoin = ' LEFT JOIN #inventoryitemfreestock IIFS WITH (NOLOCK) ON IIFS.ItemCode = II.ItemCode '                                                  
 SET @AdditionalField = ' IIFS.FreeStock, '                                                  
 END    
   
 -- Item Price  
 IF @SortingOption = 5 OR @SortingOption = 6  OR @MinPrice IS NOT NULL OR @MaxPrice IS NOT NULL                  
 BEGIN                    
   
 IF @MinPrice IS NULL   
 SET @MinPrice = 0  
   
 IF @MaxPrice IS NULL   
 SET @MaxPrice = 999999999             

DECLARE @DefaultPrice NVARCHAR(20) 
SELECT @DefaultPrice = DefaultPrice 
FROM Customer CU                  
INNER JOIN CRMContact CC ON CC.EntityCode = CU.CustomerCode 
	AND CC.ContactCode = @ContactCode                        
                                    
 CREATE TABLE #inventoryitempricingdetail (ItemCode NVARCHAR(30) NOT NULL, WholeSalePrice NUMERIC(18,6), RetailPrice NUMERIC(18,6))                                     
                              
 INSERT INTO #inventoryitempricingdetail                            
 SELECT ItemCode, WholeSalePrice, RetailPrice FROM InventoryItemPricingDetail WITH (NOLOCK)                            
 WHERE CurrencyCode = @CurrencyCode                            
                               
 --Insert pricing info for kit items in #inventoryitempricingdetail      
 IF (SELECT COUNT(PF.ItemCode) FROM InventoryItem II      
  INNER JOIN #productfilter PF ON II.ItemCode = PF.ItemCode                          
  WHERE II.ItemType = 'Kit') >  0      
         
 BEGIN    
  CREATE TABLE #KitItems (ItemCode NVARCHAR(30) NOT NULL, UseCustomerPricing BIT)      
  INSERT INTO #KitItems        
  SELECT PF.ItemCode, ISNULL(IK.IsGetItemPrice, 0) FROM InventoryItem II WITH (NOLOCK)                           
  INNER JOIN InventoryItem PF WITH (NOLOCK) ON II.ItemCode = PF.ItemCode      
  INNER JOIN InventoryKit IK WITH (NOLOCK) ON PF.ItemCode = IK.ItemKitCode                          
  WHERE II.ItemType = 'Kit'    
        
  INSERT INTO #inventoryitempricingdetail                                                  
  SELECT IKD.ItemKitCode as ItemCode, SUM(IIPD.WholeSalePrice) AS WholeSalePrice, SUM(IIPD.RetailPrice) AS RetailPrice FROM InventoryKitDetail IKD WITH (NOLOCK)         
  INNER JOIN InventoryItemPricingDetail IIPD WITH (NOLOCK)  ON IKD.ItemCode = IIPD.ItemCode AND IIPD.CurrencyCode = @CurrencyCode     
  INNER JOIN #KitItems KI ON KI.ItemCode = IKD.ItemKitCode       
  WHERE IIPD.CurrencyCode = @CurrencyCode AND IKD.IsDefault = 1 AND KI.UseCustomerPricing = 1      
  GROUP BY IKD.ItemKitCode       
  UNION ALL     
  SELECT IKD.ItemKitCode, SUM(TotalRate) AS WholeSalePrice, SUM(TotalRate) AS RetailPrice FROM InventoryKitPricingDetail IKPD WITH (NOLOCK)      
  INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON IKPD.ItemKitCode = IKD.ItemKitCode AND IKPD.ItemCode = IKD.ItemCode      
  INNER JOIN #KitItems KI ON KI.ItemCode = IKD.ItemKitCode      
  WHERE IKPD.CurrencyCode = @CurrencyCode AND IKD.IsDefault = 1 AND KI.UseCustomerPricing = 0      
  GROUP BY IKD.ItemKitCode                                                                                            
 END         
                                        
 IF @DefaultPrice = 'Retail'                  
 BEGIN                  
  SET @AdditionalFilter = ' AND ISNULL(RetailPrice,0) >= ' + QUOTENAME(@MinPrice,'''') + ' AND ISNULL(RetailPrice,0) <= ' + QUOTENAME(@MaxPrice,'''')                  
 END                  
 ELSE                   
 BEGIN                   
  SET @AdditionalFilter = ' AND ISNULL(WholesalePrice,0) >= ' + QUOTENAME(@MinPrice,'''') + ' AND ISNULL(WholesalePrice,0) <= ' + QUOTENAME(@MaxPrice,'''')                  
 END   
   
    SET @AdditionalField = @AdditionalField + ' IIPD.WholeSalePrice AS WholeSalePrice, IIPD.RetailPrice AS RetailPrice, '   
 SET @AdditionalJoin = @AdditionalJoin + ' LEFT JOIN #inventoryitempricingdetail IIPD WITH (NOLOCK) ON IIPD.ItemCode = II.ItemCode '                            
 END                           
                                                                                                                                                           
 IF @SortingOption = 9 -- New Arrival                                                  
 BEGIN                                                  
 CREATE TABLE #inventoryitemnewarrival (ItemCode NVARCHAR(30) NOT NULL, DueDate DateTime)                                                           
                                                    
 INSERT INTO #inventoryitemnewarrival                                                  
 SELECT ItemCode, DueDate                                                   
 FROM(                                                  
   -- Rank item by item and                                                   
   SELECT ItemCode, DueDate, ROW_NUMBER() OVER (PARTITION BY ItemCode ORDER BY DueDate DESC) RowNum                                             
   FROM SupplierPurchaseOrderDetail WITH (NOLOCK)                                                  
   WHERE WarehouseCode  = (SELECT DefaultWareHouse FROM InventoryPreference WITH (NOLOCK)) AND DueDate < GETDATE()                                                  
 ) A                                                  
 WHERE RowNum = 1 --Select top 1 item-code                                                  
                   
 SET @AdditionalJoin = @AdditionalJoin + ' LEFT JOIN #inventoryitemnewarrival IINA WITH (NOLOCK) ON IINA.ItemCode = II.ItemCode '                                                  
 SET @AdditionalField = @AdditionalField + ' IINA.DueDate, '                                                  
 END                                                  
                                                   
 /*Sort Option*/                                                  
 DECLARE @SortExpression NVARCHAR(100)                                                  
 SELECT @SortExpression =                                                   
 CASE                                                  
  WHEN @SortingOption = 1 THEN ' P.DisplayOrder ASC, II.ItemCode ASC'                                                   
  WHEN @SortingOption = 2 THEN ' IISO.Quantity DESC'               
  WHEN @SortingOption = 3 THEN ' IIR.TotalRating DESC'    
  WHEN @SortingOption = 4 THEN ' IIFS.FreeStock DESC'                                                   
  WHEN @SortingOption = 5 AND @DefaultPrice = 'Retail' THEN ' IIPD.RetailPrice ASC'               
  WHEN @SortingOption = 5 AND @DefaultPrice = 'Wholesale' THEN ' IIPD.WholeSalePrice ASC'                                                  
  WHEN @SortingOption = 6 AND @DefaultPrice = 'Retail' THEN ' IIPD.RetailPrice DESC'                                                  
  WHEN @SortingOption = 6 AND @DefaultPrice = 'Wholesale' THEN ' IIPD.WholeSalePrice DESC'                                                  
  WHEN @SortingOption = 7 THEN ' IID.ItemDescription ASC, II.ItemName ASC'                                                  
  WHEN @SortingOption = 8 THEN ' IID.ItemDescription DESC, II.ItemName DESC'                                           
  WHEN @SortingOption = 9 THEN ' IINA.DueDate DESC'                                                  
  ELSE ' P.DisplayOrder ASC, II.ItemCode ASC'                                                  
 END                                                   
                                                       
 /**********************/                           
                                 
 /*CBN Mode*/                                  
 IF @CBMode = 1                                  
 BEGIN                                  
 SET @AdditionalFilter = @AdditionalFilter + ' AND II.IsCBN = 1 '                                  
 END                                  
                                             
/*Out of Stock Items*/                                                                 
CREATE TABLE #InventoryFreeStock (ItemCode  NVARCHAR(50), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))                                            
CREATE TABLE #AvailableItems (ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), UM NVARCHAR(30))                                            
-- temporary tables to store available kit components and matrix attributes              
CREATE TABLE #AvailableKitComponents (ItemKitCode NVARCHAR(30), ItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), ItemType NVARCHAR(30), POStatus NVARCHAR(30))                       
CREATE TABLE #AvailableMatrixAttributes (ItemCode NVARCHAR(30), MatrixItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))                      
                                                       
DECLARE @HideOutOfStockProducts BIT                                                                                       
DECLARE @ShowInventoryFromAllWarehouses BIT                                                                                                                
DECLARE @WarehouseCode NVARCHAR(30)                                                                                 
                                                                               
SET @HideOutOfStockProducts =  0                                                   
SET @ShowInventoryFromAllWarehouses = 0                                                                                                                               
                                                  
SELECT @HideOutOfStockProducts = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'HideOutOfStockProducts' AND WebSiteCode = @WebSiteCode                      
SELECT @ShowInventoryFromAllWarehouses = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'ShowInventoryFromAllWarehouses' AND WebSiteCode = @WebSiteCode                      
SET @WarehouseCode = (SELECT TOP 1 WareHouseCode FROM CustomerShipTo WHERE ShipToCode = (SELECT DefaultShippingCode FROM CRMContact WHERE ContactCode = @ContactCode))                      
-- Get freestock of non-kit/non-matrix group items (Stock, Matrix Item, Assembly)                      
IF @HideOutOfStockProducts = 1                    
BEGIN                    
 INSERT INTO #InventoryFreeStock (ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)                      
 SELECT EISTV.ItemCode, EISTV.WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM EcommerceInventoryStockTotalView EISTV                    
 INNER JOIN #productfilter PF ON PF.ItemCode = EISTV.ItemCode               
 LEFT JOIN SupplierPurchaseOrderDetail SPOD ON SPOD.ItemCode = PF.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode                      
 LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode                    
END                    
ELSE                    
BEGIN                    
 INSERT INTO #InventoryFreeStock (ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)                    
 SELECT EISTV.ItemCode, EISTV.WarehouseCode, FreeStock, UnitMeasureCode, NULL FROM EcommerceInventoryStockTotalView EISTV                    
 INNER JOIN #productfilter PF ON PF.ItemCode = EISTV.ItemCode                    
END               
                      
IF @HideOutOfStockProducts = 1                      
BEGIN                      
	INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                            
	SELECT II.ItemCode, 0, UnitMeasureCode FROM InventoryItem II                         
	INNER JOIN #productfilter PF ON PF.ItemCode = II.ItemCode                                       
	INNER JOIN InventoryUnitMeasure IUM ON II.ItemCode = IUM.ItemCode                                   
	WHERE ItemType IN ( 'Service', 'Electronic Download', 'Non-Stock', 'Gift Card', 'Gift Certificate')                                            
	AND IsBase = 1                    
	                   
	IF ( SELECT COUNT(pf.ItemCode) FROM InventoryItem II                           
		 INNER JOIN #productfilter PF ON II.ItemCode = PF.ItemCode                          
		 WHERE II.ItemType = 'Kit' ) > 0                    
	BEGIN                                                     
		-- get the freestock of kit component/s                    
		;WITH cte_AvailableKitComponents                     
		AS                    
		(                    
			SELECT IKD.ItemKitCode, IKD.ItemCode AS ItemCode, 
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN @WarehouseCode
				ELSE EISTV.WarehouseCode
			END AS WarehouseCode, 
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN 0
				ELSE EISTV.FreeStock
			END AS FreeStock, IKD.UnitMeasureCode, II.ItemType, OrderStatus,                      
			ROW_NUMBER() over(PARTITION BY  IKD.ItemKitCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum                    
			FROM InventoryItem II
			INNER JOIN InventoryKitDetail IKD ON II.ItemCode = IKD.ItemCode
			LEFT JOIN EcommerceInventoryStockTotalView EISTV ON IKD.ItemCode = EISTV.ItemCode                    
			LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IKD.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode                      
			LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode                                          
			WHERE ItemKitCode IN (SELECT pf.ItemCode                    
					FROM InventoryItem II                     
					INNER JOIN #productfilter PF                    
					ON II.ItemCode = PF.ItemCode WHERE II.ItemType = 'Kit')                    
		)            
		INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType, POStatus)                    
		SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType, OrderStatus FROM cte_AvailableKitComponents WHERE RowNum = 1                   
	END                      
	                  
	IF ( SELECT COUNT(pf.ItemCode) FROM InventoryItem II                           
		 INNER JOIN #productfilter pf ON II.ItemCode = pf.ItemCode                          
		 WHERE II.ItemType = 'Matrix Group' ) > 0                      
	BEGIN                           
		-- get the freestock of matrix item attribute/s                    
		;WITH cte_AvailableMatrixAttributes                    
		AS                    
		(                    
			SELECT IMI.ItemCode, IMI.MatrixItemCode, EISTV.WarehouseCode , EISTV.FreeStock, EISTV.UnitMeasureCode, OrderStatus,                      
			ROW_NUMBER() over(PARTITION BY  IMI.ItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum                    
			FROM InventoryMatrixItem IMI                       
			INNER JOIN EcommerceInventoryStockTotalView EISTV ON IMI.MatrixItemCode = EISTV.ItemCode                    
			LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IMI.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode                      
			LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode                      
			WHERE (EISTV.FreeStock > 0 OR OrderStatus IN ('Open', 'Partial'))                    
			AND IMI.ItemCode IN (SELECT pf.ItemCode                    
					 FROM InventoryItem II                    
					 INNER JOIN #productfilter pf                    
					 ON II.ItemCode = pf.ItemCode WHERE II.ItemType = 'Matrix Group')                    
		)                    
		INSERT INTO #AvailableMatrixAttributes (ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)                    
		SELECT ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM cte_AvailableMatrixAttributes WHERE RowNum = 1                    
	END                    
END                    
ELSE                      
BEGIN 
	IF ( SELECT COUNT(pf.ItemCode) FROM InventoryItem II                           
         INNER JOIN #productfilter PF ON II.ItemCode = PF.ItemCode  
         WHERE II.ItemType = 'Kit' ) > 0                    
	BEGIN                      
		-- get the freestock of kit component/s                    
		;WITH cte_AvailableKitComponents                     
		AS                    
	   (                    
			SELECT IKD.ItemKitCode, IKD.ItemCode AS ItemCode, 
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN @WarehouseCode
				ELSE EISTV.WarehouseCode
			END AS WarehouseCode, 
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN 0
				ELSE EISTV.FreeStock
			END AS FreeStock, IKD.UnitMeasureCode, II.ItemType,                  
			ROW_NUMBER() over(PARTITION BY  IKD.ItemKitCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC) as RowNum                    
			FROM InventoryItem II WITH (NOLOCK)
			INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON II.ItemCode = IKD.ItemCode                      
			LEFT JOIN EcommerceInventoryStockTotalView EISTV ON IKD.ItemCode = EISTV.ItemCode                                     
			WHERE (II.Status = 'A' OR (II.Status = 'P' AND II.ItemType IN ('Stock', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly', 'Matrix Item') 
			AND EISTV.FreeStock > 0 ))                    
			AND IKD.ItemKitCode IN (SELECT pf.ItemCode FROM InventoryItem II                     
								INNER JOIN #productfilter PF ON II.ItemCode = PF.ItemCode
								WHERE II.ItemType = 'Kit')                    
		)            
	   INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType)                    
	   SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType FROM cte_AvailableKitComponents WHERE RowNum = 1                    
   END    
                                 
   IF ( SELECT COUNT(pf.ItemCode) FROM InventoryItem II                           
		INNER JOIN #productfilter pf ON II.ItemCode = pf.ItemCode                          
		WHERE II.ItemType = 'Matrix Group' ) > 0                      
   BEGIN                                               
	   -- get the freestock of matrix item attribute/s                    
	   ;WITH cte_AvailableMatrixAttributes                    
	   AS                    
	   (                    
			SELECT IMI.ItemCode, IMI.MatrixItemCode, EISTV.WarehouseCode , EISTV.FreeStock, EISTV.UnitMeasureCode,                      
			ROW_NUMBER() over(PARTITION BY  IMI.ItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC) as RowNum                    
			FROM InventoryItem II WITH (NOLOCK)
			INNER JOIN InventoryMatrixItem IMI WITH (NOLOCK) ON II.ItemCode = IMI.MatrixItemCode                       
			INNER JOIN EcommerceInventoryStockTotalView EISTV ON IMI.MatrixItemCode = EISTV.ItemCode                                        
			WHERE (II.Status = 'A' OR (II.Status = 'P' AND II.ItemType = 'Matrix Item' AND EISTV.FreeStock > 0 ))                    
			AND IMI.ItemCode IN (SELECT pf.ItemCode FROM InventoryItem II                    
								 INNER JOIN #productfilter pf ON II.ItemCode = pf.ItemCode 
								 WHERE II.ItemType = 'Matrix Group')                    
	   )                    
	   INSERT INTO #AvailableMatrixAttributes (ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode)                    
	   SELECT ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode FROM cte_AvailableMatrixAttributes WHERE RowNum = 1                    
	END                    
           
	INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                            
	SELECT II.ItemCode, 0, UnitMeasureCode FROM InventoryItem II                    
	INNER JOIN #productfilter PF ON PF.ItemCode = II.ItemCode                                            
	INNER JOIN InventoryUnitMeasure IUM ON II.ItemCode = IUM.ItemCode                                                     
	WHERE ItemType IN ('Service', 'Electronic Download', 'Non-Stock', 'Gift Card', 'Gift Certificate')                                            
	AND IsBase = 1  
	                  
END                      
-- FreeStock from all warehouse                      
IF @ShowInventoryFromAllWarehouses = 1                      
BEGIN                      
	IF @HideOutOfStockProducts = 1                      
	BEGIN                      
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                              
		BEGIN                        
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                           
			BEGIN                        
				;WITH cte_AvailableKitComponents 
				AS
				(
					SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode, ItemType, POStatus,
					ROW_NUMBER() over(PARTITION BY ItemKitCode, UnitMeasureCode ORDER BY ItemKitCode) as RowNum                        
					FROM #AvailableKitComponents                                  
					GROUP BY ItemKitCode, UnitMeasureCode, ItemType, POStatus 
				)
				
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)
				SELECT ItemKitCode, FreeStock, UnitMeasureCode
				FROM cte_AvailableKitComponents
				WHERE RowNum = 1 AND ((FreeStock > 0 OR POStatus IN ('Open', 'Partial')) OR ItemType IN ('Non-Stock', 'Electronic Download', 'Service'))
				GROUP BY ItemKitCode, UnitMeasureCode, FreeStock                
			END                          
		END                   
	                  
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                          
		BEGIN                          
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0                     
			BEGIN                    
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                    
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode                    
				FROM #AvailableMatrixAttributes                    
				GROUP BY ItemCode, UnitMeasureCode                    
			END                       
		END                      
		                    
		;WITH cte_InventoryFreeStock                     
		AS                    
		(                    
			SELECT ItemCode, FreeStock, WarehouseCode, UnitMeasureCode,                    
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode ORDER BY POStatus DESC) as RowNum                    
			FROM #InventoryFreeStock                    
			WHERE FreeStock > 0 OR POStatus IN ('Open', 'Partial')                    
		)                    
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                    
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode                    
		FROM cte_InventoryFreeStock                    
		WHERE RowNum = 1                    
		GROUP BY ItemCode, UnitMeasureCode                    
		                  
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = II.ItemCode AND IsBase = 1                      
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = II.ItemCode AND AI.UM = IUM.UnitMeasureCode '                      
	END                      
	ELSE                      
	BEGIN  
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                          
		BEGIN                    
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                       
			BEGIN                    
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                   
				SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode                    
				FROM #AvailableKitComponents                    
				GROUP BY ItemKitCode, UnitMeasureCode                    
			END                      
		END                    
	                  
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                          
		BEGIN                          
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0                       
			BEGIN                    
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                    
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode                    
				FROM #AvailableMatrixAttributes                    
				GROUP BY ItemCode, UnitMeasureCode                    
			END                       
		END  
	                    
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                                    
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode FROM #inventoryfreestock                  
		GROUP BY ItemCode, UnitMeasureCode  
			             
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = II.ItemCode AND IsBase = 1                      
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = II.ItemCode AND AI.UM = IUM.UnitMeasureCode '                      
	END                      
END                      
-- FreeStock from specific warehouse                      
ELSE                      
BEGIN                                                               
	IF @HideOutOfStockProducts = 1                                             
	BEGIN                    
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                              
		BEGIN                              
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                           
			BEGIN                         
				;WITH cte_AvailableKitComponents 
				AS
				(
					SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode, ItemType, POStatus,
					ROW_NUMBER() over(PARTITION BY ItemKitCode, UnitMeasureCode ORDER BY ItemKitCode) as RowNum                        
					FROM #AvailableKitComponents                        
					WHERE WarehouseCode = @WarehouseCode          
					GROUP BY ItemKitCode, UnitMeasureCode, ItemType, POStatus
				)
				
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)
				SELECT ItemKitCode, FreeStock, UnitMeasureCode
				FROM cte_AvailableKitComponents
				WHERE RowNum = 1 AND ((FreeStock > 0 OR POStatus IN ('Open', 'Partial')) OR ItemType IN ('Non-Stock', 'Electronic Download', 'Service'))
				GROUP BY ItemKitCode, UnitMeasureCode, FreeStock                     
			END                          
		END                    
	                      
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                          
		BEGIN                          
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0                       
			BEGIN                 
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                    
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode                    
				FROM #AvailableMatrixAttributes                    
				WHERE WarehouseCode = @WarehouseCode                    
				GROUP BY ItemCode, UnitMeasureCode                      
			END                      
		END                      
		                   
		;WITH cte_InventoryFreeStock                     
		AS                    
		(                    
			SELECT ItemCode, FreeStock, WarehouseCode, UnitMeasureCode,                    
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum                    
			FROM #InventoryFreeStock                    
			WHERE WarehouseCode = @WarehouseCode AND (FreeStock > 0 OR POStatus IN ('Open', 'Partial'))                    
		)                    
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                    
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode                    
		FROM cte_InventoryFreeStock                    
		WHERE RowNum = 1                    
		GROUP BY ItemCode, UnitMeasureCode                    
		                  
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = II.ItemCode AND IsBase = 1                                                           
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = II.ItemCode AND AI.UM = IUM.UnitMeasureCode '                                                  
	END                    
	ELSE    
	BEGIN  
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                          
		BEGIN                          
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                       
			BEGIN                     
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                      
				SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode                    
				FROM #AvailableKitComponents                    
				WHERE WarehouseCode = @WarehouseCode                    
				GROUP BY ItemKitCode, UnitMeasureCode                    
			END                      
		END                    
	                      
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                          
		BEGIN                          
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0                       
			BEGIN                 
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                    
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode                    
				FROM #AvailableMatrixAttributes                    
				WHERE WarehouseCode = @WarehouseCode                    
				GROUP BY ItemCode, UnitMeasureCode                      
			END                      
		END                                                     
		
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                         
		SELECT ItemCode, FreeStock, UnitMeasureCode FROM #inventoryfreestock                                                          
		WHERE WarehouseCode = @WarehouseCode                                                                                                                    

		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = II.ItemCode AND IsBase = 1                                                          
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = II.ItemCode AND AI.UM = IUM.UnitMeasureCode '  
	END                                                            
END                                              
/*End of Out of Stock Items*/   
                                                                                                                                                                
DECLARE @SqlQuery NVARCHAR(4000)                                                  
DECLARE @SqlQuery2 NVARCHAR(4000)                        
DECLARE @SqlQuery3 NVARCHAR(4000)                         
DECLARE @SqlQuery4 NVARCHAR(4000)                                                  
                                                  
SET @SqlQuery = '         
  SELECT II.Counter, II.ItemCode, II.ItemName, II.ItemType, IID.ItemDescription, IID.ExtendedDescription, IIWD.WebDescription, IIWD.Summary, IIWD.SETitle, ISNULL(IID.ItemDescription,II.ItemName) AS SEName,
  IIWD.SEKeywords, IIWD.SEAltText, IIWD.SENoScript, IIW.XmlPackage, IIW.MobileXmlPackage, IIW.DisplayColumns, II.Notes, IIW.RequiresRegistration, IIW.IsFeatured, IIW.Published, IIW.ShowBuyButton,               
  IIW.HidePriceUntilCart, IIW.IsCallToOrder, IIW.IsSale_C, IIW.MinOrderQuantity, II.ManufacturerCode, SMD.Description AS ManufacturerDescription, SL.ShortString, ''Promotional Price'' AS SalesPromptName,
  IIW.ShowPricePerPiece_C, IIW.PricePerPieceLabel_C, IIW.StickerInfo_C, 
  ' + @AdditionalField + '                                                    
    ROW_NUMBER() OVER (ORDER BY ' + @SortExpression + ') AS RowNumber                                                  
   FROM InventoryItem II WITH (NOLOCK)                                                                                
   INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON II.ItemCode = IID.ItemCode                                                                              
   INNER JOIN InventoryItemWebOption IIW WITH (NOLOCK) ON II.ItemCode = IIW.ItemCode AND IIW.WebSiteCode = ' + QUOTENAME(@WebSiteCode,'''') + '                                                                               
   INNER JOIN InventoryItemWebOptionDescription IIWD WITH (NOLOCK) ON II.ItemCode = IIWD.ItemCode AND IIWD.WebSiteCode = ' + QUOTENAME(@WebSiteCode, '''')  + '                                                                         
   LEFT OUTER JOIN (                                
        SELECT SpecialPrice, InventorySpecialPricing.ItemCode                          
        FROM InventorySpecialPricing WITH (NOLOCK)                               
      INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON InventorySpecialPricing.ItemCode = IUM.ItemCode AND InventorySpecialPricing.UnitMeasureCode = IUM.UnitMeasureCode                                                   
        WHERE CONVERT(DATETIME, DateFrom, 101) <= CONVERT(DATETIME, GETDATE(), 101)                                                   
         AND CONVERT(DATETIME, DateTo, 101) >= CONVERT(DATETIME, GETDATE(), 101) AND IsBase = 1 ) ISP ON  II.ItemCode = ISP.ItemCode                                                    
   INNER JOIN SystemLanguage SL WITH (NOLOCK) ON SL.LanguageCode = IID.LanguageCode AND SL.LanguageCode = IIWD.LanguageCode                                      
   LEFT JOIN SystemManufacturerDescription SMD WITH (NOLOCK) ON II.ManufacturerCode = SMD.ManufacturerCode AND SL.LanguageCode = SMD.LanguageCode                                                                              
   INNER JOIN #productfilter p ON p.ItemCode = ii.ItemCode ' +                                                
   @AdditionalJoin                                                   
                                                      
SET @SqlQuery2 = '                                                   
 WHERE SL.ShortString=' + QUOTENAME(@LocaleName, '''') + ' AND                         
  (II.Status = ''A'' OR (II.Status = ''P'' AND II.ItemType IN (''Kit'', ''Matrix Group'')) OR (II.Status = ''P'' AND                         
  II.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'') AND AI.FreeStock > 0)) AND                         
  IIW.Published >= ' + CAST(@PublishedOnly AS VARCHAR(20)) + '                                                   
  AND (( ' + ISNULL(QUOTENAME(@ManufacturerCode,''''),'NULL') + ' IS NOT NULL AND ii.ManufacturerCode = ' + ISNULL(QUOTENAME(@ManufacturerCode,''''),'NULL') + ')                         
    OR ( ' + ISNULL(QUOTENAME(@ManufacturerCode,''''),'NULL') + ' IS NULL AND (ii.ManufacturerCode is NULL or ii.ManufacturerCode = ii.ManufacturerCode)))                         
  AND ii.ItemType IN (SELECT ItemType FROM #inventoryitemtype) ' + @AdditionalFilter +                                                     
  ' AND IIW.CheckOutOption=0 AND IIW.IsExclusive = 0 AND (ISNULL(IIW.WebsiteGroup_C, '''') = ''Smartbag'' OR ISNULL(IIW.WebsiteGroup_C, '''') = '''') 
  AND (ISNULL(IIW.NonStockMatrixType_C, '''') != ''Child'' OR ISNULL(IIW.NonStockMatrixType_C, '''') = '''')'       
SET @SqlQuery3 = '                                                 
 SELECT II.Counter, II.ItemCode, II.ItemName, II.ItemType, IID.ItemDescription, IID.ExtendedDescription, IIWD.WebDescription, IIWD.Summary, IIWD.SETitle, ISNULL(IID.ItemDescription,II.ItemName) AS SEName,                                                  
 IIWD.SEKeywords, IIWD.SEAltText, IIWD.SENoScript, IIW.XmlPackage, IIW.MobileXmlPackage, IIW.DisplayColumns, II.Notes, IIW.RequiresRegistration, IIW.IsFeatured, IIW.Published, IIW.ShowBuyButton,                                    
 IIW.HidePriceUntilCart, IIW.IsCallToOrder, IIW.IsSale_C, IIW.MinOrderQuantity, II.ManufacturerCode, SMD.Description AS ManufacturerDescription, SL.ShortString, ''Promotional Price'' AS SalesPromptName,                                           
 IIW.ShowPricePerPiece_C, IIW.PricePerPieceLabel_C, IIW.StickerInfo_C,
    ' + @AdditionalField + '                                                    
    ROW_NUMBER() OVER (ORDER BY IID.ItemDescription ASC ) AS RowNumber                                                  
   FROM InventoryItem II WITH (NOLOCK)                        
   INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON II.ItemCode = IID.ItemCode                                                                              
   INNER JOIN InventoryItemWebOption IIW WITH (NOLOCK) ON II.ItemCode = IIW.ItemCode AND IIW.WebSiteCode = ' + QUOTENAME(@WebSiteCode,'''') + '                
   INNER JOIN InventoryItemWebOptionDescription IIWD WITH (NOLOCK) ON II.ItemCode = IIWD.ItemCode AND IIWD.WebSiteCode = ' + QUOTENAME(@WebSiteCode, '''')  + '                                                                         
   LEFT OUTER JOIN (                                                                    
        SELECT SpecialPrice, InventorySpecialPricing.ItemCode                                    
        FROM InventorySpecialPricing WITH (NOLOCK)                                                     
        INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON InventorySpecialPricing.ItemCode = IUM.ItemCode AND InventorySpecialPricing.UnitMeasureCode = IUM.UnitMeasureCode                                                        
        WHERE CONVERT(DATETIME, DateFrom, 101) <= CONVERT(DATETIME, GETDATE(), 101)                                                   
        AND CONVERT(DATETIME, DateTo, 101) >= CONVERT(DATETIME, GETDATE(), 101) AND IsBase = 1 ) ISP ON  II.ItemCode = ISP.ItemCode                                                    
INNER JOIN SystemLanguage SL WITH (NOLOCK) ON SL.LanguageCode = IID.LanguageCode AND SL.LanguageCode = IIWD.LanguageCode                                                                             
   LEFT JOIN SystemManufacturerDescription SMD WITH (NOLOCK) ON II.ManufacturerCode = SMD.ManufacturerCode AND SL.LanguageCode = SMD.LanguageCode                                
   INNER JOIN #productfilter p ON p.ItemCode = ii.ItemCode ' +                                                  
   @AdditionalJoin                           
SET @SqlQuery4 = '                                                   
 WHERE SL.ShortString=' + QUOTENAME(@LocaleName, '''') + ' AND                         
  (II.Status = ''A'' OR (II.Status = ''P'' AND II.ItemType IN (''Kit'', ''Matrix Group'')) OR (II.Status = ''P'' AND                         
  II.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'') AND AI.FreeStock > 0)) AND                         
  IIW.Published >= ' + CAST(@PublishedOnly AS VARCHAR(20)) + '                                                   
  AND (( ' + ISNULL(QUOTENAME(@ManufacturerCode,''''),'NULL') + ' IS NOT NULL AND ii.ManufacturerCode = ' + ISNULL(QUOTENAME(@ManufacturerCode,''''),'NULL') + ')                         
    OR ( ' + ISNULL(QUOTENAME(@ManufacturerCode,''''),'NULL') + ' IS NULL AND (ii.ManufacturerCode is NULL or ii.ManufacturerCode = ii.ManufacturerCode)))                                
  AND ii.ItemType IN (SELECT ItemType FROM #inventoryitemtype) ' + @AdditionalFilter +                                            
  ' AND IIW.CheckOutOption = 0 AND IIW.IsExclusive = 1 AND (ISNULL(IIW.WebsiteGroup_C, '''') = ''Smartbag'' OR ISNULL(IIW.WebsiteGroup_C, '''') = '''') 
  AND (ISNULL(IIW.NonStockMatrixType_C, '''') != ''Child'' OR ISNULL(IIW.NonStockMatrixType_C, '''') = '''')'                                                                                   
SET @SqlQuery = REPLACE(REPLACE(@SqlQuery, CHAR(13), ''), CHAR(10), '')                        
SET @SqlQuery2 = REPLACE(REPLACE(@SqlQuery2, CHAR(13), ''), CHAR(10), '')                        
SET @SqlQuery3 = REPLACE(REPLACE(@SqlQuery3, CHAR(13), ''), CHAR(10), '')                        
SET @SqlQuery4 = REPLACE(REPLACE(@SqlQuery4, CHAR(13), ''), CHAR(10), '')                        
                       
 EXEC('SELECT * INTO ##EcommerceProducts FROM ('+ @SqlQuery3 + @SqlQuery4 + ' UNION ALL ' + @SqlQuery + @SqlQuery2 + ') EcommerceProducts')
 
 DECLARE @MainQuery NVARCHAR(1000)
 SET @MainQuery = 'SELECT * FROM ##EcommerceProducts WHERE RowNumber BETWEEN ((' + CAST(@pagenum AS VARCHAR(10)) + ' - 1) * ' 
 + CAST(@pagesize AS VARCHAR(20)) + ' + 1) AND (' + CAST(@pagenum AS VARCHAR(20)) + ' * ' + CAST(@pagesize AS VARCHAR(20)) + ')' 
 
 EXECUTE sp_executesql @MainQuery
 
 SELECT @rcount = COUNT(*) FROM ##EcommerceProducts        
 IF @StatsFirst = 1                                                                                        
 SELECT CAST(CEILING(@rcount*1.0/@pagesize) AS INT) pages, @rcount ProductCount             
  
 IF @StatsFirst <> 1                                                                                        
 SELECT CAST(CEILING(@rcount*1.0/@pagesize) AS INT) pages, @rcount ProductCount   
                      
 SET NOCOUNT OFF                                                                         
END       
                                                                                                                              
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#displayorder'))                                                   
    DROP TABLE #displayorder                                                     
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#inventoryfilter'))                                                                                      
    DROP TABLE #inventoryfilter                                                                        
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#inventoryitemtype'))                                                                                      
    DROP TABLE #inventoryitemtype                                                                   
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#inventoryitempricingdetail'))                                                                       
    DROP TABLE #inventoryitempricingdetail                                                                   
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#inventoryitemrating'))                                                                                      
    DROP TABLE #inventoryitemrating                                
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#inventoryitemsalesorder'))                                                                                      
    DROP TABLE #inventoryitemsalesorder                                                                   
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#inventoryitemfreestock'))                                                     
    DROP TABLE #inventoryitemfreestock                                                                   
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#inventoryitemnewarrival'))                                                                                      
    DROP TABLE #inventoryitemnewarrival                                                            
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#InventoryFreeStock'))                                                                                      
    DROP TABLE #InventoryFreeStock                                
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableItems'))                      
	DROP TABLE #AvailableItems                    
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                                                                                      
	DROP TABLE #AvailableKitComponents                    
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                                                                                      
	DROP TABLE #AvailableMatrixAttributes   
 IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..##EcommerceProducts'))                                                   
	DROP TABLE ##EcommerceProducts                
/*                   End of Script                           */ 

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[eCommerceGetNonStockMatrixGroupAttributes]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[eCommerceGetNonStockMatrixGroupAttributes]
GO

CREATE PROCEDURE [dbo].[eCommerceGetNonStockMatrixGroupAttributes]
	@Counter NVARCHAR(10),
	@ItemCode NVARCHAR(30),
	@WebsiteCode NVARCHAR(30)
AS
BEGIN

DECLARE @param NVARCHAR(600)
DECLARE @paramclean NVARCHAR(600)
DECLARE @print NVARCHAR(MAX)

SELECT @param = '(' + NonStockMatrix_C + ')' FROM InventoryItemWebOption WHERE ItemCode = @ItemCode AND WebSiteCode = @WebsiteCode
SELECT @paramclean = REPLACE(NonStockMatrix_C, '''', '') FROM InventoryItemWebOption WHERE ItemCode = @ItemCode AND WebSiteCode = @WebsiteCode

SET @print ='SELECT ' + @Counter + ' AS Counter, ''' + @ItemCode + ''' AS ItemCode, A.Counter AS NonStockMatrixCounter, ItemName, ''BagSize'' AS AttributeCode, ''Bag Size'' AS AttributeDescription, b.ItemCode AS AttributeValueCode, 
	(CASE WHEN ISNULL(ProductSize_C,'''') = '''' THEN BagSize_C ELSE + BagSize_C + '' ('' + ProductSize_C + '')'' END) AS AttributeValueDescription
	FROM InventoryItem A WITH (NOLOCK) INNER JOIN InventoryItemWebOption B WITH (NOLOCK) ON A.ItemCode = B.ItemCode
	WHERE ItemName IN ' + @param + ' AND WebSiteCode = ''' + @WebsiteCode + ''' ORDER BY CHARINDEX(CAST(ItemName AS VARCHAR), ''' + @paramclean + ''')'

--SELECT @print
EXEC (@print)	
END

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[eCommerceGetSimpleObjectEntityList]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[eCommerceGetSimpleObjectEntityList]
GO

CREATE PROCEDURE [dbo].[eCommerceGetSimpleObjectEntityList]            
	@EntityCode  VARCHAR(100),    
	@WebSiteCode  NVARCHAR(30),        
	@LanguageCode NVARCHAR(50),            
	@EntityID  INT = NULL,            
	@AffiliateCode NVARCHAR(30) = NULL,            
	@PublishedOnly TINYINT = 0,            
	@OrderByLooks TINYINT = 0,
	@ContactCode NVARCHAR(30)            
AS        
BEGIN
	SET NOCOUNT ON
	CREATE TABLE #tmp (ItemCode NVARCHAR(50), ItemName NVARCHAR(200), ItemDescription NVARCHAR(MAX), WebDescription NVARCHAR(MAX), EntityCode NVARCHAR(50), 
					   Description NVARCHAR(MAX), ParentEntity NVARCHAR(50), EntityID INT, ObjectID INT, SEName NVARCHAR(MAX), Status NVARCHAR(20)) 
	CREATE TABLE #InventoryFreeStock (WarehouseCode NVARCHAR(30), ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))           
	CREATE TABLE #AvailableItems (ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), UM NVARCHAR(30))           
	-- temporary tables to store available kit components and matrix attributes                  
	CREATE TABLE #AvailableKitComponents (ItemKitCode NVARCHAR(30), ItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), ItemType NVARCHAR(30), POStatus NVARCHAR(30))
	CREATE TABLE #AvailableMatrixAttributes (ItemCode NVARCHAR(30), MatrixItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))
	
	DECLARE @AdditionalJoin NVARCHAR(1000) = ''                                         
	DECLARE @Cmd NVARCHAR(MAX)            
	DECLARE @HideOutOfStockProducts BIT  = 0                                                        
	DECLARE @ShowInventoryFromAllWarehouses BIT = 0                                                                                    
	DECLARE @WarehouseCode NVARCHAR(30)
	
	IF @EntityCode = 'category'
	BEGIN
		INSERT #tmp (ItemCode, ItemName, ItemDescription, WebDescription, EntityCode, Description, ParentEntity, EntityID, ObjectID, SEName, Status)            
		SELECT	IC.ItemCode, 
				II.ItemName, 
				IID.ItemDescription, 
				IIWD.WebDescription, 
				IC.CategoryCode, 
				SCD.Description, 
				SC.ParentCategory, 
				SC.Counter AS EntityID, 
				II.Counter AS ObjectID, 
				SCD.SEName,
				II.Status       
		FROM SystemCategory SC WITH (NOLOCK)      
		INNER JOIN SystemCategoryDescription SCD WITH (NOLOCK) ON SC.CategoryCode = SCD.CategoryCode AND SCD.LanguageCode = @LanguageCode   
		INNER JOIN SystemCategoryWebOption SCWO WITH (NOLOCK) ON SC.CategoryCode = SCWO.CategoryCode AND SCWO.WebSiteCode = @WebSiteCode        
		INNER JOIN InventoryCategory IC WITH (NOLOCK) ON SC.CategoryCode = IC.CategoryCode            
		LEFT JOIN InventoryItem II WITH (NOLOCK) ON II.ItemCode = IC.ItemCode        
		INNER JOIN InventoryItemWebOption IIW WITH (NOLOCK) ON IIW.ItemCode = II.ItemCode AND IIW.WebSiteCode = @WebSiteCode   
		INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON IID.ItemCode = II.ItemCode         
		INNER JOIN InventoryItemWebOptionDescription IIWD WITH (NOLOCK) ON IIWD.ItemCode = II.ItemCode AND IIW.WebSiteCode = IIWD.WebSiteCode AND IID.LanguageCode = IIWD.LanguageCode  
		WHERE	SC.Counter = COALESCE(@EntityID, SC.Counter) AND 
				SCWO.Published >= @PublishedOnly AND 
				IID.LanguageCode = @LanguageCode AND
				II.Status IN ('A', 'P') AND 
				II.ItemType IN ('Electronic Download', 'Kit', 'Matrix Group', 'Non-Stock', 'Service', 'Stock', 'Assembly', 'Gift Card', 'Gift Certificate') AND 
				IIW.Published=1 AND 
				IIW.CheckOutOption=0  
		ORDER BY IC.SortOrder
		
	END        
		
	ELSE IF @EntityCode = 'department'
	BEGIN          
		INSERT #tmp (ItemCode, ItemName, ItemDescription, WebDescription, EntityCode, Description, ParentEntity, EntityID, ObjectID, SEName, Status)              
		SELECT	IID.ItemCode, 
				II.ItemName, 
				IIDesc.ItemDescription, 
				IIWD.WebDescription, 
				IID.DepartmentCode, 
				ISDD.Description, 
				ISD.ParentDepartment, 
				ISD.Counter AS EntityID, 
				II.Counter AS ObjectID, 
				ISDD.SEName,
				II.Status          
		FROM InventorySellingDepartment ISD WITH (NOLOCK)    
		INNER JOIN InventorySellingDepartmentDescription ISDD WITH (NOLOCK) ON ISD.DepartmentCode = ISDD.DepartmentCode AND ISDD.LanguageCode = @LanguageCode   
		INNER JOIN InventorySellingDepartmentWebOption ISDWO WITH (NOLOCK) ON ISD.DepartmentCode = ISDWO.DepartmentCode AND ISDWO.WebSiteCode = @WebSiteCode               
		INNER JOIN InventoryItemDepartment IID WITH (NOLOCK) ON ISD.DepartmentCode = IID.DepartmentCode            
		LEFT JOIN InventoryItem II WITH (NOLOCK) ON II.ItemCode = IID.ItemCode          
		INNER JOIN InventoryItemWebOption IIW WITH (NOLOCK) ON IIW.ItemCode = II.ItemCode AND IIW.WebSiteCode = @WebSiteCode   
		INNER JOIN InventoryItemDescription IIDesc WITH (NOLOCK) ON IIDesc.ItemCode = II.ItemCode         
		INNER JOIN InventoryItemWebOptionDescription IIWD WITH (NOLOCK) ON IIWD.ItemCode = II.ItemCode AND IIW.WebSiteCode = IIWD.WebSiteCode AND IIDesc.LanguageCode = IIWD.LanguageCode    
		WHERE	ISD.Counter = COALESCE(@EntityID, ISD.Counter) AND 
				ISDWO.Published >= @PublishedOnly AND 
				IIDesc.LanguageCode = @LanguageCode AND 
				II.Status IN ('A', 'P') AND 
				II.ItemType IN ('Electronic Download', 'Kit', 'Matrix Group', 'Non-Stock', 'Service', 'Stock', 'Assembly', 'Gift Card', 'Gift Certificate') AND 
				IIW.Published=1 AND 
				IIW.CheckOutOption=0    
		ORDER BY IID.SortOrder   
	END
			         
	ELSE IF @EntityCode = 'manufacturer'
	BEGIN       
		INSERT #tmp (ItemCode, ItemName, ItemDescription, WebDescription, EntityCode, Description, ParentEntity, EntityID, ObjectID, SEName, Status)                
		SELECT	II.ItemCode, 
				II.ItemName, 
				IID.ItemDescription, 
				IIWD.WebDescription, 
				II.ManufacturerCode, 
				SMD.Description,
				NULL, 
				SM.Counter AS EntityID, 
				II.Counter AS ObjectID, 
				SMD.SEName,
				II.Status           
		FROM SystemManufacturer SM WITH (NOLOCK)    
		INNER JOIN SystemManufacturerDescription SMD WITH (NOLOCK) ON SM.ManufacturerCode = SMD.ManufacturerCode AND SMD.LanguageCode = @LanguageCode  
		INNER JOIN SystemManufacturerWebOption SMWO WITH (NOLOCK) ON SM.ManufacturerCode = SMWO.ManufacturerCode AND SMWO.WebSiteCode = @WebSiteCode              
		INNER JOIN InventoryItem II WITH (NOLOCK) ON SM.ManufacturerCode = II.ManufacturerCode    
		INNER JOIN InventoryItemWebOption IIW WITH (NOLOCK) ON IIW.ItemCode = II.ItemCode AND IIW.WebSiteCode = @WebSiteCode   
		INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON IID.ItemCode = II.ItemCode         
		INNER JOIN InventoryItemWebOptionDescription IIWD WITH (NOLOCK) ON IIWD.ItemCode = II.ItemCode AND IIW.WebSiteCode = IIWD.WebSiteCode AND IID.LanguageCode = IIWD.LanguageCode          
		WHERE	II.ManufacturerCode IS NOT NULL AND 
				SM.Counter = COALESCE(@EntityID, SM.Counter) AND 
				SMWO.Published >= @PublishedOnly AND 
				IID.LanguageCode = @LanguageCode AND 
				II.Status IN ('A', 'P') AND
				II.ItemType IN ('Electronic Download', 'Kit', 'Matrix Group', 'Non-Stock', 'Service', 'Stock', 'Assembly', 'Gift Card', 'Gift Certificate') AND 
				IIW.Published=1 AND 
				IIW.CheckOutOption=0      
	END
	
	SELECT @HideOutOfStockProducts = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'HideOutOfStockProducts' AND WebSiteCode = @WebSiteCode    
	SELECT @ShowInventoryFromAllWarehouses = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'ShowInventoryFromAllWarehouses' AND WebSiteCode = @WebSiteCode    
	SET @WarehouseCode = (SELECT TOP 1 WareHouseCode FROM CustomerShipTo WHERE ShipToCode = (SELECT DefaultShippingCode FROM CRMContact WHERE ContactCode = @ContactCode))          	
	
	-- Get freestock of non-kit/non-matrix group items (Stock, Matrix Item, Assembly)    
	IF @HideOutOfStockProducts = 1  
	BEGIN  
		INSERT INTO #inventoryfreestock (ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)    
		SELECT EISTV.ItemCode, EISTV.WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM EcommerceInventoryStockTotalView EISTV  
		INNER JOIN #tmp tmp ON tmp.ItemCode = EISTV.ItemCode    
		LEFT JOIN SupplierPurchaseOrderDetail SPOD ON SPOD.ItemCode = tmp.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode    
		LEFT JOIN SupplierPurchaseOrder SPO ON spo.PurchaseOrderCode = SPOD.PurchaseOrderCode    
	END  
	ELSE  
	BEGIN  
		INSERT INTO #inventoryfreestock (ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)  
		SELECT EISTV.ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, NULL FROM EcommerceInventoryStockTotalView EISTV  
		INNER JOIN #tmp tmp ON tmp.ItemCode = EISTV.ItemCode  
	END
	
	IF @HideOutOfStockProducts = 1    
	BEGIN    
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                          
		SELECT II.ItemCode, 0, UnitMeasureCode FROM InventoryItem II  
		INNER JOIN #tmp tmp ON tmp.ItemCode = II.ItemCode                          
		INNER JOIN InventoryUnitMeasure IUM ON II.ItemCode = IUM.ItemCode                                   
		WHERE ItemType IN ( 'Service', 'Bundle', 'Electronic Download', 'Non-Stock', 'Gift Card', 'Gift Certificate')                          
		AND IsBase = 1    

		IF ( SELECT COUNT(DISTINCT tmp.ItemCode) FROM InventoryItem II         
			 INNER JOIN #tmp tmp        
			 ON II.ItemCode = tmp.ItemCode        
			 WHERE II.ItemType = 'Kit' ) > 0  
		BEGIN  
		-- get the freestock of kit component/s  
		;WITH cte_AvailableKitComponents   
		AS  
		(  
			SELECT IKD.ItemKitCode, IKD.ItemCode AS ItemCode, 
			CASE 
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN @WarehouseCode
				ELSE EISTV.WarehouseCode
			END AS WarehouseCode,
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN 0
				ELSE EISTV.FreeStock
			END AS FreeStock, IKD.UnitMeasureCode, II.ItemType, OrderStatus,    
			ROW_NUMBER() over(PARTITION BY  IKD.ItemKitCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum  
			FROM InventoryItem II WITH (NOLOCK)
			INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON II.ItemCode = IKD.ItemCode
			LEFT JOIN EcommerceInventoryStockTotalView EISTV ON IKD.ItemCode = EISTV.ItemCode  
			LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IKD.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode    
			LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode     
			WHERE ItemKitCode IN (SELECT DISTINCT tmp.ItemCode  
								  FROM InventoryItem II   
								  INNER JOIN #tmp tmp  
								  ON II.ItemCode = tmp.ItemCode WHERE II.ItemType = 'Kit')  
		)  
		INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType, POStatus)  
		SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType, OrderStatus FROM cte_AvailableKitComponents WHERE RowNum = 1  
		END    

		IF ( SELECT COUNT(DISTINCT tmp.ItemCode) FROM InventoryItem II         
			 INNER JOIN #tmp tmp        
			 ON II.ItemCode = tmp.ItemCode        
			 WHERE II.ItemType = 'Matrix Group' ) > 0    
		BEGIN         
		-- get the freestock of matrix item attribute/s  
		;WITH cte_AvailableMatrixAttributes  
		AS  
		(  
			SELECT IMI.ItemCode, IMI.MatrixItemCode, EISTV.WarehouseCode , EISTV.FreeStock, EISTV.UnitMeasureCode, OrderStatus,    
			ROW_NUMBER() over(PARTITION BY  IMI.ItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum  
			FROM InventoryMatrixItem IMI     
			INNER JOIN EcommerceInventoryStockTotalView EISTV ON IMI.MatrixItemCode = EISTV.ItemCode  
			LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IMI.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode    
			LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode    
			WHERE (EISTV.FreeStock > 0 OR OrderStatus IN ('Open', 'Partial'))  
			AND IMI.ItemCode IN (SELECT DISTINCT tmp.ItemCode  
			FROM InventoryItem II   
			INNER JOIN #tmp tmp  
			ON II.ItemCode = tmp.ItemCode WHERE II.ItemType = 'Matrix Group')  
		)  
		INSERT INTO #AvailableMatrixAttributes (ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)  
		SELECT ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM cte_AvailableMatrixAttributes WHERE RowNum = 1  
		END  
	END  
	ELSE    
	BEGIN    
		IF ( SELECT COUNT(tmp.ItemCode) FROM InventoryItem II                               
			 INNER JOIN #tmp tmp ON II.ItemCode = tmp.ItemCode      
			 WHERE II.ItemType = 'Kit' ) > 0                        
		BEGIN                          
			-- get the freestock of kit component/s                        
			;WITH cte_AvailableKitComponents                         
			AS                        
			(                        
				SELECT IKD.ItemKitCode, IKD.ItemCode AS ItemCode, 
				CASE 
					WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN @WarehouseCode
					ELSE EISTV.WarehouseCode
				END AS WarehouseCode,
				CASE
					WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN 0
					ELSE EISTV.FreeStock
				END AS FreeStock, IKD.UnitMeasureCode, II.ItemType,                          
				ROW_NUMBER() over(PARTITION BY  IKD.ItemKitCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC) as RowNum                        
				FROM InventoryItem II WITH (NOLOCK)    
				INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON II.ItemCode = IKD.ItemCode                          
				LEFT JOIN EcommerceInventoryStockTotalView EISTV ON IKD.ItemCode = EISTV.ItemCode                                         
				WHERE (II.Status = 'A' OR (II.Status = 'P' AND II.ItemType IN ('Stock', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly', 'Matrix Item')     
				AND EISTV.FreeStock > 0 ))                        
				AND IKD.ItemKitCode IN (SELECT tmp.ItemCode FROM InventoryItem II                         
										INNER JOIN #tmp tmp ON II.ItemCode = tmp.ItemCode    
										WHERE II.ItemType = 'Kit')                        
			)    

			INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType)                        
			SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType FROM cte_AvailableKitComponents WHERE RowNum = 1                        
		END  

		IF ( SELECT COUNT(tmp.ItemCode) FROM InventoryItem II                               
			 INNER JOIN #tmp tmp ON II.ItemCode = tmp.ItemCode                              
			 WHERE II.ItemType = 'Matrix Group' ) > 0                          
		BEGIN                                 
			-- get the freestock of matrix item attribute/s                        
			;WITH cte_AvailableMatrixAttributes                        
			AS                        
			(                        
				SELECT IMI.ItemCode, IMI.MatrixItemCode, EISTV.WarehouseCode , EISTV.FreeStock, EISTV.UnitMeasureCode,                          
				ROW_NUMBER() over(PARTITION BY  IMI.ItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC) as RowNum                        
				FROM InventoryItem II WITH (NOLOCK)    
				INNER JOIN InventoryMatrixItem IMI WITH (NOLOCK) ON II.ItemCode = IMI.MatrixItemCode                           
				INNER JOIN EcommerceInventoryStockTotalView EISTV ON IMI.MatrixItemCode = EISTV.ItemCode                                            
				WHERE (II.Status = 'A' OR (II.Status = 'P' AND II.ItemType = 'Matrix Item' AND EISTV.FreeStock > 0 ))                        
				AND IMI.ItemCode IN (SELECT tmp.ItemCode FROM InventoryItem II                        
									 INNER JOIN #tmp tmp ON II.ItemCode = tmp.ItemCode     
									 WHERE II.ItemType = 'Matrix Group')                        
			)                        
			INSERT INTO #AvailableMatrixAttributes (ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode)                        
			SELECT ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode FROM cte_AvailableMatrixAttributes WHERE RowNum = 1                        
		END    

		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                          
		SELECT II.ItemCode, 0, UnitMeasureCode FROM InventoryItem II  
		INNER JOIN #tmp tmp ON tmp.ItemCode = II.ItemCode                           
		INNER JOIN InventoryUnitMeasure IUM ON II.ItemCode = IUM.ItemCode                                   
		WHERE ItemType IN ('Service', 'Electronic Download', 'Non-Stock', 'Gift Card', 'Gift Certificate')                          
		AND IsBase = 1
	    
	END
	
	-- FreeStock from all warehouse    
	IF @ShowInventoryFromAllWarehouses = 1    
	BEGIN    
		IF @HideOutOfStockProducts = 1    
		BEGIN
			IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                                
			BEGIN                          
				IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                             
				BEGIN                          
					;WITH cte_AvailableKitComponents   
					AS  
					(  
						SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode, ItemType, POStatus,  
						ROW_NUMBER() over(PARTITION BY ItemKitCode, UnitMeasureCode ORDER BY ItemKitCode) as RowNum                          
						FROM #AvailableKitComponents                                    
						GROUP BY ItemKitCode, UnitMeasureCode, ItemType, POStatus
					)  
					  
					INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)  
					SELECT ItemKitCode, FreeStock, UnitMeasureCode  
					FROM cte_AvailableKitComponents  
					WHERE RowNum = 1 AND ((FreeStock > 0 OR POStatus IN ('Open', 'Partial')) OR ItemType IN ('Non-Stock', 'Electronic Download', 'Service'))  
					GROUP BY ItemKitCode, UnitMeasureCode, FreeStock                        
				END                            
			END  

			IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttribute'))        
			BEGIN        
				IF ((SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes)) > 0     
				BEGIN  
					INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)  
					SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode  
					FROM #AvailableMatrixAttributes  
					GROUP BY ItemCode, UnitMeasureCode  
				END    
			END    

			;WITH cte_InventoryFreeStock   
			AS  
			(  
			SELECT ItemCode, FreeStock, WarehouseCode, UnitMeasureCode,  
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum  
			FROM #InventoryFreeStock  
			WHERE FreeStock > 0 OR POStatus IN ('Open', 'Partial')  
			)  
			INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)  
			SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode  
			FROM cte_InventoryFreeStock  
			WHERE RowNum = 1  
			GROUP BY ItemCode, UnitMeasureCode  

			SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = #tmp.ItemCode AND IsBase = 1               
			INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = #tmp.ItemCode AND AI.UM = IUM.UnitMeasureCode '      
		END    
		ELSE    
		BEGIN  
			IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))        
			BEGIN        
				IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0     
				BEGIN  
					INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)    
					SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode  
					FROM #AvailableKitComponents  
					GROUP BY ItemKitCode, UnitMeasureCode  
				END    
			END  

			IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttribute'))        
			BEGIN        
				IF ((SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes)) > 0     
				BEGIN  
					INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)  
					SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode  
					FROM #AvailableMatrixAttributes  
					GROUP BY ItemCode, UnitMeasureCode  
				END    
			END     

			INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                  
			SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode FROM #inventoryfreestock    
			GROUP BY ItemCode, UnitMeasureCode    
			SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = #tmp.ItemCode AND IsBase = 1               
			INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = #tmp.ItemCode AND AI.UM = IUM.UnitMeasureCode '               
		END    
	END    
	-- FreeStock from specific warehouse    
	ELSE    
	BEGIN                                             
		IF @HideOutOfStockProducts = 1                                        
		BEGIN    
			IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                              
			BEGIN                              
				IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                           
				BEGIN                         
					;WITH cte_AvailableKitComponents 
					AS
					(
						SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode, ItemType, POStatus,
						ROW_NUMBER() over(PARTITION BY ItemKitCode, UnitMeasureCode ORDER BY ItemKitCode) as RowNum                        
						FROM #AvailableKitComponents                        
						WHERE WarehouseCode = @WarehouseCode          
						GROUP BY ItemKitCode, UnitMeasureCode, ItemType, POStatus 
					)
					
					INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)
					SELECT ItemKitCode, FreeStock, UnitMeasureCode
					FROM cte_AvailableKitComponents
					WHERE RowNum = 1 AND ((FreeStock > 0 OR POStatus IN ('Open', 'Partial')) OR ItemType IN ('Non-Stock', 'Electronic Download', 'Service'))
					GROUP BY ItemKitCode, UnitMeasureCode, FreeStock                     
				END                          
			END  

			IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))        
			BEGIN        
				IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0     
				BEGIN  
					INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)  
					SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode  
					FROM #AvailableMatrixAttributes  
					WHERE WarehouseCode = @WarehouseCode  
					GROUP BY ItemCode, UnitMeasureCode    
				END    
			END  

			;WITH cte_InventoryFreeStock   
			AS  
			(  
			SELECT ItemCode, FreeStock, WarehouseCode, UnitMeasureCode,  
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum  
			FROM #InventoryFreeStock  
			WHERE WarehouseCode = @WarehouseCode AND (FreeStock > 0 OR POStatus IN ('Open', 'Partial'))  
			)  
			INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)  
			SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode  
			FROM cte_InventoryFreeStock  
			WHERE RowNum = 1  
			GROUP BY ItemCode, UnitMeasureCode                             

			SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = #tmp.ItemCode AND IsBase = 1               
			INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = #tmp.ItemCode AND AI.UM = IUM.UnitMeasureCode '               
		END  
		ELSE                                        
		BEGIN  
			IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))        
			BEGIN        
				IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0     
				BEGIN    
					INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)    
					SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode  
					FROM #AvailableKitComponents  
					WHERE WarehouseCode = @WarehouseCode  
					GROUP BY ItemKitCode, UnitMeasureCode  
				END    
			END  

			IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))        
			BEGIN        
				IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0     
				BEGIN  
					INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)  
					SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode  
					FROM #AvailableMatrixAttributes  
					WHERE WarehouseCode = @WarehouseCode  
					GROUP BY ItemCode, UnitMeasureCode    
				END    
			END   
		             
			INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                       
			SELECT ItemCode, FreeStock, UnitMeasureCode FROM #inventoryfreestock                                        
			WHERE WarehouseCode = @WarehouseCode                               
			SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = #tmp.ItemCode AND IsBase = 1                              
			INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = #tmp.ItemCode AND AI.UM = IUM.UnitMeasureCode '                                         
		END                                           
	END
	
	SET @Cmd = 'SELECT #tmp.ItemCode, ItemName, ItemDescription, WebDescription, EntityCode, Description, ParentEntity, EntityID, ObjectID, SEName FROM #tmp' + @AdditionalJoin + 'WHERE (Status = ''A'' OR (Status = ''P'' AND AI.FreeStock > 0))'
	EXEC (@Cmd)
	  
	SET NOCOUNT OFF				
END    

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[eCommerceSearchInventory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[eCommerceSearchInventory]
GO

CREATE PROCEDURE [dbo].[eCommerceSearchInventory] --[eCommerceSearchInventory] 'Blackberry Curve', 'WEB-000001','English - United States', NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0', '2011-05-04 13:55:33.350',''                                      
 @SearchTerm   NVARCHAR(3000),                                                                        
 @WebSiteCode   NVARCHAR(30),                                                                        
 @LanguageCode   NVARCHAR(50),                                                                      
 @ItemCode    NVARCHAR(1000) = NULL,                                                                       
 @ItemType    NVARCHAR(50) = NULL,                                                                    
 @UnitMeasure   NVARCHAR(30) = NULL,                                                                    
 @CategoryID   NVARCHAR (100) = NULL,                                                          
 @SectionID    NVARCHAR (100) = NULL,                                                          
 @ManufacturerID  NVARCHAR (100) = NULL,                                      
 @AttributeID      NVARCHAR (100) = NULL,                                      
 @SearchDescriptions    VARCHAR(5),                                                              
 @CurrentDate   DATETIME,                                                        
 @ProductFilterID  NVARCHAR(50),                                    
 @ContactCode NVARCHAR(30),                  
 @CBMode BIT = 0,            
 @PageNum INT = 1,          
 @CurrencyCode NVARCHAR(30) = NULL,        
 @MinPrice NUMERIC(18,6),                            
 @MaxPrice NUMERIC(18,6)        
                                         
AS        
                                                                                                 
BEGIN                                                                        
 SET NOCOUNT ON                
 --SET ANSI_WARNINGS OFF                                                                      
               
 DECLARE @SearchDescriptionString NVARCHAR(MAX)                                      
 DECLARE @JoinUnitMeasureString NVARCHAR(MAX)                                       
 DECLARE @EntityBuilderString NVARCHAR(MAX)                                                          
 DECLARE @ProductFilter NVARCHAR(MAX)                                   
 DECLARE @AdditionalFilter NVARCHAR(MAX)           
 DECLARE @AdditionalField NVARCHAR(1000)      
 DECLARE @SKUString NVARCHAR(MAX) = ' '    
 DECLARE @SKUAdditionalFields NVARCHAR(MAX) = ' '        
 DECLARE @SKUAdditionalJoin NVARCHAR(MAX) = ' '      
                                                                                                               
 SET @AdditionalField = ''                  
 DECLARE @PageSize INT             
 SELECT @PageSize = ConfigValue FROM EcommerceAppConfig WITH (NOLOCK) WHERE Name = 'SearchPageSize' AND WebSiteCode = @WebsiteCode          
     
 IF @PageSize IS NULL OR @PageSize = 0          
 BEGIN                                      
 SET @PageSize = 999999999           
 END       
        
 -------------UPCCode------------------      
 SELECT ItemCode, UnitMeasureCode, UPCCode, ShowOnWeb INTO #TempInventoryUnitMeasure     
 FROM InventoryUnitMeasure WITH (NOLOCK)        
 WHERE UPCCode = @SearchTerm        
     
 IF EXISTS(SELECT 1 FROM #TempInventoryUnitMeasure)     
 BEGIN    
 SET @SKUAdditionalFields = ', TIUM.UPCCode, TIUM.UnitMeasureCode '    
 SET @SKUAdditionalJoin = 'INNER JOIN #TempInventoryUnitMeasure TIUM WITH (NOLOCK) ON TIUM.ItemCode = A.ItemCode AND TIUM.ShowOnWeb = 1'    
 SET @SKUString = ' OR TIUM.UPCCode = ' + QUOTENAME(@SearchTerm, '''')  + ' '     
 END     
                               
 SET @AdditionalFilter=''                                          
 SET @SearchTerm = REPLACE(@SearchTerm,'''','''''')                                                                                     
 SET @SearchTerm = REPLACE(@SearchTerm,'+',' ')                     
 SET @SearchTerm = RTRIM(LTRIM(@SearchTerm))             
 SET @ItemCode = NULLIF(@ItemCode, NULL)                                                                     
 SET @EntityBuilderString = ''                                                    
 SET @ProductFilter = ''                                       
                                          
    -- ItemType                                      
 IF (@ItemType = 'ANY')                                                                  
  SET @ItemType = '''%'''                                        
 ELSE                                                               
  SET @ItemType = '''%' + @ItemType + '%'''                                                                     
                                          
    -- UnitMeasure                                      
    IF (@UnitMeasure = 'ANY')                      
  SET @UnitMeasure = '''%'''                                                                        
 ELSE                                                                    
  SET @UnitMeasure = '%' + @UnitMeasure + '%'                                                                      
                                       
 -- SearchDescriptions                                      
 IF (@SearchDescriptions = '0')                                      
  SET @SearchDescriptionString = ' '                                      
 ELSE                                                                    
  SET @SearchDescriptionString = ' OR IIWOD.WebDescription LIKE ' + QUOTENAME(@SearchTerm, '''') + ' '                                      
             
 -- UnitMeasure                                      
 IF (@UnitMeasure = '''%''')                                        
  SET @JoinUnitMeasureString = ' '                                                                     
 ELSE                                           
  SET @JoinUnitMeasureString = ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode                                       
           AND ISNULL(UnitMeasureQty,0)>0                                
           AND IUM.UnitMeasureCode LIKE ' + QUOTENAME(@UnitMeasure, '''') + ' '                                                  
                                       
 -- CategoryID                                      
 IF (@CategoryID <> '0' AND @CategoryID <> '')                     
 BEGIN                                                          
  SET @EntityBuilderString = 'INNER JOIN (SELECT ItemCode FROM InventoryCategory A WITH (NOLOCK)                                       
            INNER JOIN SystemCategory B WITH (NOLOCK) ON A.CategoryCode = B.CategoryCode                                       
            WHERE B.Counter = ' + QUOTENAME(@CategoryID, '''') + ') IC ON IC.ItemCode = A.ItemCode '                                                          
 END                                        
                                        
 -- SectionID                                      
 IF (@SectionID <> '0' AND @SectionID <> '')                                                  
 BEGIN                                        
  SET @EntityBuilderString += ' INNER JOIN (SELECT ItemCode FROM InventoryItemDepartment A WITH (NOLOCK)                                       
             INNER JOIN InventorySellingDepartment B WITH (NOLOCK) ON A.DepartmentCode = B.DepartmentCode                                       
       WHERE B.Counter = ' + QUOTENAME(@SectionID, '''') + ') IIDep ON IIDep.ItemCode = A.ItemCode '                                                          
 END                                      
                                  
 -- ManufcaturerID              
 IF (@ManufacturerID <> '0' AND @ManufacturerID <> '')                                                          
 BEGIN                                             
  SET @EntityBuilderString += ' INNER JOIN (SELECT A.ManufacturerCode, A.ItemCode                                       
             FROM InventoryItem A WITH (NOLOCK)                                       
             INNER JOIN SystemManufacturer B WITH (NOLOCK) ON A.ManufacturerCode = B.ManufacturerCode                                       
             WHERE B.Counter = ' + QUOTENAME(@ManufacturerID, '''') + ') SM ON SM.ItemCode = A.ItemCode '                                                          
 END                                      
                                       
 -- AttributeID                                      
 IF (@AttributeID <> '0' AND @AttributeID <> '')                                                          
 BEGIN                                           
  SET @EntityBuilderString += ' INNER JOIN (SELECT A.AttributeValue, A.ItemCode                                       
             FROM InventoryItemAttributeView A WITH (NOLOCK)                                       
             INNER JOIN SystemItemAttributeSourceFilterValue B WITH (NOLOCK) ON A.AttributeValue = B.SourceFilterName                                       
              AND A.AttributeSourceFilterCode = B.AttributeSourceFilterCode                                       
             WHERE A.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + '                                       
              AND B.Counter = ' + QUOTENAME(@AttributeID, '''') + ') SA ON SA.ItemCode = A.ItemCode '                                                          
 END                                                         
                                                        
    -- ProductFilterID                                      
 IF @ProductFilterID IS NULL                                                    
  SET @ProductFilterID  = ''                                                     
                                                     
 IF @ProductFilterID <> ''                                                      
 BEGIN                                                                
  SET @ProductFilter = ' INNER JOIN (SELECT Distinct(ItemCode),TemplateID                                       
           FROM InventoryProductFilterTemplateItem                                       
    WHERE TemplateID = ' + QUOTENAME(@ProductFilterID, '''') + ') PF ON A.ItemCode = PF.ItemCode                                       
            AND PF.TemplateID = ' + QUOTENAME(@ProductFilterID, '''') + ' '                                                  
 END                       
           
IF @CurrencyCode IS NULL            
BEGIN            
 SELECT @CurrencyCode = CurrencyCode            
 FROM Customer CU WITH (NOLOCK)            
 INNER JOIN CRMContact CO WITH (NOLOCK) ON CO.EntityCode = CU.CustomerCode             
  AND CO.ContactCode = @ContactCode            
END            
                                         
------------------Ranking----------------------------------------              
 CREATE TABLE #TempSearchTerms (SearchTerm NVARCHAR(MAX))                 
 INSERT INTO #TempSearchTerms (SearchTerm) VALUES (@SearchTerm);              
-----------------------------------------------------------------                                        
 DECLARE @SearchTermAdditional NVARCHAR(MAX)                                      
 DECLARE @SearchTermSplit NVARCHAR(MAX)                                     
 DECLARE @splitstring NVARCHAR(3000)                                      
 DECLARE @substring NVARCHAR(3000)                                      
             
 DECLARE @ignoreString NVARCHAR(1000)= ',,'            
 DECLARE @ignoreFilter BIT = 0;            
 SELECT @ignoreString = ',' + RTRIM(LTRIM(REPLACE(ISNULL(ConfigValue,''),'''',''''''))) + ',' FROM EcommerceAppConfig Where Name ='SearchIgnoredWords' AND WebsiteCode =@WebSiteCode            
            
 IF (CHARINDEX(','+@SearchTerm+',',@ignoreString) > 0)              
 BEGIN            
 SELECT *            
    FROM dbo.InventoryItem             
    WHERE 1=0    
                
    RETURN            
 END             
                                                         
 SET @splitstring = @SearchTerm + ' '                                      
 SET @SearchTermSplit  = ''                                      
     
 WHILE (CHARINDEX(' ', @splitstring, 1)<>0)                                      
 BEGIN                        
  SET @substring = SUBSTRING(@splitstring ,1, CHARINDEX(' ',@splitstring ,1)-1)                                       
                                       
 IF @substring <> ' '                                      
  BEGIN                                      
               
   IF LEN(@substring) > 2 AND @substring  <> ',' AND @substring  <> '%'            
   BEGIN            
               
    SET @ignoreFilter = 0            
    IF (@ignoreString <> ',,' AND CHARINDEX(','+@substring+',',@ignoreString) = 0 )            
    BEGIN            
      SET @ignoreFilter = 1            
    END            
                
     IF ( @ignoreFilter = 1 OR @ignoreString = ',,')            
    BEGIN            
     ------------------Ranking----------------------------------------              
     INSERT INTO #TempSearchTerms (SearchTerm) VALUES (@substring);              
     -----------------------------------------------------------------            
                      
     SET @SearchTermSplit += ' (IID.ItemDescription LIKE ' + '''%' + @substring + '%''' + '                                       
      OR REPLACE(IIWOD.SEKeywords,'','', '''') LIKE ' + '''%' + @substring + '%''' + '                                       
      OR A.ItemName LIKE ' + '''%' + @substring + '%''' + ' ' + @SearchDescriptionString  + @SKUString + ')  '                
                 
     SET @SearchTermSplit += 'OR'            
    END            
   END                                                    
   SET @splitstring = SUBSTRING(@splitstring ,LEN(@substring)+2,LEN(@splitstring ))                                      
   SET @splitstring = LTRIM(@splitstring)                                 
                 
  END                                      
 END                                      
             
IF (ISNULL(@SearchTermSplit,'')='')            
BEGIN            
 SELECT *            
    FROM dbo.InventoryItem             
    WHERE 1=0            
                
    RETURN            
END            
                                      
 SET @SearchTermAdditional = SUBSTRING(@SearchTermSplit, 1, LEN(@SearchTermSplit)-2)                                      
/*CBN Mode*/                  
 IF @CBMode = 1                  
 BEGIN                  
  SET @AdditionalFilter = @AdditionalFilter + ' AND A.IsCBN = 1 '                  
 END                             
                          
/*Out of Stock Items*/                      
CREATE TABLE #TempSearchResult (ItemCode NVARCHAR(30), ItemType NVARCHAR(30))                      
CREATE TABLE #InventoryFreeStock (WarehouseCode NVARCHAR(30), ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))                       
CREATE TABLE #AvailableItems (ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), UM NVARCHAR(30))                      
-- temporary tables to store available kit components and matrix attributes                
CREATE TABLE #AvailableKitComponents (ItemKitCode NVARCHAR(30), ItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), ItemType NVARCHAR(30), POStatus NVARCHAR(30))         
CREATE TABLE #AvailableMatrixAttributes (ItemCode NVARCHAR(30), MatrixItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))                        
                                            
DECLARE @SQL NVARCHAR(MAX)                                        
DECLARE @AdditionalJoin NVARCHAR(MAX)                                                      
DECLARE @SqlQuery NVARCHAR(MAX)                        
DECLARE @HideOutOfStockProducts BIT                                                                       
DECLARE @ShowInventoryFromAllWarehouses BIT                                                                                                
DECLARE @WarehouseCode NVARCHAR(30)                        
                                                               
SET @AdditionalJoin = ''                                          
SET @SqlQuery = ''                         
SET @HideOutOfStockProducts =  0                                   
SET @ShowInventoryFromAllWarehouses = 0                                                                                                               
                                  
SELECT @HideOutOfStockProducts = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'HideOutOfStockProducts' AND WebSiteCode = @WebSiteCode                
SELECT @ShowInventoryFromAllWarehouses = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'ShowInventoryFromAllWarehouses' AND WebSiteCode = @WebSiteCode                
SET @WarehouseCode = (SELECT TOP 1 WareHouseCode FROM CustomerShipTo WHERE ShipToCode = (SELECT DefaultShippingCode FROM CRMContact WHERE ContactCode = @ContactCode))                      
                      
SET @SQL = ' SELECT A.ItemCode, A.ItemType FROM InventoryItem A WITH (NOLOCK)                                               
     LEFT JOIN (SELECT ItemCode FROM InventoryItemPricingDetail B WITH (NOLOCK)                                               
     INNER JOIN SystemCurrency C WITH (NOLOCK) ON B.CurrencyCode = C.CurrencyCode WHERE IsHomeCurrency = 1) D ON A.ItemCode = D.ItemCode                                               
     LEFT JOIN (SELECT TOP 1 ItemKitCode FROM InventoryKitPricingDetail E WITH (NOLOCK)                                               
     INNER JOIN SystemCurrency F WITH (NOLOCK) ON E.CurrencyCode = F.CurrencyCode WHERE IsHomeCurrency = 1) G ON A.ItemCode = G.ItemKitCode                                               
     INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON IID.ItemCode = A.ItemCode           
      AND IID.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + '                                               
     INNER JOIN InventoryItemWebOption IIWO WITH (NOLOCK) ON IIWO.ItemCode = A.ItemCode                                               
      AND IIWO.WebsiteCode = ' + QUOTENAME(@WebsiteCode, '''') + '                                               
     INNER JOIN InventoryItemWebOptionDescription IIWOD WITH (NOLOCK) ON IIWOD.ItemCode = IIWO.ItemCode                              
      AND IIWOD.WebsiteCode = IIWO.WebsiteCode                                               
      AND IIWOD.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + ' ' + @SKUAdditionalJoin + ' ' +                                           
      @JoinUnitMeasureString + ' ' +                                              
      @EntityBuilderString  + ' ' +                         
      @ProductFilter  + ' ' +                                        
      @AdditionalJoin + '                                           
     WHERE (' + @SearchTermAdditional + ' )                                              
       AND  ItemType IN (''Non-Stock'',                                               
            ''Service'',                        
            ''Stock'',                                              
            ''Matrix Group'',                                              
            ''Kit'',                                              
            ''Service'',                                              
            ''Electronic Download'',                                               
            ''Assembly'',            
            ''Gift Card'',            
            ''Gift Certificate'')                                              
       AND ''' + CAST(@CurrentDate AS NVARCHAR(50)) + ''' BETWEEN ISNULL(IIWO.StartDate, ''1/1/1900'')                                               
       AND ISNULL(IIWO.EndDate, ''12/31/9999'')                            
       AND (A.Status = ''A'' OR (A.Status = ''P'' AND A.ItemType IN (''Kit'', ''Matrix Group'')) OR (A.Status = ''P''                                
       AND A.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'')))                                       
       AND IIWO.Published = 1                                 
       AND IIWO.CheckOutOption = 0                                               
       AND ItemType LIKE ' + @ItemType +                         
        @AdditionalFilter + '                        
       ORDER BY IIWO.IsExclusive DESC, A.ItemName ASC'                      
                             
INSERT INTO #TempSearchResult (ItemCode, ItemType)                      
EXEC (@SQL)                      
/* remove giftcard or giftcertificate items that are not sellable to customer's currency */            
DELETE FROM #TempSearchResult            
WHERE ItemCode IN ( SELECT II.ItemCode            
     FROM InventoryItem II WITH (NOLOCK)            
     INNER JOIN InventoryItemPricingDetail IP WITH (NOLOCK) ON IP.ItemCode = II.ItemCode            
      AND IP.CurrencyCode = @CurrencyCode             
      AND IP.[Enabled] = 0 
      AND II.ItemType IN ('Gift Card','Gift Certificate'))            
-- Get freestock of non-kit/non-matrix group items (Stock, Matrix Item, Assembly)            
IF @HideOutOfStockProducts = 1          
BEGIN          
 INSERT INTO #inventoryfreestock (ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)            
 SELECT EISTV.ItemCode, EISTV.WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM EcommerceInventoryStockTotalView EISTV            
 INNER JOIN #TempSearchResult TSR ON TSR.ItemCode = EISTV.ItemCode          
 LEFT JOIN SupplierPurchaseOrderDetail SPOD ON SPOD.ItemCode = TSR.ItemCode AND spod.WarehouseCode = EISTV.WarehouseCode AND spod.UnitMeasure = EISTV.UnitMeasureCode            
 LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode            
END          
ELSE          
BEGIN          
 INSERT INTO #inventoryfreestock (ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)          
 SELECT EISTV.ItemCode, EISTV.WarehouseCode, FreeStock, UnitMeasureCode, NULL FROM EcommerceInventoryStockTotalView EISTV          
 INNER JOIN #TempSearchResult TSR ON TSR.ItemCode = EISTV.ItemCode          
END

IF @HideOutOfStockProducts = 1            
BEGIN            
 INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                  
 SELECT II.ItemCode, 0, UnitMeasureCode FROM InventoryItem II          
 INNER JOIN #TempSearchResult TSR ON TSR.ItemCode = II.ItemCode                                  
 INNER JOIN InventoryUnitMeasure IUM ON II.ItemCode = IUM.ItemCode                                           
 WHERE II.ItemType IN ( 'Service', 'Electronic Download', 'Non-Stock', 'Gift Card', 'Gift Certificate')                                  
 AND IsBase = 1            
             
 IF ( SELECT COUNT(TSR.ItemCode) FROM InventoryItem II              
	  INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode AND II.ItemType = 'Kit' ) > 0          
 BEGIN          
  -- get the freestock of kit component/s          
  ;WITH cte_AvailableKitComponents           
   AS          
   (          
    SELECT IKD.ItemKitCode, IKD.ItemCode AS ItemCode, 
    CASE
		WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN @WarehouseCode
		ELSE EISTV.WarehouseCode
	END AS WarehouseCode, 
	CASE
		WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN 0
		ELSE EISTV.FreeStock
	END AS FreeStock, IKD.UnitMeasureCode, II.ItemType, OrderStatus,            
    ROW_NUMBER() over(PARTITION BY  IKD.ItemKitCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum          
    FROM InventoryItem II WITH (NOLOCK)
    INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON II.ItemCode = IKD.ItemCode             
    LEFT JOIN EcommerceInventoryStockTotalView EISTV ON IKD.ItemCode = EISTV.ItemCode          
    LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IKD.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode            
    LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode
    WHERE ItemKitCode IN (SELECT TSR.ItemCode          
						FROM InventoryItem II              
						INNER JOIN #TempSearchResult TSR              
						ON II.ItemCode = TSR.ItemCode              
						AND II.ItemType = 'Kit')          
   )          
   INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType, POStatus)          
   SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType, OrderStatus FROM cte_AvailableKitComponents WHERE RowNum = 1          
 END            
            
 IF ( SELECT COUNT(TSR.ItemCode) FROM InventoryItem II              
   INNER JOIN #TempSearchResult TSR              
   ON II.ItemCode = TSR.ItemCode              
   AND II.ItemType = 'Matrix Group' ) > 0            
 BEGIN                 
  -- get the freestock of matrix item attribute/s          
  ;WITH cte_AvailableMatrixAttributes          
   AS          
   (          
    SELECT IMI.ItemCode, IMI.MatrixItemCode, EISTV.WarehouseCode , EISTV.FreeStock, EISTV.UnitMeasureCode, OrderStatus,            
    ROW_NUMBER() over(PARTITION BY  IMI.ItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum          
    FROM InventoryMatrixItem IMI             
    INNER JOIN EcommerceInventoryStockTotalView EISTV ON IMI.MatrixItemCode = EISTV.ItemCode          
    LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IMI.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode            
    LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode            
    WHERE (EISTV.FreeStock > 0 OR OrderStatus IN ('Open', 'Partial'))          
    AND IMI.ItemCode IN (SELECT TSR.ItemCode          
          FROM InventoryItem II              
             INNER JOIN #TempSearchResult TSR              
             ON II.ItemCode = TSR.ItemCode              
             AND II.ItemType = 'Matrix Group')          
   )          
   INSERT INTO #AvailableMatrixAttributes (ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)          
   SELECT ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM cte_AvailableMatrixAttributes WHERE RowNum = 1          
 END          
END          
ELSE            
BEGIN
	IF ( SELECT COUNT(TSR.ItemCode) FROM InventoryItem II              
		 INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode 
		 AND II.ItemType = 'Kit' ) > 0                     
	BEGIN                        
		-- get the freestock of kit component/s             
		;WITH cte_AvailableKitComponents                       
		AS                      
		(                      
			SELECT IKD.ItemKitCode, IKD.ItemCode AS ItemCode, 
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN @WarehouseCode
				ELSE EISTV.WarehouseCode
			END AS WarehouseCode,
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN 0
				ELSE EISTV.FreeStock
			END AS FreeStock, IKD.UnitMeasureCode, II.ItemType,                        
			ROW_NUMBER() over(PARTITION BY  IKD.ItemKitCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC) as RowNum                      
			FROM InventoryItem II WITH (NOLOCK)  
			INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON II.ItemCode = IKD.ItemCode                        
			LEFT JOIN EcommerceInventoryStockTotalView EISTV ON IKD.ItemCode = EISTV.ItemCode                                       
			WHERE (II.Status = 'A' OR (II.Status = 'P' AND II.ItemType IN ('Stock', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly', 'Matrix Item')   
			AND EISTV.FreeStock > 0 ))                      
			AND IKD.ItemKitCode IN ( SELECT TSR.ItemCode FROM InventoryItem II              
									 INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode
									 AND II.ItemType = 'Kit')                      
		)              
		INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType)                      
		SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType FROM cte_AvailableKitComponents WHERE RowNum = 1                      
	END      

	IF ( SELECT COUNT(TSR.ItemCode) FROM InventoryItem II              
		 INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode              
		 AND II.ItemType = 'Matrix Group' ) > 0                       
	BEGIN                             
		-- get the freestock of matrix item attribute/s                      
		;WITH cte_AvailableMatrixAttributes                      
		AS                      
		(                      
			SELECT IMI.ItemCode, IMI.MatrixItemCode, EISTV.WarehouseCode , EISTV.FreeStock, EISTV.UnitMeasureCode,                        
			ROW_NUMBER() over(PARTITION BY  IMI.ItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC) as RowNum                      
			FROM InventoryItem II WITH (NOLOCK)  
			INNER JOIN InventoryMatrixItem IMI WITH (NOLOCK) ON II.ItemCode = IMI.MatrixItemCode                         
			INNER JOIN EcommerceInventoryStockTotalView EISTV ON IMI.MatrixItemCode = EISTV.ItemCode                                          
			WHERE (II.Status = 'A' OR (II.Status = 'P' AND II.ItemType = 'Matrix Item' AND EISTV.FreeStock > 0 ))                      
			AND IMI.ItemCode IN ( SELECT TSR.ItemCode FROM InventoryItem II              
								  INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode              
								  AND II.ItemType = 'Matrix Group')                      
		)                      
		INSERT INTO #AvailableMatrixAttributes (ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode)                      
		SELECT ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode FROM cte_AvailableMatrixAttributes WHERE RowNum = 1                      
	END 
	                 
	INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                  
	SELECT II.ItemCode, 0, UnitMeasureCode FROM InventoryItem II          
	INNER JOIN #TempSearchResult TSR ON TSR.ItemCode = II.ItemCode                             
	INNER JOIN InventoryUnitMeasure IUM ON II.ItemCode = IUM.ItemCode                                           
	WHERE II.ItemType IN ('Service', 'Electronic Download', 'Non-Stock', 'Gift Card', 'Gift Certificate')                                  
	AND IsBase = 1            
END         
-- FreeStock from all warehouse            
IF @ShowInventoryFromAllWarehouses = 1            
BEGIN            
	IF @HideOutOfStockProducts = 1            
	BEGIN            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                              
		BEGIN                        
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                           
			BEGIN                        
				;WITH cte_AvailableKitComponents 
				AS
				(
					SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode, ItemType, POStatus,
					ROW_NUMBER() over(PARTITION BY ItemKitCode, UnitMeasureCode ORDER BY ItemKitCode) as RowNum                        
					FROM #AvailableKitComponents                                  
					GROUP BY ItemKitCode, UnitMeasureCode, ItemType, POStatus
				)
				
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)
				SELECT ItemKitCode, FreeStock, UnitMeasureCode
				FROM cte_AvailableKitComponents
				WHERE RowNum = 1 AND ((FreeStock > 0 OR POStatus IN ('Open', 'Partial')) OR ItemType IN ('Non-Stock', 'Electronic Download', 'Service'))
				GROUP BY ItemKitCode, UnitMeasureCode, FreeStock                      
			END                          
		END          
		        
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                
		BEGIN                
			IF ((SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes)) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableMatrixAttributes          
				GROUP BY ItemCode, UnitMeasureCode          
			END            
		END            
		           
		;WITH cte_InventoryFreeStock           
		AS          
		(          
			SELECT ItemCode, FreeStock, WarehouseCode, UnitMeasureCode,          
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum          
			FROM #InventoryFreeStock          
			WHERE FreeStock > 0 OR POStatus IN ('Open', 'Partial')     
		)          
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
		FROM cte_InventoryFreeStock          
		WHERE RowNum = 1          
		GROUP BY ItemCode, UnitMeasureCode          
		        
		IF (@UnitMeasure = '''%''')        
			SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode AND IUM.IsBase = 1                  
			INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '          
		ELSE       
			SET @AdditionalJoin += ' INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '                 
	END            
	ELSE            
	BEGIN 
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                
		BEGIN          
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)            
				SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableKitComponents          
				GROUP BY ItemKitCode, UnitMeasureCode          
			END             
		END          
		        
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                
		BEGIN                
			IF ((SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes)) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableMatrixAttributes          
				GROUP BY ItemCode, UnitMeasureCode          
			END            
		END
		           
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                          
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode FROM #inventoryfreestock            
		GROUP BY ItemCode, UnitMeasureCode       
		    
		IF (@UnitMeasure = '''%''')             
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode AND IUM.IsBase = 1                  
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '        
		ELSE       
		SET @AdditionalJoin += ' INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '             
	END            
END            
-- FreeStock from specific warehouse            
ELSE            
BEGIN                                                     
	IF @HideOutOfStockProducts = 1                                                
	BEGIN            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                              
		BEGIN                              
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                           
			BEGIN                         
				;WITH cte_AvailableKitComponents 
				AS
				(
					SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode, ItemType, POStatus,
					ROW_NUMBER() over(PARTITION BY ItemKitCode, UnitMeasureCode ORDER BY ItemKitCode) as RowNum                        
					FROM #AvailableKitComponents                        
					WHERE WarehouseCode = @WarehouseCode          
					GROUP BY ItemKitCode, UnitMeasureCode, ItemType, POStatus
				)
				
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)
				SELECT ItemKitCode, FreeStock, UnitMeasureCode
				FROM cte_AvailableKitComponents
				WHERE RowNum = 1 AND ((FreeStock > 0 OR POStatus IN ('Open', 'Partial')) OR ItemType IN ('Non-Stock', 'Electronic Download', 'Service'))
				GROUP BY ItemKitCode, UnitMeasureCode, FreeStock                     
			END                          
		END          
	            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                
		BEGIN                
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableMatrixAttributes          
				WHERE WarehouseCode = @WarehouseCode          
				GROUP BY ItemCode, UnitMeasureCode            
			END            
		END          
	        
		;WITH cte_InventoryFreeStock           
		AS          
		(          
			SELECT ItemCode, FreeStock, WarehouseCode, UnitMeasureCode,          
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum          
			FROM #InventoryFreeStock          
			WHERE WarehouseCode = @WarehouseCode AND (FreeStock > 0 OR POStatus IN ('Open', 'Partial'))          
		)          
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
		FROM cte_InventoryFreeStock          
		WHERE RowNum = 1          
		GROUP BY ItemCode, UnitMeasureCode        
		                                 
		IF (@UnitMeasure = '''%''')                            
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode AND IUM.IsBase = 1                           
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '             
		ELSE       
		SET @AdditionalJoin += ' INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '       
	                 
	END          
	ELSE                                  
	BEGIN            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                
		BEGIN                
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0             
			BEGIN            
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)            
				SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableKitComponents          
				WHERE WarehouseCode = @WarehouseCode          
				GROUP BY ItemKitCode, UnitMeasureCode          
			END            
		END          
		            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                
		BEGIN                
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableMatrixAttributes          
				WHERE WarehouseCode = @WarehouseCode          
				GROUP BY ItemCode, UnitMeasureCode            
			END            
		END  
		                                              
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                               
		SELECT ItemCode, FreeStock, UnitMeasureCode FROM #inventoryfreestock                                                
		WHERE WarehouseCode = @WarehouseCode       
		       
		IF (@UnitMeasure = '''%''')
		BEGIN            
			SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode AND IUM.IsBase = 1                                          
			INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '                 
		END
		ELSE
		BEGIN      
			SET @AdditionalJoin += ' INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '                                           
		END
	END                                                   
END                                                               
/*End of Out of Stock Items*/            
         
--------------------------------------ITEM---PRICE----------------------------------------         
 IF @MinPrice IS NULL             
 SET @MinPrice = 0            
             
 IF @MaxPrice IS NULL         
 SET @MaxPrice = 999999999        
                       
 DECLARE @DefaultPrice NVARCHAR(20)           
 SELECT @DefaultPrice = DefaultPrice           
 FROM Customer CU                            
 INNER JOIN CRMContact CC ON CC.EntityCode = CU.CustomerCode           
 AND CC.ContactCode = @ContactCode                                  
                                              
 CREATE TABLE #inventoryitempricingdetail (ItemCode NVARCHAR(30) NOT NULL, WholeSalePrice NUMERIC(18,6), RetailPrice NUMERIC(18,6))                                               
                         
 INSERT INTO #inventoryitempricingdetail                                        
 SELECT IIPD.ItemCode, WholeSalePrice, RetailPrice FROM InventoryItemPricingDetail IIPD WITH (NOLOCK)
 INNER JOIN #TempSearchResult PF ON IIPD.ItemCode = PF.ItemCode   
 INNER JOIN InventoryItem II ON  II.ItemCode = PF.ItemCode         
 WHERE II.ItemType <> 'Kit' AND CurrencyCode = @CurrencyCode                                      
                                         
 --Insert pricing info for kit items in #inventoryitempricingdetail                
 IF (SELECT COUNT(PF.ItemCode) FROM InventoryItem II                
  INNER JOIN #TempSearchResult PF ON II.ItemCode = PF.ItemCode                                   
  WHERE II.ItemType = 'Kit') >  0                
                   
 BEGIN              
  CREATE TABLE #KitItems (ItemCode NVARCHAR(30) NOT NULL, UseCustomerPricing BIT)                
  INSERT INTO #KitItems                  
  SELECT PF.ItemCode, ISNULL(IK.IsGetItemPrice, 0) FROM InventoryItem II WITH (NOLOCK)                                     
  INNER JOIN InventoryItem PF WITH (NOLOCK) ON II.ItemCode = PF.ItemCode                
  INNER JOIN InventoryKit IK WITH (NOLOCK) ON PF.ItemCode = IK.ItemKitCode                                    
  WHERE II.ItemType = 'Kit'              
                  
  INSERT INTO #inventoryitempricingdetail                                                            
  SELECT IKD.ItemKitCode as ItemCode, SUM(IIPD.WholeSalePrice) AS WholeSalePrice, SUM(IIPD.RetailPrice) AS RetailPrice FROM InventoryKitDetail IKD WITH (NOLOCK)                   
  INNER JOIN InventoryItemPricingDetail IIPD WITH (NOLOCK)  ON IKD.ItemCode = IIPD.ItemCode AND IIPD.CurrencyCode = @CurrencyCode               
  INNER JOIN #KitItems KI ON KI.ItemCode = IKD.ItemKitCode                 
  WHERE IIPD.CurrencyCode = @CurrencyCode AND IKD.IsDefault = 1 AND KI.UseCustomerPricing = 1                
  GROUP BY IKD.ItemKitCode                 
  UNION ALL               
  SELECT IKD.ItemKitCode, SUM(TotalRate) AS WholeSalePrice, SUM(TotalRate) AS RetailPrice FROM InventoryKitPricingDetail IKPD WITH (NOLOCK)                
  INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON IKPD.ItemKitCode = IKD.ItemKitCode AND IKPD.ItemCode = IKD.ItemCode                
  INNER JOIN #KitItems KI ON KI.ItemCode = IKD.ItemKitCode                
  WHERE IKPD.CurrencyCode = @CurrencyCode AND IKD.IsDefault = 1 AND KI.UseCustomerPricing = 0                
  GROUP BY IKD.ItemKitCode                                                                                                      
 END                   
                                                  
 IF @DefaultPrice = 'Retail'                            
 BEGIN                            
  SET @AdditionalFilter = @AdditionalFilter + ' AND ISNULL(RetailPrice,0) >= ' + QUOTENAME(@MinPrice,'''') + ' AND ISNULL(RetailPrice,0) <= ' + QUOTENAME(@MaxPrice,'''') + ' '                           
 END                            
 ELSE                             
 BEGIN                             
  SET @AdditionalFilter = @AdditionalFilter + ' AND ISNULL(WholesalePrice,0) >= ' + QUOTENAME(@MinPrice,'''') + ' AND ISNULL(WholesalePrice,0) <= ' + QUOTENAME(@MaxPrice,'''') + ' '                            
 END             
             
 SET @AdditionalField = @AdditionalField + ' , IIPD.WholeSalePrice AS WholeSalePrice, IIPD.RetailPrice AS RetailPrice '             
 SET @AdditionalJoin = @AdditionalJoin + ' LEFT JOIN #inventoryitempricingdetail IIPD WITH (NOLOCK) ON IIPD.ItemCode = A.ItemCode '                                                                        
                                                               
--------------------------------------Ranking----------------------------------------              
CREATE TABLE #TempSearchResultRanking ( ItemCode NVARCHAR(30)              
            ,KeyWords NVARCHAR(MAX)              
            ,Ranking BIGINT            
            ,CategoryID INT            
            ,CategoryDisplayName NVARCHAR(100)            
            ,ManufacturerID INT            
            ,ManufacturerDisplayName NVARCHAR(100)            
            )                  
SET @SQL  = '               
SELECT A.ItemCode, (ISNULL(IID.ItemDescription,'''') +'' ''+ ISNULL(IIWOD.SEKeywords,'''') +'' ''+ ISNULL(A.ItemName,'''')) AS KeyWords, 0 AS Ranking            
,NULL AS CategoryID ,NULL AS CategoryDisplayName ,NULL AS ManufacturerID ,NULL AS ManufacturerDisplayName            
FROM InventoryItem A WITH (NOLOCK)                                       
 LEFT JOIN (SELECT ItemCode FROM InventoryItemPricingDetail B WITH (NOLOCK)                                       
     INNER JOIN SystemCurrency C WITH (NOLOCK) ON B.CurrencyCode = C.CurrencyCode WHERE IsHomeCurrency = 1) D ON A.ItemCode = D.ItemCode                                       
  LEFT JOIN (SELECT TOP 1 ItemKitCode FROM InventoryKitPricingDetail E WITH (NOLOCK)                                       
     INNER JOIN SystemCurrency F WITH (NOLOCK) ON E.CurrencyCode = F.CurrencyCode WHERE IsHomeCurrency = 1) G ON A.ItemCode = G.ItemKitCode                                       
     INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON IID.ItemCode = A.ItemCode                                       
      AND IID.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + '                                       
     INNER JOIN InventoryItemWebOption IIWO WITH (NOLOCK) ON IIWO.ItemCode = A.ItemCode                                       
      AND IIWO.WebsiteCode = ' + QUOTENAME(@WebsiteCode, '''') + '                                       
     INNER JOIN InventoryItemWebOptionDescription IIWOD WITH (NOLOCK) ON IIWOD.ItemCode = IIWO.ItemCode                                   
      AND IIWOD.WebsiteCode = IIWO.WebsiteCode                                       
      AND IIWOD.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + ' '  + @SKUAdditionalJoin +                                 
      @JoinUnitMeasureString + ' ' +                                      
      @EntityBuilderString  + ' ' +                                      
      @ProductFilter  + ' ' +                                
      @AdditionalJoin + '              
      WHERE (' + @SearchTermAdditional + ' )                                      
       AND  ItemType IN (''Non-Stock'',                                       
            ''Service'',                                       
            ''Stock'',                                      
          ''Matrix Group'',                             
            ''Kit'',                                      
            ''Service'',                                      
            ''Electronic Download'',                                       
            ''Assembly'',            
            ''Gift Card'',            
            ''Gift Certificate'')                                      
       AND ''' + CAST(@CurrentDate AS NVARCHAR(50)) + ''' BETWEEN ISNULL(IIWO.StartDate, ''1/1/1900'')                                       
       AND ISNULL(IIWO.EndDate, ''12/31/9999'')                    
       AND (A.Status = ''A'' OR (A.Status = ''P'' AND A.ItemType IN (''Kit'', ''Matrix Group'')) OR (A.Status = ''P''                        
       AND A.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'') AND AI.FreeStock > 0))                               
       AND IIWO.Published = 1                                       
       AND IIWO.CheckOutOption = 0                                       
       AND ItemType LIKE ' + @ItemType +                 
        @AdditionalFilter +               
       'GROUP BY A.ItemCode,IID.ItemDescription,IIWOD.SEKeywords,A.ItemName'                    
               
 INSERT INTO #TempSearchResultRanking (ItemCode, KeyWords,Ranking,CategoryID,CategoryDisplayName,ManufacturerID,ManufacturerDisplayName)                      
 EXEC (@SQL)              
               
 -------------Word Match------------------              
 DECLARE @word AS NVARCHAR(MAX)              
 WHILE EXISTS(SELECT 1 FROM #TempSearchTerms)              
 BEGIN              
 SELECT TOP 1 @word = SearchTerm FROM #TempSearchTerms              
               
 -- in word              
 EXEC('UPDATE #TempSearchResultRanking               
    SET Ranking = Ranking + 1 + (SELECT COUNT(1) FROM #TempSearchTerms)               
    WHERE REPLACE(Keywords,'','', '''') Like' + '''%' + @word + '%''')              
               
 --exact word                 
 EXEC('UPDATE #TempSearchResultRanking               
    SET Ranking = Ranking + 1 + (SELECT COUNT(1) FROM #TempSearchTerms)               
    WHERE REPLACE(Keywords,'','', '''') Like' + '''% ' + @word + ' %''')              
 DELETE FROM #TempSearchTerms WHERE SearchTerm = @word              
 END              
             
 --Update CategoryCode            
 UPDATE SRR            
 SET    SRR.CategoryID = IC.[Counter],            
  SRR.CategoryDisplayName = IC.[Description]            
 FROM   #TempSearchResultRanking SRR            
 LEFT JOIN (            
  SELECT A.ItemCode, C.[Counter],D.[Description] FROM InventoryCategory A WITH (NOLOCK)            
  INNER JOIN SystemCategory C WITH (NOLOCK) ON A.CategoryCode = C.CategoryCode            
  INNER JOIN SystemCategoryWebOption B WITH (NOLOCK) ON C.CategoryCode = B.CategoryCode            
  INNER JOIN SystemCategoryDescription D WITH (NOLOCK) ON B.CategoryCode = D.CategoryCode            
  WHERE A.IsPrimary =1 AND Published = 1 AND C.IsActive = 1 AND D.LanguageCode = @LanguageCode AND WebSiteCode = @WebSiteCode            
     ) IC ON IC.ItemCode = SRR.ItemCode              
             
UPDATE SRR            
SET    SRR.ManufacturerID = IC.[Counter],            
  SRR.ManufacturerDisplayName = IC.[Description]            
FROM   #TempSearchResultRanking SRR            
LEFT JOIN (            
  SELECT A.ItemCode, C.[Counter],D.[Description] FROM InventoryItem A WITH (NOLOCK)            
  INNER JOIN SystemManufacturer C WITH (NOLOCK) ON A.ManufacturerCode = C.ManufacturerCode            
  INNER JOIN SystemManufacturerWebOption B WITH (NOLOCK) ON C.ManufacturerCode = B.ManufacturerCode            
  INNER JOIN SystemManufacturerDescription D WITH (NOLOCK) ON B.ManufacturerCode = D.ManufacturerCode            
  WHERE Published = 1 AND C.IsActive = 1 AND D.LanguageCode = @LanguageCode AND WebSiteCode = @WebSiteCode            
     ) IC ON IC.ItemCode = SRR.ItemCode              
            
-----------------------------------------------              
---------------------------------------------------------------------------------------------------------------------------------------              
               
 SET @SQL  = ' SELECT * INTO #TempSearchResultTable FROM (              
 SELECT IID.ItemDescription, IIWOD.WebDescription,A.* ,SRR.CategoryID ,SRR.CategoryDisplayName ,SRR.ManufacturerID ,SRR.ManufacturerDisplayName,SRR.Ranking,IIWO.IsExclusive,IIWO.IsFeatured,
 IIWO.ShowPricePerPiece_C, IIWO.PricePerPieceLabel_C,
  ROW_NUMBER() OVER (ORDER BY IID.ItemDescription, A.ItemName, A.ItemCode) AS RowNumber ' + @SKUAdditionalFields + @AdditionalField +          
  ' FROM InventoryItem A WITH (NOLOCK)                    
     LEFT JOIN (SELECT ItemCode FROM InventoryItemPricingDetail B WITH (NOLOCK)                                    
     INNER JOIN SystemCurrency C WITH (NOLOCK) ON B.CurrencyCode = C.CurrencyCode WHERE IsHomeCurrency = 1) D ON A.ItemCode = D.ItemCode                                       
  LEFT JOIN (SELECT TOP 1 ItemKitCode FROM InventoryKitPricingDetail E WITH (NOLOCK)                                       
     INNER JOIN SystemCurrency F WITH (NOLOCK) ON E.CurrencyCode = F.CurrencyCode WHERE IsHomeCurrency = 1) G ON A.ItemCode = G.ItemKitCode                                       
     INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON IID.ItemCode = A.ItemCode                                       
      AND IID.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + '                                       
     INNER JOIN InventoryItemWebOption IIWO WITH (NOLOCK) ON IIWO.ItemCode = A.ItemCode                                       
      AND IIWO.WebsiteCode = ' + QUOTENAME(@WebsiteCode, '''') + '                                       
     INNER JOIN InventoryItemWebOptionDescription IIWOD WITH (NOLOCK) ON IIWOD.ItemCode = IIWO.ItemCode            
      AND IIWOD.WebsiteCode = IIWO.WebsiteCode                                       
      AND IIWOD.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + ' ' +                                      
      @JoinUnitMeasureString + ' ' +                                      
      @EntityBuilderString  + ' ' +                                      
      @ProductFilter  + ' ' +         
      @AdditionalJoin + @SKUAdditionalJoin +  '              
      INNER JOIN #TempSearchResultRanking SRR WITH (NOLOCK) ON SRR.ItemCode = A.ItemCode      
     WHERE (' + @SearchTermAdditional + ' )                                      
       AND  ItemType IN (''Non-Stock'',                                       
            ''Service'',                                       
            ''Stock'',                                      
            ''Matrix Group'',                                      
            ''Kit'',                                      
            ''Service'',                                      
            ''Electronic Download'',                                       
            ''Assembly'',            
            ''Gift Card'',            
            ''Gift Certificate'')                                      
       AND ''' + CAST(@CurrentDate AS NVARCHAR(50)) + ''' BETWEEN ISNULL(IIWO.StartDate, ''1/1/1900'')                                       
       AND ISNULL(IIWO.EndDate, ''12/31/9999'')                    
       AND (A.Status = ''A'' OR (A.Status = ''P'' AND A.ItemType IN (''Kit'', ''Matrix Group'')) OR (A.Status = ''P''                        
       AND A.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'') AND AI.FreeStock > 0))                               
       AND IIWO.Published = 1                                       
       AND IIWO.CheckOutOption = 0          
	   AND (ISNULL(IIWO.WebsiteGroup_C, '''') = ''Smartbag'' OR ISNULL(IIWO.WebsiteGroup_C, '''') = '''') 
	   AND (ISNULL(IIWO.NonStockMatrixType_C, '''') != ''Child'' OR ISNULL(IIWO.NonStockMatrixType_C, '''') = '''')                                                  
       AND ItemType LIKE ' + @ItemType +                 
        @AdditionalFilter + '                
       ) SortedResult;                 

	   DELETE #TempSearchResultTable WHERE CategoryID IS NULL                    
       SELECT * FROM #TempSearchResultTable WHERE RowNumber BETWEEN ((' + CAST(@PageNum AS VARCHAR(10)) + ' - 1) * ' + CAST(@PageSize AS VARCHAR(20)) + ' + 1) AND ('           
       + CAST(@PageNum AS VARCHAR(20)) + ' * ' + CAST(@PageSize AS VARCHAR(20)) + ')              
       ORDER BY CostCenter_DEV004817 DESC, Ranking DESC, ItemDescription;                 
                   
       DECLARE @RCount INT              
       SELECT @RCount = COUNT(*) FROM #TempSearchResultTable                     
       SELECT CAST(CEILING(@RCount*1.0/'++ CAST(@PageSize AS VARCHAR(20))+') AS INT) pages, @RCount ProductCount'                
              
 EXEC(@SQL)          
 
           
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#InventoryFreeStock'))          
 DROP TABLE #InventoryFreeStock          
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableItems'))          
 DROP TABLE #AvailableItems          
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                                                                            
 DROP TABLE #AvailableKitComponents          
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                                                                            
 DROP TABLE #AvailableMatrixAttributes     
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#TempInventoryUnitMeasure'))                                                                                    
 DROP TABLE #TempInventoryUnitMeasure          
           
 SET NOCOUNT OFF                                                                  
END     

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[EcommerceGetCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EcommerceGetCustomer]
GO

CREATE PROCEDURE [dbo].[EcommerceGetCustomer](              
 @ContactGuid UNIQUEIDENTIFIER,              
 @WebsiteCode NVARCHAR(30),              
 @CurrentDate DATETIME
                     
)              
AS              
BEGIN              
SET NOCOUNT ON              
 DECLARE @CustomerSessionID int, @LastActivity datetime, @SessionTimeOut varchar(10), @intSessionTimeOut int              
    SELECT @LastActivity = '1/1/1900', @CustomerSessionID = -1              
    SELECT @SessionTimeOut = ConfigValue FROM dbo.EcommerceAppConfig WITH (NOLOCK) WHERE [Name] = 'SessionTimeoutInMinutes' AND WebSiteCode = @WebsiteCode            
    IF ISNUMERIC(@SessionTimeOut) = 1              
        set @intSessionTimeOut = convert(int, @SessionTimeOut)              
    ELSE              
        set @intSessionTimeOut = 60              
    SELECT  @CustomerSessionID  = cs.CustomerSessionID , @LastActivity = cs.LastActivity                
    FROM dbo.EcommerceCustomerSession cs WITH (NOLOCK)               
    JOIN (              
  SELECT MAX(s.CustomerSessionID) CustomerSessionID               
        FROM dbo.EcommerceCustomerSession s WITH (NOLOCK)               
  WHERE s.ContactGuid = @ContactGuid and s.LoggedOut IS NULL AND s.LastActivity >= DATEADD(mi, -@intSessionTimeOut, @CurrentDate)              
 ) a on cs.CustomerSessionID = a.CustomerSessionID              
 
 DECLARE @IsCompanyPortal BIT = 0
 SELECT @IsCompanyPortal = CASE CustomerTypeCode WHEN 'Company Portal' THEN 1 ELSE 0 END FROM Customer A INNER JOIN CRMContact B ON A.CustomerCode = B.EntityCode WHERE B.ContactGUID = @ContactGuid
               
  -- check if the customer is registered                 
 IF(EXISTS(SELECT 1 FROM CRMContact C WITH (NOLOCK)         
  INNER JOIN EcommerceCustomerActiveSites E ON C.ContactCode=E.ContactCode         
  WHERE C.ContactGUID = @ContactGuid AND E.WebSiteCode=@WebsiteCode AND E.IsEnabled =1))            
 BEGIN              

  IF (@IsCompanyPortal = 0)
  BEGIN             
	  IF (SELECT ISNULL(DefaultShippingCode, '') FROM CRMContact WHERE ContactGUID = @ContactGuid) = ''            
	  BEGIN              
		UPDATE CRMContact SET DefaultShippingCode = (SELECT DefaultShipToCode FROM Customer WHERE CustomerCode = EntityCode) WHERE ContactGUID = @ContactGuid            
	  END            
              
	  IF (SELECT ISNULL(DefaultBillingCode, '') FROM CRMContact WHERE ContactGUID = @ContactGuid) = ''            
	  BEGIN              
		UPDATE CRMContact SET DefaultBillingCode = EntityCode WHERE ContactGUID = @ContactGuid            
	  END            
  END
  ELSE
  BEGIN
	  IF (SELECT ISNULL(ShipToCode_DEV004817, '') FROM CRMContact WHERE ContactGUID = @ContactGuid) = ''            
	  BEGIN              
		UPDATE CRMContact SET ShipToCode_DEV004817 = (SELECT DefaultShipToCode FROM Customer WHERE CustomerCode = EntityCode) WHERE ContactGUID = @ContactGuid            
	  END            
              
	  IF (SELECT ISNULL(BillingCCCode_DEV004817, '') FROM CRMContact WHERE ContactGUID = @ContactGuid) = ''            
	  BEGIN              
		UPDATE CRMContact SET BillingCCCode_DEV004817 = EntityCode WHERE ContactGUID = @ContactGuid            
	  END            
  END
                
  SELECT c.CustomerGuid,                  
    c.CustomerCode,      
	c.CustomerName,   
	c.CustomerTypeCode,        
    c.CurrencyCode,              
    CASE @IsCompanyPortal WHEN 0 THEN c.CreditCardCode ELSE BillingCCCode_DEV004817 END AS DefaultBillToCode,              
    CASE @IsCompanyPortal WHEN 0 THEN csv.ShipToCode ELSE ShipToCode_DEV004817 END AS DefaultShipToCode,              
    c.DefaultPrice,              
    c.PricingMethod,              
    c.PricingLevel,               
    c.PricingPercent,              
    c.DiscountType,              
    c.Discount,        
    c.DiscountBand,        
    c.Notes,                 
    csv.WarehouseCode,              
    csv.PaymentTermGroup,              
    csv.PaymentTermCode,              
    CASE                 
      WHEN sl.ShortString IS NOT NULL THEN sl.ShortString                
      ELSE sci.ShortString END AS LocaleSetting,                
    CASE                 
      WHEN cc.LanguageCode IS NOT NULL THEN cc.LanguageCode                
      ELSE sci.LanguageCode END AS LanguageCode,                
    cc.ContactSalutationCode AS Salutation,              
    cc.ContactFirstName AS FirstName,              
    cc.ContactLastName AS LastName,              
    cc.Email1 AS Email,              
    cc.ContactCode,   
	cc.JobRoleCode,
	cc.StoreMovex_C,
	cc.StoreAdmin_C,         
    (ISNULL(cc.ContactSalutationCode + ' ', '') + ISNULL(cc.ContactFirstName, '') + ISNULL(' ' + cc.ContactMiddleName, '') + ISNULL(' ' + cc.ContactLastName, '') + ISNULL(' ' + cc.ContactSuffixCode, '')) AS ContactFullName,                  
    cc.BusinessPhone AS Phone,              
    ecas.SubscriptionExpDate,              
    tp.PaymentMethodCode AS PaymentMethod,              
    c.BusinessType,              
    c.TaxNumber,  
    cc.IsOkToEmail,              
    CAST(1 AS BIT) AS IsRegistered,              
    CAST(0 AS BIT) AS HasAnonymousRecord,              
	NULL AS AnonymousCustomerCode,              
    NULL AS AnonymousShipToCode,    
	c.CostCenter_DEV004817,          
    c.CouponCode,              
    c.Over13Checked AS IsOver13,          
    c.IsCreditHold,              
    0 as IsUpdated,              
    @CustomerSessionID AS CustomerSessionID,              
    @LastActivity AS LastActivity,          
    cc.ProductFilterID,              
    cc.ContactGUID,              
    CASE @IsCompanyPortal WHEN 0 THEN cc.DefaultBillingCode ELSE cc.BillingCCCode_DEV004817 END AS DefaultBillingCode,              
    CASE @IsCompanyPortal WHEN 0 THEN cc.DefaultShippingCode ELSE cc.ShipToCode_DEV004817 END AS DefaultShippingCode,                    
    c.TrackLoyaltyPoints,    
	cc.Mobile,
	csv.FreightTax AS FreightTaxCode
  FROM Customer c WITH (NOLOCK)            
  INNER JOIN CRMContact cc WITH (NOLOCK) ON cc.[Type] = 'CustomerContact' AND cc.EntityCode = c.CustomerCode            
  INNER JOIN CustomerShipToView csv ON csv.CustomerCode = c.CustomerCode AND csv.ShipToCode = CASE @IsCompanyPortal WHEN 0 THEN cc.DefaultShippingCode ELSE ShipToCode_DEV004817 END
  INNER JOIN SystemPaymentTerm pt WITH (NOLOCK) ON pt.PaymentTermCode = csv.PaymentTermCode              
  INNER JOIN SystemPaymentType tp WITH (NOLOCK) ON pt.PaymentType = tp.PaymentTypeCode              
  INNER JOIN EcommerceCustomerActiveSites ecas WITH (NOLOCK) ON cc.ContactCode = ecas.ContactCode               
  LEFT OUTER JOIN SystemLanguage sl WITH (NOLOCK) ON sl.LanguageCode = cc.LanguageCode                
  LEFT OUTER JOIN (                 
   -- special case handling since LanguageCode is not required field on Interprise                
   -- it "could" be NULL, so in that scenario, we retrieve it from the Company information table                
   SELECT sl2.ShortString, sci.CompanyLanguage AS LanguageCode                
   FROM SystemCompanyInformation sci WITH (NOLOCK)               
   INNER JOIN SystemLanguage sl2 WITH (NOLOCK) ON sl2.LanguageCode = sci.CompanyLanguage                
  ) sci ON 1 = 1                  
  WHERE cc.ContactGUID = @ContactGuid              
 END              
 ELSE              
 BEGIN              
               
               
  -- if not registered, check if the customer has a anon record...              
  IF(EXISTS(SELECT * FROM EcommerceCustomer WITH (NOLOCK) WHERE ContactGUID  = @ContactGuid))              
  BEGIN              
   SELECT wc.CustomerGuid,              
     CAST(wc.CustomerID AS NVARCHAR(30)) AS CustomerCode,  
	 wac.CustomerName,   
	 wac.CustomerTypeCode,            
     wac.CurrencyCode,              
     NULL AS DefaultBillToCode,              
     NULL AS DefaultShipToCode,              
     wac.DefaultPrice,              
     wac.PricingMethod,              
     wac.PricingLevel,              
     wac.PricingPercent,              
     wac.DiscountType,              
     wac.Discount,        
     wac.DiscountBand,            
     wac.Notes,                 
     wacsv.WarehouseCode,              
     wacsv.PaymentTermGroup,              
     CASE               
      WHEN wc.PaymentTermCode IS NOT NULL THEN wc.PaymentTermCode              
      ELSE wacsv.PaymentTermCode END AS PaymentTermCode,              
     wacsv.PaymentTermCode,              
     CASE               
      WHEN sl.ShortString IS NOT NULL THEN sl.ShortString              
      ELSE sci.ShortString END AS LocaleSetting,              
     CASE               
      WHEN sl.LanguageCode IS NOT NULL THEN sl.LanguageCode              
      ELSE sci.LanguageCode END AS LanguageCode,              
     wacc.ContactSalutationCode AS Salutation,              
     wacc.ContactFirstName AS FirstName,              
     wacc.ContactLastName AS LastName,              
     wacc.Email1 AS Email,              
     wacc.ContactCode AS ContactCode,            
	 wacc.JobRoleCode,
	 wacc.StoreMovex_C,
	 wacc.StoreAdmin_C,  
     (ISNULL(wacc.ContactSalutationCode + ' ', '') + ISNULL(wacc.ContactFirstName, '') + ISNULL(' ' + wacc.ContactMiddleName, '') + ISNULL(' ' + wacc.ContactLastName, '') + ISNULL(' ' + wacc.ContactSuffixCode, '')) AS ContactFullName,                  
     NULL AS Phone,              
     NULL AS SubscriptionExpDate,              
     CASE                    
      WHEN wc.PaymentTermCode IS NOT NULL THEN tp2.PaymentMethodCode               
      ELSE tp.PaymentMethodCode END AS PaymentMethod,               
     w.BusinessType,              
     NULL AS TaxNumber,              
     CAST(0 AS BIT) AS IsOkToEmail,             
     CAST(0 AS BIT) AS IsRegistered,              
     CAST(1 AS BIT) AS HasAnonymousRecord,              
     wac.CustomerCode AS AnonymousCustomerCode,              
	 wac.CostCenter_DEV004817,
     wacsv.ShipToCode AS AnonymousShipToCode,              
     wc.CouponCode,              
     wc.IsOver13,              
     wac.IsCreditHold,            
     0 as IsSaveCCDetails,              
     wc.IsUpdated,              
     @CustomerSessionID AS CustomerSessionID,              
     @LastActivity AS LastActivity,              
     wacc.ProductFilterID,              
     wc.ContactGuid,              
     wacc.DefaultBillingCode,              
     wacc.DefaultShippingCode,      
     wac.TrackLoyaltyPoints,       
	 NULL AS Mobile,
	 wacsv.FreightTax AS FreightTaxCode	 
   FROM EcommerceCustomer wc WITH (NOLOCK)               
   INNER JOIN EcommerceSite w WITH (NOLOCK) ON w.WebsiteCode = @WebsiteCode              
   INNER JOIN Customer wac WITH (NOLOCK) ON w.CustomerCode = wac.CustomerCode              
   INNER JOIN CRMContact wacc WITH (NOLOCK) ON wacc.[Type] = 'CustomerContact' AND wacc.EntityCode = wac.CustomerCode AND wacc.ContactCode = wac.DefaultContact              
   INNER JOIN CustomerShipToView wacsv ON wacsv.CustomerCode = wac.CustomerCode AND (wacsv.ShipToCode =  wacc.DefaultShippingCode OR wacsv.ShipToCode =  wac.DefaultShipToCode)      
   LEFT OUTER JOIN SystemPaymentTerm pt WITH (NOLOCK) ON pt.PaymentTermCode = wacsv.PaymentTermCode               
   LEFT OUTER JOIN SystemPaymentType tp WITH (NOLOCK) ON pt.PaymentType = tp.PaymentTypeCode              
   LEFT OUTER JOIN SystemPaymentTerm pt2 WITH (NOLOCK) ON pt2.PaymentTermCode = wc.PaymentTermCode              
   LEFT OUTER JOIN SystemPaymentType tp2 WITH (NOLOCK) ON pt2.PaymentType = tp2.PaymentTypeCode              
   LEFT OUTER JOIN SystemLanguage sl WITH (NOLOCK) ON sl.ShortString = wc.LocaleSetting               
   LEFT OUTER JOIN (               
    -- special case handling since LanguageCode is not required field on Interprise              
    -- it "could" be NULL, so in that scenario, we retrieve it from the Company information table              
    SELECT sl2.ShortString, sci.CompanyLanguage AS LanguageCode              
    FROM SystemCompanyInformation sci WITH (NOLOCK)               
    INNER JOIN SystemLanguage sl2 WITH (NOLOCK) ON sl2.LanguageCode = sci.CompanyLanguage              
   ) sci ON 1 = 1              
   WHERE wc.ContactGuid = @ContactGuid              
                
  END              
  ELSE              
  BEGIN              
   SELECT CAST('00000000-0000-0000-0000-000000000000' AS UNIQUEIDENTIFIER) AS CustomerGuid,              
     NULL AS CustomerCode,      
	 wac.CustomerName,      
     wac.CurrencyCode,       
	 wac.CustomerTypeCode,       
     NULL AS DefaultBillToCode,              
     NULL AS DefaultShipToCode,              
     wac.DefaultPrice,              
     wac.PricingMethod,              
     wac.PricingLevel,              
     wac.PricingPercent,              
     wac.DiscountType,              
     wac.Discount,        
     wac.DiscountBand,            
     wac.Notes,                 
     wacsv.WarehouseCode,              
     wacsv.PaymentTermGroup,              
     wacsv.PaymentTermCode AS PaymentTermCode,              
     wacsv.PaymentTermCode,                   
     CASE               
      WHEN sl.ShortString IS NOT NULL THEN sl.ShortString              
      ELSE sci.ShortString END AS LocaleSetting,              
     CASE    
      WHEN wacc.LanguageCode IS NOT NULL THEN wacc.LanguageCode              
      ELSE sci.LanguageCode END AS LanguageCode,              
     NULL AS Salutation,              
     NULL AS FirstName,              
     NULL AS LastName,              
     NULL AS Email,              
     NULL AS ContactCode,            
	 wacc.JobRoleCode,
	 wacc.StoreMovex_C,
	 wacc.StoreAdmin_C,              
     NULL AS ContactFullName,                          
     NULL AS Phone,              
     NULL AS SubscriptionExpDate,              
     tp.PaymentMethodCode AS PaymentMethod,              
     w.BusinessType,              
     NULL AS TaxNumber,              
     CAST(0 AS BIT) AS IsOkToEmail,              
     CAST(0 AS BIT) AS IsRegistered,              
     CAST(0 AS BIT) AS HasAnonymousRecord, 
	 wac.CostCenter_DEV004817,             
     wac.CustomerCode AS AnonymousCustomerCode,              
     wacsv.ShipToCode AS AnonymousShipToCode,              
     NULL AS CouponCode,              
     0 AS IsOver13,              
     0 AS IsCreditHold,            
     0 AS IsSaveCCDetails,              
     0 as IsUpdated,              
     @CustomerSessionID AS CustomerSessionID,              
     @LastActivity AS LastActivity,              
     wacc.ProductFilterID,              
     wacc.ContactGuid,              
     wacc.DefaultBillingCode,              
     wacc.DefaultShippingCode,      
     wac.TrackLoyaltyPoints,    
     NULL AS Mobile,
	 wacsv.FreightTax AS FreightTaxCode	 
   FROM EcommerceSite w WITH (NOLOCK)               
   INNER JOIN Customer wac WITH (NOLOCK) ON w.CustomerCode = wac.CustomerCode              
   INNER JOIN CustomerShipToView wacsv ON wacsv.CustomerCode = wac.CustomerCode AND wacsv.ShipToCode = wac.DefaultShipToCode              
   INNER JOIN CRMContact wacc WITH (NOLOCK) ON wacc.[Type] = 'CustomerContact' AND wacc.EntityCode = wac.CustomerCode AND wac.DefaultContact = wacc.ContactCode              
   INNER JOIN SystemPaymentTerm pt WITH (NOLOCK) ON pt.PaymentTermCode = wacsv.PaymentTermCode              
   INNER JOIN SystemPaymentType tp WITH (NOLOCK) ON pt.PaymentType = tp.PaymentTypeCode              
   LEFT OUTER JOIN SystemLanguage sl WITH (NOLOCK) ON sl.LanguageCode = wacc.LanguageCode              
   LEFT OUTER JOIN (               
    -- special case handling since LanguageCode is not required field on Interprise              
    -- it "could" be NULL, so in that scenario, we retrieve it from the Company information table              
    SELECT sl2.ShortString, sci.CompanyLanguage AS LanguageCode              
    FROM SystemCompanyInformation sci WITH (NOLOCK)               
    INNER JOIN SystemLanguage sl2 WITH (NOLOCK) ON sl2.LanguageCode = sci.CompanyLanguage              
   ) sci ON 1 = 1              
   WHERE w.WebsiteCode = @WebsiteCode              
                 
  END              
 END              
END         

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[eCommerceCompanySearchInventory]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[eCommerceCompanySearchInventory]
GO

CREATE PROCEDURE [dbo].[eCommerceCompanySearchInventory]
 @SearchTerm   NVARCHAR(3000),                                                                        
 @WebSiteCode   NVARCHAR(30),                                                                        
 @LanguageCode   NVARCHAR(50),                                                                      
 @ItemCode    NVARCHAR(1000) = NULL,                                                                       
 @ItemType    NVARCHAR(50) = NULL,                                                                    
 @UnitMeasure   NVARCHAR(30) = NULL,                                                                    
 @CategoryID   NVARCHAR (100) = NULL,                                                          
 @SectionID    NVARCHAR (100) = NULL,                                                          
 @ManufacturerID  NVARCHAR (100) = NULL,                                      
 @AttributeID      NVARCHAR (100) = NULL,                                      
 @SearchDescriptions    VARCHAR(5),                                                              
 @CurrentDate   DATETIME,                                                        
 @ProductFilterID  NVARCHAR(50),                                    
 @ContactCode NVARCHAR(30),                  
 @CBMode BIT = 0,            
 @PageNum INT = 1,          
 @CurrencyCode NVARCHAR(30) = NULL,        
 @MinPrice NUMERIC(18,6),                            
 @MaxPrice NUMERIC(18,6)        
                                         
AS        
                                                                                                 
BEGIN                                                                        
 SET NOCOUNT ON                
 --SET ANSI_WARNINGS OFF                                                                      
               
 DECLARE @SearchDescriptionString NVARCHAR(MAX)                                      
 DECLARE @JoinUnitMeasureString NVARCHAR(MAX)                                       
 DECLARE @EntityBuilderString NVARCHAR(MAX)                                                          
 DECLARE @ProductFilter NVARCHAR(MAX)                                   
 DECLARE @AdditionalFilter NVARCHAR(MAX)           
 DECLARE @AdditionalField NVARCHAR(1000)      
 DECLARE @SKUString NVARCHAR(MAX) = ' '    
 DECLARE @SKUAdditionalFields NVARCHAR(MAX) = ' '        
 DECLARE @SKUAdditionalJoin NVARCHAR(MAX) = ' '      
                                                                                                               
 SET @AdditionalField = ''                  
 DECLARE @PageSize INT             
 SELECT @PageSize = ConfigValue FROM EcommerceAppConfig WITH (NOLOCK) WHERE Name = 'SearchPageSize' AND WebSiteCode = @WebsiteCode          
     
 IF @PageSize IS NULL OR @PageSize = 0          
 BEGIN                                      
 SET @PageSize = 50           
 END       
        
  -------------UPCCode------------------      
 SELECT ItemCode, UnitMeasureCode, UnitMeasureQty, UPCCode, ShowOnWeb, DefaultSelling INTO #TempInventoryUnitMeasure
 FROM InventoryUnitMeasure WITH (NOLOCK)        
 WHERE ISNULL(UPCCode, '') = @SearchTerm        
     
 IF EXISTS(SELECT 1 FROM #TempInventoryUnitMeasure)     
 BEGIN    
 SET @SKUAdditionalFields = ', TIUM.UPCCode, TIUM.UnitMeasureCode , TIUM.UnitMeasureQty'    
 SET @SKUAdditionalJoin = 'INNER JOIN #TempInventoryUnitMeasure TIUM WITH (NOLOCK) ON TIUM.ItemCode = A.ItemCode AND TIUM.ShowOnWeb = 1 AND TIUM.DefaultSelling = 1'    
 SET @SKUString = ' OR TIUM.UPCCode = ' + QUOTENAME(@SearchTerm, '''')  + ' '     
 END     
                               
 SET @AdditionalFilter=''                                          
 SET @SearchTerm = REPLACE(@SearchTerm,'''','''''')                                                                       
 SET @SearchTerm = REPLACE(@SearchTerm,'+',' ')                     
 SET @SearchTerm = RTRIM(LTRIM(@SearchTerm))             
 SET @ItemCode = NULLIF(@ItemCode, NULL)                                                                     
 SET @EntityBuilderString = ''                                                    
 SET @ProductFilter = ''                                       
                                          
    -- ItemType                                      
 IF (@ItemType = 'ANY')                                                                  
  SET @ItemType = '''%'''                                        
 ELSE                                                               
  SET @ItemType = '''%' + @ItemType + '%'''                                                                     
                                          
    -- UnitMeasure                                      
    IF (@UnitMeasure = 'ANY')                      
  SET @UnitMeasure = '''%'''                                                                        
 ELSE                                                                    
  SET @UnitMeasure = '%' + @UnitMeasure + '%'                                                                      
                                       
 -- SearchDescriptions                                      
 IF (@SearchDescriptions = '0')                                      
  SET @SearchDescriptionString = ' '                                      
 ELSE                                                                    
  SET @SearchDescriptionString = ' OR IIWOD.WebDescription LIKE ' + QUOTENAME(@SearchTerm, '''') + ' '                                      
             
 -- UnitMeasure                                      
 IF (@UnitMeasure = '''%''')                                        
  SET @JoinUnitMeasureString = ' '                                                                     
 ELSE                                           
  SET @JoinUnitMeasureString = ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode                                       
           AND ISNULL(UnitMeasureQty,0)>0                                
           AND IUM.UnitMeasureCode LIKE ' + QUOTENAME(@UnitMeasure, '''') + ' '                                                  
                                       
 -- CategoryID                                      
 IF (@CategoryID <> '0' AND @CategoryID <> '')                     
 BEGIN                                                          
  SET @EntityBuilderString = 'INNER JOIN (SELECT ItemCode_DEV004817 AS ItemCode FROM SGEContactItems_DEV004817 A WITH (NOLOCK)                                       
            INNER JOIN InventoryCategory B WITH (NOLOCK) ON A.ItemCode_DEV004817 = B.ItemCode
			INNER JOIN SystemCategory C ON B.CategoryCode = C.CategoryCode                                       
            WHERE C.Counter = ' + QUOTENAME(@CategoryID, '''') + ' AND A.ContactCode_DEV004817 = ' + QUOTENAME(@ContactCode, '''') + ') IC ON IC.ItemCode = A.ItemCode '
 END                                        
                                        
 -- SectionID                                      
 IF (@SectionID <> '0' AND @SectionID <> '')                                                  
 BEGIN                                        
  SET @EntityBuilderString += ' INNER JOIN (SELECT ItemCode FROM InventoryItemDepartment A WITH (NOLOCK)                                       
             INNER JOIN InventorySellingDepartment B WITH (NOLOCK) ON A.DepartmentCode = B.DepartmentCode                                       
       WHERE B.Counter = ' + QUOTENAME(@SectionID, '''') + ') IIDep ON IIDep.ItemCode = A.ItemCode '                                                          
 END                                      
                                  
 -- ManufcaturerID              
 IF (@ManufacturerID <> '0' AND @ManufacturerID <> '')                                                          
 BEGIN                                             
  SET @EntityBuilderString += ' INNER JOIN (SELECT A.ManufacturerCode, A.ItemCode                                       
             FROM InventoryItem A WITH (NOLOCK)                                       
             INNER JOIN SystemManufacturer B WITH (NOLOCK) ON A.ManufacturerCode = B.ManufacturerCode                                       
             WHERE B.Counter = ' + QUOTENAME(@ManufacturerID, '''') + ') SM ON SM.ItemCode = A.ItemCode '                                                          
 END                                      
                                       
 -- AttributeID                                      
 IF (@AttributeID <> '0' AND @AttributeID <> '')                                                          
 BEGIN                                           
  SET @EntityBuilderString += ' INNER JOIN (SELECT A.AttributeValue, A.ItemCode                                       
             FROM InventoryItemAttributeView A WITH (NOLOCK)                                       
             INNER JOIN SystemItemAttributeSourceFilterValue B WITH (NOLOCK) ON A.AttributeValue = B.SourceFilterName                                       
              AND A.AttributeSourceFilterCode = B.AttributeSourceFilterCode                                       
             WHERE A.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + '                                       
              AND B.Counter = ' + QUOTENAME(@AttributeID, '''') + ') SA ON SA.ItemCode = A.ItemCode '                                                          
 END                                                         
                                                        
    -- ProductFilterID                                      
 IF @ProductFilterID IS NULL                                                    
  SET @ProductFilterID  = ''                                                     
                                                     
 IF @ProductFilterID <> ''                                                      
 BEGIN                                                                
  SET @ProductFilter = ' INNER JOIN (SELECT Distinct(ItemCode),TemplateID                                       
           FROM InventoryProductFilterTemplateItem                                       
    WHERE TemplateID = ' + QUOTENAME(@ProductFilterID, '''') + ') PF ON A.ItemCode = PF.ItemCode                                       
            AND PF.TemplateID = ' + QUOTENAME(@ProductFilterID, '''') + ' '                                                  
 END                       
           
IF @CurrencyCode IS NULL            
BEGIN            
 SELECT @CurrencyCode = CurrencyCode            
 FROM Customer CU WITH (NOLOCK)            
 INNER JOIN CRMContact CO WITH (NOLOCK) ON CO.EntityCode = CU.CustomerCode             
  AND CO.ContactCode = @ContactCode            
END            
                                         
------------------Ranking----------------------------------------              
 CREATE TABLE #TempSearchTerms (SearchTerm NVARCHAR(MAX))                 
 INSERT INTO #TempSearchTerms (SearchTerm) VALUES (@SearchTerm);              
-----------------------------------------------------------------                                        
 DECLARE @SearchTermAdditional NVARCHAR(MAX)= ''                                      
 DECLARE @SearchTermSplit NVARCHAR(MAX)                                     
 DECLARE @splitstring NVARCHAR(3000)                                      
 DECLARE @substring NVARCHAR(3000)                                      
             
 DECLARE @ignoreString NVARCHAR(1000)= ',,'            
 DECLARE @ignoreFilter BIT = 0;            
 SELECT @ignoreString = ',' + RTRIM(LTRIM(REPLACE(ISNULL(ConfigValue,''),'''',''''''))) + ',' FROM EcommerceAppConfig Where Name ='SearchIgnoredWords' AND WebsiteCode =@WebSiteCode            
            
 IF (CHARINDEX(','+@SearchTerm+',',@ignoreString) > 0)              
 BEGIN            
 SELECT *            
    FROM dbo.InventoryItem             
    WHERE 1=0    
                
    RETURN            
 END             
                                                         
 SET @splitstring = @SearchTerm + ' '                                      
 SET @SearchTermSplit  = ''                                      
     
 WHILE (CHARINDEX(' ', @splitstring, 1) > 2)                                      
 BEGIN                        
  SET @substring = SUBSTRING(@splitstring ,1, CHARINDEX(' ',@splitstring ,1)-1)                                       
                                       
 IF @substring <> ' '                                      
  BEGIN                                      
               
   IF LEN(@substring) > 2 AND @substring  <> ',' AND @substring  <> '%'            
   BEGIN            
               
    SET @ignoreFilter = 0            
    IF (@ignoreString <> ',,' AND CHARINDEX(','+@substring+',',@ignoreString) = 0 )            
    BEGIN            
      SET @ignoreFilter = 1            
    END            
                
     IF ( @ignoreFilter = 1 OR @ignoreString = ',,')            
    BEGIN            
     ------------------Ranking----------------------------------------              
     INSERT INTO #TempSearchTerms (SearchTerm) VALUES (@substring);              
     -----------------------------------------------------------------            
                      
     SET @SearchTermSplit += ' (IID.ItemDescription LIKE ' + '''%' + @substring + '%''' + '                                       
      OR REPLACE(IIWOD.SEKeywords,'','', '''') LIKE ' + '''%' + @substring + '%''' + '                                       
      OR A.ItemName LIKE ' + '''%' + @substring + '%''' + ' ' + @SearchDescriptionString  + @SKUString + ')  '                
                 
     SET @SearchTermSplit += 'OR'            
    END            
   END                                                    
   SET @splitstring = SUBSTRING(@splitstring ,LEN(@substring)+2,LEN(@splitstring ))                                      
   SET @splitstring = LTRIM(@splitstring)                                 
                 
  END                                      
 END                                      
             
IF (LEN(@SearchTermSplit) > 1)
BEGIN                                      
 SET @SearchTermAdditional = '(' + SUBSTRING(@SearchTermSplit, 1, LEN(@SearchTermSplit)-2) + ') AND '
END

/*CBN Mode*/                  
 IF @CBMode = 1                  
 BEGIN                  
  SET @AdditionalFilter = @AdditionalFilter + ' AND A.IsCBN = 1 '                  
 END                             
                          
/*Out of Stock Items*/                      
CREATE TABLE #TempSearchResult (ItemCode NVARCHAR(30), ItemType NVARCHAR(30))                      
CREATE TABLE #InventoryFreeStock (WarehouseCode NVARCHAR(30), ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))                       
CREATE TABLE #AvailableItems (ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), UM NVARCHAR(30))                      
-- temporary tables to store available kit components and matrix attributes                
CREATE TABLE #AvailableKitComponents (ItemKitCode NVARCHAR(30), ItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), ItemType NVARCHAR(30), POStatus NVARCHAR(30))         
CREATE TABLE #AvailableMatrixAttributes (ItemCode NVARCHAR(30), MatrixItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))                        
                                            
DECLARE @SQL NVARCHAR(MAX)                                        
DECLARE @AdditionalJoin NVARCHAR(MAX)                                                      
DECLARE @SqlQuery NVARCHAR(MAX)                        
DECLARE @HideOutOfStockProducts BIT                                                                       
DECLARE @ShowInventoryFromAllWarehouses BIT                                                                                                
DECLARE @WarehouseCode NVARCHAR(30)                        
                                                               
SET @AdditionalJoin = ''                                          
SET @SqlQuery = ''                         
SET @HideOutOfStockProducts =  0                                   
SET @ShowInventoryFromAllWarehouses = 0                                                                                                               
                                  
SELECT @HideOutOfStockProducts = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'HideOutOfStockProducts' AND WebSiteCode = @WebSiteCode                
SELECT @ShowInventoryFromAllWarehouses = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'ShowInventoryFromAllWarehouses' AND WebSiteCode = @WebSiteCode                
SET @WarehouseCode = (SELECT TOP 1 WareHouseCode FROM CustomerShipTo WHERE ShipToCode = (SELECT DefaultShippingCode FROM CRMContact WHERE ContactCode = @ContactCode))                      
                      
SET @SQL = ' SELECT A.ItemCode, A.ItemType FROM InventoryItem A WITH (NOLOCK)                                               
     LEFT JOIN (SELECT ItemCode FROM InventoryItemPricingDetail B WITH (NOLOCK)                                               
     INNER JOIN SystemCurrency C WITH (NOLOCK) ON B.CurrencyCode = C.CurrencyCode WHERE IsHomeCurrency = 1) D ON A.ItemCode = D.ItemCode                                               
     LEFT JOIN (SELECT TOP 1 ItemKitCode FROM InventoryKitPricingDetail E WITH (NOLOCK)                                               
     INNER JOIN SystemCurrency F WITH (NOLOCK) ON E.CurrencyCode = F.CurrencyCode WHERE IsHomeCurrency = 1) G ON A.ItemCode = G.ItemKitCode                                               
     INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON IID.ItemCode = A.ItemCode           
      AND IID.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + '                                               
     INNER JOIN InventoryItemWebOption IIWO WITH (NOLOCK) ON IIWO.ItemCode = A.ItemCode                                               
      AND IIWO.WebsiteCode = ' + QUOTENAME(@WebsiteCode, '''') + '                                               
     INNER JOIN InventoryItemWebOptionDescription IIWOD WITH (NOLOCK) ON IIWOD.ItemCode = IIWO.ItemCode                              
      AND IIWOD.WebsiteCode = IIWO.WebsiteCode                                               
      AND IIWOD.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + ' ' + @SKUAdditionalJoin + ' ' +                                           
      @JoinUnitMeasureString + ' ' +                                              
      @EntityBuilderString  + ' ' +                         
      @ProductFilter  + ' ' +                                        
      @AdditionalJoin + '                                           
     WHERE ' + @SearchTermAdditional + '
			ItemType IN (''Non-Stock'',                                               
            ''Service'',                        
            ''Stock'',                 
            ''Matrix Group'',                                              
            ''Kit'',                                              
            ''Service'',                                              
            ''Electronic Download'',                                               
            ''Assembly'',            
            ''Gift Card'',            
            ''Gift Certificate'')                                              
       AND ''' + CAST(@CurrentDate AS NVARCHAR(50)) + ''' BETWEEN ISNULL(IIWO.StartDate, ''1/1/1900'')                                               
       AND ISNULL(IIWO.EndDate, ''12/31/9999'')                            
       AND (A.Status = ''A'' OR (A.Status = ''P'' AND A.ItemType IN (''Kit'', ''Matrix Group'')) OR (A.Status = ''P''                                
       AND A.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'')))                                       
       AND IIWO.Published = 1                                 
       AND IIWO.CheckOutOption = 0                                               
       AND ItemType LIKE ' + @ItemType +                         
        @AdditionalFilter + '                        
       ORDER BY IIWO.IsExclusive DESC, A.ItemName ASC'                      
                         
INSERT INTO #TempSearchResult (ItemCode, ItemType)                      
EXEC (@SQL)                      
/* remove giftcard or giftcertificate items that are not sellable to customer's currency */            
DELETE FROM #TempSearchResult            
WHERE ItemCode IN ( SELECT II.ItemCode            
     FROM InventoryItem II WITH (NOLOCK)            
     INNER JOIN InventoryItemPricingDetail IP WITH (NOLOCK) ON IP.ItemCode = II.ItemCode            
      AND IP.CurrencyCode = @CurrencyCode             
      AND IP.[Enabled] = 0 
      AND II.ItemType IN ('Gift Card','Gift Certificate'))            
-- Get freestock of non-kit/non-matrix group items (Stock, Matrix Item, Assembly)            
IF @HideOutOfStockProducts = 1          
BEGIN          
 INSERT INTO #inventoryfreestock (ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)            
 SELECT EISTV.ItemCode, EISTV.WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM EcommerceInventoryStockTotalView EISTV            
 INNER JOIN #TempSearchResult TSR ON TSR.ItemCode = EISTV.ItemCode          
 LEFT JOIN SupplierPurchaseOrderDetail SPOD ON SPOD.ItemCode = TSR.ItemCode AND spod.WarehouseCode = EISTV.WarehouseCode AND spod.UnitMeasure = EISTV.UnitMeasureCode            
 LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode            
END          
ELSE          
BEGIN          
 INSERT INTO #inventoryfreestock (ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)          
 SELECT EISTV.ItemCode, EISTV.WarehouseCode, FreeStock, UnitMeasureCode, NULL FROM EcommerceInventoryStockTotalView EISTV          
 INNER JOIN #TempSearchResult TSR ON TSR.ItemCode = EISTV.ItemCode          
END

IF @HideOutOfStockProducts = 1            
BEGIN            
 INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                  
 SELECT II.ItemCode, 0, UnitMeasureCode FROM InventoryItem II          
 INNER JOIN #TempSearchResult TSR ON TSR.ItemCode = II.ItemCode                                  
 INNER JOIN InventoryUnitMeasure IUM ON II.ItemCode = IUM.ItemCode                                           
 WHERE II.ItemType IN ( 'Service', 'Electronic Download', 'Non-Stock', 'Gift Card', 'Gift Certificate')                                  
 AND IsBase = 1            
             
 IF ( SELECT COUNT(TSR.ItemCode) FROM InventoryItem II              
	  INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode AND II.ItemType = 'Kit' ) > 0          
 BEGIN        
  -- get the freestock of kit component/s          
  ;WITH cte_AvailableKitComponents           
   AS          
   (          
    SELECT IKD.ItemKitCode, IKD.ItemCode AS ItemCode, 
    CASE
		WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN @WarehouseCode
		ELSE EISTV.WarehouseCode
	END AS WarehouseCode, 
	CASE
		WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN 0
		ELSE EISTV.FreeStock
	END AS FreeStock, IKD.UnitMeasureCode, II.ItemType, OrderStatus,            
    ROW_NUMBER() over(PARTITION BY  IKD.ItemKitCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum          
    FROM InventoryItem II WITH (NOLOCK)
    INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON II.ItemCode = IKD.ItemCode             
    LEFT JOIN EcommerceInventoryStockTotalView EISTV ON IKD.ItemCode = EISTV.ItemCode          
    LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IKD.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode            
    LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode
    WHERE ItemKitCode IN (SELECT TSR.ItemCode          
						FROM InventoryItem II              
						INNER JOIN #TempSearchResult TSR              
						ON II.ItemCode = TSR.ItemCode              
						AND II.ItemType = 'Kit')          
   )          
   INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType, POStatus)          
   SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType, OrderStatus FROM cte_AvailableKitComponents WHERE RowNum = 1          
 END            
            
 IF ( SELECT COUNT(TSR.ItemCode) FROM InventoryItem II              
   INNER JOIN #TempSearchResult TSR              
   ON II.ItemCode = TSR.ItemCode              
   AND II.ItemType = 'Matrix Group' ) > 0            
 BEGIN                 
  -- get the freestock of matrix item attribute/s          
  ;WITH cte_AvailableMatrixAttributes          
   AS          
   (          
    SELECT IMI.ItemCode, IMI.MatrixItemCode, EISTV.WarehouseCode , EISTV.FreeStock, EISTV.UnitMeasureCode, OrderStatus,            
    ROW_NUMBER() over(PARTITION BY  IMI.ItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum          
    FROM InventoryMatrixItem IMI             
    INNER JOIN EcommerceInventoryStockTotalView EISTV ON IMI.MatrixItemCode = EISTV.ItemCode          
    LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IMI.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode            
    LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode            
    WHERE (EISTV.FreeStock > 0 OR OrderStatus IN ('Open', 'Partial'))          
    AND IMI.ItemCode IN (SELECT TSR.ItemCode          
          FROM InventoryItem II              
             INNER JOIN #TempSearchResult TSR              
             ON II.ItemCode = TSR.ItemCode              
             AND II.ItemType = 'Matrix Group')          
   )          
   INSERT INTO #AvailableMatrixAttributes (ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)          
   SELECT ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM cte_AvailableMatrixAttributes WHERE RowNum = 1          
 END          
END          
ELSE            
BEGIN
	IF ( SELECT COUNT(TSR.ItemCode) FROM InventoryItem II              
		 INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode 
		 AND II.ItemType = 'Kit' ) > 0                     
	BEGIN                        
		-- get the freestock of kit component/s             
		;WITH cte_AvailableKitComponents                       
		AS              
		(                      
			SELECT IKD.ItemKitCode, IKD.ItemCode AS ItemCode, 
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN @WarehouseCode
				ELSE EISTV.WarehouseCode
			END AS WarehouseCode,
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN 0
				ELSE EISTV.FreeStock
			END AS FreeStock, IKD.UnitMeasureCode, II.ItemType,                        
			ROW_NUMBER() over(PARTITION BY  IKD.ItemKitCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC) as RowNum                      
			FROM InventoryItem II WITH (NOLOCK)  
			INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON II.ItemCode = IKD.ItemCode                        
			LEFT JOIN EcommerceInventoryStockTotalView EISTV ON IKD.ItemCode = EISTV.ItemCode                                       
			WHERE (II.Status = 'A' OR (II.Status = 'P' AND II.ItemType IN ('Stock', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly', 'Matrix Item')   
			AND EISTV.FreeStock > 0 ))                      
			AND IKD.ItemKitCode IN ( SELECT TSR.ItemCode FROM InventoryItem II              
									 INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode
									 AND II.ItemType = 'Kit')                      
		)              
		INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType)                      
		SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType FROM cte_AvailableKitComponents WHERE RowNum = 1                      
	END      

	IF ( SELECT COUNT(TSR.ItemCode) FROM InventoryItem II              
		 INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode              
		 AND II.ItemType = 'Matrix Group' ) > 0                       
	BEGIN                             
		-- get the freestock of matrix item attribute/s                      
		;WITH cte_AvailableMatrixAttributes                      
		AS                      
		(                      
			SELECT IMI.ItemCode, IMI.MatrixItemCode, EISTV.WarehouseCode , EISTV.FreeStock, EISTV.UnitMeasureCode,                        
			ROW_NUMBER() over(PARTITION BY  IMI.ItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC) as RowNum                      
			FROM InventoryItem II WITH (NOLOCK)  
			INNER JOIN InventoryMatrixItem IMI WITH (NOLOCK) ON II.ItemCode = IMI.MatrixItemCode                         
			INNER JOIN EcommerceInventoryStockTotalView EISTV ON IMI.MatrixItemCode = EISTV.ItemCode                                          
			WHERE (II.Status = 'A' OR (II.Status = 'P' AND II.ItemType = 'Matrix Item' AND EISTV.FreeStock > 0 ))                      
			AND IMI.ItemCode IN ( SELECT TSR.ItemCode FROM InventoryItem II              
								  INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode              
								  AND II.ItemType = 'Matrix Group')                      
		)                      
		INSERT INTO #AvailableMatrixAttributes (ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode)                      
		SELECT ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode FROM cte_AvailableMatrixAttributes WHERE RowNum = 1                      
	END 
	                 
	INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                  
	SELECT II.ItemCode, 0, UnitMeasureCode FROM InventoryItem II          
	INNER JOIN #TempSearchResult TSR ON TSR.ItemCode = II.ItemCode                             
	INNER JOIN InventoryUnitMeasure IUM ON II.ItemCode = IUM.ItemCode                                           
	WHERE II.ItemType IN ('Service', 'Electronic Download', 'Non-Stock', 'Gift Card', 'Gift Certificate')                                  
	AND IsBase = 1            
END         
-- FreeStock from all warehouse            
IF @ShowInventoryFromAllWarehouses = 1            
BEGIN            
	IF @HideOutOfStockProducts = 1            
	BEGIN            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                              
		BEGIN                        
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                           
			BEGIN                        
				;WITH cte_AvailableKitComponents 
				AS
				(
					SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode, ItemType, POStatus,
					ROW_NUMBER() over(PARTITION BY ItemKitCode, UnitMeasureCode ORDER BY ItemKitCode) as RowNum                        
					FROM #AvailableKitComponents                                  
					GROUP BY ItemKitCode, UnitMeasureCode, ItemType, POStatus
				)
				
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)
				SELECT ItemKitCode, FreeStock, UnitMeasureCode
				FROM cte_AvailableKitComponents
				WHERE RowNum = 1 AND ((FreeStock > 0 OR POStatus IN ('Open', 'Partial')) OR ItemType IN ('Non-Stock', 'Electronic Download', 'Service'))
				GROUP BY ItemKitCode, UnitMeasureCode, FreeStock                      
			END                          
		END          
		        
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                
		BEGIN                
			IF ((SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes)) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableMatrixAttributes          
				GROUP BY ItemCode, UnitMeasureCode          
			END            
		END            
		           
		;WITH cte_InventoryFreeStock           
		AS          
		(          
			SELECT ItemCode, FreeStock, WarehouseCode, UnitMeasureCode,          
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum          
			FROM #InventoryFreeStock          
			WHERE FreeStock > 0 OR POStatus IN ('Open', 'Partial')     
		)          
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
		FROM cte_InventoryFreeStock          
		WHERE RowNum = 1          
		GROUP BY ItemCode, UnitMeasureCode          
		        
		IF (@UnitMeasure = '''%''')        
			SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode AND IUM.IsBase = 1                  
			INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '          
		ELSE       
			SET @AdditionalJoin += ' INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '                 
	END            
	ELSE            
	BEGIN 
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                
		BEGIN          
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)            
				SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableKitComponents          
				GROUP BY ItemKitCode, UnitMeasureCode          
			END             
		END          
		        
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                
		BEGIN                
			IF ((SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes)) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableMatrixAttributes          
				GROUP BY ItemCode, UnitMeasureCode         
			END            
		END
		           
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                          
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode FROM #inventoryfreestock            
		GROUP BY ItemCode, UnitMeasureCode       
		    
		IF (@UnitMeasure = '''%''')             
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode AND IUM.IsBase = 1                  
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '        
		ELSE       
		SET @AdditionalJoin += ' INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '             
	END            
END            
-- FreeStock from specific warehouse            
ELSE            
BEGIN                                                     
	IF @HideOutOfStockProducts = 1                                                
	BEGIN            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                              
		BEGIN                              
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                           
			BEGIN                         
				;WITH cte_AvailableKitComponents 
				AS
				(
					SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode, ItemType, POStatus,
					ROW_NUMBER() over(PARTITION BY ItemKitCode, UnitMeasureCode ORDER BY ItemKitCode) as RowNum                        
					FROM #AvailableKitComponents                        
					WHERE WarehouseCode = @WarehouseCode          
					GROUP BY ItemKitCode, UnitMeasureCode, ItemType, POStatus
				)
				
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)
				SELECT ItemKitCode, FreeStock, UnitMeasureCode
				FROM cte_AvailableKitComponents
				WHERE RowNum = 1 AND ((FreeStock > 0 OR POStatus IN ('Open', 'Partial')) OR ItemType IN ('Non-Stock', 'Electronic Download', 'Service'))
				GROUP BY ItemKitCode, UnitMeasureCode, FreeStock                     
			END                          
		END          
	            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                
		BEGIN                
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableMatrixAttributes          
				WHERE WarehouseCode = @WarehouseCode          
				GROUP BY ItemCode, UnitMeasureCode            
			END            
		END          
	        
		;WITH cte_InventoryFreeStock           
		AS          
		(          
			SELECT ItemCode, FreeStock, WarehouseCode, UnitMeasureCode,          
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum          
			FROM #InventoryFreeStock          
			WHERE WarehouseCode = @WarehouseCode AND (FreeStock > 0 OR POStatus IN ('Open', 'Partial'))          
		)          
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
		FROM cte_InventoryFreeStock          
		WHERE RowNum = 1          
		GROUP BY ItemCode, UnitMeasureCode        
		                                 
		IF (@UnitMeasure = '''%''')                            
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode AND IUM.IsBase = 1                           
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '             
		ELSE       
		SET @AdditionalJoin += ' INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '       
	                 
	END          
	ELSE                                  
	BEGIN            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                
		BEGIN                
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0             
			BEGIN            
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)            
				SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableKitComponents          
				WHERE WarehouseCode = @WarehouseCode          
				GROUP BY ItemKitCode, UnitMeasureCode          
			END            
		END          
		            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                
		BEGIN                
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableMatrixAttributes          
				WHERE WarehouseCode = @WarehouseCode          
				GROUP BY ItemCode, UnitMeasureCode            
			END            
		END  
		                                              
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                               
		SELECT ItemCode, FreeStock, UnitMeasureCode FROM #inventoryfreestock                                                
		WHERE WarehouseCode = @WarehouseCode       
		       
		IF (@UnitMeasure = '''%''')
		BEGIN            
			SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode AND IUM.IsBase = 1                                          
			INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '                 
		END
		ELSE
		BEGIN      
			SET @AdditionalJoin += ' INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '                                           
		END
	END                                                   
END                                                               
/*End of Out of Stock Items*/            
         
--------------------------------------ITEM---PRICE----------------------------------------         
 IF @MinPrice IS NULL             
 SET @MinPrice = 0            
             
 IF @MaxPrice IS NULL         
 SET @MaxPrice = 999999999        
                       
 DECLARE @DefaultPrice NVARCHAR(20)           
 SELECT @DefaultPrice = DefaultPrice           
 FROM Customer CU                            
 INNER JOIN CRMContact CC ON CC.EntityCode = CU.CustomerCode           
 AND CC.ContactCode = @ContactCode                                  
                                              
 CREATE TABLE #inventoryitempricingdetail (ItemCode NVARCHAR(30) NOT NULL, WholeSalePrice NUMERIC(18,6), RetailPrice NUMERIC(18,6))                                               
                         
 INSERT INTO #inventoryitempricingdetail                                        
 SELECT IIPD.ItemCode, WholeSalePrice, RetailPrice FROM InventoryItemPricingDetail IIPD WITH (NOLOCK)
 INNER JOIN #TempSearchResult PF ON IIPD.ItemCode = PF.ItemCode   
 INNER JOIN InventoryItem II ON  II.ItemCode = PF.ItemCode         
 WHERE II.ItemType <> 'Kit' AND CurrencyCode = @CurrencyCode                                      
                                         
 --Insert pricing info for kit items in #inventoryitempricingdetail                
 IF (SELECT COUNT(PF.ItemCode) FROM InventoryItem II                
  INNER JOIN #TempSearchResult PF ON II.ItemCode = PF.ItemCode                                   
  WHERE II.ItemType = 'Kit') >  0                
                  
 BEGIN              
  CREATE TABLE #KitItems (ItemCode NVARCHAR(30) NOT NULL, UseCustomerPricing BIT)                
  INSERT INTO #KitItems                  
  SELECT PF.ItemCode, ISNULL(IK.IsGetItemPrice, 0) FROM InventoryItem II WITH (NOLOCK)                                     
  INNER JOIN InventoryItem PF WITH (NOLOCK) ON II.ItemCode = PF.ItemCode                
  INNER JOIN InventoryKit IK WITH (NOLOCK) ON PF.ItemCode = IK.ItemKitCode                                    
  WHERE II.ItemType = 'Kit'              
                  
  INSERT INTO #inventoryitempricingdetail                                                            
  SELECT IKD.ItemKitCode as ItemCode, SUM(IIPD.WholeSalePrice) AS WholeSalePrice, SUM(IIPD.RetailPrice) AS RetailPrice FROM InventoryKitDetail IKD WITH (NOLOCK)                   
  INNER JOIN InventoryItemPricingDetail IIPD WITH (NOLOCK)  ON IKD.ItemCode = IIPD.ItemCode AND IIPD.CurrencyCode = @CurrencyCode               
  INNER JOIN #KitItems KI ON KI.ItemCode = IKD.ItemKitCode                 
  WHERE IIPD.CurrencyCode = @CurrencyCode AND IKD.IsDefault = 1 AND KI.UseCustomerPricing = 1                
  GROUP BY IKD.ItemKitCode                 
  UNION ALL               
  SELECT IKD.ItemKitCode, SUM(TotalRate) AS WholeSalePrice, SUM(TotalRate) AS RetailPrice FROM InventoryKitPricingDetail IKPD WITH (NOLOCK)                
  INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON IKPD.ItemKitCode = IKD.ItemKitCode AND IKPD.ItemCode = IKD.ItemCode                
  INNER JOIN #KitItems KI ON KI.ItemCode = IKD.ItemKitCode                
  WHERE IKPD.CurrencyCode = @CurrencyCode AND IKD.IsDefault = 1 AND KI.UseCustomerPricing = 0                
  GROUP BY IKD.ItemKitCode                                                                                                      
 END                   
                                                  
 IF @DefaultPrice = 'Retail'                            
 BEGIN                            
  SET @AdditionalFilter = @AdditionalFilter + ' AND ISNULL(RetailPrice,0) >= ' + QUOTENAME(@MinPrice,'''') + ' AND ISNULL(RetailPrice,0) <= ' + QUOTENAME(@MaxPrice,'''') + ' '                           
 END                            
 ELSE                             
 BEGIN                             
  SET @AdditionalFilter = @AdditionalFilter + ' AND ISNULL(WholesalePrice,0) >= ' + QUOTENAME(@MinPrice,'''') + ' AND ISNULL(WholesalePrice,0) <= ' + QUOTENAME(@MaxPrice,'''') + ' '                            
 END             
             
 SET @AdditionalField = @AdditionalField + ' , IIPD.WholeSalePrice AS WholeSalePrice, IIPD.RetailPrice AS RetailPrice '             
 SET @AdditionalJoin = @AdditionalJoin + ' LEFT JOIN #inventoryitempricingdetail IIPD WITH (NOLOCK) ON IIPD.ItemCode = A.ItemCode '                                                                        
                                                               
--------------------------------------Ranking----------------------------------------              
CREATE TABLE #TempSearchResultRanking ( ItemCode NVARCHAR(30)              
            ,KeyWords NVARCHAR(MAX)              
            ,Ranking BIGINT            
            ,CategoryID INT            
            ,CategoryDisplayName NVARCHAR(100)            
            ,ManufacturerID INT            
            ,ManufacturerDisplayName NVARCHAR(100)            
            )                  
SET @SQL  = '               
SELECT A.ItemCode, (ISNULL(IID.ItemDescription,'''') +'' ''+ ISNULL(IIWOD.SEKeywords,'''') +'' ''+ ISNULL(A.ItemName,'''')) AS KeyWords, 0 AS Ranking            
,NULL AS CategoryID ,NULL AS CategoryDisplayName ,NULL AS ManufacturerID ,NULL AS ManufacturerDisplayName            
FROM InventoryItem A WITH (NOLOCK)                                       
 LEFT JOIN (SELECT ItemCode FROM InventoryItemPricingDetail B WITH (NOLOCK)                       
     INNER JOIN SystemCurrency C WITH (NOLOCK) ON B.CurrencyCode = C.CurrencyCode WHERE IsHomeCurrency = 1) D ON A.ItemCode = D.ItemCode                                       
  LEFT JOIN (SELECT TOP 1 ItemKitCode FROM InventoryKitPricingDetail E WITH (NOLOCK)                                       
     INNER JOIN SystemCurrency F WITH (NOLOCK) ON E.CurrencyCode = F.CurrencyCode WHERE IsHomeCurrency = 1) G ON A.ItemCode = G.ItemKitCode                                       
     INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON IID.ItemCode = A.ItemCode                                       
      AND IID.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + '                                       
     INNER JOIN InventoryItemWebOption IIWO WITH (NOLOCK) ON IIWO.ItemCode = A.ItemCode                                       
      AND IIWO.WebsiteCode = ' + QUOTENAME(@WebsiteCode, '''') + '                                       
     INNER JOIN InventoryItemWebOptionDescription IIWOD WITH (NOLOCK) ON IIWOD.ItemCode = IIWO.ItemCode                                   
      AND IIWOD.WebsiteCode = IIWO.WebsiteCode                                       
      AND IIWOD.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + ' '  + @SKUAdditionalJoin +                                 
      @JoinUnitMeasureString + ' ' +                                      
      @EntityBuilderString  + ' ' +                                      
      @ProductFilter  + ' ' +                                
      @AdditionalJoin + '              
      WHERE ' + @SearchTermAdditional + '
			ItemType IN (''Non-Stock'',                                       
            ''Service'',                                       
            ''Stock'',                                      
          ''Matrix Group'',                             
            ''Kit'',                                      
            ''Service'',                                      
            ''Electronic Download'',                                       
            ''Assembly'',            
            ''Gift Card'',            
            ''Gift Certificate'')                                      
       AND ''' + CAST(@CurrentDate AS NVARCHAR(50)) + ''' BETWEEN ISNULL(IIWO.StartDate, ''1/1/1900'')                                       
       AND ISNULL(IIWO.EndDate, ''12/31/9999'')                    
       AND (A.Status = ''A'' OR (A.Status = ''P'' AND A.ItemType IN (''Kit'', ''Matrix Group'')) OR (A.Status = ''P''                        
       AND A.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'') AND AI.FreeStock > 0))                               
       AND IIWO.Published = 1                                       
       AND IIWO.CheckOutOption = 0                                       
       AND ItemType LIKE ' + @ItemType +                 
        @AdditionalFilter +               
       'GROUP BY A.ItemCode,IID.ItemDescription,IIWOD.SEKeywords,A.ItemName'                    
               
 INSERT INTO #TempSearchResultRanking (ItemCode, KeyWords,Ranking,CategoryID,CategoryDisplayName,ManufacturerID,ManufacturerDisplayName)                      
 EXEC (@SQL)              
               
 -------------Word Match------------------              
 DECLARE @word AS NVARCHAR(MAX)              
 WHILE EXISTS(SELECT 1 FROM #TempSearchTerms)              
 BEGIN              
 SELECT TOP 1 @word = SearchTerm FROM #TempSearchTerms              
               
 -- in word              
 EXEC('UPDATE #TempSearchResultRanking               
    SET Ranking = Ranking + 1 + (SELECT COUNT(1) FROM #TempSearchTerms)               
    WHERE REPLACE(Keywords,'','', '''') Like' + '''%' + @word + '%''')              
               
 --exact word                 
 EXEC('UPDATE #TempSearchResultRanking               
    SET Ranking = Ranking + 1 + (SELECT COUNT(1) FROM #TempSearchTerms)               
    WHERE REPLACE(Keywords,'','', '''') Like' + '''% ' + @word + ' %''')              
 DELETE FROM #TempSearchTerms WHERE SearchTerm = @word              
 END              
             
 --Update CategoryCode            
 UPDATE SRR            
 SET    SRR.CategoryID = IC.[Counter],            
  SRR.CategoryDisplayName = IC.[Description]            
 FROM   #TempSearchResultRanking SRR            
 LEFT JOIN (            
  SELECT A.ItemCode, C.[Counter],D.[Description] FROM InventoryCategory A WITH (NOLOCK)            
  INNER JOIN SystemCategory C WITH (NOLOCK) ON A.CategoryCode = C.CategoryCode            
  INNER JOIN SystemCategoryWebOption B WITH (NOLOCK) ON C.CategoryCode = B.CategoryCode            
  INNER JOIN SystemCategoryDescription D WITH (NOLOCK) ON B.CategoryCode = D.CategoryCode            
  WHERE A.IsPrimary =1 AND Published = 1 AND C.IsActive = 1 AND D.LanguageCode = @LanguageCode AND WebSiteCode = @WebSiteCode            
     ) IC ON IC.ItemCode = SRR.ItemCode              
             
UPDATE SRR            
SET    SRR.ManufacturerID = IC.[Counter],            
  SRR.ManufacturerDisplayName = IC.[Description]            
FROM   #TempSearchResultRanking SRR            
LEFT JOIN (            
  SELECT A.ItemCode, C.[Counter],D.[Description] FROM InventoryItem A WITH (NOLOCK)            
  INNER JOIN SystemManufacturer C WITH (NOLOCK) ON A.ManufacturerCode = C.ManufacturerCode            
  INNER JOIN SystemManufacturerWebOption B WITH (NOLOCK) ON C.ManufacturerCode = B.ManufacturerCode            
  INNER JOIN SystemManufacturerDescription D WITH (NOLOCK) ON B.ManufacturerCode = D.ManufacturerCode            
  WHERE Published = 1 AND C.IsActive = 1 AND D.LanguageCode = @LanguageCode AND WebSiteCode = @WebSiteCode            
     ) IC ON IC.ItemCode = SRR.ItemCode              
            
-----------------------------------------------              
---------------------------------------------------------------------------------------------------------------------------------------              
               
 SET @SQL  = ' SELECT * INTO #TempSearchResultTable FROM (              
 SELECT IID.ItemDescription, IIWOD.WebDescription,A.* ,SRR.CategoryID ,SRR.CategoryDisplayName ,SRR.ManufacturerID ,SRR.ManufacturerDisplayName,SRR.Ranking,IIWO.IsExclusive,              
  ShowBuyButton, ROW_NUMBER() OVER (ORDER BY A.ItemName) AS RowNumber ' + @SKUAdditionalFields + @AdditionalField + ', ISNULL(IsShipped, 1) AS IsShipped, CONVERT(VARCHAR, ShippingDate, 103) AS ShippingDate, ISNULL(IsVoided, 0) AS IsVoided,' +         
  'SalesOrderCode FROM InventoryItem A WITH (NOLOCK)                    
     LEFT JOIN (SELECT ItemCode FROM InventoryItemPricingDetail B WITH (NOLOCK)                                    
     INNER JOIN SystemCurrency C WITH (NOLOCK) ON B.CurrencyCode = C.CurrencyCode WHERE IsHomeCurrency = 1) D ON A.ItemCode = D.ItemCode                                       
  LEFT JOIN (SELECT TOP 1 ItemKitCode FROM InventoryKitPricingDetail E WITH (NOLOCK)                                       
     INNER JOIN SystemCurrency F WITH (NOLOCK) ON E.CurrencyCode = F.CurrencyCode WHERE IsHomeCurrency = 1) G ON A.ItemCode = G.ItemKitCode                                       
     INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON IID.ItemCode = A.ItemCode                                       
      AND IID.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + '                                       
     INNER JOIN InventoryItemWebOption IIWO WITH (NOLOCK) ON IIWO.ItemCode = A.ItemCode                                       
      AND IIWO.WebsiteCode = ' + QUOTENAME(@WebsiteCode, '''') + '        
	  INNER JOIN SGEContactItems_DEV004817 SGE ON A.ItemCode = SGE.ItemCode_DEV004817
     INNER JOIN InventoryItemWebOptionDescription IIWOD WITH (NOLOCK) ON IIWOD.ItemCode = IIWO.ItemCode            
      AND IIWOD.WebsiteCode = IIWO.WebsiteCode                                       
      AND IIWOD.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + ' ' +
      @JoinUnitMeasureString + ' ' +                                      
      @EntityBuilderString  + ' ' +                                      
      @ProductFilter  + ' ' +         
      @AdditionalJoin + @SKUAdditionalJoin +  '              
      INNER JOIN #TempSearchResultRanking SRR WITH (NOLOCK) ON SRR.ItemCode = A.ItemCode  
	  LEFT JOIN (SELECT CSO.SalesOrderCode, CASE WHEN OrderStatus = ''Completed'' THEN 1 ELSE 0 END AS IsShipped, ShippingDate, ItemCode, ISNULL(IsVoided, 0) AS IsVoided, RowNum = ROW_NUMBER() OVER (PARTITION BY ItemCode ORDER BY ShippingDate DESC) FROM CustomerSalesOrder CSO WITH (NOLOCK) 
	  INNER JOIN CustomerSalesOrderDetail CSOD WITH (NOLOCK) ON CSO.SalesOrderCode = CSOD.SalesOrderCode
	  WHERE CSO.ContactCode = ' + QUOTENAME(@ContactCode , '''') + ') AS CSO ON CSO.ItemCode = A.ItemCode AND RowNum = 1 
     WHERE ' + @SearchTermAdditional + ' 
			ItemType IN (''Non-Stock'',                                       
            ''Service'',                                       
            ''Stock'',                                      
            ''Matrix Group'',                                      
            ''Kit'',                                      
            ''Service'',                                      
            ''Electronic Download'',                                       
            ''Assembly'',            
            ''Gift Card'',            
            ''Gift Certificate'')                                      
       AND ''' + CAST(@CurrentDate AS NVARCHAR(50)) + ''' BETWEEN ISNULL(IIWO.StartDate, ''1/1/1900'')                                       
       AND ISNULL(IIWO.EndDate, ''12/31/9999'')                    
       AND (A.Status = ''A'' OR (A.Status = ''P'' AND A.ItemType IN (''Kit'', ''Matrix Group'')) OR (A.Status = ''P''                        
       AND A.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'') AND AI.FreeStock > 0))                               
       AND IIWO.Published = 1                                       
       AND IIWO.CheckOutOption = 0  
	   AND ISNULL(IIWO.WebsiteGroup_C, '''') = ''Harvey Norman''
	   AND ISNULL(IIWO.NonStockMatrixType_C, '''') != ''Child''  
	   AND SGE.ContactCode_DEV004817 = ' + QUOTENAME(@ContactCode , '''') + ' ' + '
       AND ItemType LIKE ' + @ItemType +                 
        @AdditionalFilter + '                
       ) SortedResult;                 
	   
	   DELETE #TempSearchResultTable WHERE CategoryID IS NULL
       SELECT * FROM #TempSearchResultTable WHERE RowNumber BETWEEN ((' + CAST(@PageNum AS VARCHAR(10)) + ' - 1) * ' + CAST(@PageSize AS VARCHAR(20)) + ' + 1) AND ('           
       + CAST(@PageNum AS VARCHAR(20)) + ' * ' + CAST(@PageSize AS VARCHAR(20)) + ')              
       ORDER BY ItemName;                 
                   
       DECLARE @RCount INT              
       SELECT @RCount = COUNT(*) FROM #TempSearchResultTable                     
       SELECT CAST(CEILING(@RCount*1.0/'++ CAST(@PageSize AS VARCHAR(20))+') AS INT) pages, @RCount ProductCount'
       
EXEC(@SQL)          
          
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#InventoryFreeStock'))          
 DROP TABLE #InventoryFreeStock          
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableItems'))          
 DROP TABLE #AvailableItems          
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                                                                            
 DROP TABLE #AvailableKitComponents          
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                                                                            
 DROP TABLE #AvailableMatrixAttributes     
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#TempInventoryUnitMeasure'))                                                                                    
 DROP TABLE #TempInventoryUnitMeasure          
           
 SET NOCOUNT OFF                                                                  
END     

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[eCommerceEntityMgr]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[eCommerceEntityMgr]
GO

CREATE PROCEDURE [dbo].[eCommerceEntityMgr] 
@EntityName  VARCHAR(100),                                              
@PublishedOnly TINYINT,                                              
@LocaleName  NVARCHAR(100),                                              
@WebSiteCode NVARCHAR(30),                                    
@CurrentDate DateTime,
@CustomerCode NVARCHAR(60) NULL,
@WebsiteGroup NVARCHAR(100)

AS                                              
SET NOCOUNT ON                                              
                                              
DECLARE @LanguageCode nvarchar (50)                                              
SELECT @LanguageCode = LanguageCode FROM SystemLanguage WITH (NOLOCK) WHERE ShortString = @LocaleName                                              
                                              
CREATE TABLE #tmp                                              
(                                              
 EntityID   INT,                                              
 [Name]    NVARCHAR (60),                                              
 ColWidth   INT,                                              
 [Description]  NVARCHAR (200),                                              
 WebDescription  NTEXT,
 Summary  NTEXT,
 SEName    NTEXT,                                              
 SEKeywords   NTEXT,                                              
 SEDescription  NTEXT,                                              
 SETitle   NTEXT,                                              
 SENoScript   NTEXT,                                              
 SEAltText   NTEXT,                                                   
 ParentCategoryID NVARCHAR (60),                                                    
 ParentEntityID  INT,                                                    
 DisplayOrder  INT,                                                    
 SortByLooks  TINYINT,                                                  
 XmlPackage   NVARCHAR (500),                                                    
 MobileXmlPackage   NVARCHAR (500),                
 Published   TINYINT,                                                    
 ContentsBGColor INT,                                                    
 PageBGColor  INT,                                                    
 GraphicsColor  INT,                                                    
 NumProducts  NVARCHAR (60),                                                    
 NumObjects   INT,                                                     
 PageSize   INT,                                                     
 QuantityDiscountID INT,                                      
 TemplateName  NVARCHAR(200),                                
 SortOrder int,               
 VirtualType NVARCHAR(100),              
 VirtualPageOption NVARCHAR(100),              
 VirtualPageValueEntity NVARCHAR(100),              
 VirtualPageValueTopic NVARCHAR(100),              
 VirtualPageValueExternalPage NVARCHAR(100),              
 OpenInNewTab BIT               
)                                                     
                                                  
IF @EntityName = 'Category'                                                     
BEGIN                                                     
 INSERT INTO #tmp(EntityID,                                                  
  [Name],                                                  
  ColWidth,                                                  
  [Description],                                                
  WebDescription,   
  Summary,
  SEName,                                                  
  SEKeywords,                                                  
  SEDescription,                  
  SETitle,                                                    
  SENoScript,                 
  SEAltText,                                                    
  ParentCategoryID,                                    
  ParentEntityID,                                                    
  DisplayOrder,                                        
  SortByLooks,                                                  
  XmlPackage,                     
  MobileXmlPackage,                                               
  Published,                                                    
  ContentsBGColor,                                          
  PageBGColor,                                                    
  GraphicsColor,                  
  NumProducts,                                                    
  NumObjects,                                                     
  PageSize,                                    
  QuantityDiscountID,                                      
  TemplateName,                                
  SortOrder,               
  VirtualType,              
  VirtualPageOption,              
  VirtualPageValueEntity,              
  VirtualPageValueTopic,              
  VirtualPageValueExternalPage,              
  OpenInNewTab)                                                  
  SELECT A.Counter,                                                    
  RTRIM(A.CategoryCode),                                                   
  C.DisplayColumns,                                                     
  B.[Description],                                                
  D.WebDescription,   
  D.Summary,
  REPLACE(C.CategoryCode, ' ', '-'),                                                    
  D.SEKeywords,                        
  D.SEDescription,                                                    
  D.SETitle,                                                    
  D.SENoScript,                                                    
  D.SEAltText,                                                    
  A.ParentCategory,                                                    
  0,                                                    
  0 AS DisplayOrder,                                              
  C.OrderByLooks,                                                    
  C.XmlPackage,                                                    
  C.MobileXmlPackage,                 
  C.Published,                                        
  0,                                                    
  0,                                                    
  0,                                                    
  ISNULL(NumProducts, 0),                                                    
  ISNULL(NumProducts, 0),                                              
  C.PageSize,                                             
  0,                                      
  C.TemplateName,                                
  A.SortOrder,               
  ISNULL(A.VirtualType, 'Normal'),              
  ISNULL(C.VirtualPageOption, 'Show Category'),              
  C.VirtualPageValueEntity,              
  C.VirtualPageValueTopic,              
  C.VirtualPageValueExternalPage,              
  CASE  
  WHEN A.VirtualType = 'Web Menu' AND C.VirtualPageOption = 'Show external page' AND C.OpenInNewTab = 1 THEN 1  
  ELSE 0  
  END AS OpenInNewTab                                                     
  FROM SystemCategory A WITH (NOLOCK)                                         
  INNER JOIN SystemCategoryDescription B WITH (NOLOCK) ON A.CategoryCode = B.CategoryCode                                                    
  INNER JOIN SystemCategoryWebOption C WITH (NOLOCK) ON A.CategoryCode = C.CategoryCode                                                    
  INNER JOIN SystemCategoryWebOptionDescription D WITH (NOLOCK) ON A.CategoryCode = D.CategoryCode                                                   
  LEFT JOIN (                            
   SELECT A.CategoryCode, COUNT(A.ItemCode) AS NumProducts                             
   FROM InventoryCategory A WITH (NOLOCK)                                     
   JOIN InventoryItem B WITH (NOLOCK) ON A.ItemCode = B.ItemCode AND B.Status = 'A'                                            
   INNER JOIN InventoryItemWebOption C WITH (NOLOCK) ON C.ItemCode = B.ItemCode                                            
   WHERE C.WebSiteCode = @WebSiteCode AND                             
     C.Published >= @PublishedOnly AND                             
     B.ItemType IN ('Stock', 'Kit', 'Matrix Group', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly')                                          
   GROUP BY A.CategoryCode                  
  ) E ON A.CategoryCode = E.CategoryCode                            
  WHERE D.LanguageCode = @LanguageCode AND                             
    B.LanguageCode = @LanguageCode AND                             
    C.Published >= @PublishedOnly AND                       
    IsActive = 1 AND                             
    C.WebSiteCode = @WebSiteCode AND                             
    D.WebSiteCode = @WebSiteCode AND                             
    (                            
     (C.StartDate IS NULL AND C.EndDate IS NULL) OR                    
     (C.StartDate IS NOT NULL AND C.StartDate <= @CurrentDate AND C.EndDate IS NULL) OR                              
     (C.StartDate IS NULL AND C.EndDate IS NOT NULL AND C.EndDate >= @CurrentDate) OR                              
     (@CurrentDate BETWEEN C.StartDate AND C.EndDate)                            
    )                                 
    AND (ISNULL(C.WebsiteGroup_C, '') = CASE WHEN @WebsiteGroup = 'Company' THEN 'Company Portal' ELSE '' END OR ISNULL(C.WebsiteGroup_C, '') = CASE WHEN @WebsiteGroup = 'Company' THEN 'Company Portal' ELSE 'Smartbag' END)
  ORDER BY C.OrderByLooks DESC, A.ParentCategory, DisplayOrder, A.CategoryCode                                                  
  UPDATE #tmp SET ParentEntityID = A.Counter FROM (SELECT Counter, CategoryCode FROM SystemCategory WITH (NOLOCK)) A                                                   
  WHERE #tmp.Name <> #tmp.ParentCategoryID AND A.CategoryCode = #tmp.ParentCategoryID                                                  
 END                                        
                                    
                                             
IF @EntityName = 'Section' OR @EntityName = 'Department'                                                 
BEGIN                                                    
 INSERT INTO #tmp(EntityID,                                                  
  [Name],                                                  
  ColWidth,                                                  
  [Description],                                                
  WebDescription,
  Summary,                                                  
  SEName,                                                  
  SEKeywords,                                   
  SEDescription,                                                  
  SETitle,                                                    
  SENoScript,                                                    
  SEAltText,                                                    
  ParentCategoryID,                                                    
  ParentEntityID,                                                    
  DisplayOrder,                                                    
  SortByLooks,                                                  
  XmlPackage,                                                    
  MobileXmlPackage,                
  Published,                                                    
  ContentsBGColor,                                                    
  PageBGColor,                                                    
  GraphicsColor,                                                    
  NumProducts,                                            
  NumObjects,                                                     
 PageSize,                                                     
  QuantityDiscountID,                                      
  TemplateName,                                
  SortOrder,            
  VirtualType,      
  VirtualPageOption,              
  VirtualPageValueEntity,              
  VirtualPageValueTopic,              
  VirtualPageValueExternalPage,              
  OpenInNewTab)                                                  
 SELECT  A.Counter,                                                   
  RTRIM(C.DepartmentCode),                                                   
  C.DisplayColumns,                                                   
  B.[Description],                          
  D.WebDescription,    
  D.Summary,                                                
  REPLACE(D.DepartmentCode, ' ', '-'),                                              
  D.SEKeywords,                                                    
  D.SEDescription,                                                    
  D.SETitle,        
  D.SENoScript,                                                    
  D.SEAltText,                                                    
  A.ParentDepartment,  0,                                                    
  0 AS DisplayOrder,                                                    
  C.OrderByLooks,                             
  C.XmlPackage,                      
  C.MobileXmlPackage,                
  C.Published,                                                    
  0,                                                    
  0,                                                    
  0,                                                    
  ISNULL(NumProducts, 0),                                                    
  ISNULL(NumProducts, 0),                                              
  C.PageSize,               
  0,                                      
  C.TemplateName,                                
  A.SortOrder,            
  ISNULL(A.VirtualType, 'Normal'),              
  ISNULL(C.VirtualPageOption,'Show Department'),              
  C.VirtualPageValueEntity,              
  C.VirtualPageValueTopic,              
  C.VirtualPageValueExternalPage,              
  CASE  
  WHEN A.VirtualType = 'Web Menu' AND C.VirtualPageOption = 'Show external page' AND C.OpenInNewTab = 1 THEN 1  
  ELSE 0  
  END AS OpenInNewTab                                                                 
 FROM InventorySellingDepartment A WITH (NOLOCK)                             
 INNER JOIN InventorySellingDepartmentDescription B WITH (NOLOCK) ON A.DepartmentCode = B.DepartmentCode                                                   
 INNER JOIN InventorySellingDepartmentWebOption C WITH (NOLOCK) ON A.DepartmentCode = C.DepartmentCode                                                   
 INNER JOIN InventorySellingDepartmentWebOptionDescription D WITH (NOLOCK) ON A.DepartmentCode = D.DepartmentCode                                                   
 LEFT JOIN (                            
  SELECT A.DepartmentCode, COUNT(A.ItemCode) AS NumProducts                             
  FROM InventoryItemDepartment A WITH (NOLOCK)                                                      
  JOIN InventoryItem B WITH (NOLOCK) ON A.ItemCode = B.ItemCode AND B.Status = 'A'                                            
  INNER JOIN InventoryItemWebOption C WITH (NOLOCK) ON C.ItemCode = B.ItemCode                                            
  WHERE C.WebSiteCode = @WebSiteCode AND                             
    C.Published >= @PublishedOnly AND                             
    B.ItemType IN ('Stock', 'Kit', 'Matrix Group', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly')                                            
  GROUP BY A.DepartmentCode                            
 ) E ON A.DepartmentCode = E.DepartmentCode                                            
 WHERE B.LanguageCode = @LanguageCode AND                             
   D.LanguageCode = @LanguageCode AND                             
   C.Published >= @PublishedOnly AND                             
   IsActive = 1 AND                             
   C.WebSiteCode = @WebSiteCode AND                             
   D.WebSiteCode = @WebSiteCode AND               
   (                            
    (C.StartDate IS NULL AND C.EndDate IS NULL) OR                              
    (C.StartDate IS NOT NULL AND C.StartDate <= @CurrentDate AND C.EndDate IS NULL) OR                            
    (C.StartDate IS NULL AND C.EndDate IS NOT NULL AND C.EndDate >= @CurrentDate) OR                              
    (@CurrentDate BETWEEN C.StartDate AND C.EndDate)                            
   )                            
 ORDER BY  C.OrderByLooks DESC, A.ParentDepartment, DisplayOrder, A.DepartmentCode                            
 UPDATE #tmp SET ParentEntityID = A.Counter FROM (                            
  SELECT Counter, DepartmentCode FROM InventorySellingDepartment WITH (NOLOCK)                             
 ) A                                                   
WHERE #tmp.Name <> #tmp.ParentCategoryID AND A.DepartmentCode = #tmp.ParentCategoryID                                                  
END                                                  
                                              
IF @EntityName = 'Manufacturer'                                                     
BEGIN                                           
 INSERT INTO #tmp(EntityID,                                                  
  Name,                                                  
  ColWidth,                     
  Description,                                                
  WebDescription,  
  Summary,                                                
  SEName,                                                  
  SEKeywords,                                                  
  SEDescription,             
  SETitle,                                                    
  SENoScript,                                                    
  SEAltText,                                                    
  ParentCategoryID,                                                    
  ParentEntityID,                                     
  DisplayOrder,                                                    
  SortByLooks,                                                  
  XmlPackage,                       
  MobileXmlPackage,                              
  Published,                                                    
  ContentsBGColor,                                                    
  PageBGColor,                                                    
  GraphicsColor,                                                    
  NumProducts,                                                    
  NumObjects,                                                     
  PageSize,                                             
  QuantityDiscountID,                                      
  TemplateName,             
  VirtualType,              
  VirtualPageOption,              
  VirtualPageValueEntity,              
  VirtualPageValueTopic,              
  VirtualPageValueExternalPage,              
  OpenInNewTab,
  SortOrder)                                                  
 SELECT A.Counter,                                                   
  RTRIM(A.ManufacturerCode),                                                   
  C.DisplayColumns,                                                   
  B.Description,                                                  
  D.WebDescription, 
  D.Summary,                                                
  REPLACE(D.ManufacturerCode, ' ', '-'),                                                    
  D.SEKeywords,                      
  D.SEDescription,                                                    
  D.SETitle,              
  D.SENoScript,                                                    
  D.SEAltText,                                                    
  C.ManufacturerCode,                                                    
  0,                                                    
  0 AS DisplayOrder,                                                    
  C.OrderByLooks,                                                    
C.XmlPackage,                                                    
  C.MobileXmlPackage,                                                 
 C.Published,                                                    
  0,                                                    
  0,                                                    
  0,                                                    
  ISNULL(NumProducts, 0),                                               
  ISNULL(NumProducts, 0),                                  
  C.PageSize,                                                     
  0,                                      
  C.TemplateName,             
  ISNULL(A.VirtualType, 'Normal'),              
  ISNULL(C.VirtualPageOption, 'Show Manufacturer'),              
  C.VirtualPageValueEntity,              
  C.VirtualPageValueTopic,              
  C.VirtualPageValueExternalPage,              
  CASE  
  WHEN A.VirtualType = 'Web Menu' AND C.VirtualPageOption = 'Show external page' AND C.OpenInNewTab = 1 THEN 1  
  ELSE 0  
  END AS OpenInNewTab,
  ROW_NUMBER() OVER (ORDER BY B.Description ASC)                                                          
 FROM SystemManufacturer A WITH (NOLOCK)                                  
 INNER JOIN SystemManufacturerDescription B WITH (NOLOCK) ON A.ManufacturerCode = B.ManufacturerCode                                                   
 INNER JOIN SystemManufacturerWebOption C WITH (NOLOCK) ON A.ManufacturerCode = C.ManufacturerCode                                                   
 INNER JOIN SystemManufacturerWebOptionDescription D WITH (NOLOCK) ON A.ManufacturerCode = D.ManufacturerCode                                                   
 LEFT JOIN (                            
  SELECT A.ManufacturerCode, COUNT(B.ItemCode) AS NumProducts, A.WebSiteCode                                             
  FROM SystemManufacturerWebOption A WITH (NOLOCK)                                                      
  JOIN InventoryItem B WITH (NOLOCK) ON A.ManufacturerCode = B.ManufacturerCode AND B.Status = 'A'                                            
  INNER JOIN InventoryItemWebOption C WITH (NOLOCK) ON C.ItemCode = B.ItemCode AND A.WebSiteCode = C.WebSiteCode                                            
  WHERE C.WebSiteCode = @WebSiteCode AND C.Published >= @PublishedOnly                                            
  AND B.ItemType IN ('Stock', 'Kit', 'Matrix Group', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly')                                            
  GROUP BY A.ManufacturerCode,A.WebSiteCode                            
 ) E ON A.ManufacturerCode = E.ManufacturerCode                                                   
 WHERE B.LanguageCode = @LanguageCode AND                             
   D.LanguageCode = @LanguageCode AND                      
   C.Published >= @PublishedOnly AND                             
   IsActive = 1 AND                             
   C.WebSiteCode = @WebSiteCode AND                             
   D.WebSiteCode = @WebSiteCode AND                             
   (                            
    (C.StartDate IS NULL AND C.EndDate IS NULL) OR                              
    (C.StartDate IS NOT NULL AND C.StartDate <= @CurrentDate AND C.EndDate IS NULL) OR                              
    (C.StartDate IS NULL AND C.EndDate IS NOT NULL AND C.EndDate >= @CurrentDate) OR                              
    (@CurrentDate BETWEEN C.StartDate AND C.EndDate)                            
   )                
 ORDER BY  C.OrderByLooks DESC, DisplayOrder, A.ManufacturerCode                                                    
END                            
                        
IF @EntityName = 'Attribute'                        
BEGIN                        
 INSERT INTO #tmp(EntityID,                                                  
  [Name],                                                  
  ColWidth,                                                  
  [Description],                                                
  WebDescription, 
  Summary,                        
  SEName,                         
  SEKeywords,                                                  
  SEDescription,                                                  
  SETitle,                                                    
  SENoScript,                                                    
  SEAltText,                                                    
  ParentCategoryID,                                                    
  ParentEntityID,                                                    
  DisplayOrder,                                                    
  SortByLooks,                                                  
  XmlPackage,                                                    
  Published,                                                    
  ContentsBGColor,                                                    
  PageBGColor,                                 
  GraphicsColor,                                                    
  NumProducts,                                                    
  NumObjects,                          
  PageSize,                                                     
  QuantityDiscountID,                                      
  TemplateName,                                
  SortOrder)                            
SELECT c.Counter,                                                    
  A.SourceFilterName,                                                   
  D.DisplayColumns,                                                     
  a.SourceFilterDescription,                                              
  E.WebDescription,
  E.Summary,                        
  E.SEName,                
  E.SEKeywords,                
  E.SEDescription,                
  E.SETitle,                                            
  E.SENoScript,                
  E.SEAltText,                
  A.SourceFilterName,                                                    
  0,                                                    
  0 AS DisplayOrder,                                                    
  D.OrderByLooks,                                                   
  D.XMLPackage,                
  D.Published,                
  0,                                                    
  0,                                                    
  0,                                                    
  ISNULL(NumProducts, 0),                                               
  ISNULL(NumProducts, 0),                                               
  D.PageSize,                                             
  0,                                     
  D.TemplateName,                
  NULL                                                    
  FROM SystemItemAttributeSourceFilterValueDescription a with (NOLOCK)                
  INNER JOIN SystemItemAttributeSourceFilterValue c with (NOLOCK) ON a.AttributeSourceFilterCode = C.AttributeSourceFilterCode AND a.SourceFilterName = C.SourceFilterName AND a.IsActive = 1                    
     INNER JOIN SystemItemAttributeSourceFilterCode b with (NOLOCK) ON a.AttributeSourceFilterCode = b.AttributeSourceFilterCode AND b.IsActive = 1 AND a.IsActive = 1  AND a.LanguageCode = @LanguageCode                
  INNER JOIN SystemItemAttributeSourceFilterValueWebOption D WITH (NOLOCK) ON a.AttributeSourceFilterCode = D.SourceFilterCode AND a.SourceFilterName = D.SourceFilterName                                                   
   INNER JOIN SystemItemAttributeSourceFilterValueWebOptionDescription E WITH (NOLOCK) ON a.AttributeSourceFilterCode = E.SourceFilterCode AND a.SourceFilterName = E.SourceFilterName                                                    
   LEFT JOIN (                            
    SELECT A.AttributeSourceFilterCode,A.AttributeValue, COUNT(A.ItemCode) AS NumProducts                             
    FROM InventoryItemAttributeView A WITH (NOLOCK)                                        
    JOIN InventoryItem B WITH (NOLOCK) ON A.ItemCode = B.ItemCode AND B.Status = 'A'                                            
INNER JOIN InventoryItemWebOption C WITH (NOLOCK) ON C.ItemCode = B.ItemCode                                            
    WHERE C.WebSiteCode = @WebSiteCode AND                             
   C.Published >= @PublishedOnly AND                             
   B.ItemType IN ('Stock', 'Kit', 'Matrix Group', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly')                
   AND A.LanguageCode = @LanguageCode                
    GROUP BY A.AttributeSourceFilterCode,A.AttributeValue                            
   ) F ON A.AttributeSourceFilterCode = F.AttributeSourceFilterCode AND A.SourceFilterName = F.AttributeValue                
   WHERE E.LanguageCode = @LanguageCode AND                             
  E.LanguageCode = @LanguageCode AND                             
  D.Published >= @PublishedOnly AND                                     
  D.WebSiteCode = @WebSiteCode AND                             
  E.WebSiteCode = @WebSiteCode AND                             
  (                            
   (D.StartDate IS NULL AND D.EndDate IS NULL) OR                              
   (D.StartDate IS NOT NULL AND D.StartDate <= @CurrentDate AND D.EndDate IS NULL) OR                              
   (D.StartDate IS NULL AND D.EndDate IS NOT NULL AND D.EndDate >= @CurrentDate) OR                              
   (@CurrentDate BETWEEN D.StartDate AND D.EndDate)                            
  )                                 
  AND D.WebSiteCode = @WebSiteCode                                                
   ORDER BY D.OrderByLooks , DisplayOrder, A.AttributeSourceFilterCode, A.SourceFilterName                     
    END                                             

IF @EntityName = 'Company'                                                     
BEGIN 
INSERT INTO #tmp(EntityID,                                                  
  [Name],                                                  
  ColWidth,                                                  
  [Description],                                                
  WebDescription,
  Summary,                                                
  SEName,                                                  
  SEKeywords,                                                  
  SEDescription,                  
  SETitle,                                                    
  SENoScript,                         
  SEAltText,                                                    
  ParentCategoryID,                                    
  ParentEntityID,                                                    
  DisplayOrder,                                        
  SortByLooks,                                                  
  XmlPackage,                     
  MobileXmlPackage,                                               
  Published,                                                    
  ContentsBGColor,                                          
  PageBGColor,                                                    
  GraphicsColor,                  
  NumProducts,                                                    
  NumObjects,                                                     
  PageSize,                                    
  QuantityDiscountID,                                      
  TemplateName,                                
  SortOrder,               
  VirtualType,              
  VirtualPageOption,              
  VirtualPageValueEntity,              
  VirtualPageValueTopic,              
  VirtualPageValueExternalPage,              
  OpenInNewTab) 
  SELECT * FROM                                                 
  (SELECT A.Counter,                                                    
  RTRIM(A.CategoryCode) AS CategoryCode,                                                   
  C.DisplayColumns,                                                     
  B.[Description],                                                
  D.WebDescription,
  D.Summary,                                                  
  REPLACE(C.CategoryCode, ' ', '-') AS SEName,                                                    
  D.SEKeywords,                        
  D.SEDescription,                                                    
  D.SETitle,                                                    
  D.SENoScript,                                                    
  D.SEAltText,                                                    
  A.ParentCategory,                                                    
  0 AS ParentEntityID,                                                    
  0 AS DisplayOrder,                                              
  C.OrderByLooks,                                                    
  C.XmlPackage,                                                    
  C.MobileXmlPackage,                 
  C.Published,                                        
  0 AS ContentsBGColor,                                                    
  0 AS PageBGColor,                                                    
  0 AS GraphicsColor,                                                    
  ISNULL(NumProducts, 0) AS NumProducts,                                                    
  ISNULL(NumProducts, 0) AS NumObjects,                                              
  C.PageSize,                                             
  0 AS QuantityDiscountID,                                      
  C.TemplateName,                                
  A.SortOrder,               
  ISNULL(A.VirtualType, 'Normal') AS VirtualType,              
  ISNULL(C.VirtualPageOption, 'Show Category') AS VirtualPageOption,              
  C.VirtualPageValueEntity,              
  C.VirtualPageValueTopic,              
  C.VirtualPageValueExternalPage,              
  CASE  
  WHEN A.VirtualType = 'Web Menu' AND C.VirtualPageOption = 'Show external page' AND C.OpenInNewTab = 1 THEN 1  
  ELSE 0  
  END AS OpenInNewTab                                                     
  FROM SystemCategory A WITH (NOLOCK)                                         
  INNER JOIN SystemCategoryDescription B WITH (NOLOCK) ON A.CategoryCode = B.CategoryCode                                                    
  INNER JOIN SystemCategoryWebOption C WITH (NOLOCK) ON A.CategoryCode = C.CategoryCode                                                    
  INNER JOIN SystemCategoryWebOptionDescription D WITH (NOLOCK) ON A.CategoryCode = D.CategoryCode                                                   
  LEFT JOIN (                            
   SELECT A.CategoryCode, COUNT(A.ItemCode) AS NumProducts                             
   FROM InventoryCategory A WITH (NOLOCK)                                     
   JOIN InventoryItem B WITH (NOLOCK) ON A.ItemCode = B.ItemCode AND B.Status = 'A'                                            
   INNER JOIN InventoryItemWebOption C WITH (NOLOCK) ON C.ItemCode = B.ItemCode                                            
   WHERE C.WebSiteCode = @WebSiteCode AND                             
     C.Published >= @PublishedOnly AND                             
     B.ItemType IN ('Stock', 'Kit', 'Matrix Group', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly')                                          
   GROUP BY A.CategoryCode                  
  ) E ON A.CategoryCode = E.CategoryCode                            
  INNER JOIN SGECustomerCategory_DEV004817 F ON F.CategoryCode_DEV004817 = A.CategoryCode
  WHERE D.LanguageCode = @LanguageCode AND                             
    B.LanguageCode = @LanguageCode AND                             
    C.Published >= @PublishedOnly AND                       
    IsActive = 1 AND                             
    C.WebSiteCode = @WebSiteCode AND                             
    D.WebSiteCode = @WebSiteCode AND                             
    (                            
     (C.StartDate IS NULL AND C.EndDate IS NULL) OR                    
     (C.StartDate IS NOT NULL AND C.StartDate <= @CurrentDate AND C.EndDate IS NULL) OR                              
     (C.StartDate IS NULL AND C.EndDate IS NOT NULL AND C.EndDate >= @CurrentDate) OR                              
     (@CurrentDate BETWEEN C.StartDate AND C.EndDate)                            
    )                                 
    AND C.WebSiteCode = @WebSiteCode AND                                               
	F.CustomerCode_DEV004817 = @CustomerCode
  UNION ALL
  SELECT 0,                                                    
  'PRODUCTS' AS CategoryCode,                                                   
  3,                                                     
  'PRODUCTS',                                                
  'PRODUCTS',                                                  
  'PRODUCTS',  
  'PRODUCTS',                                                    
  '',                        
  '',                                                    
  '',                                                    
  '',                                                    
  '',                                                    
  'PRODUCTS' AS ParentCategory,                                                    
  -1,                                                    
  0 AS DisplayOrder,                                              
  0 AS OrderByLooks,                                                    
  '',                                                    
  '',                 
  1,                                        
  0,                                                    
  0,                                                    
  0,                                                    
  0,                                                    
  0,                                              
  50,                                             
  0,                                      
  '',                                
  1,               
  'Normal',              
  'Show Category',              
  'PRODUCTS',              
  '',              
  '',              
  0) AS output_table
  ORDER BY OrderByLooks DESC, ParentCategory, DisplayOrder, CategoryCode
 END       
                                                    
  UPDATE #tmp SET ParentEntityID = A.Counter FROM (SELECT Counter, CategoryCode FROM SystemCategory WITH (NOLOCK)) A                                                   
  WHERE #tmp.Name <> #tmp.ParentCategoryID AND A.CategoryCode = #tmp.ParentCategoryID
         
  SELECT * FROM #tmp 

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[EcommerceGetAddress]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EcommerceGetAddress]
GO

CREATE PROCEDURE [dbo].[EcommerceGetAddress]              
 @CustomerCode NVARCHAR(30),            
 @AddressType INT,            
 @AddressID  NVARCHAR(30) = NULL,            
 @IsAnonymous BIT = 0,          
 @WebsiteCode NVARCHAR(30),      
 @ContactCode NVARCHAR(30) = NULL,        
 @RegistryId NVARCHAR(50) = NULL,    
 @BusinessType NVARCHAR(50) = NULL,
 @IsCompanyPortal BIT = 0        
AS         
        
/* Modified: EAConde       
** Date: 12.2.2010      
** Description: Reaaplied foreign-key CreditCardCode in retrieval of the Billing Address      
**      
** TODO: In the future, card Information may be included in this script.       
** However, it should be in tokens so as to prevent theft and comply with PA-DSS regulations.      
**      
*/      
BEGIN              
DECLARE @SqlQuery NVARCHAR(MAX)        
 IF @AddressType = 1 AND @IsAnonymous = 0 -- Billing              
  BEGIN              
   SELECT              
  c.CustomerCode,              
  cd.NameOnCard AS CustomerName,              
  cd.Address,              
  cd.City,               
  cd.State,              
  cd.PostalCode,              
  cd.County AS County,              
  cd.Country,             
  sc.ISOCode AS CountryISOCode,      
  sc.IsWithState,          
  cd.Telephone,              
  c.Email,             
  --cd.CardNumber,        
  cd.CreditCardCode,        
  cd.MaskedCardNumber,          
  cd.NameOnCard,          
  cd.ExpMonth,          
  cd.ExpYear,      
  cd.StartMonth,      
  cd.StartYear,      
  --cd.IssueNumber,      
  --cd.CreditCardSalt,          
  --cd.CreditCardIV,          
  cd.CreditCardType,        
  cd.ResidenceType,    
  cd.Plus4,  
  CASE WHEN sci.Country IS NOT NULL THEN 1 ELSE 0 END AS IsHomeCountry    
 FROM Customer c WITH (NOLOCK)          
 INNER JOIN CRMContact cc WITH (NOLOCK) ON cc.EntityCode = c.CustomerCode      
 INNER JOIN CustomerCreditCard cd WITH (NOLOCK) ON c.CustomerCode = cd.CustomerCode AND cd.CreditCardCode = CASE @IsCompanyPortal WHEN 0 THEN
 ISNULL(@AddressID, cc.DefaultBillingCode) ELSE ISNULL(@AddressID, cc.BillingCCCode_DEV004817) END
 INNER JOIN SystemCountry sc WITH (NOLOCK) ON sc.CountryCode = CD.Country  
 LEFT OUTER JOIN SystemCompanyInformation sci with (NOLOCK) ON sci.Country = CD.Country    
 WHERE c.CustomerCode = @CustomerCode AND cc.ContactCode = @ContactCode      
  END              
            
  IF @AddressType = 2 AND @IsAnonymous = 0 -- Shipping              
  BEGIN              
        
 DECLARE @temp NVARCHAR(30) = NULL        
 SET @temp = ISNULL(QUOTENAME(@AddressID,''''),'NULL')        
         
 SET @SqlQuery = 'SELECT c.CustomerCode, cs.ShipToCode, cs.ShipToName, cs.Address, cs.City, cs.State, cs.PostalCode,' +        
   'cs.County, cs.Country, sc.ISOCode AS CountryISOCode, sc.IsWithState, cs.Telephone, cs.Email, cs.AddressType, cs.ShippingMethod,' +        
   'cs.ShippingMethodGroup, cs.Plus4,' +  
   'CASE WHEN sci.Country IS NOT NULL THEN 1 ELSE 0 END AS IsHomeCountry ' +  
   'FROM Customer c WITH (NOLOCK) INNER JOIN CRMContact cc WITH (NOLOCK) ON cc.EntityCode = c.CustomerCode ' +        
   'INNER JOIN CustomerShipTo cs WITH (NOLOCK) ON c.CustomerCode = cs.CustomerCode and cs.ShipToCode = CASE ' + QUOTENAME(@IsCompanyPortal,'''') + ' WHEN 0 THEN ' +
   ' ISNULL(' + @temp + ', cc.DefaultShippingCode) ELSE ISNULL(' + @temp + ', cc.ShipToCode_DEV004817) END ' +        
   'INNER JOIN SystemCountry sc WITH (NOLOCK) ON cs.Country = sc.CountryCode ' +  
   'LEFT OUTER JOIN SystemCompanyInformation sci with (NOLOCK) ON sci.Country = sc.CountryCode '        
              
 IF(@RegistryId IS NULL)        
 BEGIN        
  SET @SqlQuery += N'WHERE c.CustomerCode = ' + QUOTENAME(@CustomerCode,'''') + ' AND ' + 'cc.ContactCode =' + QUOTENAME(@ContactCode,'''')        
 END        
         
 EXEC (@SqlQuery)        
         
 END          
            
 IF @AddressType = 1 AND @IsAnonymous = 1 -- Anonymous Billing              
  BEGIN              
 SELECT wa.Email,             
   wa.Counter,            
   wa.CustomerID,        
   wa.FirstName,    
   wa.LastName,         
   wa.BillToName AS [Name],             
   wa.BillToAddress AS Address,             
   wa.BillToCity AS City,               
   wa.BillToState AS State,             
   wa.BillToPostalCode AS PostalCode,             
   wa.BillToCountry AS Country,          
   sc.ISOCode AS CountryISOCode,          
   sc.IsWithState,          
   wa.BillToCounty AS County,             
   wa.BillToPhone AS Phone,             
   wa.BillToResidenceType AS ResidenceType,            
   --wa.CardNumber,            
   --wa.CardNumberSalt,              
   --wa.CardNumberIV,              
   wa.CardMaskedNumber,            
   wa.CardType,            
   wa.CardName,            
   wa.CardExpMonth,            
   wa.CardExpYear,          
   wa.CardStartMonth,          
   wa.CardStartYear,  
   CASE WHEN sci.Country IS NOT NULL THEN 1 ELSE 0 END AS IsHomeCountry     
   --,          
   --wa.CardIssueNumber,  
 FROM EcommerceAddress wa WITH (NOLOCK)           
 INNER JOIN SystemCountry sc WITH (NOLOCK) ON sc.CountryCode = wa.BillToCountry  
 LEFT OUTER JOIN SystemCompanyInformation sci with (NOLOCK) ON sci.Country = wa.BillToCountry  
 WHERE CAST(CustomerID AS CHAR(30)) = @CustomerCode              
  END              
            
 IF @AddressType = 2 AND @IsAnonymous = 1 -- Anonymous Shipping              
  BEGIN              
   SELECT wa.Email,             
   wa.Counter,             
   wa.CustomerID,             
   wa.ShipToName AS [Name],             
   wa.ShipToAddress AS Address,             
   wa.ShipToCity AS City,               
   wa.ShipToState AS State,             
   wa.ShipToPostalCode As PostalCode,             
   wa.ShipToCounty AS County,             
   wa.ShipToCountry AS Country,          
   sc.ISOCode AS CountryISOCode,          
   sc.IsWithState,          
   wa.ShipToPhone AS Phone,             
   wa.ShipToResidenceType AS ResidenceType,          
   wacs.ShippingMethod,    
   CASE WHEN @BusinessType = 'Wholesale' THEN     
   ( SELECT ShippingMethodGroup          
         FROM CustomerShipToClassTemplateDetailView          
         WHERE ClassCode =           
         ( SELECT DefaultWholesaleCustomerShipToClassTemplate           
         FROM SystemCountry          
         WHERE CountryCode = wa.ShipToCountry))     
 ELSE     
  ( SELECT ShippingMethodGroup          
         FROM CustomerShipToClassTemplateDetailView          
         WHERE ClassCode =           
         ( SELECT DefaultRetailCustomerShipToClassTemplate           
         FROM SystemCountry          
         WHERE CountryCode = wa.ShipToCountry))    
 END    
 AS ShippingMethodGroup,    
 CASE WHEN @BusinessType = 'Wholesale' THEN     
   ( SELECT PaymentTermGroup          
         FROM CustomerShipToClassTemplateDetailView          
         WHERE ClassCode =           
         ( SELECT DefaultWholesaleCustomerShipToClassTemplate           
         FROM SystemCountry          
         WHERE CountryCode = wa.ShipToCountry))     
 ELSE     
  ( SELECT PaymentTermGroup          
         FROM CustomerShipToClassTemplateDetailView          
         WHERE ClassCode =           
         ( SELECT DefaultRetailCustomerShipToClassTemplate           
         FROM SystemCountry          
         WHERE CountryCode = wa.ShipToCountry))    
 END    
 AS PaymentTermGroup, wacs.AddressType,      

 CASE WHEN sci.Country IS NOT NULL THEN 1 ELSE 0 END AS IsHomeCountry            
 FROM EcommerceAddress wa WITH (NOLOCK)           
 INNER JOIN EcommerceSite w WITH (NOLOCK) ON w.WebsiteCode = @WebsiteCode            
 INNER JOIN Customer wac WITH (NOLOCK) ON wac.CustomerCode = w.CustomerCode          
 INNER JOIN CustomerShipTo wacs WITH (NOLOCK) ON wacs.CustomerCode = wac.CustomerCode AND wac.DefaultShipToCode = wacs.ShipToCode          
 INNER JOIN SystemCountry sc WITH (NOLOCK) ON sc.CountryCode = wa.ShipToCountry  
 LEFT OUTER JOIN SystemCompanyInformation sci with (NOLOCK) ON sci.Country = wa.ShipToCountry   
 WHERE CAST(wa.CustomerID AS CHAR(30)) = @CustomerCode              
  END              
END 

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[EcommerceCheckCartAvailability]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EcommerceCheckCartAvailability]
GO


CREATE PROCEDURE [dbo].[EcommerceCheckCartAvailability]              
 @CartType INT,              
 @CustomerCode NVARCHAR(30),              
 @WarehouseCode NVARCHAR(30),              
 @ContactCode NVARCHAR(30),
 @WebsiteCode NVARCHAR(30),
 @IsPortal BIT = 0
AS              
BEGIN              
            
 CREATE TABLE #inventoryfreestock (ItemCode  NVARCHAR(50), FreeStock NUMERIC, UM NVARCHAR(30))                               
 DECLARE @AdditionalJoin NVARCHAR(1000)                 
 SET @AdditionalJoin = ''                 
                         
 DECLARE @KitAdditionalJoin NVARCHAR(1000)             
 SET @KitAdditionalJoin = ''               
                       
 DECLARE @SqlQuery NVARCHAR(MAX)               
 DECLARE @ShowInventoryFromAllWarehouses BIT               
               
 SET @ShowInventoryFromAllWarehouses = 0                                                                                 
 SELECT @ShowInventoryFromAllWarehouses = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END                                                                                            
 FROM EcommerceAppConfig WITH (NOLOCK)                                                                             
 WHERE Name = 'ShowInventoryFromAllWarehouses' AND WebSiteCode = @WebsiteCode

 IF @ShowInventoryFromAllWarehouses = 1               
 BEGIN              
	--IF @IsPortal = 1
	--BEGIN
	--	INSERT INTO #inventoryfreestock (ItemCode, FreeStock, UM)                                
	--	SELECT ItemCode, SUM(FreeStock) AS FreeStock, UM FROM EcommerceStockAvailabilityView     
	--	WHERE WarehouseCode IN ('HN Warehouse','INGLEBURN-STOCK') --Hardcoded warehouse for Portal website                           
	--	GROUP BY ItemCode, UM                                       
	--END
	--ELSE
	--BEGIN
	--   INSERT INTO #inventoryfreestock (ItemCode, FreeStock, UM)                                
	--   SELECT ItemCode, SUM(FreeStock) AS FreeStock, UM FROM EcommerceStockAvailabilityView                                
	--   GROUP BY ItemCode, UM                                       
	--  END
	INSERT INTO #inventoryfreestock (ItemCode, FreeStock, UM)                                
	   SELECT ItemCode, SUM(FreeStock) AS FreeStock, UM FROM EcommerceStockAvailabilityView                                
	   GROUP BY ItemCode, UM                                       
 END              
 ELSE              
 BEGIN              
   INSERT INTO #inventoryfreestock (ItemCode, FreeStock, UM)                             
   SELECT ItemCode, FreeStock AS FreeStock, UM FROM EcommerceStockAvailabilityView                              
   WHERE WarehouseCode = @WarehouseCode                              
 END              
  
SET @AdditionalJoin += ' INNER JOIN #inventoryfreestock IFS WITH (NOLOCK) ON IFS.ItemCode = wsc.ItemCode'            
SET @KitAdditionalJoin += ' INNER JOIN #inventoryfreestock KIFS WITH (NOLOCK) ON KIFS.ItemCode = wkc.ItemCode'

 SET @SqlQuery =            
 'SELECT wsc.ShoppingCartRecGuid, NULL AS ItemKitCode, wsc.ItemCode, i.ItemType, wsc.Quantity, IFS.FreeStock, i.Status, ium.UnitMeasureCode, i.IsCBN, i.CBNItemID              
 FROM EcommerceShoppingCart wsc WITH (NOLOCK)               
 INNER JOIN InventoryItem i WITH (NOLOCK) ON i.ItemCode = wsc.ItemCode' + @AdditionalJoin +                  
              
 ' INNER JOIN InventoryUnitMeasure ium WITH (NOLOCK) ON ium.ItemCode = wsc.ItemCode AND IFS.UM = ium.UnitMeasureCode AND ium.IsBase = 1               
 WHERE wsc.CartType =  ' + QUOTENAME(@CartType, '''') +' AND wsc.CustomerCode = ' + QUOTENAME(@CustomerCode, '''') +  
 ' AND (wsc.InStoreWareHouseCode IS NULL OR wsc.InStoreWareHouseCode = '''') AND i.ItemType not in (''Kit'', ''Non-Stock'', ''Electronic Download'', ''Service'') AND wsc.ContactCode = ISNULL(' + QUOTENAME(@ContactCode, '''') +', '''')              
 UNION ALL              
 SELECT kwsc.ShoppingCartRecGuid, wkc.ItemKitCode, wkc.ItemCode, ki.ItemType, (kwsc.Quantity * ikd.Quantity) AS Quantity, KIFS.FreeStock, ki.Status, kium.UnitMeasureCode, ki.IsCBN, ki.CBNItemID              
 FROM EcommerceKitCart wkc WITH (NOLOCK)               
 INNER JOIN EcommerceShoppingCart kwsc WITH (NOLOCK) ON wkc.CartId = kwsc.ShoppingCartRecGuid              
 INNER JOIN InventoryItem ki WITH (NOLOCK) ON kwsc.ItemCode = ki.ItemCode' + @KitAdditionalJoin +             
                        
 ' INNER JOIN InventoryKitDetail ikd WITH (NOLOCK) ON ikd.ItemCode = wkc.ItemCode AND ikd.ItemKitCode = wkc.ItemKitCode              
 INNER JOIN InventoryUnitMeasure kium WITH (NOLOCK) ON kium.ItemCode = ki.ItemCode              
 WHERE  kwsc.CartType = ' + QUOTENAME(@CartType, '''') +' AND               
     kwsc.CustomerCode = ' + QUOTENAME(@CustomerCode, '''') +' AND                  
     ki.ItemType = ''Kit'''
             
 EXEC (@SqlQuery)            
 
END

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE name = 'ItemSaleItemMainView' AND type = N'V')
    DROP VIEW [dbo].[ItemSaleItemMainView]
GO

CREATE VIEW ItemSaleItemMainView AS 
SELECT TOP 100 AA.[Product Code],
AA.[Product Description],
AA.InvoiceDate,
AA.Price,
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) AND  Month(AA.InvoiceDate)= '1' AND  A.[Product Code]= AA.[Product Code]),0) +
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) AND  Month(AA.InvoiceDate)= '2' AND  A.[Product Code]= AA.[Product Code]),0) +
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) AND  Month(AA.InvoiceDate)= '3' AND  A.[Product Code]= AA.[Product Code]),0) AS '1st Qtr Sales',
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) AND  Month(AA.InvoiceDate)= '4' AND  A.[Product Code]= AA.[Product Code]),0) +
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) AND  Month(AA.InvoiceDate)= '5' AND  A.[Product Code]= AA.[Product Code]),0) +
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) AND  Month(AA.InvoiceDate)= '6' AND  A.[Product Code]= AA.[Product Code]),0) AS '2nd Qtr Sales',
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) AND  Month(AA.InvoiceDate)= '7' AND  A.[Product Code]= AA.[Product Code]),0) +
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) AND  Month(AA.InvoiceDate)= '8' AND  A.[Product Code]= AA.[Product Code]),0) +
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) AND  Month(AA.InvoiceDate)= '9' AND  A.[Product Code]= AA.[Product Code]),0) AS '3rd Qtr Sales',
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) AND  Month(AA.InvoiceDate)= '10' AND  A.[Product Code]= AA.[Product Code]),0) +
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) AND  Month(AA.InvoiceDate)= '11' AND  A.[Product Code]= AA.[Product Code]),0) +
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) AND  Month(AA.InvoiceDate)= '12' AND  A.[Product Code]= AA.[Product Code]),0) AS '4th Qtr Sales',
ISNULL((SELECT SUM(QuantityOrdered) FROM ItemSalesView A where Year(InvoiceDate) = Year(AA.InvoiceDate) and  A.[Product Code]=AA.[Product Code]),0) AS 'YTD Sales',
'0' AS 'FY2014/15 Sales',
AA.[Stock On Hand] AS 'SOH Units',
AA.[Unit Measure],
'' AS 'SOH in Months',
AA.[Stock On Order] AS 'Stock On Order (SOO)',
(AA.[Stock On Order] * AA.[Sales Price]) AS 'SOO Value'
FROM ItemSalesView AA
WHERE Year(InvoiceDate)='2017'


GO

/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReadEcommerceCustomerWebOrder]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReadEcommerceCustomerWebOrder]
GO

CREATE PROCEDURE [dbo].[ReadEcommerceCustomerWebOrder]          
 @CustomerCode NVARCHAR(30),          
 @WebsiteCode NVARCHAR(30),          
 @Pages INT = -1,          
 @Current INT = 1,
 @IsPortal BIT = 0,        
 @ContactCode NVARCHAR(30) = NULL        
AS    
  
BEGIN       
 SET NOCOUNT ON;       
 SELECT @ContactCode = CASE WHEN @ContactCode = DefaultContact AND @IsPortal = 0 THEN NULL ELSE @ContactCode END FROM Customer WHERE CustomerCode = @CustomerCode       
          
  CREATE TABLE #tmpWebOrder(          
   Row INT IDENTITY(1,1),          
   SalesOrderCode NVARCHAR(30),          
   InvoiceCode NVARCHAR(30),       
   SalesOrderDate SMALLDATETIME,          
   TotalRate NUMERIC(14,6),          
   Notes NVARCHAR(500),          
   DownloadEmailSentDate DATETIME,          
   ShippingDate SMALLDATETIME,          
   TransactionState NVARCHAR(30),          
   PaymentMethod NVARCHAR(30),          
   ElectronicDownloadItemCount INT,      
   TrackingNumber NVARCHAR(MAX),      
   TrackingLink NVARCHAR(MAX),
   ContactName NVARCHAR(MAX)            
  )        
          
 INSERT INTO #tmpWebOrder(          
    [SalesOrderCode], 
	InvoiceCode,          
    SalesOrderDate,           
    TotalRate,           
    Notes,           
    [DownloadEmailSentDate],           
    ShippingDate,           
    TransactionState,           
    PaymentMethod,          
    [ElectronicDownloadItemCount],      
    TrackingNumber,      
    TrackingLink,
	ContactName)          
  SELECT DISTINCT cso.[SalesOrderCode], 
	CASE WHEN ci.IsPosted = 1 THEN ci.InvoiceCode
	ELSE NULL        
    END AS InvoiceCode,                 
    cso.[SalesOrderDate],          
    cso.[TotalRate],          
    cso.[PublicNotes],          
    ci.DownloadEmailSentDate,          
    CASE         
  WHEN ci.IsShipped = 1 THEN ci.ShippingDate        
  ELSE NULL        
    END AS ShippingDate,        
    CASE           
  WHEN csow.Stage = 'Completed' THEN 'Completed'          
  WHEN pm.CreditCardIsVoided  = 1 THEN 'VOIDED'                      
  WHEN pm.CreditCardIsCredited  = 1 THEN 'REFUND'                      
  WHEN pm.CreditCardIsForced  = 1 THEN 'FORCED'                      
  WHEN pm.CreditCardIsCaptured  = 1 THEN 'CAPTURED'                      
  WHEN pm.CreditCardIsAuthorized = 1 THEN 'AUTH'                      
  WHEN pm.CreditCardIsSold   = 1 THEN 'AUTH CAPTURED'                      
  ELSE NULL           
    END AS TransactionState,             
    sptp.PaymentMethodCode AS PaymentMethod,          
    (          
  SELECT COUNT(1) AS N          
  FROM dbo.[CustomerSalesOrderDetail] csod WITH (NOLOCK)           
  INNER JOIN dbo.InventoryItem i WITH (NOLOCK) ON i.[ItemCode] = csod.[ItemCode]          
  WHERE csod.[SalesOrderCode] = cso.[SalesOrderCode] AND i.[ItemType] = 'Electronic Download'          
    )  AS ElectronicDownloadItemCount,      
    S.TrackingNumber, SAC.ConfigValue + S.TrackingNumber,          
  (ContactFirstName + ' ' + ContactLastName) AS ContactName          
  FROM dbo.CustomerSalesOrder cso WITH (NOLOCK)           
  INNER JOIN dbo.[SystemPaymentTerm] spt WITH (NOLOCK) ON spt.[PaymentTermCode] = cso.[PaymentTermCode]          
  INNER JOIN dbo.[SystemPaymentType] sptp WITH (NOLOCK) ON sptp.[PaymentTypeCode] = spt.[PaymentType]          
  LEFT OUTER JOIN dbo.[CustomerSalesOrderWorkflow] csow WITH (NOLOCK) ON csow.[SalesOrderCode] = cso.[SalesOrderCode]          
  LEFT OUTER JOIN (  
	SELECT CTR.DocumentCode,
			CAST(MAX(CAST(P.CreditCardIsVoided AS INT)) AS BIT) AS CreditCardIsVoided,
			CAST(MAX(CAST(P.CreditCardIsCredited AS INT)) AS BIT) AS CreditCardIsCredited,
			CAST(MAX(CAST(P.CreditCardIsForced AS INT)) AS BIT) AS CreditCardIsForced,
			CAST(MAX(CAST(P.CreditCardIsCaptured AS INT)) AS BIT) AS CreditCardIsCaptured,
			CAST(MAX(CAST(P.CreditCardIsAuthorized AS INT)) AS BIT) AS CreditCardIsAuthorized,
			CAST(MAX(CAST(P.CreditCardIsSold AS INT)) AS BIT) AS CreditCardIsSold
	FROM CustomerTransactionReceipt CTR WITH (NOLOCK)
	INNER JOIN PaymentMethod P WITH (NOLOCK) ON P.DocumentCode = CTR.ReceivableCode
	GROUP BY CTR.DocumentCode
  ) PM ON PM.DocumentCode = CSO.SalesOrderCode     
  LEFT OUTER JOIN dbo.[CustomerInvoice] ci WITH (NOLOCK) ON ci.[SourceInvoiceCode] = cso.[SalesOrderCode]  AND ci.[Type] = 'Invoice'      
  LEFT JOIN dbo.Shipment S WITH (NOLOCK) ON S.SourceDocument = cso.SalesOrderCode OR S.SourceDocument = ci.InvoiceCode      
  LEFT JOIN dbo.ShipmentCarrier SC WITH (NOLOCK) ON SC.CarrierCode = S.CarrierCode      
  LEFT JOIN dbo.ShippingAppConfig SAC WITH (NOLOCK) ON SAC.WarehouseCode = SC.WarehouseCode AND SAC.Name = SC.CarrierDescription + '.OnlineTrackingURL'      
  AND S.TrackingNumber <> 'To be generated'  
  INNER JOIN CRMContact cc ON cc.ContactCode = cso.ContactCode    
  WHERE cso.[WebSiteCode] = @WebsiteCode AND           
    cso.BillToCode = @CustomerCode AND          
    cso.IsVoided = 0 AND           
    cso.TYPE IN ('Sales Order', 'Quote') AND        
	cso.ContactCode = CASE WHEN (@ContactCode = 'ALL') THEN cso.ContactCode ELSE ISNULL(@ContactCode, cso.ContactCode) END AND
	ISNULL(cc.JobRoleCode, '') IN (CASE WHEN (@IsPortal = 1) THEN (SELECT JobRoleCode FROM SystemJobRole WHERE JobRoleCode IN ('Admin', 'Store Admin', 'Store')) ELSE (ISNULL(cc.JobRoleCode, '')) END)
  ORDER BY cso.[SalesOrderCode] DESC          
           
 DECLARE @Rows INT  
 DECLARE @MaxPage INT          
 DECLARE @AllPages INT          
 DECLARE @Offset INT          
       
 DECLARE @Start INT          
 DECLARE @End INT          
        
 SET @MaxPage = 0          
 SET @AllPages = 0          
 SET @Offset = 0          
 SET @Start = 0          
 SET @End = 0          
       
 SELECT @Rows = COUNT(1) FROM #tmpWebOrder          
        
 IF(@Rows > 0)          
 BEGIN          
 IF(@Pages <= 0)          
 BEGIN          
 SET @Pages = @Rows          
 END          
         
 SELECT @MaxPage = @Rows / @Pages          
 SELECT @AllPages = @MaxPage          
       
 SELECT @Offset = @Rows % @Pages          
 IF(@Offset > 0)          
 BEGIN          
 SET @AllPages = @AllPages + 1          
 END          
 -- make sure we don't go out of bounds          
 IF(@Current >= @AllPages)          
 BEGIN          
 SET @Current = @AllPages          
 END          
       
 IF((@Pages * @Current) > @Rows)          
 BEGIN          
 SELECT @Start = (@Pages * @MaxPage) + 1          
 SELECT @End = @Rows          
 END          
 ELSE          
 BEGIN          
 SELECT @End = @Pages * @Current          
 SELECT @Start = (@End - @Pages) + 1          
 END           
 END            
        
 -- Headers          
 SELECT @Rows AS Rows, @Current AS [Current], @Pages AS Pages, @AllPages AS AllPages, @Start AS Start, @End AS [End]          
       
 -- Sales Order History          
 SELECT * FROM [#tmpWebOrder] WHERE Row BETWEEN @Start AND @End          
        
 DROP TABLE [#tmpWebOrder]          
END   

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReadEcommerceCompanyWebOrder]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReadEcommerceCompanyWebOrder]
GO

CREATE PROCEDURE [dbo].[ReadEcommerceCompanyWebOrder]          
 @CustomerCode NVARCHAR(30),          
 @WebsiteCode NVARCHAR(30),          
 @StoreAdminCode NVARCHAR(30),          
 @Pages INT = -1,          
 @Current INT = 1,
 @IsPortal BIT = 0,        
 @ContactCode NVARCHAR(30) = NULL        
AS    
  
BEGIN       
 SET NOCOUNT ON;       
 SELECT @ContactCode = CASE WHEN @ContactCode = DefaultContact AND @IsPortal = 0 THEN NULL ELSE @ContactCode END FROM Customer WHERE CustomerCode = @CustomerCode       
          
  CREATE TABLE #tmpWebOrder(          
   Row INT IDENTITY(1,1),          
   SalesOrderCode NVARCHAR(30),          
   InvoiceCode NVARCHAR(30),       
   SalesOrderDate SMALLDATETIME,          
   TotalRate NUMERIC(14,6),          
   Notes NVARCHAR(500),          
   DownloadEmailSentDate DATETIME,          
   ShippingDate SMALLDATETIME,          
   TransactionState NVARCHAR(30),          
   PaymentMethod NVARCHAR(30),          
   ElectronicDownloadItemCount INT,      
   TrackingNumber NVARCHAR(MAX),      
   TrackingLink NVARCHAR(MAX),
   ContactName NVARCHAR(MAX)            
  )        
          
 INSERT INTO #tmpWebOrder(          
    [SalesOrderCode], 
	InvoiceCode,          
    SalesOrderDate,           
    TotalRate,           
    Notes,           
    [DownloadEmailSentDate],           
    ShippingDate,           
    TransactionState,           
    PaymentMethod,          
    [ElectronicDownloadItemCount],      
    TrackingNumber,      
    TrackingLink,
	ContactName)          
  SELECT DISTINCT cso.[SalesOrderCode], 
	CASE WHEN ci.IsPosted = 1 THEN ci.InvoiceCode
	ELSE NULL        
    END AS InvoiceCode,                 
    cso.[SalesOrderDate],          
    cso.[TotalRate],          
    cso.[PublicNotes],          
    ci.DownloadEmailSentDate,          
    CASE         
  WHEN ci.IsShipped = 1 THEN ci.ShippingDate        
  ELSE NULL        
    END AS ShippingDate,        
    CASE           
  WHEN csow.Stage = 'Completed' THEN 'Completed'          
  WHEN pm.CreditCardIsVoided  = 1 THEN 'VOIDED'                      
  WHEN pm.CreditCardIsCredited  = 1 THEN 'REFUND'                      
  WHEN pm.CreditCardIsForced  = 1 THEN 'FORCED'                      
  WHEN pm.CreditCardIsCaptured  = 1 THEN 'CAPTURED'                      
  WHEN pm.CreditCardIsAuthorized = 1 THEN 'AUTH'                      
  WHEN pm.CreditCardIsSold   = 1 THEN 'AUTH CAPTURED'                      
  ELSE NULL           
    END AS TransactionState,             
    sptp.PaymentMethodCode AS PaymentMethod,          
    (          
  SELECT COUNT(1) AS N          
  FROM dbo.[CustomerSalesOrderDetail] csod WITH (NOLOCK)           
  INNER JOIN dbo.InventoryItem i WITH (NOLOCK) ON i.[ItemCode] = csod.[ItemCode]          
  WHERE csod.[SalesOrderCode] = cso.[SalesOrderCode] AND i.[ItemType] = 'Electronic Download'          
    )  AS ElectronicDownloadItemCount,      
    S.TrackingNumber, SAC.ConfigValue + S.TrackingNumber,          
  (ContactFirstName + ' ' + ContactLastName) AS ContactName          
  FROM dbo.CustomerSalesOrder cso WITH (NOLOCK)           
  INNER JOIN dbo.[SystemPaymentTerm] spt WITH (NOLOCK) ON spt.[PaymentTermCode] = cso.[PaymentTermCode]          
  INNER JOIN dbo.[SystemPaymentType] sptp WITH (NOLOCK) ON sptp.[PaymentTypeCode] = spt.[PaymentType]          
  LEFT OUTER JOIN dbo.[CustomerSalesOrderWorkflow] csow WITH (NOLOCK) ON csow.[SalesOrderCode] = cso.[SalesOrderCode]          
  LEFT OUTER JOIN (  
	SELECT CTR.DocumentCode,
			CAST(MAX(CAST(P.CreditCardIsVoided AS INT)) AS BIT) AS CreditCardIsVoided,
			CAST(MAX(CAST(P.CreditCardIsCredited AS INT)) AS BIT) AS CreditCardIsCredited,
			CAST(MAX(CAST(P.CreditCardIsForced AS INT)) AS BIT) AS CreditCardIsForced,
			CAST(MAX(CAST(P.CreditCardIsCaptured AS INT)) AS BIT) AS CreditCardIsCaptured,
			CAST(MAX(CAST(P.CreditCardIsAuthorized AS INT)) AS BIT) AS CreditCardIsAuthorized,
			CAST(MAX(CAST(P.CreditCardIsSold AS INT)) AS BIT) AS CreditCardIsSold
	FROM CustomerTransactionReceipt CTR WITH (NOLOCK)
	INNER JOIN PaymentMethod P WITH (NOLOCK) ON P.DocumentCode = CTR.ReceivableCode
	GROUP BY CTR.DocumentCode
  ) PM ON PM.DocumentCode = CSO.SalesOrderCode     
  LEFT OUTER JOIN dbo.[CustomerInvoice] ci WITH (NOLOCK) ON ci.[SourceInvoiceCode] = cso.[SalesOrderCode]  AND ci.[Type] = 'Invoice'      
  LEFT JOIN dbo.Shipment S WITH (NOLOCK) ON S.SourceDocument = cso.SalesOrderCode OR S.SourceDocument = ci.InvoiceCode      
  LEFT JOIN dbo.ShipmentCarrier SC WITH (NOLOCK) ON SC.CarrierCode = S.CarrierCode      
  LEFT JOIN dbo.ShippingAppConfig SAC WITH (NOLOCK) ON SAC.WarehouseCode = SC.WarehouseCode AND SAC.Name = SC.CarrierDescription + '.OnlineTrackingURL'      
  AND S.TrackingNumber <> 'To be generated'  
  INNER JOIN CRMContact cc ON cc.ContactCode = cso.ContactCode    
  WHERE cso.[WebSiteCode] = @WebsiteCode AND           
    cso.BillToCode = @CustomerCode AND          
    cso.IsVoided = 0 AND           
    cso.TYPE IN ('Sales Order', 'Quote') AND        
	cso.ContactCode = CASE WHEN (@ContactCode = 'ALL') THEN cso.ContactCode ELSE ISNULL(@ContactCode, cso.ContactCode) END AND
	cc.JobRoleCode IN ('Admin', 'Store Admin', 'Store') AND
	(ISNULL(cc.StoreAdmin_C, '') LIKE '%' + @StoreAdminCode + '%' OR ISNULL(StoreMovex_C, '') = @StoreAdminCode)
  ORDER BY cso.[SalesOrderCode] DESC          
           
 DECLARE @Rows INT  
 DECLARE @MaxPage INT          
 DECLARE @AllPages INT          
 DECLARE @Offset INT          
       
 DECLARE @Start INT          
 DECLARE @End INT          
        
 SET @MaxPage = 0          
 SET @AllPages = 0          
 SET @Offset = 0          
 SET @Start = 0          
 SET @End = 0          
       
 SELECT @Rows = COUNT(1) FROM #tmpWebOrder          
        
 IF(@Rows > 0)          
 BEGIN          
 IF(@Pages <= 0)          
 BEGIN          
 SET @Pages = @Rows          
 END          
         
 SELECT @MaxPage = @Rows / @Pages          
 SELECT @AllPages = @MaxPage          
       
 SELECT @Offset = @Rows % @Pages          
 IF(@Offset > 0)          
 BEGIN          
 SET @AllPages = @AllPages + 1          
 END          
 -- make sure we don't go out of bounds          
 IF(@Current >= @AllPages)          
 BEGIN          
 SET @Current = @AllPages          
 END          
       
 IF((@Pages * @Current) > @Rows)          
 BEGIN          
 SELECT @Start = (@Pages * @MaxPage) + 1          
 SELECT @End = @Rows          
 END          
 ELSE          
 BEGIN          
 SELECT @End = @Pages * @Current          
 SELECT @Start = (@End - @Pages) + 1          
 END           
 END            
        
 -- Headers          
 SELECT @Rows AS Rows, @Current AS [Current], @Pages AS Pages, @AllPages AS AllPages, @Start AS Start, @End AS [End]          
       
 -- Sales Order History          
 SELECT * FROM [#tmpWebOrder] WHERE Row BETWEEN @Start AND @End          
        
 DROP TABLE [#tmpWebOrder]          
END   
  
GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReadEcommerceReorderApproval]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReadEcommerceReorderApproval]
GO

CREATE PROCEDURE [dbo].[ReadEcommerceReorderApproval]          
 @LanguageCode NVARCHAR(100),          
 @WebsiteCode NVARCHAR(30),          
 @WarehouseCode NVARCHAR(60),
 @CurrentMonth NVARCHAR(2),
 @Status NVARCHAR(1),
 @CostCenter NVARCHAR(100)
AS    
  
BEGIN       
	SET NOCOUNT ON;       

	IF (@CurrentMonth = '' OR  @CurrentMonth = '0')
	BEGIN
		SELECT ROW_NUMBER() OVER (ORDER BY A.Counter) AS Counter, A.ItemCode, A.ItemName, B.ItemDescription, A.IsPlastic_DEV004817 AS IsPlastic, CAST(ISNULL(D.UnitsInStock/ISNULL(I.UnitMeasureQty, E.UnitMeasureQty), 0) AS INT) AS InWH, ISNULL(D.ReorderPoint, 0) AS OrderPoint, CAST(ISNULL(E.QuantityOrdered/CASE WHEN E.UnitMeasure = I.UnitMeasureCode THEN 1 ELSE ISNULL(I.UnitMeasureQty, E.UnitMeasureQty) END, 0) AS INT) AS OnOrder, 
		ISNULL(D.ReorderQuantity_C, 0) AS ReorderQty, CAST(ISNULL(J.QuantityOrdered, 0) + ISNULL(A.BKO_C, 0) AS INT) AS OnBackOrder, E.PurchaseOrderCode, ISNULL(E.UnitMeasure, I.UnitMeasureCode) AS UnitMeasureCode, ISNULL(I.UnitMeasureQty, E.UnitMeasureQty) AS UnitMeasureQty, CONVERT(VARCHAR, E.DateApproved_DEV004817, 103) AS DateApproved, 
		ISNULL(CAST(E.IsApproved_DEV004817 AS VARCHAR(1)), 2) AS IsApproved, CASE WHEN ISNULL(A.ProductionSource_DEV004817, '') = 'Overseas' THEN CONVERT(VARCHAR, EstimatedArrival_DEV004817, 103) WHEN ISNULL(A.ProductionSource_DEV004817, '') != 'Overseas' AND  E.IsApproved_DEV004817 = 1 THEN CONVERT(VARCHAR, E.ETA_C, 103) ELSE NULL END AS ETA, 
		A.DefaultWarehouse_DEV004817 AS WarehouseCode, A.Status, CONVERT(VARCHAR, E.PODate, 103) AS PODate, CONVERT(VARCHAR, DATEADD(DAY, 14, E.PODate), 103) AS PODate_2WeekDelivery, A.ProductionSource_DEV004817 AS ProductionSource, CASE WHEN ISNULL(FYSales_DEV004817, 0) = 0 THEN 0 ELSE ISNULL(((UnitsInStock/ISNULL(I.UnitMeasureQty, 1)) / FYSales_DEV004817) * 12, 0) END AS [SOHinMonths],
		ISNULL(CAST(E.IsDeclined_DEV004817 AS VARCHAR(1)), 2) AS IsDeclined, CONVERT(VARCHAR, E.DateDeclined_DEV004817, 103) AS DateDeclined
		FROM InventoryItem A INNER JOIN InventoryItemDescription B ON A.ItemCode = B.ItemCode INNER JOIN InventoryItemWebOption C ON A.ItemCode = C.ItemCode LEFT JOIN (SELECT A.ItemCode, UnitsInStock, ReorderPoint, ReorderQuantity_C, WarehouseCode FROM InventoryStockTotal A INNER JOIN InventoryItem B ON A.ItemCode = B.ItemCode AND A.WarehouseCode = B.DefaultWarehouse_DEV004817) D ON A.ItemCode = D.ItemCode
		LEFT JOIN (SELECT A.PurchaseOrderCode, ItemCode, UnitMeasure, UnitMeasureQty, QuantityOrdered, DateApproved_DEV004817, IsApproved_DEV004817, PODate, ETA_C, IsDeclined_DEV004817, DateDeclined_DEV004817 FROM SupplierPurchaseOrder A INNER JOIN SupplierPurchaseOrderDetail B ON A.PurchaseOrderCode = B.PurchaseOrderCode WHERE Type = 'Purchase Order' AND IsVoided != '1') AS E ON E.ItemCode = A.ItemCode
		LEFT JOIN SGEShipmentAttachOrders_DEV004817 G ON G.POCode_DEV004817 = E.PurchaseOrderCode LEFT JOIN SGEShipment_DEV004817 H ON H.ShipmentID_DEV004817 = G.ShipmentID_DEV004817
		INNER JOIN InventoryUnitMeasure I ON I.ItemCode = A.ItemCode AND I.DefaultSelling = 1
		LEFT JOIN (SELECT B.ItemCode, SUM(B.QuantityOrdered) AS QuantityOrdered FROM CustomerSalesOrder A INNER JOIN CustomerSalesOrderDetail B ON A.SalesOrderCode = B.SalesOrderCode WHERE OrderStatus = 'Open' AND Type IN ('Sales Order', 'Back Order') GROUP BY B.ItemCode) AS J ON J.ItemCode = A.ItemCode
		WHERE B.LanguageCode = @LanguageCode AND C.WebSiteCode = @WebsiteCode AND A.CostCenter_DEV004817 = @CostCenter AND A.Status = @Status AND E.DateApproved_DEV004817 IS NULL AND E.PurchaseOrderCode IS NOT NULL AND ISNULL(IsDeclined_DEV004817, 0) = 0  ORDER BY E.PurchaseOrderCode
	END

	IF @CurrentMonth = '1' 
	BEGIN
		SELECT ROW_NUMBER() OVER (ORDER BY A.Counter) AS Counter, A.ItemCode, A.ItemName, B.ItemDescription, A.IsPlastic_DEV004817 AS IsPlastic, CAST(ISNULL(D.UnitsInStock/ISNULL(I.UnitMeasureQty, E.UnitMeasureQty), 0) AS INT) AS InWH, ISNULL(D.ReorderPoint, 0) AS OrderPoint, CAST(ISNULL(E.QuantityOrdered/CASE WHEN E.UnitMeasure = I.UnitMeasureCode THEN 1 ELSE ISNULL(I.UnitMeasureQty, E.UnitMeasureQty) END, 0) AS INT) AS OnOrder, 
		ISNULL(D.ReorderQuantity_C, 0) AS ReorderQty, CAST(ISNULL(J.QuantityOrdered, 0) + ISNULL(A.BKO_C, 0) AS INT) AS OnBackOrder, E.PurchaseOrderCode, ISNULL(E.UnitMeasure, I.UnitMeasureCode) AS UnitMeasureCode, ISNULL(I.UnitMeasureQty, E.UnitMeasureQty) AS UnitMeasureQty, CONVERT(VARCHAR, E.DateApproved_DEV004817, 103) AS DateApproved, 
		ISNULL(CAST(E.IsApproved_DEV004817 AS VARCHAR(1)), 2) AS IsApproved, CASE WHEN ISNULL(A.ProductionSource_DEV004817, '') = 'Overseas' THEN CONVERT(VARCHAR, EstimatedArrival_DEV004817, 103) WHEN ISNULL(A.ProductionSource_DEV004817, '') != 'Overseas' AND  E.IsApproved_DEV004817 = 1 THEN CONVERT(VARCHAR, E.ETA_C, 103) ELSE NULL END AS ETA, 
		A.DefaultWarehouse_DEV004817 AS WarehouseCode, A.Status, CONVERT(VARCHAR, E.PODate, 103) AS PODate, CONVERT(VARCHAR, DATEADD(DAY, 14, E.PODate), 103) AS PODate_2WeekDelivery, A.ProductionSource_DEV004817 AS ProductionSource, CASE WHEN ISNULL(FYSales_DEV004817, 0) = 0 THEN 0 ELSE ISNULL(((UnitsInStock/ISNULL(I.UnitMeasureQty, 1)) / FYSales_DEV004817) * 12, 0) END AS [SOHinMonths],
		ISNULL(CAST(E.IsDeclined_DEV004817 AS VARCHAR(1)), 2) AS IsDeclined, CONVERT(VARCHAR, E.DateDeclined_DEV004817, 103) AS DateDeclined
		FROM InventoryItem A INNER JOIN InventoryItemDescription B ON A.ItemCode = B.ItemCode INNER JOIN InventoryItemWebOption C ON A.ItemCode = C.ItemCode LEFT JOIN (SELECT A.ItemCode, UnitsInStock, ReorderPoint, ReorderQuantity_C, WarehouseCode FROM InventoryStockTotal A INNER JOIN InventoryItem B ON A.ItemCode = B.ItemCode AND A.WarehouseCode = B.DefaultWarehouse_DEV004817) D ON A.ItemCode = D.ItemCode
		LEFT JOIN (SELECT A.PurchaseOrderCode, ItemCode, UnitMeasure, UnitMeasureQty, QuantityOrdered, DateApproved_DEV004817, IsApproved_DEV004817, PODate, ETA_C, IsDeclined_DEV004817, DateDeclined_DEV004817 FROM SupplierPurchaseOrder A INNER JOIN SupplierPurchaseOrderDetail B ON A.PurchaseOrderCode = B.PurchaseOrderCode WHERE Type = 'Purchase Order' AND IsVoided != '1') AS E ON E.ItemCode = A.ItemCode
		LEFT JOIN SGEShipmentAttachOrders_DEV004817 G ON G.POCode_DEV004817 = E.PurchaseOrderCode LEFT JOIN SGEShipment_DEV004817 H ON H.ShipmentID_DEV004817 = G.ShipmentID_DEV004817
		INNER JOIN InventoryUnitMeasure I ON I.ItemCode = A.ItemCode AND I.DefaultSelling = 1
		LEFT JOIN (SELECT B.ItemCode, SUM(B.QuantityOrdered) AS QuantityOrdered FROM CustomerSalesOrder A INNER JOIN CustomerSalesOrderDetail B ON A.SalesOrderCode = B.SalesOrderCode WHERE OrderStatus = 'Open' AND Type IN ('Sales Order', 'Back Order') GROUP BY B.ItemCode) AS J ON J.ItemCode = A.ItemCode
		WHERE B.LanguageCode = @LanguageCode AND C.WebSiteCode = @WebsiteCode AND A.CostCenter_DEV004817 = @CostCenter AND A.Status = @Status AND ISNULL(IsDeclined_DEV004817, 0) = 1 ORDER BY A.ItemName
	END

	IF @CurrentMonth = '2' 
	BEGIN
		SELECT ROW_NUMBER() OVER (ORDER BY A.Counter) AS Counter, A.ItemCode, A.ItemName, B.ItemDescription, A.IsPlastic_DEV004817 AS IsPlastic, CAST(ISNULL(D.UnitsInStock/ISNULL(I.UnitMeasureQty, E.UnitMeasureQty), 0) AS INT) AS InWH, ISNULL(D.ReorderPoint, 0) AS OrderPoint, CAST(ISNULL(E.QuantityOrdered/CASE WHEN E.UnitMeasure = I.UnitMeasureCode THEN 1 ELSE ISNULL(I.UnitMeasureQty, E.UnitMeasureQty) END, 0) AS INT) AS OnOrder, 
		ISNULL(D.ReorderQuantity_C, 0) AS ReorderQty, CAST(ISNULL(J.QuantityOrdered, 0) + ISNULL(A.BKO_C, 0) AS INT) AS OnBackOrder, E.PurchaseOrderCode, ISNULL(E.UnitMeasure, I.UnitMeasureCode) AS UnitMeasureCode, ISNULL(I.UnitMeasureQty, E.UnitMeasureQty) AS UnitMeasureQty, CONVERT(VARCHAR, E.DateApproved_DEV004817, 103) AS DateApproved, 
		ISNULL(CAST(E.IsApproved_DEV004817 AS VARCHAR(1)), 2) AS IsApproved, CASE WHEN ISNULL(A.ProductionSource_DEV004817, '') = 'Overseas' THEN CONVERT(VARCHAR, EstimatedArrival_DEV004817, 103) WHEN ISNULL(A.ProductionSource_DEV004817, '') != 'Overseas' AND  E.IsApproved_DEV004817 = 1 THEN CONVERT(VARCHAR, E.ETA_C, 103) ELSE NULL END AS ETA, 
		A.DefaultWarehouse_DEV004817 AS WarehouseCode, A.Status, CONVERT(VARCHAR, E.PODate, 103) AS PODate, CONVERT(VARCHAR, DATEADD(DAY, 14, E.PODate), 103) AS PODate_2WeekDelivery, A.ProductionSource_DEV004817 AS ProductionSource, CASE WHEN ISNULL(FYSales_DEV004817, 0) = 0 THEN 0 ELSE ISNULL(((UnitsInStock/ISNULL(I.UnitMeasureQty, 1)) / FYSales_DEV004817) * 12, 0) END AS [SOHinMonths],
		ISNULL(CAST(E.IsDeclined_DEV004817 AS VARCHAR(1)), 2) AS IsDeclined, CONVERT(VARCHAR, E.DateDeclined_DEV004817, 103) AS DateDeclined
		FROM InventoryItem A INNER JOIN InventoryItemDescription B ON A.ItemCode = B.ItemCode INNER JOIN InventoryItemWebOption C ON A.ItemCode = C.ItemCode LEFT JOIN (SELECT A.ItemCode, UnitsInStock, ReorderPoint, ReorderQuantity_C, WarehouseCode FROM InventoryStockTotal A INNER JOIN InventoryItem B ON A.ItemCode = B.ItemCode AND A.WarehouseCode = B.DefaultWarehouse_DEV004817) D ON A.ItemCode = D.ItemCode
		LEFT JOIN (SELECT A.PurchaseOrderCode, ItemCode, UnitMeasure, UnitMeasureQty, QuantityOrdered, DateApproved_DEV004817, IsApproved_DEV004817, PODate, ETA_C, IsDeclined_DEV004817, DateDeclined_DEV004817 FROM SupplierPurchaseOrder A INNER JOIN SupplierPurchaseOrderDetail B ON A.PurchaseOrderCode = B.PurchaseOrderCode WHERE Type = 'Purchase Order' AND IsVoided != '1') AS E ON E.ItemCode = A.ItemCode
		LEFT JOIN SGEShipmentAttachOrders_DEV004817 G ON G.POCode_DEV004817 = E.PurchaseOrderCode LEFT JOIN SGEShipment_DEV004817 H ON H.ShipmentID_DEV004817 = G.ShipmentID_DEV004817
		INNER JOIN InventoryUnitMeasure I ON I.ItemCode = A.ItemCode AND I.DefaultSelling = 1
		LEFT JOIN (SELECT B.ItemCode, SUM(B.QuantityOrdered) AS QuantityOrdered FROM CustomerSalesOrder A INNER JOIN CustomerSalesOrderDetail B ON A.SalesOrderCode = B.SalesOrderCode WHERE OrderStatus = 'Open' AND Type IN ('Sales Order', 'Back Order') GROUP BY B.ItemCode) AS J ON J.ItemCode = A.ItemCode
		WHERE B.LanguageCode = @LanguageCode AND C.WebSiteCode = @WebsiteCode AND A.CostCenter_DEV004817 = @CostCenter AND A.Status = @Status  ORDER BY IsApproved
	END

	IF (@CurrentMonth != '0' AND @CurrentMonth != '1' AND @CurrentMonth != '2' AND @CurrentMonth != '')
	BEGIN
		DECLARE @CurrentDate NVARCHAR(50)
		SELECT @currentDate = EOMONTH(CAST(YEAR(GETDATE()) AS VARCHAR(4)) + '-' + @CurrentMonth + '-01')
		SET @CurrentDate += ' 23:59:00.000' 

		SELECT ROW_NUMBER() OVER (ORDER BY A.Counter) AS Counter, A.ItemCode, A.ItemName, B.ItemDescription, A.IsPlastic_DEV004817 AS IsPlastic, CAST(ISNULL(D.UnitsInStock/ISNULL(I.UnitMeasureQty, E.UnitMeasureQty), 0) AS INT) AS InWH, ISNULL(D.ReorderPoint, 0) AS OrderPoint, CAST(ISNULL(e.QuantityOrdered/CASE WHEN E.UnitMeasure = I.UnitMeasureCode THEN 1 ELSE ISNULL(I.UnitMeasureQty, E.UnitMeasureQty) END, 0) AS INT) AS OnOrder, 
		ISNULL(D.ReorderQuantity_C, 0) AS ReorderQty, CAST(ISNULL(J.QuantityOrdered, 0) + ISNULL(A.BKO_C, 0) AS INT) AS OnBackOrder, E.PurchaseOrderCode, ISNULL(E.UnitMeasure, I.UnitMeasureCode) AS UnitMeasureCode, ISNULL(I.UnitMeasureQty, E.UnitMeasureQty) AS UnitMeasureQty, CONVERT(VARCHAR, E.DateApproved_DEV004817, 103) AS DateApproved, 
		ISNULL(CAST(E.IsApproved_DEV004817 AS VARCHAR(1)), 2) AS IsApproved, CASE WHEN ISNULL(A.ProductionSource_DEV004817, '') = 'Overseas' THEN CONVERT(VARCHAR, EstimatedArrival_DEV004817, 103) WHEN ISNULL(A.ProductionSource_DEV004817, '') != 'Overseas' AND  E.IsApproved_DEV004817 = 1 THEN CONVERT(VARCHAR, E.ETA_C, 103) ELSE NULL END AS ETA, 
		A.DefaultWarehouse_DEV004817 AS WarehouseCode, A.Status, CONVERT(VARCHAR, E.PODate, 103) AS PODate, CONVERT(VARCHAR, DATEADD(DAY, 14, E.PODate), 103) AS PODate_2WeekDelivery, A.ProductionSource_DEV004817 AS ProductionSource, CASE WHEN ISNULL(FYSales_DEV004817, 0) = 0 THEN 0 ELSE ISNULL(((UnitsInStock/ISNULL(I.UnitMeasureQty, 1)) / FYSales_DEV004817) * 12, 0) END AS [SOHinMonths],
		ISNULL(CAST(E.IsDeclined_DEV004817 AS VARCHAR(1)), 2) AS IsDeclined, CONVERT(VARCHAR, E.DateDeclined_DEV004817, 103) AS DateDeclined
		FROM InventoryItem A INNER JOIN InventoryItemDescription B ON A.ItemCode = B.ItemCode INNER JOIN InventoryItemWebOption C ON A.ItemCode = C.ItemCode LEFT JOIN (SELECT A.ItemCode, ISNULL(C.UnitsInStock, 0) AS UnitsInStock, ReorderPoint, ReorderQuantity_C, ISNULL(C.WarehouseCode, A.WarehouseCode) AS WarehouseCode FROM InventoryStockTotal A INNER JOIN InventoryItem B ON A.ItemCode = B.ItemCode AND A.WarehouseCode = B.DefaultWarehouse_DEV004817
		LEFT JOIN (SELECT SUM(Quantity) AS UnitsInStock, ItemCode, WarehouseCode FROM InventoryStock WHERE [Date] <= @CurrentDate GROUP BY ItemCode, WarehouseCode) C ON C.ItemCode = A.ItemCode AND C.WarehouseCode = A.WarehouseCode) D ON A.ItemCode = D.ItemCode
		LEFT JOIN (SELECT A.PurchaseOrderCode, ItemCode, UnitMeasure, UnitMeasureQty, QuantityOrdered, DateApproved_DEV004817, IsApproved_DEV004817, PODate, ETA_C, IsDeclined_DEV004817, DateDeclined_DEV004817 FROM SupplierPurchaseOrder A INNER JOIN SupplierPurchaseOrderDetail B ON A.PurchaseOrderCode = B.PurchaseOrderCode WHERE Type = 'Purchase Order' AND IsVoided != '1') AS E ON E.ItemCode = A.ItemCode
		LEFT JOIN SGEShipmentAttachOrders_DEV004817 G ON G.POCode_DEV004817 = E.PurchaseOrderCode LEFT JOIN SGEShipment_DEV004817 H ON H.ShipmentID_DEV004817 = G.ShipmentID_DEV004817
		INNER JOIN InventoryUnitMeasure I ON I.ItemCode = A.ItemCode AND I.DefaultSelling = 1
		LEFT JOIN (SELECT B.ItemCode, SUM(B.QuantityOrdered) AS QuantityOrdered FROM CustomerSalesOrder A INNER JOIN CustomerSalesOrderDetail B ON A.SalesOrderCode = B.SalesOrderCode WHERE OrderStatus = 'Open' AND Type IN ('Sales Order', 'Back Order') GROUP BY B.ItemCode) AS J ON J.ItemCode = A.ItemCode
		WHERE B.LanguageCode = @LanguageCode AND C.WebSiteCode = @WebsiteCode AND A.CostCenter_DEV004817 = @CostCenter AND A.Status = @Status
		AND CAST(CONVERT(date, E.DateApproved_DEV004817) AS VARCHAR(7)) = CAST(YEAR(getdate()) AS VARCHAR(4)) + '-' + @CurrentMonth ORDER BY IsApproved
	END
END
GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertContactItems]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertContactItems]
GO

CREATE PROCEDURE [dbo].[InsertContactItems]          
@EntityCode NVARCHAR(60)

AS    
  
BEGIN       
	SET NOCOUNT ON;       

	DECLARE @ContactCode NVARCHAR(60)

	SET @EntityCode = @EntityCode

	CREATE TABLE #CustomerItems (ItemCode  NVARCHAR(60))                                            
	CREATE TABLE #CRMContact (ContactCode  NVARCHAR(60))                                            

	INSERT INTO #CustomerItems (ItemCode)
	SELECT InventoryItemCode FROM CustomerItems WHERE CustomerCode = @EntityCode

	INSERT INTO #CRMContact (ContactCode)
	SELECT ContactCode FROM CRMContact WHERE EntityCode = @EntityCode

	WHILE EXISTS (SELECT TOP 1 * FROM #CRMContact)
		BEGIN
		   SELECT TOP 1 @ContactCode = ContactCode FROM #CRMContact

		   INSERT INTO SGEContactItems_DEV004817(UserCreated, DateCreated, UserModified, ContactCode_DEV004817, ItemCode_DEV004817)
		   SELECT 'admin', GETDATE(), 'admin', @ContactCode, ItemCode FROM #CustomerItems WHERE ItemCode NOT IN (SELECT ItemCode_DEV004817 FROM SGEContactItems_DEV004817 WHERE ContactCode_DEV004817 = @ContactCode)
		   DELETE #CRMContact WHERE ContactCode = @ContactCode
		END
END
GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[SOHReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SOHReport]
GO

CREATE PROCEDURE [dbo].[SOHReport]          
@DateFrom DATE,
@DateTo DATE,
@Type INT,
@Status NVARCHAR(60),
@WarehouseCode NVARCHAR(60),
@CostCenter NVARCHAR(100)
AS    
  
BEGIN 
SET NOCOUNT ON; 

WITH CTE AS 
(SELECT distinct BC.InvoiceCode, BC.Invoicedate,A.ItemCode,A.ItemName,Isnull(D.ExtendedDescription, D.ItemDescription) as ItemDescription, (BC.QuantityShipped / Isnull(f.UnitMeasureQty, 1)) as QuantityShipped, G.SalesPrice, ist.InStock as UnitsInStock, ISNULL(A.IsPlastic_DEV004817,0) as IsPlastic, 
ISNULL(A.DefaultWarehouse_DEV004817, @WarehouseCode) as WarehouseCode, 
CASE WHEN A.[Status]='A'THEN 'ACTIVE' 
	 WHEN A.[Status]='P' THEN 'PHASE OUT' 
	 WHEN A.[Status]='D' THEN 'DISCONTINUED' END AS ItemStatus, 
CASE WHEN A.[Status]='A' THEN 'A' 
     WHEN A.[Status]='P' THEN 'B' 
	 WHEN A.[Status]='D' THEN 'C' END AS ItemStatusNum, 
A.FYSales_DEV004817, 
ProductionSource_DEV004817, ISNULL(UndeliveredQuantity, 0) / Isnull(f.UnitMeasureQty, 1) AS UndeliveredQuantity, F.UnitMeasureCode as [UOM], F.UnitMeasureQty, 
A.CostCenter_DEV004817 as [CostCenter]
, Case when Isnull(BC.BillToCode, '1') = '1' then '1' else '0' end AS BillToCode
, Case when Isnull(BC.BillToCode, '1') = '1' then '1' else '0' end AS IsShowAllItems
, A.JulySept2017_C
, A.OctDec2017_C
, A.Jan2018_C
, BC.LineNum
, BCI.ExtPriceRate
, A.YTDPrice_C
, EA.MinOrder
, EA.MaxOrder
, A.JanPrice2018_C
, A.FebPrice2018_C
, A.FebQty2018_C
, A.MarchQty2018_C
, A.AprilQty2018_C
, A.MayQty2018_C
, A.JuneQty2018_C
, Isnull(SO_BKO.SOBKOQty, 0) / Isnull(f.UnitMeasureQty, 1) AS SOBKOQty
, (Isnull(BCA.QuantityShipped20172018, 0) / Isnull(f.UnitMeasureQty, 1)) as QuantityShipped20172018 
FROM InventoryItem A 
LEFT JOIN (SELECT C.InvoiceCode, C.Invoicedate, ((Case when c.type = 'Credit Memo' then (-1 * B.QuantityShipped) else B.QuantityShipped end) * B.UnitMeasureQty) as QuantityShipped, C.BillToCode, B.ItemCode, B.LineNum from CustomerInvoiceDETAIL B 
			INNER JOIN CustomerInvoice C on B.InvoiceCode = C.InvoiceCode And C.IsPosted=1 
			WHERE convert(date, C.InvoiceDate) >= @DateFrom and convert(date, C.InvoiceDate) <= @DateTo) BC ON A.ItemCode = BC.ItemCode 
			
			--Sales 2017 2018
LEFT JOIN (SELECT Sum((Case when c.type = 'Credit Memo' then (-1 * B.QuantityShipped) else B.QuantityShipped end) * B.UnitMeasureQty) as QuantityShipped20172018, B.ItemCode from CustomerInvoiceDETAIL B 
			INNER JOIN CustomerInvoice C on B.InvoiceCode = C.InvoiceCode And C.IsPosted=1 
			WHERE convert(date, C.InvoiceDate) >= '7/1/2017' and convert(date, C.InvoiceDate) <= '6/30/2018' 
			GROUP BY B.ItemCode) BCA ON A.ItemCode = BCA.ItemCode 


LEFT JOIN ( SELECT B.ItemCode, SUM(Case when c.type = 'Credit Memo' then (-1 * b.ExtPriceRate) else b.ExtPriceRate end) ExtPriceRate from CustomerInvoiceDETAIL B 
			INNER JOIN CustomerInvoice C on B.InvoiceCode = C.InvoiceCode And C.IsPosted=1 
			WHERE convert(date, C.InvoiceDate) >= @DateFrom and convert(date, C.InvoiceDate) <= @DateTo 
			GROUP BY B.ItemCode
			) BCI ON A.ItemCode = BCI.ItemCode 
LEFT JOIN InventoryItemDescription D on A.ItemCode = D.ItemCode AND D.LanguageCode = (select top 1 CompanyLanguage from SystemCompanyInformation) 
LEFT JOIN (SELECT ItemCode, Max(Isnull(ReorderPoint, 0)) as MinOrder, Max(Isnull(ReorderQuantity_C, 0)) as MaxOrder from InventoryStockTotal GROUP BY ItemCode) EA on A.ItemCode = EA.ItemCode 
LEFT JOIN InventoryUnitMeasure F ON A.ItemCode = F.ItemCode AND F.DefaultSelling=1 
LEFT JOIN InventoryPricingLevel G ON A.ItemCode = G.ItemCode and F.UnitMeasureCode = G.UnitMeasureCode 
LEFT JOIN ( Select ItemCode, Sum(UndeliveredQuantity) as UndeliveredQuantity from 
			( 
			
				SELECT spod.ItemCode,spod.UnitMeasure, (cast((spod.QuantityOrdered - spod.QuantityReceived) as int)) * spod.UnitMeasureQty as UndeliveredQuantity 
				FROM SupplierPurchaseOrder spo INNER JOIN SupplierPurchaseOrderDetail spod on spo.PurchaseOrderCode = spod.PurchaseOrderCode 
				WHERE spo.[Type] = 'Purchase Order' and spod.QuantityOrdered > spod.QuantityReceived and spo.IsVoided = 0 and spo.IsApproved_DEV004817 = 1 and convert(date, PODate) <= @DateTo 
			 ) a 
			 group by a.ItemCode) po on a.ItemCode = po.ItemCode 
LEFT JOIN (SELECT ItemCode, Sum(OrigQty) as InStock 
			FROM ( SELECT ItemCode, OrigQty 
					FROM InventoryCostHistory 
					WHERE convert(date, PurchaseReceiptDate) <= @DateTo 
					UNION ALL 
					SELECT ItemCode, (Qty * -1) OutQty FROM InventoryCostAllocation 
					WHERE convert(date, SalesInvoiceDate) <= @DateTo) a GROUP BY ItemCode ) ist on a.ItemCode = ist.ItemCode 
LEFT JOIN ( 
select b.ItemCode, Sum(b.QuantityOrdered * b.UnitMeasureQty) as SOBKOQty from customersalesorder a  
inner join CustomerSalesOrderDetail b on a.SalesOrderCode = b.SalesOrderCode and a.OrderStatus = 'Open' and a.[Type] <> 'RMA' 
group by b.ItemCode 
) SO_BKO on a.ItemCode = SO_BKO.ItemCode 
WHERE (A.CostCenter_DEV004817=@CostCenter) AND A.[Status] <> 'D') 

SELECT * 
, case when (Select top 1 spo.IsDeclined_DEV004817 from SupplierPurchaseOrder spo inner join SupplierPurchaseOrderDetail spod on spo.PurchaseOrderCode = spod.PurchaseOrderCode where spod.ItemCode = m.ItemCode and spo.OrderStatus <> 'Completed' order by spo.Counter desc) = 1 then 'Declined' 
	else case when (Select top 1 spo.IsApproved_DEV004817 from SupplierPurchaseOrder spo inner join SupplierPurchaseOrderDetail spod on spo.PurchaseOrderCode = spod.PurchaseOrderCode where spod.ItemCode = m.ItemCode and spo.Isvoided = 0 and spo.OrderStatus <> 'Completed' order by spo.Counter desc) = 1 then 'Approved' 
		else case when Isnull((Select top 1 1 from SupplierPurchaseOrder spo inner join SupplierPurchaseOrderDetail spod on spo.PurchaseOrderCode = spod.PurchaseOrderCode where spod.ItemCode = m.ItemCode and spod.QuantityOrdered > spod.QuantityReceived and spo.Isvoided = 0 and spo.OrderStatus <> 'Completed'), 0) > 0 then 'Waiting for approval' 
			else '' end 
		end 
	end ReorderStatus 
, IsNull((Select top 1 spo.PurchaseOrderCode from SupplierPurchaseOrder spo inner join SupplierPurchaseOrderDetail spod on spo.PurchaseOrderCode = spod.PurchaseOrderCode where spod.ItemCode = m.ItemCode and spo.Isvoided = 0 and spo.OrderStatus <> 'Completed' order by spo.Counter desc), '') as PONumber INTO #Result
FROM 
( 
SELECT ItemCode, 
ItemName,  
ItemDescription, 
ISNULL(SalesPrice,0) as [Price], 
ISNULL(QPivot.[1],0) as [ThirdQtrSales],
ISNULL(QPivot.[2],0) As [FourthQtrSales],
ISNULL(QPivot.[3],0) As [FirstQtrSales],
ISNULL(QPivot.[4],0) As [SecondQtrSales]
, (ISNULL(QPivot.[1],0) + ISNULL(QPivot.[2],0) + ISNULL(QPivot.[3],0) + ISNULL(QPivot.[4],0) ) AS [YTDSales]
, (Isnull(ExtPriceRate, 0)) as YTDSalesValue
--ISNULL(QPivot.[1],0) As [1st Qtr Sales], 
--ISNULL(QPivot.[2],0) As [2nd Qtr Sales], 
--ISNULL(QPivot.[3],0) As [3rd Qtr Sales], 
--ISNULL(QPivot.[4],0) As [4th Qtr Sales], 
--(ISNULL(QPivot.[1],0) + ISNULL(QPivot.[2],0) + ISNULL(QPivot.[3],0) + ISNULL(QPivot.[4],0) ) AS [YTD Sales], 
--(Isnull(ExtPriceRate, 0)) as ExtPriceRate
, BillToCode
, ISNULL(FYSales_DEV004817,0) AS [FYSales]
, ROUND(ISNULL((UnitsInStock / Isnull(UnitMeasureQty, 0)),0), 0) AS [SOHUnits]
, UOM
, CASE WHEN ISNULL(FYSales_DEV004817, 0) = 0 THEN 0 ELSE ISNULL(((UnitsInStock/UnitMeasureQty) / FYSales_DEV004817) * 12,0) END as [SOHinMonths]
, ISNULL(UndeliveredQuantity,0) as [StockOnOrderSOO]
, ISNULL((UndeliveredQuantity * SalesPrice),0) as [StockOnOrderSOOValue]
, CASE WHEN ISNULL(FYSales_DEV004817, 0) = 0 THEN 0 ELSE ISNULL((((UndeliveredQuantity + (UnitsInStock / Isnull(UnitMeasureQty, 1))) / Isnull(FYSales_DEV004817, 1)) * 12),0) END as [SOHSOOinMonths]
, ISNULL((((UnitsInStock/UnitMeasureQty) + UndeliveredQuantity) * SalesPrice),0) as [Value]
, ProductionSource_DEV004817 as [ProductionSource]
, UndeliveredQuantity
, CostCenter
, ItemStatus
, IsPlastic
, WarehouseCode
, IsShowAllItems
, ItemStatusNum
, MinOrder
, MaxOrder
, SOBKOQty
, (ISNULL(SalesPrice,0) * ROUND((ISNULL((UnitsInStock / Isnull(UnitMeasureQty, 0)),0)), 0)) as SOHValue
, (Isnull(QuantityShipped20172018, 0) + Isnull(JulySept2017_C, 0) + Isnull(OctDec2017_C, 0) + Isnull(Jan2018_C, 0) + Isnull(FebQty2018_C, 0) + Isnull(MarchQty2018_C, 0) + Isnull(AprilQty2018_C, 0) + Isnull(MayQty2018_C, 0) + Isnull(JuneQty2018_C, 0)  ) as QuantityShipped20172018 
FROM (SELECT ItemCode, ItemName, ItemDescription, BillToCode,SalesPrice,UnitsInStock,UOM,FYSales_DEV004817, ProductionSource_DEV004817, UndeliveredQuantity,CostCenter,ItemStatus,IsPlastic,WarehouseCode,UnitMeasureQty,IsShowAllItems, DATEPART(QUARTER, InvoiceDate) [Quarter], QuantityShipped, JulySept2017_C, OctDec2017_C, Jan2018_C, ExtPriceRate, ItemStatusNum, YTDPrice_C, MinOrder, MaxOrder, JanPrice2018_C, FebPrice2018_C, SOBKOQty, QuantityShipped20172018, FebQty2018_C, MarchQty2018_C, AprilQty2018_C, MayQty2018_C, JuneQty2018_C FROM CTE) ssss  PIVOT( SUM(QuantityShipped) FOR QUARTER IN ([1],[2],[3],[4])) AS QPivot 
) m 

SELECT SUM(YTDSales) AS YTDSales, SUM(YTDSalesValue) AS YTDSalesValue, SUM(SOHUnits) AS SOHUnits, SUM(StockOnOrderSOO) AS StockOnOrderSOO, SUM(StockOnOrderSOOValue) AS StockOnOrderSOOValue FROM #Result

SELECT * FROM #Result ORDER BY ItemName ASC

END

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[AdminSOHReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AdminSOHReport]
GO

CREATE PROCEDURE [dbo].[AdminSOHReport]          
@DateFrom DATE,
@DateTo DATE,
@Type NVARCHAR(60),
@Status NVARCHAR(60),
@WarehouseCode NVARCHAR(60),
@CostCenter NVARCHAR(100)
AS    
  
BEGIN 
SET NOCOUNT ON; 

WITH CTE AS 
(SELECT distinct BC.InvoiceCode, BC.Invoicedate,A.ItemCode,A.ItemName, ItemType, Isnull(D.ExtendedDescription, D.ItemDescription) as ItemDescription, (BC.QuantityShipped / Isnull(f.UnitMeasureQty, 1)) as QuantityShipped, G.SalesPrice, ist.InStock as UnitsInStock, ISNULL(A.IsPlastic_DEV004817,0) as IsPlastic, 
ISNULL(A.DefaultWarehouse_DEV004817, @WarehouseCode) as WarehouseCode, 
CASE WHEN A.[Status]='A'THEN 'ACTIVE' 
	 WHEN A.[Status]='P' THEN 'PHASE OUT' 
	 WHEN A.[Status]='D' THEN 'DISCONTINUED' END AS ItemStatus, 
CASE WHEN A.[Status]='A' THEN 'A' 
     WHEN A.[Status]='P' THEN 'B' 
	 WHEN A.[Status]='D' THEN 'C' END AS ItemStatusNum, 
A.FYSales_DEV004817, 
ProductionSource_DEV004817, ISNULL(UndeliveredQuantity, 0) / Isnull(f.UnitMeasureQty, 1) AS UndeliveredQuantity, F.UnitMeasureCode as [UOM], F.UnitMeasureQty, 
A.CostCenter_DEV004817 as [CostCenter]
, Case when Isnull(BC.BillToCode, '1') = '1' then '1' else '0' end AS BillToCode
, Case when Isnull(BC.BillToCode, '1') = '1' then '1' else '0' end AS IsShowAllItems
, A.JulySept2017_C
, A.OctDec2017_C
, A.Jan2018_C
, BC.LineNum
, BCI.ExtPriceRate
, A.YTDPrice_C
, EA.MinOrder
, EA.MaxOrder
, A.JanPrice2018_C
, A.FebPrice2018_C
, A.FebQty2018_C
, A.MarchQty2018_C
, A.AprilQty2018_C
, A.MayQty2018_C
, A.JuneQty2018_C
, Isnull(SO_BKO.SOBKOQty, 0) / Isnull(f.UnitMeasureQty, 1) AS SOBKOQty
, (Isnull(BCA.QuantityShipped20172018, 0) / Isnull(f.UnitMeasureQty, 1)) as QuantityShipped20172018 
FROM InventoryItem A 
LEFT JOIN (SELECT C.InvoiceCode, C.Invoicedate, ((Case when c.type = 'Credit Memo' then (-1 * B.QuantityShipped) else B.QuantityShipped end) * B.UnitMeasureQty) as QuantityShipped, C.BillToCode, B.ItemCode, B.LineNum from CustomerInvoiceDETAIL B 
			INNER JOIN CustomerInvoice C on B.InvoiceCode = C.InvoiceCode And C.IsPosted=1 
			WHERE convert(date, C.InvoiceDate) >= @DateFrom and convert(date, C.InvoiceDate) <= @DateTo) BC ON A.ItemCode = BC.ItemCode AND A.ItemType = CASE WHEN @Type = '0' THEN ItemType WHEN  @Type = 'All' THEN ItemType ELSE @Type END
			
			--Sales 2017 2018
LEFT JOIN (SELECT Sum((Case when c.type = 'Credit Memo' then (-1 * B.QuantityShipped) else B.QuantityShipped end) * B.UnitMeasureQty) as QuantityShipped20172018, B.ItemCode from CustomerInvoiceDETAIL B 
			INNER JOIN CustomerInvoice C on B.InvoiceCode = C.InvoiceCode And C.IsPosted=1 
			WHERE convert(date, C.InvoiceDate) >= '7/1/2017' and convert(date, C.InvoiceDate) <= '6/30/2018' 
			GROUP BY B.ItemCode) BCA ON A.ItemCode = BCA.ItemCode AND A.ItemType = CASE WHEN @Type = '0' THEN ItemType WHEN  @Type = 'All' THEN ItemType ELSE @Type END 


LEFT JOIN ( SELECT B.ItemCode, SUM(Case when c.type = 'Credit Memo' then (-1 * b.ExtPriceRate) else b.ExtPriceRate end) ExtPriceRate from CustomerInvoiceDETAIL B 
			INNER JOIN CustomerInvoice C on B.InvoiceCode = C.InvoiceCode And C.IsPosted=1 
			WHERE convert(date, C.InvoiceDate) >= @DateFrom and convert(date, C.InvoiceDate) <= @DateTo 
			GROUP BY B.ItemCode
			) BCI ON A.ItemCode = BCI.ItemCode  AND A.ItemType = CASE WHEN @Type = '0' THEN ItemType WHEN  @Type = 'All' THEN ItemType ELSE @Type END
LEFT JOIN InventoryItemDescription D ON A.ItemCode = D.ItemCode AND D.LanguageCode = (select top 1 CompanyLanguage from SystemCompanyInformation) AND A.ItemType = CASE WHEN @Type = '0' THEN ItemType WHEN  @Type = 'All' THEN ItemType ELSE @Type END
LEFT JOIN (SELECT ItemCode, Max(Isnull(ReorderPoint, 0)) as MinOrder, Max(Isnull(ReorderQuantity_C, 0)) as MaxOrder from InventoryStockTotal GROUP BY ItemCode) EA on A.ItemCode = EA.ItemCode AND A.ItemType = CASE WHEN @Type = '0' THEN ItemType WHEN  @Type = 'All' THEN ItemType ELSE @Type END 
LEFT JOIN InventoryUnitMeasure F ON A.ItemCode = F.ItemCode AND F.DefaultSelling=1 AND A.ItemType = CASE WHEN @Type = '0' THEN ItemType WHEN  @Type = 'All' THEN ItemType ELSE @Type END 
LEFT JOIN InventoryPricingLevel G ON A.ItemCode = G.ItemCode and F.UnitMeasureCode = G.UnitMeasureCode AND A.ItemType = CASE WHEN @Type = '0' THEN ItemType WHEN  @Type = 'All' THEN ItemType ELSE @Type END 
LEFT JOIN ( Select ItemCode, Sum(UndeliveredQuantity) as UndeliveredQuantity from 
			( 
				SELECT spod.ItemCode,spod.UnitMeasure, (cast((spod.QuantityOrdered - spod.QuantityReceived) as int)) * spod.UnitMeasureQty as UndeliveredQuantity 
				FROM SupplierPurchaseOrder spo INNER JOIN SupplierPurchaseOrderDetail spod on spo.PurchaseOrderCode = spod.PurchaseOrderCode 
				WHERE spo.[Type] = 'Purchase Order' and spod.QuantityOrdered > spod.QuantityReceived and spo.IsVoided = 0 and spo.IsApproved_DEV004817 = 1 and convert(date, PODate) <= @DateTo 
			 ) a 
			 group by a.ItemCode) po on a.ItemCode = po.ItemCode AND A.ItemType = CASE WHEN @Type = '0' THEN ItemType WHEN  @Type = 'All' THEN ItemType ELSE @Type END
LEFT JOIN (SELECT ItemCode, Sum(OrigQty) as InStock 
			FROM ( SELECT ItemCode, OrigQty 
					FROM InventoryCostHistory 
					WHERE convert(date, PurchaseReceiptDate) <= @DateTo 
					UNION ALL 
					SELECT ItemCode, (Qty * -1) OutQty FROM InventoryCostAllocation 
					WHERE convert(date, SalesInvoiceDate) <= @DateTo) a GROUP BY ItemCode ) ist on a.ItemCode = ist.ItemCode AND A.ItemType = CASE WHEN @Type = '0' THEN ItemType WHEN  @Type = 'All' THEN ItemType ELSE @Type END 
LEFT JOIN ( 
select b.ItemCode, Sum(b.QuantityOrdered * b.UnitMeasureQty) as SOBKOQty from customersalesorder a  
inner join CustomerSalesOrderDetail b on a.SalesOrderCode = b.SalesOrderCode and a.OrderStatus = 'Open' and a.[Type] <> 'RMA' 
group by b.ItemCode 
) SO_BKO on a.ItemCode = SO_BKO.ItemCode AND A.ItemType = CASE WHEN @Type = '0' THEN ItemType WHEN  @Type = 'All' THEN ItemType ELSE @Type END 
--WHERE (A.CostCenter_DEV004817 = CASE WHEN @CostCenter = 'All' THEN A.CostCenter_DEV004817 ELSE @CostCenter END) AND A.[Status] <> 'D' AND A.ItemType = CASE WHEN @Type = '0' THEN A.ItemType WHEN  @Type = 'All' THEN A.ItemType ELSE @Type END)
WHERE A.[Status] <> 'D' AND A.ItemType = CASE WHEN @Type = '0' THEN A.ItemType WHEN  @Type = 'All' THEN A.ItemType ELSE @Type END AND A.CostCenter_DEV004817 = CASE WHEN @CostCenter = 'All' THEN A.CostCenter_DEV004817 ELSE @CostCenter END)

SELECT * 
, case when (Select top 1 spo.IsDeclined_DEV004817 from SupplierPurchaseOrder spo inner join SupplierPurchaseOrderDetail spod on spo.PurchaseOrderCode = spod.PurchaseOrderCode where spod.ItemCode = m.ItemCode and spo.OrderStatus <> 'Completed' order by spo.Counter desc) = 1 then 'Declined' 
	else case when (Select top 1 spo.IsApproved_DEV004817 from SupplierPurchaseOrder spo inner join SupplierPurchaseOrderDetail spod on spo.PurchaseOrderCode = spod.PurchaseOrderCode where spod.ItemCode = m.ItemCode and spo.Isvoided = 0 and spo.OrderStatus <> 'Completed' order by spo.Counter desc) = 1 then 'Approved' 
		else case when Isnull((Select top 1 1 from SupplierPurchaseOrder spo inner join SupplierPurchaseOrderDetail spod on spo.PurchaseOrderCode = spod.PurchaseOrderCode where spod.ItemCode = m.ItemCode and spod.QuantityOrdered > spod.QuantityReceived and spo.Isvoided = 0 and spo.OrderStatus <> 'Completed'), 0) > 0 then 'Waiting for approval' 
			else '' end 
		end 
	end ReorderStatus 
, IsNull((Select top 1 spo.PurchaseOrderCode from SupplierPurchaseOrder spo inner join SupplierPurchaseOrderDetail spod on spo.PurchaseOrderCode = spod.PurchaseOrderCode where spod.ItemCode = m.ItemCode and spo.Isvoided = 0 and spo.OrderStatus <> 'Completed' order by spo.Counter desc), '') as PONumber INTO #Result
FROM 
( 
SELECT ItemCode, 
ItemName,  
ItemType,
ItemDescription, 
ISNULL(SalesPrice,0) as [Price], 
ISNULL(QPivot.[1],0) as [ThirdQtrSales],
ISNULL(QPivot.[2],0) As [FourthQtrSales],
ISNULL(QPivot.[3],0) As [FirstQtrSales],
ISNULL(QPivot.[4],0) As [SecondQtrSales]
, (ISNULL(QPivot.[1],0) + ISNULL(QPivot.[2],0) + ISNULL(QPivot.[3],0) + ISNULL(QPivot.[4],0) ) AS [YTDSales]
, (Isnull(ExtPriceRate, 0)) as YTDSalesValue
, BillToCode
, ISNULL(FYSales_DEV004817,0) AS [FYSales]
, ROUND(ISNULL((UnitsInStock / Isnull(UnitMeasureQty, 0)),0), 0) AS [SOHUnits]
, UOM
, CASE WHEN ISNULL(FYSales_DEV004817, 0) = 0 THEN 0 ELSE ISNULL(((UnitsInStock/UnitMeasureQty) / FYSales_DEV004817) * 12,0) END as [SOHinMonths]
, ISNULL(UndeliveredQuantity,0) as [StockOnOrderSOO]
, ISNULL((UndeliveredQuantity * SalesPrice),0) as [StockOnOrderSOOValue]
, CASE WHEN ISNULL(FYSales_DEV004817, 0) = 0 THEN 0 ELSE ISNULL((((UndeliveredQuantity + (UnitsInStock / Isnull(UnitMeasureQty, 1))) / Isnull(FYSales_DEV004817, 1)) * 12),0) END as [SOHSOOinMonths]
, ISNULL((((UnitsInStock/UnitMeasureQty) + UndeliveredQuantity) * SalesPrice),0) as [Value]
, ProductionSource_DEV004817 as [ProductionSource]
, UndeliveredQuantity
, CostCenter
, ItemStatus
, IsPlastic
, WarehouseCode
, IsShowAllItems
, ItemStatusNum
, MinOrder
, MaxOrder
, SOBKOQty
, (ISNULL(SalesPrice,0) * ROUND((ISNULL((UnitsInStock / Isnull(UnitMeasureQty, 0)),0)), 0)) as SOHValue
, (Isnull(QuantityShipped20172018, 0) + Isnull(JulySept2017_C, 0) + Isnull(OctDec2017_C, 0) + Isnull(Jan2018_C, 0) + Isnull(FebQty2018_C, 0) + Isnull(MarchQty2018_C, 0) + Isnull(AprilQty2018_C, 0) + Isnull(MayQty2018_C, 0) + Isnull(JuneQty2018_C, 0)  ) as QuantityShipped20172018 
FROM (SELECT ItemCode, ItemName, ItemType, ItemDescription, BillToCode,SalesPrice,UnitsInStock,UOM,FYSales_DEV004817, ProductionSource_DEV004817, UndeliveredQuantity,CostCenter,ItemStatus,IsPlastic,WarehouseCode,UnitMeasureQty,IsShowAllItems, DATEPART(QUARTER, InvoiceDate) [Quarter], QuantityShipped, JulySept2017_C, OctDec2017_C, Jan2018_C, ExtPriceRate, ItemStatusNum, YTDPrice_C, MinOrder, MaxOrder, JanPrice2018_C, FebPrice2018_C, SOBKOQty, QuantityShipped20172018, FebQty2018_C, MarchQty2018_C, AprilQty2018_C, MayQty2018_C, JuneQty2018_C FROM CTE) ssss  PIVOT( SUM(QuantityShipped) FOR QUARTER IN ([1],[2],[3],[4])) AS QPivot 
) m WHERE ItemType = CASE WHEN @Type = '0' THEN ItemType WHEN  @Type = 'All' THEN ItemType ELSE @Type END

SELECT ISNULL(SUM(YTDSales), 0) AS YTDSales, ISNULL(SUM(YTDSalesValue), 0) AS YTDSalesValue, ISNULL(SUM(SOHUnits), 0) AS SOHUnits, ISNULL(SUM(StockOnOrderSOO), 0) AS StockOnOrderSOO, ISNULL(SUM(StockOnOrderSOOValue), 0) AS StockOnOrderSOOValue FROM #Result

SELECT * FROM #Result ORDER BY ItemName ASC

END

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEcommerceShoppingCart]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetEcommerceShoppingCart]
GO

CREATE PROCEDURE [dbo].[GetEcommerceShoppingCart]                                            
 @CartType  TINYINT, -- ShoppingCart = 0, WishCart = 1, RecurringCart = 2, GiftRegistryCart = 3                                              
 @CustomerID  NVARCHAR(30),                                        
 @WebsiteCode NVARCHAR(30),                                            
 @WarehouseCode NVARCHAR(30),                                            
 @LanguageCode NVARCHAR(30),                                      
 @ContactCode NVARCHAR(30),      
 @UserCode NVARCHAR(30)                                      
AS         

BEGIN                                              
 SET NOCOUNT ON;   
     
 /** ShoppingCartUnitMeasure **/    
 CREATE TABLE #ShoppingCartUnitMeasure (CartId UNIQUEIDENTIFIER, ItemCode NVARCHAR(30), UnitMeasureCode NVARCHAR(30),    
          UnitMeasureQty DECIMAL(18,6), WeightInPounds NUMERIC(18,6), WeightInKilograms NUMERIC(18,6),    
          IsPrePacked BIT, IsOverSized BIT, OverSizedShippingMethodCode NVARCHAR(30))    
     
 INSERT INTO #ShoppingCartUnitMeasure (CartId, ItemCode, UnitMeasureCode, UnitMeasureQty, WeightInPounds,     
            WeightInKilograms, IsPrePacked, IsOverSized, OverSizedShippingMethodCode)    
 SELECT ESC.ShoppingCartRecGUID, ESC.ItemCode, ESC.UnitMeasureCode, UnitMeasureQty,    
 CASE WHEN II.ItemType = 'Kit' THEN KI.WeightInPounds    
   ELSE ISNULL(IUM.WeightInPounds, 0) END AS WeightInPounds,     
 CASE WHEN ItemType = 'Kit' THEN KI.WeightInKilograms    
   ELSE ISNULL(IUM.WeightInKilograms, 0) END AS WeightInKilograms,    
 IsPrePacked, IsOverSized, OverSizedShippingMethodCode    
 FROM dbo.EcommerceShoppingCart ESC    
 INNER JOIN dbo.InventoryItem II ON II.ItemCode = ESC.ItemCode    
 INNER JOIN dbo.InventoryUnitMeasure IUM ON IUM.ItemCode = ESC.ItemCode AND IUM.UnitMeasureCode= ESC.UnitMeasureCode    
 LEFT JOIN (    
             SELECT CartId, SUM(ISNULL(WeightInPounds,0)) AS WeightInPounds, SUM(ISNULL(WeightInKilograms,0)) AS WeightInKilograms, UnitMeasureCode, ItemKitCode    
             FROM dbo.EcommerceKitCart EKC    
    INNER JOIN dbo.InventoryUnitMeasure IUM ON EKC.ItemCode = IUM.ItemCode    
    GROUP BY EKC.CartId, EKC.ItemKitCode, UnitMeasureCode    
   ) KI ON KI.ItemKitCode = IUM.ItemCode AND KI.UnitMeasureCode = IUM.UnitMeasureCode AND KI.CartId = ESC.ShoppingCartRecGUID    
 WHERE esc.CustomerCode = @CustomerID and ESC.ContactCode = @ContactCode AND Esc.WebSiteCode = @WebsiteCode    
 /****/    
                             
 CREATE TABLE #InventoryFreeStock (ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), InStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))  
 CREATE TABLE #AvailableItems (ItemCode  NVARCHAR(50), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), InStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))  
 DECLARE @AdditionalJoin NVARCHAR(1000)                               
 SET @AdditionalJoin = ''                               
                                       
 DECLARE @AdditionalField NVARCHAR(100)                                           
 DECLARE @SqlQuery NVARCHAR(MAX)                             
 DECLARE @ShowInventoryFromAllWarehouses BIT  
 DECLARE @HideOutOfStockProducts BIT  
 DECLARE @HNDelivery NVARCHAR(60)
 DECLARE @HNPFCartoons NVARCHAR(60)
 DECLARE @HNPFSatchel NVARCHAR(60)
                             
 SET @ShowInventoryFromAllWarehouses = 0                                                                                               
 SELECT @ShowInventoryFromAllWarehouses = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'ShowInventoryFromAllWarehouses' AND WebsiteCode = @WebsiteCode  
 SELECT @HideOutOfStockProducts = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'HideOutOfStockProducts' AND WebSiteCode = @WebSiteCode   
 SELECT @HNDelivery =  ConfigValue FROM EcommerceAppConfig WITH (NOLOCK) WHERE Name = 'custom.hn.delivery.item' AND WebSiteCode = @WebSiteCode   
 SELECT @HNPFCartoons =  ConfigValue FROM EcommerceAppConfig WITH (NOLOCK) WHERE Name = 'custom.hn.pallet.fee.cartons.item' AND WebSiteCode = @WebSiteCode   
 SELECT @HNPFSatchel =  ConfigValue FROM EcommerceAppConfig WITH (NOLOCK) WHERE Name = 'custom.hn.pallet.fee.satchel.item' AND WebSiteCode = @WebSiteCode   
     
 IF @HideOutOfStockProducts = 1  
 BEGIN  
 INSERT INTO #AvailableItems (ItemCode, WarehouseCode, FreeStock, InStock, UnitMeasureCode, POStatus)                                              
 SELECT eistv.ItemCode, eistv.WarehouseCode, FreeStock, UnitsInStock, eistv.UnitMeasureCode, OrderStatus FROM EcommerceInventoryStockTotalView eistv  
 LEFT JOIN SupplierPurchaseOrderDetail spod ON spod.ItemCode = eistv.ItemCode AND spod.WarehouseCode = eistv.WarehouseCode AND spod.UnitMeasure = eistv.UnitMeasureCode    
 LEFT JOIN SupplierPurchaseOrder spo ON spo.PurchaseOrderCode = spod.PurchaseOrderCode  
 INNER JOIN EcommerceShoppingCart ES ON ES.ItemCode = eistv.ItemCode AND ES.UnitMeasureCode = eistv.UnitMeasureCode  
 WHERE CustomerCode = @CustomerID AND ContactCode = @ContactCode  
 END  
 ELSE  
 BEGIN  
 INSERT INTO #AvailableItems (ItemCode, WarehouseCode, FreeStock, InStock, UnitMeasureCode, POStatus)  
 SELECT DISTINCT EISTV.ItemCode, WarehouseCode, FreeStock, UnitsInStock, EISTV.UnitMeasureCode, NULL FROM EcommerceInventoryStockTotalView EISTV  
 INNER JOIN EcommerceShoppingCart ES ON ES.ItemCode = EISTV.ItemCode AND ES.UnitMeasureCode = EISTV.UnitMeasureCode  
 WHERE CustomerCode = @CustomerID AND ContactCode = @ContactCode  
 END  
 IF @ShowInventoryFromAllWarehouses = 1  
 BEGIN  
 IF @HideOutOfStockProducts = 1  
 BEGIN  
  ;WITH cte_InventoryFreeStock   
  AS  
  (  
   SELECT ItemCode, FreeStock, InStock, UnitMeasureCode, POStatus,  
   ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum  
   FROM #AvailableItems  
  )  
  INSERT INTO #InventoryFreeStock (ItemCode, FreeStock, InStock, UnitMeasureCode, POStatus)  
  SELECT ItemCode, SUM(FreeStock) AS FreeStock, SUM(InStock) AS InStock, UnitMeasureCode, POStatus  
  FROM cte_InventoryFreeStock  
  WHERE RowNum = 1  
  GROUP BY ItemCode, UnitMeasureCode, POStatus  
 END  
 ELSE  
 BEGIN  
 ;WITH cte_InventoryFreeStock2   
  AS  
  (  
   SELECT ItemCode, FreeStock, InStock, UnitMeasureCode, POStatus,  
   ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum  
   FROM #AvailableItems  
  )  
   
  INSERT INTO #InventoryFreeStock(ItemCode, FreeStock, InStock, UnitMeasureCode, POStatus)  
    
  SELECT ItemCode, SUM(FreeStock) AS FreeStock, SUM(InStock) AS InStock, UnitMeasureCode, POStatus  
    
  FROM cte_InventoryFreeStock2  
  WHERE RowNum = 1           
  GROUP BY ItemCode, UnitMeasureCode, POStatus  
 END  
   
  SET @AdditionalJoin += ' LEFT OUTER JOIN #InventoryFreeStock IFS WITH (NOLOCK) ON IFS.ItemCode = I.ItemCode AND IFS.UnitMeasureCode = SC.UnitMeasureCode '            
 END  
 ELSE  
 BEGIN  
 IF @HideOutOfStockProducts = 1  
 BEGIN  
  ;WITH cte_InventoryFreeStock   
  AS  
  (  
   SELECT ItemCode, FreeStock, InStock, WarehouseCode, UnitMeasureCode, POStatus,  
   ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum  
   FROM #AvailableItems  
   WHERE WarehouseCode = @WarehouseCode  
  )  
  INSERT INTO #InventoryFreeStock (ItemCode, FreeStock, InStock, UnitMeasureCode, POStatus)  
  SELECT ItemCode, SUM(FreeStock) AS FreeStock, SUM(InStock) AS InStock, UnitMeasureCode, POStatus  
  FROM cte_InventoryFreeStock  
  WHERE RowNum = 1  
  GROUP BY ItemCode, UnitMeasureCode, POStatus  
 END  
 ELSE  
 BEGIN  
  INSERT INTO #InventoryFreeStock(ItemCode, FreeStock, InStock, UnitMeasureCode, POStatus)  
  SELECT ItemCode, SUM(FreeStock) AS FreeStock, SUM(InStock) AS InStock, UnitMeasureCode, NULL FROM #AvailableItems  
  WHERE WarehouseCode = @WarehouseCode  
  GROUP BY ItemCode, UnitMeasureCode   
 END  
 SET @AdditionalJoin += ' LEFT OUTER JOIN #inventoryfreestock IFS WITH (NOLOCK) ON IFS.ItemCode = I.ItemCode AND IFS.UnitMeasureCode = SCUM.UnitMeasureCode '                                 
 END  
SET @SqlQuery =                                      
'SELECT sc.ShoppingCartRecGUID, SC.ItemCode, SC.ProductPrice, SC.ProductExtPrice, SC.ProductTax ,i.ItemName, i.Status, iid.ItemDescription, SC.Notes, SC.CartType, SC.ShoppingCartRecID, SC.Quantity, SC.TextOption, SC.CreatedOn,      
SC.BillingAddressID AS ShoppingCartBillingAddressID, ShippingAddressCounter, SC.ShippingAddressID AS ShoppingCartShippingAddressID, SC.ShippingMethodID, SC.ShippingMethod, C.EMail, c.Notes AS OrderNotes, C.CouponCode,      
SC.RealTimeRateID, i.Counter AS ItemCounter, i.ItemType, sc.UnitMeasureCode, SCUM.UnitMeasureQty, iwo.MinOrderQuantity, sc.GiftRegistryID, sc.RegistryItemCode, i.IsCBN, i.CBNItemID,  
CASE      
WHEN ' + QUOTENAME(@UserCode, '''') +' IS NULL THEN 0      
WHEN ' + QUOTENAME(@UserCode, '''') +' IS NOT NULL THEN (      
 SELECT CASE      
 WHEN sup.UnitMeasureSystem = ''English'' THEN IsNull(SCUM.WeightInPounds,0)      
    WHEN sup.UnitMeasureSystem = ''Metric'' THEN IsNull(SCUM.WeightInKilograms,0)      
    ELSE 0      
    END      
 FROM dbo.SystemUserPreference sup WITH (NOLOCK)      
 WHERE UserCode = ' + QUOTENAME(@UserCode, '''') +')      
ELSE 0      
END AS Weight, iwo.RestrictedQuantity,      
CASE      
WHEN i.ItemType = ''Kit'' THEN 0      
ELSE IFS.InStock      
END AS InStock,      
CASE      
WHEN i.ItemType = ''Kit'' THEN 0      
ELSE IFS.FreeStock      
END AS FreeStock,  
IFS.POStatus AS POStatus,  
sc.KitPricingType,      
CASE      
WHEN i.IsDropShip IS NULL THEN 0      
ELSE i.IsDropShip      
END AS IsDropShip,      
CASE      
WHEN i.IsSpecialOrder IS NULL THEN 0      
ELSE i.IsSpecialOrder      
END AS IsSpecialOrder, isp.SupplierCode, iwo.CheckoutOption, 0 AS IsShipSeparately, sc.ProductDimensions, SCUM.IsPrePacked, SCUM.IsOverSized, SCUM.OverSizedShippingMethodCode, sc.AllocatedQty, sc.InStoreWarehouseCode, 
sc.VDPCustomisationCode_DEV004817, sc.VDPType_C, i.VDPDocumentCode_DEV004817, i.IsVDP_DEV004817, sc.DesignComments_C, sc.DesignFilename_C, sc.DesignCheckbox_C, sc.DesignFile_C, iwo.IsFeatured, iwo.AddArtworkFee_C,
ic.CategoryCode 
FROM dbo.EcommerceShoppingCart sc WITH (NOLOCK)      
INNER JOIN ( SELECT c.CustomerCode, c.Email, c.CouponCode, c.Notes      
 FROM dbo.Customer c WITH (NOLOCK)      
 WHERE c.CustomerCode = ' + QUOTENAME(@CustomerID, '''') +'      
 UNION ALL      
 SELECT cast(c.CustomerID AS NVARCHAR(30)) AS CustomerCode, c.UserName AS Email, NULL AS CouponCode, NULL AS Notes      
 FROM dbo.EcommerceCustomer c WITH (NOLOCK)      
 WHERE c.CustomerID = (CASE WHEN (ISNUMERIC(' + QUOTENAME(@CustomerID, '''') +') = 1) THEN ' + QUOTENAME(@CustomerID, '''') +' ELSE NULL END)      
 ) c ON c.CustomerCode = sc.CustomerCode      
 INNER JOIN dbo.InventoryItem i WITH (NOLOCK) ON i.ItemCode = sc.ItemCode      
 INNER JOIN dbo.InventoryItemWebOption iwo WITH (NOLOCK) ON i.ItemCode = iwo.ItemCode AND sc.WebsiteCode = iwo.Websitecode      
 INNER JOIN #ShoppingCartUnitMeasure SCUM WITH (NOLOCK) ON SCUM.ItemCode = sc.ItemCode AND SCUM.UnitMeasureCode = sc.UnitMeasureCode AND SCUM.CartID = sc.ShoppingCartRecGUID    
 INNER JOIN dbo.InventoryItemDescription iid WITH (NOLOCK) ON iid.ItemCode=  i.ItemCode AND iid.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') +'      
 ' + @AdditionalJoin +      
 'LEFT OUTER JOIN ( SELECT TOP 1 ItemCode, UnitMeasureCode, SupplierCode FROM dbo.InventorySupplierPricingLevel WITH (NOLOCK)      
 WHERE IsPriority = 1) isp ON isp.ItemCode = sc.ItemCode AND isp.UnitMeasureCode = sc.UnitMeasureCode      
 LEFT JOIN InventoryCategory ic ON i.ItemCode = ic.ItemCode AND IsPrimary = 1
 WHERE SC.CustomerCode =  ' + QUOTENAME(@CustomerID, '''') +' AND SC.ContactCode = ' + QUOTENAME(@ContactCode, '''') +' AND SC.CartType = ' + QUOTENAME(@CartType, '''') +' AND      
 sc.WebsiteCode = ' + QUOTENAME(@WebsiteCode, '''') +' ORDER BY CASE WHEN ItemName = ' + ISNULL(QUOTENAME(@HNPFCartoons,''''), 'ItemName') + ' THEN 1 
 WHEN ItemName = ' + ISNULL(QUOTENAME(@HNPFSatchel,''''), 'ItemName') + ' THEN 2       
 WHEN ItemName = ' + ISNULL(QUOTENAME(@HNDelivery,''''), 'ItemName') + ' THEN 3 ELSE 0 END'      
         
  SET @SqlQuery = Replace(Replace(@SqlQuery, CHAR(13), ' '), CHAR(10), '')        
  EXEC (@SqlQuery)    
      
/* Drop temporary table */    
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#ShoppingCartUnitMeasure'))                                                                    
 DROP TABLE #ShoppingCartUnitMeasure  
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#InventoryFreeStock'))                                                                    
 DROP TABLE #InventoryFreeStock  
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableItems'))                                                                    
 DROP TABLE #AvailableItems  
 SET NOCOUNT OFF;    
             
END      

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[EcommerceGetLoginInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EcommerceGetLoginInfo]
GO
  
CREATE PROC [dbo].[EcommerceGetLoginInfo]  
 @UserName nvarchar(100),  
 @WebsiteCode nvarchar(30)  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 DECLARE @CustomerLogin TABLE
 (
	DefaultContact NVARCHAR(30),
	CustomerCode NVARCHAR(30),
	ContactCode NVARCHAR(30), 
	[Password]  NVARCHAR(50),
	PasswordSalt NVARCHAR(50),
	PasswordIV NVARCHAR(50),
    ContactGUID UNIQUEIDENTIFIER,
    CustomerGUID UNIQUEIDENTIFIER,
    Username NVARCHAR(100),
    Telephone NVARCHAR(50),
    CurrencyCode NVARCHAR(30),
    LocaleSetting NVARCHAR(10)
 )
	
 INSERT INTO @CustomerLogin (DefaultContact, CustomerCode, ContactCode, [Password], PasswordSalt, PasswordIV, ContactGUID, CustomerGUID, Username, Telephone, CurrencyCode, LocaleSetting)   
 SELECT C.DefaultContact, 
		C.CustomerCode, 
		CC.ContactCode, 
		CC.[Password], 
		CC.PasswordSalt, 
		CC.PasswordIV, 
		CC.ContactGUID,
		C.CustomerGUID,
		CC.Username,
		C.Telephone,
		C.CurrencyCode,
		SL.ShortString
 FROM Customer C WITH (NOLOCK)     
 INNER JOIN CRMContact CC WITH (NOLOCK) ON CC.[Type] = 'CustomerContact' AND CC.EntityCode = C.CustomerCode     
 INNER JOIN EcommerceCustomerActiveSites ECAS WITH (NOLOCK) ON ECAS.ContactCode = CC.ContactCode 
 LEFT JOIN SystemLanguage SL WITH (NOLOCK) ON SL.LanguageCode = CC.LanguageCode
 WHERE ECAS.IsEnabled = 1 
	AND CC.IsAllowWebAccess=1     
	AND C.IsActive = 1 
	AND CC.IsActive = 1 
	AND CC.UserName = @UserName 
	AND ECAS.WebsiteCode = @WebsiteCode    
	AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, ISNULL(CC.SubscriptionExpirationOn, GETDATE() + 1)))) >= CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, GETDATE())))     
 ORDER BY CC.DateCreated DESC    
 
 -- Load from child table if no record is found on CRMContact
 IF NOT EXISTS (SELECT 1 FROM @CustomerLogin)
 BEGIN
 INSERT INTO @CustomerLogin (DefaultContact, CustomerCode, ContactCode, [Password], PasswordSalt, PasswordIV, ContactGUID, CustomerGUID, Username, Telephone, CurrencyCode, LocaleSetting)   
 SELECT C.DefaultContact, 
		C.CustomerCode, 
		CC.ContactCode, 
		CC.[Password], 
		CC.PasswordSalt, 
		CC.PasswordIV, 
		CC.ContactGUID,
		C.CustomerGUID,
		S.Email_DEV004817,
		S.Telephone_DEV004817,
		C.CurrencyCode,
		SL.ShortString
 FROM SGECRMContactMultipleEmailAccount_DEV004817 S WITH (NOLOCK)     
 INNER JOIN CRMContact CC WITH (NOLOCK) ON CC.[Type] = 'CustomerContact' AND CC.ContactCode = S.ContactCode_DEV004817     
 INNER JOIN Customer C ON C.CustomerCode = CC.EntityCode
 INNER JOIN EcommerceCustomerActiveSites ECAS WITH (NOLOCK) ON ECAS.ContactCode = CC.ContactCode 
 LEFT JOIN SystemLanguage SL WITH (NOLOCK) ON SL.LanguageCode = CC.LanguageCode
 WHERE ECAS.IsEnabled = 1 
	AND CC.IsAllowWebAccess=1     
	AND C.IsActive = 1 
	AND CC.IsActive = 1 
	AND S.Email_DEV004817 = @UserName 
	AND ECAS.WebsiteCode = @WebsiteCode    
	AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, ISNULL(CC.SubscriptionExpirationOn, GETDATE() + 1)))) >= CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, GETDATE())))     
 ORDER BY CC.DateCreated DESC    
 END

  -- For Company Portal only
  -- Load Contact using username not in email format
IF NOT EXISTS (SELECT 1 FROM @CustomerLogin)
 BEGIN	 
	 INSERT INTO @CustomerLogin (DefaultContact, CustomerCode, ContactCode, [Password], PasswordSalt, PasswordIV, ContactGUID, CustomerGUID, Username, Telephone, CurrencyCode, LocaleSetting)   
	 SELECT C.DefaultContact, 
			C.CustomerCode, 
			CC.ContactCode, 
			CC.[Password], 
			CC.PasswordSalt, 
			CC.PasswordIV, 
			CC.ContactGUID,
			C.CustomerGUID,
			CC.Username,
			C.Telephone,
			C.CurrencyCode,
			SL.ShortString
	 FROM Customer C WITH (NOLOCK)     
	 INNER JOIN CRMContact CC WITH (NOLOCK) ON CC.[Type] = 'CustomerContact' AND CC.EntityCode = C.CustomerCode     
	 INNER JOIN EcommerceCustomerActiveSites ECAS WITH (NOLOCK) ON ECAS.ContactCode = CC.ContactCode 
	 LEFT JOIN SystemLanguage SL WITH (NOLOCK) ON SL.LanguageCode = CC.LanguageCode
	 WHERE ECAS.IsEnabled = 1 
		AND CC.IsAllowWebAccess=1     
		AND C.IsActive = 1 
		AND CC.IsActive = 1 
		AND SUBSTRING(CC.UserName, 0, PATINDEX('%@%', CC.UserName)) = @UserName 
		AND ECAS.WebsiteCode = @WebsiteCode    
		AND CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, ISNULL(CC.SubscriptionExpirationOn, GETDATE() + 1)))) >= CONVERT(DATETIME, FLOOR(CONVERT(FLOAT, GETDATE())))     
	 ORDER BY CC.DateCreated DESC     
 END


 -- CUSTOMER LOGIN INFO
 SELECT DefaultContact, CustomerCode, ContactCode, [Password], PasswordSalt, PasswordIV, ContactGuid FROM @CustomerLogin
 
 -- ECOMMERCECUSTOMER RECORD CHECK
 IF EXISTS (SELECT 1 FROM @CustomerLogin)
 BEGIN
	IF NOT EXISTS(SELECT 1 FROM EcommerceCustomer WITH (NOLOCK) WHERE ContactGuid = (SELECT TOP 1 ContactGuid FROM @CustomerLogin))
	BEGIN
		INSERT INTO EcommerceCustomer (CustomerCode, CustomerGUID, Username, [Password], Referrer, LocaleSetting, CurrencyCode, LastIPAddress, Telephone, ContactGUID)
		SELECT TOP 1 CustomerCode, 
			CustomerGUID, 
			Username, 
			[Password], 
			'' AS Referrer, 
			CASE WHEN LocaleSetting IS NOT NULL THEN LocaleSetting 
				 ELSE ( SELECT B.ShortString 
						FROM SystemCompanyInformation A WITH (NOLOCK)
						INNER JOIN SystemLanguage B WITH (NOLOCK) ON B.LanguageCode = A.CompanyLanguage) END AS LocaleSetting, 
			CurrencyCode,
			'' AS LastIPAddress, 
			Telephone, 
			ContactGUID
		FROM @CustomerLogin
	END
 END
   
END  
  
GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE name = 'UsagebyProductView_C' AND type = N'V')
    DROP VIEW [dbo].[UsagebyProductView_C]
GO

CREATE VIEW UsagebyProductView_C AS 
SELECT e.CustomerCode, e.CostCenter_DEV004817, A.ItemCode, B.ItemDescription, 
FROM InventoryItem A INNER JOIN InventoryItemDescription B ON A.ItemCode = B.ItemCode INNER JOIN CustomerInvoiceDetail C ON C.ItemCode = A.ItemCode
INNER JOIN CustomerInvoice D ON D.InvoiceCode = C.InvoiceCode INNER JOIN Customer E ON E.CustomerCode = D.BillToCode
WHERE Year(InvoiceDate)='2017'


GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[ReadEcommerceReorderApprovalsEmail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[ReadEcommerceReorderApprovalsEmail]
GO

CREATE PROCEDURE [dbo].[ReadEcommerceReorderApprovalsEmail]          
 @LanguageCode NVARCHAR(100),          
 @Status NVARCHAR(1),
 @CostCenter NVARCHAR(100)
AS    
  
BEGIN       
	SET NOCOUNT ON;       

	SELECT A.ItemName, B.ItemDescription, C.PurchaseOrderCode, PODate, CAST(ISNULL(D.QuantityOrdered, 0) + ISNULL(A.BKO_C, 0) AS INT) AS OnBackOrder, CAST(ISNULL(C.QuantityOrdered, 0) AS INT) AS OnOrder FROM InventoryItem A INNER JOIN InventoryItemDescription B ON A.ItemCode = B.ItemCode
	INNER JOIN (SELECT A.PurchaseOrderCode, CONVERT(VARCHAR, PODate, 103) AS PODate, ItemCode, QuantityOrdered, IsApproved_DEV004817, IsDeclined_DEV004817 FROM SupplierPurchaseOrder A INNER JOIN SupplierPurchaseOrderDetail B ON A.PurchaseOrderCode = B.PurchaseOrderCode WHERE Type = 'Purchase Order' AND IsVoided != '1') AS C ON C.ItemCode = A.ItemCode
	LEFT JOIN (SELECT B.ItemCode, SUM(B.QuantityOrdered) AS QuantityOrdered FROM CustomerSalesOrder A INNER JOIN CustomerSalesOrderDetail B ON A.SalesOrderCode = B.SalesOrderCode WHERE OrderStatus = 'Open' AND Type IN ('Sales Order', 'Back Order') GROUP BY B.ItemCode) AS D ON D.ItemCode = A.ItemCode
	WHERE B.LanguageCode = @LanguageCode AND A.CostCenter_DEV004817 = @CostCenter AND A.Status = @Status AND ISNULL(C.IsApproved_DEV004817, 0) = 0 AND ISNULL(IsDeclined_DEV004817, 0) = 0 AND C.PurchaseOrderCode IS NOT NULL  ORDER BY C.PurchaseOrderCode
END
GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[InsertCompanyApprovalsEmail_C]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[InsertCompanyApprovalsEmail_C]
GO

CREATE PROCEDURE [dbo].[InsertCompanyApprovalsEmail_C]          
 @LanguageCode NVARCHAR(100),          
 @Status NVARCHAR(1),
 @CostCenter NVARCHAR(100),
 @ContactCode NVARCHAR(60)
AS    
  
BEGIN       
	SET NOCOUNT ON;       
	INSERT CompanyApprovalsEmail_C (PurchaseOrderCode, DateEmailSent, ContactCode)
	SELECT C.PurchaseOrderCode, GETDATE(), @ContactCode FROM InventoryItem A INNER JOIN InventoryItemDescription B ON A.ItemCode = B.ItemCode
	INNER JOIN (SELECT A.PurchaseOrderCode, ItemCode, QuantityOrdered, IsApproved_DEV004817, IsDeclined_DEV004817 FROM SupplierPurchaseOrder A INNER JOIN SupplierPurchaseOrderDetail B ON A.PurchaseOrderCode = B.PurchaseOrderCode WHERE Type = 'Purchase Order' AND IsVoided != '1') AS C ON C.ItemCode = A.ItemCode
	LEFT JOIN (SELECT B.ItemCode, SUM(B.QuantityOrdered) AS QuantityOrdered FROM CustomerSalesOrder A INNER JOIN CustomerSalesOrderDetail B ON A.SalesOrderCode = B.SalesOrderCode WHERE OrderStatus = 'Open' AND Type IN ('Sales Order', 'Back Order') GROUP BY B.ItemCode) AS D ON D.ItemCode = A.ItemCode
	WHERE B.LanguageCode = @LanguageCode AND A.CostCenter_DEV004817 = @CostCenter AND A.Status = @Status AND ISNULL(C.IsApproved_DEV004817, 0) = 0 AND ISNULL(IsDeclined_DEV004817, 0) = 0 AND C.PurchaseOrderCode IS NOT NULL AND C.PurchaseOrderCode NOT IN (SELECT PurchaseOrderCode FROM CompanyApprovalsEmail_C)
END
GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[EcommerceGetSpecialOffers]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EcommerceGetSpecialOffers]
GO

CREATE PROCEDURE [dbo].[EcommerceGetSpecialOffers] --[EcommerceGetSpecialOffers] 'Blackberry Curve', 'WEB-000001','English - United States', NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0', '2011-05-04 13:55:33.350',''                                      
 @SearchTerm   NVARCHAR(3000),                                                                        
 @WebSiteCode   NVARCHAR(30),                                                                        
 @LanguageCode   NVARCHAR(50),                                                                      
 @ItemCode    NVARCHAR(1000) = NULL,                                                                       
 @ItemType    NVARCHAR(50) = NULL,                                                                    
 @UnitMeasure   NVARCHAR(30) = NULL,                                                                    
 @CategoryID   NVARCHAR (100) = NULL,                                                          
 @SectionID    NVARCHAR (100) = NULL,                                                          
 @ManufacturerID  NVARCHAR (100) = NULL,                                      
 @AttributeID      NVARCHAR (100) = NULL,                                      
 @SearchDescriptions    VARCHAR(5),                                                              
 @CurrentDate   DATETIME,                                                        
 @ProductFilterID  NVARCHAR(50),                                    
 @ContactCode NVARCHAR(30),                  
 @CBMode BIT = 0,            
 @PageNum INT = 1,          
 @CurrencyCode NVARCHAR(30) = NULL,        
 @MinPrice NUMERIC(18,6),                            
 @MaxPrice NUMERIC(18,6)        
                                         
AS        
                                                                                                 
BEGIN                                                                        
 SET NOCOUNT ON                
 --SET ANSI_WARNINGS OFF                                                                      
               
 DECLARE @SearchDescriptionString NVARCHAR(MAX)                                      
 DECLARE @JoinUnitMeasureString NVARCHAR(MAX)                                       
 DECLARE @EntityBuilderString NVARCHAR(MAX)                                                          
 DECLARE @ProductFilter NVARCHAR(MAX)                                   
 DECLARE @AdditionalFilter NVARCHAR(MAX)           
 DECLARE @AdditionalField NVARCHAR(1000)      
 DECLARE @SKUString NVARCHAR(MAX) = ' '    
 DECLARE @SKUAdditionalFields NVARCHAR(MAX) = ' '        
 DECLARE @SKUAdditionalJoin NVARCHAR(MAX) = ' '      
                                                                                                               
 SET @AdditionalField = ''                  
 DECLARE @PageSize INT             
 SELECT @PageSize = ConfigValue FROM EcommerceAppConfig WITH (NOLOCK) WHERE Name = 'SearchPageSize' AND WebSiteCode = @WebsiteCode          
     
 IF @PageSize IS NULL OR @PageSize = 0          
 BEGIN                                      
 SET @PageSize = 999999999           
 END       
                               
 SET @AdditionalFilter=''                                          
 SET @SearchTerm = REPLACE(@SearchTerm,'''','''''')                                                                                     
 SET @SearchTerm = REPLACE(@SearchTerm,'+',' ')                     
 SET @SearchTerm = RTRIM(LTRIM(@SearchTerm))             
 SET @ItemCode = NULLIF(@ItemCode, NULL)                                                                     
 SET @EntityBuilderString = ''                                                    
 SET @ProductFilter = ''                                       

    -- ItemType                                      
 IF (@ItemType = 'ANY')                                                                  
  SET @ItemType = '''%'''                                        
 ELSE                                                               
  SET @ItemType = '''%' + @ItemType + '%'''                                                                     
                                          
    -- UnitMeasure                                      
    IF (@UnitMeasure = 'ANY')                      
  SET @UnitMeasure = '''%'''                                                                        
 ELSE                                                                    
  SET @UnitMeasure = '%' + @UnitMeasure + '%'                                                                      
                                       
 -- SearchDescriptions                                      
 IF (@SearchDescriptions = '0')                                      
  SET @SearchDescriptionString = ' '                                      
 ELSE                                                                    
  SET @SearchDescriptionString = ' OR IIWOD.WebDescription LIKE ' + QUOTENAME(@SearchTerm, '''') + ' '                                      
             
 -- UnitMeasure                                      
 IF (@UnitMeasure = '''%''')                                        
  SET @JoinUnitMeasureString = ' '                                                                     
 ELSE                                           
  SET @JoinUnitMeasureString = ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode                                       
           AND ISNULL(UnitMeasureQty,0)>0                                
           AND IUM.UnitMeasureCode LIKE ' + QUOTENAME(@UnitMeasure, '''') + ' '                                                  
                                       
 -- CategoryID                                      
 IF (@CategoryID <> '0' AND @CategoryID <> '')                     
 BEGIN                                                          
  SET @EntityBuilderString = 'INNER JOIN (SELECT ItemCode FROM InventoryCategory A WITH (NOLOCK)                                       
            INNER JOIN SystemCategory B WITH (NOLOCK) ON A.CategoryCode = B.CategoryCode                                       
            WHERE B.Counter = ' + QUOTENAME(@CategoryID, '''') + ') IC ON IC.ItemCode = A.ItemCode '                                                          
 END                                        
                                        
 -- SectionID                                      
 IF (@SectionID <> '0' AND @SectionID <> '')                                                  
 BEGIN                                        
  SET @EntityBuilderString += ' INNER JOIN (SELECT ItemCode FROM InventoryItemDepartment A WITH (NOLOCK)                                       
             INNER JOIN InventorySellingDepartment B WITH (NOLOCK) ON A.DepartmentCode = B.DepartmentCode                                       
       WHERE B.Counter = ' + QUOTENAME(@SectionID, '''') + ') IIDep ON IIDep.ItemCode = A.ItemCode '                                                          
 END                                      
                                  
 -- ManufcaturerID              
 IF (@ManufacturerID <> '0' AND @ManufacturerID <> '')                                                          
 BEGIN                                             
  SET @EntityBuilderString += ' INNER JOIN (SELECT A.ManufacturerCode, A.ItemCode                                       
             FROM InventoryItem A WITH (NOLOCK)                                       
             INNER JOIN SystemManufacturer B WITH (NOLOCK) ON A.ManufacturerCode = B.ManufacturerCode                                       
             WHERE B.Counter = ' + QUOTENAME(@ManufacturerID, '''') + ') SM ON SM.ItemCode = A.ItemCode '                                                          
 END                                      
                                       
 -- AttributeID                                      
 IF (@AttributeID <> '0' AND @AttributeID <> '')                                                          
 BEGIN                                           
  SET @EntityBuilderString += ' INNER JOIN (SELECT A.AttributeValue, A.ItemCode                                       
             FROM InventoryItemAttributeView A WITH (NOLOCK)                                       
             INNER JOIN SystemItemAttributeSourceFilterValue B WITH (NOLOCK) ON A.AttributeValue = B.SourceFilterName                                       
              AND A.AttributeSourceFilterCode = B.AttributeSourceFilterCode                                       
             WHERE A.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + '                                       
              AND B.Counter = ' + QUOTENAME(@AttributeID, '''') + ') SA ON SA.ItemCode = A.ItemCode '                                                          
 END                                                         
                                                        
    -- ProductFilterID                                      
 IF @ProductFilterID IS NULL                                                    
  SET @ProductFilterID  = ''                                                     
                                                     
 IF @ProductFilterID <> ''                                                      
 BEGIN                                                                
  SET @ProductFilter = ' INNER JOIN (SELECT Distinct(ItemCode),TemplateID                                       
           FROM InventoryProductFilterTemplateItem                                       
    WHERE TemplateID = ' + QUOTENAME(@ProductFilterID, '''') + ') PF ON A.ItemCode = PF.ItemCode                                       
            AND PF.TemplateID = ' + QUOTENAME(@ProductFilterID, '''') + ' '                                                  
 END                       
           
IF @CurrencyCode IS NULL            
BEGIN            
 SELECT @CurrencyCode = CurrencyCode            
 FROM Customer CU WITH (NOLOCK)            
 INNER JOIN CRMContact CO WITH (NOLOCK) ON CO.EntityCode = CU.CustomerCode             
  AND CO.ContactCode = @ContactCode            
END            
                                         
------------------Ranking----------------------------------------              
 CREATE TABLE #TempSearchTerms (SearchTerm NVARCHAR(MAX))                 
 INSERT INTO #TempSearchTerms (SearchTerm) VALUES (@SearchTerm);              
-----------------------------------------------------------------                                        
 DECLARE @SearchTermSplit NVARCHAR(MAX)                                     
 DECLARE @splitstring NVARCHAR(3000)                                      
 DECLARE @substring NVARCHAR(3000)                                      
             
 DECLARE @ignoreString NVARCHAR(1000)= ',,'            
 DECLARE @ignoreFilter BIT = 0;            
 SELECT @ignoreString = ',' + RTRIM(LTRIM(REPLACE(ISNULL(ConfigValue,''),'''',''''''))) + ',' FROM EcommerceAppConfig Where Name ='SearchIgnoredWords' AND WebsiteCode =@WebSiteCode            
                                                         
 SET @splitstring = @SearchTerm + ' '                                      
 SET @SearchTermSplit  = ''                                      
                                        
/*CBN Mode*/                  
 IF @CBMode = 1                  
 BEGIN                  
  SET @AdditionalFilter = @AdditionalFilter + ' AND A.IsCBN = 1 '                  
 END                             
                          
/*Out of Stock Items*/                      
CREATE TABLE #TempSearchResult (ItemCode NVARCHAR(30), ItemType NVARCHAR(30))                      
CREATE TABLE #InventoryFreeStock (WarehouseCode NVARCHAR(30), ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))                       
CREATE TABLE #AvailableItems (ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), UM NVARCHAR(30))                      
-- temporary tables to store available kit components and matrix attributes                
CREATE TABLE #AvailableKitComponents (ItemKitCode NVARCHAR(30), ItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), ItemType NVARCHAR(30), POStatus NVARCHAR(30))         
CREATE TABLE #AvailableMatrixAttributes (ItemCode NVARCHAR(30), MatrixItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))                        
                                            
DECLARE @SQL NVARCHAR(MAX)                                        
DECLARE @AdditionalJoin NVARCHAR(MAX)                                                      
DECLARE @SqlQuery NVARCHAR(MAX)                        
DECLARE @HideOutOfStockProducts BIT                                                                       
DECLARE @ShowInventoryFromAllWarehouses BIT                                                                                                
DECLARE @WarehouseCode NVARCHAR(30)                        
                                                               
SET @AdditionalJoin = ''                                          
SET @SqlQuery = ''                         
SET @HideOutOfStockProducts =  0                                   
SET @ShowInventoryFromAllWarehouses = 0                                                                                                               
                                  
SELECT @HideOutOfStockProducts = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'HideOutOfStockProducts' AND WebSiteCode = @WebSiteCode                
SELECT @ShowInventoryFromAllWarehouses = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'ShowInventoryFromAllWarehouses' AND WebSiteCode = @WebSiteCode                
SET @WarehouseCode = (SELECT TOP 1 WareHouseCode FROM CustomerShipTo WHERE ShipToCode = (SELECT DefaultShippingCode FROM CRMContact WHERE ContactCode = @ContactCode))                      
                      
SET @SQL = ' SELECT A.ItemCode, A.ItemType FROM InventoryItem A WITH (NOLOCK)                                               
     LEFT JOIN (SELECT ItemCode FROM InventoryItemPricingDetail B WITH (NOLOCK)                                               
     INNER JOIN SystemCurrency C WITH (NOLOCK) ON B.CurrencyCode = C.CurrencyCode WHERE IsHomeCurrency = 1) D ON A.ItemCode = D.ItemCode                                               
     LEFT JOIN (SELECT TOP 1 ItemKitCode FROM InventoryKitPricingDetail E WITH (NOLOCK)                                               
     INNER JOIN SystemCurrency F WITH (NOLOCK) ON E.CurrencyCode = F.CurrencyCode WHERE IsHomeCurrency = 1) G ON A.ItemCode = G.ItemKitCode                                               
     INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON IID.ItemCode = A.ItemCode           
      AND IID.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + '                                               
     INNER JOIN InventoryItemWebOption IIWO WITH (NOLOCK) ON IIWO.ItemCode = A.ItemCode                                               
      AND IIWO.WebsiteCode = ' + QUOTENAME(@WebsiteCode, '''') + '                                               
     INNER JOIN InventoryItemWebOptionDescription IIWOD WITH (NOLOCK) ON IIWOD.ItemCode = IIWO.ItemCode                              
      AND IIWOD.WebsiteCode = IIWO.WebsiteCode                                               
      AND IIWOD.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + ' ' + @SKUAdditionalJoin + ' ' +                                           
      @JoinUnitMeasureString + ' ' +                                              
      @EntityBuilderString  + ' ' +                         
      @ProductFilter  + ' ' +                                        
      @AdditionalJoin + '                                           
     WHERE ItemType IN (''Non-Stock'',                                               
            ''Service'',                        
            ''Stock'',                                              
            ''Matrix Group'',                                              
            ''Kit'',                                              
            ''Service'',                                              
            ''Electronic Download'',                                               
            ''Assembly'',            
            ''Gift Card'',            
            ''Gift Certificate'')                                              
       AND ''' + CAST(@CurrentDate AS NVARCHAR(50)) + ''' BETWEEN ISNULL(IIWO.StartDate, ''1/1/1900'')                                               
       AND ISNULL(IIWO.EndDate, ''12/31/9999'')                            
       AND (A.Status = ''A'' OR (A.Status = ''P'' AND A.ItemType IN (''Kit'', ''Matrix Group'')) OR (A.Status = ''P''                                
       AND A.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'')))                                       
       AND IIWO.Published = 1                                 
       AND IIWO.CheckOutOption = 0                                               
       AND ItemType LIKE ' + @ItemType +                         
        @AdditionalFilter + '                        
       ORDER BY IIWO.IsExclusive DESC, A.ItemName ASC'                      
                             
INSERT INTO #TempSearchResult (ItemCode, ItemType)                      
EXEC (@SQL)        
              
/* remove giftcard or giftcertificate items that are not sellable to customer's currency */            
DELETE FROM #TempSearchResult            
WHERE ItemCode IN ( SELECT II.ItemCode            
     FROM InventoryItem II WITH (NOLOCK)            
     INNER JOIN InventoryItemPricingDetail IP WITH (NOLOCK) ON IP.ItemCode = II.ItemCode            
      AND IP.CurrencyCode = @CurrencyCode             
      AND IP.[Enabled] = 0 
      AND II.ItemType IN ('Gift Card','Gift Certificate'))            
-- Get freestock of non-kit/non-matrix group items (Stock, Matrix Item, Assembly)            
IF @HideOutOfStockProducts = 1          
BEGIN          
 INSERT INTO #inventoryfreestock (ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)            
 SELECT EISTV.ItemCode, EISTV.WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM EcommerceInventoryStockTotalView EISTV            
 INNER JOIN #TempSearchResult TSR ON TSR.ItemCode = EISTV.ItemCode          
 LEFT JOIN SupplierPurchaseOrderDetail SPOD ON SPOD.ItemCode = TSR.ItemCode AND spod.WarehouseCode = EISTV.WarehouseCode AND spod.UnitMeasure = EISTV.UnitMeasureCode            
 LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode            
END          
ELSE          
BEGIN          
 INSERT INTO #inventoryfreestock (ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)          
 SELECT EISTV.ItemCode, EISTV.WarehouseCode, FreeStock, UnitMeasureCode, NULL FROM EcommerceInventoryStockTotalView EISTV          
 INNER JOIN #TempSearchResult TSR ON TSR.ItemCode = EISTV.ItemCode          
END

IF @HideOutOfStockProducts = 1            
BEGIN            
 INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                  
 SELECT II.ItemCode, 0, UnitMeasureCode FROM InventoryItem II          
 INNER JOIN #TempSearchResult TSR ON TSR.ItemCode = II.ItemCode                                  
 INNER JOIN InventoryUnitMeasure IUM ON II.ItemCode = IUM.ItemCode                                           
 WHERE II.ItemType IN ( 'Service', 'Electronic Download', 'Non-Stock', 'Gift Card', 'Gift Certificate')                                  
 AND IsBase = 1            
 
 IF ( SELECT COUNT(TSR.ItemCode) FROM InventoryItem II              
	  INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode AND II.ItemType = 'Kit' ) > 0          
 BEGIN          
  -- get the freestock of kit component/s          
  ;WITH cte_AvailableKitComponents           
   AS          
   (          
    SELECT IKD.ItemKitCode, IKD.ItemCode AS ItemCode, 
    CASE
		WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN @WarehouseCode
		ELSE EISTV.WarehouseCode
	END AS WarehouseCode, 
	CASE
		WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN 0
		ELSE EISTV.FreeStock
	END AS FreeStock, IKD.UnitMeasureCode, II.ItemType, OrderStatus,            
    ROW_NUMBER() over(PARTITION BY  IKD.ItemKitCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum          
    FROM InventoryItem II WITH (NOLOCK)
    INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON II.ItemCode = IKD.ItemCode             
    LEFT JOIN EcommerceInventoryStockTotalView EISTV ON IKD.ItemCode = EISTV.ItemCode          
    LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IKD.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode            
    LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode
	INNER JOIN InventoryItemWebOption IWO ON IWO.ItemCode = II.ItemCode
    WHERE ItemKitCode IN (SELECT TSR.ItemCode          
						FROM InventoryItem II              
						INNER JOIN #TempSearchResult TSR              
						ON II.ItemCode = TSR.ItemCode              
						AND II.ItemType = 'Kit') AND IsSale_C = 1         
   )          
   INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType, POStatus)          
   SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType, OrderStatus FROM cte_AvailableKitComponents WHERE RowNum = 1          
 END            
            
 IF ( SELECT COUNT(TSR.ItemCode) FROM InventoryItem II              
   INNER JOIN #TempSearchResult TSR              
   ON II.ItemCode = TSR.ItemCode              
   AND II.ItemType = 'Matrix Group' ) > 0            
 BEGIN                 
  -- get the freestock of matrix item attribute/s          
  ;WITH cte_AvailableMatrixAttributes          
   AS          
   (          
    SELECT IMI.ItemCode, IMI.MatrixItemCode, EISTV.WarehouseCode , EISTV.FreeStock, EISTV.UnitMeasureCode, OrderStatus,            
    ROW_NUMBER() over(PARTITION BY  IMI.ItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum          
    FROM InventoryMatrixItem IMI             
    INNER JOIN EcommerceInventoryStockTotalView EISTV ON IMI.MatrixItemCode = EISTV.ItemCode          
    LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IMI.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode            
    LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode            
    WHERE (EISTV.FreeStock > 0 OR OrderStatus IN ('Open', 'Partial'))          
    AND IMI.ItemCode IN (SELECT TSR.ItemCode          
          FROM InventoryItem II              
             INNER JOIN #TempSearchResult TSR              
             ON II.ItemCode = TSR.ItemCode              
             AND II.ItemType = 'Matrix Group')          
   )          
   INSERT INTO #AvailableMatrixAttributes (ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)          
   SELECT ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM cte_AvailableMatrixAttributes WHERE RowNum = 1          
 END          
END          
ELSE            
BEGIN
	IF ( SELECT COUNT(TSR.ItemCode) FROM InventoryItem II              
		 INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode 
		 AND II.ItemType = 'Kit' ) > 0                     
	BEGIN                        
		-- get the freestock of kit component/s             
		;WITH cte_AvailableKitComponents                       
		AS                      
		(                      
			SELECT IKD.ItemKitCode, IKD.ItemCode AS ItemCode, 
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN @WarehouseCode
				ELSE EISTV.WarehouseCode
			END AS WarehouseCode,
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN 0
				ELSE EISTV.FreeStock
			END AS FreeStock, IKD.UnitMeasureCode, II.ItemType,                        
			ROW_NUMBER() over(PARTITION BY  IKD.ItemKitCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC) as RowNum                      
			FROM InventoryItem II WITH (NOLOCK)  
			INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON II.ItemCode = IKD.ItemCode                        
			LEFT JOIN EcommerceInventoryStockTotalView EISTV ON IKD.ItemCode = EISTV.ItemCode     
			INNER JOIN InventoryItemWebOption IWO ON IWO.ItemCode = II.ItemCode                                  
			WHERE (II.Status = 'A' OR (II.Status = 'P' AND II.ItemType IN ('Stock', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly', 'Matrix Item')   
			AND EISTV.FreeStock > 0 ))                      
			AND IKD.ItemKitCode IN ( SELECT TSR.ItemCode FROM InventoryItem II              
									 INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode
									 AND II.ItemType = 'Kit')                 
			AND IsSale_C = 1     
		)              
		INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType)                      
		SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType FROM cte_AvailableKitComponents WHERE RowNum = 1                      
	END      

	IF ( SELECT COUNT(TSR.ItemCode) FROM InventoryItem II              
		 INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode              
		 AND II.ItemType = 'Matrix Group' ) > 0                       
	BEGIN                             
		-- get the freestock of matrix item attribute/s                      
		;WITH cte_AvailableMatrixAttributes                      
		AS                      
		(                      
			SELECT IMI.ItemCode, IMI.MatrixItemCode, EISTV.WarehouseCode , EISTV.FreeStock, EISTV.UnitMeasureCode,                        
			ROW_NUMBER() over(PARTITION BY  IMI.ItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC) as RowNum                      
			FROM InventoryItem II WITH (NOLOCK)  
			INNER JOIN InventoryMatrixItem IMI WITH (NOLOCK) ON II.ItemCode = IMI.MatrixItemCode                         
			INNER JOIN EcommerceInventoryStockTotalView EISTV ON IMI.MatrixItemCode = EISTV.ItemCode                                          
			WHERE (II.Status = 'A' OR (II.Status = 'P' AND II.ItemType = 'Matrix Item' AND EISTV.FreeStock > 0 ))                      
			AND IMI.ItemCode IN ( SELECT TSR.ItemCode FROM InventoryItem II              
								  INNER JOIN #TempSearchResult TSR ON II.ItemCode = TSR.ItemCode              
								  AND II.ItemType = 'Matrix Group')                      
		)                      
		INSERT INTO #AvailableMatrixAttributes (ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode)                      
		SELECT ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode FROM cte_AvailableMatrixAttributes WHERE RowNum = 1                      
	END 
	                 
	INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                  
	SELECT II.ItemCode, 0, UnitMeasureCode FROM InventoryItem II          
	INNER JOIN InventoryItemWebOption IWO ON IWO.ItemCode = II.ItemCode                             
	INNER JOIN InventoryUnitMeasure IUM ON II.ItemCode = IUM.ItemCode                                           
	WHERE II.ItemType IN ('Service', 'Electronic Download', 'Non-Stock', 'Gift Card', 'Gift Certificate')                                  
	AND IsBase = 1 AND IsSale_C = 1           
END         
-- FreeStock from all warehouse            
IF @ShowInventoryFromAllWarehouses = 1            
BEGIN            
	IF @HideOutOfStockProducts = 1            
	BEGIN            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                              
		BEGIN                        
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                           
			BEGIN                        
				;WITH cte_AvailableKitComponents 
				AS
				(
					SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode, ItemType, POStatus,
					ROW_NUMBER() over(PARTITION BY ItemKitCode, UnitMeasureCode ORDER BY ItemKitCode) as RowNum                        
					FROM #AvailableKitComponents                                  
					GROUP BY ItemKitCode, UnitMeasureCode, ItemType, POStatus
				)
				
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)
				SELECT ItemKitCode, FreeStock, UnitMeasureCode
				FROM cte_AvailableKitComponents
				WHERE RowNum = 1 AND ((FreeStock > 0 OR POStatus IN ('Open', 'Partial')) OR ItemType IN ('Non-Stock', 'Electronic Download', 'Service'))
				GROUP BY ItemKitCode, UnitMeasureCode, FreeStock     
				       
			END                          
		END          
		        
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                
		BEGIN                
			IF ((SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes)) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableMatrixAttributes          
				GROUP BY ItemCode, UnitMeasureCode          
			END            
		END            
		           
		;WITH cte_InventoryFreeStock           
		AS          
		(          
			SELECT ItemCode, FreeStock, WarehouseCode, UnitMeasureCode,          
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum          
			FROM #InventoryFreeStock          
			WHERE FreeStock > 0 OR POStatus IN ('Open', 'Partial')     
		)          
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
		FROM cte_InventoryFreeStock          
		WHERE RowNum = 1          
		GROUP BY ItemCode, UnitMeasureCode          
		        
		IF (@UnitMeasure = '''%''')        
			SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode AND IUM.IsBase = 1                  
			INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '          
		ELSE       
			SET @AdditionalJoin += ' INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '                 
	END            
	ELSE            
	BEGIN 
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                
		BEGIN          
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)            
				SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableKitComponents          
				GROUP BY ItemKitCode, UnitMeasureCode   
			END             
		END          
		        
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                
		BEGIN                
			IF ((SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes)) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableMatrixAttributes          
				GROUP BY ItemCode, UnitMeasureCode          
			END            
		END
		           
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                          
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode FROM #inventoryfreestock            
		GROUP BY ItemCode, UnitMeasureCode       
		
		IF (@UnitMeasure = '''%''')             
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode AND IUM.IsBase = 1                  
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '        
		ELSE       
		SET @AdditionalJoin += ' INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '             
	END            
END            
-- FreeStock from specific warehouse            
ELSE            
BEGIN                                                     
	IF @HideOutOfStockProducts = 1                                                
	BEGIN            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                              
		BEGIN                              
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                           
			BEGIN                         
				;WITH cte_AvailableKitComponents 
				AS
				(
					SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode, ItemType, POStatus,
					ROW_NUMBER() over(PARTITION BY ItemKitCode, UnitMeasureCode ORDER BY ItemKitCode) as RowNum                        
					FROM #AvailableKitComponents                        
					WHERE WarehouseCode = @WarehouseCode          
					GROUP BY ItemKitCode, UnitMeasureCode, ItemType, POStatus
				)
				
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)
				SELECT ItemKitCode, FreeStock, UnitMeasureCode
				FROM cte_AvailableKitComponents
				WHERE RowNum = 1 AND ((FreeStock > 0 OR POStatus IN ('Open', 'Partial')) OR ItemType IN ('Non-Stock', 'Electronic Download', 'Service'))
				GROUP BY ItemKitCode, UnitMeasureCode, FreeStock                     
			END                          
		END          
	            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                
		BEGIN                
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableMatrixAttributes          
				WHERE WarehouseCode = @WarehouseCode          
				GROUP BY ItemCode, UnitMeasureCode            
			END            
		END          
	        
		;WITH cte_InventoryFreeStock           
		AS          
		(          
			SELECT ItemCode, FreeStock, WarehouseCode, UnitMeasureCode,          
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum          
			FROM #InventoryFreeStock          
			WHERE WarehouseCode = @WarehouseCode AND (FreeStock > 0 OR POStatus IN ('Open', 'Partial'))          
		)          
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
		FROM cte_InventoryFreeStock          
		WHERE RowNum = 1          
		GROUP BY ItemCode, UnitMeasureCode        
		                                 
		IF (@UnitMeasure = '''%''')                            
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode AND IUM.IsBase = 1                           
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '             
		ELSE       
		SET @AdditionalJoin += ' INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '       
	                 
	END          
	ELSE                                  
	BEGIN            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                
		BEGIN                
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0             
			BEGIN            
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)            
				SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableKitComponents          
				WHERE WarehouseCode = @WarehouseCode          
				GROUP BY ItemKitCode, UnitMeasureCode          
			END            
		END          
		            
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                
		BEGIN                
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0             
			BEGIN          
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)          
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode          
				FROM #AvailableMatrixAttributes          
				WHERE WarehouseCode = @WarehouseCode          
				GROUP BY ItemCode, UnitMeasureCode            
			END            
		END  
		                                              
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                               
		SELECT ItemCode, FreeStock, UnitMeasureCode FROM #inventoryfreestock                                                
		WHERE WarehouseCode = @WarehouseCode       
		       
		IF (@UnitMeasure = '''%''')
		BEGIN            
			SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = A.ItemCode AND IUM.IsBase = 1                                          
			INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '                 
		END
		ELSE
		BEGIN      
			SET @AdditionalJoin += ' INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = A.ItemCode AND AI.UM = IUM.UnitMeasureCode '                                           
		END
	END                                                   
END                                                               
/*End of Out of Stock Items*/            
         
--------------------------------------ITEM---PRICE----------------------------------------         
 IF @MinPrice IS NULL             
 SET @MinPrice = 0            
             
 IF @MaxPrice IS NULL         
 SET @MaxPrice = 999999999        
                       
 DECLARE @DefaultPrice NVARCHAR(20)           
 SELECT @DefaultPrice = DefaultPrice           
 FROM Customer CU                            
 INNER JOIN CRMContact CC ON CC.EntityCode = CU.CustomerCode           
 AND CC.ContactCode = @ContactCode                                  
                                              
 CREATE TABLE #inventoryitempricingdetail (ItemCode NVARCHAR(30) NOT NULL, WholeSalePrice NUMERIC(18,6), RetailPrice NUMERIC(18,6))                                               
                         
 INSERT INTO #inventoryitempricingdetail                                        
 SELECT IIPD.ItemCode, WholeSalePrice, RetailPrice FROM InventoryItemPricingDetail IIPD WITH (NOLOCK)
 INNER JOIN #TempSearchResult PF ON IIPD.ItemCode = PF.ItemCode   
 INNER JOIN InventoryItem II ON  II.ItemCode = PF.ItemCode         
 WHERE II.ItemType <> 'Kit' AND CurrencyCode = @CurrencyCode                                      
                                         
 --Insert pricing info for kit items in #inventoryitempricingdetail                
 IF (SELECT COUNT(PF.ItemCode) FROM InventoryItem II                
  INNER JOIN #TempSearchResult PF ON II.ItemCode = PF.ItemCode                                   
  WHERE II.ItemType = 'Kit') >  0                
                   
 BEGIN              
  CREATE TABLE #KitItems (ItemCode NVARCHAR(30) NOT NULL, UseCustomerPricing BIT)                
  INSERT INTO #KitItems                  
  SELECT PF.ItemCode, ISNULL(IK.IsGetItemPrice, 0) FROM InventoryItem II WITH (NOLOCK)                                     
  INNER JOIN InventoryItem PF WITH (NOLOCK) ON II.ItemCode = PF.ItemCode                
  INNER JOIN InventoryKit IK WITH (NOLOCK) ON PF.ItemCode = IK.ItemKitCode                                    
  WHERE II.ItemType = 'Kit'              
                  
  INSERT INTO #inventoryitempricingdetail                                                            
  SELECT IKD.ItemKitCode as ItemCode, SUM(IIPD.WholeSalePrice) AS WholeSalePrice, SUM(IIPD.RetailPrice) AS RetailPrice FROM InventoryKitDetail IKD WITH (NOLOCK)                   
  INNER JOIN InventoryItemPricingDetail IIPD WITH (NOLOCK)  ON IKD.ItemCode = IIPD.ItemCode AND IIPD.CurrencyCode = @CurrencyCode               
  INNER JOIN #KitItems KI ON KI.ItemCode = IKD.ItemKitCode                 
  WHERE IIPD.CurrencyCode = @CurrencyCode AND IKD.IsDefault = 1 AND KI.UseCustomerPricing = 1                
  GROUP BY IKD.ItemKitCode                 
  UNION ALL               
  SELECT IKD.ItemKitCode, SUM(TotalRate) AS WholeSalePrice, SUM(TotalRate) AS RetailPrice FROM InventoryKitPricingDetail IKPD WITH (NOLOCK)                
  INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON IKPD.ItemKitCode = IKD.ItemKitCode AND IKPD.ItemCode = IKD.ItemCode                
  INNER JOIN #KitItems KI ON KI.ItemCode = IKD.ItemKitCode                
  WHERE IKPD.CurrencyCode = @CurrencyCode AND IKD.IsDefault = 1 AND KI.UseCustomerPricing = 0                
  GROUP BY IKD.ItemKitCode                                                                                                      
 END                   
                                                  
 IF @DefaultPrice = 'Retail'                            
 BEGIN                            
  SET @AdditionalFilter = @AdditionalFilter + ' AND ISNULL(RetailPrice,0) >= ' + QUOTENAME(@MinPrice,'''') + ' AND ISNULL(RetailPrice,0) <= ' + QUOTENAME(@MaxPrice,'''') + ' '                           
 END                            
 ELSE                             
 BEGIN                             
  SET @AdditionalFilter = @AdditionalFilter + ' AND ISNULL(WholesalePrice,0) >= ' + QUOTENAME(@MinPrice,'''') + ' AND ISNULL(WholesalePrice,0) <= ' + QUOTENAME(@MaxPrice,'''') + ' '                            
 END             
             
 SET @AdditionalField = @AdditionalField + ' , IIPD.WholeSalePrice AS WholeSalePrice, IIPD.RetailPrice AS RetailPrice '             
 SET @AdditionalJoin = @AdditionalJoin + ' LEFT JOIN #inventoryitempricingdetail IIPD WITH (NOLOCK) ON IIPD.ItemCode = A.ItemCode '                                                                        
                                                               
--------------------------------------Ranking----------------------------------------              
CREATE TABLE #TempSearchResultRanking ( ItemCode NVARCHAR(30)              
            ,KeyWords NVARCHAR(MAX)              
            ,Ranking BIGINT            
            ,CategoryID INT            
            ,CategoryDisplayName NVARCHAR(100)            
            ,ManufacturerID INT            
            ,ManufacturerDisplayName NVARCHAR(100)            
            )                  
SET @SQL  = '               
SELECT A.ItemCode, (ISNULL(IID.ItemDescription,'''') +'' ''+ ISNULL(IIWOD.SEKeywords,'''') +'' ''+ ISNULL(A.ItemName,'''')) AS KeyWords, 0 AS Ranking            
,NULL AS CategoryID ,NULL AS CategoryDisplayName ,NULL AS ManufacturerID ,NULL AS ManufacturerDisplayName            
FROM InventoryItem A WITH (NOLOCK)                                       
 LEFT JOIN (SELECT ItemCode FROM InventoryItemPricingDetail B WITH (NOLOCK)                                       
     INNER JOIN SystemCurrency C WITH (NOLOCK) ON B.CurrencyCode = C.CurrencyCode WHERE IsHomeCurrency = 1) D ON A.ItemCode = D.ItemCode                                       
  LEFT JOIN (SELECT TOP 1 ItemKitCode FROM InventoryKitPricingDetail E WITH (NOLOCK)                                       
     INNER JOIN SystemCurrency F WITH (NOLOCK) ON E.CurrencyCode = F.CurrencyCode WHERE IsHomeCurrency = 1) G ON A.ItemCode = G.ItemKitCode                                       
     INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON IID.ItemCode = A.ItemCode                                       
      AND IID.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + '                                       
     INNER JOIN InventoryItemWebOption IIWO WITH (NOLOCK) ON IIWO.ItemCode = A.ItemCode                                       
      AND IIWO.WebsiteCode = ' + QUOTENAME(@WebsiteCode, '''') + '                                       
     INNER JOIN InventoryItemWebOptionDescription IIWOD WITH (NOLOCK) ON IIWOD.ItemCode = IIWO.ItemCode                                   
      AND IIWOD.WebsiteCode = IIWO.WebsiteCode                                       
      AND IIWOD.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + ' '  + @SKUAdditionalJoin +                                 
      @JoinUnitMeasureString + ' ' +                                      
      @EntityBuilderString  + ' ' +                                      
      @ProductFilter  + ' ' +                                
      @AdditionalJoin + '              
      WHERE ItemType IN (''Non-Stock'',                                       
            ''Service'',                                       
            ''Stock'',                                      
          ''Matrix Group'',                             
            ''Kit'',                                      
            ''Service'',                                      
            ''Electronic Download'',                                       
            ''Assembly'',            
            ''Gift Card'',            
            ''Gift Certificate'')                                      
       AND ''' + CAST(@CurrentDate AS NVARCHAR(50)) + ''' BETWEEN ISNULL(IIWO.StartDate, ''1/1/1900'')                                       
       AND ISNULL(IIWO.EndDate, ''12/31/9999'')                    
       AND (A.Status = ''A'' OR (A.Status = ''P'' AND A.ItemType IN (''Kit'', ''Matrix Group'')) OR (A.Status = ''P''                        
       AND A.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'') AND AI.FreeStock > 0))                               
       AND IIWO.Published = 1                                       
       AND IIWO.CheckOutOption = 0                                       
       AND ItemType LIKE ' + @ItemType +                 
        @AdditionalFilter +               
       'GROUP BY A.ItemCode,IID.ItemDescription,IIWOD.SEKeywords,A.ItemName'                    
               
 INSERT INTO #TempSearchResultRanking (ItemCode, KeyWords,Ranking,CategoryID,CategoryDisplayName,ManufacturerID,ManufacturerDisplayName)                      
 EXEC (@SQL)              
               
             
 --Update CategoryCode            
 UPDATE SRR            
 SET    SRR.CategoryID = IC.[Counter],            
  SRR.CategoryDisplayName = IC.[Description]            
 FROM   #TempSearchResultRanking SRR            
 LEFT JOIN (            
  SELECT A.ItemCode, C.[Counter],D.[Description] FROM InventoryCategory A WITH (NOLOCK)            
  INNER JOIN SystemCategory C WITH (NOLOCK) ON A.CategoryCode = C.CategoryCode            
  INNER JOIN SystemCategoryWebOption B WITH (NOLOCK) ON C.CategoryCode = B.CategoryCode            
  INNER JOIN SystemCategoryDescription D WITH (NOLOCK) ON B.CategoryCode = D.CategoryCode            
  WHERE A.IsPrimary =1 AND Published = 1 AND C.IsActive = 1 AND D.LanguageCode = @LanguageCode AND WebSiteCode = @WebSiteCode            
     ) IC ON IC.ItemCode = SRR.ItemCode              
             
UPDATE SRR            
SET    SRR.ManufacturerID = IC.[Counter],            
  SRR.ManufacturerDisplayName = IC.[Description]            
FROM   #TempSearchResultRanking SRR            
LEFT JOIN (            
  SELECT A.ItemCode, C.[Counter],D.[Description] FROM InventoryItem A WITH (NOLOCK)            
  INNER JOIN SystemManufacturer C WITH (NOLOCK) ON A.ManufacturerCode = C.ManufacturerCode            
  INNER JOIN SystemManufacturerWebOption B WITH (NOLOCK) ON C.ManufacturerCode = B.ManufacturerCode            
  INNER JOIN SystemManufacturerDescription D WITH (NOLOCK) ON B.ManufacturerCode = D.ManufacturerCode            
  WHERE Published = 1 AND C.IsActive = 1 AND D.LanguageCode = @LanguageCode AND WebSiteCode = @WebSiteCode            
     ) IC ON IC.ItemCode = SRR.ItemCode              
            
-----------------------------------------------              
---------------------------------------------------------------------------------------------------------------------------------------              
               
 SET @SQL  = ' SELECT * INTO #TempSearchResultTable FROM (              
 SELECT IID.ItemDescription, IIWOD.WebDescription,A.* ,SRR.CategoryID ,SRR.CategoryDisplayName ,SRR.ManufacturerID ,SRR.ManufacturerDisplayName,SRR.Ranking,IIWO.IsExclusive,IIWO.IsFeatured, IIWO.ShowPricePerPiece_C,      
 IIWO.PricePerPieceLabel_C, ROW_NUMBER() OVER (ORDER BY IID.ItemDescription, A.ItemName, A.ItemCode) AS RowNumber ' + @SKUAdditionalFields + @AdditionalField +          
  ' FROM InventoryItem A WITH (NOLOCK)                    
     LEFT JOIN (SELECT ItemCode FROM InventoryItemPricingDetail B WITH (NOLOCK)                                    
     INNER JOIN SystemCurrency C WITH (NOLOCK) ON B.CurrencyCode = C.CurrencyCode WHERE IsHomeCurrency = 1) D ON A.ItemCode = D.ItemCode                                       
  LEFT JOIN (SELECT TOP 1 ItemKitCode FROM InventoryKitPricingDetail E WITH (NOLOCK)                                       
     INNER JOIN SystemCurrency F WITH (NOLOCK) ON E.CurrencyCode = F.CurrencyCode WHERE IsHomeCurrency = 1) G ON A.ItemCode = G.ItemKitCode                                       
     INNER JOIN InventoryItemDescription IID WITH (NOLOCK) ON IID.ItemCode = A.ItemCode                                       
      AND IID.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + '                                       
     INNER JOIN InventoryItemWebOption IIWO WITH (NOLOCK) ON IIWO.ItemCode = A.ItemCode                                       
      AND IIWO.WebsiteCode = ' + QUOTENAME(@WebsiteCode, '''') + '                                       
     INNER JOIN InventoryItemWebOptionDescription IIWOD WITH (NOLOCK) ON IIWOD.ItemCode = IIWO.ItemCode            
      AND IIWOD.WebsiteCode = IIWO.WebsiteCode                                       
      AND IIWOD.LanguageCode = ' + QUOTENAME(@LanguageCode, '''') + ' ' +                                      
      @JoinUnitMeasureString + ' ' +                                      
      @EntityBuilderString  + ' ' +                                      
      @ProductFilter  + ' ' +         
      @AdditionalJoin + @SKUAdditionalJoin +  '              
      INNER JOIN #TempSearchResultRanking SRR WITH (NOLOCK) ON SRR.ItemCode = A.ItemCode      
     WHERE IsSale_C = 1 AND ItemType IN (''Non-Stock'',                                       
            ''Service'',                                       
            ''Stock'',                                      
            ''Matrix Group'',                                      
            ''Kit'',                                      
            ''Service'',                                      
            ''Electronic Download'',                                       
            ''Assembly'',            
            ''Gift Card'',            
            ''Gift Certificate'')                                      
       AND ''' + CAST(@CurrentDate AS NVARCHAR(50)) + ''' BETWEEN ISNULL(IIWO.StartDate, ''1/1/1900'')                                       
       AND ISNULL(IIWO.EndDate, ''12/31/9999'')                    
       AND (A.Status = ''A'' OR (A.Status = ''P'' AND A.ItemType IN (''Kit'', ''Matrix Group'')) OR (A.Status = ''P''                        
       AND A.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'') AND AI.FreeStock > 0))                               
       AND IIWO.Published = 1                                       
       AND IIWO.CheckOutOption = 0          
	   AND (ISNULL(IIWO.WebsiteGroup_C, '''') = ''Smartbag'' OR ISNULL(IIWO.WebsiteGroup_C, '''') = '''') 
	   AND (ISNULL(IIWO.NonStockMatrixType_C, '''') != ''Child'' OR ISNULL(IIWO.NonStockMatrixType_C, '''') = '''')                                                  
       AND ItemType LIKE ' + @ItemType +                 
        @AdditionalFilter + '                
       ) SortedResult;                 

	   DELETE #TempSearchResultTable WHERE CategoryID IS NULL                    
       --SELECT * FROM #TempSearchResultTable WHERE RowNumber BETWEEN ((' + CAST(@PageNum AS VARCHAR(10)) + ' - 1) * ' + CAST(@PageSize AS VARCHAR(20)) + ' + 1) AND ('           
       + CAST(@PageNum AS VARCHAR(20)) + ' * ' + CAST(@PageSize AS VARCHAR(20)) + ')              
       --ORDER BY SortOrder_C, Ranking DESC,ItemDescription, WebDescription,IsExclusive DESC;                 
	   SELECT * FROM #TempSearchResultTable ORDER BY SortOrder_C ASC
                   
       DECLARE @RCount INT              
       SELECT @RCount = COUNT(*) FROM #TempSearchResultTable                     
       SELECT CAST(CEILING(@RCount*1.0/'++ CAST(@PageSize AS VARCHAR(20))+') AS INT) pages, @RCount ProductCount'                

 EXEC(@SQL)  
		   
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#InventoryFreeStock'))          
 DROP TABLE #InventoryFreeStock          
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableItems'))          
 DROP TABLE #AvailableItems          
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                                                                            
 DROP TABLE #AvailableKitComponents          
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                                                                            
 DROP TABLE #AvailableMatrixAttributes     
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#TempInventoryUnitMeasure'))                                                                                    
 DROP TABLE #TempInventoryUnitMeasure          
           
 SET NOCOUNT OFF                                                                  
END     

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetExpressKitQtyDropdown]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetExpressKitQtyDropdown]
GO

CREATE PROCEDURE [dbo].[GetExpressKitQtyDropdown]
@KitItemCode NVARCHAR(60)
                                         
AS        
                                                                                                 
BEGIN                                                                        
SET NOCOUNT ON                                                                                   
               
DECLARE @ItemCode NVARCHAR(60)                                      
DECLARE @UOM NVARCHAR(60)   

SELECT @ItemCode = ItemCode, @UOM = UnitMeasureCode FROM InventoryKitDetail WHERE ItemKitCode = @KitItemCode AND IsDefault = 1 AND GroupCode = 'Express Bag' 
SELECT ROW_NUMBER() OVER (PARTITION BY ItemCode ORDER BY MaxQuantity ASC) RowNum, ItemCode, PricingAttribute_C, FLOOR(MaxQuantity) AS MaxQuantity 
INTO #InventoryPricingLevel FROM InventoryPricingLevel WHERE ItemCode = @ItemCode and UnitMeasureCode = @UOM

DELETE #InventoryPricingLevel WHERE RowNum IN (SELECT TOP 1 RowNum FROM #InventoryPricingLevel ORDER BY RowNum DESC)
SELECT * FROM #InventoryPricingLevel

IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#InventoryPricingLevel'))                                                                                      
    DROP TABLE #InventoryPricingLevel
END

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEcommerceKitCartDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetEcommerceKitCartDetail]
GO

CREATE PROCEDURE dbo.GetEcommerceKitCartDetail (                  
 @ItemKitCode  NVARCHAR(30),                            
 @CurrencyCode NVARCHAR(30),                            
 @LanguageCode NVARCHAR(30),                  
 @CustomerCode NVARCHAR(30),                  
 @CartID       UNIQUEIDENTIFIER,                
 @ContactCode  NVARCHAR(30),
 @WebsiteCode  NVARCHAR(30) = NULL               
)                            
AS          
                   
BEGIN
	SET NOCOUNT ON
	                  
 DECLARE @WarehouseCode NVARCHAR(30)                    
                                                  
 SET @WarehouseCode = (SELECT TOP 1 WarehouseCode                                                  
     FROM dbo.CustomerShipTo                                                  
     WHERE ShipToCode IN (SELECT TOP 1 DefaultShippingCode                                                  
     FROM  dbo.CRMContact                                                   
     WHERE ContactCode = @ContactCode))                     
                   
 IF( @LanguageCode is NULL or @LanguageCode = '' )                  
  SELECT @LanguageCode = CompanyLanguage FROM dbo.SystemCompanyInformation WITH (NOLOCK)                  
 ELSE                  
  SELECT @LanguageCode = LanguageCode FROM dbo.SystemLanguage WITH (NOLOCK) WHERE ShortString = @LanguageCode                  
                
 --CREATE TABLE #inventoryfreestock (ItemCode  NVARCHAR(50), FreeStock NUMERIC, UM NVARCHAR(30))
 CREATE TABLE #InventoryFreeStock (ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), InStock NUMERIC, UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))  
 CREATE TABLE #AvailableItems (ItemCode  NVARCHAR(50), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), InStock NUMERIC, UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))  
 
 DECLARE @AdditionalJoin NVARCHAR(1000)                               
 SET @AdditionalJoin = ''                               
                                                                 
 DECLARE @SqlQuery NVARCHAR(MAX)
 DECLARE @HideOutOfStockProducts BIT = 0                              
 DECLARE @ShowInventoryFromAllWarehouses BIT                             
 SET @ShowInventoryFromAllWarehouses = 0 
 
 SELECT @ShowInventoryFromAllWarehouses = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM dbo.EcommerceAppConfig WITH (NOLOCK) WHERE name = 'ShowInventoryFromAllWarehouses'                                
 SELECT @HideOutOfStockProducts = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM dbo.EcommerceAppConfig WITH (NOLOCK) WHERE name = 'HideOutOfStockProducts' AND WebSiteCode = @WebsiteCode        
 
IF @HideOutOfStockProducts = 1  
BEGIN  
	INSERT INTO #AvailableItems (ItemCode, WarehouseCode, FreeStock, InStock, UnitMeasureCode, POStatus)                                              
	SELECT EISTV.ItemCode, EISTV.WarehouseCode, FreeStock, UnitsInStock, EISTV.UnitMeasureCode, OrderStatus FROM EcommerceInventoryStockTotalView EISTV  
	INNER JOIN InventoryKitDetail IKD ON IKD.ItemCode = EISTV.ItemCode
	LEFT JOIN SupplierPurchaseOrderDetail SPOD ON SPOD.ItemCode = EISTV.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode    
	LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode    
	WHERE ItemKitCode = @ItemKitCode  
END  
ELSE  
BEGIN  
	INSERT INTO #AvailableItems (ItemCode, WarehouseCode, FreeStock, InStock, UnitMeasureCode, POStatus)  
	SELECT EISTV.ItemCode, WarehouseCode, FreeStock, UnitsInStock, EISTV.UnitMeasureCode, NULL FROM EcommerceInventoryStockTotalView EISTV
	INNER JOIN InventoryKitDetail IKD ON IKD.ItemCode = EISTV.ItemCode
	WHERE ItemKitCode = @ItemKitCode  
END
 
IF @ShowInventoryFromAllWarehouses = 1  
BEGIN  
	IF @HideOutOfStockProducts = 1  
	BEGIN 
		;WITH cte_InventoryFreeStock 
		AS
		(
			SELECT ItemCode, FreeStock, InStock, UnitMeasureCode, POStatus,
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum
			FROM #AvailableItems
		)
		INSERT INTO #InventoryFreeStock (ItemCode, FreeStock, InStock, UnitMeasureCode, POStatus)
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, SUM(InStock) AS InStock, UnitMeasureCode, POStatus
		FROM cte_InventoryFreeStock
		WHERE RowNum = 1
		GROUP BY ItemCode, UnitMeasureCode, POStatus
	END  
	ELSE  
	BEGIN  
		INSERT INTO #InventoryFreeStock(ItemCode, FreeStock, InStock, UnitMeasureCode, POStatus)  
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, SUM(InStock) AS InStock, UnitMeasureCode, NULL FROM #AvailableItems              
		GROUP BY ItemCode, UnitMeasureCode  
	END  
	
	SET @AdditionalJoin += ' LEFT OUTER JOIN #InventoryFreeStock IFS WITH (NOLOCK) ON IFS.ItemCode = I.ItemCode '             
END  
ELSE  
BEGIN  
	IF @HideOutOfStockProducts = 1  
	BEGIN
		;WITH cte_InventoryFreeStock 
		AS
		(
			SELECT ItemCode, FreeStock, InStock, WarehouseCode, UnitMeasureCode, POStatus,
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY POStatus DESC) as RowNum
			FROM #AvailableItems
			WHERE WarehouseCode = @WarehouseCode
		)
		INSERT INTO #InventoryFreeStock (ItemCode, FreeStock, InStock, UnitMeasureCode, POStatus)
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, SUM(InStock) AS InStock, UnitMeasureCode, POStatus
		FROM cte_InventoryFreeStock
		WHERE RowNum = 1
		GROUP BY ItemCode, UnitMeasureCode, POStatus 
	END  
	ELSE  
	BEGIN  
		INSERT INTO #InventoryFreeStock(ItemCode, FreeStock, InStock, UnitMeasureCode, POStatus)  
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, SUM(InStock) AS InStock, UnitMeasureCode, NULL FROM #AvailableItems  
		WHERE WarehouseCode = @WarehouseCode  
		GROUP BY ItemCode, UnitMeasureCode   
	END  
   
	SET @AdditionalJoin += ' INNER JOIN dbo.InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = i.ItemCode AND IsBase = 1              
   LEFT JOIN #inventoryfreestock IFS WITH (NOLOCK) ON IFS.ItemCode = I.ItemCode AND IFS.UnitMeasureCode = IUM.UnitMeasureCode '                               
END                         
              
 SET @SQLQuery = 'SELECT KD.Counter, KD.ItemKitCode, KD.GroupCode, KP.CurrencyCode, KD.ItemCode, i.ItemName,                  
  i.ItemType, i.Status, KDESC.ItemDescription, KDESC.ExtendedDescription, KDESC.PlainTextDescription, KDESC.HTMLDescription,                    
  KD.IsDefault, KD.Percentage, KD.UnitMeasureCode, ( SELECT UnitMeasureQty FROM InventoryUnitMeasure WITH (NOLOCK)                      
   WHERE KD.ItemCode = ItemCode AND UnitMeasureCode = KD.UnitMeasureCode ) UnitMeasureQuantity,                    
  KD.Quantity, KP.Discount, KP.SalesPrice, KP.SalesPriceRate, KP.RetailPrice, KP.RetailPriceRate,                    
  KP.StandardCost, KP.StandardCostRate, KP.Total, KP.TotalRate, KD.UserCreated, KD.DateCreated,               
  KD.UserModified, KD.DateModified, ikog.GroupType, IFS.FreeStock, IFS.POStatus AS POStatus                 
 FROM dbo.InventoryKitDetail KD WITH (NOLOCK)             
 INNER JOIN dbo.EcommerceKitCart wkc WITH (NOLOCK) ON ( wkc.ItemKitCode = kd.ItemKitCode AND wkc.ItemCode = kd.ItemCode AND               
 wkc.GroupCode = kd.GroupCode AND wkc.CustomerCode = '+ QUOTENAME(@CustomerCode, '''') +'AND wkc.CartID = '+ QUOTENAME(@CartID, '''') +')                  
 INNER JOIN dbo.InventoryKitDetailDescription KDESC WITH (NOLOCK) ON ( KD.ItemKitCode = KDESC.ItemKitCode AND KD.ItemCode = KDESC.ItemCode AND KD.GroupCode = KDESC.GroupCode)                  
 INNER JOIN dbo.InventoryKitPricingDetail KP WITH (NOLOCK) ON KD.ItemKitCode = KP.ItemKitCode AND KD.GroupCode = KP.GroupCode AND KD.ItemCode =KP.ItemCode                    
 INNER JOIN dbo.SystemUnitMeasure um WITH (NOLOCK) ON kd.UnitMeasureCode = um.UnitMeasureCode      
 INNER JOIN dbo.InventoryItemPricingDetail iipd ON iipd.ItemCode = kp.ItemCode          
 INNER JOIN dbo.InventoryKitOptionGroup ikog WITH (NOLOCK) ON ikog.ItemKitCode = wkc.ItemKitCode AND ikog.GroupCode = ikog.GroupCode AND kd.GroupCode = ikog.GroupCode                  
 INNER JOIN dbo.InventoryItem I WITH (NOLOCK) ON i.ItemCode = kd.ItemCode' + @AdditionalJoin +                  
 + 'WHERE KD.ItemKitCode = ISNULL('+ QUOTENAME(@ItemKitCode, '''') +', KD.ItemKitCode) AND LanguageCode = '+ QUOTENAME(@LanguageCode, '''') + '        
  AND iipd.CurrencyCode = ' + QUOTENAME(@CurrencyCode , '''') + ' AND  KP.CurrencyCode =ISNULL('+ QUOTENAME(@CurrencyCode, '''') +', kp.CurrencyCode) AND KD.UnitMeasureCode = IFS.UnitMeasureCode'                   
  SET @SqlQuery = Replace(Replace(@SqlQuery, CHAR(13), ' '), CHAR(10), ' ')            
                                       
  EXECUTE SP_EXECUTESQL @SQlQuery
  

/* Drop temporary table */
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#InventoryFreeStock'))                                                                  
	DROP TABLE #InventoryFreeStock
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableItems'))                                                                  
	DROP TABLE #AvailableItems                
	
	SET NOCOUNT OFF
END   

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE name = 'EcommerceProductInfoView' AND type = N'V')
    DROP VIEW [dbo].[EcommerceProductInfoView]
GO

CREATE VIEW dbo.EcommerceProductInfoView
AS
SELECT     A.Counter, A.ItemCode, A.ItemName, A.ItemType, A.Status, B.StartDate, B.EndDate, C.Summary, C.Warranty, D.ItemDescription, C.WebDescription, C.SEName, 
                      C.SEKeywords, C.SEDescription, C.SETitle, C.SENoScript, C.SEAltText, NULL AS SpecTitle, NULL AS MiscText, NULL AS SwatchImageMap, NULL 
                      AS IsFeaturedTeaser, NULL AS FroogleDescription, NULL AS SizeOptionPrompt, NULL AS ColorOptionPrompt, NULL AS TextOptionPrompt, 1 AS ProductTypeID, NULL 
                      AS SKU, NULL AS ManufacturerpartNumber, 1 AS SalesPromptID, NULL AS SpecCall, 0 AS SpecsInline, B.WebSiteCode, B.IsFeatured, B.XMLPackage, 
                      B.MobileXMLPackage, B.DisplayColumns, B.Published, 1 AS Wholesale, B.RequiresRegistration, B.CheckOutOption, B.CheckOutOptionAddMessage, 
                      1 AS Looks, NULL AS Notes, NULL AS QuantityDiscountID, NULL AS RelatedProducts, NULL AS UpsellProducts, .000 AS UpsellProductDiscountPercentage, NULL 
                      AS RelatedDocuments, 0 AS TrackInventoryBySizeAndColor, 1 AS TrackInventoryBySize, 1 AS TrackInventoryByColor, 
                      CASE a.ItemType WHEN 'Kit' THEN 1 ELSE 0 END AS IsAKit, CASE WHEN ItemType = 'Matrix Group' THEN 1 ELSE 0 END AS IsMatrix, 0 AS PackSize, 
                      B.ShowBuyButton, NULL AS RequiresProducts, B.HidePriceUntilCart, B.IsCallToOrder, 0 AS ExcludedFromPriceFeeds, 0 AS RequiresTextOption, NULL 
                      AS TextOptionMaxLength, NULL AS ExtensionData, NULL AS ExtensionData2, NULL AS ExtensionData3, NULL AS ExtensionData4, NULL AS ExtensionData5, NULL 
                      AS ContentsBGColor, NULL AS PageBGColor, NULL AS GraphicsColor, NULL AS ImageFilenameOverride, 0 AS IsImport, 0 AS IsSystem, 0 AS Deleted, 
                      B.DateCreated AS CreatedOn, 20 AS PageSize, NULL AS WarehouseLocation, NULL AS ISSItemCode, A.Counter AS VariantID, '' AS VariantName, 1.00 AS Price, 
                      '' AS VariantDescription, .000 AS SalePrice, '' AS SkuSuffix, NULL AS Dimensions, ium.WeightInKilograms, ium.WeightInPounds, C.LanguageCode, 0 AS Points, 
                      'Promotional Price' AS SalesPromptName, .000 AS ExtendedPrice, CASE a.ItemType WHEN 'Stock' THEN
                          (SELECT     FreeStock
                            FROM          AvailabilityView
                            WHERE      ItemCode = a.ItemCode AND WarehouseCode = 'MAIN' AND UM = 'EACH') ELSE 0 END AS FreeStock, ISNULL(ShipDate.ExpShipingDate, N'') 
                      AS ExpShipingDate, A.IsCBN, B.ShipNote, A.MaterialType_C
FROM         dbo.InventoryItem AS A WITH (NOLOCK) INNER JOIN
                      dbo.InventoryItemWebOption AS B WITH (NOLOCK) ON A.ItemCode = B.ItemCode INNER JOIN
                      dbo.InventoryItemWebOptionDescription AS C WITH (NOLOCK) ON B.ItemCode = C.ItemCode AND B.WebSiteCode = C.WebSiteCode INNER JOIN
                      dbo.InventoryItemDescription AS D WITH (NOLOCK) ON A.ItemCode = D.ItemCode AND C.LanguageCode = D.LanguageCode INNER JOIN
                      dbo.InventoryUnitMeasure AS ium WITH (NOLOCK) ON A.ItemCode = ium.ItemCode AND A.ItemCode = ium.ItemCode LEFT OUTER JOIN
                          (SELECT     MIN(PO.DateExpected) AS ExpShipingDate, POD.ItemCode
                            FROM          dbo.SupplierPurchaseOrderDetail AS POD INNER JOIN
                                                       (SELECT     DateExpected, PurchaseOrderCode
                                                         FROM          dbo.SupplierPurchaseOrder
                                                         WHERE      (DateExpected > GETDATE())) AS PO ON PO.PurchaseOrderCode = POD.PurchaseOrderCode
                            WHERE      (POD.QuantityReserved < POD.QuantityOrdered)
                            GROUP BY POD.ItemCode) AS ShipDate ON A.ItemCode = ShipDate.ItemCode
WHERE     (ium.IsBase = 1)

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[EcommerceProductInfo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EcommerceProductInfo]
GO

CREATE PROCEDURE [dbo].[EcommerceProductInfo](                                        
 @ItemCode NVARCHAR(1000),                                            
 @LanguageCode NVARCHAR(50),                                            
 @UserCode NVARCHAR(30)= NULL,                                          
 @WebSiteCode NVARCHAR(30),                                         
 @CurrentDate DATETIME,                                       
 @ProductFilterID NVARCHAR(50) = NULL,                                      
 @ContactCode NVARCHAR(30),                        
 @IsShowOnlyMatrixItems BIT = 0,      
 @CurrencyCode NVARCHAR(30) = NULL                                      
)                                        
AS                         
BEGIN      
 SET NOCOUNT ON                                          
                                          
 /*use company preference language info if language code IS NOT specified*/                                            
 IF @LanguageCode IS NULL OR @LanguageCode = ''                                            
 BEGIN                                            
  SELECT @LanguageCode = CompanyLanguage                                         
  FROM SystemCompanyInformation WITH (NOLOCK)                                         
 END                                            
 ELSE                                            
 BEGIN -- must be shortstring?                                            
  SELECT @LanguageCode = LanguageCode                                         
  FROM SystemLanguage WITH (NOLOCK)                                         
  WHERE ShortString = @LanguageCode                                            
 END                                          
                                        
 IF @ProductFilterID IS NULL OR ( @ProductFilterID = '00000000-0000-0000-0000-000000000000' AND EXISTS (SELECT 1 FROM EcommerceAppConfig WHERE NAME = 'AllowProductFiltering' AND WebSiteCode = @WebsiteCode AND ConfigValue = 'TRUE') )                       
                                                                                               
 SET @ProductFilterID = ''                                      
                                        
  DECLARE @QueryProductFilter NVARCHAR(500)                                      
  SET @QueryProductFilter = ''                                      
                                        
  IF @ProductFilterID <> ''                                      
 SET @QueryProductFilter = 'INNER JOIN (SELECT DISTINCT (ItemCode), TemplateID FROM InventoryProductFilterTemplateItem WHERE TemplateID = ''' + @ProductFilterID + ''') PF ON a.ItemCode = PF.ItemCode AND PF.TemplateID = ''' + @ProductFilterID + ''' '      

    
  DECLARE @UserCodeQuery NVARCHAR(200)                                      
                                        
  IF @UserCode IS NULL                                      
 SET @UserCodeQuery = 'NULL'                                      
  ELSE                                      
 SET @UserCodeQuery = '''' + @UserCode + ''''                         
                         
 /* Out of Stock Items */                                      
CREATE TABLE #InventoryFreeStock (WarehouseCode NVARCHAR(30), ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))                            
CREATE TABLE #AvailableItems (ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), UM NVARCHAR(30))                                                            
                                        
DECLARE @AdditionalJoin NVARCHAR(1000)                          
DECLARE @HideOutOfStockProducts BIT                                                                       
DECLARE @ShowInventoryFromAllWarehouses BIT                                                                                                
DECLARE @WarehouseCode NVARCHAR(30)                                                                  
                                                            
SET @AdditionalJoin = ''                             
SET @HideOutOfStockProducts =  0                                   
SET @ShowInventoryFromAllWarehouses = 0                                          
                                  
SELECT @HideOutOfStockProducts = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'HideOutOfStockProducts' AND WebSiteCode = @WebSiteCode              
SELECT @ShowInventoryFromAllWarehouses = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM EcommerceAppConfig WITH (NOLOCK) WHERE name = 'ShowInventoryFromAllWarehouses' AND WebSiteCode = @WebSiteCode              
SET @WarehouseCode = (SELECT TOP 1 WareHouseCode FROM CustomerShipTo WHERE ShipToCode = (SELECT DefaultShippingCode FROM CRMContact WHERE ContactCode = @ContactCode))                            
     
-- temporary tables to store available kit components and matrix attributes  
CREATE TABLE #AvailableKitComponents(ItemKitCode NVARCHAR(30), ItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), ItemType NVARCHAR(30), POStatus NVARCHAR(30))
CREATE TABLE #AvailableMatrixAttributes (ItemCode NVARCHAR(30), MatrixItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))

-- Get freestock of non-kit/non-matrix group items (Stock, Matrix Item, Assembly)   
IF @HideOutOfStockProducts = 1      
BEGIN      
	IF (SELECT ItemType FROM InventoryItem WHERE ItemCode = @ItemCode) IN ('Stock', 'Assembly', 'Matrix Item')      
	BEGIN      
		INSERT INTO #inventoryfreestock (ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)        
		SELECT eistv.ItemCode, eistv.WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM EcommerceInventoryStockTotalView eistv        
		LEFT JOIN SupplierPurchaseOrderDetail spod ON spod.ItemCode = eistv.ItemCode AND spod.WarehouseCode = eistv.WarehouseCode AND spod.UnitMeasure = eistv.UnitMeasureCode        
		LEFT JOIN SupplierPurchaseOrder spo ON spo.PurchaseOrderCode = spod.PurchaseOrderCode        
		WHERE eistv.ItemCode = @ItemCode      
	END      
	ELSE      
	BEGIN        
	IF (SELECT ItemType FROM InventoryItem WHERE ItemCode = @ItemCode) = 'Kit'      
	BEGIN      
		-- get the freestock of kit component/s      
		;WITH cte_AvailableKitComponent       
		AS      
		(      
			SELECT IKD.ItemKitCode, IKD.ItemCode AS ItemCode, 
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN @WarehouseCode
				ELSE EISTV.WarehouseCode
			END AS WarehouseCode,
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN 0
				ELSE EISTV.FreeStock
			END AS FreeStock, IKD.UnitMeasureCode, II.ItemType, OrderStatus,        
			ROW_NUMBER() over(PARTITION BY  IKD.ItemKitCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum      
			FROM InventoryItem II WITH (NOLOCK)
			INNER JOIN InventoryKitDetail IKD ON II.ItemCode = IKD.ItemCode         
			LEFT JOIN EcommerceInventoryStockTotalView EISTV ON IKD.ItemCode = EISTV.ItemCode      
			LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IKD.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode        
			LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode           
			WHERE ItemKitCode = @ItemCode      
		)      
		INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType, POStatus)      
		SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType, OrderStatus FROM cte_AvailableKitComponent WHERE RowNum = 1        
	END        
	     
	IF (SELECT ItemType FROM InventoryItem WHERE ItemCode = @ItemCode) = 'Matrix Group'       
	BEGIN                
		-- get the freestock of matrix item attribute/s      
		;WITH cte_AvailableMatrixAttributes      
		AS      
		(      
			SELECT IMI.ItemCode, IMI.MatrixItemCode, EISTV.WarehouseCode , EISTV.FreeStock, EISTV.UnitMeasureCode, OrderStatus,        
			ROW_NUMBER() over(PARTITION BY  IMI.ItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum      
			FROM InventoryMatrixItem IMI      
			INNER JOIN InventoryItem II ON II.ItemCode = IMI.MatrixItemCode       
			INNER JOIN EcommerceInventoryStockTotalView EISTV ON IMI.MatrixItemCode = EISTV.ItemCode      
			LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IMI.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode        
			LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode        
			WHERE (EISTV.FreeStock > 0 OR OrderStatus IN ('Open', 'Partial')) AND IMI.ItemCode = @ItemCode      
		)      
		INSERT INTO #AvailableMatrixAttributes (ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)      
		SELECT ItemCode, MatrixItemCode, WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM cte_AvailableMatrixAttributes WHERE RowNum = 1      
	END      
	    
	IF (SELECT ItemType FROM InventoryItem WHERE ItemCode = @ItemCode) IN ('Service', 'Electronic Download', 'Non-Stock', 'Gift Card', 'Gift Certificate')      
	BEGIN      
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)      
		SELECT II.ItemCode, 0, UnitMeasureCode      
		FROM InventoryItem II      
		INNER JOIN InventoryUnitMeasure IUM ON IUM.ItemCode = II.ItemCode      
		WHERE II.ItemCode = @ItemCode      
	END      
	END      
END   
ELSE      
BEGIN      
	IF (SELECT ItemType FROM InventoryItem WHERE ItemCode = @ItemCode) IN ('Stock', 'Assembly', 'Matrix Item')      
	BEGIN      
		INSERT INTO #inventoryfreestock (ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)      
		SELECT eistv.ItemCode, eistv.WarehouseCode, FreeStock, UnitMeasureCode, NULL FROM EcommerceInventoryStockTotalView eistv      
		WHERE eistv.ItemCode = @ItemCode      
	END 
	
	IF (SELECT ItemType FROM InventoryItem WHERE ItemCode = @ItemCode) = 'Matrix Group'   
	BEGIN 
		;WITH cte_AvailableMatrixAttributes                     
		AS                      
		(    
			SELECT IMI.ItemCode, IMI.MatrixItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode, EISTV.FreeStock AS FreeStock
			FROM InventoryItem II WITH (NOLOCK)
			INNER JOIN InventoryMatrixItem IMI WITH (NOLOCK) ON II.ItemCode = IMI.MatrixItemCode
			INNER JOIN EcommerceInventoryStockTotalView EISTV WITH (NOLOCK) ON IMI.MatrixItemCode = EISTV.ItemCode 
			WHERE (II.Status = 'A' OR (II.Status = 'P' AND II.ItemType = 'Matrix Item' AND EISTV.FreeStock > 0 ))
			AND IMI.ItemCode = @ItemCode
		)
		INSERT INTO #AvailableMatrixAttributes (ItemCode, MatrixItemCode, WarehouseCode, UnitMeasureCode, FreeStock)                      
		SELECT ItemCode, MatrixItemCode, WarehouseCode, UnitMeasureCode, FreeStock FROM cte_AvailableMatrixAttributes
	END  
 
	IF (SELECT ItemType FROM InventoryItem WHERE ItemCode = @ItemCode) = 'Kit'  
	BEGIN               
		;WITH cte_AvailableKitComponent                      
		AS                      
		(  
			SELECT IKD.ItemKitCode, IKD.ItemCode,
			CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN @WarehouseCode
				ELSE EISTV.WarehouseCode
			 END AS WarehouseCode,
			 CASE
				WHEN II.ItemType IN ('Non-Stock', 'Electronic Download', 'Service') THEN 0
				ELSE EISTV.FreeStock
			 END AS FreeStock, IKD.UnitMeasureCode, II.ItemType
			FROM InventoryItem II WITH (NOLOCK)
			INNER JOIN InventoryKitDetail IKD WITH (NOLOCK) ON II.ItemCode = IKD.ItemCode
			LEFT JOIN EcommerceInventoryStockTotalView EISTV WITH (NOLOCK) ON IKD.ItemCode = EISTV.ItemCode 
			WHERE (II.Status = 'A' OR (II.Status = 'P' AND                 
			II.ItemType IN ('Stock', 'Non-Stock', 'Electronic Download', 'Service', 'Assembly', 'Matrix Item') AND EISTV.FreeStock > 0 ))
			AND IKD.ItemKitCode = @ItemCode    
	   ) 
	   INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType)                      
       SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, ItemType FROM cte_AvailableKitComponent                                  
   END     
   ELSE      
   BEGIN      
	   INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)      
	   SELECT II.ItemCode, 0, UnitMeasureCode      
       FROM InventoryItem II      
       INNER JOIN InventoryUnitMeasure IUM ON IUM.ItemCode = II.ItemCode      
       WHERE II.ItemCode = @ItemCode AND ItemType IN ('Service', 'Electronic Download', 'Non-Stock', 'Gift Card', 'Gift Certificate' )     
  END      
END    
         
-- FreeStock from all warehouse
IF @ShowInventoryFromAllWarehouses = 1        
BEGIN        
	IF @HideOutOfStockProducts = 1        
	BEGIN        
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                              
		BEGIN                        
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                           
			BEGIN                        
				;WITH cte_AvailableKitComponents 
				AS
				(
					SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode, ItemType, POStatus,
					ROW_NUMBER() over(PARTITION BY ItemKitCode, UnitMeasureCode ORDER BY ItemKitCode) as RowNum                        
					FROM #AvailableKitComponents                                  
					GROUP BY ItemKitCode, UnitMeasureCode, ItemType, POStatus 
				)
				
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)
				SELECT ItemKitCode, FreeStock, UnitMeasureCode
				FROM cte_AvailableKitComponents
				WHERE RowNum = 1 AND ((FreeStock > 0 OR POStatus IN ('Open', 'Partial')) OR ItemType IN ('Non-Stock', 'Electronic Download', 'Service'))
				GROUP BY ItemKitCode, UnitMeasureCode, FreeStock                      
			END                          
		END      
	    
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))            
		BEGIN            
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0         
			BEGIN      
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)      
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode      
				FROM #AvailableMatrixAttributes      
				GROUP BY ItemCode, UnitMeasureCode      
			END         
		END        
	       
		;WITH cte_InventoryFreeStock       
		AS      
		(      
			SELECT ItemCode, FreeStock, WarehouseCode, UnitMeasureCode,      
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY ItemCode DESC) as RowNum      
			FROM #InventoryFreeStock      
			WHERE FreeStock > 0 OR POStatus IN ('Open', 'Partial')      
		)      
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)      
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode      
		FROM cte_InventoryFreeStock      
		WHERE RowNum = 1      
		GROUP BY ItemCode, UnitMeasureCode      
		    
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = a.ItemCode AND IsBase = 1                                          
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = a.ItemCode AND AI.UM = IUM.UnitMeasureCode '                
	END        
	ELSE        
	BEGIN 
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))            
		BEGIN            
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0         
			BEGIN       
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)        
				SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode      
				FROM #AvailableKitComponents        
				GROUP BY ItemKitCode, UnitMeasureCode      
			END        
		END      

		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))            
		BEGIN            
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0         
			BEGIN      
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)      
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode      
				FROM #AvailableMatrixAttributes          
				GROUP BY ItemCode, UnitMeasureCode        
			END        
		 END   
	 
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                      
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode FROM #inventoryfreestock        
		GROUP BY ItemCode, UnitMeasureCode   
		  
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = a.ItemCode AND IsBase = 1                                          
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = a.ItemCode AND AI.UM = IUM.UnitMeasureCode '                
	END        
END        
-- FreeStock from specific warehouse        
ELSE        
BEGIN                                                 
	IF @HideOutOfStockProducts = 1                                            
	BEGIN        
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                              
		BEGIN                              
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0                           
			BEGIN                         
				;WITH cte_AvailableKitComponents 
				AS
				(
					SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode, ItemType, POStatus,
					ROW_NUMBER() over(PARTITION BY ItemKitCode, UnitMeasureCode ORDER BY ItemKitCode) as RowNum                        
					FROM #AvailableKitComponents                        
					WHERE WarehouseCode = @WarehouseCode          
					GROUP BY ItemKitCode, UnitMeasureCode, ItemType, POStatus 
				)
				
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)
				SELECT ItemKitCode, FreeStock, UnitMeasureCode
				FROM cte_AvailableKitComponents
				WHERE RowNum = 1 AND ((FreeStock > 0 OR POStatus IN ('Open', 'Partial')) OR ItemType IN ('Non-Stock', 'Electronic Download', 'Service'))
				GROUP BY ItemKitCode, UnitMeasureCode, FreeStock                     
			END                          
		END      
	        
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))            
		BEGIN            
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0         
			BEGIN      
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)      
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode      
				FROM #AvailableMatrixAttributes     
				WHERE WarehouseCode = @WarehouseCode      
				GROUP BY ItemCode, UnitMeasureCode        
			END        
		END      
	    
		;WITH cte_InventoryFreeStock       
		AS      
		(      
			SELECT ItemCode, FreeStock, WarehouseCode, UnitMeasureCode,      
			ROW_NUMBER() over(PARTITION BY ItemCode, FreeStock, WarehouseCode, UnitMeasureCode ORDER BY ItemCode DESC) as RowNum      
			FROM #InventoryFreeStock      
			WHERE WarehouseCode = @WarehouseCode AND (FreeStock > 0 OR POStatus IN ('Open', 'Partial'))      
		)      
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)      
		SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode      
		FROM cte_InventoryFreeStock      
		WHERE RowNum = 1       
		GROUP BY ItemCode, UnitMeasureCode                                 
		                    
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = a.ItemCode AND IsBase = 1                                          
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = a.ItemCode AND AI.UM = IUM.UnitMeasureCode '                                                        
	END      
	ELSE                                            
	BEGIN 
		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))            
		BEGIN            
			IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0         
			BEGIN       
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)        
				SELECT ItemKitCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode      
				FROM #AvailableKitComponents      
				WHERE WarehouseCode = @WarehouseCode      
				GROUP BY ItemKitCode, UnitMeasureCode      
			END        
		END      

		IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))            
		BEGIN            
			IF (SELECT COUNT(ItemCode) FROM #AvailableMatrixAttributes) > 0         
			BEGIN      
				INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)      
				SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode      
				FROM #AvailableMatrixAttributes      
				WHERE WarehouseCode = @WarehouseCode      
				GROUP BY ItemCode, UnitMeasureCode        
			END        
		 END   
	                                     
		INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                           
		SELECT ItemCode, FreeStock, UnitMeasureCode FROM #inventoryfreestock                                            
		WHERE WarehouseCode = @WarehouseCode       
		   
		SET @AdditionalJoin += ' INNER JOIN InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = a.ItemCode AND IsBase = 1                                          
		INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = a.ItemCode AND AI.UM = IUM.UnitMeasureCode '                                                    
	END                                               
END  
         
/*End of Out of Stock Items*/                           
IF @CurrencyCode IS NULL      
BEGIN      
 SELECT @CurrencyCode = CurrencyCode      
 FROM Customer CU WITH (NOLOCK)      
 INNER JOIN CRMContact CO WITH (NOLOCK) ON CO.EntityCode = CU.CustomerCode       
  AND CO.ContactCode = @ContactCode      
END      

/* filter giftcard or giftcertificate items that are not sellable to customer's currency */ 
DECLARE @Sellable BIT = 1      
SELECT @Sellable = ISNULL(IP.[Enabled], 1)       
FROM InventoryItem II WITH (NOLOCK)
INNER JOIN InventoryItemPricingDetail IP WITH (NOLOCK) ON IP.ItemCode = II.ItemCode 
WHERE CurrencyCode = @CurrencyCode 
	AND II.ItemCode = @ItemCode     
	AND II.ItemType IN ('Gift Card', 'Gift Certificate')  
      
 IF @IsShowOnlyMatrixItems = 0                                          
 BEGIN                                        
  exec ('SELECT  a.*, iiwo.IsSale_C, iiwo.ShowPricePerPiece_C, iiwo.PricePerPieceLabel_C, REPLACE(REPLACE(iiwo.StickerInfo_C,''Sticker Size:<br>Height: '',''''),''<br>Width: '','' x '') AS CustomStickerInfo_C,
	iiwo.BagperCarton_C,
    CASE                                            
     WHEN ' + @UserCodeQuery + ' IS NULL THEN 0                                          
     WHEN ' + @UserCodeQuery + ' IS NOT NULL THEN (                                             
      SELECT CASE                                            
        WHEN sup.UnitMeasureSystem = ''English'' THEN IsNull(a.WeightInPounds,0)                                            
        WHEN sup.UnitMeasureSystem = ''Metric'' THEN IsNull(a.WeightInKilograms,0)                                            
        ELSE 0                                           
        END                                          
      FROM SystemUserPreference sup WITH (NOLOCK)                                         
      WHERE UserCode = ' +  @UserCodeQuery + '                                      
     )                        
     ELSE 0                                           
    END AS Weight                                        
  FROM EcommerceProductInfoView a ' + @QueryProductFilter + '' + @AdditionalJoin + '
  INNER JOIN InventoryItemWebOption iiwo ON A.ItemCode = iiwo.ItemCode                                   
  WHERE a.ItemCode = ''' + @ItemCode + ''' AND                                         
    a.LanguageCode = ''' + @LanguageCode + ''' AND                                         
    a.WebSiteCode = ''' + @WebSiteCode + ''' AND                
   (a.Status = ''A'' OR (a.Status IN (''A'', ''P'') AND a.ItemType IN (''Kit'', ''Matrix Group'')) OR (a.Status = ''P'' AND                       
    a.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'') AND AI.FreeStock > 0)) AND        
    (                                        
     (a.StartDate IS NULL AND a.EndDate IS NULL) OR                                         
     (a.StartDate IS NOT NULL AND a.StartDate <= ''' + @CurrentDate + ''' AND a.EndDate IS NULL) OR                                          
     (a.StartDate IS NULL AND a.EndDate IS NOT NULL AND a.EndDate >= ''' + @CurrentDate + ''') OR                                          
     (''' + @CurrentDate + ''' BETWEEN a.StartDate AND a.EndDate)                                        
    ) AND 1=' + @Sellable)                           
 END                    
 ELSE                                          
 BEGIN                                          
    
  EXEC ('SELECT a.*, iiwo.IsSale_C, iiwo.ShowPricePerPiece_C, iiwo.PricePerPieceLabel_C, iiwo.StickerInfo_C, iiwo.BagperCarton_C,
    CASE                                        
     WHEN ' + @UserCodeQuery + ' IS NULL THEN 0                                          
     WHEN ' + @UserCodeQuery + ' IS NOT NULL THEN (                                             
      SELECT CASE                                            
      WHEN sup.UnitMeasureSystem = ''English'' THEN IsNull(a.WeightInPounds,0)                                  
      WHEN sup.UnitMeasureSystem = ''Metric'' THEN IsNull(a.WeightInKilograms,0)                                            
      ELSE 0                                         
      END                                          
      FROM SystemUserPreference sup WITH (NOLOCK)                                         
      WHERE UserCode = ' +  @UserCodeQuery + '                                     
     )                                          
     ELSE 0                                           
    END AS Weight                                            
  FROM EcommerceProductInfoView a   ' + @QueryProductFilter + '' + @AdditionalJoin + '                                       
  INNER JOIN InventoryMatrixItem imi WITH (NOLOCK) ON a.ItemCode = imi.MatrixItemCode AND imi.ItemCode = ''' + @ItemCode + '''   
  INNER JOIN InventoryItemWebOption iiwo ON A.ItemCode = iiwo.ItemCode                                                                          
  WHERE a.LanguageCode = ''' + @LanguageCode + ''' AND                                         
    a.WebSiteCode = ''' + @WebSiteCode + ''' AND                                         
    imi.Selected = 1 AND                
    (a.Status = ''A'' OR (a.Status IN (''A'', ''P'') AND a.ItemType IN (''Kit'', ''Matrix Group'')) OR (a.Status = ''P'' AND                       
    a.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Gift Card'', ''Gift Certificate'') AND AI.FreeStock > 0)) AND                                         
    a.Published = 1 AND                                  
    a.CheckOutOption = 0 AND                                         
    (                                        
     (a.StartDate IS NULL AND a.EndDate IS NULL) OR                                          
     (a.StartDate IS NOT NULL AND a.StartDate <= ''' + @CurrentDate + ''' AND a.EndDate IS NULL) OR                                          
     (a.StartDate IS NULL AND a.EndDate IS NOT NULL AND a.EndDate >= ''' + @CurrentDate + ''') OR                                        
     (''' + @CurrentDate + ''' BETWEEN a.StartDate AND a.EndDate)                                        
    ) AND 1=' + @Sellable)                         
 END      
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#InventoryFreeStock'))      
 DROP TABLE #InventoryFreeStock      
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableItems'))      
 DROP TABLE #AvailableItems      
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                                                                        
 DROP TABLE #AvailableKitComponents      
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableMatrixAttributes'))                                                                        
 DROP TABLE #AvailableMatrixAttributes      
 
 SET NOCOUNT OFF      
END                 
                
GO              
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[GetEcommerceKitItems]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetEcommerceKitItems]
GO

CREATE PROCEDURE GetEcommerceKitItems(                              
 @ItemKitCode  NVARCHAR(50),                                
 @CurrencyCode NVARCHAR(50),                                
 @LanguageCode NVARCHAR(50),                                
 @CustomerCode NVARCHAR(30),                                
 @CartID    UNIQUEIDENTIFIER,                               
 @WebsiteCode NVARCHAR(30),                              
 @IsAnonymous BIT,                              
 @AnonymousCustomerCode NVARCHAR(30),                      
 @ContactCode NVARCHAR(30),
 @IncludeAllGroup BIT                              
)                              
AS          
            
BEGIN    
 SET NOCOUNT ON    
          
/* Out of Stock Items */                                               
CREATE TABLE #AvailableItems (ItemCode  NVARCHAR(50), FreeStock NUMERIC(18,6), UM NVARCHAR(30))                  
                                    
DECLARE @AdditionalJoin NVARCHAR(1000) = ''                 
DECLARE @AdditionalField NVARCHAR(100) = ''                                                 
DECLARE @SqlQuery NVARCHAR(MAX) = ''                  
DECLARE @SqlQuery2 NVARCHAR(MAX) = ''                    
DECLARE @HideOutOfStockProducts BIT = 0                                                                  
DECLARE @ShowInventoryFromAllWarehouses BIT = 0                                                                                           
DECLARE @WarehouseCode NVARCHAR(30)          
DECLARE @CustomSelectQry NVARCHAR(1000) = ''              
DECLARE @CustomJoinQry NVARCHAR(1000) = ''   
                   
                                                                                                                                                               
SELECT @HideOutOfStockProducts = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM dbo.EcommerceAppConfig WITH (NOLOCK) WHERE name = 'HideOutOfStockProducts' AND WebSiteCode = @WebsiteCode            
SELECT @ShowInventoryFromAllWarehouses = CASE configvalue WHEN 'true' THEN 1 ELSE 0 END FROM dbo.EcommerceAppConfig WITH (NOLOCK) WHERE name = 'ShowInventoryFromAllWarehouses' AND WebSiteCode = @WebsiteCode            
SET @WarehouseCode = (SELECT TOP 1 WareHouseCode FROM dbo.CustomerShipTo WHERE ShipToCode = (SELECT DefaultShippingCode FROM dbo.CRMContact WHERE ContactCode = @ContactCode))                             
                                                
INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                        
SELECT II.ItemCode, 0, IKD.UnitMeasureCode FROM dbo.InventoryItem II    
INNER JOIN InventoryKitDetail IKD ON IKD.ItemCode = II.ItemCode                        
INNER JOIN dbo.InventoryUnitMeasure IUM ON II.ItemCode = IUM.ItemCode                                 
WHERE ItemType IN ( 'Non-Stock', 'Electronic Download', 'Service' )                        
AND IsBase = 1 AND ItemKitCode = @ItemKitCode    
      
IF @HideOutOfStockProducts = 1    
BEGIN    
 CREATE TABLE #AvailableKitComponents (ItemKitCode NVARCHAR(30), ItemCode NVARCHAR(30), WarehouseCode NVARCHAR(30), FreeStock NUMERIC(18,6), UnitMeasureCode NVARCHAR(30), POStatus NVARCHAR(30))       
        
 -- get the freestock of kit component/s    
 ;WITH cte_AvailableKitComponent     
  AS    
  (    
   SELECT IKD.ItemKitCode, IKD.ItemCode AS ItemCode, EISTV.WarehouseCode , EISTV.FreeStock, EISTV.UnitMeasureCode, OrderStatus,      
   ROW_NUMBER() over(PARTITION BY  IKD.ItemCode, EISTV.WarehouseCode, EISTV.UnitMeasureCode ORDER BY EISTV.FreeStock DESC, OrderStatus DESC) as RowNum    
   FROM InventoryKitDetail IKD       
   INNER JOIN EcommerceInventoryStockTotalView EISTV ON IKD.ItemCode = EISTV.ItemCode    
   LEFT JOIN SupplierPurchaseOrderDetail SPOD ON spod.ItemCode = IKD.ItemCode AND SPOD.WarehouseCode = EISTV.WarehouseCode AND SPOD.UnitMeasure = EISTV.UnitMeasureCode      
   LEFT JOIN SupplierPurchaseOrder SPO ON SPO.PurchaseOrderCode = SPOD.PurchaseOrderCode      
   WHERE (EISTV.FreeStock > 0 OR OrderStatus IN ('Open', 'Partial'))    
   AND ItemKitCode = @ItemKitCode    
  )    
  INSERT INTO #AvailableKitComponents (ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, POStatus)    
  SELECT ItemKitCode, ItemCode, WarehouseCode, FreeStock, UnitMeasureCode, OrderStatus FROM cte_AvailableKitComponent WHERE RowNum = 1    
END              
                  
-- All Warehouse                                     
IF @ShowInventoryFromAllWarehouses = 1                                      
 BEGIN                 
  IF @HideOutOfStockProducts = 1                                  
   BEGIN                         
  IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))          
  BEGIN    
   IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0       
   BEGIN    
    INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)      
    SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode    
    FROM #AvailableKitComponents    
    GROUP BY ItemCode, UnitMeasureCode    
   END      
  END                                   
                                  
    SET @AdditionalJoin += ' INNER JOIN dbo.InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = IKD.ItemCode AND IsBase = 1                       
    INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = IKD.ItemCode AND AI.UM = IUM.UnitMeasureCode '                  
    SET @AdditionalField += ', AI.FreeStock'                    
   END                               
  ELSE                                      
   BEGIN                                                
    INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                                
    SELECT EISTV.ItemCode, SUM(FreeStock) AS FreeStock, EISTV.UnitMeasureCode FROM dbo.EcommerceInventoryStockTotalView EISTV    
    INNER JOIN InventoryKitDetail IKD ON IKD.ItemCode = EISTV.ItemCode    
    WHERE ItemKitCode = @ItemKitCode                                                
    GROUP BY EISTV.ItemCode, EISTV.UnitMeasureCode                                                       
  
    SET @AdditionalJoin += ' INNER JOIN dbo.InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = IKD.ItemCode AND IsBase = 1                       
    INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = IKD.ItemCode AND AI.UM = IUM.UnitMeasureCode '                  
    SET @AdditionalField += ', AI.FreeStock'                  
   END                                      
 END                             
-- Specific Warehouse                                      
ELSE                                      
 BEGIN                                           
  IF @HideOutOfStockProducts = 1                                      
   BEGIN                                      
  IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0       
  BEGIN      
   IF (SELECT COUNT(ItemKitCode) FROM #AvailableKitComponents) > 0       
   BEGIN      
    INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)      
    SELECT ItemCode, SUM(FreeStock) AS FreeStock, UnitMeasureCode    
    FROM #AvailableKitComponents    
    WHERE WarehouseCode = @WarehouseCode    
    GROUP BY ItemCode, UnitMeasureCode    
   END     
  END                                      
                  
                  
    SET @AdditionalJoin += ' INNER JOIN dbo.InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = IKD.ItemCode AND IsBase = 1                       
    INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = IKD.ItemCode AND AI.UM = IUM.UnitMeasureCode '                  
    SET @AdditionalField += ', AI.FreeStock'                       
   END                                      
 ELSE                                      
  BEGIN                                      
   INSERT INTO #AvailableItems (ItemCode, FreeStock, UM)                                     
   SELECT EISTV.ItemCode, SUM(FreeStock) AS FreeStock, EISTV.UnitMeasureCode FROM dbo.EcommerceInventoryStockTotalView EISTV    
   INNER JOIN InventoryKitDetail IKD ON IKD.ItemCode = EISTV.ItemCode    
   WHERE ItemKitCode = @ItemKitCode AND WarehouseCode = @WarehouseCode                                                 
   GROUP BY EISTV.ItemCode, EISTV.UnitMeasureCode                                                                   
                  
   SET @AdditionalJoin += ' INNER JOIN dbo.InventoryUnitMeasure IUM WITH (NOLOCK) ON IUM.ItemCode = IKD.ItemCode AND IsBase = 1                                      
   INNER JOIN #AvailableItems AI WITH (NOLOCK) ON AI.ItemCode = IKD.ItemCode AND AI.UM = IUM.UnitMeasureCode '                  
   SET @AdditionalField += ', AI.FreeStock'                                                
  END                                         
 END                                     
/*End of Out of Stock Items*/           
        
IF(@CartId IS NULL)              
BEGIN              
 SET @CustomSelectQry = 'ikd.IsDefault'              
 SET @CustomJoinQry = ''        
END              
ELSE                            
BEGIN              
 SET @CustomSelectQry = 'CAST(                               
    (CASE              
     WHEN wkc.ItemCode IS NOT NULL THEN 1              
     ELSE 0              
     END              
    ) AS BIT)'              
                  
 SET @CustomJoinQry = 'LEFT OUTER JOIN dbo.EcommerceKitCart wkc WITH (NOLOCK) ON ( wkc.ItemKitCode = ikd.ItemKitCode AND wkc.ItemCode = ikd.ItemCode AND wkc.GroupCode = ikd.GroupCode               
          AND wkc.CustomerCode = ' + QUOTENAME(@CustomerCode, '''') + ' AND wkc.CartID = ' + ISNULL(QUOTENAME(@CartID,''''),'NULL') + ')'              
END              
SET @CustomSelectQry += ' AS [Select]'                            
                                       
SET @SqlQuery = ' SELECT  i.Counter, ikd.ItemCode, i.ItemName, it.ItemType, ikd.Quantity,                         
  ikd.UnitMeasureCode, ikdd.ItemDescription, ikd.IsDefault, ikpd.TotalRate, ikd.GroupCode,                           
  ikogd.Description AS GroupDescription, ikogd.HTMLDescription AS GroupHTMLDescription,         
  ikog.GroupType, ikog.SelectionControl, i.Counter + ikd.Counter AS ItemId,                          
  ikog.Counter AS GroupId, ikog.SortOrder, '+@CustomSelectQry+                              
   + @AdditionalField + '                         
  FROM dbo.InventoryKitDetail ikd WITH (NOLOCK)                               
  INNER JOIN dbo.InventoryKitDetailDescription ikdd WITH (NOLOCK) ON ( ikdd.ItemKitCode = ikd.ItemKitCode AND ikdd.ItemCode = ikd.ItemCode AND ikdd.GroupCode = ikd.GroupCode )                           
  INNER JOIN dbo.InventoryKitOptionGroup ikog WITH (NOLOCK) ON ( ikog.ItemKitCode = ikd.ItemKitCode AND ikog.GroupCode = ikd.GroupCode )                              
  INNER JOIN dbo.InventoryKitOptionGroupDescription ikogd WITH (NOLOCK) ON ( ikog.ItemKitCode = ikogd.ItemKitCode AND ikog.GroupCode = ikogd.GroupCode AND ikogd.LanguageCode = ' + QUOTENAME(@languageCode, '''') + ')                          
  INNER JOIN dbo.InventoryItem i WITH (NOLOCK) ON i.ItemCode = ikd.ItemKitCode                          
  INNER JOIN dbo.InventoryItem it WITH (NOLOCK) ON (it.ItemCode = ikd.ItemCode)                          
  INNER JOIN dbo.InventoryKitPricingDetail ikpd WITH (NOLOCK) ON ( ikd.ItemCode = ikpd.ItemCode AND ikpd.ItemKitCode = ikd.ItemKitCode AND ikpd.GroupCode = ikd.GroupCode)                              
  INNER JOIN dbo.InventoryItemDescription iid WITH (NOLOCK) ON iid.ItemCode = ikd.ItemCode '+ @CustomJoinQry +'                                                
  INNER JOIN dbo.Customer c WITH (NOLOCK) ON (' + QUOTENAME(@IsAnonymous, '''') + ' = 1 AND c.CustomerCode = ' + QUOTENAME(@AnonymousCustomerCode, '''') + ')                 
  OR (c.CustomerCode = ' + QUOTENAME(@CustomerCode, '''') + ')                
  INNER JOIN dbo.CRMContact cc WITH (NOLOCK) ON (' + QUOTENAME(@IsAnonymous, '''') + ' = 1 AND cc.EntityCode = ' + QUOTENAME(@AnonymousCustomerCode, '''') + ')                 
  OR (c.CustomerCode = ' + QUOTENAME(@CustomerCode, '''') + ')                 
  AND cc.ContactCode = ' + QUOTENAME(@ContactCode, '''') + '                                  
  INNER JOIN dbo.CustomerShipTo cs WITH (NOLOCK) ON cs.CustomerCode = c.CustomerCode AND cs.ShipToCode = cc.DefaultShippingCode ' +                        
  @AdditionalJoin                        
                          
SET @SqlQuery2 = ' WHERE i.ItemCode = ' + QUOTENAME(@ItemKitcode, '''') + ' AND                           
    ikd.ItemKitCode = ' + QUOTENAME(@ItemKitcode, '''') + ' AND                                
    ikpd.CurrencyCode = ' + QUOTENAME(@currencyCode , '''') + ' AND  
	ikd.GroupCode = ' + CASE WHEN @IncludeAllGroup = 1 THEN 'ikd.GroupCode' ELSE '''Express Bag''' END + ' AND                         
    ikdd.LanguageCode = ' + QUOTENAME(@languageCode , '''') + ' AND                              
    iid.LanguageCode = ' + QUOTENAME(@languageCode , '''') + ' AND                  
    (it.Status = ''A'' OR (it.Status = ''P'' AND it.ItemType IN (''Kit'', ''Matrix Group'')) OR (it.Status = ''P'' AND                     
    it.ItemType IN (''Stock'', ''Non-Stock'', ''Electronic Download'', ''Service'', ''Assembly'', ''Matrix Item'') AND AI.FreeStock > 0))  '    
  
EXEC('SELECT * INTO ##EcommerceKitItems FROM ('+ @SqlQuery + @SqlQuery2 +') QueryResults ORDER BY SortOrder, ItemCode ASC')

DECLARE @HasGroupWithNoDefault BIT = 0 

IF EXISTS (SELECT 1 FROM (SELECT CASE WHEN SUM(CONVERT(INT, IsDefault)) < 1 THEN 0 ELSE SUM(CONVERT(INT, IsDefault)) END AS DefaultCnt, GroupCode FROM ##EcommerceKitItems 
           WHERE GroupType IN ('Required', 'Multi-Select') GROUP BY GroupCode) A WHERE A.DefaultCnt < 1 )
BEGIN
  SET @HasGroupWithNoDefault = 1
END

IF @HideOutOfStockProducts = 1 OR @HasGroupWithNoDefault = 1   
BEGIN    

 DECLARE @KitGroup TABLE (GroupCode NVARCHAR(30), GroupType NVARCHAR(30))   
  
 ;WITH NonOutOfStockKitItemsIsDefault AS (     
  SELECT GroupCode FROM ##EcommerceKitItems WHERE IsDefault = 1 GROUP BY GroupCode    
 )    
     
 INSERT INTO @KitGroup (GroupCode, GroupType)     
 SELECT GroupCode, GroupType FROM ##EcommerceKitItems    
 WHERE GroupCode NOT IN (SELECT GroupCode FROM NonOutOfStockKitItemsIsDefault)    
 GROUP BY GroupCode, GroupType    
     
 IF (SELECT COUNT(GroupCode) FROM @KitGroup) > 0                   
 BEGIN               
  DECLARE @kitGroupCode TABLE (GroupCode NVARCHAR(30))                
  INSERT INTO @kitGroupCode (GroupCode)                
  SELECT GroupCode  FROM ##EcommerceKitItems EKI                
  WHERE EXISTS (SELECT GroupCode FROM @KitGroup WHERE GroupCode = EKI.GroupCode) AND GroupType IN ('Required', 'Multi-Select')                
  GROUP BY GroupCode         
  DECLARE @varGroupCode NVARCHAR(30)                
  WHILE (SELECT COUNT(GroupCode) FROM @kitGroupCode)>0                
  BEGIN                
   SELECT @varGroupCode = GroupCode FROM @kitGroupCode                
       
   UPDATE TOP (1) ##EcommerceKitItems                
   SET IsDefault = 1,                
   [Select] = 1                
   WHERE GroupCode = @varGroupCode                
       
   DELETE FROM @kitGroupCode WHERE GroupCode = @varGroupCode                
  END                
 END                      
  SELECT * FROM ##EcommerceKitItems    
END    
ELSE    
BEGIN    
  SELECT * FROM ##EcommerceKitItems
END                                                                        
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#NonOutOfStockKitItems'))                        
 DROP TABLE #NonOutOfStockKitItems     
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableItems'))                       
 DROP TABLE #AvailableItems    
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AvailableKitComponents'))                                                                      
 DROP TABLE #AvailableKitComponents    
IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..##EcommerceKitItems'))                                                                      
 DROP TABLE ##EcommerceKitItems    
         
 SET NOCOUNT OFF    
END 

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[EcommerceUpdateCustomer]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[EcommerceUpdateCustomer]
GO

CREATE PROCEDURE [dbo].[EcommerceUpdateCustomer](            
 @ContactGUID UNIQUEIDENTIFIER,            
 @FirstName NVARCHAR(50),            
 @LastName NVARCHAR(50),            
 @SalutationCode NVARCHAR(30),            
 @Phone NVARCHAR(50),            
 @IsOver13 BIT,            
 @EmailRule NVARCHAR(20),            
 @IsOkToEmail BIT,      
 @BusinessType NVARCHAR(30),            
 @TaxNumber NVARCHAR(50),             
 @Email NVARCHAR(50),             
 @Password NVARCHAR(50),             
 @PasswordSalt NVARCHAR(50),            
 @PasswordIV NVARCHAR(50),  
 @Mobile NVARCHAR(50)= NULL,
 @Company NVARCHAR(200)= NULL
)            
AS            
BEGIN            
 UPDATE c            
 SET c.Email = @Email,            
  c.Telephone = @Phone,            
  c.Over13Checked = @IsOver13,            
  c.BusinessType = @BusinessType,            
  c.TaxNumber = @TaxNumber,
  c.CustomerName = ISNULL(@Company, c.CustomerName)
 FROM Customer c            
 INNER JOIN CRMContact cc ON cc.EntityCode = c.CustomerCode --AND c.DefaultContact = cc.ContactCode          
 WHERE cc.ContactGUID = @ContactGUID       
            
 UPDATE cc            
 SET cc.ContactFirstName = @FirstName,            
  cc.ContactLastName = @LastName,            
  cc.ContactSalutationCode = @SalutationCode,            
  cc.UserName = @Email,            
  cc.Email1 = @Email,            
  cc.EmailRule = @EmailRule,       
  cc.IsOkToEmail = @IsOkToEmail,           
  cc.BusinessPhone = @Phone,            
  cc.HomePhone = @Phone,            
  cc.Password = ISNULL(@Password, cc.Password),            
  cc.PasswordSalt = ISNULL(@PasswordSalt, cc.PasswordSalt),            
  cc.PasswordIV = ISNULL(@PasswordIV, cc.PasswordIV),      
  cc.Mobile =  @Mobile     
 FROM CRMContact cc         
 WHERE cc.ContactGUID = @ContactGUID       
     
 UPDATE ec    
 SET  ec.IsOver13 = @IsOver13    
 FROM EcommerceCustomer ec    
 WHERE ec.ContactGUID = @ContactGUID       
END 

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[eCommerceGetPaymentTermGroup]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[eCommerceGetPaymentTermGroup]
GO

CREATE PROCEDURE [dbo].[eCommerceGetPaymentTermGroup]      
 @ContactCode NVARCHAR(30),  
 @IsAnonymous BIT = 0,  
 @CountryCode NVARCHAR(30) = NULL,  
 @BusinessType NVARCHAR(30) = NULL       
AS      
BEGIN  
 IF @IsAnonymous = 0  
 BEGIN  
  SELECT tp.PaymentMethodCode,         
     pt.PaymentTermCode,         
     pt.PaymentTermDescription,        
	 pt.PaymentType,
     CASE         
   WHEN pt.PaymentTermCode = cs.PaymentTermCode THEN 1         
   ELSE 0         
     END AS IsDefault           
   FROM SystemPaymentTerm pt WITH (NOLOCK)         
   INNER JOIN SystemPaymentType tp WITH (NOLOCK) ON pt.PaymentType = tp.PaymentTypeCode        
   INNER JOIN SystemPaymentTermGroupDetail ptgd WITH (NOLOCK) ON ptgd.PaymentTermCode = pt.PaymentTermCode AND pt.IsActive = 1        
   INNER JOIN SystemPaymentTermGroup ptg WITH (NOLOCK) ON ptg.PaymentTermGroup = ptgd.PaymentTermGroup AND ptg.IsActive = 1        
   INNER JOIN CRMContact crm WITH (NOLOCK) ON crm.ContactCode = @ContactCode   
   INNER JOIN Customer c WITH (NOLOCK) ON c.CustomerCode = crm.EntityCode        
   INNER JOIN CustomerShipTo cs WITH (NOLOCK) ON crm.DefaultShippingCode = cs.ShipToCode AND cs.CustomerCode = c.CustomerCode AND cs.PaymentTermGroup = ptg.PaymentTermGroup          
   WHERE pt.PaymentTermCode <> 'Google'    
   ORDER BY IsDefault DESC  
 END  
 ELSE  
 BEGIN  
  CREATE TABLE #AnonPaymentMethod (  
       PaymentMethodCode NVARCHAR(30),  
       PaymentTermCode NVARCHAR(30),  
       PaymentTermDescription NVARCHAR(MAX),  
	   PaymentType NVARCHAR(60),  
       IsDefault BIT )   
    
  IF @BusinessType = 'Retail'  
  BEGIN  
   INSERT INTO #AnonPaymentMethod (PaymentMethodCode, PaymentTermCode, PaymentTermDescription, PaymentType,IsDefault)  
   SELECT tp.PaymentMethodCode,         
      pt.PaymentTermCode,         
      pt.PaymentTermDescription,  
	  pt.PaymentType,
      0 AS IsDefault       
    FROM SystemPaymentTerm pt WITH (NOLOCK)         
    INNER JOIN SystemPaymentType tp WITH (NOLOCK) ON pt.PaymentType = tp.PaymentTypeCode        
    INNER JOIN SystemPaymentTermGroupDetail ptgd WITH (NOLOCK) ON ptgd.PaymentTermCode = pt.PaymentTermCode AND pt.IsActive = 1        
    INNER JOIN SystemPaymentTermGroup ptg WITH (NOLOCK) ON ptg.PaymentTermGroup = ptgd.PaymentTermGroup AND ptg.IsActive = 1   
    WHERE ptg.PaymentTermGroup =   
       ( SELECT PaymentTermGroup        
      FROM CustomerShipToClassTemplateDetailView        
      WHERE ClassCode =         
      ( SELECT DefaultRetailCustomerShipToClassTemplate  
      FROM SystemCountry        
      WHERE CountryCode = @CountryCode))  
   AND pt.PaymentTermCode <> 'Google'   
  END  
  ELSE  
  BEGIN  
   INSERT INTO #AnonPaymentMethod (PaymentMethodCode, PaymentTermCode, PaymentTermDescription, PaymentType, IsDefault)  
   SELECT tp.PaymentMethodCode,         
   pt.PaymentTermCode,         
   pt.PaymentTermDescription,  
   pt.PaymentType,
   0 AS IsDefault          
   FROM SystemPaymentTerm pt WITH (NOLOCK)         
   INNER JOIN SystemPaymentType tp WITH (NOLOCK) ON pt.PaymentType = tp.PaymentTypeCode        
   INNER JOIN SystemPaymentTermGroupDetail ptgd WITH (NOLOCK) ON ptgd.PaymentTermCode = pt.PaymentTermCode AND pt.IsActive = 1        
   INNER JOIN SystemPaymentTermGroup ptg WITH (NOLOCK) ON ptg.PaymentTermGroup = ptgd.PaymentTermGroup AND ptg.IsActive = 1   
   WHERE ptg.PaymentTermGroup =   
       ( SELECT PaymentTermGroup        
      FROM CustomerShipToClassTemplateDetailView        
      WHERE ClassCode =         
      ( SELECT DefaultWholesaleCustomerShipToClassTemplate  
      FROM SystemCountry        
      WHERE CountryCode = @CountryCode))  
   AND pt.PaymentTermCode <> 'Google'   
  END  
    
  UPDATE TOP (1) #AnonPaymentMethod  
  SET [IsDefault] = 1  
    
  SELECT * FROM #AnonPaymentMethod  
    
  IF EXISTS (SELECT 1 FROM TEMPDB..SYSOBJECTS WHERE ID = OBJECT_ID('TEMPDB..#AnonPaymentMethod'))                                                          
   DROP TABLE #AnonPaymentMethod                                       
    
 END        
END 

GO
/************************************************************************************************************************/
IF  EXISTS (SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'[dbo].[eCommerceGetPaymentTerm]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[eCommerceGetPaymentTerm]
GO

CREATE PROCEDURE [dbo].[eCommerceGetPaymentTerm]
	@PaymentTermCode NVARCHAR(30)	
AS
BEGIN
	SELECT	tp.PaymentMethodCode, 
			pt.PaymentTermCode, 
			pt.PaymentTermDescription,
			pt.PaymentType
	FROM SystemPaymentTerm pt WITH (NOLOCK) 
	INNER JOIN SystemPaymentType tp WITH (NOLOCK) ON pt.PaymentType = tp.PaymentTypeCode 
	WHERE pt.PaymentTermCode = @PaymentTermCode				
END

GO
/************************************************************************************************************************/

/************************************************************************************************************************/

/************************************************************************************************************************/

/************************************************************************************************************************/

/************************************************************************************************************************/

/************************************************************************************************************************/










